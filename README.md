# README #

### What is this repository for? ###

* This is a Vivado project for the Condor Mk3 LET FPGA prototype design in the ZCU208 board. 
* Vivado version: 2021.1

### ZCU208 Board ###

* In order to set up all clocks needed in this design, run BoardUI.exe and configure the clock generator with the following config files:

    * 8A34001_20191007_100MHz_CLK0_156MHz.txt
    * 8A34001_20191007_100MHz_CLK0_156MHz.tcs

* Click 'Set' and verify the config in the 'Read' tab.

* The clock Si570 User C0 must be configured to 333.5 MHz. 

* The clocks in 8A34001 can not be programmed in the EEPROM, so the configuration must be done after every power up.

### How do I get set up? ###

* Ensure the required Vivado version is installed in your computer.
* Set the enviroment variable 'VIVADO_SCRIPT' pointing to the target vivado installation path. In Windows, search for "Edit enviroment variables for your account" and open the dialog to add or modify an enviroment variable. This is an example of VIVADO_SCRIPT pointing to a Vivado 2021.1 installation:
VIVADO_SCRIPT=C:\Xilinx\Vivado\2021.1\bin
* Execute run.sh (if you are using Windows, you may need git bash terminal for example).
Move to the project path and type: ./run.sh
* In the menu, press 1 to build the Vivado proyect.
* In the menu, press 4 to build the project and generate bitstream, hdf and mcs files.

### How do I update the software (elf file)? ###

* Replace the elf file in the "elf" folder.
* The name of the elf file must be the same name of the Block design (check bd/). For example:

  ./bd/system_LET.tcl
  ./elf/system_LET.elf

"system_LET" will be the name of the block design which contains the Microblaze. The names must match. If there is more than one, you must provide an elf file for each block design.

### Script menu guidelines ###

* 1) Build project: Creates the vivado project in "prj" folder according to the config file and adds all the sources.
* 2) Clean project /!\: Deletes "prj" folder.
* 3) Generate Outputs: Runs synthesis, implementation and bitstream.
* 4) Run All: Runs Build project, synthesis, implementation and bitstream.
* 5) Update block design and IPs: Saves the block designs of the project (in "prj") to a tcl file in "bd" folder. Saves the IPs into tcl scripts.
* 6) Create folder structure (for new project): Creates the folder structure "src", "bd" and "ip".
* 7) Open Vivado: Opens Vivado software.
* 8) Re-run Bitstream only: Resets bitstream step and re-runs it.
* 9) Generate documentation: saves Block designs into pdf files.

### Who do I run the unit tests? ###
* 1) Install docker in a linux machine.
* 2) Run ./tb/run.sh in the terminal in the root path of the project.
* 3) The tests are automatically launched in the repo cloud when merging to master.
### Who do I talk to? ###

* Repo owner: Gustavo Martin (gustavo.martin@mynaric.com)