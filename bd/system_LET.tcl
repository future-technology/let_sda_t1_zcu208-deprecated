
################################################################
# This is a generated script based on design: system_LET
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2022.1
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_gid_msg -ssname BD::TCL -id 2041 -severity "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source system_LET_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xczu48dr-fsvg1517-2-e
   set_property BOARD_PART xilinx.com:zcu208:part0:2.0 [current_project]
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name system_LET

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_gid_msg -ssname BD::TCL -id 2001 -severity "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_gid_msg -ssname BD::TCL -id 2002 -severity "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_gid_msg -ssname BD::TCL -id 2003 -severity "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_gid_msg -ssname BD::TCL -id 2004 -severity "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

  # Add USER_COMMENTS on $design_name
  set_property USER_COMMENTS.comment_21 "Each GPIO instance is connected as ( 4 instances for 
2 EDFAs for each LET:
0: rx_en
1: tx_en
2: flt_n
3: disable_1
4: disable_2
5: rst
6: tcvr_en

" [get_bd_designs $design_name]

common::send_gid_msg -ssname BD::TCL -id 2005 -severity "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_gid_msg -ssname BD::TCL -id 2006 -severity "ERROR" $errMsg}
   return $nRet
}

set bCheckIPsPassed 1
##################################################################
# CHECK IPs
##################################################################
set bCheckIPs 1
if { $bCheckIPs == 1 } {
   set list_check_ips "\ 
xilinx.com:ip:axi_quad_spi:3.2\
xilinx.com:ip:clk_wiz:6.0\
xilinx.com:ip:proc_sys_reset:5.0\
xilinx.com:ip:xlconcat:2.1\
xilinx.com:ip:axi_uart16550:2.0\
xilinx.com:ip:xlconstant:1.1\
xilinx.com:ip:axi_bram_ctrl:4.1\
xilinx.com:ip:blk_mem_gen:8.4\
xilinx.com:ip:axi_gpio:2.0\
xilinx.com:ip:axi_intc:4.1\
xilinx.com:ip:axi_timebase_wdt:3.0\
xilinx.com:ip:axi_timer:2.0\
xilinx.com:ip:mdm:3.2\
xilinx.com:ip:microblaze:11.0\
xilinx.com:ip:lmb_bram_if_cntlr:4.0\
xilinx.com:ip:lmb_v10:3.0\
"

   set list_ips_missing ""
   common::send_gid_msg -ssname BD::TCL -id 2011 -severity "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

   foreach ip_vlnv $list_check_ips {
      set ip_obj [get_ipdefs -all $ip_vlnv]
      if { $ip_obj eq "" } {
         lappend list_ips_missing $ip_vlnv
      }
   }

   if { $list_ips_missing ne "" } {
      catch {common::send_gid_msg -ssname BD::TCL -id 2012 -severity "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
      set bCheckIPsPassed 0
   }

}

if { $bCheckIPsPassed != 1 } {
  common::send_gid_msg -ssname BD::TCL -id 2023 -severity "WARNING" "Will not continue with creation of design due to the error(s) above."
  return 3
}

##################################################################
# DESIGN PROCs
##################################################################


# Hierarchical cell: mb_core_local_memory
proc create_hier_cell_mb_core_local_memory { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2092 -severity "ERROR" "create_hier_cell_mb_core_local_memory() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 DLMB

  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 ILMB


  # Create pins
  create_bd_pin -dir I -type clk Clk
  create_bd_pin -dir I -type rst SYS_Rst

  # Create instance: dlmb_bram_if_cntlr, and set properties
  set dlmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 dlmb_bram_if_cntlr ]
  set_property -dict [ list \
   CONFIG.C_CE_COUNTER_WIDTH {0} \
   CONFIG.C_CE_FAILING_REGISTERS {0} \
   CONFIG.C_ECC {0} \
   CONFIG.C_ECC_ONOFF_REGISTER {0} \
   CONFIG.C_ECC_STATUS_REGISTERS {0} \
   CONFIG.C_FAULT_INJECT {0} \
   CONFIG.C_INTERCONNECT {0} \
   CONFIG.C_NUM_LMB {1} \
   CONFIG.C_UE_FAILING_REGISTERS {0} \
 ] $dlmb_bram_if_cntlr

  # Create instance: dlmb_v10, and set properties
  set dlmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 dlmb_v10 ]

  # Create instance: ilmb_bram_if_cntlr, and set properties
  set ilmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 ilmb_bram_if_cntlr ]
  set_property -dict [ list \
   CONFIG.C_CE_COUNTER_WIDTH {0} \
   CONFIG.C_CE_FAILING_REGISTERS {0} \
   CONFIG.C_ECC {0} \
   CONFIG.C_ECC_ONOFF_REGISTER {0} \
   CONFIG.C_ECC_STATUS_REGISTERS {0} \
   CONFIG.C_FAULT_INJECT {0} \
   CONFIG.C_INTERCONNECT {0} \
   CONFIG.C_UE_FAILING_REGISTERS {0} \
 ] $ilmb_bram_if_cntlr

  # Create instance: ilmb_v10, and set properties
  set ilmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 ilmb_v10 ]

  # Create instance: lmb_bram, and set properties
  set lmb_bram [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 lmb_bram ]
  set_property -dict [ list \
   CONFIG.Enable_B {Use_ENB_Pin} \
   CONFIG.Memory_Type {True_Dual_Port_RAM} \
   CONFIG.Port_B_Clock {100} \
   CONFIG.Port_B_Enable_Rate {100} \
   CONFIG.Port_B_Write_Rate {50} \
   CONFIG.Use_RSTB_Pin {true} \
   CONFIG.use_bram_block {BRAM_Controller} \
 ] $lmb_bram

  # Create interface connections
  connect_bd_intf_net -intf_net processor_dlmb [get_bd_intf_pins DLMB] [get_bd_intf_pins dlmb_v10/LMB_M]
  connect_bd_intf_net -intf_net processor_dlmb_bus [get_bd_intf_pins dlmb_bram_if_cntlr/SLMB] [get_bd_intf_pins dlmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net processor_dlmb_cntlr [get_bd_intf_pins dlmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTA]
  connect_bd_intf_net -intf_net processor_ilmb [get_bd_intf_pins ILMB] [get_bd_intf_pins ilmb_v10/LMB_M]
  connect_bd_intf_net -intf_net processor_ilmb_bus [get_bd_intf_pins ilmb_bram_if_cntlr/SLMB] [get_bd_intf_pins ilmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net processor_ilmb_cntlr [get_bd_intf_pins ilmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTB]

  # Create port connections
  connect_bd_net -net SYS_Rst_1 [get_bd_pins SYS_Rst] [get_bd_pins dlmb_bram_if_cntlr/LMB_Rst] [get_bd_pins dlmb_v10/SYS_Rst] [get_bd_pins ilmb_bram_if_cntlr/LMB_Rst] [get_bd_pins ilmb_v10/SYS_Rst]
  connect_bd_net -net processor_Clk [get_bd_pins Clk] [get_bd_pins dlmb_bram_if_cntlr/LMB_Clk] [get_bd_pins dlmb_v10/LMB_Clk] [get_bd_pins ilmb_bram_if_cntlr/LMB_Clk] [get_bd_pins ilmb_v10/LMB_Clk]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: Service_Interface
proc create_hier_cell_Service_Interface { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2092 -severity "ERROR" "create_hier_cell_Service_Interface() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S_AXI_0

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S_AXI_1


  # Create pins
  create_bd_pin -dir I -type rst s_axi_aresetn
  create_bd_pin -dir O -from 2 -to 0 serv_int
  create_bd_pin -dir I serv_uart_rx1
  create_bd_pin -dir I serv_uart_rx2
  create_bd_pin -dir O serv_uart_tx1
  create_bd_pin -dir O serv_uart_tx2
  create_bd_pin -dir I -type clk sys_clk

  # Create instance: Service_UART1, and set properties
  set Service_UART1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_uart16550:2.0 Service_UART1 ]

  # Create instance: Service_UART2, and set properties
  set Service_UART2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_uart16550:2.0 Service_UART2 ]

  # Create instance: Service_int_xlconcat, and set properties
  set Service_int_xlconcat [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 Service_int_xlconcat ]
  set_property -dict [ list \
   CONFIG.NUM_PORTS {3} \
 ] $Service_int_xlconcat

  # Create instance: xlconstant_w1v0, and set properties
  set xlconstant_w1v0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_w1v0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
 ] $xlconstant_w1v0

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins S_AXI_0] [get_bd_intf_pins Service_UART1/S_AXI]
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins S_AXI_1] [get_bd_intf_pins Service_UART2/S_AXI]

  # Create port connections
  connect_bd_net -net Service_int_xlconcat_dout [get_bd_pins serv_int] [get_bd_pins Service_int_xlconcat/dout]
  connect_bd_net -net processor_Clk [get_bd_pins sys_clk] [get_bd_pins Service_UART1/s_axi_aclk] [get_bd_pins Service_UART2/s_axi_aclk]
  connect_bd_net -net rst_Clk_100M_peripheral_aresetn [get_bd_pins s_axi_aresetn] [get_bd_pins Service_UART1/s_axi_aresetn] [get_bd_pins Service_UART2/s_axi_aresetn]
  connect_bd_net -net sin_0_1 [get_bd_pins serv_uart_rx1] [get_bd_pins Service_UART1/sin]
  connect_bd_net -net sin_1_1 [get_bd_pins serv_uart_rx2] [get_bd_pins Service_UART2/sin]
  connect_bd_net -net uart_serv_conn1_ip2intc_irpt [get_bd_pins Service_UART1/ip2intc_irpt] [get_bd_pins Service_int_xlconcat/In0]
  connect_bd_net -net uart_serv_conn1_sout [get_bd_pins serv_uart_tx1] [get_bd_pins Service_UART1/sout]
  connect_bd_net -net uart_serv_conn2_ip2intc_irpt [get_bd_pins Service_UART2/ip2intc_irpt] [get_bd_pins Service_int_xlconcat/In1]
  connect_bd_net -net uart_serv_conn2_sout [get_bd_pins serv_uart_tx2] [get_bd_pins Service_UART2/sout]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins Service_UART1/freeze] [get_bd_pins Service_UART2/freeze] [get_bd_pins xlconstant_w1v0/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: LET_AXI_Interconnects
proc create_hier_cell_LET_AXI_Interconnects { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2092 -severity "ERROR" "create_hier_cell_LET_AXI_Interconnects() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 AXI_LET0_AXI_ETH_REG_IF

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 AXI_LET0_AXI_LET

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 AXI_LET1_AXI_ETH_REG_IF

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 AXI_LET1_AXI_LET

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S_AXI_LET1

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S_AXI_LET2


  # Create pins
  create_bd_pin -dir I -type clk LET1_clk
  create_bd_pin -dir I -type rst LET1_rst_n
  create_bd_pin -dir I -type clk LET2_clk
  create_bd_pin -dir I -type rst LET2_rst_n
  create_bd_pin -dir I -type clk sys_clk
  create_bd_pin -dir I -type rst sys_resetn

  # Create instance: LET0_SW_Control_axi_interconnect, and set properties
  set LET0_SW_Control_axi_interconnect [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 LET0_SW_Control_axi_interconnect ]
  set_property -dict [ list \
   CONFIG.NUM_MI {2} \
 ] $LET0_SW_Control_axi_interconnect

  # Create instance: LET1_SW_Control_axi_interconnect, and set properties
  set LET1_SW_Control_axi_interconnect [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 LET1_SW_Control_axi_interconnect ]
  set_property -dict [ list \
   CONFIG.NUM_MI {2} \
 ] $LET1_SW_Control_axi_interconnect

  # Create interface connections
  connect_bd_intf_net -intf_net LET1_SW_Control_axi_interconnect_M01_AXI [get_bd_intf_pins AXI_LET0_AXI_ETH_REG_IF] [get_bd_intf_pins LET0_SW_Control_axi_interconnect/M01_AXI]
  connect_bd_intf_net -intf_net LET1_interconnect_M00_AXI [get_bd_intf_pins AXI_LET0_AXI_LET] [get_bd_intf_pins LET0_SW_Control_axi_interconnect/M00_AXI]
  connect_bd_intf_net -intf_net LET2_SW_Control_axi_interconnect_M01_AXI [get_bd_intf_pins AXI_LET1_AXI_ETH_REG_IF] [get_bd_intf_pins LET1_SW_Control_axi_interconnect/M01_AXI]
  connect_bd_intf_net -intf_net LET2_interconnect_M00_AXI [get_bd_intf_pins AXI_LET1_AXI_LET] [get_bd_intf_pins LET1_SW_Control_axi_interconnect/M00_AXI]
  connect_bd_intf_net -intf_net S00_AXI_1 [get_bd_intf_pins S_AXI_LET2] [get_bd_intf_pins LET1_SW_Control_axi_interconnect/S00_AXI]
  connect_bd_intf_net -intf_net processor_axi_periph_M04_AXI [get_bd_intf_pins S_AXI_LET1] [get_bd_intf_pins LET0_SW_Control_axi_interconnect/S00_AXI]

  # Create port connections
  connect_bd_net -net LET1_framer_clk_1 [get_bd_pins LET1_clk] [get_bd_pins LET0_SW_Control_axi_interconnect/M00_ACLK]
  connect_bd_net -net LET1_framer_rst_n_1 [get_bd_pins LET1_rst_n] [get_bd_pins LET0_SW_Control_axi_interconnect/M00_ARESETN]
  connect_bd_net -net LET2_framer_clk_1 [get_bd_pins LET2_clk] [get_bd_pins LET1_SW_Control_axi_interconnect/M00_ACLK]
  connect_bd_net -net LET2_framer_rst_n_1 [get_bd_pins LET2_rst_n] [get_bd_pins LET1_SW_Control_axi_interconnect/M00_ARESETN]
  connect_bd_net -net processor_Clk [get_bd_pins sys_clk] [get_bd_pins LET0_SW_Control_axi_interconnect/ACLK] [get_bd_pins LET0_SW_Control_axi_interconnect/M01_ACLK] [get_bd_pins LET0_SW_Control_axi_interconnect/S00_ACLK] [get_bd_pins LET1_SW_Control_axi_interconnect/ACLK] [get_bd_pins LET1_SW_Control_axi_interconnect/M01_ACLK] [get_bd_pins LET1_SW_Control_axi_interconnect/S00_ACLK]
  connect_bd_net -net rst_Clk_100M_peripheral_aresetn [get_bd_pins sys_resetn] [get_bd_pins LET0_SW_Control_axi_interconnect/ARESETN] [get_bd_pins LET0_SW_Control_axi_interconnect/M01_ARESETN] [get_bd_pins LET0_SW_Control_axi_interconnect/S00_ARESETN] [get_bd_pins LET1_SW_Control_axi_interconnect/ARESETN] [get_bd_pins LET1_SW_Control_axi_interconnect/M01_ARESETN] [get_bd_pins LET1_SW_Control_axi_interconnect/S00_ARESETN]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: LET1G_MB_Core
proc create_hier_cell_LET1G_MB_Core { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2092 -severity "ERROR" "create_hier_cell_LET1G_MB_Core() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M04_AXI_SYS

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M05_AXI_LET


  # Create pins
  create_bd_pin -dir I dcm_locked
  create_bd_pin -dir I -from 23 -to 0 -type intr intr
  create_bd_pin -dir O -type rst mb_debug_sys_rst
  create_bd_pin -dir I -type rst reset
  create_bd_pin -dir O -from 0 -to 0 -type rst sync100_rstn
  create_bd_pin -dir I -type clk sys_clk_out
  create_bd_pin -dir O -type intr timer_int
  create_bd_pin -dir O -type intr wdt_interrupt
  create_bd_pin -dir O -type rst wdt_reset
  create_bd_pin -dir O wdt_reset_pending

  # Create instance: axi_bram_ctrl_0, and set properties
  set axi_bram_ctrl_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.1 axi_bram_ctrl_0 ]
  set_property -dict [ list \
   CONFIG.ECC_TYPE {0} \
   CONFIG.READ_LATENCY {3} \
   CONFIG.SINGLE_PORT_BRAM {1} \
   CONFIG.USE_ECC {0} \
 ] $axi_bram_ctrl_0

  # Create instance: blk_mem_gen_0, and set properties
  set blk_mem_gen_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 blk_mem_gen_0 ]

  # Create instance: mb_core_axi_gpio, and set properties
  set mb_core_axi_gpio [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 mb_core_axi_gpio ]
  set_property -dict [ list \
   CONFIG.C_ALL_INPUTS {1} \
   CONFIG.C_GPIO_WIDTH {8} \
 ] $mb_core_axi_gpio

  # Create instance: mb_core_axi_intc, and set properties
  set mb_core_axi_intc [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_intc:4.1 mb_core_axi_intc ]
  set_property -dict [ list \
   CONFIG.C_HAS_FAST {1} \
 ] $mb_core_axi_intc

  # Create instance: mb_core_axi_interconnect, and set properties
  set mb_core_axi_interconnect [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 mb_core_axi_interconnect ]
  set_property -dict [ list \
   CONFIG.NUM_MI {7} \
 ] $mb_core_axi_interconnect

  # Create instance: mb_core_axi_timebase_wdt, and set properties
  set mb_core_axi_timebase_wdt [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_timebase_wdt:3.0 mb_core_axi_timebase_wdt ]
  set_property -dict [ list \
   CONFIG.ENABLE_WINDOW_WDT {1} \
 ] $mb_core_axi_timebase_wdt

  # Create instance: mb_core_axi_timer, and set properties
  set mb_core_axi_timer [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_timer:2.0 mb_core_axi_timer ]

  # Create instance: mb_core_local_memory
  create_hier_cell_mb_core_local_memory $hier_obj mb_core_local_memory

  # Create instance: mb_core_mdm, and set properties
  set mb_core_mdm [ create_bd_cell -type ip -vlnv xilinx.com:ip:mdm:3.2 mb_core_mdm ]
  set_property -dict [ list \
   CONFIG.C_ADDR_SIZE {32} \
   CONFIG.C_DBG_MEM_ACCESS {0} \
   CONFIG.C_DBG_REG_ACCESS {0} \
   CONFIG.C_M_AXI_ADDR_WIDTH {32} \
   CONFIG.C_S_AXI_ADDR_WIDTH {4} \
   CONFIG.C_TRIG_IN_PORTS {1} \
   CONFIG.C_TRIG_OUT_PORTS {1} \
   CONFIG.C_USE_CROSS_TRIGGER {0} \
   CONFIG.C_USE_UART {0} \
 ] $mb_core_mdm

  # Create instance: mb_core_rst_Clk_100M, and set properties
  set mb_core_rst_Clk_100M [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 mb_core_rst_Clk_100M ]

  # Create instance: mb_core_xlconcat, and set properties
  set mb_core_xlconcat [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 mb_core_xlconcat ]

  # Create instance: microblaze_0, and set properties
  set microblaze_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:microblaze:11.0 microblaze_0 ]
  set_property -dict [ list \
   CONFIG.C_ADDR_TAG_BITS {0} \
   CONFIG.C_DCACHE_ADDR_TAG {0} \
   CONFIG.C_DEBUG_ENABLED {2} \
   CONFIG.C_DIV_ZERO_EXCEPTION {1} \
   CONFIG.C_D_AXI {1} \
   CONFIG.C_D_LMB {1} \
   CONFIG.C_FAULT_TOLERANT {1} \
   CONFIG.C_FPU_EXCEPTION {1} \
   CONFIG.C_ILL_OPCODE_EXCEPTION {1} \
   CONFIG.C_I_LMB {1} \
   CONFIG.C_M_AXI_D_BUS_EXCEPTION {1} \
   CONFIG.C_M_AXI_I_BUS_EXCEPTION {1} \
   CONFIG.C_OPCODE_0x0_ILLEGAL {1} \
   CONFIG.C_UNALIGNED_EXCEPTIONS {1} \
   CONFIG.C_USE_BARREL {1} \
   CONFIG.C_USE_DIV {1} \
   CONFIG.C_USE_FPU {2} \
   CONFIG.C_USE_HW_MUL {2} \
   CONFIG.C_USE_MSR_INSTR {1} \
   CONFIG.C_USE_PCMP_INSTR {1} \
   CONFIG.C_USE_STACK_PROTECTION {1} \
   CONFIG.G_USE_EXCEPTIONS {1} \
 ] $microblaze_0

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins M04_AXI_SYS] [get_bd_intf_pins mb_core_axi_interconnect/M04_AXI]
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins M05_AXI_LET] [get_bd_intf_pins mb_core_axi_interconnect/M05_AXI]
  connect_bd_intf_net -intf_net axi_bram_ctrl_0_BRAM_PORTA [get_bd_intf_pins axi_bram_ctrl_0/BRAM_PORTA] [get_bd_intf_pins blk_mem_gen_0/BRAM_PORTA]
  connect_bd_intf_net -intf_net axi_interconnect_0_M00_AXI [get_bd_intf_pins mb_core_axi_intc/s_axi] [get_bd_intf_pins mb_core_axi_interconnect/M00_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_0_M01_AXI [get_bd_intf_pins mb_core_axi_interconnect/M01_AXI] [get_bd_intf_pins mb_core_axi_timer/S_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_0_M02_AXI [get_bd_intf_pins mb_core_axi_interconnect/M02_AXI] [get_bd_intf_pins mb_core_axi_timebase_wdt/S_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_0_M03_AXI [get_bd_intf_pins mb_core_axi_gpio/S_AXI] [get_bd_intf_pins mb_core_axi_interconnect/M03_AXI]
  connect_bd_intf_net -intf_net mb_core_axi_interconnect_M06_AXI [get_bd_intf_pins axi_bram_ctrl_0/S_AXI] [get_bd_intf_pins mb_core_axi_interconnect/M06_AXI]
  connect_bd_intf_net -intf_net microblaze_0_M_AXI_DP [get_bd_intf_pins mb_core_axi_interconnect/S00_AXI] [get_bd_intf_pins microblaze_0/M_AXI_DP]
  connect_bd_intf_net -intf_net processor_debug [get_bd_intf_pins mb_core_mdm/MBDEBUG_0] [get_bd_intf_pins microblaze_0/DEBUG]
  connect_bd_intf_net -intf_net processor_dlmb_1 [get_bd_intf_pins mb_core_local_memory/DLMB] [get_bd_intf_pins microblaze_0/DLMB]
  connect_bd_intf_net -intf_net processor_ilmb_1 [get_bd_intf_pins mb_core_local_memory/ILMB] [get_bd_intf_pins microblaze_0/ILMB]
  connect_bd_intf_net -intf_net processor_interrupt [get_bd_intf_pins mb_core_axi_intc/interrupt] [get_bd_intf_pins microblaze_0/INTERRUPT]

  # Create port connections
  connect_bd_net -net axi_timebase_wdt_0_wdt_interrupt [get_bd_pins wdt_interrupt] [get_bd_pins mb_core_axi_timebase_wdt/wdt_interrupt]
  connect_bd_net -net axi_timebase_wdt_0_wdt_reset [get_bd_pins wdt_reset] [get_bd_pins mb_core_axi_timebase_wdt/wdt_reset] [get_bd_pins mb_core_rst_Clk_100M/aux_reset_in]
  connect_bd_net -net axi_timebase_wdt_0_wdt_reset_pending [get_bd_pins wdt_reset_pending] [get_bd_pins mb_core_axi_timebase_wdt/wdt_reset_pending] [get_bd_pins mb_core_xlconcat/In0]
  connect_bd_net -net axi_timebase_wdt_0_wdt_state_vec [get_bd_pins mb_core_axi_timebase_wdt/wdt_state_vec] [get_bd_pins mb_core_xlconcat/In1]
  connect_bd_net -net axi_timer_0_interrupt [get_bd_pins timer_int] [get_bd_pins mb_core_axi_timer/interrupt]
  connect_bd_net -net clk_wiz_0_locked [get_bd_pins dcm_locked] [get_bd_pins mb_core_rst_Clk_100M/dcm_locked]
  connect_bd_net -net mdm_0_Debug_SYS_Rst [get_bd_pins mb_debug_sys_rst] [get_bd_pins mb_core_mdm/Debug_SYS_Rst] [get_bd_pins mb_core_rst_Clk_100M/mb_debug_sys_rst]
  connect_bd_net -net processor_Clk [get_bd_pins sys_clk_out] [get_bd_pins axi_bram_ctrl_0/s_axi_aclk] [get_bd_pins mb_core_axi_gpio/s_axi_aclk] [get_bd_pins mb_core_axi_intc/processor_clk] [get_bd_pins mb_core_axi_intc/s_axi_aclk] [get_bd_pins mb_core_axi_interconnect/ACLK] [get_bd_pins mb_core_axi_interconnect/M00_ACLK] [get_bd_pins mb_core_axi_interconnect/M01_ACLK] [get_bd_pins mb_core_axi_interconnect/M02_ACLK] [get_bd_pins mb_core_axi_interconnect/M03_ACLK] [get_bd_pins mb_core_axi_interconnect/M04_ACLK] [get_bd_pins mb_core_axi_interconnect/M05_ACLK] [get_bd_pins mb_core_axi_interconnect/M06_ACLK] [get_bd_pins mb_core_axi_interconnect/S00_ACLK] [get_bd_pins mb_core_axi_timebase_wdt/s_axi_aclk] [get_bd_pins mb_core_axi_timer/s_axi_aclk] [get_bd_pins mb_core_local_memory/Clk] [get_bd_pins mb_core_rst_Clk_100M/slowest_sync_clk] [get_bd_pins microblaze_0/Clk]
  connect_bd_net -net reset_1 [get_bd_pins reset] [get_bd_pins mb_core_rst_Clk_100M/ext_reset_in]
  connect_bd_net -net rst_Clk_100M_bus_struct_reset [get_bd_pins mb_core_local_memory/SYS_Rst] [get_bd_pins mb_core_rst_Clk_100M/bus_struct_reset]
  connect_bd_net -net rst_Clk_100M_mb_reset [get_bd_pins mb_core_axi_intc/processor_rst] [get_bd_pins mb_core_rst_Clk_100M/mb_reset] [get_bd_pins microblaze_0/Reset]
  connect_bd_net -net rst_Clk_100M_peripheral_aresetn [get_bd_pins sync100_rstn] [get_bd_pins axi_bram_ctrl_0/s_axi_aresetn] [get_bd_pins mb_core_axi_gpio/s_axi_aresetn] [get_bd_pins mb_core_axi_intc/s_axi_aresetn] [get_bd_pins mb_core_axi_interconnect/ARESETN] [get_bd_pins mb_core_axi_interconnect/M00_ARESETN] [get_bd_pins mb_core_axi_interconnect/M01_ARESETN] [get_bd_pins mb_core_axi_interconnect/M02_ARESETN] [get_bd_pins mb_core_axi_interconnect/M03_ARESETN] [get_bd_pins mb_core_axi_interconnect/M04_ARESETN] [get_bd_pins mb_core_axi_interconnect/M05_ARESETN] [get_bd_pins mb_core_axi_interconnect/M06_ARESETN] [get_bd_pins mb_core_axi_interconnect/S00_ARESETN] [get_bd_pins mb_core_axi_timebase_wdt/s_axi_aresetn] [get_bd_pins mb_core_axi_timer/s_axi_aresetn] [get_bd_pins mb_core_rst_Clk_100M/peripheral_aresetn]
  connect_bd_net -net xlconcat_0_dout [get_bd_pins intr] [get_bd_pins mb_core_axi_intc/intr]
  connect_bd_net -net xlconcat_1_dout [get_bd_pins mb_core_axi_gpio/gpio_io_i] [get_bd_pins mb_core_xlconcat/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: FSO_EDFA_Control
proc create_hier_cell_FSO_EDFA_Control { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2092 -severity "ERROR" "create_hier_cell_FSO_EDFA_Control() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S00_AXI


  # Create pins
  create_bd_pin -dir I -type rst ARESETN
  create_bd_pin -dir O clnt1_edfa_nom_rx
  create_bd_pin -dir I clnt1_edfa_nom_tx
  create_bd_pin -dir O clnt1_edfa_red_rx
  create_bd_pin -dir I clnt1_edfa_red_tx
  create_bd_pin -dir O clnt2_edfa_nom_rx
  create_bd_pin -dir I clnt2_edfa_nom_tx
  create_bd_pin -dir O clnt2_edfa_red_rx
  create_bd_pin -dir I clnt2_edfa_red_tx
  create_bd_pin -dir O -from 6 -to 0 edfa_control_int
  create_bd_pin -dir I -type clk sys_clk_out

  # Create instance: FSO1_EDFA_NOM_UART, and set properties
  set FSO1_EDFA_NOM_UART [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_uart16550:2.0 FSO1_EDFA_NOM_UART ]

  # Create instance: FSO1_EDFA_RED_UART, and set properties
  set FSO1_EDFA_RED_UART [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_uart16550:2.0 FSO1_EDFA_RED_UART ]

  # Create instance: FSO2_EDFA_NOM_UART, and set properties
  set FSO2_EDFA_NOM_UART [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_uart16550:2.0 FSO2_EDFA_NOM_UART ]

  # Create instance: FSO2_EDFA_RED_UART, and set properties
  set FSO2_EDFA_RED_UART [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_uart16550:2.0 FSO2_EDFA_RED_UART ]

  # Create instance: FSO_EDFA_Control_axi_interconnect, and set properties
  set FSO_EDFA_Control_axi_interconnect [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 FSO_EDFA_Control_axi_interconnect ]
  set_property -dict [ list \
   CONFIG.NUM_MI {5} \
 ] $FSO_EDFA_Control_axi_interconnect

  # Create instance: FSO_EDFA_Control_int_xlconcat, and set properties
  set FSO_EDFA_Control_int_xlconcat [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 FSO_EDFA_Control_int_xlconcat ]
  set_property -dict [ list \
   CONFIG.NUM_PORTS {7} \
 ] $FSO_EDFA_Control_int_xlconcat

  # Create instance: FSO_EDFA_Control_xlconstant_w1v0, and set properties
  set FSO_EDFA_Control_xlconstant_w1v0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 FSO_EDFA_Control_xlconstant_w1v0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
 ] $FSO_EDFA_Control_xlconstant_w1v0

  # Create interface connections
  connect_bd_intf_net -intf_net FSO_EDFA_Control_axi_interconnect_M00_AXI [get_bd_intf_pins FSO2_EDFA_RED_UART/S_AXI] [get_bd_intf_pins FSO_EDFA_Control_axi_interconnect/M00_AXI]
  connect_bd_intf_net -intf_net FSO_EDFA_Control_axi_interconnect_M02_AXI [get_bd_intf_pins FSO1_EDFA_NOM_UART/S_AXI] [get_bd_intf_pins FSO_EDFA_Control_axi_interconnect/M02_AXI]
  connect_bd_intf_net -intf_net FSO_EDFA_Control_axi_interconnect_M03_AXI [get_bd_intf_pins FSO1_EDFA_RED_UART/S_AXI] [get_bd_intf_pins FSO_EDFA_Control_axi_interconnect/M03_AXI]
  connect_bd_intf_net -intf_net FSO_EDFA_Control_axi_interconnect_M04_AXI [get_bd_intf_pins FSO2_EDFA_NOM_UART/S_AXI] [get_bd_intf_pins FSO_EDFA_Control_axi_interconnect/M04_AXI]
  connect_bd_intf_net -intf_net processor_axi_periph_M10_AXI [get_bd_intf_pins S00_AXI] [get_bd_intf_pins FSO_EDFA_Control_axi_interconnect/S00_AXI]

  # Create port connections
  connect_bd_net -net CLNT1_EDFA_NOM_UART_ip2intc_irpt [get_bd_pins FSO1_EDFA_NOM_UART/ip2intc_irpt] [get_bd_pins FSO_EDFA_Control_int_xlconcat/In2]
  connect_bd_net -net CLNT1_EDFA_RED_UART_ip2intc_irpt [get_bd_pins FSO1_EDFA_RED_UART/ip2intc_irpt] [get_bd_pins FSO_EDFA_Control_int_xlconcat/In3]
  connect_bd_net -net CLNT2_EDFA_NOM_UART_ip2intc_irpt [get_bd_pins FSO2_EDFA_NOM_UART/ip2intc_irpt] [get_bd_pins FSO_EDFA_Control_int_xlconcat/In4]
  connect_bd_net -net CLNT2_EDFA_RED_UART_ip2intc_irpt [get_bd_pins FSO2_EDFA_RED_UART/ip2intc_irpt] [get_bd_pins FSO_EDFA_Control_int_xlconcat/In5]
  connect_bd_net -net EDFA_FSO1_NOM_UART_sout [get_bd_pins clnt1_edfa_nom_rx] [get_bd_pins FSO1_EDFA_NOM_UART/sout]
  connect_bd_net -net EDFA_FSO1_RED_UART_sout [get_bd_pins clnt1_edfa_red_rx] [get_bd_pins FSO1_EDFA_RED_UART/sout]
  connect_bd_net -net EDFA_FSO2_NOM_UART_sout [get_bd_pins clnt2_edfa_nom_rx] [get_bd_pins FSO2_EDFA_NOM_UART/sout]
  connect_bd_net -net EDFA_FSO2_RED_UART_sout [get_bd_pins clnt2_edfa_red_rx] [get_bd_pins FSO2_EDFA_RED_UART/sout]
  connect_bd_net -net FSO_Serial_IFs_int_xlconcat_dout [get_bd_pins edfa_control_int] [get_bd_pins FSO_EDFA_Control_int_xlconcat/dout]
  connect_bd_net -net processor_Clk [get_bd_pins sys_clk_out] [get_bd_pins FSO1_EDFA_NOM_UART/s_axi_aclk] [get_bd_pins FSO1_EDFA_RED_UART/s_axi_aclk] [get_bd_pins FSO2_EDFA_NOM_UART/s_axi_aclk] [get_bd_pins FSO2_EDFA_RED_UART/s_axi_aclk] [get_bd_pins FSO_EDFA_Control_axi_interconnect/ACLK] [get_bd_pins FSO_EDFA_Control_axi_interconnect/M00_ACLK] [get_bd_pins FSO_EDFA_Control_axi_interconnect/M01_ACLK] [get_bd_pins FSO_EDFA_Control_axi_interconnect/M02_ACLK] [get_bd_pins FSO_EDFA_Control_axi_interconnect/M03_ACLK] [get_bd_pins FSO_EDFA_Control_axi_interconnect/M04_ACLK] [get_bd_pins FSO_EDFA_Control_axi_interconnect/S00_ACLK]
  connect_bd_net -net rst_Clk_100M_peripheral_aresetn [get_bd_pins ARESETN] [get_bd_pins FSO1_EDFA_NOM_UART/s_axi_aresetn] [get_bd_pins FSO1_EDFA_RED_UART/s_axi_aresetn] [get_bd_pins FSO2_EDFA_NOM_UART/s_axi_aresetn] [get_bd_pins FSO2_EDFA_RED_UART/s_axi_aresetn] [get_bd_pins FSO_EDFA_Control_axi_interconnect/ARESETN] [get_bd_pins FSO_EDFA_Control_axi_interconnect/M00_ARESETN] [get_bd_pins FSO_EDFA_Control_axi_interconnect/M01_ARESETN] [get_bd_pins FSO_EDFA_Control_axi_interconnect/M02_ARESETN] [get_bd_pins FSO_EDFA_Control_axi_interconnect/M03_ARESETN] [get_bd_pins FSO_EDFA_Control_axi_interconnect/M04_ARESETN] [get_bd_pins FSO_EDFA_Control_axi_interconnect/S00_ARESETN]
  connect_bd_net -net sin_0_1 [get_bd_pins clnt1_edfa_red_tx] [get_bd_pins FSO1_EDFA_RED_UART/sin]
  connect_bd_net -net sin_1_1 [get_bd_pins clnt2_edfa_red_tx] [get_bd_pins FSO2_EDFA_RED_UART/sin]
  connect_bd_net -net sin_2_1 [get_bd_pins clnt2_edfa_nom_tx] [get_bd_pins FSO2_EDFA_NOM_UART/sin]
  connect_bd_net -net sin_3_1 [get_bd_pins clnt1_edfa_nom_tx] [get_bd_pins FSO1_EDFA_NOM_UART/sin]
  connect_bd_net -net zero_1bit_dout [get_bd_pins FSO1_EDFA_NOM_UART/freeze] [get_bd_pins FSO1_EDFA_RED_UART/freeze] [get_bd_pins FSO2_EDFA_NOM_UART/freeze] [get_bd_pins FSO2_EDFA_RED_UART/freeze] [get_bd_pins FSO_EDFA_Control_xlconstant_w1v0/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: Backplane
proc create_hier_cell_Backplane { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2092 -severity "ERROR" "create_hier_cell_Backplane() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S00_AXI_0


  # Create pins
  create_bd_pin -dir O -from 3 -to 0 bp_int
  create_bd_pin -dir I let1g_uart1_lvttl_rx
  create_bd_pin -dir O let1g_uart1_lvttl_tx
  create_bd_pin -dir I let1g_uart2_lvttl_rx
  create_bd_pin -dir O let1g_uart2_lvttl_tx
  create_bd_pin -dir I -type rst s_axi_aresetn
  create_bd_pin -dir I -type clk sys_clk_out
  create_bd_pin -dir O tcu_sem_rx
  create_bd_pin -dir I tcu_sem_tx

  # Create instance: BP_int_xlconcat, and set properties
  set BP_int_xlconcat [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 BP_int_xlconcat ]
  set_property -dict [ list \
   CONFIG.NUM_PORTS {4} \
 ] $BP_int_xlconcat

  # Create instance: Backplane_axi_interconnect, and set properties
  set Backplane_axi_interconnect [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 Backplane_axi_interconnect ]
  set_property -dict [ list \
   CONFIG.NUM_MI {3} \
 ] $Backplane_axi_interconnect

  # Create instance: LET1G_BP_UART1, and set properties
  set LET1G_BP_UART1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_uart16550:2.0 LET1G_BP_UART1 ]

  # Create instance: LET1G_BP_UART2, and set properties
  set LET1G_BP_UART2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_uart16550:2.0 LET1G_BP_UART2 ]

  # Create instance: TCU_SEM_UART, and set properties
  set TCU_SEM_UART [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_uart16550:2.0 TCU_SEM_UART ]

  # Create instance: xlconstant_w1v0, and set properties
  set xlconstant_w1v0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_w1v0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
 ] $xlconstant_w1v0

  # Create interface connections
  connect_bd_intf_net -intf_net Backplane_axi_interconnect_M00_AXI [get_bd_intf_pins Backplane_axi_interconnect/M00_AXI] [get_bd_intf_pins TCU_SEM_UART/S_AXI]
  connect_bd_intf_net -intf_net Backplane_axi_interconnect_M01_AXI [get_bd_intf_pins Backplane_axi_interconnect/M01_AXI] [get_bd_intf_pins LET1G_BP_UART2/S_AXI]
  connect_bd_intf_net -intf_net Backplane_axi_interconnect_M02_AXI [get_bd_intf_pins Backplane_axi_interconnect/M02_AXI] [get_bd_intf_pins LET1G_BP_UART1/S_AXI]
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins S00_AXI_0] [get_bd_intf_pins Backplane_axi_interconnect/S00_AXI]

  # Create port connections
  connect_bd_net -net LET1G_BP_UART1_ip2intc_irpt [get_bd_pins BP_int_xlconcat/In0] [get_bd_pins LET1G_BP_UART1/ip2intc_irpt]
  connect_bd_net -net LET1G_BP_UART2_ip2intc_irpt [get_bd_pins BP_int_xlconcat/In1] [get_bd_pins LET1G_BP_UART2/ip2intc_irpt]
  connect_bd_net -net axi_uart16550_0_ip2intc_irpt [get_bd_pins BP_int_xlconcat/In2] [get_bd_pins TCU_SEM_UART/ip2intc_irpt]
  connect_bd_net -net axi_uart16550_0_sout [get_bd_pins tcu_sem_rx] [get_bd_pins TCU_SEM_UART/sout]
  connect_bd_net -net axi_uart16550_backplane2_sout [get_bd_pins let1g_uart1_lvttl_tx] [get_bd_pins LET1G_BP_UART1/sout]
  connect_bd_net -net processor_Clk [get_bd_pins sys_clk_out] [get_bd_pins Backplane_axi_interconnect/ACLK] [get_bd_pins Backplane_axi_interconnect/M00_ACLK] [get_bd_pins Backplane_axi_interconnect/M01_ACLK] [get_bd_pins Backplane_axi_interconnect/M02_ACLK] [get_bd_pins Backplane_axi_interconnect/S00_ACLK] [get_bd_pins LET1G_BP_UART1/s_axi_aclk] [get_bd_pins LET1G_BP_UART2/s_axi_aclk] [get_bd_pins TCU_SEM_UART/s_axi_aclk]
  connect_bd_net -net rst_Clk_100M_peripheral_aresetn [get_bd_pins s_axi_aresetn] [get_bd_pins Backplane_axi_interconnect/ARESETN] [get_bd_pins Backplane_axi_interconnect/M00_ARESETN] [get_bd_pins Backplane_axi_interconnect/M01_ARESETN] [get_bd_pins Backplane_axi_interconnect/M02_ARESETN] [get_bd_pins Backplane_axi_interconnect/S00_ARESETN] [get_bd_pins LET1G_BP_UART1/s_axi_aresetn] [get_bd_pins LET1G_BP_UART2/s_axi_aresetn] [get_bd_pins TCU_SEM_UART/s_axi_aresetn]
  connect_bd_net -net sin_0_1 [get_bd_pins let1g_uart1_lvttl_rx] [get_bd_pins LET1G_BP_UART1/sin]
  connect_bd_net -net sin_0_2 [get_bd_pins let1g_uart2_lvttl_rx] [get_bd_pins LET1G_BP_UART2/sin]
  connect_bd_net -net sin_0_3 [get_bd_pins tcu_sem_tx] [get_bd_pins TCU_SEM_UART/sin]
  connect_bd_net -net uart16550_backplane2_sout [get_bd_pins let1g_uart2_lvttl_tx] [get_bd_pins LET1G_BP_UART2/sout]
  connect_bd_net -net xlconcat_0_dout [get_bd_pins bp_int] [get_bd_pins BP_int_xlconcat/dout]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins LET1G_BP_UART1/freeze] [get_bd_pins LET1G_BP_UART2/freeze] [get_bd_pins TCU_SEM_UART/freeze] [get_bd_pins xlconstant_w1v0/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}


# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set AXI_LET0_AXI_ETH_REG_IF [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 AXI_LET0_AXI_ETH_REG_IF ]
  set_property -dict [ list \
   CONFIG.ADDR_WIDTH {32} \
   CONFIG.DATA_WIDTH {32} \
   CONFIG.HAS_BURST {0} \
   CONFIG.HAS_CACHE {0} \
   CONFIG.HAS_LOCK {0} \
   CONFIG.HAS_QOS {0} \
   CONFIG.HAS_REGION {0} \
   CONFIG.NUM_READ_OUTSTANDING {2} \
   CONFIG.NUM_WRITE_OUTSTANDING {2} \
   CONFIG.PROTOCOL {AXI4LITE} \
   ] $AXI_LET0_AXI_ETH_REG_IF

  set AXI_LET0_AXI_LET [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 AXI_LET0_AXI_LET ]
  set_property -dict [ list \
   CONFIG.ADDR_WIDTH {32} \
   CONFIG.DATA_WIDTH {32} \
   CONFIG.FREQ_HZ {333500000} \
   CONFIG.HAS_BURST {0} \
   CONFIG.HAS_CACHE {0} \
   CONFIG.HAS_LOCK {0} \
   CONFIG.HAS_QOS {0} \
   CONFIG.HAS_REGION {0} \
   CONFIG.NUM_READ_OUTSTANDING {2} \
   CONFIG.NUM_WRITE_OUTSTANDING {2} \
   CONFIG.PROTOCOL {AXI4LITE} \
   ] $AXI_LET0_AXI_LET

  set AXI_LET1_AXI_ETH_REG_IF [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 AXI_LET1_AXI_ETH_REG_IF ]
  set_property -dict [ list \
   CONFIG.ADDR_WIDTH {32} \
   CONFIG.DATA_WIDTH {32} \
   CONFIG.HAS_BURST {0} \
   CONFIG.HAS_CACHE {0} \
   CONFIG.HAS_LOCK {0} \
   CONFIG.HAS_QOS {0} \
   CONFIG.HAS_REGION {0} \
   CONFIG.NUM_READ_OUTSTANDING {2} \
   CONFIG.NUM_WRITE_OUTSTANDING {2} \
   CONFIG.PROTOCOL {AXI4LITE} \
   ] $AXI_LET1_AXI_ETH_REG_IF

  set AXI_LET1_AXI_LET [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 AXI_LET1_AXI_LET ]
  set_property -dict [ list \
   CONFIG.ADDR_WIDTH {32} \
   CONFIG.DATA_WIDTH {32} \
   CONFIG.FREQ_HZ {333500000} \
   CONFIG.HAS_BURST {0} \
   CONFIG.HAS_CACHE {0} \
   CONFIG.HAS_LOCK {0} \
   CONFIG.HAS_QOS {0} \
   CONFIG.HAS_REGION {0} \
   CONFIG.NUM_READ_OUTSTANDING {2} \
   CONFIG.NUM_WRITE_OUTSTANDING {2} \
   CONFIG.PROTOCOL {AXI4LITE} \
   ] $AXI_LET1_AXI_LET

  set Vp_Vn [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:diff_analog_io_rtl:1.0 Vp_Vn ]


  # Create ports
  set clk_let [ create_bd_port -dir I -type clk -freq_hz 333500000 clk_let ]
  set_property -dict [ list \
   CONFIG.ASSOCIATED_BUSIF {AXI_LET0_AXI_LET:AXI_LET1_AXI_LET} \
   CONFIG.CLK_DOMAIN {system_LET_clk_wiz_0_clk_out1} \
 ] $clk_let
  set clk_sys [ create_bd_port -dir I -type clk -freq_hz 100000000 clk_sys ]
  set_property -dict [ list \
   CONFIG.ASSOCIATED_BUSIF {AXI_LET0_AXI_ETH_REG_IF:AXI_LET1_AXI_ETH_REG_IF} \
   CONFIG.CLK_DOMAIN {system_LET_clk_wiz_0_clk_out1} \
 ] $clk_sys
  set clnt1_edfa_nom_rx [ create_bd_port -dir O clnt1_edfa_nom_rx ]
  set clnt1_edfa_nom_tx [ create_bd_port -dir I clnt1_edfa_nom_tx ]
  set clnt1_edfa_red_rx [ create_bd_port -dir O clnt1_edfa_red_rx ]
  set clnt1_edfa_red_tx [ create_bd_port -dir I clnt1_edfa_red_tx ]
  set clnt2_edfa_nom_rx [ create_bd_port -dir O clnt2_edfa_nom_rx ]
  set clnt2_edfa_nom_tx [ create_bd_port -dir I clnt2_edfa_nom_tx ]
  set clnt2_edfa_red_rx [ create_bd_port -dir O clnt2_edfa_red_rx ]
  set clnt2_edfa_red_tx [ create_bd_port -dir I clnt2_edfa_red_tx ]
  set dcm_locked [ create_bd_port -dir I dcm_locked ]
  set eeprom_spi_cs_n [ create_bd_port -dir O -from 1 -to 0 eeprom_spi_cs_n ]
  set eeprom_spi_miso [ create_bd_port -dir I eeprom_spi_miso ]
  set eeprom_spi_mosi [ create_bd_port -dir O eeprom_spi_mosi ]
  set eeprom_spi_sck [ create_bd_port -dir O eeprom_spi_sck ]
  set ext_reset [ create_bd_port -dir I -type rst ext_reset ]
  set_property -dict [ list \
   CONFIG.POLARITY {ACTIVE_HIGH} \
 ] $ext_reset
  set let1g_pps_rx [ create_bd_port -dir I -type intr let1g_pps_rx ]
  set_property -dict [ list \
   CONFIG.SENSITIVITY {EDGE_RISING} \
 ] $let1g_pps_rx
  set let1g_uart1_lvttl_rx [ create_bd_port -dir I let1g_uart1_lvttl_rx ]
  set let1g_uart1_lvttl_txd [ create_bd_port -dir O let1g_uart1_lvttl_txd ]
  set let1g_uart2_lvttl_rx [ create_bd_port -dir I let1g_uart2_lvttl_rx ]
  set let1g_uart2_lvttl_txd [ create_bd_port -dir O let1g_uart2_lvttl_txd ]
  set sem_flash_cs_b [ create_bd_port -dir O -from 0 -to 0 sem_flash_cs_b ]
  set sem_flash_miso [ create_bd_port -dir I sem_flash_miso ]
  set sem_flash_mosi [ create_bd_port -dir O sem_flash_mosi ]
  set sem_flash_sck [ create_bd_port -dir O sem_flash_sck ]
  set serv_uart_rx1 [ create_bd_port -dir I serv_uart_rx1 ]
  set serv_uart_rx2 [ create_bd_port -dir I serv_uart_rx2 ]
  set serv_uart_tx1 [ create_bd_port -dir O serv_uart_tx1 ]
  set serv_uart_tx2 [ create_bd_port -dir O serv_uart_tx2 ]
  set sync100_rstn [ create_bd_port -dir O -from 0 -to 0 -type rst sync100_rstn ]
  set tcu_sem_error [ create_bd_port -dir I -type intr tcu_sem_error ]
  set_property -dict [ list \
   CONFIG.PortWidth {1} \
 ] $tcu_sem_error
  set tcu_sem_rx [ create_bd_port -dir O tcu_sem_rx ]
  set tcu_sem_tx [ create_bd_port -dir I tcu_sem_tx ]

  # Create instance: Backplane
  create_hier_cell_Backplane [current_bd_instance .] Backplane

  # Create instance: EEPROM_SPI, and set properties
  set EEPROM_SPI [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_quad_spi:3.2 EEPROM_SPI ]
  set_property -dict [ list \
   CONFIG.C_NUM_SS_BITS {2} \
   CONFIG.C_SCK_RATIO {16} \
   CONFIG.Multiples16 {2} \
 ] $EEPROM_SPI

  # Create instance: FSO_EDFA_Control
  create_hier_cell_FSO_EDFA_Control [current_bd_instance .] FSO_EDFA_Control

  # Create instance: LET1G_MB_Core
  create_hier_cell_LET1G_MB_Core [current_bd_instance .] LET1G_MB_Core

  # Create instance: LET_AXI_Interconnects
  create_hier_cell_LET_AXI_Interconnects [current_bd_instance .] LET_AXI_Interconnects

  # Create instance: SEM_Flash_SPI, and set properties
  set SEM_Flash_SPI [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_quad_spi:3.2 SEM_Flash_SPI ]

  # Create instance: Service_Interface
  create_hier_cell_Service_Interface [current_bd_instance .] Service_Interface

  # Create instance: clk_wiz, and set properties
  set clk_wiz [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:6.0 clk_wiz ]
  set_property -dict [ list \
   CONFIG.AUTO_PRIMITIVE {PLL} \
   CONFIG.CLKOUT1_DRIVES {BUFGCE} \
   CONFIG.CLKOUT1_JITTER {191.696} \
   CONFIG.CLKOUT1_PHASE_ERROR {114.212} \
   CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {25} \
   CONFIG.CLKOUT1_USED {true} \
   CONFIG.CLKOUT2_DRIVES {BUFGCE} \
   CONFIG.CLKOUT2_JITTER {102.086} \
   CONFIG.CLKOUT2_PHASE_ERROR {87.180} \
   CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {100.000} \
   CONFIG.CLKOUT2_USED {false} \
   CONFIG.CLKOUT3_DRIVES {BUFGCE} \
   CONFIG.CLKOUT3_JITTER {154.057} \
   CONFIG.CLKOUT3_PHASE_ERROR {87.180} \
   CONFIG.CLKOUT3_REQUESTED_OUT_FREQ {100.000} \
   CONFIG.CLKOUT3_USED {false} \
   CONFIG.CLKOUT4_DRIVES {BUFGCE} \
   CONFIG.CLKOUT4_JITTER {104.542} \
   CONFIG.CLKOUT4_PHASE_ERROR {98.575} \
   CONFIG.CLKOUT4_REQUESTED_OUT_FREQ {100.000} \
   CONFIG.CLKOUT4_USED {false} \
   CONFIG.CLKOUT5_DRIVES {BUFGCE} \
   CONFIG.CLKOUT6_DRIVES {BUFGCE} \
   CONFIG.CLKOUT7_DRIVES {BUFGCE} \
   CONFIG.FEEDBACK_SOURCE {FDBK_AUTO} \
   CONFIG.MMCM_BANDWIDTH {OPTIMIZED} \
   CONFIG.MMCM_CLKFBOUT_MULT_F {8} \
   CONFIG.MMCM_CLKIN1_PERIOD {10.000} \
   CONFIG.MMCM_CLKIN2_PERIOD {10.000} \
   CONFIG.MMCM_CLKOUT0_DIVIDE_F {32} \
   CONFIG.MMCM_CLKOUT1_DIVIDE {1} \
   CONFIG.MMCM_CLKOUT2_DIVIDE {1} \
   CONFIG.MMCM_CLKOUT3_DIVIDE {1} \
   CONFIG.MMCM_COMPENSATION {AUTO} \
   CONFIG.MMCM_DIVCLK_DIVIDE {1} \
   CONFIG.NUM_OUT_CLKS {1} \
   CONFIG.OPTIMIZE_CLOCKING_STRUCTURE_EN {true} \
   CONFIG.PRIMITIVE {Auto} \
   CONFIG.PRIM_SOURCE {No_buffer} \
   CONFIG.USE_LOCKED {false} \
   CONFIG.USE_RESET {true} \
   CONFIG.USE_SAFE_CLOCK_STARTUP {true} \
 ] $clk_wiz

  # Create instance: processor_axi_periph_let, and set properties
  set processor_axi_periph_let [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 processor_axi_periph_let ]
  set_property -dict [ list \
   CONFIG.NUM_MI {4} \
 ] $processor_axi_periph_let

  # Create instance: processor_axi_periph_sys, and set properties
  set processor_axi_periph_sys [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 processor_axi_periph_sys ]
  set_property -dict [ list \
   CONFIG.NUM_MI {6} \
 ] $processor_axi_periph_sys

  # Create instance: rst_clk_let, and set properties
  set rst_clk_let [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 rst_clk_let ]

  # Create instance: xlconcat_int, and set properties
  set xlconcat_int [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_int ]
  set_property -dict [ list \
   CONFIG.NUM_PORTS {13} \
 ] $xlconcat_int

  # Create interface connections
  connect_bd_intf_net -intf_net LET1G_MB_M04_AXI_SYS [get_bd_intf_pins LET1G_MB_Core/M04_AXI_SYS] [get_bd_intf_pins processor_axi_periph_sys/S00_AXI]
  connect_bd_intf_net -intf_net LET1G_MB_M05_AXI_LET [get_bd_intf_pins LET1G_MB_Core/M05_AXI_LET] [get_bd_intf_pins processor_axi_periph_let/S00_AXI]
  connect_bd_intf_net -intf_net LET_AXI_Interconnects_AXI_LET1_AXI_ETH_REG_IF [get_bd_intf_ports AXI_LET0_AXI_ETH_REG_IF] [get_bd_intf_pins LET_AXI_Interconnects/AXI_LET0_AXI_ETH_REG_IF]
  connect_bd_intf_net -intf_net LET_AXI_Interconnects_AXI_LET2_AXI_ETH_REG_IF [get_bd_intf_ports AXI_LET1_AXI_ETH_REG_IF] [get_bd_intf_pins LET_AXI_Interconnects/AXI_LET1_AXI_ETH_REG_IF]
  connect_bd_intf_net -intf_net LET_SW_Control_AXI_LET1_AXI_LET [get_bd_intf_ports AXI_LET0_AXI_LET] [get_bd_intf_pins LET_AXI_Interconnects/AXI_LET0_AXI_LET]
  connect_bd_intf_net -intf_net LET_SW_Control_AXI_LET2_AXI_LET [get_bd_intf_ports AXI_LET1_AXI_LET] [get_bd_intf_pins LET_AXI_Interconnects/AXI_LET1_AXI_LET]
  connect_bd_intf_net -intf_net S00_AXI_0_1 [get_bd_intf_pins Backplane/S00_AXI_0] [get_bd_intf_pins processor_axi_periph_let/M03_AXI]
  connect_bd_intf_net -intf_net S00_AXI_1 [get_bd_intf_pins FSO_EDFA_Control/S00_AXI] [get_bd_intf_pins processor_axi_periph_let/M02_AXI]
  connect_bd_intf_net -intf_net processor_axi_periph1_M00_AXI [get_bd_intf_pins LET_AXI_Interconnects/S_AXI_LET2] [get_bd_intf_pins processor_axi_periph_let/M00_AXI]
  connect_bd_intf_net -intf_net processor_axi_periph1_M01_AXI [get_bd_intf_pins LET_AXI_Interconnects/S_AXI_LET1] [get_bd_intf_pins processor_axi_periph_let/M01_AXI]
  connect_bd_intf_net -intf_net processor_axi_periph_sys_M00_AXI [get_bd_intf_pins Service_Interface/S_AXI_0] [get_bd_intf_pins processor_axi_periph_sys/M00_AXI]
  connect_bd_intf_net -intf_net processor_axi_periph_sys_M01_AXI [get_bd_intf_pins Service_Interface/S_AXI_1] [get_bd_intf_pins processor_axi_periph_sys/M01_AXI]
  connect_bd_intf_net -intf_net processor_axi_periph_sys_M04_AXI [get_bd_intf_pins EEPROM_SPI/AXI_LITE] [get_bd_intf_pins processor_axi_periph_sys/M04_AXI]
  connect_bd_intf_net -intf_net processor_axi_periph_sys_M05_AXI [get_bd_intf_pins SEM_Flash_SPI/AXI_LITE] [get_bd_intf_pins processor_axi_periph_sys/M05_AXI]

  # Create port connections
  connect_bd_net -net Backplane_bp_int [get_bd_pins Backplane/bp_int] [get_bd_pins xlconcat_int/In7]
  connect_bd_net -net Backplane_let1g_bp_uart1_txd [get_bd_ports let1g_uart1_lvttl_txd] [get_bd_pins Backplane/let1g_uart1_lvttl_tx]
  connect_bd_net -net Backplane_sout_0 [get_bd_ports let1g_uart2_lvttl_txd] [get_bd_pins Backplane/let1g_uart2_lvttl_tx]
  connect_bd_net -net Backplane_sout_1 [get_bd_ports tcu_sem_rx] [get_bd_pins Backplane/tcu_sem_rx]
  connect_bd_net -net EEPROM_SPI_ip2intc_irpt [get_bd_pins EEPROM_SPI/ip2intc_irpt] [get_bd_pins xlconcat_int/In4]
  connect_bd_net -net EEPROM_spi_io0_o [get_bd_ports eeprom_spi_mosi] [get_bd_pins EEPROM_SPI/io0_o]
  connect_bd_net -net EEPROM_spi_sck_o [get_bd_ports eeprom_spi_sck] [get_bd_pins EEPROM_SPI/sck_o]
  connect_bd_net -net EEPROM_spi_ss_o [get_bd_ports eeprom_spi_cs_n] [get_bd_pins EEPROM_SPI/ss_o]
  connect_bd_net -net FSO_EDFA_Control_clnt1_edfa_nom_rx [get_bd_ports clnt1_edfa_nom_rx] [get_bd_pins FSO_EDFA_Control/clnt1_edfa_nom_rx]
  connect_bd_net -net FSO_EDFA_Control_clnt1_edfa_red_rx [get_bd_ports clnt1_edfa_red_rx] [get_bd_pins FSO_EDFA_Control/clnt1_edfa_red_rx]
  connect_bd_net -net FSO_EDFA_Control_clnt2_edfa_nom_rx [get_bd_ports clnt2_edfa_nom_rx] [get_bd_pins FSO_EDFA_Control/clnt2_edfa_nom_rx]
  connect_bd_net -net FSO_EDFA_Control_clnt2_edfa_red_rx [get_bd_ports clnt2_edfa_red_rx] [get_bd_pins FSO_EDFA_Control/clnt2_edfa_red_rx]
  connect_bd_net -net FSO_EDFA_Control_edfa_control_int [get_bd_pins FSO_EDFA_Control/edfa_control_int] [get_bd_pins xlconcat_int/In12]
  connect_bd_net -net LET1G_MB_mb_debug_sys_rst [get_bd_pins LET1G_MB_Core/mb_debug_sys_rst] [get_bd_pins rst_clk_let/mb_debug_sys_rst]
  connect_bd_net -net LET1G_MB_timer_int [get_bd_pins LET1G_MB_Core/timer_int] [get_bd_pins xlconcat_int/In0]
  connect_bd_net -net LET1G_MB_wdt_interrupt [get_bd_pins LET1G_MB_Core/wdt_interrupt] [get_bd_pins xlconcat_int/In1]
  connect_bd_net -net LET1G_MB_wdt_reset [get_bd_pins LET1G_MB_Core/wdt_reset] [get_bd_pins rst_clk_let/aux_reset_in]
  connect_bd_net -net LET1G_MB_wdt_reset_pending [get_bd_pins LET1G_MB_Core/wdt_reset_pending] [get_bd_pins xlconcat_int/In2]
  connect_bd_net -net Service_Interface_serv_int [get_bd_pins Service_Interface/serv_int] [get_bd_pins xlconcat_int/In6]
  connect_bd_net -net clk_wiz_0_clk_out2 [get_bd_ports clk_let] [get_bd_pins LET_AXI_Interconnects/LET1_clk] [get_bd_pins LET_AXI_Interconnects/LET2_clk] [get_bd_pins rst_clk_let/slowest_sync_clk]
  connect_bd_net -net clk_wiz_0_clk_out3 [get_bd_pins EEPROM_SPI/ext_spi_clk] [get_bd_pins SEM_Flash_SPI/ext_spi_clk] [get_bd_pins clk_wiz/clk_out1]
  connect_bd_net -net clk_wiz_0_locked [get_bd_ports dcm_locked] [get_bd_pins LET1G_MB_Core/dcm_locked] [get_bd_pins rst_clk_let/dcm_locked]
  connect_bd_net -net clnt1_edfa_nom_tx_1 [get_bd_ports clnt1_edfa_nom_tx] [get_bd_pins FSO_EDFA_Control/clnt1_edfa_nom_tx]
  connect_bd_net -net clnt1_edfa_red_tx_1 [get_bd_ports clnt1_edfa_red_tx] [get_bd_pins FSO_EDFA_Control/clnt1_edfa_red_tx]
  connect_bd_net -net clnt2_edfa_nom_tx_1 [get_bd_ports clnt2_edfa_nom_tx] [get_bd_pins FSO_EDFA_Control/clnt2_edfa_nom_tx]
  connect_bd_net -net clnt2_edfa_red_tx_1 [get_bd_ports clnt2_edfa_red_tx] [get_bd_pins FSO_EDFA_Control/clnt2_edfa_red_tx]
  connect_bd_net -net io1_i_0_1 [get_bd_ports eeprom_spi_miso] [get_bd_pins EEPROM_SPI/io1_i]
  connect_bd_net -net io1_i_0_2 [get_bd_ports sem_flash_miso] [get_bd_pins SEM_Flash_SPI/io1_i]
  connect_bd_net -net pps_rx_1 [get_bd_ports let1g_pps_rx] [get_bd_pins xlconcat_int/In8]
  connect_bd_net -net processor_Clk [get_bd_ports clk_sys] [get_bd_pins Backplane/sys_clk_out] [get_bd_pins EEPROM_SPI/s_axi_aclk] [get_bd_pins FSO_EDFA_Control/sys_clk_out] [get_bd_pins LET1G_MB_Core/sys_clk_out] [get_bd_pins LET_AXI_Interconnects/sys_clk] [get_bd_pins SEM_Flash_SPI/s_axi_aclk] [get_bd_pins Service_Interface/sys_clk] [get_bd_pins clk_wiz/clk_in1] [get_bd_pins processor_axi_periph_let/ACLK] [get_bd_pins processor_axi_periph_let/M00_ACLK] [get_bd_pins processor_axi_periph_let/M01_ACLK] [get_bd_pins processor_axi_periph_let/M02_ACLK] [get_bd_pins processor_axi_periph_let/M03_ACLK] [get_bd_pins processor_axi_periph_let/S00_ACLK] [get_bd_pins processor_axi_periph_sys/ACLK] [get_bd_pins processor_axi_periph_sys/M00_ACLK] [get_bd_pins processor_axi_periph_sys/M01_ACLK] [get_bd_pins processor_axi_periph_sys/M02_ACLK] [get_bd_pins processor_axi_periph_sys/M03_ACLK] [get_bd_pins processor_axi_periph_sys/M04_ACLK] [get_bd_pins processor_axi_periph_sys/M05_ACLK] [get_bd_pins processor_axi_periph_sys/S00_ACLK]
  connect_bd_net -net reset_1 [get_bd_ports ext_reset] [get_bd_pins LET1G_MB_Core/reset] [get_bd_pins clk_wiz/reset] [get_bd_pins rst_clk_let/ext_reset_in]
  connect_bd_net -net rst_Clk_100M_peripheral_aresetn [get_bd_ports sync100_rstn] [get_bd_pins Backplane/s_axi_aresetn] [get_bd_pins EEPROM_SPI/s_axi_aresetn] [get_bd_pins FSO_EDFA_Control/ARESETN] [get_bd_pins LET1G_MB_Core/sync100_rstn] [get_bd_pins LET_AXI_Interconnects/sys_resetn] [get_bd_pins SEM_Flash_SPI/s_axi_aresetn] [get_bd_pins Service_Interface/s_axi_aresetn] [get_bd_pins processor_axi_periph_let/ARESETN] [get_bd_pins processor_axi_periph_let/M00_ARESETN] [get_bd_pins processor_axi_periph_let/M01_ARESETN] [get_bd_pins processor_axi_periph_let/M02_ARESETN] [get_bd_pins processor_axi_periph_let/M03_ARESETN] [get_bd_pins processor_axi_periph_let/S00_ARESETN] [get_bd_pins processor_axi_periph_sys/ARESETN] [get_bd_pins processor_axi_periph_sys/M00_ARESETN] [get_bd_pins processor_axi_periph_sys/M01_ARESETN] [get_bd_pins processor_axi_periph_sys/M02_ARESETN] [get_bd_pins processor_axi_periph_sys/M03_ARESETN] [get_bd_pins processor_axi_periph_sys/M04_ARESETN] [get_bd_pins processor_axi_periph_sys/M05_ARESETN] [get_bd_pins processor_axi_periph_sys/S00_ARESETN]
  connect_bd_net -net rst_Clk_200M_peripheral_aresetn [get_bd_pins LET_AXI_Interconnects/LET1_rst_n] [get_bd_pins LET_AXI_Interconnects/LET2_rst_n] [get_bd_pins rst_clk_let/peripheral_aresetn]
  connect_bd_net -net sem_flash_spi_io0_o [get_bd_ports sem_flash_mosi] [get_bd_pins SEM_Flash_SPI/io0_o]
  connect_bd_net -net sem_flash_spi_ip2intc_irpt [get_bd_pins SEM_Flash_SPI/ip2intc_irpt] [get_bd_pins xlconcat_int/In5]
  connect_bd_net -net sem_flash_spi_sck_o [get_bd_ports sem_flash_sck] [get_bd_pins SEM_Flash_SPI/sck_o]
  connect_bd_net -net sem_flash_spi_ss_o [get_bd_ports sem_flash_cs_b] [get_bd_pins SEM_Flash_SPI/ss_o]
  connect_bd_net -net service_conn_sout_0 [get_bd_ports serv_uart_tx1] [get_bd_pins Service_Interface/serv_uart_tx1]
  connect_bd_net -net service_conn_sout_1 [get_bd_ports serv_uart_tx2] [get_bd_pins Service_Interface/serv_uart_tx2]
  connect_bd_net -net sin_0_1 [get_bd_ports let1g_uart1_lvttl_rx] [get_bd_pins Backplane/let1g_uart1_lvttl_rx]
  connect_bd_net -net sin_0_2 [get_bd_ports let1g_uart2_lvttl_rx] [get_bd_pins Backplane/let1g_uart2_lvttl_rx]
  connect_bd_net -net sin_0_3 [get_bd_ports serv_uart_rx1] [get_bd_pins Service_Interface/serv_uart_rx1]
  connect_bd_net -net sin_0_4 [get_bd_ports tcu_sem_tx] [get_bd_pins Backplane/tcu_sem_tx]
  connect_bd_net -net sin_1_1 [get_bd_ports serv_uart_rx2] [get_bd_pins Service_Interface/serv_uart_rx2]
  connect_bd_net -net tcu_sem_error_1 [get_bd_ports tcu_sem_error] [get_bd_pins xlconcat_int/In9]
  connect_bd_net -net xlconcat_0_dout [get_bd_pins LET1G_MB_Core/intr] [get_bd_pins xlconcat_int/dout]

  # Create address segments
  assign_bd_address -offset 0x44A00000 -range 0x00010000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs AXI_LET0_AXI_ETH_REG_IF/Reg] -force
  assign_bd_address -offset 0x44A10000 -range 0x00010000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs AXI_LET0_AXI_LET/Reg] -force
  assign_bd_address -offset 0x44A20000 -range 0x00010000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs AXI_LET1_AXI_ETH_REG_IF/Reg] -force
  assign_bd_address -offset 0x44A30000 -range 0x00010000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs AXI_LET1_AXI_LET/Reg] -force
  assign_bd_address -offset 0x44AA0000 -range 0x00010000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs EEPROM_SPI/AXI_LITE/Reg] -force
  assign_bd_address -offset 0x44A80000 -range 0x00010000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs FSO_EDFA_Control/FSO1_EDFA_NOM_UART/S_AXI/Reg] -force
  assign_bd_address -offset 0x44A90000 -range 0x00010000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs FSO_EDFA_Control/FSO1_EDFA_RED_UART/S_AXI/Reg] -force
  assign_bd_address -offset 0x44AB0000 -range 0x00010000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs FSO_EDFA_Control/FSO2_EDFA_NOM_UART/S_AXI/Reg] -force
  assign_bd_address -offset 0x44AC0000 -range 0x00010000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs FSO_EDFA_Control/FSO2_EDFA_RED_UART/S_AXI/Reg] -force
  assign_bd_address -offset 0x44A40000 -range 0x00010000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs Backplane/LET1G_BP_UART1/S_AXI/Reg] -force
  assign_bd_address -offset 0x44A50000 -range 0x00010000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs Backplane/LET1G_BP_UART2/S_AXI/Reg] -force
  assign_bd_address -offset 0x44B30000 -range 0x00010000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs SEM_Flash_SPI/AXI_LITE/Reg] -force
  assign_bd_address -offset 0x44B10000 -range 0x00010000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs Service_Interface/Service_UART1/S_AXI/Reg] -force
  assign_bd_address -offset 0x44B20000 -range 0x00010000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs Service_Interface/Service_UART2/S_AXI/Reg] -force
  assign_bd_address -offset 0x44A60000 -range 0x00010000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs Backplane/TCU_SEM_UART/S_AXI/Reg] -force
  assign_bd_address -offset 0xC0000000 -range 0x00002000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs LET1G_MB_Core/axi_bram_ctrl_0/S_AXI/Mem0] -force
  assign_bd_address -offset 0x00000000 -range 0x00080000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs LET1G_MB_Core/mb_core_local_memory/dlmb_bram_if_cntlr/SLMB/Mem] -force
  assign_bd_address -offset 0x40000000 -range 0x00010000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs LET1G_MB_Core/mb_core_axi_gpio/S_AXI/Reg] -force
  assign_bd_address -offset 0x41200000 -range 0x00010000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs LET1G_MB_Core/mb_core_axi_intc/S_AXI/Reg] -force
  assign_bd_address -offset 0x41A00000 -range 0x00010000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs LET1G_MB_Core/mb_core_axi_timebase_wdt/S_AXI/Reg] -force
  assign_bd_address -offset 0x41C00000 -range 0x00010000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Data] [get_bd_addr_segs LET1G_MB_Core/mb_core_axi_timer/S_AXI/Reg] -force
  assign_bd_address -offset 0x00000000 -range 0x00080000 -target_address_space [get_bd_addr_spaces LET1G_MB_Core/microblaze_0/Instruction] [get_bd_addr_segs LET1G_MB_Core/mb_core_local_memory/ilmb_bram_if_cntlr/SLMB/Mem] -force


  # Restore current instance
  current_bd_instance $oldCurInst

  validate_bd_design
  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


