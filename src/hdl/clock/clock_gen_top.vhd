-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
--  Project    : Vivado script 
--  Author     : Gustavo Martin
--  Description: Wrapper for Clock generation IP             
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Xilinx components
library unisim;
use unisim.vcomponents.all;

entity clock_gen_top is
    port(
        clk_in0_p               : in  std_logic; -- 300 MHz
        clk_in0_n               : in  std_logic;
        clk_in1_p               : in  std_logic; -- 300 MHz
        clk_in1_n               : in  std_logic;
        rst_ext                : in  std_logic;
--        mgtrefclk_p            : in  std_logic; -- 156.25 MHz
--        mgtrefclk_n            : in  std_logic;
--        CLK_125_P              : in  std_logic;
--        CLK_125_N              : in  std_logic;
--        USER_SMA_MGT_CLOCK_C_P : out std_logic;
--        USER_SMA_MGT_CLOCK_C_N : out std_logic;
        Q7_OUT_C_P             : in  std_logic;
        Q7_OUT_C_N             : in  std_logic;        
        Q11_OUT_C_P             : in  std_logic;
        Q11_OUT_C_N             : in  std_logic;
        clk_sys                : out std_logic;
        clk_let                : out std_logic; -- 330 MHz
        clk_ldpc               : out std_logic; -- 600 MHz
        clk_ref_fso             : out std_logic; -- 156.25 MHz
        clk_ref_eth             : out std_logic; -- 156.25 MHz
        clk_gt_free            : out std_logic; -- 25 MHz
        clk_eth_free           : out std_logic; -- 50 MHz
        rst_async              : out std_logic;
        rst_async_n            : out std_logic;
        dcm_locked_0             : out std_logic;
        dcm_locked_1             : out std_logic
    );
end entity clock_gen_top;

architecture RTL of clock_gen_top is
    signal locked0     : std_logic;
    signal locked1     : std_logic;
    signal clk_ext125 : std_logic;
    signal clk_125 : std_logic;
    signal clk_ref125_aux : std_logic;



begin

    dcm_locked_0 <= locked0;
    dcm_locked_1 <= locked1;

    inst_clk_wiz_main : entity work.clk_wiz_main
        port map(
            -- Clock out ports  
            --clk_let      => clk_let,
            clk_ldpc     => clk_ldpc,

            -- Status and control signals                
            reset        => rst_ext,
            -- Clock in ports
            clk_in1_p    => clk_in0_p,
            clk_in1_n    => clk_in0_n,
            locked       => locked0
        );
        
    --TODO: 
    inst_let_clk_gen : entity work.clk_wiz_0
        port map(
            -- Clock out ports  
            clk_gt_free  => clk_gt_free,
            clk_eth_free => clk_eth_free,
            clk_sys      => clk_sys,
            clk_let      => clk_let,
            -- Status and control signals                
            locked    => locked1,
            reset        => rst_ext,
            -- Clock in ports
            clk_in1_p    => clk_in1_p,
            clk_in1_n    => clk_in1_n
        );

--    IBUFDS_GTE4_fso_inst : IBUFDS_GTE4
--        generic map(
--            REFCLK_EN_TX_PATH  => '0',  -- Refer to Transceiver User Guide
--            REFCLK_HROW_CK_SEL => "00", -- Refer to Transceiver User Guide
--            REFCLK_ICNTL_RX    => "00"  -- Refer to Transceiver User Guide
--        )
--        port map(
--            O     => clk_ref_fso,        -- 1-bit output: Refer to Transceiver User Guide
--            ODIV2 => open,              -- 1-bit output: Refer to Transceiver User Guide
--            CEB   => '0',               -- 1-bit input: Refer to Transceiver User Guide
--            I     => Q11_OUT_C_P,       -- 1-bit input: Refer to Transceiver User Guide
--            IB    => Q11_OUT_C_N        -- 1-bit input: Refer to Transceiver User Guide
--        );
        
    IBUFDS_GTE4_eth_inst : IBUFDS_GTE4
        generic map(
            REFCLK_EN_TX_PATH  => '0',  -- Refer to Transceiver User Guide
            REFCLK_HROW_CK_SEL => "00", -- Refer to Transceiver User Guide
            REFCLK_ICNTL_RX    => "00"  -- Refer to Transceiver User Guide
        )
        port map(
            O     => clk_ref_eth,        -- 1-bit output: Refer to Transceiver User Guide
            ODIV2 => open,              --clk_ref125, --open,              -- 1-bit output: Refer to Transceiver User Guide
            CEB   => '0',               -- 1-bit input: Refer to Transceiver User Guide
            I     => Q7_OUT_C_P,        -- 1-bit input: Refer to Transceiver User Guide
            IB    => Q7_OUT_C_N         -- 1-bit input: Refer to Transceiver User Guide
        );
--    clk_ref125 <= clk_ext125;

--        IBUFDS_inst : IBUF
--        generic map (
--            DQS_BIAS => "FALSE" -- (FALSE, TRUE)
--        )
--        port map (
--            O => clk_ext125, -- 1-bit output: Buffer output
--            I => CLK_125_P -- 1-bit input: Diff_p buffer input (connect directly to top-level port)
--        );     

    inst_proc_sys_reset_0 : entity work.proc_sys_reset_0
        PORT MAP(
            slowest_sync_clk      => clk_gt_free,
            ext_reset_in          => rst_ext,
            aux_reset_in          => '0',
            mb_debug_sys_rst      => '0',
            dcm_locked            => locked1,
            mb_reset              => open,
            bus_struct_reset      => open,
            peripheral_reset(0)   => rst_async,
            interconnect_aresetn  => open,
            peripheral_aresetn(0) => rst_async_n
        );

end architecture RTL;
