--!
--! Copyright (C) 2011 - 2012 Creonic GmbH
--!
--! This file is part of the Creonic Viterbi Decoder, which is distributed
--! under the terms of the GNU General Public License version 2.
--!
--! @file
--! @brief  Trellis parameter calculations (e.g., transitions, init values).
--! @author Markus Fehrenz
--! @date   2011/07/27
--!
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library dec_viterbi;
use dec_viterbi.pkg_param.all;
use dec_viterbi.pkg_param_derived.all;
use dec_viterbi.pkg_types.all;


package pkg_trellis is

	constant PREVIOUS_STATES    : t_previous_states := calc_previous_states;
	constant TRANSITIONS        : t_transitions     := calc_transitions;
	constant INITIALIZE_TRELLIS : t_node_s          := calc_initialize;

end package pkg_trellis;


