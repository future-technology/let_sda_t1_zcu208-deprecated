--! Copyright (C) 2020 Creonic GmbH
--!
--! @file
--! @brief  Check the CRC16 and then compute the error flag
--! @author Nhan Nguyen
--! @date   2021/02/01
--!


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_decoding;
use frame_decoding.pkg_support.all;


entity crc16_checker is
	port (
		clk : in  std_logic;
		rst : in  std_logic;

		input_ready : out std_logic;
		input_valid : in  std_logic;
		input_last  : in  std_logic;
		input_data  : in  std_logic_vector(7 downto 0);

		crc_valid   : out std_logic;
		crc_correct : out std_logic;

		output_ready : in  std_logic;
		output_valid : out std_logic;
		output_last  : out std_logic;
		output_data  : out std_logic_vector(7 downto 0)
	);
end entity crc16_checker;

architecture rtl of crc16_checker is

	constant FRAME_SIZE : natural := 20;

	-- CRC-16 parameters
	constant NUM_PARITY_BITS_16 : natural := 16;

	-- the checksum polynomial
	constant CRC16_POLY    : std_logic_vector(NUM_PARITY_BITS_16 - 1 downto 0) := x"1021";
	constant INIT_STATE_16 : std_logic_vector(NUM_PARITY_BITS_16 - 1 downto 0) := X"0000";

	-- The magic number - the residue  1D0F                                        1   D   0   F
	constant RESIDUE       : std_logic_vector(NUM_PARITY_BITS_16 - 1 downto 0) := (others => '0');

	signal shift_reg_16    : std_logic_vector(NUM_PARITY_BITS_16 - 1 downto 0);
	signal crc_result_int  : std_logic_vector(NUM_PARITY_BITS_16 - 1 downto 0);
	signal data_cnt        : unsigned(4 downto 0);
	signal remove_crc      : std_logic;
	signal crc_valid_int   : std_logic;
	signal input_ready_int : std_logic;

begin

	input_ready     <= input_ready_int;
	input_ready_int <= output_ready or remove_crc;

	-- Calculate the CRC-16 for the input data.
	pr_calc_crc16: process(clk) is
		variable v_shift_reg : std_logic_vector(NUM_PARITY_BITS_16 - 1 downto 0);
	begin
	if rising_edge(clk) then
		if rst = '1' then
			shift_reg_16   <= INIT_STATE_16;
			crc_result_int <= (others => '0');
			data_cnt       <= (others => '0');
			remove_crc     <= '0';
			output_data    <= (others => '0');
			output_valid   <= '0';
			output_last    <= '0';
			crc_valid      <= '0';
			crc_correct    <= '0';
			crc_valid_int  <= '0';
		else
			v_shift_reg   := shift_reg_16;
			crc_valid_int <= '0';

			-- Handshake at input
			if input_ready_int = '1' and input_valid = '1' then
				output_data  <= input_data;
				output_valid <= input_valid and (not remove_crc);

				-- When last byte from data, reset signals
				-- if not, count and pass data to output
				if input_last = '1' then
					v_shift_reg   := next_crc(v_shift_reg, CRC16_POLY, input_data(7 downto 6));
					remove_crc    <= '0';
					data_cnt      <= (others => '0');
					shift_reg_16  <= INIT_STATE_16;
					crc_valid_int <= '1';
				else
					v_shift_reg   := next_crc(v_shift_reg, CRC16_POLY, input_data);
					shift_reg_16  <= v_shift_reg;
					data_cnt      <= data_cnt + 1;
					output_last   <= '0';

					-- Last 3 bytes contain the CRC16, do not pass to the output
					if data_cnt = FRAME_SIZE - 3 then
						output_last <= '1';
						remove_crc  <= '1';
						output_data <= input_data(7 downto 6) & "000000";
					end if;
				end if;

				crc_result_int <= v_shift_reg;
			end if;

			crc_valid   <= crc_valid_int;
			crc_correct <= '0';

			if crc_valid_int = '1' then
				if crc_result_int = RESIDUE then
					crc_correct <= '1';
				end if;
			end if;
		end if;
	end if;
	end process pr_calc_crc16;

end rtl;

