--! Copyright (C) Creonic GmbH
--!
--! @file
--! @brief  Check the CRC32 and then compute the error flag
--! @author Nhan Nguyen
--! @date   2021/12/09
--!


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_decoding;
use frame_decoding.pkg_support.all;


entity crc32_checker is
	port (
		clk : in  std_logic;
		rst : in  std_logic;

		input_ready : out std_logic;
		input_valid : in  std_logic;
		input_last  : in  std_logic;
		input_data  : in  std_logic_vector(7 downto 0);

		crc_valid   : out std_logic;
		crc_correct : out std_logic;

		output_ready : in  std_logic;
		output_valid : out std_logic;
		output_last  : out std_logic;
		output_data  : out std_logic_vector(7 downto 0)
	);
end entity crc32_checker;

architecture arch of crc32_checker is

	constant FRAME_SIZE : natural := 1056;

	-- CRC-32 parameters
	constant NUM_PARITY_BITS_32 : natural := 32;

	-- the checksum polynomial   0   4   C   1   1   D   B   7
	-- POLYNOMIAL_32            "00000100110000010001110110110111";
	constant INIT_STATE_32 : std_logic_vector(NUM_PARITY_BITS_32 - 1 downto 0) := X"00000000";

	-- The magic number - the residue                                               C   7   0   4   D   D   7   B
	-- constant RESIDUE       : std_logic_vector(NUM_PARITY_BITS_32 - 1 downto 0) := "11000111000001001101110101111011";
	constant RESIDUE : std_logic_vector(NUM_PARITY_BITS_32 - 1 downto 0) := (others => '0');

	signal shift_reg_32    : std_logic_vector(NUM_PARITY_BITS_32 - 1 downto 0);
	signal crc_result_int  : std_logic_vector(NUM_PARITY_BITS_32 - 1 downto 0);
	signal data_cnt        : unsigned(10 downto 0);
	signal remove_crc      : std_logic;
	signal crc_valid_int   : std_logic;
	signal input_ready_int : std_logic;

begin

	input_ready     <= input_ready_int;
	input_ready_int <= output_ready or remove_crc;
	output_valid    <= input_valid and (not remove_crc);
	output_data     <= input_data;

	-- Calculate the CRC-32 for the payload.
	pr_calc_crc32: process(clk) is
		variable v_shift_reg : std_logic_vector(NUM_PARITY_BITS_32 - 1 downto 0);
	begin
	if rising_edge(clk) then
		if rst = '1' then
			shift_reg_32   <= INIT_STATE_32;
			crc_result_int <= (others => '0');
			data_cnt       <= (others => '0');
			remove_crc     <= '0';
			output_last    <= '0';
			crc_valid      <= '0';
			crc_correct    <= '0';
			crc_valid_int  <= '0';
		else
			v_shift_reg   := shift_reg_32;
			crc_valid_int <= '0';

			if input_ready_int = '1' and input_valid = '1' then
				v_shift_reg  := crc32(input_data, v_shift_reg);
				shift_reg_32 <= v_shift_reg;
				data_cnt     <= data_cnt + 1;

				if data_cnt = FRAME_SIZE - 5 then
					remove_crc <= '1';
				end if;

				if data_cnt = FRAME_SIZE - 6 then
					output_last <= '1';
				else
					output_last <= '0';
				end if;

				if data_cnt = FRAME_SIZE - 1 then
					remove_crc     <= '0';
					data_cnt       <= (others => '0');
					shift_reg_32   <= INIT_STATE_32;
					crc_result_int <= v_shift_reg;
					crc_valid_int  <= '1';
				end if;
			end if;

			crc_valid   <= crc_valid_int;
			crc_correct <= '0';

			if crc_valid_int = '1' then
				if crc_result_int = RESIDUE then
					crc_correct <= '1';
				end if;
			end if;
		end if;
	end if;
	end process pr_calc_crc32;

end arch;

