--!
--! Copyright (C) 2022 Creonic GmbH
--!
--! @file
--! @brief  LDPC encoder for 3gpp rl15 (5G new radio)
--! @author Nhan Nguyen
--! @date   2022/09/01
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_decoding;
library dec_ldpc;

library xilinx_ip;
use xilinx_ip.pkg_components.all;

entity dec_ldpc_wrapper is
port (

	clk            : in  std_logic;
	rst            : in  std_logic;

	ldpc_clk       : in  std_logic;
	ldpc_rst       : in  std_logic;

	-- Configuration input
	----------------------
	cfg_tdata      : in  std_logic_vector(63 downto 0);
	cfg_tvalid     : in  std_logic;
	cfg_tready     : out std_logic;

	-- Input data handling
	----------------------

	s_axis_tdata    : in  std_logic_vector(63 downto 0);
	s_axis_tvalid   : in  std_logic;
	s_axis_tlast    : in  std_logic;
	s_axis_tready   : out std_logic;

	-- Configuration output
	----------------------
	cfg_out_tdata  : out std_logic_vector(63 downto 0);
	cfg_out_tvalid : out std_logic;
	cfg_out_tready : in  std_logic;

	-- Output data handling
	-----------------------
	m_axis_tdata   : out std_logic_vector(7 downto 0);
	m_axis_tvalid  : out std_logic;
	m_axis_tlast   : out std_logic;
	m_axis_tready  : in  std_logic
);


end entity dec_ldpc_wrapper;


architecture rtl of dec_ldpc_wrapper is

	constant BW_CHV          : natural := 6;
	constant BW_PASS_THROUGH : natural := 20;

	component dec_ldpc_top is
	port (
		clk                    : in  std_logic;
		rst                    : in  std_logic;

		-- Input data handling
		----------------------
		s_axis_data            : in  std_logic_vector(16 * BW_CHV - 1 downto 0);
		s_axis_valid           : in  std_logic;
		s_axis_last            : in  std_logic;
		s_axis_ready           : out std_logic;

		-- Configuration input
		cfg_block_size         : in  std_logic_vector(15 - 1 downto 0);
		cfg_code_rate          : in  std_logic_vector(7 - 1 downto 0);
		cfg_lifting_size       : in  std_logic_vector(9 - 1 downto 0);
		cfg_iterations         : in  std_logic_vector(6 - 1 downto 0);
		cfg_pass_through       : in  std_logic_vector(20 - 1 downto 0);


		-- Output data handling
		-----------------------
		m_axis_data            : out std_logic_vector(16 - 1 downto 0);
		m_axis_valid           : out std_logic;
		m_axis_last            : out std_logic;
		m_axis_ready           : in  std_logic;

		-- Output configuration handling
		----------------------------------
		cfg_out_block_size     : out std_logic_vector(15 - 1 downto 0);
		cfg_out_code_rate      : out std_logic_vector(7 - 1 downto 0);
		cfg_out_lifting_size   : out std_logic_vector(9 - 1 downto 0);
		cfg_out_pass_through   : out std_logic_vector(20 - 1 downto 0);

		-- Statistics
		--------------------------------------
		stat_decoding_success  : out std_logic;
		stat_iterations_needed : out std_logic_vector(6 - 1 downto 0)
	);
	end component;


	signal rst_n      : std_logic;
	signal ldpc_rst_n : std_logic;

	signal input_dwidth_out_tvalid : std_logic;
	signal input_dwidth_out_tlast  : std_logic;
	signal input_dwidth_out_tready : std_logic;
	signal input_dwidth_out_tdata  : std_logic_vector(127 downto 0);

	signal ldpc_fifo_in_tvalid          : std_logic;
	signal ldpc_fifo_in_tlast           : std_logic;
	signal ldpc_fifo_in_tready          : std_logic;
	signal ldpc_fifo_in_tdata           : std_logic_vector(127 downto 0);
	signal ldpc_fifo_in_tdata_llr       : std_logic_vector(16 * BW_CHV - 1 downto 0);

	signal cfg_ldpc_fifo_in_tdata       : std_logic_vector(63 downto 0);
	signal cfg_ldpc_fifo_in_tvalid      : std_logic;
	signal cfg_ldpc_fifo_in_tready      : std_logic;


	signal ldpc_in_tvalid          : std_logic;
	signal ldpc_in_tlast           : std_logic;
	signal ldpc_in_tready          : std_logic;
	signal ldpc_in_tdata           : std_logic_vector(16 * BW_CHV - 1 downto 0);

	signal cfg_ldpc_in_tdata       : std_logic_vector(63 downto 0);
	signal cfg_ldpc_in_tvalid      : std_logic;
	signal cfg_ldpc_in_tready      : std_logic;

	signal cfg_block_size          : std_logic_vector(15 - 1 downto 0);
	signal cfg_code_rate           : std_logic_vector(7 - 1 downto 0);
	signal cfg_lifting_size        : std_logic_vector(9 - 1 downto 0);
	signal cfg_iterations          : std_logic_vector(6 - 1 downto 0);
	signal cfg_pass_through        : std_logic_vector(BW_PASS_THROUGH - 1 downto 0);

	signal ldpc_out_tvalid         : std_logic;
	signal ldpc_out_tlast          : std_logic;
	signal ldpc_out_tready         : std_logic;
	signal ldpc_out_tdata          : std_logic_vector(15 downto 0);

	signal cfg_ldpc_out_tdata      : std_logic_vector(63 downto 0);
	signal cfg_ldpc_out_tvalid     : std_logic;
	signal cfg_ldpc_out_tready     : std_logic;

	signal cfg_out_block_size      : std_logic_vector(15 - 1 downto 0);
	signal cfg_out_code_rate       : std_logic_vector(7 - 1 downto 0);
	signal cfg_out_lifting_size    : std_logic_vector(9 - 1 downto 0);
	signal cfg_out_pass_through    : std_logic_vector(BW_PASS_THROUGH - 1 downto 0);
	signal stat_decoding_success   : std_logic;
	signal stat_iterations_needed  : std_logic_vector(6 - 1 downto 0);
	signal ldpc_out_cfg_sent       : std_logic;

	signal output_dwidth_in_tvalid : std_logic;
	signal output_dwidth_in_tlast  : std_logic;
	signal output_dwidth_in_tready : std_logic;
	signal output_dwidth_in_tdata  : std_logic_vector(15 downto 0);

begin

	rst_n      <= not rst;
	ldpc_rst_n <= not ldpc_rst;

	-- 8 to 16 bit input data
	inst_dwidth_8_to_16 : dwidth_8_to_16_llr
	port map(
		aclk          => clk,
		aresetn       => rst_n,

		s_axis_tvalid => s_axis_tvalid,
		s_axis_tready => s_axis_tready,
		s_axis_tdata  => s_axis_tdata,
		s_axis_tlast  => s_axis_tlast,

		m_axis_tvalid => input_dwidth_out_tvalid,
		m_axis_tready => input_dwidth_out_tready,
		m_axis_tdata  => input_dwidth_out_tdata,
		m_axis_tlast  => input_dwidth_out_tlast
	);

	-- CDC : from 330 MHz (clk) domain to 230 MHz (ldpc_clk)
	inst_axis_128bit_clk_conv : axis_128bit_clk_conv
	port map(
		s_axis_aclk    => clk,
		s_axis_aresetn => rst_n,

		s_axis_tvalid  => input_dwidth_out_tvalid,
		s_axis_tready  => input_dwidth_out_tready,
		s_axis_tdata   => input_dwidth_out_tdata,
		s_axis_tlast   => input_dwidth_out_tlast,

		m_axis_aclk    => ldpc_clk,
		m_axis_aresetn => ldpc_rst_n,

		m_axis_tvalid  => ldpc_fifo_in_tvalid,
		m_axis_tready  => ldpc_fifo_in_tready,
		m_axis_tdata   => ldpc_fifo_in_tdata,
		m_axis_tlast   => ldpc_fifo_in_tlast
	);
	inst_axis_64bit_clk_conv : axis_64bit_clk_conv
	port map(
		s_axis_aclk    => clk,
		s_axis_aresetn => rst_n,

		s_axis_tvalid  => cfg_tvalid,
		s_axis_tready  => cfg_tready,
		s_axis_tdata   => cfg_tdata,

		m_axis_aclk    => ldpc_clk,
		m_axis_aresetn => ldpc_rst_n,

		m_axis_tvalid  => cfg_ldpc_fifo_in_tvalid,
		m_axis_tready  => cfg_ldpc_fifo_in_tready,
		m_axis_tdata   => cfg_ldpc_fifo_in_tdata
	);

	gen_input_fifo : for i in 0 to 15 generate
		ldpc_fifo_in_tdata_llr((i + 1) * BW_CHV - 1 downto i * BW_CHV) <= ldpc_fifo_in_tdata(BW_CHV + i * 8 - 1 downto i * 8);
	end generate;

	-- FIFOs for storing the input LLR and CFG
	inst_llr_fifo : entity frame_decoding.axi4s_fifo
	generic map(
		DISTR_RAM         => false,
		FIFO_DEPTH        => (16384) / 16,
		DATA_WIDTH        => 16 * BW_CHV,
		FULL_THRESHOLD    => (16384) / 16,
		USE_OUTPUT_BUFFER => true
	)
	port map(

		clk             => ldpc_clk,
		rst             => ldpc_rst,

		-- Input data handling
		----------------------
		input_ready       => ldpc_fifo_in_tready,
		input_valid       => ldpc_fifo_in_tvalid,
		input_last        => ldpc_fifo_in_tlast,
		input_data        => ldpc_fifo_in_tdata_llr,

		input_almost_full => open,


		-- Output data handling
		-----------------------
		output_ready     => ldpc_in_tready,
		output_valid     => ldpc_in_tvalid,
		output_last      => ldpc_in_tlast,
		output_data      => ldpc_in_tdata
	);

	inst_cfg_fifo : entity frame_decoding.axi4s_fifo
	generic map(
		DISTR_RAM         => true,
		FIFO_DEPTH        => 16,
		DATA_WIDTH        => 64,
		FULL_THRESHOLD    => 16,
		USE_OUTPUT_BUFFER => true
	)
	port map(

		clk             => ldpc_clk,
		rst             => ldpc_rst,

		-- Input data handling
		----------------------
		input_ready      => cfg_ldpc_fifo_in_tready,
		input_valid      => cfg_ldpc_fifo_in_tvalid,
		input_last       => '0',
		input_data       => cfg_ldpc_fifo_in_tdata,

		input_almost_full => open,


		-- Output data handling
		-----------------------
		output_ready     => cfg_ldpc_in_tready,
		output_valid     => cfg_ldpc_in_tvalid,
		output_last      => open,
		output_data      => cfg_ldpc_in_tdata
	);

	-- control the cfg fifo input
	pr_ldpc_cfg_in : process(ldpc_clk) is
	begin
	if rising_edge(ldpc_clk) then
		if ldpc_rst = '1' then
			cfg_ldpc_in_tready <= '0';
		else
			if ldpc_in_tvalid = '1' and ldpc_in_tready = '1' and ldpc_in_tlast = '1' then
				cfg_ldpc_in_tready <= '1';
			end if;
			if cfg_ldpc_in_tvalid = '1' and cfg_ldpc_in_tready = '1' then
				cfg_ldpc_in_tready <= '0';
			end if;
		end if;
	end if;
	end process;

	cfg_block_size   <= cfg_ldpc_in_tdata(14 downto 0);
	cfg_code_rate    <= cfg_ldpc_in_tdata(21 downto 15);
	cfg_lifting_size <= cfg_ldpc_in_tdata(30 downto 22);
	cfg_iterations   <= cfg_ldpc_in_tdata(36 downto 31);
	cfg_pass_through <= cfg_ldpc_in_tdata(63 downto  64 - BW_PASS_THROUGH);

	inst_dec_ldpc_top : dec_ldpc_top
	port map(
		clk                    => ldpc_clk,
		rst                    => ldpc_rst,

		-- Input data handling
		----------------------
		s_axis_data            => ldpc_in_tdata,
		s_axis_valid           => ldpc_in_tvalid,
		s_axis_last            => ldpc_in_tlast,
		s_axis_ready           => ldpc_in_tready,

		-- Configuration input
		cfg_block_size         => cfg_block_size,
		cfg_code_rate          => cfg_code_rate,
		cfg_lifting_size       => cfg_lifting_size,
		cfg_iterations         => cfg_iterations,
		cfg_pass_through       => cfg_pass_through,


		-- Output data handling
		-----------------------
		m_axis_data            => ldpc_out_tdata,
		m_axis_valid           => ldpc_out_tvalid,
		m_axis_last            => ldpc_out_tlast,
		m_axis_ready           => ldpc_out_tready,

		-- Output configuration handling
		----------------------------------
		cfg_out_block_size     => cfg_out_block_size,
		cfg_out_code_rate      => cfg_out_code_rate,
		cfg_out_lifting_size   => cfg_out_lifting_size,
		cfg_out_pass_through   => cfg_out_pass_through,

		-- Statistics
		--------------------------------------
		stat_decoding_success  => stat_decoding_success,
		stat_iterations_needed => stat_iterations_needed
	);


	cfg_ldpc_out_tdata(14 downto 0)  <= cfg_out_block_size;
	cfg_ldpc_out_tdata(21 downto 15) <= cfg_out_code_rate;
	cfg_ldpc_out_tdata(30 downto 22) <= cfg_out_lifting_size;
	cfg_ldpc_out_tdata(36 downto 31) <= stat_iterations_needed;
	cfg_ldpc_out_tdata(37)           <= stat_decoding_success;
	cfg_ldpc_out_tdata(63 downto  64 - BW_PASS_THROUGH) <= cfg_out_pass_through;
	cfg_ldpc_out_tdata(64 - BW_PASS_THROUGH - 1 downto 38) <= (others => '0');

	-- output cfg
	pr_ldpc_cfg_out : process(ldpc_clk) is
	begin
	if rising_edge(ldpc_clk) then
		if ldpc_rst = '1' then
			cfg_ldpc_out_tvalid <= '0';
			ldpc_out_cfg_sent   <= '0';
		else
			if ldpc_out_tvalid = '1' and ldpc_out_tready = '1' and ldpc_out_cfg_sent = '0' then
				ldpc_out_cfg_sent   <= '1';
				cfg_ldpc_out_tvalid <= '1';
			end if;
			if ldpc_out_tvalid = '1' and ldpc_out_tready = '1' and ldpc_out_tlast = '1' then
				ldpc_out_cfg_sent   <= '0';
			end if;
			if cfg_ldpc_out_tvalid = '1' and cfg_ldpc_out_tready = '1' then
				cfg_ldpc_out_tvalid <= '0';
			end if;
		end if;
	end if;
	end process;

	-- CDC : from 230 MHz (clk) domain to 330 MHz (ldpc_clk)
	inst_axis_16bit_clk_conv_out : axis_16bit_clk_conv
	port map(
		s_axis_aclk    => ldpc_clk,
		s_axis_aresetn => ldpc_rst_n,

		s_axis_tvalid  => ldpc_out_tvalid,
		s_axis_tready  => ldpc_out_tready,
		s_axis_tdata   => ldpc_out_tdata,
		s_axis_tlast   => ldpc_out_tlast,

		m_axis_aclk    => clk,
		m_axis_aresetn => rst_n,

		m_axis_tvalid  => output_dwidth_in_tvalid,
		m_axis_tready  => output_dwidth_in_tready,
		m_axis_tdata   => output_dwidth_in_tdata,
		m_axis_tlast   => output_dwidth_in_tlast
	);

	inst_axis_64bit_clk_conv_out : axis_64bit_clk_conv
	port map(
		s_axis_aclk    => ldpc_clk,
		s_axis_aresetn => ldpc_rst_n,

		s_axis_tvalid  => cfg_ldpc_out_tvalid,
		s_axis_tready  => cfg_ldpc_out_tready,
		s_axis_tdata   => cfg_ldpc_out_tdata,

		m_axis_aclk    => clk,
		m_axis_aresetn => rst_n,

		m_axis_tvalid  => cfg_out_tvalid,
		m_axis_tready  => cfg_out_tready,
		m_axis_tdata   => cfg_out_tdata
	);

	-- 16 to 8 bit input data
	inst_dwidth_16_to_8 : dwidth_16_to_8
	port map(
		aclk          => clk,
		aresetn       => rst_n,

		s_axis_tvalid => output_dwidth_in_tvalid,
		s_axis_tready => output_dwidth_in_tready,
		s_axis_tdata  => output_dwidth_in_tdata,
		s_axis_tlast  => output_dwidth_in_tlast,

		m_axis_tvalid => m_axis_tvalid,
		m_axis_tready => m_axis_tready,
		m_axis_tdata  => m_axis_tdata,
		m_axis_tlast  => m_axis_tlast
	);


end architecture rtl;
