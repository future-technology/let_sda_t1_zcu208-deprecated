--!
--! Copyright (C) 2021 Creonic GmbH
--!
--! @file
--! @brief
--! @author Nhan Nguyen
--! @date   2021/02/23
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_decoding;
use frame_decoding.pkg_support.all;


entity diagnostic_module is
	port(
		clk : in std_logic;
		rst : in std_logic;

		clear_stat    : in  std_logic;

		crc16_valid   : in  std_logic;
		crc16_correct : in  std_logic;
		crc32_valid   : in  std_logic;
		crc32_correct : in  std_logic;

		total_frame_counter : out std_logic_vector(31 downto 0);
		crc_error_counter   : out std_logic_vector(31 downto 0);
		crc16_err_counter   : out std_logic_vector(31 downto 0);
		crc32_err_counter   : out std_logic_vector(31 downto 0)
	);
end entity diagnostic_module;

architecture rtl of diagnostic_module is

	signal crc16_correct_vec : std_logic_vector(0 downto 0);
	signal crc16_correct_buf : std_logic_vector(0 downto 0);
	signal crc16_buf_valid   : std_logic;
	signal crc16_buf_ready   : std_logic;
	signal crc32_correct_vec : std_logic_vector(0 downto 0);
	signal crc32_correct_buf : std_logic_vector(0 downto 0);
	signal crc32_buf_valid   : std_logic;
	signal crc32_buf_ready   : std_logic;

	signal frame_cnt        : unsigned(31 downto 0);
	signal err_frame_cnt    : unsigned(31 downto 0);
	signal crc16_err_cnt    : unsigned(31 downto 0);
	signal crc32_err_cnt    : unsigned(31 downto 0);

begin

	crc16_correct_vec(0) <= crc16_correct;
	crc32_correct_vec(0) <= crc32_correct;

	total_frame_counter <= std_logic_vector(frame_cnt);
	crc_error_counter   <= std_logic_vector(err_frame_cnt);
	crc16_err_counter   <= std_logic_vector(crc16_err_cnt);
	crc32_err_counter   <= std_logic_vector(crc32_err_cnt);

	-- collect all stats using simple counters
	pr_stats_collect: process(clk) is
	begin
	if rising_edge(clk) then
		if rst = '1' then
			frame_cnt     <= (others => '0');
			err_frame_cnt <= (others => '0');
			crc16_err_cnt <= (others => '0');
			crc32_err_cnt <= (others => '0');
			crc16_buf_ready <= '0';
			crc32_buf_ready <= '0';
		else
			if crc16_buf_valid = '1' and crc32_buf_valid = '1' then
				crc16_buf_ready  <= '1';
				crc32_buf_ready  <= '1';
			end if;
			if crc32_buf_valid = '1' and crc32_buf_ready = '1' then
				crc16_buf_ready  <= '0';
				crc32_buf_ready  <= '0';
				frame_cnt <= frame_cnt + 1;
				if crc16_correct_buf(0) = '0' then
					crc16_err_cnt <= crc16_err_cnt + 1;
				end if;
				if crc32_correct_buf(0) = '0' then
					crc32_err_cnt <= crc32_err_cnt + 1;
				end if;
				if crc16_correct_buf(0) = '0' or crc32_correct_buf(0) = '0' then
					err_frame_cnt <= err_frame_cnt + 1;
				end if;
			end if;

			if clear_stat = '1' then
				frame_cnt     <= (others => '0');
				err_frame_cnt <= (others => '0');
				crc16_err_cnt <= (others => '0');
				crc32_err_cnt <= (others => '0');
			end if;
		end if;
	end if;
	end process pr_stats_collect;

	-- FIFOs for different stats
	inst_crc16_buf : entity frame_decoding.axi4s_fifo
	generic map(
		DISTR_RAM         => true,
		FIFO_DEPTH        => 32,
		DATA_WIDTH        => 1,
		FULL_THRESHOLD    => 32,
		USE_OUTPUT_BUFFER => false
	)
	port map(
		clk            => clk,
		rst            => rst,

		-- Input data handling
		----------------------
		input_ready       => open,
		input_valid       => crc16_valid,
		input_last        => '0',
		input_data        => crc16_correct_vec,
		input_almost_full => open,

		-- Output data handling
		-----------------------
		output_ready => crc16_buf_ready,
		output_valid => crc16_buf_valid,
		output_last  => open,
		output_data  => crc16_correct_buf
	);

	-- FIFOs for different stats
	inst_crc32_buf : entity frame_decoding.axi4s_fifo
	generic map(
		DISTR_RAM         => true,
		FIFO_DEPTH        => 32,
		DATA_WIDTH        => 1,
		FULL_THRESHOLD    => 32,
		USE_OUTPUT_BUFFER => false
	)
	port map(
		clk            => clk,
		rst            => rst,

		-- Input data handling
		----------------------
		input_ready       => open,
		input_valid       => crc32_valid,
		input_last        => '0',
		input_data        => crc32_correct_vec,
		input_almost_full => open,

		-- Output data handling
		-----------------------
		output_ready => crc32_buf_ready,
		output_valid => crc32_buf_valid,
		output_last  => open,
		output_data  => crc32_correct_buf
	);


end architecture rtl;
