--!
--! Copyright (C) 2021 Creonic GmbH
--!
--! @file
--! @brief
--! @author Nhan Nguyen
--! @date   2021/02/25
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_decoding;
use frame_decoding.pkg_support.all;
use frame_decoding.pkg_types.all;


entity frame_decoding_top is
	generic (
		USE_XILINX_FEC : boolean := false
	);
	port (
		ldpc_clk : in  std_logic;
		ldpc_rst : in  std_logic;

		clk      : in  std_logic;
		rst      : in  std_logic;

		preamble : in  std_logic_vector(63 downto 0);

		-- configuration for the preamble sync algorithm
		cfg_acquisition_length : in std_logic_vector(31 downto 0);
		cfg_tracking_length    : in std_logic_vector(31 downto 0);
		cfg_max_tracking_error : in std_logic_vector(31 downto 0);
		cfg_preamble_max_corr  : in std_logic_vector(6 downto 0); -- CAGS: preamle detection thresh control

		s_axis_ctrl_tready : out std_logic;
		s_axis_ctrl_tvalid : in  std_logic;
		s_axis_ctrl_tdata  : in  std_logic_vector(2 downto 0);

		-- Input data handling
		----------------------
		s_axis_tvalid   : in  std_logic;
		s_axis_tdata    : in  std_logic_vector(7 downto 0);

		-- Status
		-----------------------
		crc16_valid     : out std_logic;
		crc16_correct   : out std_logic;
		crc32_valid     : out std_logic;
		crc32_correct   : out std_logic;

		preamble_synced : out std_logic;

		-- More diagnostic informations
		-------------------------------------------------------
		clear_stat          : in  std_logic;
		total_frame_counter : out std_logic_vector(31 downto 0);
		crc_error_counter   : out std_logic_vector(31 downto 0);
		crc16_err_counter   : out std_logic_vector(31 downto 0);
		crc32_err_counter   : out std_logic_vector(31 downto 0);

		-- Stats from the preamble sync
		new_search_counter  : out std_logic_vector(31 downto 0);
		sync_loss_counter   : out std_logic_vector(31 downto 0);

		-- Output data handling
		-----------------------
		m_axis_tready : in  std_logic;
		m_axis_tvalid : out std_logic;
		m_axis_tlast  : out std_logic;
		m_axis_tdata  : out std_logic_vector(7 downto 0)
	);
end entity frame_decoding_top;


architecture rtl of frame_decoding_top is

	signal sync_input_valid : std_logic;
	signal sync_input_start : std_logic;
	signal sync_input_last  : std_logic;
	signal sync_input_data  : std_logic_vector(7 downto 0);

	signal sync_cfg  : trec_cfg_preamble_sync;
	signal sync_stat : trec_stat_preamble_sync;

	signal scrambler_in_ready : std_logic;
	signal scrambler_in_valid : std_logic;
	signal scrambler_in_last  : std_logic;
	signal scrambler_in_data  : std_logic_vector(7 downto 0);

	signal scrambler_out_ready : std_logic;
	signal scrambler_out_valid : std_logic;
	signal scrambler_out_last  : std_logic;
	signal scrambler_out_data  : std_logic_vector(7 downto 0);

	signal header_dec_ready : std_logic;
	signal header_dec_valid : std_logic;
	signal header_dec_last  : std_logic;
	signal header_dec_data  : std_logic_vector(7 downto 0);

	signal dec_ctrl_ready : std_logic;
	signal dec_ctrl_valid : std_logic;
	signal dec_ctrl_data  : std_logic_vector(2 downto 0);
	signal dec_ctrl_user  : std_logic_vector(5 downto 0);

	signal dec_input_ready : std_logic;
	signal dec_input_valid : std_logic;
	signal dec_input_last  : std_logic;
	signal dec_input_data  : std_logic_vector(7 downto 0);

	signal dec_output_ready : std_logic;
	signal dec_output_valid : std_logic;
	signal dec_output_last  : std_logic;
	signal dec_output_data  : std_logic_vector(7 downto 0);

	signal payload_ready : std_logic;
	signal payload_valid : std_logic;
	signal payload_last  : std_logic;
	signal payload_data  : std_logic_vector(7 downto 0);

	signal header_buf_in_ready : std_logic;
	signal header_buf_in_valid : std_logic;
	signal header_buf_in_last  : std_logic;
	signal header_buf_in_data  : std_logic_vector(7 downto 0);

	signal header_buf_out_ready : std_logic;
	signal header_buf_out_valid : std_logic;
	signal header_buf_out_last  : std_logic;
	signal header_buf_out_data  : std_logic_vector(7 downto 0);

	signal header_ready     : std_logic;
	signal header_valid     : std_logic;
	signal header_last      : std_logic;
	signal header_data      : std_logic_vector(7 downto 0);
	signal s_axis_tdata_int : std_logic_vector(7 downto 0);

	signal slip_en    : std_logic;
	signal bits_slip  : unsigned( 2 downto 0);
	signal bytes_slip : unsigned(11 downto 0);
	signal frame_size : unsigned(11 downto 0);
	signal slip_done  : std_logic;
	signal preamble_synced_int : std_logic;
	signal soft_rst : std_logic;
	signal rx_rst : std_logic;

	signal crc16_valid_int   : std_logic;
	signal crc16_correct_int : std_logic;
	signal crc32_valid_int   : std_logic;
	signal crc32_correct_int : std_logic;

	signal dec_status_valid : std_logic;
	signal dec_status_data  : std_logic_vector(63 downto 0);

begin

	preamble_synced <= preamble_synced_int;
	rx_rst <= rst or soft_rst;

	crc16_valid        <= crc16_valid_int;
	crc16_correct      <= crc16_correct_int;
	crc32_valid        <= crc32_valid_int;
	crc32_correct      <= crc32_correct_int;
	new_search_counter <= std_logic_vector(sync_stat.new_search_counter);
	sync_loss_counter  <= std_logic_vector(sync_stat.sync_loss_counter);

	sync_cfg.acquisition_length <= unsigned(cfg_acquisition_length);
	sync_cfg.tracking_length    <= unsigned(cfg_tracking_length);
	sync_cfg.max_tracking_error <= unsigned(cfg_max_tracking_error);
	sync_cfg.preamble_max_corr  <= unsigned(cfg_preamble_max_corr);    -- CAGS: preamle detection thresh control

	inst_diagnostic_module : entity frame_decoding.diagnostic_module
	port map(
		clk => clk,
		rst => rst,

		clear_stat    => clear_stat,

		crc16_valid   => crc16_valid_int,
		crc16_correct => crc16_correct_int,
		crc32_valid   => crc32_valid_int,
		crc32_correct => crc32_correct_int,

		total_frame_counter => total_frame_counter,
		crc_error_counter   => crc_error_counter,
		crc16_err_counter   => crc16_err_counter,
		crc32_err_counter   => crc32_err_counter
	);

	-- Xilinx Gigabit Tranceiver send the LSB first
	pr_in_data : process(s_axis_tdata) is
		begin
			for i in 0 to 7 loop
				s_axis_tdata_int(i) <= s_axis_tdata(7 - i);
			end loop;
		end process;

	-- Deframing - remove the preamble
	inst_fso_framing : entity frame_decoding.fso_framing
	port map(
		clk         => clk,
		rst         => rx_rst,

		input_valid => s_axis_tvalid,
		input_data  => s_axis_tdata_int,

		slip_en      => slip_en,
		bytes_slip   => bytes_slip,
		bits_slip    => bits_slip,
		frame_size   => frame_size,
		slip_done    => slip_done,

		output_valid => sync_input_valid,
		output_start => sync_input_start,
		output_last  => sync_input_last,
		output_data  => sync_input_data
	);


	inst_preamble_sync : entity frame_decoding.preamble_sync
	port map(
		clk      => clk,
		rst      => rst,
		preamble => preamble,

		ctrl_in_ready => s_axis_ctrl_tready,
		ctrl_in_valid => s_axis_ctrl_tvalid,
		ctrl_in_data  => s_axis_ctrl_tdata,

		input_valid => sync_input_valid,
		input_start => sync_input_start,
		input_last  => sync_input_last,
		input_data  => sync_input_data,

		slip_en      => slip_en,
		bits_slip    => bits_slip,
		bytes_slip   => bytes_slip,
		frame_size   => frame_size,
		slip_done    => slip_done,

		output_valid => scrambler_in_valid,
		output_last  => scrambler_in_last,
		output_data  => scrambler_in_data,

		reconfiguration => soft_rst,
		preamble_synced => preamble_synced_int,

		ctrl_out_ready => dec_ctrl_ready,
		ctrl_out_valid => dec_ctrl_valid,
		ctrl_out_data  => dec_ctrl_data,
		ctrl_out_user  => dec_ctrl_user,

		configurations => sync_cfg,
		statistics     => sync_stat,
		clear_stat     => clear_stat
	);


	-- Scrambler
	inst_scrambler : entity frame_decoding.scrambling
	port map(
		clk => clk,
		rst => rst,

		input_ready => scrambler_in_ready,
		input_valid => scrambler_in_valid,
		input_last  => scrambler_in_last,
		input_data  => scrambler_in_data,

		output_ready => scrambler_out_ready,
		output_valid => scrambler_out_valid,
		output_last  => scrambler_out_last,
		output_data  => scrambler_out_data
	);

	-- Split data into header and payload
	inst_frame_demux : entity frame_decoding.frame_demux
	port map(
		clk => clk,
		rst => rst,

		input_ready => scrambler_out_ready,
		input_valid => scrambler_out_valid,
		input_last  => scrambler_out_last,
		input_data  => scrambler_out_data,

		header_ready => header_dec_ready,
		header_valid => header_dec_valid,
		header_last  => header_dec_last,
		header_data  => header_dec_data,

		payload_ready => dec_input_ready,
		payload_valid => dec_input_valid,
		payload_last  => dec_input_last,
		payload_data  => dec_input_data
	);

	-- Payload Decoder (LDPC)
	inst_payload_decoder : entity frame_decoding.payload_decoder
	generic map(
		USE_XILINX_FEC => USE_XILINX_FEC
	)
	port map(
		core_clk => ldpc_clk,
		core_rst => ldpc_rst,
		clk      => clk,
		rst      => rst,

		ctrl_ready => dec_ctrl_ready,
		ctrl_valid => dec_ctrl_valid,
		ctrl_data  => dec_ctrl_data,
		ctrl_user  => dec_ctrl_user,

		-- Input data handling
		----------------------

		input_ready => dec_input_ready,
		input_valid => dec_input_valid,
		input_last  => dec_input_last,
		input_data  => dec_input_data,

		-- Status
		---------------------------
		status_ready => '1',
		status_valid => dec_status_valid,
		status_data  => dec_status_data,

		-- Output data handling
		-----------------------

		output_ready => dec_output_ready,
		output_valid => dec_output_valid,
		output_last  => dec_output_last,
		output_data  => dec_output_data
	);

	-- CRC32 for payload data
	inst_crc32 : entity frame_decoding.crc32_checker
	port map(
		clk => clk,
		rst => rst,

		input_ready => dec_output_ready,
		input_valid => dec_output_valid,
		input_last  => dec_output_last,
		input_data  => dec_output_data,

		crc_valid   => crc32_valid_int,
		crc_correct => crc32_correct_int,

		output_ready => payload_ready,
		output_valid => payload_valid,
		output_last  => payload_last,
		output_data  => payload_data
	);

	-- Header Decoder (Convolutional)
	inst_header_decoder : entity frame_decoding.header_decoder
	port map(
		clk => clk,
		rst => rst,

		-- Input data handling
		----------------------

		input_ready  => header_dec_ready,
		input_valid  => header_dec_valid,
		input_last   => header_dec_last,
		input_data   => header_dec_data,

		-- Output data handling
		-----------------------
		output_ready => header_buf_in_ready,
		output_valid => header_buf_in_valid,
		output_last  => header_buf_in_last,
		output_data  => header_buf_in_data,

		cfg_ready => open,
		cfg_valid => '1',
		cfg_data  => x"005F001F"
	);

	-- Header buffer
	inst_axi4s_fifo : entity frame_decoding.axi4s_fifo
	generic map(
		DISTR_RAM         => false,
		FIFO_DEPTH        => 8192,
		DATA_WIDTH        => 8,
		FULL_THRESHOLD    => 8192,
		USE_OUTPUT_BUFFER => false
	)
	port map(
		clk => clk,
		rst => rst,

		-- Input data handling
		----------------------
		input_ready       => header_buf_in_ready,
		input_valid       => header_buf_in_valid,
		input_last        => header_buf_in_last,
		input_data        => header_buf_in_data,
		input_almost_full => open,

		-- Output data handling
		-----------------------
		output_ready => header_buf_out_ready,
		output_valid => header_buf_out_valid,
		output_last  => header_buf_out_last,
		output_data  => header_buf_out_data
	);

	-- CRC16 for header data
	inst_crc16 : entity frame_decoding.crc16_checker
	port map(
		clk => clk,
		rst => rst,

		input_ready => header_buf_out_ready,
		input_valid => header_buf_out_valid,
		input_last  => header_buf_out_last,
		input_data  => header_buf_out_data,

		crc_valid   => crc16_valid_int,
		crc_correct => crc16_correct_int,

		output_ready => header_ready,
		output_valid => header_valid,
		output_last  => header_last,
		output_data  => header_data
	);


	-- Combine the header and payload data
	inst_frame_mux : entity frame_decoding.frame_mux
	port map(
		clk => clk,
		rst => rst,

		header_ready => header_ready,
		header_valid => header_valid,
		header_last  => header_last,
		header_data  => header_data,

		payload_ready => payload_ready,
		payload_valid => payload_valid,
		payload_last  => payload_last,
		payload_data  => payload_data,

		frame_ready => m_axis_tready,
		frame_valid => m_axis_tvalid,
		frame_last  => m_axis_tlast,
		frame_data  => m_axis_tdata
	);

end rtl;
