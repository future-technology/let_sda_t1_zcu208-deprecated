--!
--! Copyright (C) Creonic GmbH
--!
--! @file
--! @brief
--! @author Carlos Velez
--! @date   2021/11/30
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_decoding;
use frame_decoding.pkg_support.all;


entity frame_demux is
	port(
		clk : in std_logic;
		rst : in std_logic;

		input_ready : out std_logic;
		input_valid : in  std_logic;
		input_last  : in  std_logic;
		input_data  : in  std_logic_vector(8 - 1 downto 0);

		header_ready : in  std_logic;
		header_valid : out std_logic;
		header_last  : out std_logic;
		header_data  : out std_logic_vector(8 - 1 downto 0);

		payload_ready : in  std_logic;
		payload_valid : out std_logic;
		payload_last  : out std_logic;
		payload_data  : out std_logic_vector(8 - 1 downto 0)
	);
end entity frame_demux;

architecture rtl of frame_demux is

	constant HEADER_LEN  : natural := 120;

	type t_data_read_state is (GET_HEADER, GET_PAYLOAD);
	signal data_read_state : t_data_read_state;
	signal input_ready_int : std_logic;
	signal input_cnt       : unsigned(11 downto 0);

begin

	header_data  <= input_data;
	header_valid <= input_valid when data_read_state = GET_HEADER else '0';

	payload_data  <= input_data;
	payload_last  <= input_last;
	payload_valid <= input_valid when data_read_state = GET_PAYLOAD else '0';

	input_ready_int <= payload_ready when data_read_state = GET_PAYLOAD else header_ready;
	input_ready     <= input_ready_int;

	-- FSM for reading the input data
	pr_input_data_read : process(clk) is
	begin
	if rising_edge(clk) then
		if rst = '1' then
			input_cnt       <= (others => '0');
			header_last     <= '0';
			data_read_state <= GET_HEADER;
		else

			case data_read_state is

			when GET_HEADER =>
				if input_valid = '1' and input_ready_int = '1' then
					input_cnt <= input_cnt + 1;

					if input_cnt = HEADER_LEN - 2 then
						header_last <= '1';
					elsif input_cnt = HEADER_LEN - 1 then
						header_last     <= '0';
						data_read_state <= GET_PAYLOAD;
					end if;
				end if;

			when GET_PAYLOAD =>
				if input_valid = '1' and input_ready_int = '1' then
					input_cnt <= input_cnt + 1;

					if input_last = '1' then
						input_cnt       <= (others => '0');
						data_read_state <= GET_HEADER;
					end if;
				end if;
			end case;
		end if;
	end if;
	end process pr_input_data_read;

end architecture rtl;
