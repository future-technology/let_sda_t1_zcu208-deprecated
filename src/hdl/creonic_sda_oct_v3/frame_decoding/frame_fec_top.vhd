--!
--! Copyright (C) 2021 Creonic GmbH
--!
--! @file
--! @brief  Example file which contain both frane encoder and decoder
--!         This file is for post synthesis simulation testing only
--! @author Nhan Nguyen
--! @date   2021/02/25
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_encoding;
library frame_decoding;

entity frame_fec_top is
	port (
		--ldpc_clk : in  std_logic; -- not used
		clk      : in  std_logic;
		rst      : in  std_logic;

		ldpc_clk          : in  std_logic;  -- 230M
		ldpc_rst          : in  std_logic;

		preamble               : in  std_logic_vector(63 downto 0);

		-- configuration for the preamble sync algorithm
		cfg_acquisition_length : in std_logic_vector(31 downto 0);
		cfg_tracking_length    : in std_logic_vector(31 downto 0);
		cfg_max_tracking_error : in std_logic_vector(31 downto 0);
        cfg_preamble_max_corr  : in std_logic_vector(6 downto 0);
		
		tx_axis_ctrl_tready : out std_logic;
		tx_axis_ctrl_tvalid : in  std_logic;
		tx_axis_ctrl_tdata  : in  std_logic_vector(2 downto 0);

		-- TX data input - header and payload data
		----------------------
		tx_axis_tready         : out std_logic;
		tx_axis_tvalid         : in  std_logic;
		tx_axis_tlast          : in  std_logic;
		tx_axis_tdata          : in  std_logic_vector(7  downto 0);

		-- Output data handling
		-----------------------
		ch_tx_axis_tready      : in  std_logic;
		ch_tx_axis_tvalid      : out std_logic;
		ch_tx_axis_tlast       : out std_logic;
		ch_tx_axis_tdata       : out std_logic_vector(7  downto 0);

		ch_rx_axis_ctrl_tready : out std_logic;
		ch_rx_axis_ctrl_tvalid : in  std_logic;
		ch_rx_axis_ctrl_tdata  : in  std_logic_vector(2  downto 0);

		-- RX data input
		----------------------
		ch_rx_axis_tvalid      : in  std_logic;
		ch_rx_axis_tdata       : in  std_logic_vector(7  downto 0);

		-- Status
		-----------------------
		crc16_valid            : out std_logic;
		crc16_correct          : out std_logic;
		crc32_valid            : out std_logic;
		crc32_correct          : out std_logic;

		preamble_synced        : out std_logic;

		-- More diagnostic informations
		-------------------------------------------------------
		clear_stat             : in  std_logic;
		total_frame_counter    : out std_logic_vector(31 downto 0);
		crc_error_counter      : out std_logic_vector(31 downto 0);
		crc16_err_counter      : out std_logic_vector(31 downto 0);
		crc32_err_counter      : out std_logic_vector(31 downto 0);

		-- Stats from the preamble sync
		new_search_counter     : out std_logic_vector(31 downto 0);
		sync_loss_counter      : out std_logic_vector(31 downto 0);

		-- RX data output - header and payload data
		-----------------------
		rx_axis_tready         : in  std_logic;
		rx_axis_tvalid         : out std_logic;
		rx_axis_tlast          : out std_logic;
		rx_axis_tdata          : out std_logic_vector(7  downto 0)
	);
end entity frame_fec_top;


architecture rtl of frame_fec_top is

	constant USE_XILINX_FEC : boolean := false;

begin

	inst_frame_encoding: entity frame_encoding.frame_encoding_top
	generic map(
		USE_XILINX_FEC => USE_XILINX_FEC
	)
	port map(
		ldpc_clk => ldpc_clk,
		ldpc_rst => ldpc_rst,
		clk      => clk,
		rst      => rst,

		preamble => preamble,

		s_axis_ctrl_tready => tx_axis_ctrl_tready,
		s_axis_ctrl_tvalid => tx_axis_ctrl_tvalid,
		s_axis_ctrl_tdata  => tx_axis_ctrl_tdata,

		-- Input data handling
		----------------------
		s_axis_tready => tx_axis_tready,
		s_axis_tvalid => tx_axis_tvalid,
		s_axis_tlast  => tx_axis_tlast,
		s_axis_tdata  => tx_axis_tdata,

		-- Output data handling
		-----------------------
		m_axis_tready => ch_tx_axis_tready,
		m_axis_tvalid => ch_tx_axis_tvalid,
		m_axis_tlast  => ch_tx_axis_tlast,
		m_axis_tdata  => ch_tx_axis_tdata
	);

	inst_frame_decoding: entity frame_decoding.frame_decoding_top
	generic map(
		USE_XILINX_FEC => USE_XILINX_FEC
	)
	port map(
		ldpc_clk => ldpc_clk,
		ldpc_rst => ldpc_rst,
		clk      => clk,
		rst      => rst,

		preamble => preamble,

		cfg_acquisition_length => cfg_acquisition_length,
		cfg_tracking_length    => cfg_tracking_length,
		cfg_max_tracking_error => cfg_max_tracking_error,
		cfg_preamble_max_corr  => cfg_preamble_max_corr,

		s_axis_ctrl_tready => ch_rx_axis_ctrl_tready,
		s_axis_ctrl_tvalid => ch_rx_axis_ctrl_tvalid,
		s_axis_ctrl_tdata  => ch_rx_axis_ctrl_tdata,

		-- Input data handling
		----------------------
		s_axis_tvalid  => ch_rx_axis_tvalid,
		s_axis_tdata   => ch_rx_axis_tdata,

		-- Status
		-----------------------
		crc16_valid       => crc16_valid,
		crc16_correct     => crc16_correct,
		crc32_valid       => crc32_valid,
		crc32_correct     => crc32_correct,

		preamble_synced  => preamble_synced,

		-- More diagnostic informations
		-------------------------------------------------------
		clear_stat          => clear_stat,
		total_frame_counter => total_frame_counter,
		crc_error_counter   => crc_error_counter,
		crc16_err_counter   => crc16_err_counter,
		crc32_err_counter   => crc32_err_counter,

		-- Stats from the preamble sync
		new_search_counter  => new_search_counter,
		sync_loss_counter   => sync_loss_counter,

		-- Output data handling
		-----------------------
		m_axis_tready  => rx_axis_tready,
		m_axis_tvalid  => rx_axis_tvalid,
		m_axis_tlast   => rx_axis_tlast,
		m_axis_tdata   => rx_axis_tdata
	);

end rtl;
