--!
--! Copyright (C) Creonic GmbH
--!
--! @file
--! @brief
--! @author Nhan Nguyen
--! @date   2020/02/28
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_decoding;
use frame_decoding.pkg_support.all;


entity frame_mux is
	port(
		clk : in std_logic;
		rst : in std_logic;

		header_ready : out std_logic;
		header_valid : in  std_logic;
		header_last  : in  std_logic;
		header_data  : in  std_logic_vector(7 downto 0);

		payload_ready : out std_logic;
		payload_valid : in  std_logic;
		payload_last  : in  std_logic;
		payload_data  : in  std_logic_vector(7 downto 0);

		frame_ready : in  std_logic;
		frame_valid : out std_logic;
		frame_last  : out std_logic;
		frame_data  : out std_logic_vector(7 downto 0)
	);
end entity frame_mux;

architecture rtl of frame_mux is

	type t_mux_fsm is (IDLE, GET_HEADER, GET_PAYLOAD);
	signal mux_fsm : t_mux_fsm;
	signal header_ready_int  : std_logic;
	signal payload_ready_int : std_logic;
	signal transmit_start    : std_logic;

	signal input_valid_reg : std_logic;
	signal input_last_reg  : std_logic;
	signal input_data_reg  : std_logic_vector(7 downto 0);

	signal buffer_data  : std_logic_vector(7 downto 0);
	signal buffer_valid : std_logic;
	signal buffer_last  : std_logic;
	signal buffer_ready : std_logic;

begin

	header_ready_int  <= buffer_ready when mux_fsm = GET_HEADER else '0';
	payload_ready_int <= buffer_ready when mux_fsm = GET_PAYLOAD else '0';
	header_ready  <= header_ready_int;
	payload_ready <= payload_ready_int;

	-- simple state machine for the mux
	pr_mux: process(clk) is
	begin
	if rising_edge(clk) then
		if rst = '1' then
			mux_fsm <= IDLE;
			transmit_start <= '0';

			input_valid_reg <= '0';
			input_last_reg  <= '0';
			input_data_reg  <= (others => '0');

			buffer_valid <= '0';
			buffer_last  <= '0';
			buffer_data  <= (others => '0');
		else
			buffer_valid <= '0';
			buffer_last  <= '0';
			buffer_data  <= (others => '0');

			if payload_valid = '1' then
				transmit_start <= '1';
			end if;

			case mux_fsm is
			when IDLE =>
				if transmit_start = '1' then
					mux_fsm <= GET_HEADER;
				end if;

				if input_last_reg = '1' then
					buffer_data  <= input_data_reg(1 downto 0) & "000000";
					buffer_valid <= '1';
					buffer_last  <= '1';
					input_valid_reg <= '0';
					input_last_reg  <= '0';
				end if;

			when GET_HEADER =>
				input_valid_reg <= '0';
				input_last_reg  <= '0';

				if header_valid = '1' and header_ready_int = '1' then
					input_valid_reg <= header_valid;
					input_data_reg  <= header_data;
					buffer_valid    <= input_valid_reg;
					buffer_last     <= input_last_reg;
					buffer_data     <= input_data_reg;

					-- Prepare data to be combined with payload (We need the 2 MSB's from last header byte)
					if header_last = '1' then
						mux_fsm        <= GET_PAYLOAD;
						input_data_reg <= header_data(5 downto 0) & header_data(7 downto 6);
					end if;
				end if;

			when GET_PAYLOAD =>
				input_valid_reg <= '0';
				input_last_reg  <= '0';

				-- Output data is shifted. 2 LSB's from previous value + 5 MSB's from input
				if payload_valid = '1' and payload_ready_int = '1' then
					input_data_reg <= payload_data;
					input_last_reg <= payload_last;
					buffer_valid   <= '1';
					buffer_data    <= input_data_reg(1 downto 0) & payload_data(7 downto 2);

					if payload_last = '1' then
						mux_fsm        <= IDLE;
						transmit_start <= '0';
					end if;
				end if;
			end case;
		end if;
	end if;
	end process pr_mux;


	-- Add axis buffer for cutting the combinational logic path
	inst_axis_buf : entity frame_decoding.axi4s_buffer
	generic map(
		DATA_WIDTH => 8
	)
	port map(

		clk => clk,
		rst => rst,

		-- Input data handling
		----------------------

		input_ready => buffer_ready,
		input_valid => buffer_valid,
		input_last  => buffer_last,
		input_data  => buffer_data,


		-- Output data handling
		-----------------------
		output_ready => frame_ready,
		output_valid => frame_valid,
		output_last  => frame_last,
		output_data  => frame_data
	);

end architecture rtl;
