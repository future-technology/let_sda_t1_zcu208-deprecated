--!
--! Copyright (C) 2020 Creonic GmbH
--!
--! @file
--! @brief
--! @author Nhan Nguyen
--! @date   2020/02/28
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_decoding;
use frame_decoding.pkg_support.all;


entity fso_framing is
	port(
		clk : in std_logic;
		rst : in std_logic;

		input_valid : in  std_logic;
		input_data  : in  std_logic_vector(7 downto 0);

		slip_en      : in  std_logic;
		bytes_slip   : in  unsigned(11 downto 0);
		bits_slip    : in  unsigned( 2 downto 0);
		frame_size   : in  unsigned(11 downto 0);
		slip_done    : out std_logic;

		output_valid : out std_logic;
		output_start : out std_logic;
		output_last  : out std_logic;
		output_data  : out std_logic_vector(7 downto 0)
	);
end entity fso_framing;

architecture rtl of fso_framing is

	signal byte_cnt       : unsigned(11 downto 0);
	signal bit_shift      : unsigned(2 downto 0);
	signal input_data_reg : std_logic_vector(7 downto 0);
	signal input_valid_d  : std_logic;
	signal slip_done_int  : std_logic;
	signal slip_en_d      : std_logic;
	signal bits_slip_d    : unsigned( 2 downto 0);
	signal bytes_slip_d   : unsigned(11 downto 0);
	signal frame_size_d   : unsigned(11 downto 0);

begin

	slip_done <= slip_done_int;

	-- simple state machine for the mux
	pr_fso_framing: process(clk) is
		variable v_bit_slip : unsigned(3 downto 0);
	begin
	if rising_edge(clk) then
		if rst = '1' then
			byte_cnt       <= (others => '0');
			input_data_reg <= (others => '0');
			bit_shift      <= (others => '0');
			output_data    <= (others => '0');
			output_valid   <= '0';
			output_start   <= '0';
			output_last    <= '0';
			input_valid_d  <= '0';
			slip_done_int  <= '0';
			slip_en_d      <= '0';
			bits_slip_d    <= (others => '0');
			bytes_slip_d   <= (others => '0');
			frame_size_d   <= (others => '0');
		else

			slip_en_d <= slip_en;
			frame_size_d <= frame_size;

			if slip_en = '1' and slip_en_d = '0' then
				v_bit_slip := ('0' & bit_shift) + bits_slip;
				bits_slip_d <= bits_slip + bit_shift;
				bytes_slip_d <= bytes_slip;

				if v_bit_slip > 7 then
					bytes_slip_d <= bytes_slip + 1;

					if bytes_slip = frame_size_d then
						bytes_slip_d <= (others => '0');
					end if;
				end if;
			end if;

			output_valid <= '0';
			output_start <= '0';
			output_last  <= '0';

			if slip_done_int = '1' and slip_en = '0' then
				slip_done_int <= '0';
			end if;

			if input_valid = '1' then
				input_valid_d  <= input_valid;
				input_data_reg <= input_data;

				if input_valid_d = '1' then
					output_valid <= '1';
					byte_cnt <= byte_cnt + 1;

					if byte_cnt = 0 then
						output_start <= '1';
					end if;

					if byte_cnt = frame_size_d then
						byte_cnt <= (others => '0');
						output_last <= '1';
					end if;

					if slip_en_d = '1' and slip_done_int = '0' then
						if (byte_cnt = bytes_slip_d - 1) or (byte_cnt = frame_size_d and bytes_slip_d = 0) then
							bit_shift <= bits_slip_d;
							byte_cnt  <= (others => '0');
							slip_done_int <= '1';
						end if;
					end if;

					case to_integer(bit_shift) is
					when 0 => output_data <= input_data_reg;
					when 1 => output_data <= input_data_reg(6 downto 0) & input_data(0);
					when 2 => output_data <= input_data_reg(5 downto 0) & input_data(7 downto 6);
					when 3 => output_data <= input_data_reg(4 downto 0) & input_data(7 downto 5);
					when 4 => output_data <= input_data_reg(3 downto 0) & input_data(7 downto 4);
					when 5 => output_data <= input_data_reg(2 downto 0) & input_data(7 downto 3);
					when 6 => output_data <= input_data_reg(1 downto 0) & input_data(7 downto 2);
					when others => output_data <= input_data_reg(0) & input_data(7 downto 1);
					end case;
				end if;
			end if;
		end if;
	end if;
	end process pr_fso_framing;

end architecture rtl;
