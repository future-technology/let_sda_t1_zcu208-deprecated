--!
--! Copyright (C) Creonic GmbH
--!
--! @file
--! @brief  Generic dual port RAM with one read and one write port
--! @author Carlos Velez
--! @date   2020/09/03
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_decoding;
use frame_decoding.pkg_support.all;


entity generic_simple_dp_ram is
	generic(
		DISTR_RAM     : boolean := false;
		ULTRA_RAM     : boolean := false;
		WORDS         : integer := 1024;
		BITWIDTH      : integer := 32;
		CASCADE       : integer := -1;
		PIPELINE_REGS : boolean := true;
		WR_REG        : boolean := false;
		RD_REG        : boolean := true
	);
	port(
		clk : in std_logic;
		rst : in std_logic;

		port_a_wen     : in  std_logic;
		port_a_addr    : in  std_logic_vector(no_bits_natural(WORDS - 1) - 1 downto 0);
		port_a_data_in : in  std_logic_vector(BITWIDTH - 1 downto 0);

		port_b_en             : in  std_logic;
		port_b_addr           : in  std_logic_vector(no_bits_natural(WORDS - 1) - 1 downto 0);
		port_b_data_out       : out std_logic_vector(BITWIDTH - 1 downto 0);
		port_b_data_out_valid : out std_logic
	);
end generic_simple_dp_ram;


architecture rtl of generic_simple_dp_ram is

	type t_ram is array(WORDS - 1 downto 0) of std_logic_vector(BITWIDTH - 1 downto 0);
	signal simple_dp_ram : t_ram;
	signal ram_data      : std_logic_vector(BITWIDTH - 1 downto 0);
	signal port_b_en_int : std_logic;

	signal port_a_wen_reg     : std_logic;
	signal port_a_addr_reg    : std_logic_vector(no_bits_natural(WORDS - 1) - 1 downto 0);
	signal port_a_data_in_reg : std_logic_vector(BITWIDTH - 1 downto 0);

	--vhdl_cover_off
	function get_ram_style_xilinx(dist_ram, ultra_ram : in boolean) return string is
	begin
		if ultra_ram then
			return "ultra";
		else
			if dist_ram then
				return "distributed";
			else
				return "block";
			end if;
		end if;
	end function;

	-- Determine the number of stages required in case ULTRARAM
	function get_pipeline_stages(act: in boolean; mem_size, cascade : in integer) return integer is
		variable v_num_rows, v_num_cols, v_res : integer;
	begin
		if act = false or cascade = -1 then
			return 0;
		else
			v_res := mem_size / 4096;
			if v_res = 0 then
				v_num_rows := 1;
				v_num_cols := 1;
			elsif v_res >= 0 and v_res <= cascade then
				v_num_rows := v_res;
				v_num_cols := 1;
			else
				v_num_rows := cascade;
				if (v_res mod cascade) = 0 then
					v_num_cols := v_res / cascade;
				else
					v_num_cols := 1 + (v_res - cascade);
				end if;
			end if;

			return (v_num_rows + v_num_cols);
		end if;
	end function;
	--vhdl_cover_on

	constant LEVELS_ULTRA : integer := get_pipeline_stages(PIPELINE_REGS, WORDS, CASCADE);

	attribute ram_style : string;
	attribute ram_style of simple_dp_ram : signal is get_ram_style_xilinx(DISTR_RAM, ULTRA_RAM);

	attribute cascade_height : integer;
	attribute cascade_height of simple_dp_ram: signal is CASCADE;

begin


	--
	-- (optional) Input Register
	--

	gen_wr_reg: if WR_REG generate
	begin

		-- Write data into extra register before passing it to the memory
		pr_wr_reg: process(clk)
		begin
		if rising_edge(clk) then
			if rst = '1' then
				port_a_wen_reg     <= '0';
				port_a_addr_reg    <= (others => '0');
				port_a_data_in_reg <= (others => '0');
			else
				port_a_wen_reg     <= port_a_wen;
				port_a_addr_reg    <= port_a_addr;
				port_a_data_in_reg <= port_a_data_in;
			end if;
		end if;
		end process pr_wr_reg;

	end generate;

	-- No input register => pass data to RAM directly
	gen_no_wr_reg: if not WR_REG generate
		port_a_wen_reg     <= port_a_wen;
		port_a_addr_reg    <= port_a_addr;
		port_a_data_in_reg <= port_a_data_in;
	end generate;


	--
	-- RAM content
	--

	-- Storage is port A
	pr_port_a: process(clk)
	begin
	if rising_edge(clk) then
		--creonic_lint off
		if port_a_wen_reg = '1' then
			simple_dp_ram(to_integer(unsigned(port_a_addr_reg))) <= port_a_data_in_reg;
		end if;
		--creonic_lint on
	end if;
	end process pr_port_a;

	-- Reading is port B
	pr_port_b: process(clk)
	begin
	if rising_edge(clk) then
		--creonic_lint off
		if port_b_en = '1' then
			ram_data <= simple_dp_ram(to_integer(unsigned(port_b_addr)));
		end if;
		--creonic_lint on
	end if;
	end process pr_port_b;

	-- Indicate when the data from the RAM read is ready
	pr_reg_enable: process(clk) is
	begin
	if rising_edge(clk) then
		if rst = '1' then
			port_b_en_int <= '0';
		else
			port_b_en_int <= port_b_en;
		end if;
	end if;
	end process pr_reg_enable;

	-- No Output Register
	gen_no_rd_reg: if not RD_REG generate
		port_b_data_out       <= ram_data;
		port_b_data_out_valid <= port_b_en_int;
	end generate;

	-- (Optional) Output Register
	gen_rd_reg: if RD_REG generate
		signal port_b_ram_data       : std_logic_vector(BITWIDTH - 1 downto 0);
		signal port_b_ram_valid      : std_logic;
		signal port_b_data_out_reg   : std_logic_vector(BITWIDTH - 1 downto 0);
		signal port_b_data_valid_reg : std_logic;
	begin

		-- Generate Pipeline Stages (Strongly Recommended for ULTRA_RAM)
		gen_ultra_pipeline: if ULTRA_RAM = true and LEVELS_ULTRA > 0 generate
			type t_data_array is array(natural range<>) of std_logic_vector(BITWIDTH - 1 downto 0);
			signal ram_data_pipe : t_data_array(LEVELS_ULTRA - 1 downto 0);    -- Pipelines for Memory
			signal enable_pipe   : std_logic_vector(LEVELS_ULTRA downto 0);
		begin

			-- Pipeline Registers
			pr_pipeline : process(clk)
			begin
			if rising_edge(clk) then
				--creonic_lint off
				enable_pipe(0) <= port_b_en;
				for i in 0 to LEVELS_ULTRA - 1 loop
					enable_pipe(i + 1) <= enable_pipe(i);
				end loop;

				if enable_pipe(0) = '1' then
					ram_data_pipe(0) <= ram_data;
				end if;
				for i in 0 to LEVELS_ULTRA - 2 loop
					if(enable_pipe(i + 1) = '1') then
						ram_data_pipe(i + 1) <= ram_data_pipe(i);
					end if;
				end loop;
				--creonic_lint on
			end if; -- clk
			end process pr_pipeline;

			port_b_ram_data  <= ram_data_pipe(LEVELS_ULTRA - 1);
			port_b_ram_valid <= enable_pipe(LEVELS_ULTRA);
		end generate;

		-- Do not generate Pipeline
		gen_no_pipeline: if ULTRA_RAM = false or LEVELS_ULTRA = 0 generate
			port_b_ram_data  <= ram_data;
			port_b_ram_valid <= port_b_en_int;
		end generate;

		-- Output Register
		pr_out: process(clk)
		begin
		if rising_edge(clk) then
			if rst = '1' then
				port_b_data_out_reg   <= (others => '0');
				port_b_data_valid_reg <= '0';
			else
				port_b_data_valid_reg <= port_b_ram_valid;
				if port_b_ram_valid = '1' then
					port_b_data_out_reg <= port_b_ram_data;
				end if;
			end if;
		end if;
		end process pr_out;

		port_b_data_out       <= port_b_data_out_reg;
		port_b_data_out_valid <= port_b_data_valid_reg;
	end generate;

end architecture rtl;
