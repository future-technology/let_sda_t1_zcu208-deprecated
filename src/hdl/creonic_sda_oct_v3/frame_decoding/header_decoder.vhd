--!
--! Copyright (C) Creonic GmbH
--!
--! @file
--! @brief
--! @author Carlos Velez
--! @date   2021/11/16
--!
--! The encoder expects the output ready to be asserted during the transmission
--! of a whole block.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_decoding;
use frame_decoding.pkg_support.all;

library dec_viterbi;
use dec_viterbi.pkg_param.all;


entity header_decoder is
port (
	clk : in  std_logic;
	rst : in  std_logic;

	-- Input data handling
	----------------------

	input_ready  : out std_logic;
	input_valid  : in  std_logic;
	input_last   : in  std_logic;
	input_data   : in  std_logic_vector(7 downto 0);

	-- Output data handling
	-----------------------
	output_ready : in  std_logic;
	output_valid : out std_logic;
	output_last  : out std_logic;
	output_data  : out std_logic_vector(7 downto 0);

	-- Configures window length and acquisition length.
	--------------------------------------------------
	cfg_ready : out std_logic;
	cfg_valid : in  std_logic;
	cfg_data  : in  std_logic_vector(31 downto 0)
);
end entity header_decoder;


architecture rtl of header_decoder is

	signal rst_n : std_logic;

	signal bits_to_map_ready    : std_logic;
	signal bits_to_map_valid    : std_logic;
	signal bits_to_map_last     : std_logic;
	signal bits_to_map_last_reg : std_logic;
	signal bits_to_map          : std_logic_vector(7 downto 0);
	signal bits_to_map_reg      : std_logic_vector(7 downto 0);

	signal input_dec_ready : std_logic;
	signal input_dec_valid : std_logic;
	signal input_dec_last  : std_logic;
	signal input_dec_data  : std_logic_vector(47 downto 0);

	signal input_dec_buf_ready     : std_logic;
	signal input_dec_buf_ready_int : std_logic;
	signal input_dec_buf_valid     : std_logic;
	signal input_dec_buf_valid_int : std_logic;
	signal input_dec_buf_last      : std_logic;
	signal input_dec_buf_data      : std_logic_vector(47 downto 0);

	signal output_dec_ready : std_logic;
	signal output_dec_valid : std_logic;
	signal output_dec_data  : std_logic;
	signal output_dec_last  : std_logic;

	signal output_buffer_valid : std_logic;
	signal output_buffer_last  : std_logic;
	signal output_buffer_data  : std_logic_vector(7 downto 0);

	signal output_byte_ready : std_logic;
	signal output_byte_valid : std_logic;
	signal output_byte_last  : std_logic;
	signal output_byte_data  : std_logic_vector(7 downto 0);

	signal output_ready_int : std_logic;
	signal output_valid_int : std_logic;
	signal output_last_int  : std_logic;
	signal output_data_int  : std_logic_vector(7 downto 0);

	signal bit_pos  : unsigned(2 downto 0);
	signal byte_cnt : unsigned(1 downto 0);
	signal read_buf_dec : std_logic;
	signal buf_out_cnt : unsigned(7 downto 0);

	signal buf_dec_din_last   : std_logic;
	signal buf_dec_dout_last  : std_logic;
	signal buf_byte_din_last  : std_logic;
	signal buf_byte_dout_last : std_logic;
begin

	rst_n <= not rst;

	buf_dec_din_last   <= input_dec_ready and input_dec_valid and input_dec_last;
	buf_dec_dout_last  <= input_dec_buf_ready and input_dec_buf_valid and input_dec_buf_last;
	buf_byte_din_last  <= output_byte_ready and output_byte_valid and output_byte_last;
	buf_byte_dout_last <= output_ready_int and output_valid_int and output_last_int;

	input_dec_buf_ready_int <= input_dec_buf_ready when read_buf_dec = '1' else '0';
	input_dec_buf_valid_int <= input_dec_buf_valid when read_buf_dec = '1' else '0';

	output_ready_int <= output_ready     when buf_out_cnt > 0 else '0';
	output_valid     <= output_valid_int when buf_out_cnt > 0 else '0';
	output_last      <= output_last_int  when buf_out_cnt > 0 else '0';
	output_data      <= output_data_int;

	-- Store input (allow to keep the ready high for the data)
	inst_input_fifo : entity frame_decoding.axi4s_fifo
	generic map (
		DISTR_RAM         => false,
		FIFO_DEPTH        => 512,
		DATA_WIDTH        => 8,
		FULL_THRESHOLD    => 512,
		USE_OUTPUT_BUFFER => false
	)
	port map (
		clk => clk,
		rst => rst,

		input_ready       => input_ready,
		input_valid       => input_valid,
		input_last        => input_last,
		input_data        => input_data,
		input_almost_full => open,

		output_ready => bits_to_map_ready,
		output_valid => bits_to_map_valid,
		output_last  => bits_to_map_last,
		output_data  => bits_to_map
	);

	-- Store LLRs before passing to viterbi_decoder (works with burst)
	inst_burst_fifo : entity frame_decoding.axi4s_fifo
	generic map (
		DISTR_RAM         => false,
		FIFO_DEPTH        => 512,
		DATA_WIDTH        => 48,
		FULL_THRESHOLD    => 512,
		USE_OUTPUT_BUFFER => false
	)
	port map (
		clk => clk,
		rst => rst,

		input_ready       => input_dec_ready,
		input_valid       => input_dec_valid,
		input_last        => input_dec_last,
		input_data        => input_dec_data,
		input_almost_full => open,

		output_ready => input_dec_buf_ready_int,
		output_valid => input_dec_buf_valid,
		output_last  => input_dec_buf_last,
		output_data  => input_dec_buf_data
	);

	-- Viterbi Decoder
	inst_dec_viterbi : entity dec_viterbi.dec_viterbi_top
	port map(
		aclk    => clk,
		aresetn => rst_n,

		s_axis_input_tvalid  => input_dec_buf_valid_int,
		s_axis_input_tdata   => input_dec_buf_data,
		s_axis_input_tlast   => input_dec_buf_last,
		s_axis_input_tready  => input_dec_buf_ready,

		m_axis_output_tvalid => output_dec_valid,
		m_axis_output_tdata  => output_dec_data,
		m_axis_output_tlast  => output_dec_last,
		m_axis_output_tready => output_dec_ready,

		s_axis_ctrl_tvalid   => cfg_valid,
		s_axis_ctrl_tdata    => cfg_data,
		s_axis_ctrl_tready   => cfg_ready
	);

	-- Store output (decoder output bits and we assign to bytes)
	inst_out_fifo : entity frame_decoding.axi4s_fifo
	generic map (
		DISTR_RAM         => false,
		FIFO_DEPTH        => 512,
		DATA_WIDTH        => 8,
		FULL_THRESHOLD    => 512,
		USE_OUTPUT_BUFFER => false
	)
	port map (
		clk => clk,
		rst => rst,

		input_ready       => output_byte_ready,
		input_valid       => output_byte_valid,
		input_last        => output_byte_last,
		input_data        => output_byte_data,
		input_almost_full => open,

		output_ready => output_ready_int,
		output_valid => output_valid_int,
		output_last  => output_last_int,
		output_data  => output_data_int
	);


	-- Process to map the coded bits into LLR values
	pr_mapper : process(clk)
		variable v_mapper: std_logic_vector(5 downto 0);
	begin
	if rising_edge(clk) then
		if rst = '1' then
			input_dec_valid <= '0';
			input_dec_last  <= '0';
			input_dec_data  <= (others => '0');

			bits_to_map_ready    <= '1';
			bits_to_map_last_reg <= '0';
			bits_to_map_reg      <= (others => '0');

			byte_cnt <= (others => '0');
		else
			if input_dec_ready = '1' then
				input_dec_data  <= (others => '0');
				input_dec_valid <= '0';
				input_dec_last  <= '0';

				if bits_to_map_valid = '1' or byte_cnt = 3 then
					byte_cnt <= byte_cnt + 1;
					bits_to_map_reg <= bits_to_map;
					bits_to_map_last_reg <= bits_to_map_last;

					if byte_cnt = 2 then
						bits_to_map_ready <= '0';
					else
						bits_to_map_ready <= '1';
					end if;

					case( to_integer(byte_cnt) ) is
						when 0 => v_mapper := bits_to_map(7 downto 2);
						when 1 => v_mapper := bits_to_map_reg(1 downto 0) & bits_to_map(7 downto 4);
						when 2 => v_mapper := bits_to_map_reg(3 downto 0) & bits_to_map(7 downto 6);
						when others => v_mapper := bits_to_map_reg(5 downto 0);
					end case;

					for i in NUMBER_PARITY_BITS - 1 downto 0 loop
						if v_mapper(NUMBER_PARITY_BITS - 1 - i) = '1' then
							input_dec_data(8*(i + 1) - 1 downto 8*i) <= "00001000";
						else
							input_dec_data(8*(i + 1) - 1 downto 8*i) <= "00000111";
						end if;
					end loop;

					input_dec_valid <= '1';
					input_dec_last  <= bits_to_map_last_reg;
				end if;
			end if;
		end if; -- rst
	end if; -- clk
	end process pr_mapper;


	-- Process controlling the read signals for buffers
	pr_read_buffers : process(clk)
	begin
	if rising_edge(clk) then
		if rst = '1' then
			read_buf_dec <= '0';
			buf_out_cnt  <= (others => '0');
		else
			if buf_dec_din_last = '1' then
				read_buf_dec <= '1';
			elsif buf_dec_dout_last = '1' then
				read_buf_dec <= '0';
			end if;

			if buf_byte_din_last = '1' and buf_byte_dout_last = '0' then
				buf_out_cnt <= buf_out_cnt + 1;
			elsif buf_byte_din_last = '0' and buf_byte_dout_last = '1' then
				buf_out_cnt <= buf_out_cnt - 1;
			end if;
		end if; -- rst
	end if; -- clk
	end process pr_read_buffers;


	-- Process to output individual bits into bytes
	pr_output : process(clk)
	begin
	if rising_edge(clk) then
		if rst = '1' then
			bit_pos             <= (others => '1');
			output_byte_data    <= (others => '0');
			output_buffer_data  <= (others => '0');
			output_byte_last    <= '0';
			output_byte_valid   <= '0';
			output_buffer_valid <= '0';
			output_buffer_last  <= '0';
			output_dec_ready    <= '1';
		else
			output_dec_ready <= output_byte_ready;

			-- Handle the output from component
			if output_byte_ready = '1' then
				output_byte_valid <= '0';
				output_byte_last  <= '0';
				output_byte_data  <= (others => '0');

				if output_buffer_valid = '1' then
					output_byte_valid <= output_buffer_valid;
					output_byte_last  <= output_buffer_last;
					output_byte_data  <= output_buffer_data;

					output_buffer_valid <= '0';
					output_buffer_last  <= '0';
					output_buffer_data  <= (others => '0');
				end if;
			end if;

			-- Handle data from decoder (convert from bit to byte)
			if output_dec_ready = '1' and output_dec_valid = '1' then
				if bit_pos = 7 then
					output_buffer_data <= output_dec_data & "0000000";
				else
					output_buffer_data(to_integer(bit_pos)) <= output_dec_data;
				end if;

				if bit_pos = 0 or output_dec_last = '1' then
					bit_pos <= (others => '1');
					output_buffer_valid <= '1';
				else
					bit_pos <= bit_pos - 1;
				end if;

				output_buffer_last <= output_dec_last;
			end if;
		end if; -- rst
	end if; -- clk
	end process pr_output;

end architecture rtl;
