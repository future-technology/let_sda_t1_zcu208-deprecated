--!
--! Copyright (C) 2021 Creonic GmbH
--!
--! @file
--! @brief
--! @author Nhan Nguyen
--! @date   2015/04/30
--!
--! The encoder expects the output ready to be asserted during the transmission
--! of a whole block.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library xilinx_ip;
use xilinx_ip.pkg_components.all;
library frame_decoding;

entity payload_decoder is
	generic (
		USE_XILINX_FEC : boolean := true
	);
	port (
		core_clk : in  std_logic;
		core_rst : in  std_logic;
		clk      : in  std_logic;
		rst      : in  std_logic;

		-- Control
		---------------------------
		ctrl_ready : out std_logic;
		ctrl_valid : in  std_logic;
		ctrl_data  : in  std_logic_vector(2 downto 0);
		ctrl_user  : in  std_logic_vector(5 downto 0);

		-- Input data handling
		----------------------

		input_ready : out std_logic;
		input_valid : in  std_logic;
		input_last  : in  std_logic;
		input_data  : in  std_logic_vector(7 downto 0);

		-- Status
		---------------------------
		status_ready : in  std_logic;
		status_valid : out std_logic;
		status_data  : out std_logic_vector(63 downto 0);

		-- Output data handling
		-----------------------
		output_ready : in  std_logic;
		output_valid : out std_logic;
		output_last  : out std_logic;
		output_data  : out std_logic_vector(7 downto 0)
	);
end entity payload_decoder;


architecture rtl of payload_decoder is

	component ldpc_dec is
	port (
		reset_n    : in  std_logic;
		core_clk   : in  std_logic;
		s_axi_aclk : in  std_logic;
		interrupt  : out std_logic;

		s_axis_ctrl_aclk   : in  std_logic;
		s_axis_ctrl_tready : out std_logic;
		s_axis_ctrl_tvalid : in  std_logic;
		s_axis_ctrl_tdata  : in  std_logic_vector(39 downto 0);

		s_axis_din_aclk   : in  std_logic;
		s_axis_din_tready : out std_logic;
		s_axis_din_tvalid : in  std_logic;
		s_axis_din_tlast  : in  std_logic;
		s_axis_din_tdata  : in  std_logic_vector(127 downto 0);

		m_axis_status_aclk   : in  std_logic;
		m_axis_status_tready : in  std_logic;
		m_axis_status_tvalid : out std_logic;
		m_axis_status_tdata  : out std_logic_vector(39 downto 0);

		m_axis_dout_aclk   : in  std_logic;
		m_axis_dout_tready : in  std_logic;
		m_axis_dout_tvalid : out std_logic;
		m_axis_dout_tlast  : out std_logic;
		m_axis_dout_tdata  : out std_logic_vector(127 downto 0)
	);
	end component ldpc_dec;

	constant R084_22_26 : std_logic_vector(7 - 1 downto 0) := "0000000";
	constant R075_22_29 : std_logic_vector(7 - 1 downto 0) := "0000011";
	constant R066_22_33 : std_logic_vector(7 - 1 downto 0) := "0000111";
	constant R050_22_44 : std_logic_vector(7 - 1 downto 0) := "0010010";

	constant LLR_1 : std_logic_vector(7 downto 0) := "00010000";
	constant LLR_0 : std_logic_vector(7 downto 0) := "11110000";
	signal input_cnt : unsigned(6 downto 0);

	signal rst_n         : std_logic;
	signal apply_new_cfg : std_logic;
	signal bypass_ldpc   : std_logic;

	signal ctrl_buf_ready : std_logic;
	signal ctrl_buf_valid : std_logic;
	signal ctrl_buf_data  : std_logic_vector(8 downto 0);
	signal ctrl_data_int  : std_logic_vector(8 downto 0);

	signal ctrl_ldpc_ready : std_logic;
	signal ctrl_ldpc_valid : std_logic;
	signal ctrl_ldpc_data  : std_logic_vector(63 downto 0);

	signal input_frame_buf_ready : std_logic;
	signal input_frame_buf_valid : std_logic;
	signal input_frame_buf_last  : std_logic;
	signal input_frame_buf_data  : std_logic_vector(7 downto 0);

	signal input_frame_fifo_ready : std_logic;
	signal input_frame_fifo_valid : std_logic;
	signal input_frame_fifo_last  : std_logic;
	signal input_frame_fifo_data  : std_logic_vector(7 downto 0);

	signal input_frame_ldpc_ready : std_logic;
	signal input_frame_ldpc_valid : std_logic;
	signal input_frame_ldpc_last  : std_logic;
	signal input_frame_ldpc_data  : std_logic_vector(7 downto 0);

	signal input_frame_buf_ldpc_ready : std_logic;
	signal input_frame_buf_ldpc_valid : std_logic;
	signal input_frame_buf_ldpc_last  : std_logic;
	signal input_frame_buf_ldpc_data  : std_logic_vector(7 downto 0);

	signal input_frame_llr_ldpc_ready : std_logic;
	signal input_frame_llr_ldpc_valid : std_logic;
	signal input_frame_llr_ldpc_last  : std_logic;
	signal input_frame_llr_ldpc_data  : std_logic_vector(127 downto 0);

	signal output_frame_fifo_ready : std_logic;
	signal output_frame_fifo_valid : std_logic;
	signal output_frame_fifo_last  : std_logic;
	signal output_frame_fifo_data  : std_logic_vector(7 downto 0);

	signal output_frame_ldpc_ready : std_logic;
	signal output_frame_ldpc_valid : std_logic;
	signal output_frame_ldpc_last  : std_logic;
	signal output_frame_ldpc_data  : std_logic_vector(127 downto 0);

	signal output_int_ready : std_logic;
	signal output_int_valid : std_logic;
	signal output_int_last  : std_logic;
	signal output_int_data  : std_logic_vector(7 downto 0);

	signal status_ready_buf : std_logic;
	signal status_ready_dec : std_logic;
	signal status_valid_buf : std_logic;
	signal status_data_buf  : std_logic_vector(63 downto 0);

	type t_ctrl_state is (IDLE, WAIT_LAST);
	signal ctrl_in_state : t_ctrl_state;

begin

	rst_n          <= not rst;
	ctrl_buf_ready <= not apply_new_cfg;
	ctrl_data_int  <= ctrl_data & ctrl_user;

	input_frame_buf_ldpc_ready <= input_frame_llr_ldpc_ready;

	-- Demux at the input
	input_frame_buf_ready <= input_frame_ldpc_ready when bypass_ldpc = '0' and apply_new_cfg = '1' else
	                         input_frame_fifo_ready when bypass_ldpc = '1' and apply_new_cfg = '1' else
									 '0';

	-- Mux at the output
	output_frame_ldpc_ready <= output_int_ready when bypass_ldpc = '0' else '1';
	status_ready_dec        <= status_ready_buf when bypass_ldpc = '0' else '1';
	output_frame_fifo_ready <= output_int_ready when bypass_ldpc = '1' else '0';

	output_int_valid <= output_frame_fifo_valid when bypass_ldpc = '1' else output_frame_ldpc_valid;
	output_int_last  <= output_frame_fifo_last  when bypass_ldpc = '1' else output_frame_ldpc_last;
	output_int_data  <= output_frame_fifo_data  when bypass_ldpc = '1' else output_frame_ldpc_data(7 downto 0);

	-- Pass data from axi4s_buffer to bypass(data_fifo) and ldpc_fifo
	pr_din_fifo : process(clk)
	begin
	if rising_edge(clk) then
		if rst = '1' then
			input_frame_fifo_valid <= '0';
			input_frame_fifo_last  <= '0';
			input_frame_fifo_data  <= (others => '0');
			input_frame_ldpc_valid <= '0';
			input_frame_ldpc_last  <= '0';
			input_frame_ldpc_data  <= (others => '0');
		else

			-- bypass(frame_fifo)
			if input_frame_fifo_ready = '1' then
				input_frame_fifo_valid <= '0';
				input_frame_fifo_last  <= '0';
				input_frame_fifo_data  <= (others => '0');

				if input_frame_buf_valid = '1' then
					input_frame_fifo_valid <= bypass_ldpc;
					input_frame_fifo_last  <= input_frame_buf_last;
					input_frame_fifo_data  <= input_frame_buf_data;
				end if;
			end if;

			-- ldpc_fifo
			if input_frame_ldpc_ready = '1' then
				input_frame_ldpc_valid <= '0';
				input_frame_ldpc_last  <= '0';
				input_frame_ldpc_data  <= (others => '0');

				if input_frame_buf_valid = '1' then
					input_frame_ldpc_valid <= not bypass_ldpc;
					input_frame_ldpc_last  <= input_frame_buf_last;
					input_frame_ldpc_data  <= input_frame_buf_data;
				end if;
			end if;
		end if; -- rst
	end if; -- clk
	end process pr_din_fifo;

	-- Mapping hard bits to LLRs and puncturing
	pr_mapper : process(clk)
	begin
	if rising_edge(clk) then
		if rst = '1' then
			input_frame_llr_ldpc_valid <= '0';
			input_frame_llr_ldpc_last  <= '0';
			input_frame_llr_ldpc_data  <= (others => '0');
			input_cnt                  <= (others => '0');
		else
			if input_frame_llr_ldpc_ready = '1' then
				input_frame_llr_ldpc_valid <= '0';
				input_frame_llr_ldpc_last  <= '0';
				input_frame_llr_ldpc_data  <= (others => '0');

				if input_frame_buf_ldpc_valid = '1' then
					for i in 7 downto 0 loop
						if USE_XILINX_FEC then
							if input_frame_buf_ldpc_data(i) = '1' then
								input_frame_llr_ldpc_data(8*(i+1) - 1 downto 8*i) <= LLR_1;
							else
								input_frame_llr_ldpc_data(8*(i+1) - 1 downto 8*i) <= LLR_0;
							end if;
						else

							-- Creonic LDPC Decoder use different LLR formular
							if input_frame_buf_ldpc_data(i) = '1' then
								input_frame_llr_ldpc_data(8*(i+1) - 1 downto 8*i) <= LLR_0;
							else
								input_frame_llr_ldpc_data(8*(i+1) - 1 downto 8*i) <= LLR_1;--(others => '0');
							end if;
						end if;
					end loop;

					input_frame_llr_ldpc_valid <= '1';
					input_frame_llr_ldpc_last  <= input_frame_buf_ldpc_last;

				end if;
			end if;
		end if; -- rst
	end if; -- clk
	end process pr_mapper;


	gen_xilinx_fec : if USE_XILINX_FEC generate

		signal ctrl_ldpc_data_xilinx : std_logic_vector(39 downto 0);
		signal status_data_xilinx    : std_logic_vector(39 downto 0);

	begin

		-- control the cfg
		pr_ctrl_in : process(clk)
		begin
		if rising_edge(clk) then
			if rst = '1' then
				ctrl_ldpc_valid <= '0';
				ctrl_ldpc_data  <= (others => '0');
				ctrl_in_state   <= IDLE;
				apply_new_cfg   <= '0';
				bypass_ldpc     <= '0';
			else
				case ctrl_in_state is
					when IDLE =>
						if ctrl_buf_ready = '1' and ctrl_buf_valid = '1' then
							apply_new_cfg <= '1';
							bypass_ldpc   <= '1';
							ctrl_in_state <= WAIT_LAST;
							ctrl_ldpc_data(39 downto 38) <= "00";
							ctrl_ldpc_data(31 downto 24) <= x"00"; -- id
							ctrl_ldpc_data(23 downto 18) <= ctrl_buf_data(5 downto 0); -- iterations
							ctrl_ldpc_data(17) <= '1'; --term_on_no_change
							ctrl_ldpc_data(16) <= '1'; --term_on_pass
							ctrl_ldpc_data(15) <= '0'; --include_parity_op
							ctrl_ldpc_data(14) <= '1'; --hard_op
							ctrl_ldpc_data(13) <= '0'; --reserved
							ctrl_ldpc_data(12 downto 9) <= std_logic_vector(to_unsigned(12, 4)); --sc_idx
							ctrl_ldpc_data( 8 downto 6)  <= std_logic_vector(to_unsigned(0, 3)); --bg
							ctrl_ldpc_data( 5 downto 3)  <= std_logic_vector(to_unsigned(1, 3)); --z_set
							ctrl_ldpc_data( 2 downto 0)  <= std_logic_vector(to_unsigned(7, 3)); --z_j

							if unsigned(ctrl_buf_data(8 downto 6)) > 0 then
								ctrl_ldpc_valid <= '1';
								bypass_ldpc     <= '0';

								case to_integer(unsigned(ctrl_buf_data(8 downto 6))) is
									when 1 => ctrl_ldpc_data(37 downto 32) <= std_logic_vector(to_unsigned( 4, 6));
									when 2 => ctrl_ldpc_data(37 downto 32) <= std_logic_vector(to_unsigned( 7, 6));
									when 3 => ctrl_ldpc_data(37 downto 32) <= std_logic_vector(to_unsigned(11, 6));
									when others => ctrl_ldpc_data(37 downto 32) <= std_logic_vector(to_unsigned(22, 6));
								end case;
							end if;
						end if;

					when WAIT_LAST =>
						if ctrl_ldpc_ready = '1' then
							ctrl_ldpc_valid <= '0';
							ctrl_ldpc_data  <= (others => '0');
						end if;

						if input_frame_buf_ready = '1' and input_frame_buf_valid = '1' and input_frame_buf_last = '1' then
							apply_new_cfg <= '0';
							ctrl_in_state <= IDLE;
						end if;
				end case;

			end if; -- rst
		end if; -- clk
		end process pr_ctrl_in;

		ctrl_ldpc_data_xilinx        <= ctrl_ldpc_data(39 downto 0);
		status_data_buf(39 downto 0) <= status_data_xilinx;

		-- SD-FEC Instantiation
		inst_dec_ldpc: ldpc_dec
		port map(
			reset_n  => rst_n,
			core_clk => core_clk,

			s_axi_aclk => clk,
			interrupt  => open,

			s_axis_ctrl_aclk   => clk,
			s_axis_ctrl_tready => ctrl_ldpc_ready,
			s_axis_ctrl_tvalid => ctrl_ldpc_valid,
			s_axis_ctrl_tdata  => ctrl_ldpc_data_xilinx,

			s_axis_din_aclk   => clk,
			s_axis_din_tready => input_frame_llr_ldpc_ready,
			s_axis_din_tvalid => input_frame_llr_ldpc_valid,
			s_axis_din_tlast  => input_frame_llr_ldpc_last,
			s_axis_din_tdata  => input_frame_llr_ldpc_data,

			m_axis_status_aclk   => clk,
			m_axis_status_tready => status_ready_dec,
			m_axis_status_tvalid => status_valid_buf,
			m_axis_status_tdata  => status_data_xilinx,

			m_axis_dout_aclk   => clk,
			m_axis_dout_tready => output_frame_ldpc_ready,
			m_axis_dout_tvalid => output_frame_ldpc_valid,
			m_axis_dout_tlast  => output_frame_ldpc_last,
			m_axis_dout_tdata  => output_frame_ldpc_data
		);
	end generate;

	-- Use Creonic FEC
	gen_creonic_fec : if not USE_XILINX_FEC generate

		signal input_frame_llr_ldpc_data_creonic  : std_logic_vector(63 downto 0);
		signal output_frame_ldpc_data_creonic : std_logic_vector(7 downto 0);

	begin

		-- control the cfg
		pr_ctrl_in : process(clk)
		begin
		if rising_edge(clk) then
			if rst = '1' then
				ctrl_ldpc_valid <= '0';
				ctrl_ldpc_data  <= (others => '0');
				ctrl_in_state   <= IDLE;
				apply_new_cfg   <= '0';
				bypass_ldpc     <= '0';
			else
				case ctrl_in_state is
					when IDLE =>
						if ctrl_buf_ready = '1' and ctrl_buf_valid = '1' then
							apply_new_cfg <= '1';
							bypass_ldpc   <= '1';
							ctrl_in_state <= WAIT_LAST;
							ctrl_ldpc_data(63 downto 37) <= (others => '0');
							ctrl_ldpc_data(36 downto 31) <= ctrl_buf_data(5 downto 0);
							ctrl_ldpc_data(30 downto 22) <= std_logic_vector(to_unsigned(384, 9));

							if unsigned(ctrl_buf_data(8 downto 6)) > 0 then
								ctrl_ldpc_valid <= '1';
								bypass_ldpc     <= '0';

								case to_integer(unsigned(ctrl_buf_data(8 downto 6))) is
								when 1 =>
									ctrl_ldpc_data(14 downto 0)  <= std_logic_vector(to_unsigned(26 * 384, 15));
									ctrl_ldpc_data(21 downto 15) <= R084_22_26;
								when 2 =>
									ctrl_ldpc_data(14 downto 0)  <= std_logic_vector(to_unsigned(29 * 384, 15));
									ctrl_ldpc_data(21 downto 15) <= R075_22_29;
								when 3 =>
									ctrl_ldpc_data(14 downto 0)  <= std_logic_vector(to_unsigned(33 * 384, 15));
									ctrl_ldpc_data(21 downto 15) <= R066_22_33;
								when others =>
									ctrl_ldpc_data(14 downto 0)  <= std_logic_vector(to_unsigned(44 * 384, 15));
									ctrl_ldpc_data(21 downto 15) <= R050_22_44;
								end case;
							end if;
						end if;

					when WAIT_LAST =>
						if ctrl_ldpc_ready = '1' then
							ctrl_ldpc_valid <= '0';
							ctrl_ldpc_data  <= (others => '0');
						end if;

						if input_frame_buf_ready = '1' and input_frame_buf_valid = '1' and input_frame_buf_last = '1' then
							apply_new_cfg <= '0';
							ctrl_in_state <= IDLE;
						end if;
				end case;

			end if; -- rst
		end if; -- clk
		end process pr_ctrl_in;

		input_frame_llr_ldpc_data_creonic  <= input_frame_llr_ldpc_data(63 downto 0);
		output_frame_ldpc_data(7 downto 0) <= output_frame_ldpc_data_creonic;

		inst_dec_ldpc_wrapper : entity frame_decoding.dec_ldpc_wrapper
		port map(

			clk            => clk,
			rst            => rst,

			ldpc_clk       => core_clk,
			ldpc_rst       => core_rst,

			-- Configuration input
			----------------------
			cfg_tdata      => ctrl_ldpc_data,
			cfg_tvalid     => ctrl_ldpc_valid,
			cfg_tready     => ctrl_ldpc_ready,

			-- Input data handling
			----------------------

			s_axis_tdata    => input_frame_llr_ldpc_data_creonic,
			s_axis_tvalid   => input_frame_llr_ldpc_valid,
			s_axis_tlast    => input_frame_llr_ldpc_last,
			s_axis_tready   => input_frame_llr_ldpc_ready,

			-- Configuration output
			----------------------
			cfg_out_tdata  => status_data_buf,
			cfg_out_tvalid => status_valid_buf,
			cfg_out_tready => status_ready_dec,

			-- Output data handling
			-----------------------
			m_axis_tdata   => output_frame_ldpc_data_creonic,
			m_axis_tvalid  => output_frame_ldpc_valid,
			m_axis_tlast   => output_frame_ldpc_last,
			m_axis_tready  => output_frame_ldpc_ready
		);

	end generate;


	-- Add axis buffer for cutting the combinational logic path
	inst_buffer_ctrl : entity frame_decoding.axi4s_buffer
	generic map(
		DATA_WIDTH => 9
	)
	port map(
		clk => clk,
		rst => rst,

		input_ready => ctrl_ready,
		input_valid => ctrl_valid,
		input_last  => '0',
		input_data  => ctrl_data_int,

		output_ready => ctrl_buf_ready,
		output_valid => ctrl_buf_valid,
		output_last  => open,
		output_data  => ctrl_buf_data
	);

	-- Add axis buffer for cutting the combinational logic path
	inst_buffer_data_in : entity frame_decoding.axi4s_buffer
	generic map(
		DATA_WIDTH => 8
	)
	port map(
		clk => clk,
		rst => rst,

		input_ready => input_ready,
		input_valid => input_valid,
		input_last  => input_last,
		input_data  => input_data,

		output_ready => input_frame_buf_ready,
		output_valid => input_frame_buf_valid,
		output_last  => input_frame_buf_last,
		output_data  => input_frame_buf_data
	);

	-- Store the data to avoid the LDPC
	inst_data_fifo : entity frame_decoding.axi4s_fifo
	generic map (
		DISTR_RAM         => false,
		FIFO_DEPTH        => 16384,
		DATA_WIDTH        => 8,
		FULL_THRESHOLD    => 16384,
		USE_OUTPUT_BUFFER => false
	)
	port map (
		clk => clk,
		rst => rst,

		input_ready       => input_frame_fifo_ready,
		input_valid       => input_frame_fifo_valid,
		input_last        => input_frame_fifo_last,
		input_data        => input_frame_fifo_data,
		input_almost_full => open,

		output_ready => output_frame_fifo_ready,
		output_valid => output_frame_fifo_valid,
		output_last  => output_frame_fifo_last,
		output_data  => output_frame_fifo_data
	);

	-- Store the data for LDPC to keep ready always high
	inst_ldpc_fifo : entity frame_decoding.axi4s_fifo
	generic map (
		DISTR_RAM         => false,
		FIFO_DEPTH        => 16384,
		DATA_WIDTH        => 8,
		FULL_THRESHOLD    => 16384,
		USE_OUTPUT_BUFFER => false
	)
	port map (
		clk => clk,
		rst => rst,

		input_ready       => input_frame_ldpc_ready,
		input_valid       => input_frame_ldpc_valid,
		input_last        => input_frame_ldpc_last,
		input_data        => input_frame_ldpc_data,
		input_almost_full => open,

		output_ready => input_frame_buf_ldpc_ready,
		output_valid => input_frame_buf_ldpc_valid,
		output_last  => input_frame_buf_ldpc_last,
		output_data  => input_frame_buf_ldpc_data
	);

	-- Add axis buffer for cutting the combinational logic path
	inst_buffer_status: entity frame_decoding.axi4s_buffer
	generic map(
		DATA_WIDTH => 64
	)
	port map(

		clk => clk,
		rst => rst,

		input_ready => status_ready_buf,
		input_valid => status_valid_buf,
		input_last  => '0',
		input_data  => status_data_buf,

		output_ready => status_ready,
		output_valid => status_valid,
		output_last  => open,
		output_data  => status_data
	);

	-- Add axis buffer for cutting the combinational logic path
	inst_buffer_data_out : entity frame_decoding.axi4s_buffer
	generic map(
		DATA_WIDTH => 8
	)
	port map(
		clk => clk,
		rst => rst,

		input_ready => output_int_ready,
		input_valid => output_int_valid,
		input_last  => output_int_last,
		input_data  => output_int_data,

		output_ready => output_ready,
		output_valid => output_valid,
		output_last  => output_last,
		output_data  => output_data
	);

end architecture rtl;
