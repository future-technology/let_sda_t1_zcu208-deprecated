--!
--! Copyright (C) 2010 - 2011 Creonic GmbH
--!
--! @file
--! @brief  Support package with useful functions
--! @author Matthias Alles
--! @date   2010/07/14
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package pkg_support is

	--!
	--! Return the log_2 of an natural value, i.e. the number of bits required
	--! to represent this unsigned value.
	--!
	function no_bits_natural(value_in : natural) return natural;

	function crc32(input: std_logic_vector(7 downto 0); crc:  std_logic_vector(31 downto 0)) return std_logic_vector;
	function scramble(v_input: std_logic_vector(7 downto 0); v_lfsr: std_logic_vector(14 downto 0)) return std_logic_vector;
	function update_lsfr(v_lfsr: std_logic_vector(14 downto 0)) return std_logic_vector;
	function hamming_distance(value_in : std_logic_vector(5 downto 0)) return unsigned;
	function calc_zeros(v_input : std_logic_vector) return unsigned;

	-- Calculate CRC
	function next_crc(crc, crc_poly, new_bits: std_logic_vector) return std_logic_vector;

end pkg_support;


package body pkg_support is

	function no_bits_natural(value_in: natural) return natural is
		variable v_n_bit : unsigned(31 downto 0);
	begin
		--vhdl_cover_off
		if value_in = 0 then
			return 0;
		end if;
		v_n_bit := to_unsigned(value_in, 32);
		for i in 31 downto 0 loop
			if v_n_bit(i) = '1' then
				return i + 1;
			end if;
		end loop;
		return 1;
		--vhdl_cover_on
	end function no_bits_natural;

	function crc32(input: std_logic_vector(7 downto 0); crc:  std_logic_vector(31 downto 0)) return std_logic_vector is
		variable v_sreg: std_logic_vector(31 downto 0);
		variable v_input : std_logic_vector(7 downto 0);
	begin
		v_input := input;

		v_sreg(0) := v_input(6) xor v_input(0) xor crc(24) xor crc(30);
		v_sreg(1) := v_input(7) xor v_input(6) xor v_input(1) xor v_input(0) xor crc(24) xor crc(25) xor crc(30) xor crc(31);
		v_sreg(2) := v_input(7) xor v_input(6) xor v_input(2) xor v_input(1) xor v_input(0) xor crc(24) xor crc(25) xor crc(26) xor crc(30) xor crc(31);
		v_sreg(3) := v_input(7) xor v_input(3) xor v_input(2) xor v_input(1) xor crc(25) xor crc(26) xor crc(27) xor crc(31);
		v_sreg(4) := v_input(6) xor v_input(4) xor v_input(3) xor v_input(2) xor v_input(0) xor crc(24) xor crc(26) xor crc(27) xor crc(28) xor crc(30);
		v_sreg(5) := v_input(7) xor v_input(6) xor v_input(5) xor v_input(4) xor v_input(3) xor v_input(1) xor v_input(0) xor crc(24) xor crc(25) xor crc(27) xor crc(28) xor crc(29) xor crc(30) xor crc(31);
		v_sreg(6) := v_input(7) xor v_input(6) xor v_input(5) xor v_input(4) xor v_input(2) xor v_input(1) xor crc(25) xor crc(26) xor crc(28) xor crc(29) xor crc(30) xor crc(31);
		v_sreg(7) := v_input(7) xor v_input(5) xor v_input(3) xor v_input(2) xor v_input(0) xor crc(24) xor crc(26) xor crc(27) xor crc(29) xor crc(31);
		v_sreg(8) := v_input(4) xor v_input(3) xor v_input(1) xor v_input(0) xor crc(0) xor crc(24) xor crc(25) xor crc(27) xor crc(28);
		v_sreg(9) := v_input(5) xor v_input(4) xor v_input(2) xor v_input(1) xor crc(1) xor crc(25) xor crc(26) xor crc(28) xor crc(29);
		v_sreg(10) := v_input(5) xor v_input(3) xor v_input(2) xor v_input(0) xor crc(2) xor crc(24) xor crc(26) xor crc(27) xor crc(29);
		v_sreg(11) := v_input(4) xor v_input(3) xor v_input(1) xor v_input(0) xor crc(3) xor crc(24) xor crc(25) xor crc(27) xor crc(28);
		v_sreg(12) := v_input(6) xor v_input(5) xor v_input(4) xor v_input(2) xor v_input(1) xor v_input(0) xor crc(4) xor crc(24) xor crc(25) xor crc(26) xor crc(28) xor crc(29) xor crc(30);
		v_sreg(13) := v_input(7) xor v_input(6) xor v_input(5) xor v_input(3) xor v_input(2) xor v_input(1) xor crc(5) xor crc(25) xor crc(26) xor crc(27) xor crc(29) xor crc(30) xor crc(31);
		v_sreg(14) := v_input(7) xor v_input(6) xor v_input(4) xor v_input(3) xor v_input(2) xor crc(6) xor crc(26) xor crc(27) xor crc(28) xor crc(30) xor crc(31);
		v_sreg(15) := v_input(7) xor v_input(5) xor v_input(4) xor v_input(3) xor crc(7) xor crc(27) xor crc(28) xor crc(29) xor crc(31);
		v_sreg(16) := v_input(5) xor v_input(4) xor v_input(0) xor crc(8) xor crc(24) xor crc(28) xor crc(29);
		v_sreg(17) := v_input(6) xor v_input(5) xor v_input(1) xor crc(9) xor crc(25) xor crc(29) xor crc(30);
		v_sreg(18) := v_input(7) xor v_input(6) xor v_input(2) xor crc(10) xor crc(26) xor crc(30) xor crc(31);
		v_sreg(19) := v_input(7) xor v_input(3) xor crc(11) xor crc(27) xor crc(31);
		v_sreg(20) := v_input(4) xor crc(12) xor crc(28);
		v_sreg(21) := v_input(5) xor crc(13) xor crc(29);
		v_sreg(22) := v_input(0) xor crc(14) xor crc(24);
		v_sreg(23) := v_input(6) xor v_input(1) xor v_input(0) xor crc(15) xor crc(24) xor crc(25) xor crc(30);
		v_sreg(24) := v_input(7) xor v_input(2) xor v_input(1) xor crc(16) xor crc(25) xor crc(26) xor crc(31);
		v_sreg(25) := v_input(3) xor v_input(2) xor crc(17) xor crc(26) xor crc(27);
		v_sreg(26) := v_input(6) xor v_input(4) xor v_input(3) xor v_input(0) xor crc(18) xor crc(24) xor crc(27) xor crc(28) xor crc(30);
		v_sreg(27) := v_input(7) xor v_input(5) xor v_input(4) xor v_input(1) xor crc(19) xor crc(25) xor crc(28) xor crc(29) xor crc(31);
		v_sreg(28) := v_input(6) xor v_input(5) xor v_input(2) xor crc(20) xor crc(26) xor crc(29) xor crc(30);
		v_sreg(29) := v_input(7) xor v_input(6) xor v_input(3) xor crc(21) xor crc(27) xor crc(30) xor crc(31);
		v_sreg(30) := v_input(7) xor v_input(4) xor crc(22) xor crc(28) xor crc(31);
		v_sreg(31) := v_input(5) xor crc(23) xor crc(29);
		return v_sreg;
	end crc32;

	function scramble(v_input: std_logic_vector(7 downto 0); v_lfsr: std_logic_vector(14 downto 0)) return std_logic_vector is
		variable v_data : std_logic_vector(7 downto 0);
	begin
		v_data(0) := v_input(0) xor v_lfsr(14);
		v_data(1) := v_input(1) xor v_lfsr(13) xor v_lfsr(14);
		v_data(2) := v_input(2) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
		v_data(3) := v_input(3) xor v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
		v_data(4) := v_input(4) xor v_lfsr(10) xor v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
		v_data(5) := v_input(5) xor v_lfsr(9)  xor v_lfsr(10) xor v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
		v_data(6) := v_input(6) xor v_lfsr(8)  xor v_lfsr(9)  xor v_lfsr(10) xor v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
		v_data(7) := v_input(7) xor v_lfsr(7)  xor v_lfsr(8)  xor v_lfsr(9)  xor v_lfsr(10) xor v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
		return v_data;
	end scramble;

	function update_lsfr(v_lfsr: std_logic_vector(14 downto 0)) return std_logic_vector is
		variable v_out : std_logic_vector(14 downto 0);
	begin
		v_out(0)  := v_lfsr(7)  xor v_lfsr(8)  xor v_lfsr(9)  xor v_lfsr(10) xor v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
		v_out(1)  := v_lfsr(8)  xor v_lfsr(9)  xor v_lfsr(10) xor v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
		v_out(2)  := v_lfsr(9)  xor v_lfsr(10) xor v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
		v_out(3)  := v_lfsr(10) xor v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
		v_out(4)  := v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
		v_out(5)  := v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
		v_out(6)  := v_lfsr(13) xor v_lfsr(14);
		v_out(7)  := v_lfsr(14);
		v_out(8)  := v_lfsr(0);
		v_out(9)  := v_lfsr(1);
		v_out(10) := v_lfsr(2);
		v_out(11) := v_lfsr(3);
		v_out(12) := v_lfsr(4);
		v_out(13) := v_lfsr(5);
		v_out(14) := v_lfsr(6) xor v_lfsr(7) xor v_lfsr(8) xor v_lfsr(9) xor v_lfsr(10) xor v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
		return v_out;
	end update_lsfr;

	function hamming_distance(value_in : std_logic_vector(5 downto 0)) return unsigned is
		variable v_error : unsigned(2 downto 0);
	begin
		--vhdl_cover_off
		case value_in is
			when "000000" => v_error := to_unsigned(0, 3);
			when "000001" => v_error := to_unsigned(1, 3);
			when "000010" => v_error := to_unsigned(1, 3);
			when "000011" => v_error := to_unsigned(2, 3);
			when "000100" => v_error := to_unsigned(1, 3);
			when "000101" => v_error := to_unsigned(2, 3);
			when "000110" => v_error := to_unsigned(2, 3);
			when "000111" => v_error := to_unsigned(3, 3);
			when "001000" => v_error := to_unsigned(1, 3);
			when "001001" => v_error := to_unsigned(2, 3);
			when "001010" => v_error := to_unsigned(2, 3);
			when "001011" => v_error := to_unsigned(3, 3);
			when "001100" => v_error := to_unsigned(2, 3);
			when "001101" => v_error := to_unsigned(3, 3);
			when "001110" => v_error := to_unsigned(3, 3);
			when "001111" => v_error := to_unsigned(4, 3);
			when "010000" => v_error := to_unsigned(1, 3);
			when "010001" => v_error := to_unsigned(2, 3);
			when "010010" => v_error := to_unsigned(2, 3);
			when "010011" => v_error := to_unsigned(3, 3);
			when "010100" => v_error := to_unsigned(2, 3);
			when "010101" => v_error := to_unsigned(3, 3);
			when "010110" => v_error := to_unsigned(3, 3);
			when "010111" => v_error := to_unsigned(4, 3);
			when "011000" => v_error := to_unsigned(2, 3);
			when "011001" => v_error := to_unsigned(3, 3);
			when "011010" => v_error := to_unsigned(3, 3);
			when "011011" => v_error := to_unsigned(4, 3);
			when "011100" => v_error := to_unsigned(3, 3);
			when "011101" => v_error := to_unsigned(4, 3);
			when "011110" => v_error := to_unsigned(4, 3);
			when "011111" => v_error := to_unsigned(5, 3);
			when "100000" => v_error := to_unsigned(1, 3);
			when "100001" => v_error := to_unsigned(2, 3);
			when "100010" => v_error := to_unsigned(2, 3);
			when "100011" => v_error := to_unsigned(3, 3);
			when "100100" => v_error := to_unsigned(2, 3);
			when "100101" => v_error := to_unsigned(3, 3);
			when "100110" => v_error := to_unsigned(3, 3);
			when "100111" => v_error := to_unsigned(4, 3);
			when "101000" => v_error := to_unsigned(2, 3);
			when "101001" => v_error := to_unsigned(3, 3);
			when "101010" => v_error := to_unsigned(3, 3);
			when "101011" => v_error := to_unsigned(4, 3);
			when "101100" => v_error := to_unsigned(3, 3);
			when "101101" => v_error := to_unsigned(4, 3);
			when "101110" => v_error := to_unsigned(4, 3);
			when "101111" => v_error := to_unsigned(5, 3);
			when "110000" => v_error := to_unsigned(2, 3);
			when "110001" => v_error := to_unsigned(3, 3);
			when "110010" => v_error := to_unsigned(3, 3);
			when "110011" => v_error := to_unsigned(4, 3);
			when "110100" => v_error := to_unsigned(3, 3);
			when "110101" => v_error := to_unsigned(4, 3);
			when "110110" => v_error := to_unsigned(4, 3);
			when "110111" => v_error := to_unsigned(5, 3);
			when "111000" => v_error := to_unsigned(3, 3);
			when "111001" => v_error := to_unsigned(4, 3);
			when "111010" => v_error := to_unsigned(4, 3);
			when "111011" => v_error := to_unsigned(5, 3);
			when "111100" => v_error := to_unsigned(4, 3);
			when "111101" => v_error := to_unsigned(5, 3);
			when "111110" => v_error := to_unsigned(5, 3);
			when others => v_error := to_unsigned(6, 3);
		end case;
		--vhdl_cover_on
		return v_error;
	end function;

	function calc_zeros(v_input: std_logic_vector) return unsigned is
		variable v_zeros_cnt  : unsigned(3 downto 0);
	begin
		--vhdl_cover_off
		case v_input(7 downto 0) is
		when "00000000" => v_zeros_cnt := to_unsigned(8 - 0, 4);
		when "00000001" => v_zeros_cnt := to_unsigned(8 - 1, 4);
		when "00000010" => v_zeros_cnt := to_unsigned(8 - 1, 4);
		when "00000011" => v_zeros_cnt := to_unsigned(8 - 2, 4);
		when "00000100" => v_zeros_cnt := to_unsigned(8 - 1, 4);
		when "00000101" => v_zeros_cnt := to_unsigned(8 - 2, 4);
		when "00000110" => v_zeros_cnt := to_unsigned(8 - 2, 4);
		when "00000111" => v_zeros_cnt := to_unsigned(8 - 3, 4);
		when "00001000" => v_zeros_cnt := to_unsigned(8 - 1, 4);
		when "00001001" => v_zeros_cnt := to_unsigned(8 - 2, 4);
		when "00001010" => v_zeros_cnt := to_unsigned(8 - 2, 4);
		when "00001011" => v_zeros_cnt := to_unsigned(8 - 3, 4);
		when "00001100" => v_zeros_cnt := to_unsigned(8 - 2, 4);
		when "00001101" => v_zeros_cnt := to_unsigned(8 - 3, 4);
		when "00001110" => v_zeros_cnt := to_unsigned(8 - 3, 4);
		when "00001111" => v_zeros_cnt := to_unsigned(8 - 4, 4);
		when "00010000" => v_zeros_cnt := to_unsigned(8 - 1, 4);
		when "00010001" => v_zeros_cnt := to_unsigned(8 - 2, 4);
		when "00010010" => v_zeros_cnt := to_unsigned(8 - 2, 4);
		when "00010011" => v_zeros_cnt := to_unsigned(8 - 3, 4);
		when "00010100" => v_zeros_cnt := to_unsigned(8 - 2, 4);
		when "00010101" => v_zeros_cnt := to_unsigned(8 - 3, 4);
		when "00010110" => v_zeros_cnt := to_unsigned(8 - 3, 4);
		when "00010111" => v_zeros_cnt := to_unsigned(8 - 4, 4);
		when "00011000" => v_zeros_cnt := to_unsigned(8 - 2, 4);
		when "00011001" => v_zeros_cnt := to_unsigned(8 - 3, 4);
		when "00011010" => v_zeros_cnt := to_unsigned(8 - 3, 4);
		when "00011011" => v_zeros_cnt := to_unsigned(8 - 4, 4);
		when "00011100" => v_zeros_cnt := to_unsigned(8 - 3, 4);
		when "00011101" => v_zeros_cnt := to_unsigned(8 - 4, 4);
		when "00011110" => v_zeros_cnt := to_unsigned(8 - 4, 4);
		when "00011111" => v_zeros_cnt := to_unsigned(8 - 5, 4);

		when "00100000" => v_zeros_cnt := to_unsigned(8-0-1, 4);
		when "00100001" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "00100010" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "00100011" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "00100100" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "00100101" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "00100110" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "00100111" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "00101000" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "00101001" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "00101010" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "00101011" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "00101100" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "00101101" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "00101110" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "00101111" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "00110000" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "00110001" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "00110010" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "00110011" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "00110100" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "00110101" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "00110110" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "00110111" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "00111000" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "00111001" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "00111010" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "00111011" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "00111100" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "00111101" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "00111110" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "00111111" => v_zeros_cnt := to_unsigned(8-5-1, 4);

		when "01000000" => v_zeros_cnt := to_unsigned(8-0-1, 4);
		when "01000001" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "01000010" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "01000011" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "01000100" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "01000101" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "01000110" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "01000111" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "01001000" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "01001001" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "01001010" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "01001011" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "01001100" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "01001101" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "01001110" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "01001111" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "01010000" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "01010001" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "01010010" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "01010011" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "01010100" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "01010101" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "01010110" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "01010111" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "01011000" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "01011001" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "01011010" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "01011011" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "01011100" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "01011101" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "01011110" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "01011111" => v_zeros_cnt := to_unsigned(8-5-1, 4);

		when "01100000" => v_zeros_cnt := to_unsigned(8-0-2, 4);
		when "01100001" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "01100010" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "01100011" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "01100100" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "01100101" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "01100110" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "01100111" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "01101000" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "01101001" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "01101010" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "01101011" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "01101100" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "01101101" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "01101110" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "01101111" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "01110000" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "01110001" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "01110010" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "01110011" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "01110100" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "01110101" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "01110110" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "01110111" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "01111000" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "01111001" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "01111010" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "01111011" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "01111100" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "01111101" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "01111110" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "01111111" => v_zeros_cnt := to_unsigned(8-5-2, 4);

		when "10000000" => v_zeros_cnt := to_unsigned(8-0-1, 4);
		when "10000001" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "10000010" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "10000011" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "10000100" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "10000101" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "10000110" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "10000111" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "10001000" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "10001001" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "10001010" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "10001011" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "10001100" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "10001101" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "10001110" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "10001111" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "10010000" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "10010001" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "10010010" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "10010011" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "10010100" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "10010101" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "10010110" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "10010111" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "10011000" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "10011001" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "10011010" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "10011011" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "10011100" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "10011101" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "10011110" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "10011111" => v_zeros_cnt := to_unsigned(8-5-1, 4);

		when "10100000" => v_zeros_cnt := to_unsigned(8-0-2, 4);
		when "10100001" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "10100010" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "10100011" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "10100100" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "10100101" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "10100110" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "10100111" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "10101000" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "10101001" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "10101010" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "10101011" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "10101100" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "10101101" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "10101110" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "10101111" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "10110000" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "10110001" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "10110010" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "10110011" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "10110100" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "10110101" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "10110110" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "10110111" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "10111000" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "10111001" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "10111010" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "10111011" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "10111100" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "10111101" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "10111110" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "10111111" => v_zeros_cnt := to_unsigned(8-5-2, 4);

		when "11000000" => v_zeros_cnt := to_unsigned(8-0-2, 4);
		when "11000001" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "11000010" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "11000011" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "11000100" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "11000101" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "11000110" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "11000111" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "11001000" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "11001001" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "11001010" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "11001011" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "11001100" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "11001101" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "11001110" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "11001111" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "11010000" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "11010001" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "11010010" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "11010011" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "11010100" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "11010101" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "11010110" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "11010111" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "11011000" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "11011001" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "11011010" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "11011011" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "11011100" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "11011101" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "11011110" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "11011111" => v_zeros_cnt := to_unsigned(8-5-2, 4);

		when "11100000" => v_zeros_cnt := to_unsigned(8-0-3, 4);
		when "11100001" => v_zeros_cnt := to_unsigned(8-1-3, 4);
		when "11100010" => v_zeros_cnt := to_unsigned(8-1-3, 4);
		when "11100011" => v_zeros_cnt := to_unsigned(8-2-3, 4);
		when "11100100" => v_zeros_cnt := to_unsigned(8-1-3, 4);
		when "11100101" => v_zeros_cnt := to_unsigned(8-2-3, 4);
		when "11100110" => v_zeros_cnt := to_unsigned(8-2-3, 4);
		when "11100111" => v_zeros_cnt := to_unsigned(8-3-3, 4);
		when "11101000" => v_zeros_cnt := to_unsigned(8-1-3, 4);
		when "11101001" => v_zeros_cnt := to_unsigned(8-2-3, 4);
		when "11101010" => v_zeros_cnt := to_unsigned(8-2-3, 4);
		when "11101011" => v_zeros_cnt := to_unsigned(8-3-3, 4);
		when "11101100" => v_zeros_cnt := to_unsigned(8-2-3, 4);
		when "11101101" => v_zeros_cnt := to_unsigned(8-3-3, 4);
		when "11101110" => v_zeros_cnt := to_unsigned(8-3-3, 4);
		when "11101111" => v_zeros_cnt := to_unsigned(8-4-3, 4);
		when "11110000" => v_zeros_cnt := to_unsigned(8-1-3, 4);
		when "11110001" => v_zeros_cnt := to_unsigned(8-2-3, 4);
		when "11110010" => v_zeros_cnt := to_unsigned(8-2-3, 4);
		when "11110011" => v_zeros_cnt := to_unsigned(8-3-3, 4);
		when "11110100" => v_zeros_cnt := to_unsigned(8-2-3, 4);
		when "11110101" => v_zeros_cnt := to_unsigned(8-3-3, 4);
		when "11110110" => v_zeros_cnt := to_unsigned(8-3-3, 4);
		when "11110111" => v_zeros_cnt := to_unsigned(8-4-3, 4);
		when "11111000" => v_zeros_cnt := to_unsigned(8-2-3, 4);
		when "11111001" => v_zeros_cnt := to_unsigned(8-3-3, 4);
		when "11111010" => v_zeros_cnt := to_unsigned(8-3-3, 4);
		when "11111011" => v_zeros_cnt := to_unsigned(8-4-3, 4);
		when "11111100" => v_zeros_cnt := to_unsigned(8-3-3, 4);
		when "11111101" => v_zeros_cnt := to_unsigned(8-4-3, 4);
		when "11111110" => v_zeros_cnt := to_unsigned(8-4-3, 4);
		when others     => v_zeros_cnt := to_unsigned(0, 4);
		end case;
		--vhdl_cover_on
		return v_zeros_cnt;
	end function calc_zeros;

	function next_crc(crc, crc_poly, new_bits: std_logic_vector) return std_logic_vector is
		variable v_next_crc: std_logic_vector(crc'range);
	begin
		v_next_crc := crc;

		for i in new_bits'range loop
			if v_next_crc(v_next_crc'left) /= new_bits(i) then
				v_next_crc := (v_next_crc(v_next_crc'left - 1 downto 0) & '0') xor crc_poly;
			else
				v_next_crc := v_next_crc(v_next_crc'left - 1 downto 0) & '0';
			end if;
		end loop;
		return v_next_crc;
	end next_crc;

end pkg_support;
