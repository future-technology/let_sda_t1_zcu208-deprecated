--!
--! Copyright (C)  2020 Creonic GmbH
--!
--! @file
--! @brief  Package defining some usefull shared types.
--! @author Markus Fehrenz/Nhan Nguyen
--! @date   2020/03/02
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package pkg_types is

	type trec_cfg_preamble_sync is record
		acquisition_length : unsigned(32 - 1 downto 0);
		tracking_length    : unsigned(32 - 1 downto 0);
		max_tracking_error : unsigned(32 - 1 downto 0);
		preamble_max_corr  : unsigned(7 - 1 downto 0);
	end record;

	type trec_stat_preamble_sync is record
		new_search_counter : unsigned(32 - 1 downto 0);
		sync_loss_counter  : unsigned(32 - 1 downto 0);
		sync_locked        : std_logic;
	end record;

	constant RESET_CFG_PREAMBLE_SYNC : trec_cfg_preamble_sync := (acquisition_length => to_unsigned(4, 32),
	                                                              tracking_length    => to_unsigned(64, 32),
	                                                              max_tracking_error => to_unsigned(16, 32),
	                                                              preamble_max_corr  => to_unsigned(62, 7));

	constant RESET_STAT_PREAMBLE_SYNC : trec_stat_preamble_sync := (new_search_counter  => to_unsigned(0, 32),
	                                                                sync_loss_counter   => to_unsigned(0, 32),
	                                                                sync_locked         => '0');


end package pkg_types;

