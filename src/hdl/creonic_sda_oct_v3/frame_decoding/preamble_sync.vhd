--!
--! Copyright (C) 2020 Creonic GmbH
--!
--! @file
--! @brief  Entity synchronizes the fso preamble.
--! @author Nhan Nguyen
--! @date   2021/02/19
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_decoding;
use frame_decoding.pkg_support.all;
use frame_decoding.pkg_types.all;


entity preamble_sync is
	port(
		clk : in std_logic;
		rst : in std_logic;

		preamble : in  std_logic_vector(63 downto 0);

		ctrl_in_ready : out std_logic;
		ctrl_in_valid : in  std_logic;
		ctrl_in_data  : in  std_logic_vector(2 downto 0);

		input_valid : in  std_logic;
		input_start : in  std_logic;
		input_last  : in  std_logic;
		input_data  : in  std_logic_vector(7 downto 0);

		slip_en      : out std_logic;
		bits_slip    : out unsigned( 2 downto 0);
		bytes_slip   : out unsigned(11 downto 0);
		frame_size   : out unsigned(11 downto 0);
		slip_done    : in  std_logic;

		output_valid : out std_logic;
		output_last  : out std_logic;
		output_data  : out std_logic_vector(7 downto 0);

		reconfiguration : out std_logic;
		preamble_synced : out std_logic;

		ctrl_out_ready : in  std_logic;
		ctrl_out_valid : out std_logic;
		ctrl_out_data  : out std_logic_vector(2 downto 0);
		ctrl_out_user  : out std_logic_vector(5 downto 0);

		configurations : in  trec_cfg_preamble_sync;
		statistics     : out trec_stat_preamble_sync;
		clear_stat     : in  std_logic
	);
end entity preamble_sync;

architecture rtl of preamble_sync is
	constant BW_PREAMBLE        : natural := 64;
	constant NUM_BYTES_PREAMBLE : natural := BW_PREAMBLE / 8;

	type t_frame_size is array (0 to 4) of natural;
	constant OUTPUT_FRAME_SIZE : t_frame_size := (1176, 1368, 1512, 1704, 2232);

	type t_check_header_fsm is (WAIT_FOR_FRAME, CHECK, TRACK_SYNC, NEW_SEARCH, ACQUISITION, ADAPT_STREAM);
	signal check_header_fsm : t_check_header_fsm;

	constant TOLERATED_BITFLIP_PREAMBLE : natural := 2;
	constant PREAMBLE_MAX_CORR    : natural := BW_PREAMBLE - TOLERATED_BITFLIP_PREAMBLE;
	constant PIPE_LEN             : natural := 8;
	constant NUM_FRAMES_WINDOW    : natural := 4;
	constant BW_NUM_FRAMES_WINDOW : natural := no_bits_natural(NUM_FRAMES_WINDOW - 1);
	constant BW_CORR_DATA         : natural := BW_NUM_FRAMES_WINDOW + 7;
	constant BW_CORR_ADDR         : natural := no_bits_natural(OUTPUT_FRAME_SIZE(4) + NUM_BYTES_PREAMBLE - 1);

	type t_input_vector is array (0 to 7) of std_logic_vector(BW_PREAMBLE - 1 downto 0);
	type t_stage_0_correlation is array (0 to 8) of unsigned(3 downto 0);
	type t_stage_0_correlation_array is array (0 to 7) of t_stage_0_correlation;
	signal stage_0_corr_arr : t_stage_0_correlation_array;
	type t_stage_1_correlation is array (0 to 2) of unsigned(5 downto 0);
	type t_stage_1_correlation_array is array (0 to 7) of t_stage_1_correlation;
	signal stage_1_corr_arr : t_stage_1_correlation_array;
	type t_stage_2_correlation_array is array (0 to 7) of unsigned(6 downto 0);
	signal stage_2_corr_arr : t_stage_2_correlation_array;
	type t_max_correlation_array is array(natural range <>) of unsigned(BW_NUM_FRAMES_WINDOW + 6 downto 0);
	signal final_corr_arr    : t_max_correlation_array(0 to 7);
	signal saved_corr_arr    : t_max_correlation_array(0 to 7);
	signal correlation_max_4 : t_max_correlation_array(0 to 3);
	signal correlation_max_2 : t_max_correlation_array(0 to 1);
	signal correlation_max   : unsigned(BW_NUM_FRAMES_WINDOW + 6 downto 0);
	signal correlation_final : unsigned(BW_NUM_FRAMES_WINDOW + 6 downto 0);
	type t_index_array is array(natural range <>) of unsigned(2 downto 0);
	signal correlation_idx_4 : t_index_array(0 to 3);
	signal correlation_idx_2 : t_index_array(0 to 1);
	signal correlation_idx   : unsigned(2 downto 0);

	signal corr_ram_wen   : std_logic;
	signal corr_ram_waddr : std_logic_vector(BW_CORR_ADDR - 1 downto 0);
	signal corr_ram_wdata : std_logic_vector(8 * BW_CORR_DATA - 1 downto 0);
	signal corr_ram_ren   : std_logic;
	signal corr_ram_raddr : std_logic_vector(BW_CORR_ADDR - 1 downto 0);
	signal corr_ram_rdata : std_logic_vector(8 * BW_CORR_DATA - 1 downto 0);

	signal input_valid_d  : std_logic;
	signal input_data_d   : std_logic_vector(7 downto 0);
	signal input_sreg     : std_logic_vector(BW_PREAMBLE + 7 downto 0);
	signal frame_preamble : std_logic_vector(BW_PREAMBLE - 1 downto 0);
	signal preamble_valid_sreg : std_logic_vector(5 downto 0);
	signal preamble_cnt   : unsigned(2 downto 0);
	signal wait_preamble  : std_logic;
	signal pre_corr_0     : t_stage_0_correlation;
	signal pre_corr_1     : t_stage_1_correlation;
	signal pre_corr       : unsigned(6 downto 0);
	signal sync_locked    : std_logic;

	type t_output_data_sreg is array(4 downto 0) of std_logic_vector(7 downto 0);
	signal output_data_sreg  : t_output_data_sreg;
	signal output_valid_sreg : std_logic_vector(4 downto 0);
	signal output_cnt        : unsigned(11 downto 0);

	signal input_cnt    : unsigned(3 downto 0);
	signal window_cnt_3 : unsigned(BW_NUM_FRAMES_WINDOW + 1 downto 0);
	signal window_cnt_7 : unsigned(BW_NUM_FRAMES_WINDOW + 1 downto 0);
	type t_byte_cnt_arr is array(PIPE_LEN - 1 downto 0) of unsigned(BW_CORR_ADDR - 1 downto 0);
	signal byte_cnt_sreg    : t_byte_cnt_arr;
	signal frame_valid_sreg : std_logic_vector(PIPE_LEN - 1 downto 0);

	signal acq_cnt  : unsigned(31 downto 0);
	signal trk_cnt  : unsigned(31 downto 0);
	signal fail_cnt : unsigned(31 downto 0);

	signal output_block_size : unsigned(11 downto 0);
	signal frame_ext_size    : unsigned(11 downto 0);
	signal code_rate_reg     : unsigned(2 downto 0);
	signal ctrl_ready_int    : std_logic;
	signal recfg_int         : std_logic;
	signal rx_rst            : std_logic;
	signal recfg_cnt         : unsigned(3 downto 0);
	signal output_last_int   : std_logic;

	type t_output_fsm is (WAIT_SYNC, GET_PREAMBLE, GET_DATA);
	signal output_fsm : t_output_fsm;

	signal statistics_int : trec_stat_preamble_sync;

	attribute RAM_STYLE : string;
	attribute RAM_STYLE of stage_0_corr_arr : signal is "distributed";
	attribute RAM_STYLE of pre_corr_0 : signal is "distributed";

begin

	preamble_synced <= sync_locked;
	statistics      <= statistics_int;
	frame_size      <= frame_ext_size;
	ctrl_in_ready   <= ctrl_ready_int;
	reconfiguration <= recfg_int;
	rx_rst          <= rst or recfg_int;

	-- RAM for storing the correlations
	inst_generic_simple_dp_ram : entity frame_decoding.generic_simple_dp_ram
	generic map(
		DISTR_RAM => false,
		WORDS     => OUTPUT_FRAME_SIZE(4) + NUM_BYTES_PREAMBLE,
		BITWIDTH  => 8 * BW_CORR_DATA,
		WR_REG    => false,
		RD_REG    => false
	)
	port map(
		clk => clk,
		rst => rx_rst,

		port_a_wen     => corr_ram_wen,
		port_a_addr    => corr_ram_waddr,
		port_a_data_in => corr_ram_wdata,

		port_b_en       => corr_ram_ren,
		port_b_addr     => corr_ram_raddr,
		port_b_data_out => corr_ram_rdata,
		port_b_data_out_valid => open
	);

	-- corr ram input data
	pr_ram_data : process(final_corr_arr) is
	begin
		for i in 0 to 7 loop
			corr_ram_wdata((i + 1) * BW_CORR_DATA - 1 downto i * BW_CORR_DATA) <= std_logic_vector(final_corr_arr(i));
		end loop;
	end process pr_ram_data;

	-- Evaluate the FSO header and synchronize to this.
	pr_check_header : process(clk) is
		variable v_input_vec  : t_input_vector;
		variable v_input_data : std_logic_vector(BW_PREAMBLE - 1 downto 0);
		variable v_tmp_byte   : std_logic_vector(7 downto 0);
	begin
	if rising_edge(clk) then
		if rst = '1' then
			input_cnt           <= (others => '0');
			byte_cnt_sreg       <= (others => (others => '0'));
			frame_valid_sreg    <= (others => '0');
			window_cnt_3        <= (others => '0');
			window_cnt_7        <= (others => '0');
			corr_ram_wen        <= '0';
			corr_ram_waddr      <= (others => '0');
			corr_ram_ren        <= '0';
			corr_ram_raddr      <= (others => '0');
			stage_0_corr_arr    <= (others => (others => (others => '0')));
			stage_1_corr_arr    <= (others => (others => (others => '0')));
			stage_2_corr_arr    <= (others => (others => '0'));
			final_corr_arr      <= (others => (others => '0'));
			saved_corr_arr      <= (others => (others => '0'));
			correlation_max_4   <= (others => (others => '0'));
			correlation_max_2   <= (others => (others => '0'));
			correlation_max     <= (others => '0');
			correlation_idx_4   <= (others => (others => '0'));
			correlation_idx_2   <= (others => (others => '0'));
			correlation_idx     <= (others => '0');
			input_valid_d       <= '0';
			input_data_d        <= (others => '0');
			input_sreg          <= (others => '0');
			slip_en             <= '0';
			bytes_slip          <= (others => '0');
			bits_slip           <= (others => '0');
			preamble_valid_sreg <= (others => '0');
			wait_preamble       <= '0';
			frame_preamble      <= (others => '0');
			pre_corr_0          <= (others => (others => '0'));
			pre_corr_1          <= (others => (others => '0'));
			pre_corr            <= (others => '0');
			correlation_final   <= (others => '0');
			preamble_cnt        <= (others => '0');

			statistics_int   <= RESET_STAT_PREAMBLE_SYNC;
			acq_cnt          <= (others => '0');
			trk_cnt          <= (others => '0');
			fail_cnt         <= (others => '0');
			sync_locked      <= '0';
			check_header_fsm <= NEW_SEARCH;

			output_block_size <= (others => '0');
			frame_ext_size    <= to_unsigned(OUTPUT_FRAME_SIZE(4) + NUM_BYTES_PREAMBLE - 1, 12);
			code_rate_reg     <= (others => '0');
			ctrl_ready_int    <= '1';
			recfg_int         <= '0';
			recfg_cnt         <= (others => '0');
		else

			input_valid_d <= input_valid;
			input_data_d  <= input_data;

			-- Manage the header synchronization.
			case check_header_fsm is

			-- If we are not locked, reset everything and wait for the first frame to arrive.
			when NEW_SEARCH =>
				acq_cnt           <= (others => '0');
				trk_cnt           <= (others => '0');
				fail_cnt          <= (others => '0');
				window_cnt_3      <= (others => '0');
				window_cnt_7      <= (others => '0');
				byte_cnt_sreg     <= (others => (others => '0'));
				frame_valid_sreg  <= (others => '0');
				correlation_max   <= (others => '0');
				correlation_idx   <= (others => '0');
				input_cnt         <= (others => '0');
				correlation_final <= (others => '0');
				check_header_fsm  <= WAIT_FOR_FRAME;

				sync_locked <= '0';
				statistics_int.new_search_counter <= statistics_int.new_search_counter + 1;

			-- Wait for a new frame to arrive, which we can sync to.
			when WAIT_FOR_FRAME =>
				recfg_cnt <= recfg_cnt + 1;
				if recfg_cnt = 15 then
					recfg_int <= '0';
				end if;

				if input_valid = '1' and input_start = '1' then
					check_header_fsm <= CHECK;
					recfg_cnt        <= (others => '0');
					frame_ext_size   <= to_unsigned(OUTPUT_FRAME_SIZE(to_integer(code_rate_reg)) + NUM_BYTES_PREAMBLE - 1, 12);
				end if;

			-- Calculate the correlations for each position.
			when CHECK =>

				if input_valid_d = '1' then
					input_sreg(7 downto 0)  <= input_data_d;
					input_sreg(BW_PREAMBLE  + 7 downto 8) <= input_sreg(BW_PREAMBLE - 1 downto 0);
					if input_cnt >= NUM_BYTES_PREAMBLE then
						frame_valid_sreg(0) <= '1';
					else
						input_cnt <= input_cnt + 1;
					end if;
				else
					frame_valid_sreg(0) <= '0';
				end if;

				-- shift registers
				byte_cnt_sreg(PIPE_LEN - 1 downto 1)    <= byte_cnt_sreg(PIPE_LEN - 2 downto 0);
				frame_valid_sreg(PIPE_LEN - 1 downto 1) <= frame_valid_sreg(PIPE_LEN - 2 downto 0);

				corr_ram_ren    <= frame_valid_sreg(0);
				corr_ram_raddr  <= std_logic_vector(byte_cnt_sreg(0));
				if frame_valid_sreg(0) = '1' then
					byte_cnt_sreg(0) <= byte_cnt_sreg(0) + 1;
					if byte_cnt_sreg(0) = frame_ext_size then
						byte_cnt_sreg(0) <= (others => '0');
					end if;

					for i in 0 to 7 loop
						v_input_vec(i) := input_sreg(BW_PREAMBLE + 7 - i downto 8 - i) xor preamble;
						for j in 0 to NUM_BYTES_PREAMBLE - 1 loop
							v_tmp_byte := v_input_vec(i)(8 * (j + 1) - 1 downto 8 * j);
							stage_0_corr_arr(i)(j) <= calc_zeros(v_tmp_byte);
						end loop;
					end loop;
				end if;

				if frame_valid_sreg(1) = '1' then
					for i in 0 to 7 loop
						stage_1_corr_arr(i)(0) <= ("00" & stage_0_corr_arr(i)(0)) + ("00" & stage_0_corr_arr(i)(1)) +
						                          ("00" & stage_0_corr_arr(i)(2)) + ("00" & stage_0_corr_arr(i)(3));
						stage_1_corr_arr(i)(1) <= ("00" & stage_0_corr_arr(i)(4)) + ("00" & stage_0_corr_arr(i)(5)) +
						                          ("00" & stage_0_corr_arr(i)(6)) + ("00" & stage_0_corr_arr(i)(7));
						stage_1_corr_arr(i)(2) <=  "00" & stage_0_corr_arr(i)(8);
					end loop;
				end if;

				if frame_valid_sreg(2) = '1' then
					for i in 0 to 7 loop

						-- get the saved corr from RAM
						saved_corr_arr(i) <= unsigned(corr_ram_rdata((i + 1) * BW_CORR_DATA - 1 downto i * BW_CORR_DATA));
						stage_2_corr_arr(i) <= ('0' & stage_1_corr_arr(i)(0)) + ('0' & stage_1_corr_arr(i)(1)) + ('0' & stage_1_corr_arr(i)(2));
					end loop;
				end if;

				-- combine with the stored correlation in the RAM
				corr_ram_wen   <= frame_valid_sreg(3);
				corr_ram_waddr <= std_logic_vector(byte_cnt_sreg(3));
				if frame_valid_sreg(3) = '1' then
					if byte_cnt_sreg(3) = frame_ext_size then
						window_cnt_3 <= window_cnt_3 + 1;
					end if;
					for i in 0 to 7 loop
						if window_cnt_3 > 0 then
							final_corr_arr(i) <= saved_corr_arr(i) + stage_2_corr_arr(i);
						else
							final_corr_arr(i) <= (others => '0');
							final_corr_arr(i)(6 downto 0) <= stage_2_corr_arr(i);
						end if;
					end loop;
				end if;

				-- search for max in 8 correlations
				if frame_valid_sreg(4) = '1' then
					for i in 0 to 3 loop
						correlation_max_4(i) <= final_corr_arr(2 * i);
						correlation_idx_4(i) <= to_unsigned(2 * i, 3);
						if final_corr_arr(2 * i + 1) > final_corr_arr(2 * i) then
							correlation_max_4(i) <= final_corr_arr(2 * i + 1);
							correlation_idx_4(i) <= to_unsigned(2 * i + 1, 3);
						end if;
					end loop;
				end if;

				-- search for max in 4 correlations
				if frame_valid_sreg(5) = '1' then
					for i in 0 to 1 loop
						correlation_max_2(i) <= correlation_max_4(2 * i);
						correlation_idx_2(i) <= correlation_idx_4(2 * i);
						if correlation_max_4(2 * i + 1) > correlation_max_4(2 * i) then
							correlation_max_2(i) <= correlation_max_4(2 * i + 1);
							correlation_idx_2(i) <= correlation_idx_4(2 * i + 1);
						end if;
					end loop;
				end if;

				-- search for max in 2 correlations
				if frame_valid_sreg(6) = '1' then
					correlation_max <= correlation_max_2(0);
					correlation_idx <= correlation_idx_2(0);
					if correlation_max_2(1) > correlation_max_2(0) then
						correlation_max <= correlation_max_2(1);
						correlation_idx <= correlation_idx_2(1);
					end if;
				end if;

				-- get the up to date correlation
				if frame_valid_sreg(7) = '1' then
					-- compare with the previous correlation
					if correlation_max > correlation_final then
						correlation_final <= correlation_max;
						bytes_slip <= byte_cnt_sreg(7);
						bits_slip  <= correlation_idx;
					end if;
					if byte_cnt_sreg(7) = frame_ext_size then
						window_cnt_7 <= window_cnt_7 + 1;
						if window_cnt_7 = NUM_FRAMES_WINDOW - 1 then
							slip_en          <= '1';
							check_header_fsm <= ADAPT_STREAM;
						end if;
					end if;
				end if;

			-- Once the maximum correlation has been found, the stream is adapted.
			when ADAPT_STREAM =>
				acq_cnt <= (others => '0');
				if slip_done = '1' then
					slip_en  <= '0';
					check_header_fsm <= ACQUISITION;
				end if;

			-- After stream adaptation, we try to lock to this position, if this is successfull we go to tracking. New search otherwise.
			when ACQUISITION =>

				if preamble_valid_sreg(3) = '1' then
					acq_cnt <= acq_cnt + 1;
					if acq_cnt = configurations.acquisition_length - 1 then
						sync_locked       <= '1';
						check_header_fsm  <= TRACK_SYNC;
						fail_cnt          <= (others => '0');
						output_block_size <= to_unsigned(OUTPUT_FRAME_SIZE(to_integer(code_rate_reg)) - 1, 12);
					end if;

					--if pre_corr < PREAMBLE_MAX_CORR then
					if pre_corr < configurations.preamble_max_corr then    -- CAGS: lets add some detection thresh control
						check_header_fsm <= NEW_SEARCH;
						sync_locked <= '0';
					end if;

				end if;

			-- Tracking state, which is only left if there are to many errors.
			when TRACK_SYNC =>
				sync_locked <= '1';
				if preamble_valid_sreg(3) = '1' then
					if trk_cnt = configurations.tracking_length - 1 then
						fail_cnt <= (others => '0');
					end if;
					trk_cnt <= trk_cnt + 1;
					
					--if pre_corr < PREAMBLE_MAX_CORR then
					if pre_corr < configurations.preamble_max_corr then    -- CAGS: lets add some detection thresh control
						fail_cnt <= fail_cnt + 1;
					end if;

					-- Check the fail count and compare with the error threshold.
					if fail_cnt > configurations.max_tracking_error - 1 then
						check_header_fsm <= NEW_SEARCH;
						sync_locked <= '0';
						statistics_int.sync_loss_counter <= statistics_int.sync_loss_counter + 1;
					end if;

					if trk_cnt = configurations.tracking_length - 1 then
						trk_cnt <= (others => '0');
					end if;
				end if;
			end case;

			--- Check if we get correct preamble, the result is used when in ACQ or TRACKING state
			preamble_valid_sreg(0) <= '0';
			if wait_preamble = '0' then
				if input_valid = '1' then
					if input_start = '1' then
						wait_preamble <= '1';
						preamble_cnt  <= (others => '0');
						frame_preamble(7 downto 0) <= input_data;
					end if;
				end if;
			else
				if input_valid = '1' then
					frame_preamble(7 downto 0)  <= input_data;
					frame_preamble(BW_PREAMBLE - 1 downto 8) <= frame_preamble(BW_PREAMBLE - 9 downto 0);
					preamble_cnt <= preamble_cnt + 1;
					if preamble_cnt = NUM_BYTES_PREAMBLE - 2 then
						wait_preamble  <= '0';
						preamble_valid_sreg(0) <= '1';
					end if;
				end if;
			end if;

			preamble_valid_sreg(5 downto 1) <= preamble_valid_sreg(4 downto 0);
			if preamble_valid_sreg(0) = '1' then
				v_input_data := frame_preamble xor preamble;
				for i in 0 to NUM_BYTES_PREAMBLE - 1 loop
					v_tmp_byte := v_input_data(8 * (i + 1) - 1 downto 8 * i);
					pre_corr_0(i) <= calc_zeros(v_tmp_byte);
				end loop;
			end if;

			if preamble_valid_sreg(1) = '1' then
					pre_corr_1(0) <= ("00" & pre_corr_0(0)) + ("00" & pre_corr_0(1)) + ("00" & pre_corr_0(2)) + ("00" & pre_corr_0(3));
					pre_corr_1(1) <= ("00" & pre_corr_0(4)) + ("00" & pre_corr_0(5)) + ("00" & pre_corr_0(6)) + ("00" & pre_corr_0(7));
					pre_corr_1(2) <= ("00" & pre_corr_0(8));
			end if;

			if preamble_valid_sreg(2) = '1' then
				pre_corr <= ('0' & pre_corr_1(0)) + ('0' & pre_corr_1(1)) + ('0' & pre_corr_1(2));
			end if;

			statistics_int.sync_locked <= sync_locked;
			if clear_stat = '1' then
				statistics_int <= RESET_STAT_PREAMBLE_SYNC;
			end if;

			-- If there is a new code rate, we start a new search
			if ctrl_ready_int = '1' and ctrl_in_valid = '1' then
				ctrl_ready_int <= '0';

				if unsigned(ctrl_in_data) /= code_rate_reg then
					code_rate_reg <= unsigned(ctrl_in_data);
					check_header_fsm <= NEW_SEARCH;
					sync_locked   <= '0';
					recfg_int     <= '1';
					preamble_cnt  <= (others => '0');
					wait_preamble <= '0';
					corr_ram_wen  <= '0';
					corr_ram_ren  <= '0';
					input_valid_d <= '0';
					input_data_d  <= (others => '0');
					pre_corr_0    <= (others => (others => '0'));
					pre_corr_1    <= (others => (others => '0'));
					pre_corr      <= (others => '0');

					if check_header_fsm = TRACK_SYNC then
						statistics_int.sync_loss_counter <= statistics_int.sync_loss_counter + 1;
					end if;
				end if;
			end if;

			if (sync_locked = '1' and output_last_int = '1') or
			   (sync_locked = '0' and (input_valid = '1' and input_last = '1')) then
				ctrl_ready_int <= '1';
			end if;
		end if;
	end if;
	end process pr_check_header;

	output_last <= output_last_int;

	-- Output data interface
	pr_output : process(clk) is
	begin
	if rising_edge(clk) then
		if rst = '1' then
			output_data  <= (others => '0');
			output_valid <= '0';
			output_last_int  <= '0';
			output_data_sreg  <= (others => (others => '0'));
			output_valid_sreg <= (others => '0');
			output_cnt <= (others => '0');
			output_fsm <= WAIT_SYNC;

			ctrl_out_valid <= '0';
			ctrl_out_data  <= (others => '0');
			ctrl_out_user  <= (others => '0');
		else

			if ctrl_out_ready = '1' then
				ctrl_out_valid <= '0';
				ctrl_out_data  <= (others => '0');
				ctrl_out_user  <= (others => '0');
			end if;

			output_data_sreg(0) <= input_data_d;
			output_data_sreg(4 downto 1) <= output_data_sreg(3 downto 0);
			output_valid_sreg(0) <= input_valid_d;
			output_valid_sreg(4 downto 1) <= output_valid_sreg(3 downto 0);

			case output_fsm is

			when WAIT_SYNC =>
				output_valid <= '0';
				output_last_int <= '0';
				if sync_locked = '1' then
					output_fsm <= GET_PREAMBLE;
				end if;

			when GET_PREAMBLE =>
				output_valid <= '0';
				output_last_int  <= '0';
				if preamble_valid_sreg(5) = '1' then
					output_fsm <= GET_DATA;
					ctrl_out_valid <= '1';
					ctrl_out_data  <= std_logic_vector(code_rate_reg);
					ctrl_out_user  <= std_logic_vector(to_unsigned(8, 6));
				end if;

			when GET_DATA =>
				output_valid <= output_valid_sreg(4);
				output_data  <= output_data_sreg(4);
				if output_valid_sreg(4) = '1' then
					output_cnt <= output_cnt + 1;
					if output_cnt = output_block_size then
						output_cnt  <= (others => '0');
						output_last_int <= '1';
						output_fsm  <= GET_PREAMBLE;
						if sync_locked = '0' then
							output_fsm <= WAIT_SYNC;
						end if;
					end if;
				end if;
			end case;

			if recfg_int = '1' then
				output_fsm <= WAIT_SYNC;
				output_cnt <= (others => '0');
				output_data_sreg  <= (others => (others => '0'));
				output_valid_sreg <= (others => '0');
			end if;
		end if;
	end if;
	end process pr_output;


end architecture rtl;
