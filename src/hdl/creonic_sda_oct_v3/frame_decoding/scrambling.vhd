--!
--! Copyright (C) Creonic GmbH
--!
--! @file
--! @brief  This entity contains the scrambling for the FSO frame.
--! @author Carlos Velez
--! @date   2021/10/21
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_decoding;
use frame_decoding.pkg_support.all;

entity scrambling is
	port(
		clk : in std_logic;
		rst : in std_logic;

		input_ready : out std_logic;
		input_valid : in  std_logic;
		input_last  : in  std_logic;
		input_data  : in  std_logic_vector(8 - 1 downto 0);

		output_ready : in  std_logic;
		output_valid : out std_logic;
		output_last  : out std_logic;
		output_data  : out std_logic_vector(8 - 1 downto 0)
	);
end entity scrambling;

architecture rtl of scrambling is

	----------------------------
	-- Code parameter signals --
	----------------------------

	constant NUM_SHIFTER_BITS : natural := 15;
	constant INIT_STATE       : std_logic_vector(NUM_SHIFTER_BITS - 1 downto 0) :=  "001110110110000";

	signal scramble_en  : std_logic;
	signal scramble_rst : std_logic;
	signal shift_reg    : std_logic_vector(NUM_SHIFTER_BITS - 1 downto 0);
	signal buffer_data  : std_logic_vector(8 - 1 downto 0);
	signal buffer_valid : std_logic;
	signal buffer_last  : std_logic;
	signal buffer_ready : std_logic;

begin

	input_ready  <= buffer_ready;
	scramble_en  <= input_valid and buffer_ready;
	scramble_rst <= '1' when (scramble_en = '1' and input_last = '1') else '0';

	-- compute the parallel PRBS as in the standard
	pr_scrambler: process(clk) is
	begin
	if rising_edge(clk) then
		if rst = '1' then
			shift_reg    <= INIT_STATE;
			buffer_data  <= (others => '0');
			buffer_valid <= '0';
			buffer_last  <= '0';
		else
			if buffer_ready = '1' then
				buffer_valid <= '0';
				buffer_last  <= '0';
			end if;

			if scramble_en = '1' then
				shift_reg    <= update_lsfr(shift_reg);
				buffer_data  <= scramble(input_data, shift_reg);
				buffer_valid <= input_valid;
				buffer_last  <= input_last;
			end if;

			if scramble_rst = '1' then
				shift_reg <= INIT_STATE;
			end if;
		end if;
	end if;
	end process pr_scrambler;

	-- Add axis buffer for cutting the combinational logic path
	inst_axis_buf : entity frame_decoding.axi4s_buffer
	generic map(
		DATA_WIDTH => 8
	)
	port map(

		clk => clk,
		rst => rst,

		-- Input data handling
		----------------------

		input_ready => buffer_ready,
		input_valid => buffer_valid,
		input_last  => buffer_last,
		input_data  => buffer_data,


		-- Output data handling
		-----------------------
		output_ready => output_ready,
		output_valid => output_valid,
		output_last  => output_last,
		output_data  => output_data
	);

end architecture rtl;
