--! Copyright (C) Creonic GmbH
--!
--! @file
--! @brief  Check the CRC16 and then compute the error flag
--! @author Carlos Velez
--! @date   2021/11/29
--!


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_encoding;
use frame_encoding.pkg_support.all;


entity crc16_checker is
	port (
		clk : in  std_logic;
		rst : in  std_logic;

		input_ready : out std_logic;
		input_valid : in  std_logic;
		input_last  : in  std_logic;
		input_data  : in  std_logic_vector(7 downto 0);

		output_ready : in  std_logic;
		output_valid : out std_logic;
		output_last  : out std_logic;
		output_data  : out std_logic_vector(7 downto 0)
	);
end entity crc16_checker;

architecture arch of crc16_checker is

	-- CRC-16 parameters
	constant NUM_PARITY_BITS_16 : natural := 16;

	-- the checksum polynomial
	-- POLYNOMIAL_16  "0001000000100001";
	constant CRC16_POLY    : std_logic_vector(NUM_PARITY_BITS_16 - 1 downto 0) := x"1021";
	constant INIT_STATE_16 : std_logic_vector(NUM_PARITY_BITS_16 - 1 downto 0) := X"0000";

	signal input_valid_d : std_logic;
	signal input_data_d  : std_logic_vector(7 downto 0);

	signal shift_reg_16   : std_logic_vector(NUM_PARITY_BITS_16 - 1 downto 0);
	signal crc_result_int : std_logic_vector(NUM_PARITY_BITS_16 - 1 downto 0);
	signal crc_cnt        : unsigned(1 downto 0);
	signal append_crc     : std_logic;

	signal input_buf_ready : std_logic;
	signal input_buf_valid : std_logic;
	signal input_buf_last  : std_logic;
	signal input_buf_data  : std_logic_vector(7 downto 0);
begin
	input_ready <= input_buf_ready;

	-- Calculate the CRC-16 for the baseband data field.
	pr_calc_crc16: process(clk) is
		variable v_shift_reg : std_logic_vector(NUM_PARITY_BITS_16 - 1 downto 0);
	begin
	if rising_edge(clk) then
		if rst = '1' then
			input_buf_valid <= '0';
			input_buf_last  <= '0';
			input_buf_data  <= (others => '0');
			input_valid_d   <= '0';
			input_data_d    <= (others => '0');

			append_crc      <= '0';
			crc_result_int  <= (others => '0');
			crc_cnt         <= (others => '0');
			shift_reg_16    <= INIT_STATE_16;
		else

			v_shift_reg := shift_reg_16;

			if input_buf_ready = '1' then
				input_valid_d   <= '0';
				input_data_d    <= (others => '0');
				input_buf_valid <= '0';
				input_buf_last  <= '0';
				input_buf_data  <= (others => '0');

				if input_valid = '1' then
					if input_last = '1' then
						append_crc   <= '1';
						shift_reg_16 <= INIT_STATE_16;
						v_shift_reg  := next_crc(v_shift_reg, CRC16_POLY, input_data(7 downto 6));
					else
						v_shift_reg  := next_crc(v_shift_reg, CRC16_POLY, input_data);
						shift_reg_16 <= v_shift_reg;
					end if;

					crc_result_int <= v_shift_reg;
					input_valid_d  <= input_valid;
					input_data_d   <= input_data;
				end if;

				-- Output data
				input_buf_valid <= input_valid_d;
				input_buf_last  <= '0';
				input_buf_data  <= input_data_d;

				if append_crc = '1' then
					crc_cnt <= crc_cnt + 1;
					input_buf_valid <= '1';

					if crc_cnt = 0 then
						input_buf_data <= input_data_d(7 downto 6) & crc_result_int(15 downto 10);
					elsif crc_cnt = 1 then
						input_buf_data <= crc_result_int(9 downto 2);
					else
						input_buf_data <= crc_result_int(1 downto 0) & "000000";
						input_buf_last <= '1';
						crc_cnt        <= (others => '0');
						append_crc     <= '0';
					end if;
				end if;
			end if;
		end if;
	end if;
	end process pr_calc_crc16;

	-- Store the header which is generated as burst but read at different rate
	inst_fifo : entity frame_encoding.axi4s_fifo
		generic map (
			DISTR_RAM         => false,
			FIFO_DEPTH        => 1024,
			DATA_WIDTH        => 8,
			FULL_THRESHOLD    => 1024,
			USE_OUTPUT_BUFFER => false
		)
		port map (
			clk => clk,
			rst => rst,

			input_ready       => input_buf_ready,
			input_valid       => input_buf_valid,
			input_last        => input_buf_last,
			input_data        => input_buf_data,
			input_almost_full => open,

			output_ready => output_ready,
			output_valid => output_valid,
			output_last  => output_last,
			output_data  => output_data
		);

end arch;
