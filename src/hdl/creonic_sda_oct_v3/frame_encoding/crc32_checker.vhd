--! Copyright (C) Creonic GmbH
--!
--! @file
--! @brief  Check the CRC32 and then compute the error flag
--! @author Carlos Velez
--! @date   2021/12/09
--!


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_encoding;
use frame_encoding.pkg_support.all;


entity crc32_checker is
	port (
		clk : in  std_logic;
		rst : in  std_logic;

		input_ready : out std_logic;
		input_valid : in  std_logic;
		input_last  : in  std_logic;
		input_data  : in  std_logic_vector(7 downto 0);

		output_ready : in  std_logic;
		output_valid : out std_logic;
		output_last  : out std_logic;
		output_data  : out std_logic_vector(7 downto 0)
	);
end entity crc32_checker;

architecture arch of crc32_checker is

	-- CRC-32 parameters
	constant NUM_PARITY_BITS_32 : natural := 32;

	-- the checksum polynomial   --  0   4   C   1   1   D   B   7
	-- POLYNOMIAL_32                "00000100110000010001110110110111";
	constant INIT_STATE_32 : std_logic_vector(NUM_PARITY_BITS_32 - 1 downto 0) := X"00000000";

	signal shift_reg_32     : std_logic_vector(NUM_PARITY_BITS_32 - 1 downto 0);
	signal crc_result_int   : std_logic_vector(NUM_PARITY_BITS_32 - 1 downto 0);
	signal crc_cnt          : unsigned(1 downto 0);
	signal append_crc       : std_logic;
	signal output_valid_int : std_logic;
begin

	input_ready  <= output_ready;
	output_valid <= output_valid_int;

	-- Calculate the CRC-32 for the baseband data field.
	pr_calc_crc32: process(clk) is
		variable v_shift_reg : std_logic_vector(NUM_PARITY_BITS_32 - 1 downto 0);
	begin
	if rising_edge(clk) then
		if rst = '1' then
			output_valid_int <= '0';
			output_data  <= (others => '0');
			output_last  <= '0';

			crc_result_int  <= (others => '0');
			crc_cnt         <= (others => '0');
			append_crc      <= '0';
			shift_reg_32    <= INIT_STATE_32;
		else
			v_shift_reg := shift_reg_32;

			if output_ready = '1' then
				output_valid_int <= '0';
				output_last      <= '0';
				output_data      <= (others => '0');

				if input_valid = '1' then
					v_shift_reg    := crc32(input_data, v_shift_reg);
					shift_reg_32   <= v_shift_reg;
					crc_result_int <= v_shift_reg;

					if input_last = '1' then
						append_crc     <= '1';
						shift_reg_32   <= INIT_STATE_32;
					end if;

					output_valid_int <= input_valid;
					output_data  <= input_data;
					output_last  <= '0';
				end if;

				if append_crc = '1' then
					output_valid_int <= '1';
					output_data  <= crc_result_int(31 downto 24);

					crc_result_int(31 downto 8) <= crc_result_int(23 downto 0);
					crc_cnt <= crc_cnt + 1;

					if crc_cnt = 3 then
						output_last <= '1';
						crc_cnt     <= (others => '0');
						append_crc  <= '0';
					end if;
				end if;
			end if;
		end if;
	end if;
	end process pr_calc_crc32;

end arch;

