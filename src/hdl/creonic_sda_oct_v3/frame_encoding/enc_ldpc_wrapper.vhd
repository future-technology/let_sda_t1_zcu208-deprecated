--!
--! Copyright (C) 2022 Creonic GmbH
--!
--! @file
--! @brief  LDPC encoder for 3gpp rl15 (5G new radio)
--! @author Nhan Nguyen
--! @date   2022/09/01
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_encoding;
library enc_ldpc_struct;

library xilinx_ip;
use xilinx_ip.pkg_components.all;

entity enc_ldpc_wrapper is
port (

	clk            : in  std_logic;
	rst            : in  std_logic;

	ldpc_clk       : in  std_logic;
	ldpc_rst       : in  std_logic;

	-- Configuration input
	----------------------
	cfg_tdata      : in  std_logic_vector(63 downto 0);
	cfg_tvalid     : in  std_logic;
	cfg_tready     : out std_logic;

	-- Input data handling
	----------------------

	input_tdata    : in  std_logic_vector(7 downto 0);
	input_tvalid   : in  std_logic;
	input_tlast    : in  std_logic;
	input_tready   : out std_logic;

	-- Configuration output
	----------------------
	cfg_out_tdata  : out std_logic_vector(63 downto 0);
	cfg_out_tvalid : out std_logic;
	cfg_out_tready : in  std_logic;

	-- Output data handling
	-----------------------
	result_tdata   : out std_logic_vector(7 downto 0);
	result_tvalid  : out std_logic;
	result_tlast   : out std_logic;
	result_tready  : in  std_logic
);


end entity enc_ldpc_wrapper;


architecture rtl of enc_ldpc_wrapper is

	component enc_ldpc_struct_top is
	port (

		clk            : in  std_logic;
		rst            : in  std_logic;

		-- Configuration input
		----------------------
		cfg_tdata      : in  std_logic_vector(63 downto 0);
		cfg_tvalid     : in  std_logic;
		cfg_tready     : out std_logic;

		-- Input data handling
		----------------------

		input_tdata    : in  std_logic_vector(15 downto 0);
		input_tvalid   : in  std_logic;
		input_tlast    : in  std_logic;
		input_tready   : out std_logic;

		-- Configuration output
		----------------------
		cfg_out_tdata  : out std_logic_vector(63 downto 0);
		cfg_out_tvalid : out std_logic;
		cfg_out_tready : in  std_logic;

		-- Output data handling
		-----------------------
		result_tdata   : out std_logic_vector(15 downto 0);
		result_tvalid  : out std_logic;
		result_tlast   : out std_logic;
		result_tready  : in  std_logic
	);
	end component;


	signal rst_n      : std_logic;
	signal ldpc_rst_n : std_logic;

	signal input_dwidth_out_tvalid : std_logic;
	signal input_dwidth_out_tlast  : std_logic;
	signal input_dwidth_out_tready : std_logic;
	signal input_dwidth_out_tdata  : std_logic_vector(15 downto 0);

	signal ldpc_in_tvalid          : std_logic;
	signal ldpc_in_tlast           : std_logic;
	signal ldpc_in_tready          : std_logic;
	signal ldpc_in_tdata           : std_logic_vector(15 downto 0);

	signal cfg_ldpc_in_tdata       : std_logic_vector(63 downto 0);
	signal cfg_ldpc_in_tvalid      : std_logic;
	signal cfg_ldpc_in_tready      : std_logic;

	signal ldpc_out_tvalid         : std_logic;
	signal ldpc_out_tlast          : std_logic;
	signal ldpc_out_tready         : std_logic;
	signal ldpc_out_tdata          : std_logic_vector(15 downto 0);

	signal cfg_ldpc_out_tdata      : std_logic_vector(63 downto 0);
	signal cfg_ldpc_out_tvalid     : std_logic;
	signal cfg_ldpc_out_tready     : std_logic;

	signal output_dwidth_in_tvalid : std_logic;
	signal output_dwidth_in_tlast  : std_logic;
	signal output_dwidth_in_tready : std_logic;
	signal output_dwidth_in_tdata  : std_logic_vector(15 downto 0);

begin

	rst_n      <= not rst;
	ldpc_rst_n <= not ldpc_rst;

	-- 8 to 16 bit input data
	inst_dwidth_8_to_16 : dwidth_8_to_16
	port map(
		aclk          => clk,
		aresetn       => rst_n,

		s_axis_tvalid => input_tvalid,
		s_axis_tready => input_tready,
		s_axis_tdata  => input_tdata,
		s_axis_tlast  => input_tlast,

		m_axis_tvalid => input_dwidth_out_tvalid,
		m_axis_tready => input_dwidth_out_tready,
		m_axis_tdata  => input_dwidth_out_tdata,
		m_axis_tlast  => input_dwidth_out_tlast
	);

	-- CDC : from 330 MHz (clk) domain to 200 MHz (ldpc_clk)
	inst_axis_16bit_clk_conv : axis_16bit_clk_conv
	port map(
		s_axis_aclk    => clk,
		s_axis_aresetn => rst_n,

		s_axis_tvalid  => input_dwidth_out_tvalid,
		s_axis_tready  => input_dwidth_out_tready,
		s_axis_tdata   => input_dwidth_out_tdata,
		s_axis_tlast   => input_dwidth_out_tlast,

		m_axis_aclk    => ldpc_clk,
		m_axis_aresetn => ldpc_rst_n,

		m_axis_tvalid  => ldpc_in_tvalid,
		m_axis_tready  => ldpc_in_tready,
		m_axis_tdata   => ldpc_in_tdata,
		m_axis_tlast   => ldpc_in_tlast
	);
	inst_axis_64bit_clk_conv : axis_64bit_clk_conv
	port map(
		s_axis_aclk    => clk,
		s_axis_aresetn => rst_n,

		s_axis_tvalid  => cfg_tvalid,
		s_axis_tready  => cfg_tready,
		s_axis_tdata   => cfg_tdata,

		m_axis_aclk    => ldpc_clk,
		m_axis_aresetn => ldpc_rst_n,

		m_axis_tvalid  => cfg_ldpc_in_tvalid,
		m_axis_tready  => cfg_ldpc_in_tready,
		m_axis_tdata   => cfg_ldpc_in_tdata
	);


	inst_enc_ldpc_struct_top : enc_ldpc_struct_top
	port map(

		clk             => ldpc_clk,
		rst             => ldpc_rst,

		-- Configuration input
		----------------------
		cfg_tdata       => cfg_ldpc_in_tdata,
		cfg_tvalid      => cfg_ldpc_in_tvalid,
		cfg_tready      => cfg_ldpc_in_tready,

		-- Input data handling
		----------------------

		input_tdata     => ldpc_in_tdata,
		input_tvalid    => ldpc_in_tvalid,
		input_tlast     => ldpc_in_tlast,
		input_tready    => ldpc_in_tready,

		-- Configuration output
		----------------------
		cfg_out_tdata   => cfg_ldpc_out_tdata,
		cfg_out_tvalid  => cfg_ldpc_out_tvalid,
		cfg_out_tready  => cfg_ldpc_out_tready,

		-- Output data handling
		-----------------------
		result_tdata    => ldpc_out_tdata,
		result_tvalid   => ldpc_out_tvalid,
		result_tlast    => ldpc_out_tlast,
		result_tready   => ldpc_out_tready
	);

	-- CDC : from 200 MHz (clk) domain to 330 MHz (ldpc_clk)
	inst_axis_16bit_clk_conv_out : axis_16bit_clk_conv
	port map(
		s_axis_aclk    => ldpc_clk,
		s_axis_aresetn => ldpc_rst_n,

		s_axis_tvalid  => ldpc_out_tvalid,
		s_axis_tready  => ldpc_out_tready,
		s_axis_tdata   => ldpc_out_tdata,
		s_axis_tlast   => ldpc_out_tlast,

		m_axis_aclk    => clk,
		m_axis_aresetn => rst_n,

		m_axis_tvalid  => output_dwidth_in_tvalid,
		m_axis_tready  => output_dwidth_in_tready,
		m_axis_tdata   => output_dwidth_in_tdata,
		m_axis_tlast   => output_dwidth_in_tlast
	);

	inst_axis_64bit_clk_conv_out : axis_64bit_clk_conv
	port map(
		s_axis_aclk    => ldpc_clk,
		s_axis_aresetn => ldpc_rst_n,

		s_axis_tvalid  => cfg_ldpc_out_tvalid,
		s_axis_tready  => cfg_ldpc_out_tready,
		s_axis_tdata   => cfg_ldpc_out_tdata,

		m_axis_aclk    => clk,
		m_axis_aresetn => rst_n,

		m_axis_tvalid  => cfg_out_tvalid,
		m_axis_tready  => cfg_out_tready,
		m_axis_tdata   => cfg_out_tdata
	);

	-- 16 to 8 bit input data
	inst_dwidth_16_to_8 : dwidth_16_to_8
	port map(
		aclk          => clk,
		aresetn       => rst_n,

		s_axis_tvalid => output_dwidth_in_tvalid,
		s_axis_tready => output_dwidth_in_tready,
		s_axis_tdata  => output_dwidth_in_tdata,
		s_axis_tlast  => output_dwidth_in_tlast,

		m_axis_tvalid => result_tvalid,
		m_axis_tready => result_tready,
		m_axis_tdata  => result_tdata,
		m_axis_tlast  => result_tlast
	);


end architecture rtl;
