--!
--! Copyright (C) Creonic GmbH
--!
--! @file
--! @brief
--! @author Carlos Velez
--! @date   2021/11/25
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_encoding;
use frame_encoding.pkg_support.all;


entity frame_demux is
	port(
		clk : in std_logic;
		rst : in std_logic;

		input_ready : out std_logic;
		input_valid : in  std_logic;
		input_last  : in  std_logic;
		input_data  : in  std_logic_vector(7 downto 0);

		header_ready : in  std_logic;
		header_valid : out std_logic;
		header_last  : out std_logic;
		header_data  : out std_logic_vector(7 downto 0);

		payload_ready : in  std_logic;
		payload_valid : out std_logic;
		payload_last  : out std_logic;
		payload_data  : out std_logic_vector(7 downto 0)
	);
end entity frame_demux;

architecture rtl of frame_demux is

	constant HEADER_LEN  : natural := 18;
	constant PAYLOAD_LEN : natural := 1052;
	constant FRAME_LEN   : natural := PAYLOAD_LEN + HEADER_LEN;

	type t_data_read_state is (IDLE, GET_HEADER, GET_PAYLOAD, WAIT_BUFFER);
	signal data_read_state   : t_data_read_state;
	signal input_buf_ready   : std_logic;
	signal input_buf_valid   : std_logic;
	signal input_buf_last    : std_logic;
	signal input_buf_data    : std_logic_vector(7 downto 0);
	signal payload_valid_int : std_logic;
	signal input_cnt         : unsigned(10 downto 0);
	signal input_data_reg    : std_logic_vector(7 downto 0);
begin

	input_buf_ready <= header_ready when data_read_state = GET_HEADER else
	                   payload_ready when data_read_state = GET_PAYLOAD else
							 '0';
	payload_valid <= payload_valid_int;

	-- Add axis buffer for cutting the combinational logic path
	inst_buffer_data_in : entity frame_encoding.axi4s_buffer
	generic map(
		DATA_WIDTH => 8
	)
	port map(
		clk => clk,
		rst => rst,

		input_ready => input_ready,
		input_valid => input_valid,
		input_last  => input_last,
		input_data  => input_data,

		output_ready => input_buf_ready,
		output_valid => input_buf_valid,
		output_last  => input_buf_last,
		output_data  => input_buf_data
	);

	-- FSM for reading the input data
	pr_input_data_read : process(clk) is
	begin
	if rising_edge(clk) then
		if rst = '1' then
			header_data   <= (others => '0');
			header_valid  <= '0';
			header_last   <= '0';
			payload_data  <= (others => '0');
			payload_valid_int <= '0';
			payload_last  <= '0';

			input_cnt       <= (others => '0');
			input_data_reg  <= (others => '0');
			data_read_state <= IDLE;
		else
			case data_read_state is

			when IDLE =>
				if payload_ready = '1' then
					data_read_state <= GET_HEADER;
				end if;

			when GET_HEADER =>
				if input_buf_ready = '1' then
					header_valid <= '0';
					header_last  <= '0';
					header_data  <= (others => '0');

					if input_buf_valid = '1' then
						input_cnt      <= input_cnt + 1;
						input_data_reg <= input_buf_data;
						header_valid   <= '1';

						if input_cnt = HEADER_LEN - 1 then
							header_last <= '1';
							header_data <= input_buf_data(7 downto 6) & "000000";
							data_read_state <= GET_PAYLOAD;
						else
							header_data <= input_buf_data;
						end if;
					end if;
				end if;

			when GET_PAYLOAD =>
				if header_ready = '1' then
					header_valid <= '0';
					header_last  <= '0';
					header_data  <= (others => '0');
				end if;

				if payload_ready = '1' then
					payload_valid_int <= '0';
					payload_last      <= '0';

					if input_buf_valid = '1' and input_buf_ready = '1' then
						payload_data      <= input_data_reg(5 downto 0) & input_buf_data(7 downto 6);
						payload_valid_int <= '1';
						input_cnt         <= input_cnt + 1;
						input_data_reg    <= input_buf_data;

						if input_cnt = FRAME_LEN - 1 then
							payload_last    <= '1';
							data_read_state <= WAIT_BUFFER;
						end if;
					end if;
				end if;

			when WAIT_BUFFER =>
				input_cnt <= input_cnt + 1;

				if payload_ready = '1' then
					payload_valid_int <= '0';
					payload_last      <= '0';
				end if;

				-- Add this counter here because the added crc32 and latency
				if input_cnt >= FRAME_LEN + 5 then
					input_cnt <= (others => '0');
					data_read_state <= IDLE;
				end if;
			end case;
		end if;
	end if;
	end process pr_input_data_read;

end architecture rtl;
