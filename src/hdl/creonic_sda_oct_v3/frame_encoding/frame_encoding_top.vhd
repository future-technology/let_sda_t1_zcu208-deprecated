--!
--! Copyright (C) 2021 Creonic GmbH
--!
--! @file
--! @brief
--! @author Nhan Nguyen
--! @date   2021/02/25
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_encoding;
use frame_encoding.pkg_support.all;


entity frame_encoding_top is
	generic (
		USE_XILINX_FEC : boolean := true
	);
	port (
		ldpc_clk : in  std_logic;
		ldpc_rst : in  std_logic;
		clk      : in  std_logic;
		rst      : in  std_logic;

		preamble : in  std_logic_vector(63 downto 0);

		s_axis_ctrl_tready : out std_logic;
		s_axis_ctrl_tvalid : in  std_logic;
		s_axis_ctrl_tdata  : in  std_logic_vector(2 downto 0);

		-- Input data handling
		----------------------
		s_axis_tready : out std_logic;
		s_axis_tvalid : in  std_logic;
		s_axis_tlast  : in  std_logic;
		s_axis_tdata  : in  std_logic_vector(7 downto 0);

		-- Output data handling
		-----------------------
		m_axis_tready : in  std_logic;
		m_axis_tvalid : out std_logic;
		m_axis_tlast  : out std_logic;
		m_axis_tdata  : out std_logic_vector(7 downto 0)
	);
end entity frame_encoding_top;


architecture rtl of frame_encoding_top is

	signal header_ready : std_logic;
	signal header_valid : std_logic;
	signal header_last  : std_logic;
	signal header_data  : std_logic_vector(7 downto 0);

	signal header_enc_ready : std_logic;
	signal header_enc_valid : std_logic;
	signal header_enc_last  : std_logic;
	signal header_enc_data  : std_logic_vector(7 downto 0);

	signal header_enc_out_ready : std_logic;
	signal header_enc_out_valid : std_logic;
	signal header_enc_out_last  : std_logic;
	signal header_enc_out_data  : std_logic_vector(7 downto 0);

	signal payload_ready : std_logic;
	signal payload_valid : std_logic;
	signal payload_last  : std_logic;
	signal payload_data  : std_logic_vector(7 downto 0);

	signal enc_input_ready : std_logic;
	signal enc_input_valid : std_logic;
	signal enc_input_last  : std_logic;
	signal enc_input_data  : std_logic_vector(7 downto 0);

	signal enc_output_ready : std_logic;
	signal enc_output_valid : std_logic;
	signal enc_output_last  : std_logic;
	signal enc_output_data  : std_logic_vector(7 downto 0);

	signal scrambler_in_ready : std_logic;
	signal scrambler_in_valid : std_logic;
	signal scrambler_in_last  : std_logic;
	signal scrambler_in_data  : std_logic_vector(7 downto 0);

	signal scrambler_out_ready : std_logic;
	signal scrambler_out_valid : std_logic;
	signal scrambler_out_last  : std_logic;
	signal scrambler_out_data  : std_logic_vector(7 downto 0);

	signal m_axis_tdata_int : std_logic_vector(7 downto 0);

begin

	-- Xilinx Gigabit Tranceiver send the LSB first
	pr_out_data : process(m_axis_tdata_int) is
	begin
		for i in 0 to 7 loop
			m_axis_tdata(i) <= m_axis_tdata_int(7 - i);
		end loop;
	end process;

	inst_frame_demux : entity frame_encoding.frame_demux
	port map(
		clk => clk,
		rst => rst,

		input_ready => s_axis_tready,
		input_valid => s_axis_tvalid,
		input_last  => s_axis_tlast,
		input_data  => s_axis_tdata,

		header_ready => header_ready,
		header_valid => header_valid,
		header_last  => header_last,
		header_data  => header_data,

		payload_ready => payload_ready,
		payload_valid => payload_valid,
		payload_last  => payload_last,
		payload_data  => payload_data
	);

	-- CRC32 for payload data
	inst_crc32 : entity frame_encoding.crc32_checker
	port map(
		clk     => clk,
		rst     => rst,

		input_ready => payload_ready,
		input_valid => payload_valid,
		input_last  => payload_last,
		input_data  => payload_data,

		output_ready => enc_input_ready,
		output_valid => enc_input_valid,
		output_last  => enc_input_last,
		output_data  => enc_input_data
	);

	inst_payload_encoder : entity frame_encoding.payload_encoder
	generic map(
		USE_XILINX_FEC => USE_XILINX_FEC
	)
	port map(

		core_clk => ldpc_clk,
		core_rst => ldpc_rst,
		clk      => clk,
		rst      => rst,

		ctrl_ready => s_axis_ctrl_tready,
		ctrl_valid => s_axis_ctrl_tvalid,
		ctrl_data  => s_axis_ctrl_tdata,

		-- Input data handling
		----------------------
		input_ready => enc_input_ready,
		input_valid => enc_input_valid,
		input_last  => enc_input_last,
		input_data  => enc_input_data,

		status_ready => '1',
		status_valid => open,
		status_data  => open,

		-- Output data handling
		-----------------------
		output_ready => enc_output_ready,
		output_valid => enc_output_valid,
		output_last  => enc_output_last,
		output_data  => enc_output_data
	);


	-- CRC16 for header data
	inst_crc16 : entity frame_encoding.crc16_checker
	port map(
		clk => clk,
		rst => rst,

		input_ready => header_ready,
		input_valid => header_valid,
		input_last  => header_last,
		input_data  => header_data,

		output_ready => header_enc_ready,
		output_valid => header_enc_valid,
		output_last  => header_enc_last,
		output_data  => header_enc_data
	);

	inst_header_encoder : entity frame_encoding.header_encoder
	port map(

		clk          => clk,
		rst          => rst,

		-- Input data handling
		----------------------

		input_ready  => header_enc_ready,
		input_valid  => header_enc_valid,
		input_last   => header_enc_last,
		input_data   => header_enc_data,

		-- Output data handling
		-----------------------
		output_ready => header_enc_out_ready,
		output_valid => header_enc_out_valid,
		output_last  => header_enc_out_last,
		output_data  => header_enc_out_data
	);


	-- Combine the header and payload data
	inst_frame_mux : entity frame_encoding.frame_mux
	port map(
		clk => clk,
		rst => rst,

		header_ready => header_enc_out_ready,
		header_valid => header_enc_out_valid,
		header_last  => header_enc_out_last,
		header_data  => header_enc_out_data,

		payload_ready => enc_output_ready,
		payload_valid => enc_output_valid,
		payload_last  => enc_output_last,
		payload_data  => enc_output_data,

		frame_ready => scrambler_in_ready,
		frame_valid => scrambler_in_valid,
		frame_last  => scrambler_in_last,
		frame_data  => scrambler_in_data
	);

	-- Scrambler
	inst_scrambler : entity frame_encoding.scrambling
	port map(
		clk => clk,
		rst => rst,

		input_ready => scrambler_in_ready,
		input_valid => scrambler_in_valid,
		input_last  => scrambler_in_last,
		input_data  => scrambler_in_data,

		output_ready => scrambler_out_ready,
		output_valid => scrambler_out_valid,
		output_last  => scrambler_out_last,
		output_data  => scrambler_out_data
	);

	-- Combine data and preamble
	inst_framing : entity frame_encoding.framing
	port map(
		clk      => clk,
		rst      => rst,

		preamble => preamble,

		input_ready => scrambler_out_ready,
		input_valid => scrambler_out_valid,
		input_last  => scrambler_out_last,
		input_data  => scrambler_out_data,

		output_ready => m_axis_tready,
		output_valid => m_axis_tvalid,
		output_last  => m_axis_tlast,
		output_data  => m_axis_tdata_int
	);

end rtl;
