--!
--! Copyright (C) 2020 Creonic GmbH
--!
--! @file
--! @brief
--! @author Nhan Nguyen
--! @date   2020/02/28
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_encoding;
use frame_encoding.pkg_support.all;


entity frame_mux is
	port(
		clk : in std_logic;
		rst : in std_logic;

		header_ready : out std_logic;
		header_valid : in  std_logic;
		header_last  : in  std_logic;
		header_data  : in  std_logic_vector(8 - 1 downto 0);

		payload_ready : out std_logic;
		payload_valid : in  std_logic;
		payload_last  : in  std_logic;
		payload_data  : in  std_logic_vector(8 - 1 downto 0);

		frame_ready : in  std_logic;
		frame_valid : out std_logic;
		frame_last  : out std_logic;
		frame_data  : out std_logic_vector(8 - 1 downto 0)
	);
end entity frame_mux;

architecture rtl of frame_mux is

	type t_mux_fsm is (IDLE, GET_HEADER, GET_PAYLOAD);
	signal mux_fsm : t_mux_fsm;
	signal header_ready_int  : std_logic;
	signal payload_ready_int : std_logic;
	signal transmit_start    : std_logic;

	signal buffer_ready : std_logic;
	signal buffer_valid : std_logic;
	signal buffer_last  : std_logic;
	signal buffer_data  : std_logic_vector(7 downto 0);
begin

	buffer_data  <= header_data   when mux_fsm = GET_HEADER else payload_data;
	buffer_valid <= header_valid  when mux_fsm = GET_HEADER else
	                payload_valid when mux_fsm = GET_PAYLOAD else '0';
	header_ready_int  <= buffer_ready when mux_fsm = GET_HEADER else '0';
	payload_ready_int <= buffer_ready when mux_fsm = GET_PAYLOAD else '0';
	header_ready   <= header_ready_int;
	payload_ready  <= payload_ready_int;

	buffer_last <= payload_last when mux_fsm = GET_PAYLOAD else '0';

	-- simple state machine for the mux
	pr_scrambling: process(clk) is
	begin
	if rising_edge(clk) then
		if rst = '1' then
			mux_fsm <= IDLE;
			transmit_start <= '0';
		else

			if payload_valid = '1' then
				transmit_start <= '1';
			end if;

			case mux_fsm is
			when IDLE =>
				if transmit_start = '1' then
					mux_fsm <= GET_HEADER;
				end if;

			when GET_HEADER =>
				if header_valid = '1' and header_ready_int = '1' then
					if header_last = '1' then
						mux_fsm <= GET_PAYLOAD;
					end if;
				end if;

			when GET_PAYLOAD =>
				if payload_valid = '1' and payload_ready_int = '1' then
					if payload_last = '1' then
						mux_fsm <= GET_HEADER;
					end if;
				end if;
			end case;
		end if;
	end if;
	end process pr_scrambling;

	-- Add axis buffer for cutting the combinational logic path
	inst_axis_buf : entity frame_encoding.axi4s_buffer
	generic map(
		DATA_WIDTH => 8
	)
	port map(

		clk => clk,
		rst => rst,

		-- Input data handling
		----------------------

		input_ready => buffer_ready,
		input_valid => buffer_valid,
		input_last  => buffer_last,
		input_data  => buffer_data,


		-- Output data handling
		-----------------------
		output_ready => frame_ready,
		output_valid => frame_valid,
		output_last  => frame_last,
		output_data  => frame_data
	);

end architecture rtl;
