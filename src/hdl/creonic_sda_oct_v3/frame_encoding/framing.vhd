--!
--! Copyright (C) 2020 Creonic GmbH
--!
--! @file
--! @brief
--! @author Nhan Nguyen
--! @date   2020/02/28
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_encoding;
use frame_encoding.pkg_support.all;


entity framing is
	port(
		clk : in std_logic;
		rst : in std_logic;

		preamble : in  std_logic_vector(63 downto 0);

		input_ready : out std_logic;
		input_valid : in  std_logic;
		input_last  : in  std_logic;
		input_data  : in  std_logic_vector(7 downto 0);

		output_ready : in  std_logic;
		output_valid : out std_logic;
		output_last  : out std_logic;
		output_data  : out std_logic_vector(7 downto 0)
	);
end entity framing;

architecture rtl of framing is
	constant BW_PREAMBLE        : natural := 64;
	constant NUM_BYTES_PREAMBLE : natural := BW_PREAMBLE / 8;

	type t_mux_fsm is (IDLE, GET_PREAMBLE, GET_INPUT);
	signal mux_fsm : t_mux_fsm;
	signal input_ready_int : std_logic;
	signal preamble_cnt    : unsigned(2 downto 0);
	signal transmit_start  : std_logic;
	signal preamble_reg    : std_logic_vector(BW_PREAMBLE - 1 downto 0);
	signal preamble_ready  : std_logic;
	signal preamble_valid  : std_logic;
	signal preamble_last   : std_logic;
	signal preamble_data   : std_logic_vector(7 downto 0);

	signal buffer_ready : std_logic;
	signal buffer_valid : std_logic;
	signal buffer_last  : std_logic;
	signal buffer_data  : std_logic_vector(7 downto 0);
begin

	buffer_data  <= preamble_data  when mux_fsm = GET_PREAMBLE else input_data;
	buffer_valid <= preamble_valid when mux_fsm = GET_PREAMBLE else
	                input_valid    when mux_fsm = GET_INPUT    else '0';
	buffer_last  <= input_last;
	preamble_ready  <= buffer_ready when mux_fsm = GET_PREAMBLE else '0';
	input_ready_int <= buffer_ready when mux_fsm = GET_INPUT else '0';
	input_ready  <= input_ready_int;

	-- simple state machine for the mux
	pr_framing: process(clk) is
	begin
	if rising_edge(clk) then
		if rst = '1' then
			mux_fsm <= IDLE;
			preamble_data  <= (others => '0');
			preamble_reg   <= (others => '0');
			preamble_cnt   <= (others => '0');
			preamble_valid <= '0';
			transmit_start <= '0';
			preamble_last  <= '0';
		else
			if input_valid = '1' then
				transmit_start <= '1';
			end if;

			case mux_fsm is
			when IDLE =>
				if transmit_start = '1' then
					mux_fsm        <= GET_PREAMBLE;
					preamble_valid <= '1';
					preamble_data  <= preamble(BW_PREAMBLE - 1 downto BW_PREAMBLE - 8);
					preamble_reg(BW_PREAMBLE - 1 downto 8) <= preamble(BW_PREAMBLE - 9 downto 0);
				end if;

			when GET_PREAMBLE =>
				preamble_valid <= '1';

				if preamble_valid = '1' and preamble_ready = '1' then
					preamble_data <= preamble_reg(BW_PREAMBLE - 1 downto BW_PREAMBLE - 8);
					preamble_reg(BW_PREAMBLE - 1 downto 8) <= preamble_reg(BW_PREAMBLE - 9 downto 0);
					preamble_cnt <= preamble_cnt + 1;

					if preamble_cnt = NUM_BYTES_PREAMBLE - 2 then
						preamble_last <= '1';
					end if;

					if preamble_last = '1' then
						preamble_last  <= '0';
						preamble_valid <= '0';
						mux_fsm <= GET_INPUT;
					end if;
				end if;
			when GET_INPUT =>
				if input_valid = '1' and input_ready_int = '1' then
					if input_last = '1' then
						mux_fsm        <= GET_PREAMBLE;
						preamble_reg   <= preamble;
						preamble_valid <= '1';
						preamble_data  <= preamble(BW_PREAMBLE - 1 downto BW_PREAMBLE - 8);
						preamble_reg(BW_PREAMBLE - 1 downto 8) <= preamble(BW_PREAMBLE - 9 downto 0);
					end if;
				end if;
			end case;
		end if;
	end if;
	end process pr_framing;

	-- Add axis buffer for cutting the combinational logic path
	inst_axis_buf : entity frame_encoding.axi4s_buffer
	generic map(
		DATA_WIDTH => 8
	)
	port map(

		clk => clk,
		rst => rst,

		-- Input data handling
		----------------------

		input_ready => buffer_ready,
		input_valid => buffer_valid,
		input_last  => buffer_last,
		input_data  => buffer_data,


		-- Output data handling
		-----------------------
		output_ready => output_ready,
		output_valid => output_valid,
		output_last  => output_last,
		output_data  => output_data
	);

end architecture rtl;
