--!
--! Copyright (C) Creonic GmbH
--!
--! @file
--! @brief  avalon-st wrapper for the encoder
--! @author Carlos Velez
--! @date   2021/11/16
--!
--! The encoder expects the output ready to be asserted during the transmission
--! of a whole block.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_encoding;
use frame_encoding.pkg_support.all;
use frame_encoding.pkg_header_support.all;

entity header_encoder is
	port (

	clk          : in  std_logic;
	rst          : in  std_logic;

	-- Input data handling
	----------------------

	input_ready  : out std_logic;
	input_valid  : in  std_logic;
	input_last   : in  std_logic;
	input_data   : in  std_logic_vector(8 - 1 downto 0);

	-- Output data handling
	-----------------------
	output_ready : in  std_logic;
	output_valid : out std_logic;
	output_last  : out std_logic;
	output_data  : out std_logic_vector(8 - 1 downto 0)
);
end entity header_encoder;


architecture rtl of header_encoder is

	constant GENERATORS : t_generators := calc_generators;

	constant HEADER_SIZE        : natural := 160; -- bits
	constant NUM_CODED_PER_ITER : natural := 8 * NUMBER_PARITY_BITS;
	constant TOTAL_CODED_BITS   : natural := HEADER_SIZE * NUMBER_PARITY_BITS;

	signal input_ready_int  : std_logic;
	signal output_valid_int : std_logic;
	signal data_in_valid    : std_logic;
	signal data_in_last     : std_logic;
	signal data_in          : std_logic_vector(8 - 1 downto 0);
	signal sreg             : std_logic_vector(CONSTRAINT_LENGTH - 2 downto 0);
	signal coded_bits_valid : std_logic;
	signal coded_bits       : std_logic_vector(NUM_CODED_PER_ITER - 1 downto 0);
	signal input_cnt        : unsigned(7 downto 0);
	signal bits_out_cnt     : unsigned(9 downto 0);
	signal bytes_per_word_cnt : natural range 0 to NUMBER_PARITY_BITS;

begin

	input_ready  <= input_ready_int;
	output_valid <= output_valid_int;
	output_data  <= coded_bits(NUM_CODED_PER_ITER - 1 downto NUM_CODED_PER_ITER - 8);

	-- input data handling for the encoder
	pr_in_ctrl: process(clk)
		variable v_sreg : std_logic_vector(CONSTRAINT_LENGTH - 2 downto 0);
		variable v_bits : std_logic_vector(NUMBER_PARITY_BITS - 1 downto 0);
	begin
	if rising_edge(clk) then
		if rst = '1' then
			sreg             <= (others => '0');
			input_cnt        <= (others => '0');
			data_in          <= (others => '0');
			coded_bits       <= (others => '0');
			coded_bits_valid <= '0';
			data_in_valid    <= '0';
			data_in_last     <= '0';
			input_ready_int  <= '0';

			bits_out_cnt       <= (others => '0');
			bytes_per_word_cnt <= 0;
			output_valid_int   <= '0';
			output_last        <= '0';
		else

			if output_ready = '1' and input_cnt = 0 and bits_out_cnt = 0 then
				input_ready_int  <= '1';
			end if;

			-- Store input
			if input_ready_int = '1' and input_valid = '1' then
				data_in_valid   <= '1';
				data_in_last    <= input_last;
				data_in         <= input_data;
				input_ready_int <= '0';
				input_cnt       <= input_cnt + 8;

				if input_cnt = 152 then
					input_cnt <= (others => '0');
				end if;
			end if;

			-- Output handshake -> clear output
			if output_ready = '1' then
				output_valid_int <= '0';

				-- If no new data available and end of count, clear flag
				if data_in_valid = '0' and bytes_per_word_cnt = NUMBER_PARITY_BITS - 1 then
					coded_bits_valid <= '0';
				end if;

				if coded_bits_valid = '1' then
					if bytes_per_word_cnt = NUMBER_PARITY_BITS - 1 then
						bytes_per_word_cnt <= 0;
						output_valid_int   <= '0';
					else
						output_valid_int <= '1';
						bytes_per_word_cnt <= bytes_per_word_cnt + 1;
						coded_bits <= coded_bits(NUM_CODED_PER_ITER - 9 downto 0) & x"00";

						if bytes_per_word_cnt = NUMBER_PARITY_BITS - 3 and data_in_last = '0' then
							input_ready_int <= '1';
						end if;
					end if;

					bits_out_cnt <= bits_out_cnt + 8;

					if bits_out_cnt = TOTAL_CODED_BITS - 8 then
						output_last  <= '0';
						bits_out_cnt <= (others => '0');
					elsif bits_out_cnt = TOTAL_CODED_BITS - 16 then
						output_last  <= '1';
					end if;
				end if;

				-- Process input data (generate 48 bits -> 6 bits x 1 data_bit)
				if data_in_valid = '1' and (bytes_per_word_cnt = 0 or bytes_per_word_cnt = NUMBER_PARITY_BITS - 1) then
					data_in_valid <= '0';
					v_sreg := sreg;

					for i in 7 downto 0 loop
						for j in NUMBER_PARITY_BITS - 1 downto 0 loop
							v_bits(j) := data_in(i);

							for m in sreg'length - 1 downto 0 loop
								if GENERATORS(j)(m) = '1' then
									v_bits(j) := v_bits(j) xor v_sreg(m);
								end if;
							end loop;
						end loop;

						coded_bits(NUMBER_PARITY_BITS*(i + 1) - 1 downto NUMBER_PARITY_BITS*i) <= v_bits;
						v_sreg := data_in(i) & v_sreg(v_sreg'left downto 1);
					end loop;

					sreg <= v_sreg;
					coded_bits_valid <= '1';
					output_valid_int <= '1';
				end if;
			end if;

		end if;
	end if;
	end process pr_in_ctrl;

end architecture rtl;
