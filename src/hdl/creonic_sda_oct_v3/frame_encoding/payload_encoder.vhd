--!
--! Copyright (C) Creonic GmbH
--!
--! @file
--! @brief  Wrapper for the encoder
--! @author Carlos Velez
--! @date   2021/12/21
--!
--! The encoder expects the output ready to be asserted during the transmission
--! of a whole block.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_encoding;
library xilinx_ip;
use xilinx_ip.pkg_components.all;

library enc_ldpc_struct;

entity payload_encoder is
	generic (
		USE_XILINX_FEC : boolean := true
	);
	port (

	core_clk : in  std_logic;
	core_rst : in  std_logic;
	clk      : in  std_logic;
	rst      : in  std_logic;

	ctrl_ready : out std_logic;
	ctrl_valid : in  std_logic;
	ctrl_data  : in  std_logic_vector(2 downto 0);

	-- Input data handling
	----------------------

	input_ready : out std_logic;
	input_valid : in  std_logic;
	input_last  : in  std_logic;
	input_data  : in  std_logic_vector(7 downto 0);

	status_ready : in  std_logic;
	status_valid : out std_logic;
	status_data  : out std_logic_vector(63 downto 0);

	-- Output data handling
	-----------------------
	output_ready : in  std_logic;
	output_valid : out std_logic;
	output_last  : out std_logic;
	output_data  : out std_logic_vector(7 downto 0)
);
end entity payload_encoder;


architecture rtl of payload_encoder is

	constant R084_22_26 : std_logic_vector(7 - 1 downto 0) := "0000000";
	constant R075_22_29 : std_logic_vector(7 - 1 downto 0) := "0000011";
	constant R066_22_33 : std_logic_vector(7 - 1 downto 0) := "0000111";
	constant R050_22_44 : std_logic_vector(7 - 1 downto 0) := "0010010";

	component ldpc_enc is
	port (
		reset_n    : in  std_logic;
		core_clk   : in  std_logic;
		s_axi_aclk : in  std_logic;
		interrupt  : out std_logic;

		s_axis_ctrl_aclk   : in  std_logic;
		s_axis_ctrl_tready : out std_logic;
		s_axis_ctrl_tvalid : in  std_logic;
		s_axis_ctrl_tdata  : in  std_logic_vector(39 downto 0);

		s_axis_din_aclk   : in  std_logic;
		s_axis_din_tready : out std_logic;
		s_axis_din_tvalid : in  std_logic;
		s_axis_din_tlast  : in  std_logic;
		s_axis_din_tdata  : in  std_logic_vector(127 downto 0);

		m_axis_status_aclk   : in  std_logic;
		m_axis_status_tready : in  std_logic;
		m_axis_status_tvalid : out std_logic;
		m_axis_status_tdata  : out std_logic_vector(39 downto 0);

		m_axis_dout_aclk   : in  std_logic;
		m_axis_dout_tready : in  std_logic;
		m_axis_dout_tvalid : out std_logic;
		m_axis_dout_tlast  : out std_logic;
		m_axis_dout_tdata  : out std_logic_vector(127 downto 0)
	);
	end component ldpc_enc;

	signal rst_n           : std_logic;
	signal apply_new_cfg   : std_logic;

	signal bypass_ldpc_in_ready : std_logic;
	signal bypass_ldpc_in_valid : std_logic;
	signal bypass_ldpc_in       : std_logic_vector(0 downto 0);

	signal bypass_ldpc_out_ready : std_logic;
	signal bypass_ldpc_out_valid : std_logic;
	signal bypass_ldpc_out_data  : std_logic_vector(0 downto 0);
	signal bypass_ldpc_out       : std_logic;

	signal ctrl_ldpc_ready : std_logic;
	signal ctrl_ldpc_valid : std_logic;
	signal ctrl_ldpc_data  : std_logic_vector(63 downto 0);

	signal input_frame_buf_ready : std_logic;
	signal input_frame_buf_valid : std_logic;
	signal input_frame_buf_last  : std_logic;
	signal input_frame_buf_data  : std_logic_vector(7 downto 0);

	signal input_frame_fifo_ready : std_logic;
	signal input_frame_fifo_valid : std_logic;
	signal input_frame_fifo_last  : std_logic;
	signal input_frame_fifo_data  : std_logic_vector(7 downto 0);

	signal input_frame_ldpc_ready : std_logic;
	signal input_frame_ldpc_valid : std_logic;
	signal input_frame_ldpc_last  : std_logic;
	signal input_frame_ldpc_data  : std_logic_vector(127 downto 0);

	signal output_frame_fifo_ready : std_logic;
	signal output_frame_fifo_valid : std_logic;
	signal output_frame_fifo_last  : std_logic;
	signal output_frame_fifo_data  : std_logic_vector(7 downto 0);

	signal output_frame_ldpc_ready : std_logic;
	signal output_frame_ldpc_valid : std_logic;
	signal output_frame_ldpc_last  : std_logic;
	signal output_frame_ldpc_data  : std_logic_vector(127 downto 0);

	signal output_int_ready : std_logic;
	signal output_int_valid : std_logic;
	signal output_int_last  : std_logic;
	signal output_int_data  : std_logic_vector(7 downto 0);

	signal status_ready_int : std_logic;
	signal status_valid_int : std_logic;
	signal status_data_int  : std_logic_vector(63 downto 0);

	type t_ctrl_state is (IDLE, WAIT_LAST);
	signal ctrl_in_state : t_ctrl_state;
begin

	rst_n <= not rst;

	-- Demux at the input
	input_frame_buf_ready <= input_frame_ldpc_ready when bypass_ldpc_in(0) = '0' and apply_new_cfg = '1' else
	                         input_frame_fifo_ready when bypass_ldpc_in(0) = '1' and apply_new_cfg = '1' else
									 '0';

	input_frame_ldpc_valid <= input_frame_buf_valid when bypass_ldpc_in(0) = '0' and apply_new_cfg = '1' else '0';
	input_frame_ldpc_last  <= input_frame_buf_last  when bypass_ldpc_in(0) = '0' and apply_new_cfg = '1' else '0';
	input_frame_ldpc_data(127 downto 8) <= (others => '0');
	input_frame_ldpc_data(  7 downto 0) <= input_frame_buf_data;

	input_frame_fifo_valid <= input_frame_buf_valid when bypass_ldpc_in(0) = '1' and apply_new_cfg = '1' else '0';
	input_frame_fifo_last  <= input_frame_buf_last  when bypass_ldpc_in(0) = '1' and apply_new_cfg = '1' else '0';
	input_frame_fifo_data  <= input_frame_buf_data;

	-- Mux at the output
	output_frame_ldpc_ready <= output_int_ready when bypass_ldpc_out = '0' else '0';
	output_frame_fifo_ready <= output_int_ready when bypass_ldpc_out = '1' else '0';

	output_int_valid <= output_frame_fifo_valid when bypass_ldpc_out = '1' else output_frame_ldpc_valid;
	output_int_last  <= output_frame_fifo_last  when bypass_ldpc_out = '1' else output_frame_ldpc_last;
	output_int_data  <= output_frame_fifo_data  when bypass_ldpc_out = '1' else output_frame_ldpc_data(7 downto 0);

	gen_xilinx_fec : if USE_XILINX_FEC generate

		signal ctrl_ldpc_data_xilinx : std_logic_vector(39 downto 0);
		signal status_data_xilinx    : std_logic_vector(39 downto 0);

	begin
		-- control input signals for xilinx fec
		pr_ctrl_in : process(clk)
		begin
		if rising_edge(clk) then
			if rst = '1' then
				ctrl_ready      <= '1';
				ctrl_ldpc_valid <= '0';
				ctrl_ldpc_data  <= (others => '0');
				ctrl_in_state   <= IDLE;
				apply_new_cfg   <= '0';
				bypass_ldpc_in  <= (others => '0');
				bypass_ldpc_in_valid  <= '0';
			else

				if bypass_ldpc_in_valid = '1' and bypass_ldpc_in_ready = '1' then
					bypass_ldpc_in_valid  <= '0';
				end if;

				case ctrl_in_state is
					when IDLE =>
						if apply_new_cfg = '0' and ctrl_valid = '1' and bypass_ldpc_in_ready = '1' then
							ctrl_ready           <= '0';
							apply_new_cfg        <= '1';
							bypass_ldpc_in(0)    <= '1';
							bypass_ldpc_in_valid <= '1';
							ctrl_in_state  <= WAIT_LAST;
							ctrl_ldpc_data(39 downto 38) <= "00";
							ctrl_ldpc_data(31 downto 24) <= x"00"; -- id
							ctrl_ldpc_data(23 downto 9)  <= (others => '0'); --reserved
							ctrl_ldpc_data( 8 downto 6)  <= std_logic_vector(to_unsigned(0, 3)); --bg
							ctrl_ldpc_data( 5 downto 3)  <= std_logic_vector(to_unsigned(1, 3)); --z_set
							ctrl_ldpc_data( 2 downto 0)  <= std_logic_vector(to_unsigned(7, 3)); --z_j

							if unsigned(ctrl_data) > 0 then
								ctrl_ldpc_valid   <= '1';
								bypass_ldpc_in(0) <= '0';

								case to_integer(unsigned(ctrl_data)) is
									when 1 => ctrl_ldpc_data(37 downto 32) <= std_logic_vector(to_unsigned( 4, 6));
									when 2 => ctrl_ldpc_data(37 downto 32) <= std_logic_vector(to_unsigned( 7, 6));
									when 3 => ctrl_ldpc_data(37 downto 32) <= std_logic_vector(to_unsigned(11, 6));
									when others => ctrl_ldpc_data(37 downto 32) <= std_logic_vector(to_unsigned(22, 6));
								end case;
							end if;
						end if;

					when WAIT_LAST =>
						if ctrl_ldpc_ready = '1' then
							ctrl_ldpc_valid <= '0';
							ctrl_ldpc_data  <= (others => '0');
						end if;

						if input_frame_buf_ready = '1' and input_frame_buf_valid = '1' and input_frame_buf_last = '1' then
							apply_new_cfg <= '0';
							ctrl_ready    <= '1';
							ctrl_in_state <= IDLE;
						end if;
				end case;

			end if; -- rst
		end if; -- clk
		end process pr_ctrl_in;

		ctrl_ldpc_data_xilinx        <= ctrl_ldpc_data(39 downto 0);
		status_data_int(39 downto 0) <= status_data_xilinx;

		-- SD-FEC Instantiation
		inst_enc_ldpc : ldpc_enc
		port map(
			reset_n    => rst_n,
			core_clk   => core_clk,

			s_axi_aclk => clk,
			interrupt  => open,

			s_axis_ctrl_aclk     => clk,
			s_axis_ctrl_tready   => ctrl_ldpc_ready,
			s_axis_ctrl_tvalid   => ctrl_ldpc_valid,
			s_axis_ctrl_tdata    => ctrl_ldpc_data_xilinx,

			s_axis_din_aclk      => clk,
			s_axis_din_tready    => input_frame_ldpc_ready,
			s_axis_din_tvalid    => input_frame_ldpc_valid,
			s_axis_din_tlast     => input_frame_ldpc_last,
			s_axis_din_tdata     => input_frame_ldpc_data,

			m_axis_status_aclk   => clk,
			m_axis_status_tready => status_ready_int,
			m_axis_status_tvalid => status_valid_int,
			m_axis_status_tdata  => status_data_xilinx,

			m_axis_dout_aclk     => clk,
			m_axis_dout_tready   => output_frame_ldpc_ready,
			m_axis_dout_tvalid   => output_frame_ldpc_valid,
			m_axis_dout_tlast    => output_frame_ldpc_last,
			m_axis_dout_tdata    => output_frame_ldpc_data
		);
	end generate;

	-- Use Creonic FEC
	gen_creonic_fec : if not USE_XILINX_FEC generate

		signal input_frame_ldpc_data_creonic  : std_logic_vector(7 downto 0);
		signal output_frame_ldpc_data_creonic : std_logic_vector(7 downto 0);

	begin

		-- control input signals for xilinx fec
		pr_ctrl_in : process(clk)
		begin
		if rising_edge(clk) then
			if rst = '1' then
				ctrl_ready      <= '1';
				ctrl_ldpc_valid <= '0';
				ctrl_ldpc_data  <= (others => '0');
				ctrl_in_state   <= IDLE;
				apply_new_cfg   <= '0';
				bypass_ldpc_in  <= (others => '0');
				bypass_ldpc_in_valid  <= '0';
			else

				if bypass_ldpc_in_valid = '1' and bypass_ldpc_in_ready = '1' then
					bypass_ldpc_in_valid  <= '0';
				end if;

				case ctrl_in_state is
					when IDLE =>
						if apply_new_cfg = '0' and ctrl_valid = '1' and bypass_ldpc_in_ready = '1' then
							ctrl_ready           <= '0';
							apply_new_cfg        <= '1';
							bypass_ldpc_in(0)    <= '1';
							bypass_ldpc_in_valid <= '1';
							ctrl_in_state  <= WAIT_LAST;
							ctrl_ldpc_data(63 downto 31) <= (others => '0');
							ctrl_ldpc_data(30 downto 22) <= std_logic_vector(to_unsigned(384, 9));

							if unsigned(ctrl_data) > 0 then
								ctrl_ldpc_valid   <= '1';
								bypass_ldpc_in(0) <= '0';

								case to_integer(unsigned(ctrl_data)) is
									when 1 =>
										ctrl_ldpc_data(14 downto 0)  <= std_logic_vector(to_unsigned(26 * 384, 15));
										ctrl_ldpc_data(21 downto 15) <= R084_22_26;
									when 2 =>
										ctrl_ldpc_data(14 downto 0)  <= std_logic_vector(to_unsigned(29 * 384, 15));
										ctrl_ldpc_data(21 downto 15) <= R075_22_29;
									when 3 =>
										ctrl_ldpc_data(14 downto 0)  <= std_logic_vector(to_unsigned(33 * 384, 15));
										ctrl_ldpc_data(21 downto 15) <= R066_22_33;
									when others =>
										ctrl_ldpc_data(14 downto 0)  <= std_logic_vector(to_unsigned(44 * 384, 15));
										ctrl_ldpc_data(21 downto 15) <= R050_22_44;
								end case;
							end if;
						end if;

					when WAIT_LAST =>
						if ctrl_ldpc_ready = '1' then
							ctrl_ldpc_valid <= '0';
							ctrl_ldpc_data  <= (others => '0');
						end if;

						if input_frame_buf_ready = '1' and input_frame_buf_valid = '1' and input_frame_buf_last = '1' then
							apply_new_cfg <= '0';
							ctrl_ready    <= '1';
							ctrl_in_state <= IDLE;
						end if;
				end case;

			end if; -- rst
		end if; -- clk
		end process pr_ctrl_in;

		input_frame_ldpc_data_creonic      <= input_frame_ldpc_data(7 downto 0);
		output_frame_ldpc_data(7 downto 0) <= output_frame_ldpc_data_creonic;

		inst_enc_ldpc : entity frame_encoding.enc_ldpc_wrapper
		port map(
			clk            => clk,
			rst            => rst,

			ldpc_clk       => core_clk,
			ldpc_rst       => core_rst,

			-- Configuration input
			----------------------
			cfg_tdata      => ctrl_ldpc_data,
			cfg_tvalid     => ctrl_ldpc_valid,
			cfg_tready     => ctrl_ldpc_ready,

			-- Input data handling
			----------------------

			input_tdata    => input_frame_ldpc_data_creonic,
			input_tvalid   => input_frame_ldpc_valid,
			input_tlast    => input_frame_ldpc_last,
			input_tready   => input_frame_ldpc_ready,

			-- Configuration output
			----------------------
			cfg_out_tdata  => status_data_int,
			cfg_out_tvalid => status_valid_int,
			cfg_out_tready => status_ready_int,

			-- Output data handling
			-----------------------
			result_tdata   => output_frame_ldpc_data_creonic,
			result_tvalid  => output_frame_ldpc_valid,
			result_tlast   => output_frame_ldpc_last,
			result_tready  => output_frame_ldpc_ready
	);
	end generate;

	-- output signal control
	pr_ctrl_out : process(clk)
	begin
	if rising_edge(clk) then
		if rst = '1' then
			bypass_ldpc_out_ready <= '1';
			bypass_ldpc_out <= '0';
		else
			if bypass_ldpc_out_ready = '1' and bypass_ldpc_out_valid = '1' then
				bypass_ldpc_out_ready <= '0';
				bypass_ldpc_out <= bypass_ldpc_out_data(0);
			end if;

			if output_int_ready = '1' and output_int_valid = '1' and output_int_last = '1' then
				bypass_ldpc_out_ready <= '1';
			end if;
		end if; -- rst
	end if; -- clk
	end process pr_ctrl_out;


	-- Add axis buffer for cutting the combinational logic path
	inst_buffer_data_in : entity frame_encoding.axi4s_buffer
	generic map(
		DATA_WIDTH => 8
	)
	port map(
		clk => clk,
		rst => rst,

		input_ready => input_ready,
		input_valid => input_valid,
		input_last  => input_last,
		input_data  => input_data,

		output_ready => input_frame_buf_ready,
		output_valid => input_frame_buf_valid,
		output_last  => input_frame_buf_last,
		output_data  => input_frame_buf_data
	);

	-- Store the data to avoid the LDPC
	inst_data_fifo : entity frame_encoding.axi4s_fifo
	generic map (
		DISTR_RAM         => false,
		FIFO_DEPTH        => 16384,
		DATA_WIDTH        => 8,
		FULL_THRESHOLD    => 16384,
		USE_OUTPUT_BUFFER => false
	)
	port map (
		clk => clk,
		rst => rst,

		input_ready       => input_frame_fifo_ready,
		input_valid       => input_frame_fifo_valid,
		input_last        => input_frame_fifo_last,
		input_data        => input_frame_fifo_data,
		input_almost_full => open,

		output_ready => output_frame_fifo_ready,
		output_valid => output_frame_fifo_valid,
		output_last  => output_frame_fifo_last,
		output_data  => output_frame_fifo_data
	);

	-- Store the output control
	inst_ctrl_fifo : entity frame_encoding.axi4s_fifo
	generic map (
		DISTR_RAM         => true,
		FIFO_DEPTH        => 32,
		DATA_WIDTH        => 1,
		FULL_THRESHOLD    => 32,
		USE_OUTPUT_BUFFER => false
	)
	port map (
		clk => clk,
		rst => rst,

		input_ready       => bypass_ldpc_in_ready,
		input_valid       => bypass_ldpc_in_valid,
		input_last        => '0',
		input_data        => bypass_ldpc_in,
		input_almost_full => open,

		output_ready => bypass_ldpc_out_ready,
		output_valid => bypass_ldpc_out_valid,
		output_last  => open,
		output_data  => bypass_ldpc_out_data
	);


	-- Add axis buffer for cutting the combinational logic path
	inst_buffer_status: entity frame_encoding.axi4s_buffer
	generic map(
		DATA_WIDTH => 64
	)
	port map(
		clk => clk,
		rst => rst,

		input_ready => status_ready_int,
		input_valid => status_valid_int,
		input_last  => '0',
		input_data  => status_data_int,

		output_ready => status_ready,
		output_valid => status_valid,
		output_last  => open,
		output_data  => status_data
	);

	inst_buffer_data_out : entity frame_encoding.axi4s_buffer
	generic map(
		DATA_WIDTH => 8
	)
	port map(
		clk => clk,
		rst => rst,

		input_ready => output_int_ready,
		input_valid => output_int_valid,
		input_last  => output_int_last,
		input_data  => output_int_data,

		output_ready => output_ready,
		output_valid => output_valid,
		output_last  => output_last,
		output_data  => output_data
	);

end architecture rtl;
