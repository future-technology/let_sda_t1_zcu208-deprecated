--!
--! Copyright (C) Creonic GmbH
--!
--!
--! @file
--! @brief  Parameters
--! @author Carlos Velez
--! @date   2021/11/16
--!
--! @details This is the configuration file of the Convolutional Encoder.
--!          Any changes for parameters should be done here.
--!          Changing parameters somewhere else may result in a malicious
--!          behavior.
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package pkg_header_support is

	-----------------------------------
	-- Convolutional Code Parameters --
	-----------------------------------

	constant CONSTRAINT_LENGTH : natural := 7;

	constant BW_LLR_INPUT : natural := 4;

	--
	-- Set the number of parity values
	-- This has to correspond to PARITY_POLYNOMIALS
	--
	constant NUMBER_PARITY_BITS : natural := 6;
	type t_parity is array (NUMBER_PARITY_BITS - 1 downto 0) of natural;

	--
	-- Set parity polynoms in decimal notation
	-- NUMBER_PARITY_BITS has to correspond to the number of elements
	--
	constant PARITY_POLYNOMIALS   : t_parity := (79,87,91,105,121,125);

	type t_generators is array (NUMBER_PARITY_BITS - 1 downto 0) of std_logic_vector(CONSTRAINT_LENGTH - 2 downto 0);

	function calc_generators return t_generators;

end pkg_header_support;

package body pkg_header_support is

	--vhdl_cover_off
	function calc_generators return t_generators is
		variable v_out : t_generators;
		variable v_tmp : std_logic_vector(CONSTRAINT_LENGTH - 1 downto 0);
	begin

		for i in PARITY_POLYNOMIALS'length - 1 downto 0 loop
			v_tmp    := std_logic_vector(to_unsigned(PARITY_POLYNOMIALS(i), CONSTRAINT_LENGTH));
			v_out(i) := v_tmp(CONSTRAINT_LENGTH - 2 downto 0);
		end loop;

		return v_out;
	end calc_generators;
	--vhdl_cover_on

end pkg_header_support;