--!
--! Copyright (C) 2010 - 2011 Creonic GmbH
--!
--! @file
--! @brief  Support package with useful functions
--! @author Matthias Alles
--! @date   2010/07/14
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package pkg_support is

	--!
	--! Return the log_2 of an natural value, i.e. the number of bits required
	--! to represent this unsigned value.
	--!
	function no_bits_natural(value_in : natural) return natural;

	function crc32(input: std_logic_vector(7 downto 0); crc:  std_logic_vector(31 downto 0)) return std_logic_vector;
	function scramble(v_input: std_logic_vector(7 downto 0); v_lfsr: std_logic_vector(14 downto 0)) return std_logic_vector;
	function update_lsfr(v_lfsr: std_logic_vector(14 downto 0)) return std_logic_vector;

	-- Calculate CRC32
	function next_crc(crc, crc_poly, new_bits: std_logic_vector) return std_logic_vector;

end pkg_support;


package body pkg_support is

	function no_bits_natural(value_in: natural) return natural is
		variable v_n_bit : unsigned(31 downto 0);
	begin
		--vhdl_cover_off
		if value_in = 0 then
			return 0;
		end if;
		v_n_bit := to_unsigned(value_in, 32);
		for i in 31 downto 0 loop
			if v_n_bit(i) = '1' then
				return i + 1;
			end if;
		end loop;
		return 1;
		--vhdl_cover_on
	end function no_bits_natural;

	function crc32(input: std_logic_vector(7 downto 0); crc:  std_logic_vector(31 downto 0)) return std_logic_vector is
		variable v_sreg: std_logic_vector(31 downto 0);
		variable v_input : std_logic_vector(7 downto 0);
	begin
		v_input := input;

		v_sreg(0) := v_input(6) xor v_input(0) xor crc(24) xor crc(30);
		v_sreg(1) := v_input(7) xor v_input(6) xor v_input(1) xor v_input(0) xor crc(24) xor crc(25) xor crc(30) xor crc(31);
		v_sreg(2) := v_input(7) xor v_input(6) xor v_input(2) xor v_input(1) xor v_input(0) xor crc(24) xor crc(25) xor crc(26) xor crc(30) xor crc(31);
		v_sreg(3) := v_input(7) xor v_input(3) xor v_input(2) xor v_input(1) xor crc(25) xor crc(26) xor crc(27) xor crc(31);
		v_sreg(4) := v_input(6) xor v_input(4) xor v_input(3) xor v_input(2) xor v_input(0) xor crc(24) xor crc(26) xor crc(27) xor crc(28) xor crc(30);
		v_sreg(5) := v_input(7) xor v_input(6) xor v_input(5) xor v_input(4) xor v_input(3) xor v_input(1) xor v_input(0) xor crc(24) xor crc(25) xor crc(27) xor crc(28) xor crc(29) xor crc(30) xor crc(31);
		v_sreg(6) := v_input(7) xor v_input(6) xor v_input(5) xor v_input(4) xor v_input(2) xor v_input(1) xor crc(25) xor crc(26) xor crc(28) xor crc(29) xor crc(30) xor crc(31);
		v_sreg(7) := v_input(7) xor v_input(5) xor v_input(3) xor v_input(2) xor v_input(0) xor crc(24) xor crc(26) xor crc(27) xor crc(29) xor crc(31);
		v_sreg(8) := v_input(4) xor v_input(3) xor v_input(1) xor v_input(0) xor crc(0) xor crc(24) xor crc(25) xor crc(27) xor crc(28);
		v_sreg(9) := v_input(5) xor v_input(4) xor v_input(2) xor v_input(1) xor crc(1) xor crc(25) xor crc(26) xor crc(28) xor crc(29);
		v_sreg(10) := v_input(5) xor v_input(3) xor v_input(2) xor v_input(0) xor crc(2) xor crc(24) xor crc(26) xor crc(27) xor crc(29);
		v_sreg(11) := v_input(4) xor v_input(3) xor v_input(1) xor v_input(0) xor crc(3) xor crc(24) xor crc(25) xor crc(27) xor crc(28);
		v_sreg(12) := v_input(6) xor v_input(5) xor v_input(4) xor v_input(2) xor v_input(1) xor v_input(0) xor crc(4) xor crc(24) xor crc(25) xor crc(26) xor crc(28) xor crc(29) xor crc(30);
		v_sreg(13) := v_input(7) xor v_input(6) xor v_input(5) xor v_input(3) xor v_input(2) xor v_input(1) xor crc(5) xor crc(25) xor crc(26) xor crc(27) xor crc(29) xor crc(30) xor crc(31);
		v_sreg(14) := v_input(7) xor v_input(6) xor v_input(4) xor v_input(3) xor v_input(2) xor crc(6) xor crc(26) xor crc(27) xor crc(28) xor crc(30) xor crc(31);
		v_sreg(15) := v_input(7) xor v_input(5) xor v_input(4) xor v_input(3) xor crc(7) xor crc(27) xor crc(28) xor crc(29) xor crc(31);
		v_sreg(16) := v_input(5) xor v_input(4) xor v_input(0) xor crc(8) xor crc(24) xor crc(28) xor crc(29);
		v_sreg(17) := v_input(6) xor v_input(5) xor v_input(1) xor crc(9) xor crc(25) xor crc(29) xor crc(30);
		v_sreg(18) := v_input(7) xor v_input(6) xor v_input(2) xor crc(10) xor crc(26) xor crc(30) xor crc(31);
		v_sreg(19) := v_input(7) xor v_input(3) xor crc(11) xor crc(27) xor crc(31);
		v_sreg(20) := v_input(4) xor crc(12) xor crc(28);
		v_sreg(21) := v_input(5) xor crc(13) xor crc(29);
		v_sreg(22) := v_input(0) xor crc(14) xor crc(24);
		v_sreg(23) := v_input(6) xor v_input(1) xor v_input(0) xor crc(15) xor crc(24) xor crc(25) xor crc(30);
		v_sreg(24) := v_input(7) xor v_input(2) xor v_input(1) xor crc(16) xor crc(25) xor crc(26) xor crc(31);
		v_sreg(25) := v_input(3) xor v_input(2) xor crc(17) xor crc(26) xor crc(27);
		v_sreg(26) := v_input(6) xor v_input(4) xor v_input(3) xor v_input(0) xor crc(18) xor crc(24) xor crc(27) xor crc(28) xor crc(30);
		v_sreg(27) := v_input(7) xor v_input(5) xor v_input(4) xor v_input(1) xor crc(19) xor crc(25) xor crc(28) xor crc(29) xor crc(31);
		v_sreg(28) := v_input(6) xor v_input(5) xor v_input(2) xor crc(20) xor crc(26) xor crc(29) xor crc(30);
		v_sreg(29) := v_input(7) xor v_input(6) xor v_input(3) xor crc(21) xor crc(27) xor crc(30) xor crc(31);
		v_sreg(30) := v_input(7) xor v_input(4) xor crc(22) xor crc(28) xor crc(31);
		v_sreg(31) := v_input(5) xor crc(23) xor crc(29);
		return v_sreg;
	end crc32;

	function scramble(v_input: std_logic_vector(7 downto 0); v_lfsr: std_logic_vector(14 downto 0)) return std_logic_vector is
		variable v_data : std_logic_vector(7 downto 0);
	begin
--		v_data(0) := v_input(0) xor v_lfsr(14);
--		v_data(1) := v_input(1) xor v_lfsr(13) xor v_lfsr(14);
--		v_data(2) := v_input(2) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
--		v_data(3) := v_input(3) xor v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
--		v_data(4) := v_input(4) xor v_lfsr(10) xor v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
--		v_data(5) := v_input(5) xor v_lfsr(9)  xor v_lfsr(10) xor v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
--		v_data(6) := v_input(6) xor v_lfsr(8)  xor v_lfsr(9)  xor v_lfsr(10) xor v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
--		v_data(7) := v_input(7) xor v_lfsr(7)  xor v_lfsr(8)  xor v_lfsr(9)  xor v_lfsr(10) xor v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
--		return v_data;
		
		v_data(0) := v_input(0) xor v_lfsr(7);
		v_data(1) := v_input(1) xor v_lfsr(8);
		v_data(2) := v_input(2) xor v_lfsr(9);
		v_data(3) := v_input(3) xor v_lfsr(10);
		v_data(4) := v_input(4) xor v_lfsr(11);
		v_data(5) := v_input(5) xor v_lfsr(12);
		v_data(6) := v_input(6) xor v_lfsr(13);
		v_data(7) := v_input(7) xor v_lfsr(14);
		return v_data;
		
	end scramble;

	function update_lsfr(v_lfsr: std_logic_vector(14 downto 0)) return std_logic_vector is
		variable v_out : std_logic_vector(14 downto 0);
	begin
--		v_out(0)  := v_lfsr(7)  xor v_lfsr(8)  xor v_lfsr(9)  xor v_lfsr(10) xor v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
--		v_out(1)  := v_lfsr(8)  xor v_lfsr(9)  xor v_lfsr(10) xor v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
--		v_out(2)  := v_lfsr(9)  xor v_lfsr(10) xor v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
--		v_out(3)  := v_lfsr(10) xor v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
--		v_out(4)  := v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
--		v_out(5)  := v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
--		v_out(6)  := v_lfsr(13) xor v_lfsr(14);
--		v_out(7)  := v_lfsr(14);
--		v_out(8)  := v_lfsr(0);
--		v_out(9)  := v_lfsr(1);
--		v_out(10) := v_lfsr(2);
--		v_out(11) := v_lfsr(3);
--		v_out(12) := v_lfsr(4);
--		v_out(13) := v_lfsr(5);
--		v_out(14) := v_lfsr(6) xor v_lfsr(7) xor v_lfsr(8) xor v_lfsr(9) xor v_lfsr(10) xor v_lfsr(11) xor v_lfsr(12) xor v_lfsr(13) xor v_lfsr(14);
		v_out(0)  := v_lfsr(6)  xor v_lfsr(7);
		v_out(1)  := v_lfsr(7) xor v_lfsr(8);
		v_out(2)  := v_lfsr(8) xor v_lfsr(9);
		v_out(3)  := v_lfsr(9) xor v_lfsr(10);
		v_out(4)  := v_lfsr(10) xor v_lfsr(11);
		v_out(5)  := v_lfsr(11) xor v_lfsr(12);
		v_out(6)  := v_lfsr(12) xor v_lfsr(13);
		v_out(7)  := v_lfsr(13) xor v_lfsr(14);
		v_out(8)  := v_lfsr(0);
		v_out(9)  := v_lfsr(1);
		v_out(10) := v_lfsr(2);
		v_out(11) := v_lfsr(3);
		v_out(12) := v_lfsr(4);
		v_out(13) := v_lfsr(5);
		v_out(14) := v_lfsr(6);
		
		
		return v_out;
	end update_lsfr;

	function next_crc(crc, crc_poly, new_bits: std_logic_vector) return std_logic_vector is
		variable v_next_crc: std_logic_vector(crc'range);
	begin
		v_next_crc := crc;

		for i in new_bits'range loop
			if v_next_crc(v_next_crc'left) /= new_bits(i) then
				v_next_crc := (v_next_crc(v_next_crc'left - 1 downto 0) & '0') xor crc_poly;
			else
				v_next_crc := v_next_crc(v_next_crc'left - 1 downto 0) & '0';
			end if;
		end loop;
		return v_next_crc;
	end next_crc;

end pkg_support;
