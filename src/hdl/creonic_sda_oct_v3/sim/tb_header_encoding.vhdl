-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 22/12/2022 11:40:33 AM
-- Design Name: 
-- Module Name: tb_header_encoding
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;

library frame_encoding;

library frame_decoding;
--use frame_decoding.txt_util.all;
use frame_decoding.test_vector_pkg.all;

entity tb_header_encoding is
	generic(

	LDPC_230M_CLK_PERIOD : time := 4.329 ns;
	CLK_PERIOD        : time := 3.030 ns;

	-- Which level has an assertion when it fails?
	ASSERTION_SEVERITY  : severity_level := FAILURE

	);
end tb_header_encoding;


architecture sim of tb_header_encoding is

	constant PREAMBLE_IN : std_logic_vector(63 downto 0) := X"53225B1D0D73DF03";

	component header_encoder is
		port (
			clk          : in  std_logic;
			rst          : in  std_logic;
		
			-- Input data handling
			----------------------
		
			input_ready  : out std_logic;
			input_valid  : in  std_logic;
			input_last   : in  std_logic;
			input_data   : in  std_logic_vector(8 - 1 downto 0);
		
			-- Output data handling
			-----------------------
			output_ready : in  std_logic;
			output_valid : out std_logic;
			output_last  : out std_logic;
			output_data  : out std_logic_vector(8 - 1 downto 0)
		);
	end component header_encoder;

	signal sd_fec_clk    : std_logic := '0';
	signal clk           : std_logic := '0';
	signal rst           : std_logic;
	signal ldpc_230m_clk : std_logic := '0';
	signal ldpc_230m_rst : std_logic;
	signal preamble      : std_logic_vector(63 downto 0);

	-- More diagnostic informations
	-------------------------------------------------------
	signal clear_stat          : std_logic := '0';
	signal total_frame_counter : std_logic_vector(31 downto 0);
	signal crc_error_counter   : std_logic_vector(31 downto 0);
	signal crc16_err_counter   : std_logic_vector(31 downto 0);
	signal crc32_err_counter   : std_logic_vector(31 downto 0);

	-- Stats from the preamble sync
	signal new_search_counter  : std_logic_vector(31 downto 0);
	signal sync_loss_counter   : std_logic_vector(31 downto 0);

	signal header_enc_ready : std_logic;
	signal header_enc_valid : std_logic;
	signal header_enc_last  : std_logic;
	signal header_enc_data  : std_logic_vector(7 downto 0);

	signal header_enc_out_ready : std_logic;
	signal header_enc_out_valid : std_logic;
	signal header_enc_out_last  : std_logic;
	signal header_enc_out_data  : std_logic_vector(7 downto 0);

	signal byte_number : natural := 0;
	signal byte_errors : natural := 0;
	signal test_errors : natural := 0;

begin

	-------------------------------------------
	-- component instantiation
	-------------------------------------------

	UUT : entity frame_encoding.header_encoder
	port map(

		clk          => clk,
		rst          => rst,

		-- Input data handling
		----------------------

		input_ready  => header_enc_ready,
		input_valid  => header_enc_valid,
		input_last   => header_enc_last,
		input_data   => header_enc_data,

		-- Output data handling
		-----------------------
		output_ready => header_enc_out_ready,
		output_valid => header_enc_out_valid,
		output_last  => header_enc_out_last,
		output_data  => header_enc_out_data
	);

	-------------------------------------------
	-- clock generation
	-------------------------------------------
	clk        <= not clk after CLK_PERIOD / 2;

	preamble  <= PREAMBLE_IN;
    header_enc_out_ready <= '1';
	-------------------------------------------
	-- Stimuli generation
	-------------------------------------------
	pr_stimuli: process
		variable v_idx            : natural;
		variable v_frame_cnt      : natural;
		variable v_frame_cnt_word : std_logic_vector(15 downto 0);
	begin
	    header_enc_valid <= '0';
	    header_enc_last <= '0';
        header_enc_data  <= (others=>'0');
        
		-- reset the system
		rst       <= '1';

		wait for 200 * CLK_PERIOD;
		-- release the reset
		rst         <= '0';

		wait for 5 * CLK_PERIOD;
		wait until rising_edge(clk);

		-- send data of one block
		for test_id in 0 to 5-1 loop
			for word_id in 0 to C_HEADER_WORDS - 1 loop
				for byte_id in (32/8) - 1 downto 0 loop

					header_enc_valid <= '1';
					header_enc_data  <= c_header_test_array_raw(test_id)(word_id)((byte_id*8)+8-1 downto byte_id*8);

					if word_id = C_HEADER_WORDS-1 and byte_id = 0 then
						header_enc_last <= '1';
					else
						header_enc_last <= '0';
					end if;

					wait until rising_edge(clk) and header_enc_ready = '1';
					--wait for 0.2 * CLK_PERIOD;
					header_enc_last <= '0';
					--wait until falling_edge(clk);

				end loop;
			end loop;
			header_enc_valid <= '0';
			wait for 10 * CLK_PERIOD;
			-- reset the system
			rst       <= '1';

			wait for 20 * CLK_PERIOD;
			-- release the reset
			rst         <= '0';

			wait for 10 * CLK_PERIOD;
			wait until rising_edge(clk);
		end loop;
		wait;
	end process pr_stimuli;


	---------------------------
	-- output comparison
	---------------------------

	pr_output : process
		--variable v_data : std_logic_vector(32-1 downto 0);
	begin
		byte_errors <= 0;
		byte_number <= 0;
		test_errors <= 0;
		-- send data of one block
		for test_id in 0 to 5-1 loop
			for word_id in 0 to C_ENCODED_HEADER_WORDS - 1 loop
				for byte_id in (32/8) - 1 downto 0 loop

					wait until rising_edge(clk) and header_enc_out_valid = '1';
					byte_number <= byte_number + 1;
					--v_data := v_data(32-8-1 downto 0) & header_enc_out_data;
					--wait until rising_edge(clk) and header_enc_out_valid = '1';
					--v_data := v_data(32-8-1 downto 0) & header_enc_out_data;
					--wait until rising_edge(clk) and header_enc_out_valid = '1';
					--v_data := v_data(32-8-1 downto 0) & header_enc_out_data;
					--wait until rising_edge(clk) and header_enc_out_valid = '1';
					--v_data := v_data(32-8-1 downto 0) & header_enc_out_data;

					if c_header_test_array_encoded(test_id)(word_id)((byte_id*8)+8-1 downto byte_id*8) /= header_enc_out_data then
						byte_errors <= byte_errors + 1;
					end if;

					--if word_id = C_HEADER_WORDS-1 and byte_id = 0 then
	--
					--end if;

					--wait until rising_edge(clk) and header_enc_out_valid = '1';
					--wait for 0.2 * CLK_PERIOD;
					--wait until falling_edge(clk);
						
				end loop;
			end loop;
			if byte_errors > 0 then
				test_errors <= test_errors + 1;
			end if;
			wait until rst = '0';
			byte_errors <= 0;
			byte_number <= 0;
		end loop;
		wait for 800 ns;
		if test_errors = 0 then
			assert false report "Simulation finished successfully. No errors :)"
			severity failure;
		else
			assert false report "Simulation finished with Errors: Check byte_errors signal."
			severity failure;
		end if;
	end process pr_output;

end architecture sim;
