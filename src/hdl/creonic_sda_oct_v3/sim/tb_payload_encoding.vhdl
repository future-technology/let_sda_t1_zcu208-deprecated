-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 22/12/2022 11:40:33 AM
-- Design Name: 
-- Module Name: tb_payload_encoding
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;

library frame_encoding;

library frame_decoding;
--use frame_decoding.txt_util.all;
use frame_decoding.test_vector_pkg.all;

entity tb_payload_encoding is
	generic(

	LDPC_230M_CLK_PERIOD : time := 4.329 ns;
	CLK_PERIOD        : time := 3.030 ns;

	-- Which level has an assertion when it fails?
	ASSERTION_SEVERITY  : severity_level := FAILURE

	);
end tb_payload_encoding;


architecture sim of tb_payload_encoding is

	constant PREAMBLE_IN : std_logic_vector(63 downto 0) := X"53225B1D0D73DF03";
	constant USE_XILINX_FEC : boolean := false;

	component payload_encoder is
		generic (
				USE_XILINX_FEC : boolean := true
			);
		port (
			core_clk : in  std_logic;
			core_rst : in  std_logic;
			clk      : in  std_logic;
			rst      : in  std_logic;
		
			ctrl_ready : out std_logic;
			ctrl_valid : in  std_logic;
			ctrl_data  : in  std_logic_vector(2 downto 0);
		
			-- Input data handling
			----------------------
		
			input_ready : out std_logic;
			input_valid : in  std_logic;
			input_last  : in  std_logic;
			input_data  : in  std_logic_vector(7 downto 0);
		
			status_ready : in  std_logic;
			status_valid : out std_logic;
			status_data  : out std_logic_vector(63 downto 0);
		
			-- Output data handling
			-----------------------
			output_ready : in  std_logic;
			output_valid : out std_logic;
			output_last  : out std_logic;
			output_data  : out std_logic_vector(7 downto 0)
		);
	end component payload_encoder;

	signal sd_fec_clk    : std_logic := '0';
	signal clk           : std_logic := '0';
	signal ldpc_clk      : std_logic := '0';
	signal rst           : std_logic;
	signal ldpc_rst      : std_logic;
	signal ldpc_230m_clk : std_logic := '0';
	signal ldpc_230m_rst : std_logic;
	signal preamble      : std_logic_vector(63 downto 0);

	-- More diagnostic informations
	-------------------------------------------------------
	signal clear_stat          : std_logic := '0';
	signal total_frame_counter : std_logic_vector(31 downto 0);
	signal crc_error_counter   : std_logic_vector(31 downto 0);
	signal crc16_err_counter   : std_logic_vector(31 downto 0);
	signal crc32_err_counter   : std_logic_vector(31 downto 0);

	-- Stats from the preamble sync
	signal new_search_counter  : std_logic_vector(31 downto 0);
	signal sync_loss_counter   : std_logic_vector(31 downto 0);

	signal s_axis_ctrl_tready : std_logic;
	signal s_axis_ctrl_tvalid : std_logic;
	signal s_axis_ctrl_tdata  : std_logic_vector(2 downto 0);

	signal enc_input_ready : std_logic;
	signal enc_input_valid : std_logic;
	signal enc_input_last  : std_logic;
	signal enc_input_data  : std_logic_vector(7 downto 0);

	signal enc_output_ready : std_logic;
	signal enc_output_valid : std_logic;
	signal enc_output_last  : std_logic;
	signal enc_output_data  : std_logic_vector(7 downto 0);

	signal byte_number : natural := 0;
	signal byte_errors : natural := 0;
	signal test_errors : natural := 0;

	signal code : natural := 0; -- LDPC code rate.

begin

	-------------------------------------------
	-- component instantiation
	-------------------------------------------

	UUT : entity frame_encoding.payload_encoder
	generic map(
		USE_XILINX_FEC => USE_XILINX_FEC
	)
	port map(

		core_clk => ldpc_clk,
		core_rst => ldpc_rst,
		clk      => clk,
		rst      => rst,

		ctrl_ready => s_axis_ctrl_tready,
		ctrl_valid => s_axis_ctrl_tvalid,
		ctrl_data  => s_axis_ctrl_tdata,

		-- Input data handling
		----------------------
		input_ready => enc_input_ready,
		input_valid => enc_input_valid,
		input_last  => enc_input_last,
		input_data  => enc_input_data,

		status_ready => '1',
		status_valid => open,
		status_data  => open,

		-- Output data handling
		-----------------------
		output_ready => enc_output_ready,
		output_valid => enc_output_valid,
		output_last  => enc_output_last,
		output_data  => enc_output_data
	);

	-------------------------------------------
	-- clock generation
	-------------------------------------------
	clk        <= not clk after CLK_PERIOD / 2;
	ldpc_clk   <= not ldpc_clk after LDPC_230M_CLK_PERIOD / 2;

	preamble  <= PREAMBLE_IN;
    enc_output_ready <= '1';
	--s_axis_ctrl_tdata <= std_logic_vector(to_unsigned(code, 3)); -- code rate
	--s_axis_ctrl_tvalid <= '1';


	pr_cfg_enc_stimuli : process
		variable v_line_out : line;
	begin
		s_axis_ctrl_tvalid <= '0';
		s_axis_ctrl_tdata  <= (others => '0');
		wait for 2 * CLK_PERIOD;

		wait until rst = '0';

		wait until rising_edge(clk);
		wait for 0.1 * CLK_PERIOD;

		wait until falling_edge(clk);

		loop
			s_axis_ctrl_tdata <= std_logic_vector(to_unsigned(code, 3)); -- code rate

			wait for CLK_PERIOD;
			wait until falling_edge(clk);
			s_axis_ctrl_tvalid <= '1';

			wait until rising_edge(clk);
			while s_axis_ctrl_tready = '0' loop
				wait until rising_edge(clk);
			end loop;
			wait until falling_edge(clk);
			s_axis_ctrl_tvalid <= '0';
			s_axis_ctrl_tdata  <= (others => '0');
		end loop;

		wait;
	end process pr_cfg_enc_stimuli;


	-------------------------------------------
	-- Stimuli generation
	-------------------------------------------
	pr_stimuli: process
		variable v_idx            : natural;
		variable v_frame_cnt      : natural;
		variable v_frame_cnt_word : std_logic_vector(15 downto 0);
	begin
	    enc_input_valid <= '0';
	    enc_input_last <= '0';
        enc_input_data  <= (others=>'0');
		code <= 0;
		-- reset the system
		rst       <= '1';
		ldpc_rst  <= '1';

		wait for 200 * CLK_PERIOD;
		-- release the reset
		rst         <= '0';
		ldpc_rst    <= '0';
		

		wait for 5 * CLK_PERIOD;
		wait until rising_edge(clk);
		code <= 0;

		--s_axis_ctrl_tdata  <= (others => '0');
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		-- send data of one block
		for test_id in 0 to 5-1 loop
			for word_id in 0 to C_PAYLOAD_WORDS - 1 loop
				for byte_id in (32/8) - 1 downto 0 loop
					
					enc_input_valid <= '1';
					enc_input_data  <= c_payload_test_array_raw(test_id)(word_id)((byte_id*8)+8-1 downto byte_id*8);

					if word_id = C_PAYLOAD_WORDS-1 and byte_id = 0 then
						enc_input_last <= '1';
					else
						enc_input_last <= '0';
					end if;

					wait until rising_edge(clk) and enc_input_ready = '1';
					--wait for 0.2 * CLK_PERIOD;
					enc_input_last <= '0';
					--wait until falling_edge(clk);

				end loop;
			end loop;
			enc_input_valid <= '0';
			code <= test_id+1;

			wait for 50 * CLK_PERIOD;
			-- reset the system
			rst       <= '1';
			ldpc_rst  <= '1';

			wait for 20 * CLK_PERIOD;
			-- release the reset
			rst         <= '0';
			ldpc_rst  <= '0';

			wait for 30 * CLK_PERIOD;
			wait until rising_edge(clk);

			wait until rising_edge(clk);
		end loop;
		wait;
	end process pr_stimuli;


	---------------------------
	-- output comparison
	---------------------------

--	pr_output : process
--		--variable v_data : std_logic_vector(32-1 downto 0);
--	begin
--		byte_errors <= 0;
--		byte_number <= 0;
--		test_errors <= 0;
--		-- send data of one block
--		for test_id in 0 to 5-1 loop
--			for word_id in 0 to C_ENCODED_HEADER_WORDS - 1 loop
--				for byte_id in (32/8) - 1 downto 0 loop
--
--					wait until rising_edge(clk) and enc_output_valid = '1';
--					byte_number <= byte_number + 1;
--					--v_data := v_data(32-8-1 downto 0) & header_enc_out_data;
--					--wait until rising_edge(clk) and header_enc_out_valid = '1';
--					--v_data := v_data(32-8-1 downto 0) & header_enc_out_data;
--					--wait until rising_edge(clk) and header_enc_out_valid = '1';
--					--v_data := v_data(32-8-1 downto 0) & header_enc_out_data;
--					--wait until rising_edge(clk) and header_enc_out_valid = '1';
--					--v_data := v_data(32-8-1 downto 0) & header_enc_out_data;
--
--					if c_header_test_array_encoded(test_id)(word_id)((byte_id*8)+8-1 downto byte_id*8) /= enc_output_data then
--						byte_errors <= byte_errors + 1;
--					end if;
--
--					--if word_id = C_HEADER_WORDS-1 and byte_id = 0 then
--	--
--					--end if;
--
--					--wait until rising_edge(clk) and header_enc_out_valid = '1';
--					--wait for 0.2 * CLK_PERIOD;
--					--wait until falling_edge(clk);
--						
--				end loop;
--			end loop;
--			if byte_errors > 0 then
--				test_errors <= test_errors + 1;
--			end if;
--			wait until rst = '0';
--			byte_errors <= 0;
--			byte_number <= 0;
--		end loop;
--		wait for 800 ns;
--		if test_errors = 0 then
--			assert false report "Simulation finished successfully. No errors :)"
--			severity failure;
--		else
--			assert false report "Simulation finished with Errors: Check byte_errors signal."
--			severity failure;
--		end if;
--	end process pr_output;

end architecture sim;
