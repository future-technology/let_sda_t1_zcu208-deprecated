-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: test_vector_pkg
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: arq_support_pkg
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package test_vector_pkg is
    constant C_HEADER_WORDS : natural := 5;
    constant C_ENCODED_HEADER_WORDS : natural := 30;
    constant C_PAYLOAD_WORDS : natural := 264;

    type header_vector_t is array (0 to C_HEADER_WORDS - 1) of std_logic_vector(32 - 1 downto 0);
    type encoded_header_vector_t is array (0 to C_ENCODED_HEADER_WORDS - 1) of std_logic_vector(32 - 1 downto 0);
    type payload_vector_t is array (0 to C_PAYLOAD_WORDS - 1) of std_logic_vector(32 - 1 downto 0);
    type header_test_array_t is array (0 to 5-1) of header_vector_t;
    type encoded_header_test_array_t is array (0 to 5-1) of encoded_header_vector_t;

    type payload_test_array_t is array (0 to 5-1) of payload_vector_t;


    constant c_header_tv1_raw : header_vector_t := (
        x"34120000",
        x"00200002",
        x"00000000",
        x"02FCFFFF",
        x"ECD60000"
    );

    constant c_header_tv2_raw : header_vector_t := (
        x"45230000",
        x"00200804",
        x"00000000",
        x"04003E81",
        x"CBF30000"
    );

    constant c_header_tv3_raw : header_vector_t := (
        x"56340000",
        x"00201006",
        x"00000000",
        x"0604D17F",
        x"11E60000"
    );

    constant c_header_tv4_raw : header_vector_t := (
        x"67450000",
        x"00201808",
        x"00000000",
        x"08082165",
        x"C50A0000"
    );

    constant c_header_tv5_raw : header_vector_t := (
        x"78560000",
        x"0020200A",
        x"00000000",
        x"0A100120",
        x"0A2F0000"
    );

    constant c_header_test_array_raw : header_test_array_t := (
        c_header_tv1_raw,
        c_header_tv2_raw,
        c_header_tv3_raw,
        c_header_tv4_raw,
        c_header_tv5_raw
    );      

    constant c_header_tv1_encoded : encoded_header_vector_t := (
        x"000FF870", x"B652A0EE", x"001DB436", x"8D0C78FC",
        x"00000000", x"00000000", x"00000000", x"0000000F",
        x"C76EFC78", x"FC000000", x"00000000", x"00000FC7",
        x"6EFC78FC", x"00000000", x"00000000", x"00000000",
        x"00000000", x"00000000", x"00000000", x"0FC79174",
        x"B4085142", x"98E933F4", x"5EBAEBAE", x"BAEBAEBA",
        x"EBAE85F6", x"1AB37B16", x"F466D412", x"321247FC",
        x"00000000", x"00000000"
    );

    constant c_header_tv2_encoded : encoded_header_vector_t := (
        x"03F1DBBC", x"EFDBA2AA", x"098D0380", x"8F47891F",
        x"F0000000", x"00000000", x"00000000", x"0000000F",
        x"C76EFC78", x"FC0000FC", x"76EFC78F", x"C003F1DB",
        x"BF1E3F00", x"00000000", x"00000000", x"00000000",
        x"00000000", x"00000000", x"00000003", x"F1DBBF1E",
        x"3F000000", x"000FF88C", x"CF7AF5EB", x"683B8FFF",
        x"E23CC567", x"18E84B40", x"8514298E", x"6CB7891F",
        x"F0000000", x"00000000"
    );

    constant c_header_tv3_encoded : encoded_header_vector_t := (
        x"03F1E4A1", x"5BD2321D", x"BF8CB652", x"A0EE3F00",
        x"00000000", x"00000000", x"00000000", x"0000000F",
        x"C76EFC78", x"FC003F1D", x"BBF1E3F0", x"0003FE1C",
        x"D1E247FC", x"00000000", x"00000000", x"00000000",
        x"00000000", x"00000000", x"00000003", x"FE1CD1E2",
        x"47FFF1DB", x"40993465", x"2A31FDB5", x"D2D0217A",
        x"14264902", x"4BCE01C3", x"02F6639B", x"2DE247FC",
        x"00000000", x"00000000"
    );

    constant c_header_tv4_encoded : encoded_header_vector_t := (
        x"03FE1CD2", x"1C6433A8", x"6DA31FDB", x"A2A5CEE3",
        x"F0000000", x"00000000", x"00000000", x"0000000F",
        x"C76EFC78", x"FC003FE1", x"CD1E247F", x"C0FC76EF",
        x"C78FC000", x"00000000", x"00000000", x"00000000",
        x"00000000", x"00000000", x"000000FC", x"76EFC78F",
        x"C0FC76EF", x"C780076E", x"FC47E245", x"ED31E3A3",
        x"A122FA98", x"901BA2A5", x"CE1F8928", x"A973B8FC",
        x"00000000", x"00000000"
    );

    constant c_header_tv5_encoded : encoded_header_vector_t := (
        x"03FE2330", x"2F59DB8E", x"24A15BD2", x"321247FC",
        x"00000000", x"00000000", x"00000000", x"0000000F",
        x"C76EFC78", x"FC0FC76E", x"FC78FC00", x"00FC7928",
        x"A973B8FC", x"00000000", x"00000000", x"00000000",
        x"00000000", x"00000000", x"000000FC", x"7928A973",
        x"87E1BBF1", x"E3F00000", x"003F1DB4", x"368D0C78",
        x"FC0000FC", x"7928A97C", x"7F6D74B4", x"F7D6761F",
        x"F0000000", x"00000000"
    );

    constant c_header_test_array_encoded : encoded_header_test_array_t := (
        c_header_tv1_encoded,
        c_header_tv2_encoded,
        c_header_tv3_encoded,
        c_header_tv4_encoded,
        c_header_tv5_encoded
    );

    constant c_payload_tv1_raw : payload_vector_t := (
        x"2468D972", x"D72EF2E6", x"2E54E5FA", x"5C1DC84C",
        x"B1ABA5F9", x"DC14C87A", x"B11FA641", x"D584FD1A",
        x"0E5C25C8", x"DCB2CBAE", x"B9E79451", x"79E71452",
        x"79ED146E", x"7965175E", x"73C5289E", x"F3462B94",
        x"F97A171C", x"72492DB6", x"EDB66DB5", x"6DBF6D83",
        x"6D0B6E3B", x"649B5B5B", x"DBD8D8D2", x"D2EEEE66",
        x"65555FFF", x"C0008003", x"000A003C", x"00880330",
        x"0AA03FC0", x"8083030A", x"0A3C3C88", x"8B333AAA",
        x"9FFF4003", x"80090036", x"00B403B8", x"09903560",
        x"BF438389", x"093636B4", x"B7BBB199", x"A555DFFC",
        x"C00A803F", x"0082030C", x"0A283CF0", x"8A233CCA",
        x"8ABF3F82", x"810F0622", x"14CC7AA9", x"1FF64035",
        x"80BD038E", x"092436D8", x"B6D3B6E9", x"B675B53D",
        x"BE8D872D", x"12EE6E65", x"655F5FC3", x"C0888333",
        x"0AAA3FFC", x"800B003A", x"009C0348", x"0BB039A0",
        x"95C37C8B", x"0B3A3A9C", x"9F4B43BB", x"89993556",
        x"BFF78031", x"00A603D4", x"08F83210", x"AC63E948",
        x"77B131A6", x"A5D7DCF0", x"CA22BCCF", x"8AA13FC6",
        x"80970372", x"0B2C3AE8", x"9E73452B", x"9EF94617",
        x"94717927", x"16D276ED", x"366EB567", x"BF5183E5",
        x"085E31C4", x"A49BDB58", x"DBD2D8EE", x"D266ED56",
        x"6FF5603F", x"40838309", x"0A363CB4", x"8BBB399A",
        x"955F7FC3", x"008A033C", x"0A883F30", x"82A30FCA",
        x"20BCC38A", x"893F3682", x"B70FB221", x"ACC5EA9C",
        x"7F4903B6", x"09B435B8", x"BD938D69", x"2F76E336",
        x"4AB5BFBD", x"818D052E", x"1EE44659", x"95D57CFF",
        x"0A023C0C", x"882B30FA", x"A21FCC40", x"A983F508",
        x"3E3084A3", x"1BCA58BD", x"D38CE92A", x"76FD360E",
        x"B427B8D1", x"92E56E5F", x"65C35C8B", x"CB38BA93",
        x"9F694377", x"8B313AA6", x"9FD740F3", x"82290CF6",
        x"2A34FCBA", x"0B9C3948", x"97B371AB", x"25FADC1E",
        x"C846B197", x"A571DF24", x"C2DA8EDF", x"26C2D68E",
        x"F72632D4", x"AEFBE618", x"5451F9E4", x"145879D1",
        x"14E67A55", x"1DFE4C05", x"A81DF04C", x"21A8C5F2",
        x"9C2F48E3", x"B249ADB5", x"EDBC6D89", x"6D376EB3",
        x"67AB51FB", x"E4185851", x"D1E4E45A", x"59DDD4CC",
        x"FAAA1FFC", x"40098035", x"00BE0384", x"09183650",
        x"B5E3BC49", x"89B535BE", x"BD878D11", x"2E66E556",
        x"5FF5C03C", x"808B033A", x"0A9C3F48", x"83B309AA",
        x"35FCBC0B", x"88393096", x"A377CB30", x"BAA39FC9",
        x"40B783B1", x"09A635D4", x"BCFB8A19", x"3C5689F7",
        x"3432B8AF", x"93E16847", x"7193256A", x"DF7EC306",
        x"8A173C72", x"892F36E2", x"B64FB5A1", x"BDC58C9D",
        x"2B4EFBA6", x"19D454F9", x"FA141C78", x"4911B665",
        x"B55DBFCD", x"80AD03EE", x"08643158", x"A7D3D0E8",
        x"E2724D2D", x"AEEDE66C", x"5569FF74", x"03380A90",
        x"3F608343", x"0B8A393C", x"968B773B", x"329AAF5F",
        x"E3C04881", x"B305AA1D", x"FC4C09A8", x"35F0BC23",
        x"88C932B6", x"AFB7E1B0", x"45A19DC5", x"4C9FAB41",
        x"FB841918", x"5651F5E4", x"3C5889D3", x"34EABA7F",
        x"9D014E07", x"A411D864", x"D15AE7DE", x"50C5E29C",
        x"4F49A3B5", x"C9BCB58B", x"BD398E95", x"277ED306",
        x"EA167C75", x"093E3684", x"B71BB259", x"ADD5ECFC",
        x"6A097C37", x"08B233AC", x"A9EBF478", x"39109663",
        x"754B3FBA", x"819F0542", x"1F8C4129", x"86F5163E",
        x"74853B1E", x"9A475D93", x"CD68AF73", x"E3284AF1",
        x"BE2584DD", x"1ACE5EA5", x"C7DC90CB", x"62BB4F9B",
        x"A159C7D4", x"90FB621B", x"4C5BA9D9", x"F4D43AF8",
        x"9E13446B", x"99795717", x"F2702D20", x"EEC2668D",
        x"572FF2E0", x"2E40E582", x"5D0DCE2C", x"A4EBDA78",
        x"DD12CE6E", x"A567DF50", x"C3E2884F", x"31A2A5CF",
        x"DCA0CBC2", x"B88F9321", x"6AC77E93", x"076A137C",
        x"6B097A37", x"1CB24BAD", x"B9ED946D", x"796F1762",
        x"734D2BAE", x"F9E61454", x"79F91416", x"7875113E",
        x"6685571F", x"F2402D80", x"ED026E0D", x"642F58E3",
        x"D248EDB2", x"6DAD6DEF", x"6C63694B", x"A40F38F1"
    );

    constant c_payload_tv2_raw : payload_vector_t := (
        x"468B9739", x"72972F72", x"E32E4AE5", x"BE5D85CD",
        x"1CAE4BE5", x"B85D91CD", x"64AF5BE3", x"D848D1B2",
        x"E5AE5DE5", x"CC5CA9CB", x"F4B83B90", x"9963574B",
        x"F3B82990", x"F5623F4C", x"83AB09FA", x"341CB84B",
        x"91B96597", x"5D73CF28", x"A2F3CE28", x"A4F3DA28",
        x"DCF2CA2E", x"BCE78A51", x"3DE68C57", x"29F2F42E",
        x"38E4925B", x"6DDB6CDB", x"6ADB7EDB", x"06DA16DC",
        x"76C936B6", x"B7B7B1B1", x"A5A5DDDC", x"CCCAAABF",
        x"FF800100", x"06001400", x"78011006", x"6015407F",
        x"81010606", x"14147879", x"11166675", x"553FFE80",
        x"07001200", x"6C016807", x"7013206A", x"C17E8707",
        x"12126C6D", x"696F7763", x"334AABBF", x"F9801500",
        x"7E010406", x"18145079", x"E1144679", x"95157E7F",
        x"05021E0C", x"442998F5", x"523FEC80", x"6B017A07",
        x"1C12486D", x"B16DA76D", x"D36CEB6A", x"7B7D1B0E",
        x"5A25DCDC", x"CACABEBF", x"87811106", x"6615547F",
        x"F9001600", x"74013806", x"90176073", x"412B86F9",
        x"16167475", x"393E9687", x"7713326A", x"AD7FEF00",
        x"62014C07", x"A811F064", x"2158C7D2", x"90EF6263",
        x"4D4BAFB9", x"E1944579", x"9F15427F", x"8D012E06",
        x"E4165875", x"D13CE68A", x"573DF28C", x"2F28E2F2",
        x"4E2DA4ED", x"DA6CDD6A", x"CF7EA307", x"CA10BC63",
        x"894937B6", x"B1B7A5B1", x"DDA4CDDA", x"ACDFEAC0",
        x"7E810706", x"12146C79", x"69177673", x"352ABEFF",
        x"86011406", x"7815107E", x"6105461F", x"94417987",
        x"15127E6D", x"056E1F64", x"43598BD5", x"38FE9207",
        x"6C13686B", x"717B271A", x"D25EEDC6", x"6C956B7F",
        x"7B031A0A", x"5C3DC88C", x"B32BAAF9", x"FE140478",
        x"19105661", x"F5443F98", x"815307EA", x"107C6109",
        x"463794B1", x"7BA719D2", x"54EDFA6C", x"1D684F71",
        x"A325CADC", x"BECB86B9", x"17967175", x"273ED286",
        x"EF166275", x"4D3FAE81", x"E7045219", x"EC5469F9",
        x"74173872", x"912F66E3", x"564BF5B8", x"3D908D63",
        x"2F4AE3BE", x"4985B51D", x"BE4D85AD", x"1DEE4C65",
        x"A95DF7CC", x"30A8A3F3", x"C828B0F3", x"A229CCF4",
        x"AA3BFC98", x"0B503BE0", x"9843518B", x"E5385E91",
        x"C764935B", x"6BDB78DB", x"12DA6EDD", x"66CF56A3",
        x"F7C830B0", x"A3A3C9C8", x"B4B3BBA9", x"99F5543F",
        x"F8801300", x"6A017C07", x"0812306C", x"A16BC778",
        x"93136A6B", x"7D7B0F1A", x"225CCDCA", x"ACBFEB80",
        x"79011606", x"7415387E", x"91076613", x"546BF978",
        x"17107261", x"2D46EF96", x"6175473F", x"92816F07",
        x"62134C6B", x"A979F714", x"3278AD13", x"EE686571",
        x"5F27C2D0", x"8EE3264A", x"D5BEFD86", x"0D142E78",
        x"E5125E6D", x"C56C9F6B", x"437B8B19", x"3A569DF7",
        x"4C33A8A9", x"F3F42838", x"F092236C", x"CB6ABB7F",
        x"9B015A07", x"DC10C862", x"B14FA7A1", x"D1C4E49A",
        x"5B5DDBCC", x"D8AAD3FE", x"E8067015", x"207EC106",
        x"86171472", x"792D16EE", x"7665355E", x"BFC78091",
        x"03660B54", x"3BF89813", x"506BE178", x"47119265",
        x"6D5F6FC3", x"608B433B", x"8A993F56", x"83F70832",
        x"30ACA3EB", x"C878B113", x"A669D574", x"FF3A029C",
        x"0F4823B0", x"C9A2B5CF", x"BCA18BC5", x"389E9347",
        x"6B93796B", x"177A731D", x"2A4EFDA6", x"0DD42CF8",
        x"EA127C6D", x"096E3764", x"B35BABD9", x"F8D412F8",
        x"6E116467", x"5953D7E8", x"F072212C", x"C6EA967F",
        x"75033E0A", x"843F1882", x"530DEA2C", x"7CE90A76",
        x"3D348EBB", x"279AD15E", x"E7C65095", x"E37C4B09",
        x"BA359CBD", x"4B8FB921", x"96C5769F", x"3742B38F",
        x"A921F6C4", x"3698B753", x"B3E9A875", x"F13C2688",
        x"D732F2AE", x"2FE4E05A", x"41DD84CD", x"1AAE5FE5",
        x"C05C81CB", x"04BA1B9C", x"5949D7B4", x"F1BA259C",
        x"DD4ACFBE", x"A187C510", x"9E63454B", x"9FB94197",
        x"85711F26", x"42D58EFD", x"260ED426", x"F8D612F4",
        x"6E396497", x"5B73DB28", x"DAF2DE2E", x"C4E69A57",
        x"5DF3CC28", x"A8F3F228", x"2CF0EA22", x"EAE8FFB7"
    );

    constant c_payload_tv3_raw : payload_vector_t := (
        x"68AD73EF", x"2862F14E", x"27A4D1DA", x"E4DE5AC5",
        x"DE9CC74A", x"93BF6983", x"750B3E3A", x"849F1B42",
        x"5B8DD92C", x"D6EAF67E", x"3504BE1B", x"845919D6",
        x"54F5FA3C", x"1C884B31", x"BAA59FDD", x"40CF82A1",
        x"0FC62094", x"C37A8B1F", x"3A429D8F", x"4D23AEC9",
        x"E6B457B9", x"F1942578", x"DF12C26E", x"8D672F52",
        x"E3EE4865", x"B15DA7CD", x"D0ACE3EA", x"487DB10D",
        x"A62DD4EC", x"FA6A1D7C", x"4F09A235", x"CCBCAB8B",
        x"F9381690", x"77613346", x"AB97F970", x"172072C1",
        x"2E86E716", x"5275ED3C", x"6E896737", x"52B3EFA8",
        x"61F14427", x"98D152E7", x"EE5065E1", x"5C47C990",
        x"B563BF49", x"83B509BE", x"3584BD1B", x"8E5925D6",
        x"DCF6CA36", x"BCB78BB1", x"39A695D7", x"7CF30A2A",
        x"3CFC8A0B", x"3C3A889F", x"3342AB8F", x"F92016C0",
        x"76813706", x"B217AC71", x"E92476D9", x"36D6B6F7",
        x"B631B4A5", x"BBDD98CD", x"52AFEFE0", x"60414187",
        x"85111E66", x"45559FFD", x"400F8021", x"00C60294",
        x"0F782310", x"CA62BD4F", x"8FA121C6", x"C4969B77",
        x"5B33DAA8", x"DFF2C02E", x"80E70252", x"0DEC2C68",
        x"E972772D", x"32EEAE67", x"E5505FE1", x"C044819B",
        x"055A1FDC", x"40C982B5", x"0FBE2184", x"C51A9E5F",
        x"45C39C89", x"4B37BAB1", x"9FA541DF", x"84C11A86",
        x"5F15C27C", x"8D0B2E3A", x"E49E5B45", x"DB9CD94A",
        x"D7BEF186", x"2514DE7A", x"C51E9E47", x"45939D69",
        x"4F77A331", x"CAA4BFDB", x"80D902D6", x"0EF42638",
        x"D492FB6E", x"1B645B59", x"DBD4D8FA", x"D21EEC46",
        x"6995757F", x"3F02820F", x"0C2228CC", x"F2AA2FFC",
        x"E00A403D", x"808D032E", x"0AE43E58", x"85D31CEA",
        x"4A7DBD0D", x"8E2D24EE", x"DA66DD56", x"CFF6A037",
        x"C0B083A3", x"09CA34BC", x"BB8B9939", x"5697F770",
        x"3320AAC3", x"FE880730", x"12A06FC1", x"60874313",
        x"8A693D76", x"8F3722B2", x"CFAEA1E7", x"C45099E3",
        x"544BF9B8", x"15907D61", x"0F462394", x"C97AB71F",
        x"B241AD85", x"ED1C6E49", x"65B75DB3", x"CDA8ADF3",
        x"EC2868F1", x"72272CD2", x"EAEE7E65", x"055E1FC4",
        x"40998355", x"0BFE3804", x"901B605B", x"41DB84D9",
        x"1AD65EF5", x"C63C948B", x"7B3B1A9A", x"5F5DC3CC",
        x"88AB33FA", x"A81FF040", x"2180C502", x"9E0F4423",
        x"98C952B7", x"EFB061A1", x"45C79C91", x"4B67BB51",
        x"9BE5585F", x"D1C0E482", x"5B0DDA2C", x"DCEACA7E",
        x"BD078E11", x"2466D956", x"D7F6F036", x"20B4C3BA",
        x"899F3542", x"BF8F8121", x"06C61694", x"77793316",
        x"AA77FD30", x"0EA027C0", x"D082E30E", x"4A25BCDD",
        x"8ACD3EAE", x"87E71052", x"61ED446F", x"99615747",
        x"F3902960", x"F742338C", x"A92BF6F8", x"3610B463",
        x"B94997B5", x"71BF2582", x"DD0ECE26", x"A4D7DAF0",
        x"DE22C4CE", x"9AA75FD3", x"C0E88273", x"0D2A2EFC",
        x"E60A543D", x"F88C1328", x"6AF17E27", x"04D21AEC",
        x"5E69C574", x"9F3B429B", x"8F5923D6", x"C8F6B237",
        x"ACB1EBA4", x"79D914D6", x"7AF51E3E", x"44859B1D",
        x"5A4FDDA0", x"CDC2AC8F", x"EB207AC1", x"1E864715",
        x"927D6D0F", x"6E2364CB", x"5ABBDF98", x"C15287EF",
        x"1062614D", x"47AF91E1", x"64475993", x"D568FF72",
        x"032C0AE8", x"3E708523", x"1ECA46BD", x"978D712F",
        x"26E2D64E", x"F5A63DD4", x"8CFB2A1A", x"FC5E09C4",
        x"3498BB53", x"9BE95877", x"D130E6A2", x"57CDF0AC",
        x"23E8C872", x"B12FA6E1", x"D644F59A", x"3D5C8FCB",
        x"20BAC39E", x"89473792", x"B16FA761", x"D344EB9A",
        x"795D17CE", x"70A523DE", x"C8C6B297", x"AF71E324",
        x"4AD9BED5", x"86FD160E", x"742538DE", x"92C76E93",
        x"676B537B", x"EB187A51", x"1DE64C55", x"A9FDF40C",
        x"382890F3", x"622B4CFB", x"AA19FC54", x"09F83410",
        x"B8639149", x"67B751B3", x"E5A85DF1", x"CC24A8DB",
        x"F2D82ED0", x"E6E2564D", x"F5AC3DE8", x"8C73292A",
        x"F6FE3604", x"B41BB859", x"91D564FF", x"5A03DC08",
        x"C832B0AF", x"A3E1C844", x"B19BA559", x"1AAB9107"
    );

    constant c_payload_tv4_raw : payload_vector_t := (
        x"8ACF3EA2", x"87CF10A2", x"63CD48AF", x"B3E1A845",
        x"F19C2548", x"DFB2C1AE", x"85E71C52", x"49EDB46D",
        x"B96D976D", x"736F2B62", x"FB4E1BA4", x"59D9D4D4",
        x"FAFA1E1C", x"444999B5", x"55BFFD80", x"0D002E00",
        x"E402580D", x"D02CE0EA", x"427D8D0D", x"2E2EE4E6",
        x"5A55DDFC", x"CC0AA83F", x"F0802300", x"CA02BC0F",
        x"882130C6", x"A297CF70", x"A323CAC8", x"BEB387A9",
        x"11F66435", x"58BFD380", x"E902760D", x"342EB8E7",
        x"92516DE7", x"6C5369EB", x"747B391A", x"965F75C3",
        x"3C8A8B3F", x"3A829F0F", x"42238CC9", x"2AB6FFB6",
        x"01B405B8", x"1D904D61", x"AF45E39C", x"4949B7B5",
        x"B1BDA58D", x"DD2CCEEA", x"A67FD500", x"FE02040C",
        x"182850F1", x"E2244CD9", x"AAD5FEFC", x"06081430",
        x"78A113C6", x"6895737F", x"2B02FA0E", x"1C2448D9",
        x"B2D5AEFD", x"E60C5429", x"F8F41238", x"6C916B67",
        x"7B531BEA", x"587DD10C", x"E62A54FD", x"FA0C1C28",
        x"48F1B225", x"ACDDEACC", x"7EA907F6", x"103460B9",
        x"43978971", x"3726B2D7", x"AEF1E624", x"54D9FAD4",
        x"1EF84611", x"9465795F", x"17C2708D", x"232ECAE6",
        x"BE5785F1", x"1C2648D5", x"B2FDAE0D", x"E42C58E9",
        x"D274ED3A", x"6E9D674F", x"53A3E9C8", x"74B13BA6",
        x"99D754F3", x"FA281CF0", x"4A21BCC5", x"8A9D3F4E",
        x"83A709D2", x"34ECBA6B", x"9D794F17", x"A271CD24",
        x"AEDBE6D8", x"56D1F6E4", x"3658B5D3", x"BCE98A75",
        x"3D3E8E87", x"2712D26E", x"ED666F55", x"63FF4803",
        x"B009A035", x"C0BC838B", x"093A369C", x"B74BB3B9",
        x"A995F57C", x"3F088233", x"0CAA2BFC", x"F80A103C",
        x"60894337", x"8AB13FA6", x"81D704F2", x"1A2C5CE9",
        x"CA74BD3B", x"8E992756", x"D3F6E836", x"70B523BE",
        x"C986B517", x"BE718525", x"1EDE46C5", x"969D774F",
        x"33A2A9CF", x"F4A03BC0", x"9883530B", x"EA387C91",
        x"0B663B54", x"9BFB581B", x"D058E1D2", x"44ED9A6D",
        x"5D6FCF60", x"A343CB88", x"B93396A9", x"77F73032",
        x"A0AFC3E0", x"8843318A", x"A53FDE80", x"C702920F",
        x"6C2368CB", x"72BB2F9A", x"E15E47C5", x"909D634F",
        x"4BA3B9C9", x"94B57BBF", x"1982550D", x"FE2C04E8",
        x"1A705D21", x"CEC4A69B", x"D758F3D2", x"28ECF26A",
        x"2D7CEF0A", x"623D4C8F", x"AB21FAC4", x"1E984751",
        x"93E5685F", x"71C3248A", x"DB3EDA86", x"DF16C276",
        x"8D372EB2", x"E7AE51E5", x"E45C59C9", x"D4B4FBBA",
        x"199C5549", x"FFB401B8", x"05901D60", x"4F41A385",
        x"C91CB64B", x"B5B9BD95", x"8D7D2F0E", x"E2264CD5",
        x"AAFDFE0C", x"042818F0", x"5221ECC4", x"6A997F57",
        x"03F2082C", x"30E8A273", x"CD28AEF3", x"E62854F1",
        x"FA241CD8", x"4AD1BEE5", x"865D15CE", x"7CA50BDE",
        x"38C4929B", x"6F5B63DB", x"48DBB2D9", x"AED5E6FC",
        x"5609F434", x"38B89393", x"696B777B", x"331AAA5F",
        x"FDC00C80", x"2B00FA02", x"1C0C4829", x"B0F5A23D",
        x"CC8CAB2B", x"FAF81E10", x"44619945", x"579FF140",
        x"2780D102", x"E60E5425", x"F8DC12C8", x"6EB167A7",
        x"51D3E4E8", x"5A71DD24", x"CEDAA6DF", x"D6C0F682",
        x"370CB22B", x"ACF9EA14", x"7C790916", x"3674B53B",
        x"BE998755", x"13FE6805", x"701F2042", x"C18E8527",
        x"1ED246ED", x"966D756F", x"3F62834F", x"0BA239CC",
        x"94AB7BFB", x"181A505D", x"E1CC44A9", x"9BF5583F",
        x"D080E302", x"4A0DBC2D", x"88ED326E", x"AD67EF50",
        x"63E14847", x"B191A565", x"DF5CC3CA", x"88BF3382",
        x"A90FF620", x"34C0BA83", x"9F094237", x"8CB12BA6",
        x"F9D614F4", x"7A391C96", x"4B75BB3D", x"9A8D5F2F",
        x"C2E08E43", x"258ADD3E", x"CE86A717", x"D270ED22",
        x"6ECD66AF", x"57E3F048", x"21B0C5A2", x"9DCF4CA3",
        x"ABC9F8B4", x"13B86991", x"75673F52", x"83EF0862",
        x"314CA7AB", x"D1F8E412", x"586DD16C", x"E76A537D",
        x"EB0C7A29", x"1CF64A35", x"BCBD8B8D", x"392E96E7",
        x"765335EA", x"BC7F8901", x"3606B417", x"B8719125",
        x"66DF56C3", x"F6883730", x"B2A3AFC9", x"2A7EC1F4"
    );

    constant c_payload_tv5_raw : payload_vector_t := (
        x"ACF1EA24", x"7CD90AD6", x"3EF4863B", x"149A7B5D",
        x"1BCE58A5", x"D3DCE8CA", x"72BD2F8E", x"E12646D5",
        x"96FD760F", x"3422B8CF", x"92A16FC7", x"6093436B",
        x"8B793B16", x"9A775D33", x"CEA8A7F3", x"D028E0F2",
        x"422D8CED", x"2A6EFD66", x"0F5423F8", x"C812B06F",
        x"A161C744", x"939B695B", x"77DB30DA", x"A2DFCEC0",
        x"A683D708", x"F2322CAC", x"EBEA787D", x"110E6625",
        x"54DFFAC0", x"1E804701", x"92056C1F", x"6843718B",
        x"253ADE9E", x"C7469397", x"6973772B", x"32FAAE1F",
        x"E4405981", x"D504FE1A", x"045C19C8", x"54B1FBA4",
        x"19D854D1", x"FAE41E58", x"45D19CE5", x"4A5FBDC1",
        x"8C852B1E", x"FA461D94", x"4D79AF15", x"E27C4D09",
        x"AE35E4BC", x"5B89D934", x"D6BAF79E", x"3144A79B",
        x"D158E7D2", x"50EDE26C", x"4D69AF75", x"E33C4A89",
        x"BF3582BD", x"0F8E2124", x"C6DA96DF", x"76C3368A",
        x"B73FB281", x"AF05E21C", x"4C49A9B5", x"F5BC3D88",
        x"8D332EAA", x"E7FE5005", x"E01C4049", x"81B505BE",
        x"1D844D19", x"AE55E5FC", x"5C09C834", x"B0BBA399",
        x"C954B7FB", x"B019A055", x"C1FC840B", x"183A509D",
        x"E34C4BA9", x"B9F5943D", x"788F1322", x"6ACD7EAF",
        x"07E2104C", x"61A945F7", x"9C3148A7", x"B3D1A8E5",
        x"F25C2DC8", x"ECB26BAD", x"79EF1462", x"794D17AE",
        x"71E5245E", x"D9C6D496", x"FB761B34", x"5AB9DF94",
        x"C17A871F", x"12426D8D", x"6D2F6EE3", x"664B55BB",
        x"FD980D50", x"2FE0E042", x"418D852D", x"1EEE4665",
        x"955D7FCF", x"00A203CC", x"08A833F0", x"A823F0C8",
        x"22B0CFA2", x"A1CFC4A0", x"9BC3588B", x"D338EA92",
        x"7F6D036E", x"0B643B58", x"9BD358EB", x"D278ED12",
        x"6E6D656F", x"5F63C348", x"8BB339AA", x"95FF7C03",
        x"080A303C", x"A08BC338", x"8A933F6A", x"837F0B02",
        x"3A0C9C2B", x"48FBB219", x"AC55E9FC", x"74093836",
        x"90B763B3", x"49ABB5F9", x"BC15887D", x"310EA627",
        x"D4D0FAE2", x"1E4C45A9", x"9DF54C3F", x"A881F304",
        x"2A18FC52", x"09EC3468", x"B9739729", x"72F72E32",
        x"E4AE5BE5", x"D85CD1CA", x"E4BE5B85", x"D91CD64A",
        x"F5BE3D84", x"8D1B2E5A", x"E5DE5CC5", x"CA9CBF4B",
        x"83B90996", x"3574BF3B", x"82990F56", x"23F4C83A",
        x"B09FA341", x"CB84B91B", x"965975D7", x"3CF28A2F",
        x"3CE28A4F", x"3DA28DCF", x"2CA2EBCE", x"78A513DE",
        x"68C5729F", x"2F42E38E", x"4925B6DD", x"B6CDB6AD",
        x"B7EDB06D", x"A16DC76C", x"936B6B7B", x"7B1B1A5A",
        x"5DDDCCCC", x"AAABFFF8", x"00100060", x"01400780",
        x"11006601", x"5407F810", x"10606141", x"47879111",
        x"66675553", x"FFE80070", x"012006C0", x"16807701",
        x"3206AC17", x"E8707121", x"26C6D696", x"F7763334",
        x"AABBFF98", x"015007E0", x"10406181", x"45079E11",
        x"44679951", x"57E7F050", x"21E0C442", x"998F5523",
        x"FEC806B0", x"17A071C1", x"2486DB16", x"DA76DD36",
        x"CEB6A7B7", x"D1B0E5A2", x"5DCDCCAC", x"ABEBF878",
        x"11106661", x"5547FF90", x"01600740", x"13806901",
        x"76073412", x"B86F9161", x"67475393", x"E9687771",
        x"3326AAD7", x"FEF00620", x"14C07A81", x"1F064215",
        x"8C7D290E", x"F62634D4", x"BAFB9E19", x"445799F1",
        x"5427F8D0", x"12E06E41", x"65875D13", x"CE68A573",
        x"DF28C2F2", x"8E2F24E2", x"DA4EDDA6", x"CDD6ACF7",
        x"EA307CA1", x"0BC63894", x"937B6B1B", x"7A5B1DDA",
        x"4CDDAACD", x"FEAC07E8", x"10706121", x"46C79691",
        x"77673352", x"ABEFF860", x"11406781", x"5107E610",
        x"5461F944", x"17987151", x"27E6D056", x"E1F64435",
        x"98BD538F", x"E92076C1", x"3686B717", x"B271AD25",
        x"EEDC66C9", x"56B7F7B0", x"31A0A5C3", x"DC88CB32",
        x"BAAF9FE1", x"40478191", x"05661F54", x"43F98815",
        x"307EA107", x"C6109463", x"794B17BA", x"719D254E",
        x"DFA6C1D6", x"84F71A32", x"5CADCBEC", x"B86B9179",
        x"67175273", x"ED286EF1", x"662754D3", x"FAE81E70",
        x"45219EC5", x"469F9741", x"73872912", x"75C8F24C"
    );

    constant c_payload_test_array_raw : payload_test_array_t := (
        c_payload_tv1_raw,
        c_payload_tv2_raw,
        c_payload_tv3_raw,
        c_payload_tv4_raw,
        c_payload_tv5_raw
    );    


end package test_vector_pkg;

package body test_vector_pkg is


end package body test_vector_pkg;
