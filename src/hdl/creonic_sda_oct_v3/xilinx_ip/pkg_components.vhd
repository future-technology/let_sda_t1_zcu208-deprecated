--!
--! Copyright (C) 2011 Creonic GmbH
--!
--! @file
--! @brief  Component declarations of all Xilinx's entities within design.
--! @author Nhan Nguyen
--! @date   2022/08/08
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package pkg_components is

	component dwidth_8_to_16_llr
	port (
		aclk          : in  std_logic;
		aresetn       : in  std_logic;
		s_axis_tvalid : in  std_logic;
		s_axis_tready : out std_logic;
		s_axis_tdata  : in  std_logic_vector(63 downto 0);
		s_axis_tlast  : in  std_logic;
		m_axis_tvalid : out std_logic;
		m_axis_tready : in  std_logic;
		m_axis_tdata  : out std_logic_vector(127 downto 0);
		m_axis_tlast  : out std_logic
	);
	end component;

	component dwidth_8_to_16
	port (
		aclk          : in  std_logic;
		aresetn       : in  std_logic;
		s_axis_tvalid : in  std_logic;
		s_axis_tready : out std_logic;
		s_axis_tdata  : in  std_logic_vector(7 downto 0);
		s_axis_tlast  : in  std_logic;
		m_axis_tvalid : out std_logic;
		m_axis_tready : in  std_logic;
		m_axis_tdata  : out std_logic_vector(15 downto 0);
		m_axis_tlast  : out std_logic
	);
	end component;

	component dwidth_16_to_8
	port (
		aclk          : in  std_logic;
		aresetn       : in  std_logic;
		s_axis_tvalid : in  std_logic;
		s_axis_tready : out std_logic;
		s_axis_tdata  : in  std_logic_vector(15 downto 0);
		s_axis_tlast  : in  std_logic;
		m_axis_tvalid : out std_logic;
		m_axis_tready : in  std_logic;
		m_axis_tdata  : out std_logic_vector(7 downto 0);
		m_axis_tlast  : out std_logic
	);
	end component;

	component axis_16bit_clk_conv
	port (
		s_axis_aresetn : in  std_logic;
		m_axis_aresetn : in  std_logic;
		s_axis_aclk    : in  std_logic;
		s_axis_tvalid  : in  std_logic;
		s_axis_tready  : out std_logic;
		s_axis_tdata   : in  std_logic_vector(15 downto 0);
		s_axis_tlast   : in  std_logic;
		m_axis_aclk    : in  std_logic;
		m_axis_tvalid  : out std_logic;
		m_axis_tready  : in  std_logic;
		m_axis_tdata   : out std_logic_vector(15 downto 0);
		m_axis_tlast   : out std_logic
	);
	end component;

	component axis_64bit_clk_conv
	port (
		s_axis_aresetn : in  std_logic;
		m_axis_aresetn : in  std_logic;
		s_axis_aclk    : in  std_logic;
		s_axis_tvalid  : in  std_logic;
		s_axis_tready  : out std_logic;
		s_axis_tdata   : in  std_logic_vector(63 downto 0);
		m_axis_aclk    : in  std_logic;
		m_axis_tvalid  : out std_logic;
		m_axis_tready  : in  std_logic;
		m_axis_tdata   : out std_logic_vector(63 downto 0)
	);
	end component;

	component axis_128bit_clk_conv
	port (
		s_axis_aresetn : in  std_logic;
		m_axis_aresetn : in  std_logic;
		s_axis_aclk    : in  std_logic;
		s_axis_tvalid  : in  std_logic;
		s_axis_tready  : out std_logic;
		s_axis_tdata   : in  std_logic_vector(127 downto 0);
		s_axis_tlast   : in  std_logic;
		m_axis_aclk    : in  std_logic;
		m_axis_tvalid  : out std_logic;
		m_axis_tready  : in  std_logic;
		m_axis_tdata   : out std_logic_vector(127 downto 0);
		m_axis_tlast   : out std_logic
	);
	end component;

	component ldpc_dec
	port (
		reset_n              : in  std_logic;
		core_clk             : in  std_logic;
		s_axi_aclk           : in  std_logic;
		interrupt            : out std_logic;
		s_axis_ctrl_aclk     : in  std_logic;
		s_axis_ctrl_tready   : out std_logic;
		s_axis_ctrl_tvalid   : in  std_logic;
		s_axis_ctrl_tdata    : in  std_logic_vector(39 downto 0);
		s_axis_din_aclk      : in  std_logic;
		s_axis_din_tready    : out std_logic;
		s_axis_din_tvalid    : in  std_logic;
		s_axis_din_tlast     : in  std_logic;
		s_axis_din_tdata     : in  std_logic_vector(127 downto 0);
		m_axis_status_aclk   : in  std_logic;
		m_axis_status_tready : in  std_logic;
		m_axis_status_tvalid : out std_logic;
		m_axis_status_tdata  : out std_logic_vector(39 downto 0);
		m_axis_dout_aclk     : in  std_logic;
		m_axis_dout_tready   : in  std_logic;
		m_axis_dout_tvalid   : out std_logic;
		m_axis_dout_tlast    : out std_logic;
		m_axis_dout_tdata    : out std_logic_vector(127 downto 0)
	);
	end component;

	component ldpc_enc
	port (
		reset_n              : in  std_logic;
		core_clk             : in  std_logic;
		s_axi_aclk           : in  std_logic;
		interrupt            : out std_logic;
		s_axis_ctrl_aclk     : in  std_logic;
		s_axis_ctrl_tready   : out std_logic;
		s_axis_ctrl_tvalid   : in  std_logic;
		s_axis_ctrl_tdata    : in  std_logic_vector(39 downto 0);
		s_axis_din_aclk      : in  std_logic;
		s_axis_din_tready    : out std_logic;
		s_axis_din_tvalid    : in  std_logic;
		s_axis_din_tlast     : in  std_logic;
		s_axis_din_tdata     : in  std_logic_vector(127 downto 0);
		m_axis_status_aclk   : in  std_logic;
		m_axis_status_tready : in  std_logic;
		m_axis_status_tvalid : out std_logic;
		m_axis_status_tdata  : out std_logic_vector(39 downto 0);
		m_axis_dout_aclk     : in  std_logic;
		m_axis_dout_tready   : in  std_logic;
		m_axis_dout_tvalid   : out std_logic;
		m_axis_dout_tlast    : out std_logic;
		m_axis_dout_tdata    : out std_logic_vector(127 downto 0)
	);
	end component;

	component dc_fifo_phy
	port (
		m_aclk        : in  std_logic;
		s_aclk        : in  std_logic;
		s_aresetn     : in  std_logic;
		s_axis_tvalid : in  std_logic;
		s_axis_tready : out std_logic;
		s_axis_tdata  : in  std_logic_vector(15 downto 0);
		m_axis_tvalid : out std_logic;
		m_axis_tready : in  std_logic;
		m_axis_tdata  : out std_logic_vector(15 downto 0)
	);
	end component;

end package;
