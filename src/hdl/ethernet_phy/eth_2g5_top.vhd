-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
--  Project    : Vivado script 
--  Author     : Gustavo Martin
--  Description: Wrapper for Ethernet IP               
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;
use work.let_pckg.all;
-- Xilinx components
--library unisim;
--use unisim.vcomponents.all;

-- instantiates the Ethernet IP and GTs
entity eth_2g5_top is
    port(
        s_axi_resetn               : in  std_logic;
        glbl_rst            : in  std_logic;
        ref_clk_50          : in  std_logic;
        clk_ref156            : in  std_logic;
        eth_txp         : out std_logic; -- the GT serial link I/O pins
        eth_txn         : out std_logic;
        eth_rxp         : in  std_logic;
        eth_rxn         : in  std_logic;
        s_axi_aclk          : in  std_logic;
        AXI_ETH_REG_IF_m    : in  AXI_Lite_master;
        AXI_ETH_REG_IF_s    : out AXI_Lite_slave;
        rx_user_rst_out     : out std_logic;
        rx_clk_out          : out std_logic; -- all rx logic touching this ip is clocked by this clock

        rx_axis_tvalid      : out std_logic; -- ethernet rx axi stream interface
        rx_axis_tdata       : out std_logic_vector(7 downto 0);
        rx_axis_tlast       : out std_logic;
        rx_axis_tuser       : out std_logic;
        tx_user_rst_out     : out std_logic;
        tx_clk_out          : out std_logic;
        tx_axis_tready      : out std_logic; -- ethernet tx axi stream interface
        tx_axis_tvalid      : in  std_logic;
        tx_axis_tdata       : in  std_logic_vector(7 downto 0);
        tx_axis_tlast       : in  std_logic;
        tx_axis_tuser       : in  std_logic;
        s_axis_pause_tdata  : in  std_logic_vector(15 DOWNTO 0);
        s_axis_pause_tvalid : in  std_logic
    );
end eth_2g5_top;

architecture rtl of eth_2g5_top is

    signal s_axi_araddr_constr : std_logic_vector(17 downto 0);
    signal s_axi_awaddr_constr : std_logic_vector(17 downto 0);
    signal clk_wide            : std_logic; -- 125/8 MHz clock for 64 bit side interface
    signal rx_mac_aclk         : std_logic;
    signal tx_mac_aclk         : std_logic;
    signal s_axis_tx_tdata     : std_logic_vector(7 downto 0);
    signal s_axis_tx_tlast     : std_logic;
    signal s_axis_tx_tready    : std_logic;
    signal s_axis_tx_tuser     : std_logic;
    signal s_axis_tx_tvalid    : std_logic;
    signal resetn              : std_logic;
    signal status_vector              : std_logic_vector(15 downto 0);

    component axi_ethernet_support
        port(
            s_axi_lite_resetn   : in  std_logic;
            s_axi_araddr        : in  std_logic_vector;
            s_axi_arready       : out std_logic;
            s_axi_arvalid       : in  std_logic;
            s_axi_awaddr        : in  std_logic_vector;
            s_axi_awready       : out std_logic;
            s_axi_awvalid       : in  std_logic;
            s_axi_bready        : in  std_logic;
            s_axi_bresp         : out std_logic_vector;
            s_axi_bvalid        : out std_logic;
            s_axi_rdata         : out std_logic_vector;
            s_axi_rready        : in  std_logic;
            s_axi_rresp         : out std_logic_vector;
            s_axi_rvalid        : out std_logic;
            s_axi_wdata         : in  std_logic_vector;
            s_axi_wready        : out std_logic;
            s_axi_wvalid        : in  std_logic;
            tx_mac_aclk         : out std_logic;
            rx_mac_aclk         : out std_logic;
            glbl_rst            : in  std_logic;
            s_axis_tx_tdata     : in  std_logic_vector;
            s_axis_tx_tlast     : in  std_logic;
            s_axis_tx_tready    : out std_logic;
            s_axis_tx_tuser     : in  std_logic;
            s_axis_tx_tvalid    : in  std_logic;
            m_axis_rx_tdata     : out std_logic_vector;
            m_axis_rx_tlast     : out std_logic;
            m_axis_rx_tuser     : out std_logic;
            m_axis_rx_tvalid    : out std_logic;
            ref_clk             : in  std_logic;
            sfp_rxn           : in  std_logic;
            sfp_rxp           : in  std_logic;
            sfp_txn           : out std_logic;
            sfp_txp           : out std_logic;
            signal_detect       : in  std_logic;
            mgt_clk             : in  std_logic;
            status_vector       : out std_logic_vector;
            s_axis_pause_tdata  : in  std_logic_vector;
            s_axis_pause_tvalid : in  std_logic;
            tx_reset            : out std_logic;
            rx_reset            : out std_logic;
            s_axi_lite_clk      : in  std_logic
        );
    end component axi_ethernet_support;

begin
    rx_clk_out          <= rx_mac_aclk;
    tx_clk_out          <= tx_mac_aclk;
    s_axi_araddr_constr <= AXI_ETH_REG_IF_m.araddr(17 downto 0); -- constrain address to lower bits 
    s_axi_awaddr_constr <= AXI_ETH_REG_IF_m.awaddr(17 downto 0);

    eth_subsys_0_inst : axi_ethernet_support
        port map(
            
            s_axi_lite_resetn   => s_axi_resetn,
            s_axi_awaddr        => s_axi_awaddr_constr,
            s_axi_awvalid       => AXI_ETH_REG_IF_m.awvalid(0),
            s_axi_awready       => AXI_ETH_REG_IF_s.awready(0),
            s_axi_wdata         => AXI_ETH_REG_IF_m.wdata,
            --s_axi_wstrb         => AXI_ETH_REG_IF_m.wstrb,
            s_axi_wvalid        => AXI_ETH_REG_IF_m.wvalid(0),
            s_axi_wready        => AXI_ETH_REG_IF_s.wready(0),
            s_axi_bresp         => AXI_ETH_REG_IF_s.bresp,
            s_axi_bvalid        => AXI_ETH_REG_IF_s.bvalid(0),
            s_axi_bready        => AXI_ETH_REG_IF_m.bready(0),
            s_axi_araddr        => s_axi_araddr_constr,
            s_axi_arvalid       => AXI_ETH_REG_IF_m.arvalid(0),
            s_axi_arready       => AXI_ETH_REG_IF_s.arready(0),
            s_axi_rdata         => AXI_ETH_REG_IF_s.rdata,
            s_axi_rresp         => AXI_ETH_REG_IF_s.rresp,
            s_axi_rvalid        => AXI_ETH_REG_IF_s.rvalid(0),
            s_axi_rready        => AXI_ETH_REG_IF_m.rready(0),
            
--            s_axi_lite_resetn   => s_axi_aresetn, --: in std_logic                      ;  -- AXI-lite interface
--            s_axi_araddr        => s_axi_araddr_constr, --: in std_logic_vector(17 downto 0) ; 
--            s_axi_arready       => s_axi_arready, --: out std_logic                    ; 
--            s_axi_arvalid       => s_axi_arvalid, --: in std_logic                     ; 
--            s_axi_awaddr        => s_axi_awaddr_constr, --: in std_logic_vector(17 downto 0) ; 
--            s_axi_awready       => s_axi_awready, --: out std_logic                    ; 
--            s_axi_awvalid       => s_axi_awvalid, --: in std_logic                     ; 
--            s_axi_bready        => s_axi_bready, --: in std_logic                     ; 
--            s_axi_bresp         => s_axi_bresp, --: out std_logic_vector(1 downto 0) ; 
--            s_axi_bvalid        => s_axi_bvalid, --: out std_logic                    ; 
--            s_axi_rdata         => s_axi_rdata, --: out std_logic_vector(31 downto 0); 
--            s_axi_rready        => s_axi_rready, --: in std_logic                     ; 
--            s_axi_rresp         => s_axi_rresp, --: out std_logic_vector(1 downto 0) ; 
--            s_axi_rvalid        => s_axi_rvalid, --: out std_logic                    ; 
--            s_axi_wdata         => s_axi_wdata, --: in std_logic_vector(31 downto 0) ; 
--            s_axi_wready        => s_axi_wready, --: out std_logic                    ; 
--            s_axi_wvalid        => s_axi_wvalid, --: in std_logic                     ; 

            tx_mac_aclk         => tx_mac_aclk, -- : out std_logic                    ; 
            rx_mac_aclk         => rx_mac_aclk, -- : out std_logic                    ; 
            glbl_rst            => glbl_rst, -- : in std_logic                     ; 

            s_axis_tx_tdata     => tx_axis_tdata, -- : in std_logic_vector(7 downto 0)  ; 
            s_axis_tx_tlast     => tx_axis_tlast, -- : in std_logic                     ; 
            s_axis_tx_tready    => tx_axis_tready, -- : out std_logic                    ; 
            s_axis_tx_tuser     => tx_axis_tuser, -- : in std_logic                     ; 
            s_axis_tx_tvalid    => tx_axis_tvalid, -- : in std_logic                     ;

            m_axis_rx_tdata     => rx_axis_tdata, -- : out std_logic_vector(7 downto 0) ; 
            m_axis_rx_tlast     => rx_axis_tlast, -- : out std_logic                    ; 
            m_axis_rx_tuser     => rx_axis_tuser, -- : out std_logic                    ; 
            m_axis_rx_tvalid    => rx_axis_tvalid, -- : out std_logic                    ; 

            ref_clk             => ref_clk_50, --: in std_logic                     ; -- 50 MHz clock
            sfp_rxn           => eth_rxn, --: in std_logic                     ; 
            sfp_rxp           => eth_rxp, --: in std_logic                     ; 
            sfp_txn           => eth_txn, --: out std_logic                    ; 
            sfp_txp           => eth_txp, --: out std_logic                    ; 

            signal_detect       => '1', -- : in std_logic                     ; 
            mgt_clk             => clk_ref156, -- : in std_logic                     ; 
            status_vector       => status_vector, -- : out std_logic_vector(15 downto 0);

            --      userclk_div8      => clk_wide,

            s_axis_pause_tdata  => s_axis_pause_tdata,
            s_axis_pause_tvalid => s_axis_pause_tvalid,
            tx_reset            => tx_user_rst_out,
            rx_reset            => rx_user_rst_out,
            s_axi_lite_clk      => s_axi_aclk --: in std_logic
        );

end rtl;

