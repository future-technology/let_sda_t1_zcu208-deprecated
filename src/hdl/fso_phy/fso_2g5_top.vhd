-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
--  Project    : Vivado script 
--  Author     : Gustavo Martin
--  Description: Wrapper for GT IP and FIFOs               
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;

entity fso_2g5_top is
    port(
        clk_fec            : in  std_logic; -- clock at which the FEC is running
        reset_fec          : in  std_logic;
        rst_async          : in  std_logic;
        refclk_p             : in  std_logic; -- ref clock for PLL, 156.25 MHz LET clock
        refclk_n             : in  std_logic; -- ref clock for PLL, 156.25 MHz LET clock
        freerun_clk        : in  std_logic; -- free run ref clock, 25 MHz
        fso_txp            : out std_logic; -- the  serial link I/O pins 
        fso_txn            : out std_logic;
        fso_rxp            : in  std_logic;
        fso_rxn            : in  std_logic;
        fso_rx_axis_tdata  : out std_logic_vector(8 - 1 downto 0); -- RX data input FSO -> FEC decoder stream
        fso_rx_axis_tvalid : out std_logic;
        fso_tx_axis_tdata  : in  std_logic_vector(8 - 1 downto 0); -- interface  FEC FEC -> FSO data stream
        fso_tx_axis_tvalid : in  std_logic;
        fso_tx_axis_tready : out std_logic;
        data_good_in       : in  std_logic;
        loopback_in        : in  std_logic
    );
end entity fso_2g5_top;

architecture RTL of fso_2g5_top is

    signal loopback_in_int              : std_logic_vector(2 downto 0);
    signal gtwiz_userclk_tx_usrclk2_out : std_logic;
    signal gtwiz_userclk_rx_usrclk2_out : std_logic;
    signal rx_reset_out                 : std_logic;
    signal tx_reset_out                 : std_logic;
    signal rst_async_n                  : std_logic;
    signal gtwiz_userdata_rx_out        : std_logic_vector(31 downto 0);
    signal gtwiz_userdata_tx_in         : std_logic_vector(31 downto 0);
    signal fso_tx_fifo_tdata            : std_logic_vector(31 downto 0);
    signal fso_tx_fifo_tvalid           : std_logic;
    signal fso_tx_fifo_tready           : std_logic;
    signal rxfifo_tdata                 : std_logic_vector(31 downto 0);
    signal rxfifo_tvalid                : std_logic;
    signal rxfifo_tready                : std_logic;
    signal reset_fec_n                  : std_logic;

    component gtwizard_ultrascale_0_example_top
        port(
            mgtrefclk0_x0y1_p               : in  std_logic;
            mgtrefclk0_x0y1_n               : in  std_logic;
            ch0_gtyrxn_in                 : in  std_logic;
            ch0_gtyrxp_in                 : in  std_logic;
            ch0_gtytxn_out                : out std_logic;
            ch0_gtytxp_out                : out std_logic;
            hb_gtwiz_reset_clk_freerun_in : in  std_logic;
            hb_gtwiz_reset_all_in         : in  std_logic;
            rx_reset_out                  : out std_logic;
            rx_usrclk2_out                : out std_logic;
            gtwiz_userdata_rx_out         : out std_logic_vector(31 downto 0);
            data_good_in                  : in  std_logic;
            tx_reset_out                  : out std_logic;
            tx_usrclk2_out                : out std_logic;
            gtwiz_userdata_tx_in          : in  std_logic_vector(31 downto 0);
            loopback_in                   : in  std_logic_vector(2 downto 0);
            link_down_latched_reset_in    : in  std_logic;
            link_status_out                 : out  std_logic
            --link_down_latched_out           : out  std_logic
        );
    end component gtwizard_ultrascale_0_example_top;

begin

    loopback_in_int <= '0' & loopback_in & '0';
    rst_async_n     <= not rst_async;
    reset_fec_n     <= not reset_fec;

    -- GT instance
    inst_gtwizard_ultrascale_0_example_top : gtwizard_ultrascale_0_example_top
        PORT MAP(
            mgtrefclk0_x0y1_p               => refclk_p,
            mgtrefclk0_x0y1_n               => refclk_n,
            ch0_gtyrxn_in                 => fso_rxn,
            ch0_gtyrxp_in                 => fso_rxp,
            ch0_gtytxn_out                => fso_txn,
            ch0_gtytxp_out                => fso_txp,
            hb_gtwiz_reset_clk_freerun_in => freerun_clk, --gtwiz_reset_clk_freerun_in,
            hb_gtwiz_reset_all_in         => rst_async,
            rx_reset_out                  => rx_reset_out,
            rx_usrclk2_out                => gtwiz_userclk_rx_usrclk2_out,
            gtwiz_userdata_rx_out         => gtwiz_userdata_rx_out,
            data_good_in                  => data_good_in,
            tx_reset_out                  => tx_reset_out,
            tx_usrclk2_out                => gtwiz_userclk_tx_usrclk2_out,
            gtwiz_userdata_tx_in          => gtwiz_userdata_tx_in,
            loopback_in                   => loopback_in_int,
            link_down_latched_reset_in    => '0', --link_down_latched_reset_in,
            link_status_out               => open
            --link_down_latched_out         => open
        );

    -- FIFOs:
    inst_rx_fifo_wave_a : entity work.dc_fifo_phy
        port map(
            m_aclk        => clk_fec,   -- 200 MHz
            s_aclk        => gtwiz_userclk_rx_usrclk2_out, -- 156.25/2 MHz
            s_aresetn     => rst_async_n, -- rx_reset_out, --rst_async_n,
            s_axis_tdata  => gtwiz_userdata_rx_out,
            s_axis_tvalid => '1',
            s_axis_tready => open,
            m_axis_tdata  => rxfifo_tdata,
            m_axis_tvalid => rxfifo_tvalid,
            m_axis_tready => rxfifo_tready
        );

    inst_tx_fifo_wave_a : entity work.dc_fifo_phy
        port map(
            m_aclk        => gtwiz_userclk_tx_usrclk2_out,
            s_aclk        => clk_fec,
            s_aresetn     => reset_fec, --rst_async_n,
            s_axis_tdata  => fso_tx_fifo_tdata,
            s_axis_tvalid => fso_tx_fifo_tvalid,
            s_axis_tready => fso_tx_fifo_tready,
            m_axis_tdata  => gtwiz_userdata_tx_in,
            m_axis_tvalid => open,
            m_axis_tready => '1'
        );

    inst_16_to_8_rx : entity work.dwidth_32_to_8
        port map(
            aclk          => clk_fec,
            aresetn       => reset_fec, --rst_async_n,
            s_axis_tdata  => rxfifo_tdata,
            s_axis_tvalid => rxfifo_tvalid,
            s_axis_tready => rxfifo_tready,
            m_axis_tdata  => fso_rx_axis_tdata, -- to FEC decoder
            m_axis_tvalid => fso_rx_axis_tvalid,
            m_axis_tready => '1'
        );

    inst_8_to_16_tx : entity work.dwidth_8_to_32 -- for both waves a and b
        port map(
            aclk          => clk_fec,
            aresetn       => reset_fec, --rst_async_n,
            s_axis_tdata  => fso_tx_axis_tdata,
            s_axis_tvalid => fso_tx_axis_tvalid,
            s_axis_tready => fso_tx_axis_tready,
            m_axis_tdata  => fso_tx_fifo_tdata,
            m_axis_tvalid => fso_tx_fifo_tvalid,
            m_axis_tready => fso_tx_fifo_tready
        );

end architecture RTL;
