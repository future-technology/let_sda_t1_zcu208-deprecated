-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 23/09/2022 11:40:33 AM
-- Design Name: 
-- Module Name: arq_axis_mux_1xn
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Mux for axis interface 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library arq_lib;
use arq_lib.arq_support_pkg.all;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;
library share_let_1g;
use share_let_1g.pkg_support.all;

entity arq_axis_mux_1xn is
    generic(
        G_OUTPUT_NUM : natural := 3
    );
    port(
        --Input interfaces
        axis_if_in_m  : in t_axis_if_m;
        axis_if_in_s  : out t_axis_if_s;
        ctrl        : in    std_logic_vector(no_bits_natural(G_OUTPUT_NUM) - 1 downto 0);
        hold        : in    std_logic;
        --Output interfaces
        axis_if_out_m : out axis_if_m_array_t(0 to G_OUTPUT_NUM - 1);
        axis_if_out_s : in axis_if_s_array_t(0 to G_OUTPUT_NUM - 1)
    );
end entity arq_axis_mux_1xn;

architecture RTL of arq_axis_mux_1xn is

begin

    axis_if_in_s.tready <= axis_if_out_s(to_integer(unsigned(ctrl))).tready when hold = '0' else '0';

    process(all)
    begin
        for ii in 0 to G_OUTPUT_NUM - 1 loop
            axis_if_out_m(ii).tvalid <= axis_if_in_m.tvalid when (to_integer(unsigned(ctrl)) = ii and hold = '0') else '0';
            axis_if_out_m(ii).tdata  <= axis_if_in_m.tdata when (to_integer(unsigned(ctrl)) = ii and hold = '0') else (others => '0');
            axis_if_out_m(ii).tlast  <= axis_if_in_m.tlast when (to_integer(unsigned(ctrl)) = ii and hold = '0') else '0';
            axis_if_out_m(ii).tstart <= axis_if_in_m.tstart when (to_integer(unsigned(ctrl)) = ii and hold = '0') else '0';
            axis_if_out_m(ii).tuser  <= '0';
            --axis_if_out(ii).tready <= 'Z';
        end loop;
    end process;

end architecture RTL;
