-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 23/09/2022 11:40:33 AM
-- Design Name: 
-- Module Name: arq_axis_pl_router_1x2 
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Mux for axis interface 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library arq_lib;
use arq_lib.arq_support_pkg.all;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;
library share_let_1g;
use share_let_1g.pkg_support.all;

entity arq_axis_pl_router_1x2 is
    port(
        --Input interfaces
        axis_payload_if_in_m    : in t_axis_payload_if_m;
        axis_payload_if_in_s    : out t_axis_payload_if_s;
        sel                   : in    std_logic;
        --Output interfaces
        axis_payload_if_out_a_m : out t_axis_payload_if_m;
        axis_payload_if_out_a_s : in t_axis_payload_if_s;
        axis_payload_if_out_b_m : out t_axis_payload_if_m;
        axis_payload_if_out_b_s : in t_axis_payload_if_s
    );
end entity arq_axis_pl_router_1x2;

architecture RTL of arq_axis_pl_router_1x2 is

begin

    -- PORT A:
    -------------------------------------------
    axis_payload_if_out_a_m.axis_if_m.tvalid <= axis_payload_if_in_m.axis_if_m.tvalid when sel = '0' else '0';
    axis_payload_if_out_a_m.axis_if_m.tdata  <= axis_payload_if_in_m.axis_if_m.tdata when sel = '0' else (others => '0');
    axis_payload_if_out_a_m.axis_if_m.tlast  <= axis_payload_if_in_m.axis_if_m.tlast when sel = '0' else '0';
    axis_payload_if_out_a_m.axis_if_m.tstart <= axis_payload_if_in_m.axis_if_m.tstart when sel = '0' else '0';
    axis_payload_if_out_a_m.axis_if_m.tuser  <= '0';
    --axis_payload_if_out_a.axis_if.tready <= 'Z';
    axis_payload_if_in_s.axis_if_s.tready    <= axis_payload_if_out_a_s.axis_if_s.tready when sel = '0' else 
                                            axis_payload_if_out_b_s.axis_if_s.tready;

    axis_payload_if_out_a_m.tx_fn      <= axis_payload_if_in_m.tx_fn when sel = '0' else (others => '0');
    axis_payload_if_out_a_m.frame_type <= axis_payload_if_in_m.frame_type when sel = '0' else (others => '0');
    axis_payload_if_out_a_m.tx_num     <= axis_payload_if_in_m.tx_num when sel = '0' else (others => '0');
    --------------------------------------------

    -- PORT B:
    -------------------------------------------
    axis_payload_if_out_b_m.axis_if_m.tvalid <= axis_payload_if_in_m.axis_if_m.tvalid when sel = '1' else '0';
    axis_payload_if_out_b_m.axis_if_m.tdata  <= axis_payload_if_in_m.axis_if_m.tdata when sel = '1' else (others => '0');
    axis_payload_if_out_b_m.axis_if_m.tlast  <= axis_payload_if_in_m.axis_if_m.tlast when sel = '1' else '0';
    axis_payload_if_out_b_m.axis_if_m.tstart <= axis_payload_if_in_m.axis_if_m.tstart when sel = '1' else '0';
    axis_payload_if_out_b_m.axis_if_m.tuser  <= '0';
    --axis_payload_if_out_b.axis_if.tready <= 'Z';

    axis_payload_if_out_b_m.tx_fn      <= axis_payload_if_in_m.tx_fn when sel = '1' else (others => '0');
    axis_payload_if_out_b_m.frame_type <= axis_payload_if_in_m.frame_type when sel = '1' else (others => '0');
    axis_payload_if_out_b_m.tx_num     <= axis_payload_if_in_m.tx_num when sel = '1' else (others => '0');

    -------------------------------------------

end architecture RTL;
