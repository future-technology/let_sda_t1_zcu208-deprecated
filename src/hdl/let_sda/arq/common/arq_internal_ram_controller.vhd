-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 23/09/2022 11:40:33 AM
-- Design Name: 
-- Module Name: arq_internal_ram_controller
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Interface for the ARQ external RAM.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- Internal error indicates that the controller may need to be reset if its FSM is stuck.
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library arq_lib;
library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;
library share_let_1g;
use share_let_1g.pkg_support.all;

entity arq_internal_ram_controller is
    generic(
        G_INTERNAL_MEMORY_FSIZE : natural := 16 -- Size in Frames
    );
    port(
        clk            : in    std_logic;
        rst            : in    std_logic;
        --Input interfaces
        ram_cmd_if_rd_m  : in t_ram_cmd_if_m;
        ram_cmd_if_rd_s  : out t_ram_cmd_if_s;
        ram_cmd_if_wr_m  : in t_ram_cmd_if_m;
        ram_cmd_if_wr_s  : out t_ram_cmd_if_s;
        axis_in_m        : in t_axis_if_m;
        axis_in_s        : out t_axis_if_s;
        --Output interfaces
        axis_out_m       : out t_axis_if_m;
        axis_out_s       : in t_axis_if_s;
        internal_error   : out std_logic
    ); 
end entity arq_internal_ram_controller;

architecture RTL of arq_internal_ram_controller is

    constant C_WATCHDOG_TIMEOUT        : natural := 5;
    constant C_INTERNAL_RAM_WORDS      : natural := (G_INTERNAL_MEMORY_FSIZE * (C_FSO_FRAME_SIZE + 36)) / (C_INTERNAL_RAM_BITWIDTH / 8);
    constant C_INTERNAL_RAM_ADDR_WIDTH : natural := no_bits_natural(C_INTERNAL_RAM_WORDS - 1);

    -- FSM signals:
    type access_fsm_t is (RD_GRANTED, WR_GRANTED);
    signal access_fsm : access_fsm_t;

    -- Defines RAM access operation 
    type op_fsm_rd_t is (SUBOPR_GET_WORD, SUBOPR_SET_WORD, SUBOPR_SET_ADDR, SUBOPR_ADDR_WAIT);
    signal op_fsm_rd : op_fsm_rd_t;

    type op_fsm_wr_t is (SUBOPW_GET_WORD, SUBOPW_SET_WORD);
    signal op_fsm_wr : op_fsm_wr_t;

    type main_fsm_t is (INIT,
                        CHECK_ACCESS_REQ,
                        GRANT_ACCESS,
                        CHECK_CMD,
                        ADDRESS_CONVERSION,
                        ADDRESS_CONVERSION_WAIT,
                        READ_CMD,
                        WRITE_CMD,
                        REGISTER_ADDRESS,
                        RAM_PUSH_FRAME,
                        RAM_PULL_FRAME,
                        WAIT_LAST,
                        ADDR_OVERFLOW
                       );

    signal main_fsm : main_fsm_t;
    --signal r_metadata : metadata_t;

    -- Internal RAM signals:
    constant C_RAM_ADDR_WIDTH : natural := no_bits_natural(C_INTERNAL_RAM_WORDS - 1);
    signal ram_wr_ena         : std_logic;
    signal ram_ena_A          : std_logic;
    signal ram_addr_A         : std_logic_vector(C_RAM_ADDR_WIDTH - 1 downto 0);
    signal ram_data_A         : std_logic_vector(C_INTERNAL_RAM_BITWIDTH - 1 downto 0);
    signal ram_ena_B          : std_logic;
    signal ram_addr_B         : std_logic_vector(C_RAM_ADDR_WIDTH - 1 downto 0);
    signal ram_data_B         : std_logic_vector(C_INTERNAL_RAM_BITWIDTH - 1 downto 0);

    -- Generic signals:
    signal r_addr     : std_logic_vector(C_ADDRESS_WIDTH - 1 downto 0);
    signal r_ram_addr : std_logic_vector(C_INTERNAL_RAM_ADDR_WIDTH_MAX - 1 downto 0);

    -- FIFO buffers signals:
    signal int_axis_rd_m : t_axis_if_m;
    signal int_axis_rd_s : t_axis_if_s;
    signal int_axis_wr_m : t_axis_if_m;
    signal int_axis_wr_s : t_axis_if_s;

    -- ROM address translator
    signal rom_addr_in  : std_logic_vector(C_ADDRESS_WIDTH - 1 downto 0);
    signal rom_addr_ena : std_logic;
    signal ram_addr_out : std_logic_vector(C_INTERNAL_RAM_ADDR_WIDTH_MAX - 1 downto 0);

    --frame byte counter:
    constant C_SUB_CNT_MAX   : natural := 8;
    constant C_FRAME_CNT_MAX : natural := 132; -- 64bit words, last 4 bytes are not used.
    signal sub_cnt8          : natural; -- 8 byte counter for 64bit words
    signal frame_cnt         : natural;

    --data buffer:
    signal r_data   : std_logic_vector(8 - 1 downto 0);
    signal r_data64 : std_logic_vector(64 - 1 downto 0);

begin



    -------------------------------------------
    --- FSM
    -------------------------------------------
    process(clk) is
        variable v_timer : natural := 0;
    begin
        if rising_edge(clk) then
            if rst = '1' then
                ram_cmd_if_rd_s    <= C_RAM_CMD_IF_SLAVE_INIT;
                ram_cmd_if_wr_s    <= C_RAM_CMD_IF_SLAVE_INIT;
                main_fsm           <= INIT;
                op_fsm_wr          <= SUBOPW_GET_WORD;
                op_fsm_rd          <= SUBOPR_SET_WORD;
                access_fsm         <= RD_GRANTED;
                rom_addr_ena       <= '0';
                v_timer            := 0;
                frame_cnt          <= 0;
                sub_cnt8           <= 0;
                internal_error     <= '0';
                int_axis_wr_s.tready <= '0';
                rom_addr_in        <= (others => '0');
                rom_addr_ena       <= '0';
                r_ram_addr         <= (others => '0');
                ram_data_A         <= (others => '0');
                ram_addr_A         <= (others => '0');
                ram_addr_B         <= (others => '0');
                r_addr             <= (others => '0');
                int_axis_rd_m.tdata  <= (others => '0');
                ram_ena_B          <= '0';
                ram_wr_ena         <= '0';
                ram_ena_A          <= '0';
            else
                -- default:
                ram_wr_ena         <= '0';
                ram_ena_A          <= '0';
                ram_ena_B          <= '0';
                rom_addr_ena       <= '0';
                int_axis_rd_m.tvalid <= '0';
                int_axis_rd_m.tlast  <= '0';
                int_axis_rd_m.tstart <= '0';
                int_axis_wr_s.tready <= '0';
                rom_addr_in        <= (others => '0');
                rom_addr_ena       <= '0';

                case main_fsm is

                    when INIT =>
                        ram_cmd_if_rd_s.granted <= '0';
                        ram_cmd_if_wr_s.granted <= '0';
                        ram_cmd_if_rd_s.busy    <= '0';
                        ram_cmd_if_wr_s.busy    <= '0';
                        main_fsm              <= CHECK_ACCESS_REQ;
                        frame_cnt             <= 0;
                        sub_cnt8              <= 0;

                    when CHECK_ACCESS_REQ =>
                        main_fsm <= CHECK_ACCESS_REQ;
                        -- If the previous access was Read, Write request has priority and viceversa.
                        -- if access_fsm = RD_GRANTED then
                        --     if ram_cmd_if_wr.request = '1' then
                        --         access_fsm <= WR_GRANTED;
                        --         main_fsm   <= GRANT_ACCESS;
                        --     elsif ram_cmd_if_rd.request = '1' then
                        --         access_fsm <= RD_GRANTED;
                        --         main_fsm   <= GRANT_ACCESS;
                        --     end if;
                        -- else
                        --Give CACHE priority to create back pressure in insertion:
                            if ram_cmd_if_rd_m.request = '1' then
                                access_fsm <= RD_GRANTED;
                                main_fsm   <= GRANT_ACCESS;
                            elsif ram_cmd_if_wr_m.request = '1' then
                                access_fsm <= WR_GRANTED;
                                main_fsm   <= GRANT_ACCESS;
                            end if;
                        -- end if;

                    when GRANT_ACCESS =>
                        if access_fsm = RD_GRANTED then
                            ram_cmd_if_rd_s.granted <= '1';
                            ram_cmd_if_wr_s.granted <= '0';
                        else
                            ram_cmd_if_rd_s.granted <= '0';
                            ram_cmd_if_wr_s.granted <= '1';
                        end if;
                        main_fsm <= READ_CMD;
                        v_timer  := 0;

                    -- Buffer needs to be ready for both operations when the command is sent.
                    when READ_CMD =>
                        if access_fsm = RD_GRANTED then
                            if ram_cmd_if_rd_m.cmd_valid = '1' and int_axis_rd_s.tready = '1' then
                                r_addr             <= ram_cmd_if_rd_m.addr;
                                main_fsm           <= ADDRESS_CONVERSION;
                                ram_cmd_if_rd_s.busy <= '1';
                                ram_cmd_if_wr_s.busy <= '1';
                            end if;
                        else
                            -- TODO: Hold axis until cmd valid is received??? . 
                            if ram_cmd_if_wr_m.cmd_valid = '1' then --and int_axis_wr_m.tvalid = '1' and int_axis_wr_m.tstart = '1' then
                                r_addr             <= ram_cmd_if_wr_m.addr;
                                main_fsm           <= ADDRESS_CONVERSION;
                                ram_cmd_if_wr_s.busy <= '1';
                                ram_cmd_if_rd_s.busy <= '1';
                            end if;
                        end if;
                        -- watchdog:
                        if v_timer = C_WATCHDOG_TIMEOUT then
                            v_timer  := 0;
                            -- TIMEOUT!: THis may indicate an error.
                            main_fsm <= INIT;
                        else
                            v_timer := v_timer + 1;
                        end if;

                    when ADDRESS_CONVERSION =>
                        if unsigned(r_addr) < to_unsigned(G_INTERNAL_MEMORY_FSIZE, r_addr'length) then 
                            rom_addr_in  <= r_addr;
                            rom_addr_ena <= '1';
                            main_fsm     <= ADDRESS_CONVERSION_WAIT;
                        else
                            rom_addr_ena <= '0';
                            main_fsm     <= ADDR_OVERFLOW;
                        end if;

                    when ADDR_OVERFLOW =>
                        internal_error <= '1';
                        main_fsm <= INIT; 

                    when ADDRESS_CONVERSION_WAIT =>
                        main_fsm <= REGISTER_ADDRESS;

                    when REGISTER_ADDRESS =>
                        r_ram_addr   <= ram_addr_out;
                        rom_addr_ena <= '0';
                        sub_cnt8     <= 0;
                        frame_cnt    <= 0;
                        r_data64     <= (others => '0');

                        if access_fsm = RD_GRANTED then
                            op_fsm_rd <= SUBOPR_SET_ADDR;
                            main_fsm  <= RAM_PULL_FRAME;
                        else
                            op_fsm_wr <= SUBOPW_GET_WORD;
                            main_fsm  <= RAM_PUSH_FRAME;
                        end if;

                    -- WR Operation in RAM
                    when RAM_PUSH_FRAME =>
                        --Default:
                        int_axis_wr_s.tready <= '0';
                        main_fsm           <= RAM_PUSH_FRAME;

                        case op_fsm_wr is
                            when SUBOPW_GET_WORD =>
                                if int_axis_wr_m.tvalid = '1' then
                                    r_data64           <= r_data64(64 - 8 - 1 downto 0) & int_axis_wr_m.tdata;
                                    sub_cnt8           <= sub_cnt8 + 1;
                                    int_axis_wr_s.tready <= '1';
                                    if sub_cnt8 = C_SUB_CNT_MAX and frame_cnt /= C_FRAME_CNT_MAX - 1 then
                                        op_fsm_wr          <= SUBOPW_SET_WORD;
                                        sub_cnt8           <= 0;
                                        int_axis_wr_s.tready <= '0';
                                    elsif sub_cnt8 = (C_SUB_CNT_MAX / 2) and frame_cnt = C_FRAME_CNT_MAX - 1 then
                                        op_fsm_wr          <= SUBOPW_SET_WORD;
                                        sub_cnt8           <= 0;
                                        int_axis_wr_s.tready <= '0';
                                        --Check if last is asserted:
                                        if int_axis_wr_m.tlast /= '1' then
                                            --Error detected:
                                            internal_error <= '1';
                                        end if;
                                    else
                                        op_fsm_wr <= SUBOPW_GET_WORD;
                                    end if;

                                else
                                    op_fsm_wr <= SUBOPW_GET_WORD;
                                end if;
                                ram_addr_A <= r_ram_addr(C_RAM_ADDR_WIDTH - 1 downto 0);

                            when SUBOPW_SET_WORD =>
                                ram_wr_ena <= '1';
                                ram_ena_A  <= '1';
                                op_fsm_wr  <= SUBOPW_GET_WORD;
                                r_ram_addr <= std_logic_vector(unsigned(r_ram_addr) + 1);

                                if frame_cnt = C_FRAME_CNT_MAX - 1 then
                                    -- Last word is only 4 Bytes
                                    ram_data_A         <= x"0000_0000" & r_data64(31 downto 0);
                                    -- Finished Push:
                                    main_fsm           <= INIT;
                                    op_fsm_wr          <= SUBOPW_GET_WORD;
                                    frame_cnt          <= 0;
                                    ram_cmd_if_wr_s.busy <= '0';
                                else
                                    ram_data_A <= r_data64;
                                    frame_cnt  <= frame_cnt + 1;
                                end if;

                        end case;

                    -- RD Operation in RAM
                    when RAM_PULL_FRAME =>

                        --Default:
                        --int_axis_rd.tready  <= '0';
                        main_fsm <= RAM_PULL_FRAME;

                        case op_fsm_rd is

                            when SUBOPR_SET_ADDR =>
                                --ram_addr_B  <= r_ram_addr;
                                ram_addr_B <= r_ram_addr(C_RAM_ADDR_WIDTH - 1 downto 0);
                                ram_ena_B  <= '1';
                                op_fsm_rd  <= SUBOPR_ADDR_WAIT;

                            when SUBOPR_ADDR_WAIT =>
                                op_fsm_rd <= SUBOPR_GET_WORD;

                            when SUBOPR_GET_WORD =>

                                op_fsm_rd  <= SUBOPR_SET_WORD;
                                r_ram_addr <= std_logic_vector(unsigned(r_ram_addr) + 1);

                                if frame_cnt = C_FRAME_CNT_MAX - 1 then
                                    -- Last word is only 4 Bytes
                                    r_data64 <= ram_data_B(32 - 1 downto 0) & x"0000_0000";
                                else
                                    r_data64 <= ram_data_B;
                                end if;

                            when SUBOPR_SET_WORD =>
                                if int_axis_rd_s.tready = '1' then
                                    --Shift 8 bits 
                                    r_data64           <= r_data64(64 - 8 - 1 downto 0) & x"00";
                                    --Set data into bus
                                    int_axis_rd_m.tdata  <= r_data64(64 - 1 downto 64 - 8);
                                    int_axis_rd_m.tvalid <= '1';
                                    sub_cnt8           <= sub_cnt8 + 1;
                                    --End of word, but not end of frame
                                    if sub_cnt8 = C_SUB_CNT_MAX and frame_cnt /= C_FRAME_CNT_MAX - 1 then
                                        op_fsm_rd          <= SUBOPR_SET_ADDR;
                                        sub_cnt8           <= 0;
                                        int_axis_rd_m.tvalid <= '0';

                                        if frame_cnt = C_FRAME_CNT_MAX - 1 then
                                            frame_cnt <= 0;
                                        --main_fsm    <= INIT;
                                        else
                                            frame_cnt <= frame_cnt + 1;
                                        end if;

                                    --End of frame. Half Word
                                    elsif sub_cnt8 = (C_SUB_CNT_MAX / 2) - 1 and frame_cnt = C_FRAME_CNT_MAX - 1 then
                                        op_fsm_rd          <= SUBOPR_SET_WORD;
                                        main_fsm           <= INIT;
                                        sub_cnt8           <= sub_cnt8 + 1;
                                        ram_cmd_if_rd_s.busy <= '0';
                                        int_axis_rd_m.tlast  <= '1';
                                        int_axis_rd_m.tvalid <= '1'; -- ??????????????????????????????????  <=====================
                                    -- Indicate start of frame:
                                    elsif sub_cnt8 = 0 and frame_cnt = 0 then
                                        int_axis_rd_m.tstart <= '1';
                                        op_fsm_rd          <= SUBOPR_SET_WORD;
                                    else
                                        op_fsm_rd <= SUBOPR_SET_WORD;
                                    end if;

                                else
                                    op_fsm_rd          <= SUBOPR_SET_WORD;
                                    int_axis_rd_m.tvalid <= '0';
                                    int_axis_rd_m.tlast  <= '0';
                                    int_axis_rd_m.tstart <= '0';
                                end if;
                        end case;

                    when others =>
                        main_fsm <= INIT;
                end case;
            end if;
        end if;
    end process;

    -------------------------------------------
    --- INTERNAL RAM
    -------------------------------------------
    inst_int_ram : entity share_let_1g.generic_simple_dp_ram
        generic map(
            DISTR_RAM => false,         --: boolean := true;
            WORDS     => C_INTERNAL_RAM_WORDS, --: integer := 5;
            BITWIDTH  => C_INTERNAL_RAM_BITWIDTH --: integer := 1
        )
        port map(
            clk   => clk,               --: in std_logic;
            rst   => rst,               --: in std_logic;   
            wen_A => ram_wr_ena,        --: in  std_logic;
            en_A  => ram_ena_A,         --: in  std_logic;
            a_A   => ram_addr_A,        --: in  std_logic_vector(no_bits_natural(WORDS - 1) - 1 downto 0);
            d_A   => ram_data_A,        --: in  std_logic_vector(BITWIDTH - 1 downto 0 );
            en_B  => ram_ena_B,         --: in  std_logic;
            a_B   => ram_addr_B,        --: in  std_logic_vector(no_bits_natural(WORDS - 1) - 1 downto 0);
            q_B   => ram_data_B         --: out std_logic_vector(BITWIDTH - 1 downto 0)
        );

    -------------------------------------------
    --- FIFO BUFFERS INPUT AND OUTPUT
    -------------------------------------------
    Inst_input_buffer : entity share_let_1g.axi4s_fifo_v2
        generic map(
            DISTR_RAM         => false,
            FIFO_DEPTH        => C_FSO_PAYLOAD_SIZE,
            DATA_WIDTH        => 8,
            FULL_THRESHOLD    => C_FSO_PAYLOAD_SIZE,
            EMPTY_THRESHOLD   => 0,
            USE_OUTPUT_BUFFER => false
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => axis_in_m.tdata,
            input_valid        => axis_in_m.tvalid,
            input_last         => axis_in_m.tlast,
            input_ready        => axis_in_s.tready,
            input_user         => '0',
            input_start        => axis_in_m.tstart,
            input_almost_full  => open,
            input_almost_empty => open,
            fifo_max_level     => open,
            -- Output data handling
            -----------------------
            output             => int_axis_wr_m.tdata,
            output_valid       => int_axis_wr_m.tvalid,
            output_last        => int_axis_wr_m.tlast,
            output_ready       => int_axis_wr_s.tready,
            output_user        => open,
            output_start       => int_axis_wr_m.tstart
        );

    Inst_output_buffer : entity share_let_1g.axi4s_fifo_v2
        generic map(
            DISTR_RAM         => false,
            FIFO_DEPTH        => C_FSO_PAYLOAD_SIZE,
            DATA_WIDTH        => 8,
            FULL_THRESHOLD    => C_FSO_PAYLOAD_SIZE,
            EMPTY_THRESHOLD   => 0,
            USE_OUTPUT_BUFFER => false
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => int_axis_rd_m.tdata,
            input_valid        => int_axis_rd_m.tvalid,
            input_last         => int_axis_rd_m.tlast,
            input_ready        => int_axis_rd_s.tready,
            input_user         => '0',
            input_start        => int_axis_rd_m.tstart,
            input_almost_full  => open,
            input_almost_empty => open,
            fifo_max_level     => open,
            -- Output data handling
            -----------------------
            output             => axis_out_m.tdata,
            output_valid       => axis_out_m.tvalid,
            output_last        => axis_out_m.tlast,
            output_ready       => axis_out_s.tready,
            output_user        => axis_out_m.tuser,
            output_start       => axis_out_m.tstart
        );

    -------------------------------------------
    --- RAM ADDRESS TRANSLATOR
    -------------------------------------------
    inst_arq_rom_addr_translator : entity arq_lib.arq_rom_addr_translator
        port map(
            clk      => clk,            --: in std_logic;

            --Input interfaces
            addr     => rom_addr_in,    --: in std_logic_vector(C_ADDRESS_WIDTH-1 downto 0);
            enable   => rom_addr_ena,   --: in std_logic;

            --Output interfaces
            ram_addr => ram_addr_out    --: out std_logic_vector(C_INTERNAL_RAM_ADDR_WIDTH_MAX-1 downto 0)
        );

end architecture RTL;
