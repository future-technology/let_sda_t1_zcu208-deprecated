-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 23/09/2022 11:40:33 AM
-- Design Name: 
-- Module Name: arq_ram_controller_top
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Interface for the ARQ external RAM. It may have internal RAM to
-- make simulation and tests easier.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library arq_lib;
use arq_lib.arq_support_pkg.all;
library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;
library share_let_1g;
use share_let_1g.pkg_support.all;
use ieee.std_logic_misc.all;

entity arq_ram_controller_top is
    generic(
        --G_INTERNAL_RAM : boolean := true;
        G_INTERNAL_MEMORY_SIZE : natural := 16; -- Size in Frames
        G_CACHE_SIZE           : natural := 16
    );
    port(
        clk                 : in    std_logic;
        rst                 : in    std_logic;
        --Input interfaces
        req_insertion_if_m    : in t_request_insertion_ram_if_m;
        req_insertion_if_s    : out t_request_insertion_ram_if_s;
        req_resend_if_m       : in t_request_resend_if_m;
        req_resend_if_s       : out t_request_resend_if_s;
        req_cache_if_m        : in t_request_cache_if_m;
        req_cache_if_s        : out t_request_cache_if_s;
        axis_payload_if_in_m  : in t_axis_payload_if_m;
        axis_payload_if_in_s  : out t_axis_payload_if_s;
        insert_fifo_empty   : out   std_logic;
        arq_config_if        : in t_arq_config_if;
        -- cache_insertion_fifo_empty : out std_logic;        
        --Output interfaces
        axis_payload_if_out_m : out t_axis_payload_if_m;
        axis_payload_if_out_s : in t_axis_payload_if_s;
        clear_diag          : in std_logic;
        diag_if             : out t_arq_ram_controller_diag_if --arq_ram_diagnostics_if : inout t_arq_control_tx_diagnostics_if
    );
end entity arq_ram_controller_top;

architecture RTL of arq_ram_controller_top is
    constant C_INTERNAL_MEMORY_FSIZE : natural := 64;
    constant C_MEMORY_TOP_ADDR       : natural := G_INTERNAL_MEMORY_SIZE-1;
    constant C_INTERNAL_RAM_WORDS      : natural := (G_INTERNAL_MEMORY_SIZE * (C_FSO_FRAME_SIZE + 36)) / C_INTERNAL_RAM_BITWIDTH;
    constant C_INTERNAL_RAM_ADDR_WIDTH : natural := no_bits_natural(C_INTERNAL_RAM_WORDS - 1);

    constant C_ROUTER_BYPASS_ENABLE  : std_logic        := '1';
    constant C_ROUTER_BYPASS_DISABLE : std_logic        := '0';
    
    type insert_metadata_t is record
        tx_fn : std_logic_vector(C_TX_FN_WIDTH - 1 downto 0);
        addr  : std_logic_vector(C_ADDRESS_WIDTH - 1 downto 0); -- ?
    end record;

    type resend_metadata_t is record
        tx_fn      : std_logic_vector(C_TX_FN_WIDTH - 1 downto 0);
        addr       : std_logic_vector(C_ADDRESS_WIDTH - 1 downto 0); -- ?
        frame_type : std_logic_vector(1 downto 0);
        tx_num     : std_logic_vector(C_TX_NUM_WIDTH - 1 downto 0);
        valid      : std_logic;
    end record;

    constant C_RESEND_METADATA_INIT : resend_metadata_t := (
        tx_fn   => (others => '0'),
        addr    => (others => '0'),
        frame_type => (others=>'0'),
        tx_num  => (others=>'0'),
        valid   => '0'
    );
    
    -- FSM signals:
    type insertion_fsm_t is (INIT,
                             CHECK_INSERTION_REQ,
                             EXECUTE_CMD_WR,
                             RAM_ACCESS_REQ,
                             WAIT_FOR_GRANTED,
                             INSERTION_ERROR,
                             WAIT_FINISH_OP,
                             PUSH_FRAME,
                             WAIT_LAST,
                             INSERTION_ABORT);

    type cache_fsm_t is (CACHE_START,
                          RESET_CACHE, 
                          CHECK_REQ_CACHE,
                          CACHE_HIT_PULL,
                          FIND_FREE_CACHE,
                          CMD_RD_FOR_CACHE,
                          WAIT_CACHE_LOAD,
                          CACHE_LOAD_WAIT_LAST,
                          CACHE_ERROR
                         );

    type resend_fsm_t is (RESEND_START,
                          CHECK_REQ_RESEND,
                          FIND_IN_CACHE,
                          CACHE_HIT_PUSH,
                          CACHE_MISS,
                          PULL_FROM_CACHE,
                          RESEND_ERROR
                         );

    signal insertion_fsm : insertion_fsm_t;
    signal cache_fsm    : cache_fsm_t;
    signal resend_fsm    : resend_fsm_t;
    signal r_req_resend  : resend_metadata_t;
    signal r_req_insert  : insert_metadata_t;
    signal r_req_cache   : resend_metadata_t;

    --     -- Cache signals:
    constant C_SEARCH_RETRAY_MAX : natural := 5;
    constant C_RESEND_RETRAY_MAX : natural := 230;
    constant C_CACHE_SIZE : unsigned(no_bits_natural(G_CACHE_SIZE) - 1 downto 0) := to_unsigned(G_CACHE_SIZE, no_bits_natural(G_CACHE_SIZE));
    type cache_metadata_array_t is array (0 to G_CACHE_SIZE - 1) of resend_metadata_t;
    signal cache_metadata_array   : cache_metadata_array_t;
    signal cache_it_cnt           : unsigned(no_bits_natural(G_CACHE_SIZE) - 1 downto 0);
    signal resend_it_cnt           : unsigned(no_bits_natural(G_CACHE_SIZE) - 1 downto 0);
    signal resend_retry_cnt        : unsigned(8 - 1 downto 0);
    signal search_retry_cnt        : unsigned(8 - 1 downto 0);
    signal cache_empty_flag_array : std_logic_vector(G_CACHE_SIZE - 1 downto 0);
    -- Mux signals: 
    --signal axis_payload_if_cache_array_in : axis_payload_if_array_t(0 to G_CACHE_SIZE-1); --Cache + fifo 
    --constant C_RAM_MUX_INDEX      : natural := G_CACHE_SIZE; -- RAM Mux index is the last one 
    signal mux_hold_in            : std_logic;
    signal mux_hold_out           : std_logic;
    signal mux_ctrl_in            : std_logic_vector(no_bits_natural(G_CACHE_SIZE) - 1 downto 0);
    signal mux_ctrl_out           : std_logic_vector(no_bits_natural(G_CACHE_SIZE) - 1 downto 0);
    signal axis_if_array_in_m       : axis_if_m_array_t(0 to G_CACHE_SIZE-1);
    signal axis_if_array_in_s       : axis_if_s_array_t(0 to G_CACHE_SIZE-1);
    signal axis_if_mux_in_m         : t_axis_if_m;
    signal axis_if_mux_in_s         : t_axis_if_s;
    signal axis_if_array_mux_out_m  : axis_if_m_array_t(0 to G_CACHE_SIZE-1);
    signal axis_if_array_mux_out_s  : axis_if_s_array_t(0 to G_CACHE_SIZE-1);

    -- RAM controller signals:
    signal int_axis_in_m   : t_axis_payload_if_m;
    signal int_axis_in_s   : t_axis_payload_if_s;
    signal int_axis_out_m  : t_axis_if_m;
    signal int_axis_out_s  : t_axis_if_s;
    signal int_ram_error : std_logic;
    signal sel_router_wr : std_logic;
    -- RAM command interfaces:
    signal ram_cmd_if_rd_m : t_ram_cmd_if_m;
    signal ram_cmd_if_rd_s : t_ram_cmd_if_s;
    signal ram_cmd_if_wr_m : t_ram_cmd_if_m;
    signal ram_cmd_if_wr_s : t_ram_cmd_if_s;

    signal r_resend_fifo_empty      : std_logic;
    signal r_insert_fifo_empty      : std_logic;
    signal insertion_fifo_reset_int : std_logic;
    signal insertion_fifo_reset_mux : std_logic;
    signal resend_fifo_reset_int    : std_logic;
    signal resend_fifo_reset_mux    : std_logic;
    --buffer signals:
    signal axis_payload_if_buf_in_m   : t_axis_payload_if_m;
    signal axis_payload_if_buf_in_s   : t_axis_payload_if_s;
    signal axis_payload_if_buf_out_m  : t_axis_payload_if_m;
    signal axis_payload_if_buf_out_s  : t_axis_payload_if_s;
    signal axis_if_ground_m         : t_axis_payload_if_m;
    signal axis_if_ground_s         : t_axis_payload_if_s;
   
    -- FIFO auto reset on timeout:
    signal reset_clk_cnt : natural := 0; 
    signal reset_cache_mux  : std_logic_vector(G_CACHE_SIZE-1 downto 0) := (others=>'0'); 
    signal reset_cache_array : std_logic_vector(G_CACHE_SIZE-1 downto 0) := (others=>'0'); 
    type t_r_cache_tx_fn_array is array (0 to G_CACHE_SIZE - 1) of std_logic_vector(16-1 downto 0);
    signal r_cache_tx_fn_array : t_r_cache_tx_fn_array := (others=>(others=>'0'));
    type t_cache_counter_array is array (0 to G_CACHE_SIZE-1) of natural;
    signal cache_counter_array : t_cache_counter_array;
    signal cache_req_reset_array : std_logic_vector(G_CACHE_SIZE-1 downto 0) := (others=>'0');
    signal cache_release_reset_array : std_logic_vector(G_CACHE_SIZE-1 downto 0) := (others=>'0');
    signal core_tick : std_logic := '0';
    signal core_counter : natural := 0;
    signal time_counter : natural := 0;
begin

    ------------------------------------------------------
    -- ASYNC CONNECTIONS
    ------------------------------------------------------
    insert_fifo_empty                     <= r_insert_fifo_empty;
    insertion_fifo_reset_mux              <= rst or insertion_fifo_reset_int;
    resend_fifo_reset_mux                 <= rst or resend_fifo_reset_int;
    diag_if.ram_error                     <= int_ram_error;
    diag_if.address_error                 <= (others => '0');
    --input buffer:
    -- int_axis_in.tdata                     <= axis_payload_if_buf_in.axis_if.tdata;
    -- int_axis_in.tvalid                    <= axis_payload_if_buf_in.axis_if.tvalid;
    -- int_axis_in.tlast                     <= axis_payload_if_buf_in.axis_if.tlast;
    -- int_axis_in.tstart                    <= axis_payload_if_buf_in.axis_if.tstart;
    -- int_axis_in.tuser                     <= '0';
    -- axis_payload_if_buf_in.axis_if.tready <= int_axis_in.tready;

    axis_if_ground_s <= C_AXIS_PAYLOAD_IF_SLAVE_INIT;

    --output buffer
    -- axis_payload_if_mux_in.axis_if.tdata <= int_axis_out.tdata; 
    -- axis_payload_if_mux_in.axis_if.tvalid <= int_axis_out.tvalid; 
    -- axis_payload_if_mux_in.axis_if.tlast <= int_axis_out.tlast; 
    -- axis_payload_if_mux_in.axis_if.tstart <= int_axis_out.tstart; 
    -- axis_payload_if_mux_in.axis_if.tuser <= int_axis_out.tuser; 
    -- int_axis_out.tready <= axis_payload_if_mux_in.axis_if.tready;

    ------------------------------------------------------
    -- PROCESS INSERTION FSM
    ------------------------------------------------------

    process(clk) is
        variable v_rst_cnt : natural := 0;
    begin
        if rising_edge(clk) then
            if rst = '1' then
                v_rst_cnt                     := 0;
                insertion_fsm                 <= INIT;
                ram_cmd_if_wr_m.request         <= '0';
                ram_cmd_if_wr_m.cmd_valid       <= '0';
                ram_cmd_if_wr_m.addr            <= (others => '0');
                insertion_fifo_reset_int      <= '0';
                req_insertion_if_s.ready        <= '0';
                req_insertion_if_s.busy         <= '0';
                diag_if.insertion_req_counter <= (others => '0');
                sel_router_wr                 <= C_ROUTER_BYPASS_DISABLE;
                diag_if.insertion_error       <= (others => '0');
                diag_if.insertion_abort       <= (others => '0');
            else
                --default:
                ram_cmd_if_wr_m.request    <= '0';
                ram_cmd_if_wr_m.cmd_valid  <= '0';
                insertion_fifo_reset_int <= '0';
                req_insertion_if_s.ready   <= '0';
                req_insertion_if_s.busy    <= '0';
                sel_router_wr <= C_ROUTER_BYPASS_DISABLE;
                
                case insertion_fsm is
                    when INIT =>
                        -- Reset and default all signals and counters;
                        ram_cmd_if_wr_m.request    <= '0';
                        ram_cmd_if_wr_m.cmd_valid  <= '0';
                        ram_cmd_if_wr_m.addr       <= (others => '0');
                        insertion_fifo_reset_int <= '0';
                        sel_router_wr            <= C_ROUTER_BYPASS_DISABLE;
                        v_rst_cnt := 0;
                        if ram_cmd_if_wr_s.busy = '0' then
                            insertion_fsm            <= CHECK_INSERTION_REQ;
                        end if;

                    when CHECK_INSERTION_REQ =>
                        req_insertion_if_s.ready <= '1'; --TODO: consider other apporach for this logic.
                        if req_insertion_if_m.valid = '1' then -- TODO: check busy signal first. If busy wait and block insertion.
                            insertion_fsm                 <= RAM_ACCESS_REQ;
                            r_req_insert.tx_fn            <= req_insertion_if_m.tx_fn;
                            r_req_insert.addr             <= req_insertion_if_m.addr;
                            req_insertion_if_s.ready        <= '0';
                            req_insertion_if_s.busy         <= '1';
                            diag_if.insertion_req_counter <= diag_if.insertion_req_counter + 1;
                        else
                            -- If no insertion request, do nothing
                            insertion_fsm <= CHECK_INSERTION_REQ;
                        end if;

                    -- Request ram access for insertion and wait until granted:
                    when RAM_ACCESS_REQ =>
                        insertion_fsm         <= RAM_ACCESS_REQ;
                        ram_cmd_if_wr_m.request <= '1';
                        req_insertion_if_s.busy <= '1';
                        if ram_cmd_if_wr_s.granted = '1' then
                            insertion_fsm <= EXECUTE_CMD_WR;
                        end if;
                    -- this state may not be needed if the push is automatic.
                    --when PUSH_FRAME =>
                    when WAIT_LAST =>

                        sel_router_wr <= C_ROUTER_BYPASS_ENABLE;
                        req_insertion_if_s.busy <= '1';
                        
                        if axis_payload_if_buf_in_m.axis_if_m.tvalid = '1' and axis_payload_if_buf_in_m.axis_if_m.tlast = '1' and axis_payload_if_buf_in_s.axis_if_s.tready = '1' then
                            sel_router_wr <= C_ROUTER_BYPASS_DISABLE;
                            -- check fifo is empty.
                            if r_insert_fifo_empty = '1' then
                                insertion_fsm <= WAIT_FINISH_OP;
                            -- if not empty, ERROR.
                            else
                                insertion_fsm <= INSERTION_ERROR;
                            end if;
                        end if;

                    when EXECUTE_CMD_WR =>
                        req_insertion_if_s.busy <= '1';
                        if unsigned(r_req_insert.addr) <= to_unsigned(C_MEMORY_TOP_ADDR, r_req_insert.addr'length) then 
                            ram_cmd_if_wr_m.cmd_valid <= '1';
                            ram_cmd_if_wr_m.addr      <= r_req_insert.addr;
                            insertion_fsm           <= WAIT_LAST;
                        else
                            insertion_fsm <= INSERTION_ABORT; 
                        end if;

                    when INSERTION_ABORT =>
                        --Block fifo output:
                        sel_router_wr <= C_ROUTER_BYPASS_DISABLE;
                        --Wait until fifo is filled:
                        if axis_payload_if_in_m.axis_if_m.tvalid = '1' and axis_payload_if_in_m.axis_if_m.tlast = '1' and axis_payload_if_in_s.axis_if_s.tready = '1' then
                            insertion_fsm <= INSERTION_ERROR;
                            diag_if.insertion_abort <= diag_if.insertion_abort + 1;
                        end if;

                    when WAIT_FINISH_OP =>
                        req_insertion_if_s.busy <= '1';
                        if ram_cmd_if_wr_s.busy = '0' then
                            insertion_fsm <= INIT;
                        else
                            insertion_fsm <= WAIT_FINISH_OP;
                        end if;

                    when INSERTION_ERROR =>
                        insertion_fifo_reset_int <= '1';
                        if v_rst_cnt = 7 then
                            v_rst_cnt := 0;
                            insertion_fsm            <= INIT;
                            diag_if.insertion_error  <= diag_if.insertion_error + 1;
                        else
                            v_rst_cnt := v_rst_cnt + 1;
                        end if;

                    when others =>
                        insertion_fsm <= INSERTION_ERROR;
                end case;

                if clear_diag = '1' then
                    diag_if.insertion_req_counter <= (others => '0');
                    diag_if.insertion_error       <= (others => '0');
                    diag_if.insertion_abort       <= (others => '0');
                end if;

            end if;
        end if;
    end process;

    -------------------------------------------
    --- PROCESS RD RAM & CACHE WR FSM
    -------------------------------------------
    PRCS_CACHE_WR : process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                --default:
                -- req_resend_if.ready            <= '0';
                reset_clk_cnt <= 0; 
                req_cache_if_s.ready             <= '0';
                mux_hold_in                    <= '1';
                -- mux_hold_out                   <= '1';
                mux_ctrl_in                    <= (others => '0');
                -- mux_ctrl_out                   <= (others => '0');
                ram_cmd_if_rd_m.cmd_valid        <= '0';
                search_retry_cnt <= (others=>'0');
                cache_it_cnt                   <= (others => '0');
                cache_fsm                     <= CACHE_START;
                resend_fifo_reset_int          <= '0';
                --axis_payload_if_out.tx_fn      <= (others => '0');
                --axis_payload_if_out.frame_type <= (others => '0');
                --axis_payload_if_out.tx_num     <= (others => '0');
                --diag_if.retx_counter           <= (others => '0');
                diag_if.cache_req_counter      <= (others => '0');
                --diag_if.cache_retx_counter     <= (others => '0');
                diag_if.cache_timeout_counter  <= (others => '0');
                --diag_if.retx_error             <= (others => '0');
                diag_if.cache_error            <= (others => '0');
                for ii in 0 to G_CACHE_SIZE-1 loop
                    cache_metadata_array(ii)   <= C_RESEND_METADATA_INIT;
                end loop;
                cache_release_reset_array <= (others=>'0');
            else
                --default:
                -- req_resend_if.ready     <= '0';
                req_cache_if_s.ready      <= '0';
                mux_hold_in             <= '1';
                -- mux_hold_out            <= '1';
                ram_cmd_if_rd_m.cmd_valid <= '0';
                resend_fifo_reset_int   <= '0';
                cache_release_reset_array <= (others=>'0');

                case cache_fsm is

                    when CACHE_START =>
                        cache_fsm              <= RESET_CACHE ;
                        -- Reset and default all signals and counters;
                        ram_cmd_if_rd_m.request   <= '0';
                        ram_cmd_if_rd_m.cmd_valid <= '0';
                        ram_cmd_if_rd_m.addr      <= (others => '0');
                        cache_it_cnt            <= (others => '0');
                        search_retry_cnt <= (others=>'0');
                        --resend_fifo_reset_int <= '0';                
                        reset_clk_cnt <= 0; 

                    when RESET_CACHE =>
                        reset_clk_cnt <= 0; 
                        cache_release_reset_array <= (others=>'0');
                        -- Check if there is at least one fifo to reset:
                        if or_reduce(cache_req_reset_array) = '1' then
                            --r_cache_req_reset_array <= cache_req_reset_array; 
                            -- Apply reset for 7 clk cycles:
                            if reset_clk_cnt = 7 then
                                -- release reset:
                                reset_cache_array <= (others=>'0');
                                cache_fsm <= CHECK_REQ_CACHE ; 
                                cache_release_reset_array <= cache_req_reset_array;    
                                diag_if.cache_timeout_counter  <= diag_if.cache_timeout_counter + 1;
                            else
                                reset_clk_cnt <= reset_clk_cnt + 1;
                                -- apply reset:
                                reset_cache_array <= cache_req_reset_array;
                            end if;
                        else
                            cache_fsm <= CHECK_REQ_CACHE ;    
                        end if;

                    when CHECK_REQ_CACHE =>
                        --check if ram cmd bus is busy and there is at least one cache free to use:
                        if ram_cmd_if_rd_s.busy = '0' and or_reduce(cache_empty_flag_array) = '1' then
                            req_cache_if_s.ready <= '0';
                            if req_cache_if_m.valid = '1' then
                                cache_fsm         <= FIND_FREE_CACHE;
                                r_req_cache.tx_fn  <= req_cache_if_m.tx_fn;
                                --r_req_resend.tx_num <= req_resend_if.tx_num;
                                --r_req_resend.frame_type <= req_resend_if.frame_type;
                                r_req_cache.addr   <= req_cache_if_m.addr;
                                req_cache_if_s.ready <= '1';
                            else
                                -- If no insertion request, go to check cache_req_if:
                                 if or_reduce(cache_req_reset_array) = '1' then
                                    cache_fsm <= RESET_CACHE ;
                                else
                                    cache_fsm <= CHECK_REQ_CACHE;
                                end if;                               
                            end if;
                        else

                            if or_reduce(cache_req_reset_array) = '1' then
                                cache_fsm <= RESET_CACHE ;
                            else
                                cache_fsm <= CHECK_REQ_CACHE;
                            end if;
                        end if;

                    -- Request to cache one frame, find free cache buffer
                    when FIND_FREE_CACHE =>
                        -- iterate over empty signals of cache buffer:
                        cache_fsm <= FIND_FREE_CACHE;
                        if cache_it_cnt = C_CACHE_SIZE then
                            cache_it_cnt <= (others=>'0');
                            -- Cache MISS, pull frame from RAM:
                            -- cache_fsm <= RAM_PULL_REQ; --TODO: rethink this
                             if search_retry_cnt = to_unsigned(C_RESEND_RETRAY_MAX, search_retry_cnt'length) then
                                search_retry_cnt <= (others=>'0');
                                cache_fsm   <= CACHE_START;
                            else
                                search_retry_cnt <= search_retry_cnt + 1;
                                cache_fsm   <= FIND_FREE_CACHE;
                            end if;                           
                        else
                            --check empty flags:
                            if cache_empty_flag_array(to_integer(cache_it_cnt)) = '1' then
                                -- CACHE HIT!
                                cache_fsm <= CACHE_HIT_PULL;
                            else
                                cache_it_cnt <= cache_it_cnt + 1;
                            end if;
                        end if;

                    -- Free cache buffer found! begin to pull frame from ram:
                    when CACHE_HIT_PULL =>
                        mux_ctrl_in           <= std_logic_vector(cache_it_cnt); --std_logic_vector(to_unsigned(cache_it_cnt, mux_ctrl_in'length));
                        mux_hold_in           <= '1';
                        -- send pull commands
                        ram_cmd_if_rd_m.request <= '1';
                        if ram_cmd_if_rd_s.granted = '1' then
                            --access granted:
                            cache_fsm <= CMD_RD_FOR_CACHE;
                        end if;
                    --TODO: timeout?

                    when CMD_RD_FOR_CACHE =>
                        -- Addr safety check:
                        if unsigned(r_req_cache.addr) <= to_unsigned(C_MEMORY_TOP_ADDR, r_req_cache.addr'length) then 
                            ram_cmd_if_rd_m.request   <= '0';
                            ram_cmd_if_rd_m.cmd_valid <= '1';
                            ram_cmd_if_rd_m.addr      <= r_req_cache.addr; 
                            cache_fsm               <= CACHE_LOAD_WAIT_LAST;
                        else
                        -- If safety check not passed, drop request:
                            cache_fsm <= CACHE_START;
                        end if;

                    when WAIT_CACHE_LOAD =>
                        -- release mux:
                        mux_hold_in  <= '0';
                        --mux_hold_out <= '1';
                        -- wait until operation is finished:
                        if ram_cmd_if_rd_s.busy = '0' then
                            cache_fsm <= CACHE_START;
                        --else
                            --cache_fsm <= WAIT_CMD_RD_FINISH;
                        end if;

                    when CACHE_LOAD_WAIT_LAST =>
                        -- release mux:
                        mux_hold_in  <= '0';
                        if int_axis_out_m.tvalid = '1' and int_axis_out_m.tlast = '1' and int_axis_out_s.tready = '1' then
                            -- check fifo is empty.
                            if cache_empty_flag_array(to_integer(cache_it_cnt)) = '0' then
                                cache_fsm                               <= WAIT_CACHE_LOAD;
                                --insert metadata:
                                cache_metadata_array(to_integer(cache_it_cnt)).tx_fn <= r_req_cache.tx_fn;
                                cache_metadata_array(to_integer(cache_it_cnt)).addr  <= r_req_cache.addr;
                                diag_if.cache_req_counter                <= diag_if.cache_req_counter + 1;
                            -- if not empty, ERROR.
                            else
                                cache_fsm <= CACHE_ERROR;
                            end if;
                        end if;


                     when CACHE_ERROR =>
                        cache_fsm          <= CACHE_START;
                        diag_if.cache_error <= diag_if.cache_error + 1;


                        
                    --when CHECK_REQ_RESEND =>
                    --    --THis logic is different as there is not intermediate FIFO:
                    --    --req_resend_if.ready <= '1';   -- TODO: Rethink this!!
                    --    --if req_resend_if.pending_request_n = '0' then 
                    --    if req_resend_if.valid = '1' then --and req_resend_if.ready = '1' then
                    --            req_resend_if.ready     <= '1';

                    --            if arq_config_if.cache_enable = '1' then
                    --                resend_fsm <= FIND_IN_CACHE;
                    --            else
                    --                resend_fsm <= RAM_PULL_REQ;
                    --            end if;
                    --            r_req_resend.tx_fn      <= req_resend_if.tx_fn;
                    --            r_req_resend.tx_num     <= req_resend_if.tx_num;
                    --            r_req_resend.frame_type <= req_resend_if.frame_type;
                    --            r_req_resend.addr       <= req_resend_if.addr;
                    --    --    end if;
                    --    -- elsif req_resend_if.valid = '1' and req_resend_if.ready = '0' then
                    --    --     resend_fsm <= CHECK_REQ;
                    --    else
                    --        -- If no insertion request, go to check cache_req_if:
                    --        resend_fsm <= CHECK_REQ_CACHE ;
                    --    end if;



                    --when FIND_IN_CACHE =>
                    --    -- Iterate over cache metadata records to find TX_FN
                    --    if cache_it_cnt = G_CACHE_SIZE then
                    --        cache_it_cnt <= 0;
                    --        -- Cache MISS, pull frame from RAM:
                    --        resend_fsm   <= RAM_PULL_REQ;

                    --    else
                    --        -- check cache tx_fn matches and it is a valid entry:
                    --        if cache_metadata_array(cache_it_cnt).tx_fn = r_req_resend.tx_fn and cache_empty_flag_array(cache_it_cnt) = '0' then
                    --            -- CACHE HIT!
                    --            resend_fsm <= CACHE_HIT_PUSH;
                    --        else
                    --            cache_it_cnt <= cache_it_cnt + 1;
                    --        end if;
                    --    end if;
                    ----resend_fsm <= CHECK_RESEND_REQ;

                    --when CACHE_HIT_PUSH =>
                    --    -- Set muxes to the cache buffer hit
                    --    mux_ctrl_in  <= std_logic_vector(to_unsigned(cache_it_cnt, mux_ctrl_in'length));
                    --    mux_ctrl_out <= std_logic_vector(to_unsigned(cache_it_cnt, mux_ctrl_out'length));
                    --    mux_hold_in  <= '1';
                    --    mux_hold_out <= '1';
                    --    resend_fsm   <= PULL_FROM_CACHE;

                    ---- 
                    --when RAM_PULL_REQ =>
                    --    -- set muxes to use RAM path.
                    --    mux_ctrl_in           <= std_logic_vector(to_unsigned(C_RAM_MUX_INDEX, mux_ctrl_in'length));
                    --    mux_ctrl_out          <= std_logic_vector(to_unsigned(C_RAM_MUX_INDEX, mux_ctrl_out'length));
                    --    mux_hold_in           <= '1';
                    --    mux_hold_out          <= '1';
                    --    -- send pull commands
                    --    ram_cmd_if_rd.request <= '1';
                    --    if ram_cmd_if_rd.granted = '1' then
                    --        --access granted:
                    --        resend_fsm <= EXECUTE_CMD_RD;
                    --    end if;
                    --TODO: timeout?

                    -- when EXECUTE_CMD_RD =>
                    --     ram_cmd_if_rd.request          <= '0';
                    --     ram_cmd_if_rd.cmd_valid        <= '1';
                    --     ram_cmd_if_rd.addr             <= r_req_resend.addr;
                    --     resend_fsm                     <= RESEND_WAIT_LAST;
                    --     -- set metadata: 
                    --     axis_payload_if_out.tx_fn      <= r_req_resend.tx_fn;
                    --     axis_payload_if_out.frame_type <= r_req_resend.frame_type;
                    --     axis_payload_if_out.tx_num     <= r_req_resend.tx_num;

                    -- when WAIT_CMD_RD_FINISH =>
                    --     mux_hold_in  <= '0';
                    --     mux_hold_out <= '0';
                    --     -- wait until operation is finished:
                    --     if ram_cmd_if_rd.busy = '0' then
                    --         resend_fsm           <= START;
                    --         diag_if.retx_counter <= diag_if.retx_counter + 1;
                    --     else
                    --         resend_fsm <= WAIT_CMD_RD_FINISH;
                    --     end if;

                    -- when RESEND_WAIT_LAST =>
                    --     -- release mux:
                    --     mux_hold_in  <= '0';
                    --     mux_hold_out <= '0';
                    --     if axis_payload_if_out.axis_if.tvalid = '1' and axis_payload_if_out.axis_if.tlast = '1' then
                    --         -- check fifo is empty.
                    --         if r_resend_fifo_empty = '1' then
                    --             resend_fsm <= WAIT_CMD_RD_FINISH;
                    --         -- if not empty, ERROR.
                    --         else
                    --             resend_fsm <= RESEND_ERROR;
                    --         end if;
                    --     end if;

                    -- when PULL_FROM_RAM =>
                    --     -- Release hold on MUXs:
                    --     mux_hold <= '0';
                    --     ram_cmd_if_rd.request   <= '1'; 
                    --     ram_cmd_if_rd.cmd_valid <= '0';
                    --     ram_cmd_if_rd.addr      <= (others=>'0');

                    -- Resend request is in cache. Pulls frame until it is finished
                    -- when PULL_FROM_CACHE =>
                    --     -- set hold on MUXs:
                    --     mux_hold_in                    <= '1';
                    --     mux_hold_out                   <= '0';
                    --     -- set metadata: 
                    --     axis_payload_if_out.frame_type <= r_req_resend.frame_type;
                    --     axis_payload_if_out.tx_num     <= r_req_resend.tx_num;
                    --     axis_payload_if_out.tx_fn      <= cache_metadata_array(cache_it_cnt).tx_fn;
                    --     -- axis_payload_if_out.frame_type <= cache_metadata_array(cache_it_cnt).frame_type;     
                    --     -- axis_payload_if_out.tx_num <= cache_metadata_array(cache_it_cnt).tx_num;     

                    --     if axis_payload_if_out.axis_if.tvalid = '1' and axis_payload_if_out.axis_if.tlast = '1' then
                    --         -- check fifo is empty.
                    --         if r_resend_fifo_empty = '1' then
                    --             resend_fsm                 <= WAIT_PULL_FROM_CACHE_FINISH;
                    --             diag_if.cache_retx_counter <= diag_if.cache_retx_counter + 1;
                    --         -- if not empty, ERROR.
                    --         else
                    --             resend_fsm <= CACHE_ERROR;
                    --         end if;
                    --     end if;

                    -- when WAIT_PULL_FROM_CACHE_FINISH =>
                    --     mux_hold_in  <= '1';
                    --     mux_hold_out <= '0';
                    --     -- wait until operation is finished:
                    --     if ram_cmd_if_rd.busy = '0' then
                    --         resend_fsm <= START;
                    --     else
                    --         resend_fsm <= WAIT_PULL_FROM_CACHE_FINISH;
                    --     end if;

                    -- Checks the req cache interface:
                    --when CHECK_CACHE_REQ => 
                    --    req_cache_if.ready <= '1';
                    --    if req_cache_if.valid = '1' then
                    --        req_cache_if.ready <= '0';
                    --        r_req_cache.tx_fn  <= req_cache_if.tx_fn;
                    --        --r_req_cache.tx_num <= req_cache_if.tx_num;
                    --        --r_req_cache.frame_type <= req_cache_if.frame_type;
                    --        r_req_cache.addr   <= req_cache_if.addr;
                    --        resend_fsm <= FIND_FREE_CACHE;
                    --    else
                    --        resend_fsm <= START; --TODO: CHECK THIS!
                    --    end if;


                end case;

                if clear_diag = '1' then
                    --diag_if.retx_counter       <= (others => '0');
                    diag_if.cache_req_counter  <= (others => '0');
                    --diag_if.cache_retx_counter <= (others => '0');
                    diag_if.cache_timeout_counter <= (others => '0');
                    --diag_if.retx_error         <= (others => '0');
                    diag_if.cache_error        <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    -------------------------------------------
    --- PROCESS RESEND RD FROM CACHE
    -------------------------------------------
    PRCS_RESEND : process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                --default:
                req_resend_if_s.ready            <= '0';
                mux_hold_out                   <= '1';
                mux_ctrl_out                   <= (others => '0');
                resend_fsm                     <= RESEND_START;
                axis_payload_if_out_m.tx_fn      <= (others => '0');
                axis_payload_if_out_m.frame_type <= (others => '0');
                axis_payload_if_out_m.tx_num     <= (others => '0');
                --diag_if.retx_counter           <= (others => '0');
                --diag_if.cache_req_counter      <= (others => '0');
                diag_if.cache_retx_counter     <= (others => '0');
                --diag_if.cache_timeout_counter  <= (others => '0');
                diag_if.retx_error             <= (others => '0');
                diag_if.cache_resend_miss <= (others => '0');
                resend_it_cnt <= (others=>'0');
                resend_retry_cnt  <= (others =>'0');
            else
                --default:
                req_resend_if_s.ready     <= '0';
                mux_hold_out            <= '1';
                --ram_cmd_if_rd.cmd_valid <= '0';
                --resend_fifo_reset_int   <= '0';
                --cache_release_reset_array <= (others=>'0');

                case resend_fsm is

                    when RESEND_START =>
                        resend_fsm              <= CHECK_REQ_RESEND ;
                        -- Reset and default all signals and counters;
                        -- ram_cmd_if_rd.request   <= '0';
                        -- ram_cmd_if_rd.cmd_valid <= '0';
                        -- ram_cmd_if_rd.addr      <= (others => '0');
                        resend_retry_cnt  <= (others =>'0');
                        resend_it_cnt            <= (others => '0');
                    --resend_fifo_reset_int <= '0';                
                        --reset_clk_cnt <= 0; 
                        
                    when CHECK_REQ_RESEND =>
                        --THis logic is different as there is not intermediate FIFO:
                        resend_fsm <= CHECK_REQ_RESEND;
                        --req_resend_if.ready <= '1';   -- TODO: Rethink this!!
                        --if req_resend_if.pending_request_n = '0' then 
                        if req_resend_if_m.valid = '1' then --and req_resend_if.ready = '1' then
                            req_resend_if_s.ready     <= '1';
                            resend_fsm <= FIND_IN_CACHE;
                            r_req_resend.tx_fn      <= req_resend_if_m.tx_fn;
                            r_req_resend.tx_num     <= req_resend_if_m.tx_num;
                            r_req_resend.frame_type <= req_resend_if_m.frame_type;
                            r_req_resend.addr       <= req_resend_if_m.addr;
                        end if;

                    when FIND_IN_CACHE =>
                        -- Iterate over cache metadata records to find TX_FN
                        if resend_it_cnt = C_CACHE_SIZE then
                            resend_it_cnt <= (others=>'0');
                            -- Cache MISS, pull frame from RAM:
                            if resend_retry_cnt = to_unsigned(C_RESEND_RETRAY_MAX, resend_retry_cnt'length) then
                                resend_retry_cnt <= (others=>'0');
                                resend_fsm   <= CACHE_MISS;
                            else
                                resend_retry_cnt <= resend_retry_cnt + 1;
                                resend_fsm   <= FIND_IN_CACHE ;
                            end if;
                        else
                            -- check cache tx_fn matches and it is a valid entry:
                            if cache_metadata_array(to_integer(resend_it_cnt)).tx_fn = r_req_resend.tx_fn and cache_empty_flag_array(to_integer(resend_it_cnt)) = '0' then
                                -- CACHE HIT!
                                resend_fsm <= CACHE_HIT_PUSH;
                            else
                                resend_it_cnt <= resend_it_cnt + 1;
                            end if;
                        end if;
                    --resend_fsm <= CHECK_RESEND_REQ;

                    when CACHE_HIT_PUSH =>
                        -- Set muxe to the cache buffer hit
                        mux_ctrl_out <= std_logic_vector(resend_it_cnt); --std_logic_vector(to_unsigned(resend_it_cnt, mux_ctrl_out'length));
                        mux_hold_out <= '1';
                        resend_fsm   <= PULL_FROM_CACHE;
                    
                    when CACHE_MISS =>
                        -- Failed to find TX_FN in cache. Drop resend request and report error.
                        diag_if.cache_resend_miss <= diag_if.cache_resend_miss + 1;                       
                        resend_fsm <= RESEND_START;

                    --Resend request is in cache. Pulls frame until it is finished
                    when PULL_FROM_CACHE =>
                        -- set hold on MUXs:
                        mux_hold_out                   <= '0';
                        -- set metadata: 
                        axis_payload_if_out_m.frame_type <= r_req_resend.frame_type;
                        axis_payload_if_out_m.tx_num     <= r_req_resend.tx_num;
                        axis_payload_if_out_m.tx_fn      <= cache_metadata_array(to_integer(resend_it_cnt)).tx_fn;
                        -- axis_payload_if_out.frame_type <= cache_metadata_array(cache_it_cnt).frame_type;     
                        -- axis_payload_if_out.tx_num <= cache_metadata_array(cache_it_cnt).tx_num;     

                        if axis_payload_if_out_m.axis_if_m.tvalid = '1' and axis_payload_if_out_m.axis_if_m.tlast = '1' then
                            -- check fifo is empty.
                            if  cache_empty_flag_array(to_integer(resend_it_cnt)) = '1' then
                                resend_fsm                 <= RESEND_START;
                                diag_if.cache_retx_counter <= diag_if.cache_retx_counter + 1;
                            -- if not empty, ERROR.
                            else
                                resend_fsm <= RESEND_ERROR;
                            end if;
                        end if;

                    when RESEND_ERROR =>
                        resend_fsm            <= RESEND_START;
                        --resend_fifo_reset_int <= '1';
                        diag_if.retx_error    <= diag_if.retx_error + 1;                       


                    --when WAIT_LAST =>

                    when others =>
                        resend_fsm <= RESEND_START;                       

                end case;

                if clear_diag = '1' then
                    --diag_if.retx_counter       <= (others => '0');
                    --diag_if.cache_req_counter  <= (others => '0');
                    diag_if.cache_retx_counter <= (others => '0');
                    --diag_if.cache_timeout_counter <= (others => '0');
                    diag_if.retx_error         <= (others => '0');
                    --diag_if.cache_error        <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    -------------------------------------------
    --- FIFO BUFFERS INPUT AND OUTPUT
    -------------------------------------------
    Inst_insertion_input_buffer : entity share_let_1g.axi4s_fifo_v2
        generic map(
            DISTR_RAM         => false,
            FIFO_DEPTH        => C_FSO_PAYLOAD_SIZE,
            DATA_WIDTH        => 8,
            FULL_THRESHOLD    => C_FSO_PAYLOAD_SIZE,
            EMPTY_THRESHOLD   => 0,
            USE_OUTPUT_BUFFER => false
        )
        port map(
            clk                => clk,
            rst                => insertion_fifo_reset_mux,
            -- Input data handling
            ----------------------
            input              => axis_payload_if_in_m.axis_if_m.tdata,
            input_valid        => axis_payload_if_in_m.axis_if_m.tvalid,
            input_last         => axis_payload_if_in_m.axis_if_m.tlast,
            input_ready        => axis_payload_if_in_s.axis_if_s.tready,
            input_user         => '0',
            input_start        => axis_payload_if_in_m.axis_if_m.tstart,
            input_almost_full  => open,
            input_almost_empty => r_insert_fifo_empty,
            fifo_max_level     => open,
            -- Output data handling
            -----------------------
            output             => axis_payload_if_buf_in_m.axis_if_m.tdata,
            output_valid       => axis_payload_if_buf_in_m.axis_if_m.tvalid,
            output_last        => axis_payload_if_buf_in_m.axis_if_m.tlast,
            output_ready       => axis_payload_if_buf_in_s.axis_if_s.tready,
            output_user        => open,
            output_start       => axis_payload_if_buf_in_m.axis_if_m.tstart
        );

    inst_arq_axis_pl_router_ram_wr : entity arq_lib.arq_axis_pl_router_1x2
        port map(
            --Input interfaces
            axis_payload_if_in_m    => axis_payload_if_buf_in_m, -- : inout t_axis_payload_if;
            axis_payload_if_in_s    => axis_payload_if_buf_in_s, -- : inout t_axis_payload_if;
            sel                   => (not sel_router_wr), -- : in    std_logic;
            --Output interfaces
            axis_payload_if_out_a_m => int_axis_in_m, -- : inout t_axis_payload_if;
            axis_payload_if_out_a_s => int_axis_in_s, -- : inout t_axis_payload_if;
            axis_payload_if_out_b_m => open, --axis_if_ground_m, -- : inout t_axis_payload_if
            axis_payload_if_out_b_s => axis_if_ground_s -- : inout t_axis_payload_if
        );

    --Inst_output_buffer : entity share_let_1g.axi4s_fifo_v2
    --    generic map(
    --        DISTR_RAM         => false,
    --        FIFO_DEPTH        => C_FSO_PAYLOAD_SIZE,
    --        DATA_WIDTH        => 8,
    --        FULL_THRESHOLD    => C_FSO_PAYLOAD_SIZE,
    --        EMPTY_THRESHOLD   => 0,
    --        USE_OUTPUT_BUFFER => false
    --    )
    --    port map(
    --        clk                => clk,
    --        rst                => rst,  -- TODO: resend_fifo_reset_mux,

    --        -- Input data handling
    --        ----------------------
    --        input              => axis_if_array_mux_out(C_RAM_MUX_INDEX).tdata,
    --        input_valid        => axis_if_array_mux_out(C_RAM_MUX_INDEX).tvalid,
    --        input_last         => axis_if_array_mux_out(C_RAM_MUX_INDEX).tlast,
    --        input_ready        => axis_if_array_mux_out(C_RAM_MUX_INDEX).tready,
    --        input_user         => '0',
    --        input_start        => axis_if_array_mux_out(C_RAM_MUX_INDEX).tstart,
    --        input_almost_full  => open,
    --        input_almost_empty => r_resend_fifo_empty,
    --        fifo_max_level     => open,
    --        -- Output data handling
    --        -----------------------
    --        output             => axis_if_array_in(C_RAM_MUX_INDEX).tdata,
    --        output_valid       => axis_if_array_in(C_RAM_MUX_INDEX).tvalid,
    --        output_last        => axis_if_array_in(C_RAM_MUX_INDEX).tlast,
    --        output_ready       => axis_if_array_in(C_RAM_MUX_INDEX).tready,
    --        output_user        => open,
    --        output_start       => axis_if_array_in(C_RAM_MUX_INDEX).tstart
    --    );

    -- TODO: The payload output IF needs to set metadata into the bus.

    -------------------------------------------
    --- CACHE
    -------------------------------------------
    g_cache : for ii in 0 to (G_CACHE_SIZE - 1) generate
        
        reset_cache_mux(ii) <= rst or reset_cache_array(ii);  
        
        Inst_axi_cache_buffer : entity share_let_1g.axi4s_fifo_v2
            generic map(
                DISTR_RAM         => false,
                FIFO_DEPTH        => C_FSO_PAYLOAD_SIZE,
                DATA_WIDTH        => 8,
                FULL_THRESHOLD    => C_FSO_PAYLOAD_SIZE,
                EMPTY_THRESHOLD   => 0,
                USE_OUTPUT_BUFFER => false
            )
            port map(
                clk                => clk,
                rst                => reset_cache_mux(ii),
                input              => axis_if_array_mux_out_m(ii).tdata,
                input_valid        => axis_if_array_mux_out_m(ii).tvalid,
                input_last         => axis_if_array_mux_out_m(ii).tlast,
                input_ready        => axis_if_array_mux_out_s(ii).tready,
                input_user         => '0',
                input_start        => axis_if_array_mux_out_m(ii).tstart,
                input_almost_full  => open,
                input_almost_empty => cache_empty_flag_array(ii),
                fifo_max_level     => open,
                -- Output data handling
                -----------------------
                output             => axis_if_array_in_m(ii).tdata,
                output_valid       => axis_if_array_in_m(ii).tvalid,
                output_last        => axis_if_array_in_m(ii).tlast,
                output_ready       => axis_if_array_in_s(ii).tready,
                output_user        => open,
                output_start       => axis_if_array_in_m(ii).tstart
            );
    end generate;

    -------------------------------------------
    --- Cache timeout management
    -------------------------------------------
    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                r_cache_tx_fn_array <= (others=>(others=>'0')); 
                cache_counter_array <= (others=>0);
                cache_req_reset_array <= (others=>'0');
            else
                 -- Clear reset request when reset is asserted:
                for ii in 0 to G_CACHE_SIZE-1 loop
                    if cache_release_reset_array(ii) = '1' then
                        cache_req_reset_array(ii) <= '0';
                    end if;               
                end loop;

                if core_tick = '1' then
                    for ii in 0 to G_CACHE_SIZE-1 loop
                        r_cache_tx_fn_array(ii) <= cache_metadata_array(ii).tx_fn;

                        -- Chech that TX_fn match and cache is not empty:
                        if cache_metadata_array(ii).tx_fn = r_cache_tx_fn_array(ii) and cache_empty_flag_array(ii) = '0' then
                            if cache_counter_array(ii) > to_integer(unsigned(arq_config_if.holdoff_time)*8) then
                                --timeout:
                                cache_counter_array(ii) <= 0;
                                --apply reset:
                                cache_req_reset_array(ii) <= '1';
                            else
                                cache_counter_array(ii) <= cache_counter_array(ii) + 1;
                            end if;
                        else
                            cache_counter_array(ii) <= 0;
                        end if;
                    end loop;
                end if;
            end if;
        end if;

    end process;

    -------------------------------------------
    --- Timers management
    -------------------------------------------
    process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                core_counter <= 0;
                time_counter <= 0;
                core_tick <= '0';
            else
                -- Core counter to get 1us:
                core_tick <= '0';
                if core_counter < to_integer(unsigned(arq_config_if.core_counter_period)) then
                    core_counter <= core_counter + 1;
                else
                    core_counter <= 0;
                    core_tick <= '1';
                    -- if time_counter < to_integer(unsigned(arq_config_if.holdoff_time)) then
                    --     time_counter <= time_counter + 1;
                    -- else
                    --     time_counter <= 0;
                    --     core_tick <= '1';
                    -- end if;
                end if;
            end if;
        end if;
    end process;
    
    -------------------------------------------
    --- INST OF INTERNAL RAM CONTROLLER
    ------------------------------------------- 
    inst_arq_internal_ram_controller : entity arq_lib.arq_internal_ram_controller
        generic map(
            G_INTERNAL_MEMORY_FSIZE => G_INTERNAL_MEMORY_SIZE  --: natural := 16 -- Size in Frames
        )
        port map(
            clk            => clk,      --: in std_logic;
            rst            => rst,      --:: in std_logic;

            --Input interfaces
            ram_cmd_if_rd_m     => ram_cmd_if_rd_m, -- : inout t_ram_cmd_if;
            ram_cmd_if_rd_s     => ram_cmd_if_rd_s, -- : inout t_ram_cmd_if;
            ram_cmd_if_wr_m     => ram_cmd_if_wr_m, -- => --: inout t_ram_cmd_if;
            ram_cmd_if_wr_s     => ram_cmd_if_wr_s, -- => --: inout t_ram_cmd_if;
            axis_in_m           => int_axis_in_m.axis_if_m, ---:: inout t_axis_if;
            axis_in_s           => int_axis_in_s.axis_if_s, ---:: inout t_axis_if;

            --Output interfaces
            axis_out_m          => int_axis_out_m, --:: inout t_axis_if;
            axis_out_s          => int_axis_out_s, --:: inout t_axis_if;
            internal_error      => int_ram_error --:: out std_logic
        );

    -------------------------------------------
    --- AXIS MUX FOR OUTPUT
    -------------------------------------------
    inst_arq_axis_mux_nx1 : entity arq_lib.arq_axis_mux_nx1
        generic map(
            G_INPUT_NUM => G_CACHE_SIZE
        )
        port map(
            axis_if_in_m  => axis_if_array_in_m,
            axis_if_in_s  => axis_if_array_in_s,
            ctrl        => mux_ctrl_out, --: in std_logic_vector((no_bits_natural(G_INPUT_NUM)-1) downto 0);
            hold        => mux_hold_out,
            --Output interfaces
            axis_if_out_m => axis_payload_if_out_m.axis_if_m, --: inout t_axis_payload_if
            axis_if_out_s => axis_payload_if_out_s.axis_if_s --: inout t_axis_payload_if
        );
    -- end generate;

    --axis_payload_if_out.axis_if.tdata <= axis_payload_if_array_in(16).axis_if.tdata;
    --axis_payload_if_out.axis_if.tvalid <= axis_payload_if_array_in(16).axis_if.tvalid;
    --axis_payload_if_out.axis_if.tstart <= axis_payload_if_array_in(16).axis_if.tstart;
    --axis_payload_if_out.axis_if.tlast <= axis_payload_if_array_in(16).axis_if.tlast;
    --axis_payload_if_array_in(16).axis_if.tready <= axis_payload_if_out.axis_if.tready;

    ---------------------------------------------
    --- AXIS MUX FOR INPUT 
    -------------------------------------------
    inst_arq_axis_mux_1xn : entity arq_lib.arq_axis_mux_1xn
        generic map(
            G_OUTPUT_NUM => G_CACHE_SIZE 
        )
        port map(
            axis_if_in_m  => int_axis_out_m, -- axis_if_mux_in,       
            axis_if_in_s  => int_axis_out_s, -- axis_if_mux_in,       
            ctrl        => mux_ctrl_in, --: in std_logic_vector((no_bits_natural(G_INPUT_NUM)-1) downto 0);
            hold        => mux_hold_in,
            --Output interfaces
            axis_if_out_m => axis_if_array_mux_out_m, --: inout t_axis_payload_if
            axis_if_out_s => axis_if_array_mux_out_s --: inout t_axis_payload_if
        );
        -- end generate;

end architecture RTL;

