-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: arq_req_control_support_pkg
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Converts Ethernet packets to SDA Standard FSO frames in AXI-Stream format.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
package arq_req_control_support_pkg is

    --function calc_holdoff_number(signal cfg_holdoff : natural; signal block_num : natural) return std_logic_vector;
    --function find_valid_entry (signal timer_array : t_timer_array) return integer;

end package arq_req_control_support_pkg;

package body arq_req_control_support_pkg is

    --function calc_holdoff_number(signal cfg_holdoff : natural; signal block_num : natural) return std_logic_vector
    --        v_return : std_logic_vector(8-1 downto 0);
    --    begin 
    --            
    --        return v_return;
    --end function;

end package body arq_req_control_support_pkg;
