-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 23/09/2022 11:40:33 AM
-- Design Name: 
-- Module Name: arq_rom_addr_translator
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Interface for the ARQ external RAM. It may have internal RAM to
-- make simulation and tests easier.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library arq_lib;
use arq_lib.arq_support_pkg.all;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

entity arq_rom_addr_translator is
    port(
        clk      : in  std_logic;
        --Input interfaces
        addr     : in  std_logic_vector(C_ADDRESS_WIDTH - 1 downto 0);
        enable   : in  std_logic;
        --Output interfaces
        ram_addr : out std_logic_vector(C_INTERNAL_RAM_ADDR_WIDTH_MAX - 1 downto 0)
    );
end entity arq_rom_addr_translator;

architecture RTL of arq_rom_addr_translator is

    constant ROM : rom_addr_array_t := LUT_init_ram_addr;

begin

    prc_rom : process(clk)
    begin
        if rising_edge(clk) then
            if enable = '1' then
                ram_addr <= ROM(to_integer(unsigned(addr)));
            end if;
        end if;
    end process prc_rom;

end architecture RTL;
