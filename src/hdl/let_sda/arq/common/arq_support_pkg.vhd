-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: arq_support_pkg
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: arq_support_pkg
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library arq_lib;
library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

package arq_support_pkg is

    type axis_if_m_array_t is array (integer range <>) of t_axis_if_m;
    type axis_if_s_array_t is array (integer range <>) of t_axis_if_s;
    type axis_payload_if_m_array_t is array (integer range <>) of t_axis_payload_if_m;
    type axis_payload_if_s_array_t is array (integer range <>) of t_axis_payload_if_s;

    type rom_addr_array_t is array (0 to C_ARQ_HOLDOFF_NFRAMES_MAX - 1) of std_logic_vector(C_INTERNAL_RAM_ADDR_WIDTH_MAX - 1 downto 0);

    --function calc_holdoff_number(signal cfg_holdoff : natural; signal block_num : natural) return std_logic_vector;
    --function find_valid_entry (signal timer_array : t_timer_array) return integer;
    function LUT_init_ram_addr return rom_addr_array_t;

end package arq_support_pkg;

package body arq_support_pkg is

    function LUT_init_ram_addr return rom_addr_array_t is
        variable v_rom_addr_rom : rom_addr_array_t;
        variable v_ram_addr     : std_logic_vector(C_INTERNAL_RAM_ADDR_WIDTH_MAX - 1 downto 0);
    begin
        for ii in 0 to rom_addr_array_t'high loop
            --Need to divide by 8 because every word is 8 bytes (64 bits in RAM)
            v_rom_addr_rom(ii) := std_logic_vector(to_unsigned(ii * C_INTERNAL_RAM_BLOCK_SIZE / 8, C_INTERNAL_RAM_ADDR_WIDTH_MAX));
        end loop;
        return v_rom_addr_rom;
    end function;

end package body arq_support_pkg;
