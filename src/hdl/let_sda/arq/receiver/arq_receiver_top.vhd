-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 23/09/2022 11:40:33 AM
-- Design Name: 
-- Module Name: arq_receiver_top 
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Interface for the ARQ external RAM. It may have internal RAM to
-- make simulation and tests easier.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library arq_lib;
use arq_lib.arq_support_pkg.all;
library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;
library share_let_1g;
use share_let_1g.pkg_support.all;

entity arq_receiver_top is
    generic(
        --G_INTERNAL_RAM : boolean := true;
        G_INTERNAL_MEMORY_SIZE       : natural := 16; -- Size in Frames
        G_CACHE_SIZE                 : natural := 16;
        G_MEMORY_BLOCK_SIZE          : natural := 256; -- A low value increases performance at a huge resource cost. Must be power of 2.
        G_CORE_COUNTER_PERIOD_CLOCKS : natural := 100
    );
    port(
        clk            : in    std_logic;
        rst            : in    std_logic;
        -- ARQ config:
        arq_config_if        : in    t_arq_config_if;
        -- ACK interface:
        ack_data_if    : inout t_ack_data_if;
        -- AXI payload interfaces:
        axis_pl_if_in  : inout t_axis_payload_if;
        axis_pl_if_out : inout t_axis_payload_if;
        -- Diagnostics:
        arq_hw_config  : out   t_arq_hw_config;
        diag_control_if : inout t_arq_control_tx_diagnostics_if;
        diag_ram_if     : inout t_arq_ram_controller_diag_if
        -- Ext. RAM interface:
        -- TODO: TBD

    );
end entity arq_receiver_top;

architecture RTL of arq_receiver_top is

    -- Ledger interface: 
    signal req_insertion_if        : t_request_insertion_if;
    signal metadata_retrieve_if    : t_metadata_retrieve_if;
    -- Ram interface: 
    signal req_insertion_ram_if    : t_request_insertion_ram_if;
    signal axis_payload_if_ram_in  : t_axis_payload_if;
    signal axis_payload_if_ram_out : t_axis_payload_if;
    -- Diagnostics:
    signal metadata_error          : std_logic;
    signal req_resend_if           : t_request_resend_if;
    signal req_cache_if            : t_request_cache_if;
    signal insert_fifo_empty        : std_logic;
begin

    inst_arq_timekeeper_rx_wrapper : entity arq_lib.arq_timekeeper_rx_wrapper
        generic map(
            G_MEMORY_BLOCK_SIZE          => G_MEMORY_BLOCK_SIZE, -- A low value increases performance at a huge resource cost.Must be power of 2.
            G_CORE_COUNTER_PERIOD_CLOCKS => G_CORE_COUNTER_PERIOD_CLOCKS
        )
        port map(
            clk                  => clk, --: in std_logic;
            rst                  => rst, --: in std_logic;

            --Input interfaces:
            arq_config_if        => arq_config_if, --: in t_arq_config_if;
            ack_data_if          => ack_data_if, --: inout  t_ack_data_if;
            req_insertion_if     => req_insertion_if, --: inout t_request_insertion_if;

            --Output interfaces;
            req_resend_if        => req_resend_if, --: inout t_request_resend_if;
            req_cache_if         => req_cache_if, --: out t_request_cache_bus_if
            metadata_retrieve_if => metadata_retrieve_if,
            diag_if              => diag_control_if, --: inout t_arq_control_tx_diagnostics_if
            arq_hw_config        => arq_hw_config
        );

    inst_arq_router_rx : entity arq_lib.arq_router_rx
        port map(
            clk                     => clk, --: in    std_logic;
            rst                     => rst, --: in    std_logic;
            -- Config interface:
            arq_config_if           => arq_config_if, --: in    t_arq_config_if;
            -- Input interfaces:
            axis_payload_if_in      => axis_pl_if_in, --: inout t_axis_payload_if;
            -- Output interface:
            axis_payload_if_out     => axis_pl_if_out, --: inout t_axis_payload_if;
            -- Ledger interface: 
            req_insertion_if        => req_insertion_if, --: inout t_request_insertion_if;
            metadata_retrieve_if    => metadata_retrieve_if, --: in t_metadata_retrieve_if; 
            -- Ram interface: 
            req_insertion_ram_if    => req_insertion_ram_if, --: inout t_request_insertion_ram_if;
            axis_payload_if_ram_in  => axis_payload_if_ram_in, --: inout t_axis_payload_if;
            axis_payload_if_ram_out => axis_payload_if_ram_out, --: inout t_axis_payload_if;
            -- Diagnostics:
            metadata_error          => metadata_error --: out std_logic    
            -- TODO: add interface
        );

    inst_arq_ram_controller_top : entity arq_lib.arq_ram_controller_top
        generic map(
            --G_INTERNAL_RAM : boolean := true;
            G_INTERNAL_MEMORY_SIZE => G_INTERNAL_MEMORY_SIZE, --: natural := 16; -- Size in Frames
            G_CACHE_SIZE           => G_CACHE_SIZE --: natural := 16
        )
        port map(
            clk                 => clk, --: in std_logic;
            rst                 => rst, --: in std_logic;

            --Input interfaces
            req_resend_if       => req_resend_if, --: inout t_request_resend_if;
            req_insertion_if    => req_insertion_ram_if, --: inout t_request_insertion_if;
            req_cache_if        => req_cache_if, --: inout t_request_cache_if;
            axis_payload_if_in  => axis_payload_if_ram_out, --  : inout t_axis_payload_if;
            insert_fifo_empty   => insert_fifo_empty, --: out std_logic; 
            arq_config_if       => arq_config_if,
            --Output interfaces
            axis_payload_if_out => axis_payload_if_ram_in, --: inout t_axis_payload_if
            --arq_ram_diagnostics_if : inout t_arq_control_tx_diagnostics_if
            diag_if             => diag_ram_if --t_arq_ram_controller_diag_if  --arq_ram_diagnostics_if : inout t_arq_control_tx_diagnostics_if
        );

end architecture RTL;
