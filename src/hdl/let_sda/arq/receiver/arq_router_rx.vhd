-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 23/09/2022 11:40:33 AM
-- Design Name: 
-- Module Name: arq_router 
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Interface for the ARQ external RAM. It may have internal RAM to
-- make simulation and tests easier.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library arq_lib;
use arq_lib.arq_support_pkg.all;
library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;
library share_let_1g;
use share_let_1g.pkg_support.all;

entity arq_router_rx is
    port(
        clk                     : in    std_logic;
        rst                     : in    std_logic;
        -- Config interface:
        arq_config_if           : in    t_arq_config_if;
        -- Input interfaces:
        axis_payload_if_in_m      : in t_axis_payload_if_m;
        axis_payload_if_in_s      : out t_axis_payload_if_s;
        -- Output interface:
        axis_payload_if_out_m     : out t_axis_payload_if_m;
        axis_payload_if_out_s     : in t_axis_payload_if_s;
        -- Ledger interface: 
        req_insertion_if_m        : out t_request_insertion_if_m;
        req_insertion_if_s        : in t_request_insertion_if_s;
        metadata_retrieve_if    : in    t_metadata_retrieve_if;
        seek_frame_if_m           : out t_seek_frame_if_m;
        seek_frame_if_s           : in t_seek_frame_if_s;
        tx_fn_ref               : in    std_logic_vector(C_TX_FN_WIDTH - 1 downto 0); -- next tx_fn to forward
        frame_bypassed          : out   std_logic; -- Indicates frame is bypassed
        -- Ram interface: 
        req_insertion_ram_if_m    : out t_request_insertion_ram_if_m;
        req_insertion_ram_if_s    : in t_request_insertion_ram_if_s;
        axis_payload_if_ram_in_m  : in t_axis_payload_if_m;
        axis_payload_if_ram_in_s  : out t_axis_payload_if_s;
        axis_payload_if_ram_out_m : out t_axis_payload_if_m;
        axis_payload_if_ram_out_s : in t_axis_payload_if_s;
        -- Diagnostics:
        metadata_error          : out   std_logic
        -- TODO: add interface
    );
end entity arq_router_rx;

architecture RTL of arq_router_rx is

    -- Define ARQ config disabled:
    constant C_ARQ_CONFIG_DISABLED  : std_logic_vector             := std_logic_vector(to_unsigned(0, arq_config_if.max_retx'length));
    constant C_ROUTER_BLOCK         : std_logic_vector(1 downto 0) := "00";
    constant C_ROUTER_BYPASS_ENABLE : std_logic_vector(1 downto 0) := "11";
    constant C_ROUTER_RAM_ENABLE    : std_logic_vector(1 downto 0) := "01";

    constant C_ROUTER_OUT_ENABLE_RAM    : std_logic := '1';
    constant C_ROUTER_OUT_ENABLE_BYPASS : std_logic := '0';

    -- There are corner cases where an old frame tx_fn can be larger than tx_fn_ref but actually is older
    -- and this happens when there is a frame counter overflow and starts at 0 again.
    -- The exception window applies in that scenario, to avoid insertion of frames that are older:
    constant C_EXCEPTION_WINDOW    : unsigned(16 - 1 downto 0) := to_unsigned(500, 16);
    constant C_EXCEPTION_WINDOW_UP : unsigned(16 - 1 downto 0) := to_unsigned(65535, 16) - C_EXCEPTION_WINDOW;

    constant C_TX_NUM_NULL : unsigned(C_TX_NUM_WIDTH-1 downto 0) := (others=>'0');
    
    -- Router OUT
    signal axis_router_if_in_bypass_m    : t_axis_payload_if_m;
    signal axis_router_if_in_bypass_s    : t_axis_payload_if_s;
    signal axis_router_if_in_ram_m       : t_axis_payload_if_m;
    signal axis_router_if_in_ram_s       : t_axis_payload_if_s;
    signal sel_router_out              : std_logic;
    signal axis_router_if_out_m          : t_axis_payload_if_m;
    signal axis_router_if_out_s          : t_axis_payload_if_s;
    signal axis_payload_if_ram_buf_in_m  : t_axis_payload_if_m;
    signal axis_payload_if_ram_buf_in_s  : t_axis_payload_if_s;
    signal axis_payload_if_ram_buf_out_m : t_axis_payload_if_m;
    signal axis_payload_if_ram_buf_out_s : t_axis_payload_if_s;
    signal int_back_pressure           : std_logic;
    -- Routers IN 
    signal axis_bypass_fifo_if_in_m      : t_axis_payload_if_m;
    signal axis_bypass_fifo_if_in_s      : t_axis_payload_if_s;
    signal axis_ram_fifo_if_in_m         : t_axis_payload_if_m;
    signal axis_ram_fifo_if_in_s         : t_axis_payload_if_s;
    signal axis_bypass_if_out_m          : t_axis_payload_if_m;
    signal axis_bypass_if_out_s          : t_axis_payload_if_s;
    signal axis_ram_if_out_m             : t_axis_payload_if_m;
    signal axis_ram_if_out_s             : t_axis_payload_if_s;
    signal axis_if_ground_m              : t_axis_payload_if_m;
    signal axis_if_ground_s              : t_axis_payload_if_s;
    signal axis_if_ground_ram_m          : t_axis_payload_if_m;
    signal axis_if_ground_ram_s          : t_axis_payload_if_s;

    -- signal sel_router_ram    : std_logic;
    signal sel_router : std_logic_vector(1 downto 0);

    -- Dual Buffers:
    signal axis_pl_if_in_m         : t_axis_payload_if_m;
    signal axis_pl_if_in_s         : t_axis_payload_if_s;
    signal axis_pl_if_ram_buf_m    : t_axis_payload_if_m;
    signal axis_pl_if_ram_buf_s    : t_axis_payload_if_s;
    signal axis_pl_if_bypass_buf_m : t_axis_payload_if_m;
    signal axis_pl_if_bypass_buf_s : t_axis_payload_if_s;
    signal int_axis_pl_if_in_m     : t_axis_payload_if_m;
    signal int_axis_pl_if_in_s     : t_axis_payload_if_s;

    -- FSM:
    type router_fsm_t is (SET_CONFIG,
                          INIT,
                          DETECT_START,
                          COMPUTE_CONDITIONS,
                          NORMAL_CONDITION,
                          LOW_LIMIT_CONDITION,
                          HIGH_LIMIT_CONDITION,
                          SEEK_FRAME_START,
                          SEEK_FRAME_WAIT,
                          FIFO_IN_RESET,
                          DROP_FRAME,
                          SET_ROUTER_FOR_INSERTION,
                          SET_ROUTER_BYPASS,
                          REQ_INSERTION,
                          GET_METADATA,
                          REQ_RAM_INSERTION,
                          DETECT_LAST,
                          DETECT_CACHE_PRESSURE,
                          CHECK_RAM_INSERTION_FINISHED,
                          CACHE_FORWARD,
                          ARQ_DISABLED
                         );

    signal router_fsm : router_fsm_t;

    type metadata_t is record
        tx_fn : std_logic_vector(C_TX_FN_WIDTH - 1 downto 0);
        addr  : std_logic_vector(C_ADDRESS_WIDTH - 1 downto 0); -- ?
    end record;
    signal r_metadata : metadata_t;
begin

    -------------------------------------------
    --- ASYNC:
    -------------------------------------------
    --axis_if_ground <= C_AXIS_PAYLOAD_IF_SLAVE_INIT;
    --axis_if_ground.tx_fn          <= (others => 'Z');
    --axis_if_ground.frame_type     <= (others => 'Z');
    --axis_if_ground.tx_num         <= (others => 'Z');
    --axis_if_ground.axis_if.tdata  <= (others => 'Z');
    --axis_if_ground.axis_if.tuser  <= 'Z';
    --axis_if_ground.axis_if.tvalid <= 'Z';
    --axis_if_ground.axis_if.tstart <= 'Z';
    --axis_if_ground.axis_if.tlast  <= 'Z';

    -- used in process to drop frame:
    --axis_if_ground.axis_if.tready      <= '0'; 

    --req_insertion_if.ready     <= 'Z';
    --req_insertion_ram_if.ready <= 'Z';
    --req_insertion_ram_if.busy  <= 'Z';

    -- seek frame interface as a slave:
    --seek_frame_if.busy  <= 'Z';
    --seek_frame_if.found <= 'Z';

    -------------------------------------------
    --- MAIN FSM:
    -------------------------------------------
    process(clk) is
        variable v_fifo_rst_cnt : natural := 0;
    begin
        if rising_edge(clk) then
            if rst = '1' then
                router_fsm                    <= SET_CONFIG;
                frame_bypassed                <= '0';
                axis_if_ground_s.axis_if_s.tready <= '0';
                --req_insertion_if            <= C_REQ_INSERTION_IF_MASTER_INIT;
                req_insertion_if_m.tx_fn        <= (others => '0');
                req_insertion_if_m.frame_type   <= (others => '0');
                req_insertion_if_m.valid        <= '0';
                req_insertion_ram_if_m          <= C_REQ_INSERTION_RAM_IF_MASTER_INIT;
                sel_router                    <= C_ROUTER_BLOCK;
                sel_router_out                <= C_ROUTER_OUT_ENABLE_BYPASS;
                r_metadata.tx_fn              <= (others => '0');
                r_metadata.addr               <= (others => '0');
                axis_if_ground_ram_s            <= C_AXIS_PAYLOAD_IF_SLAVE_INIT;
                v_fifo_rst_cnt                := 0;
                metadata_error                <= '0';
                seek_frame_if_m.seek            <= '0';
                seek_frame_if_m.tx_fn           <= (others => '0');
            else
                -- default: 
                req_insertion_if_m.valid            <= '0';
                req_insertion_ram_if_m.valid        <= '0';
                axis_if_ground_ram_s.axis_if_s.tready <= '0';
                seek_frame_if_m.seek                <= '0';
                frame_bypassed                    <= '0';
                -- Default use block frames
                axis_if_ground_s.axis_if_s.tready     <= '0';

                case router_fsm is

                    when SET_CONFIG =>
                        if arq_config_if.max_retx = C_ARQ_CONFIG_DISABLED then
                            router_fsm <= ARQ_DISABLED;
                        else
                            router_fsm <= INIT;
                        end if;

                        sel_router     <= C_ROUTER_BLOCK;
                        sel_router_out <= C_ROUTER_OUT_ENABLE_BYPASS;

                    when INIT =>
                        sel_router     <= C_ROUTER_BLOCK;
                        sel_router_out <= C_ROUTER_OUT_ENABLE_BYPASS;
                        router_fsm     <= DETECT_START;

                    when DETECT_START =>
                        sel_router     <= C_ROUTER_BLOCK;
                        sel_router_out <= C_ROUTER_OUT_ENABLE_BYPASS;
                        -- Pause FSM if back pressure is detected:
                        if int_back_pressure = '0' then
                            if axis_pl_if_bypass_buf_m.axis_if_m.tvalid = '1' and axis_pl_if_bypass_buf_m.axis_if_m.tstart = '1' then
                                -- If frame is IDLE of TX_FN matches the reference pointer, bypass the frame.
                                if axis_pl_if_bypass_buf_m.frame_type = C_FRAME_IDLE then
                                    router_fsm <= SET_ROUTER_BYPASS;

                                -- If txfn ref matches the frame, frame is bypassed.
                                elsif tx_fn_ref = axis_pl_if_bypass_buf_m.tx_fn then
                                    router_fsm     <= SET_ROUTER_BYPASS;
                                    frame_bypassed <= '1';
                                else
                                    router_fsm <= COMPUTE_CONDITIONS;
                                end if;
                            else
                                if axis_payload_if_ram_buf_in_m.axis_if_m.tvalid = '1' then
                                    router_fsm <= DETECT_CACHE_PRESSURE;
                                end if;
                            end if;
                        end if;

                    when COMPUTE_CONDITIONS =>
                        if unsigned(tx_fn_ref) < C_EXCEPTION_WINDOW then
                            router_fsm <= LOW_LIMIT_CONDITION;
                        elsif unsigned(tx_fn_ref) > C_EXCEPTION_WINDOW_UP then
                            router_fsm <= HIGH_LIMIT_CONDITION;
                        else
                            router_fsm <= NORMAL_CONDITION;
                        end if;

                    when LOW_LIMIT_CONDITION =>
                        if unsigned(tx_fn_ref) < unsigned(axis_pl_if_bypass_buf_m.tx_fn) and unsigned(axis_pl_if_bypass_buf_m.tx_fn) > C_EXCEPTION_WINDOW_UP then
                            -- drop
                            router_fsm <= DROP_FRAME;
                            sel_router <= C_ROUTER_BLOCK;

                        elsif unsigned(tx_fn_ref) < unsigned(axis_pl_if_bypass_buf_m.tx_fn) and unsigned(axis_pl_if_bypass_buf_m.tx_fn) < C_EXCEPTION_WINDOW_UP then
                            -- insertion
                            seek_frame_if_m.tx_fn <= axis_pl_if_bypass_buf_m.tx_fn;
                            router_fsm          <= SEEK_FRAME_START;

                        elsif unsigned(tx_fn_ref) > unsigned(axis_pl_if_bypass_buf_m.tx_fn) then
                            -- drop
                            router_fsm <= DROP_FRAME;
                            sel_router <= C_ROUTER_BLOCK;
                        else
                            --drop
                            router_fsm <= DROP_FRAME;
                            sel_router <= C_ROUTER_BLOCK;
                        end if;

                    when HIGH_LIMIT_CONDITION =>
                        if unsigned(tx_fn_ref) < unsigned(axis_pl_if_bypass_buf_m.tx_fn) and unsigned(axis_pl_if_bypass_buf_m.tx_fn) > C_EXCEPTION_WINDOW_UP then
                            -- insertion
                            seek_frame_if_m.tx_fn <= axis_pl_if_bypass_buf_m.tx_fn;
                            router_fsm          <= SEEK_FRAME_START;

                        elsif unsigned(axis_pl_if_bypass_buf_m.tx_fn) < C_EXCEPTION_WINDOW then
                            -- insertion
                            seek_frame_if_m.tx_fn <= axis_pl_if_bypass_buf_m.tx_fn;
                            router_fsm          <= SEEK_FRAME_START;

                        elsif unsigned(tx_fn_ref) > unsigned(axis_pl_if_bypass_buf_m.tx_fn) then
                            --drop
                            router_fsm <= DROP_FRAME;
                            sel_router <= C_ROUTER_BLOCK;
                        else
                            --drop
                            router_fsm <= DROP_FRAME;
                            sel_router <= C_ROUTER_BLOCK;
                        end if;

                    when NORMAL_CONDITION =>
                        if unsigned(tx_fn_ref) < unsigned(axis_pl_if_bypass_buf_m.tx_fn) then
                            --insertion
                            seek_frame_if_m.tx_fn <= axis_pl_if_bypass_buf_m.tx_fn;
                            router_fsm          <= SEEK_FRAME_START;
                        else
                            --drop
                            router_fsm <= DROP_FRAME;
                            sel_router <= C_ROUTER_BLOCK;
                        end if;

                    when DROP_FRAME =>
                        router_fsm                    <= DROP_FRAME;
                        sel_router                    <= C_ROUTER_BLOCK;
                        -- Dropping frame:
                        axis_if_ground_s.axis_if_s.tready <= '1';

                        if axis_pl_if_bypass_buf_m.axis_if_m.tvalid = '1' and axis_pl_if_bypass_buf_m.axis_if_m.tlast = '1' and axis_pl_if_bypass_buf_s.axis_if_s.tready = '1' then
                            router_fsm                    <= INIT;
                            -- Block frame:
                            axis_if_ground_s.axis_if_s.tready <= '0';
                        end if;

                    when SEEK_FRAME_START =>
                        -- Tx_num is not 0. It means it could have been already inserted in timekeeper.
                        -- Start procedure to search it and avoid double insertion.
                        if unsigned(axis_pl_if_bypass_buf_m.tx_num) > C_TX_NUM_NULL then
                            -- No timeout needed in this procedure. Timekeeper needs to guarantee the response:
                            seek_frame_if_m.seek <= '1';
                            --confirm timekeeper is seeking:
                            if seek_frame_if_s.busy = '1' then
                                router_fsm <= SEEK_FRAME_WAIT;
                            end if;
                        else
                            -- Tx_num is 0, so no need to seek the frame in timekeeper. Just insert it.
                            router_fsm <= REQ_INSERTION;
                        end if;

                    -- First, try to find if tx_fn is already in timekeeper.
                    when SEEK_FRAME_WAIT =>
                        seek_frame_if_m.seek <= '1';
                        if seek_frame_if_s.found = '1' then
                            -- Frame is already in timekeeper. No need to insert it.
                            -- TODO: Think if drop is needed
                            -- Reset Fifo buffer to drop frame:
                            router_fsm <= FIFO_IN_RESET;

                        elsif seek_frame_if_s.busy = '0' then
                            -- Seek finished, not found. Start frame insertion:
                            router_fsm <= REQ_INSERTION;
                        end if;

                    when FIFO_IN_RESET =>
                        if v_fifo_rst_cnt = 7 then
                            router_fsm     <= INIT;
                            v_fifo_rst_cnt := 0;
                        else
                            v_fifo_rst_cnt := v_fifo_rst_cnt + 1;
                        end if;

                    when REQ_INSERTION =>
                        if req_insertion_if_s.ready = '1' then
                            req_insertion_if_m.valid      <= '1';
                            req_insertion_if_m.frame_type <= axis_pl_if_bypass_buf_m.frame_type;
                            req_insertion_if_m.tx_fn      <= axis_pl_if_bypass_buf_m.tx_fn;
                            router_fsm                  <= GET_METADATA;
                            r_metadata.tx_fn            <= axis_pl_if_bypass_buf_m.tx_fn;
                        end if;

                    when GET_METADATA =>
                        if metadata_retrieve_if.valid = '1' then
                            -- TX_FN MUST MATCH:
                            if r_metadata.tx_fn = metadata_retrieve_if.tx_fn then
                                r_metadata.tx_fn <= metadata_retrieve_if.tx_fn;
                                r_metadata.addr  <= metadata_retrieve_if.addr;
                                router_fsm       <= REQ_RAM_INSERTION;
                            else
                                metadata_error <= '1';
                                router_fsm     <= INIT;
                            end if;
                        end if;

                    when REQ_RAM_INSERTION =>
                        if req_insertion_ram_if_s.ready = '1' and req_insertion_ram_if_s.busy = '0' then
                            req_insertion_ram_if_m.valid <= '1';
                            req_insertion_ram_if_m.tx_fn <= r_metadata.tx_fn;
                            req_insertion_ram_if_m.addr  <= r_metadata.addr;
                            router_fsm                 <= SET_ROUTER_FOR_INSERTION;
                        end if;

                    when SET_ROUTER_FOR_INSERTION =>
                        sel_router_out <= C_ROUTER_OUT_ENABLE_BYPASS;
                        sel_router     <= C_ROUTER_RAM_ENABLE;
                        router_fsm     <= DETECT_LAST;

                    when SET_ROUTER_BYPASS =>
                        sel_router_out <= C_ROUTER_OUT_ENABLE_BYPASS;
                        sel_router     <= C_ROUTER_BYPASS_ENABLE;
                        --axis_if_ground_ram.axis_if.tready <= '1';
                        router_fsm     <= DETECT_LAST;

                    when DETECT_LAST =>
                        --axis_if_ground_ram.axis_if.tready <= '1';

                        if axis_pl_if_bypass_buf_m.axis_if_m.tvalid = '1' and axis_pl_if_bypass_buf_m.axis_if_m.tlast = '1' and axis_pl_if_bypass_buf_s.axis_if_s.tready = '1' then
                            sel_router <= C_ROUTER_BLOCK;
                            if sel_router = C_ROUTER_BYPASS_ENABLE then
                                router_fsm <= DETECT_CACHE_PRESSURE;
                            else
                                router_fsm <= CHECK_RAM_INSERTION_FINISHED;
                            end if;
                            --axis_if_ground_ram.axis_if.tready <= '0';
                        end if;

                    when CHECK_RAM_INSERTION_FINISHED =>
                        -- Ensure insertion process has finished:
                        if req_insertion_ram_if_s.busy = '0' then
                            router_fsm <= DETECT_CACHE_PRESSURE;
                        end if;

                    when DETECT_CACHE_PRESSURE =>
                        sel_router     <= C_ROUTER_BLOCK;
                        sel_router_out <= C_ROUTER_OUT_ENABLE_BYPASS;
                        if axis_payload_if_ram_buf_in_m.axis_if_m.tvalid = '1' then
                            router_fsm <= CACHE_FORWARD;
                        else
                            router_fsm <= INIT;
                        end if;

                    when CACHE_FORWARD =>
                        if axis_payload_if_ram_buf_in_m.axis_if_m.tvalid = '1' and axis_payload_if_ram_buf_in_m.axis_if_m.tlast = '1' and axis_payload_if_ram_buf_in_s.axis_if_s.tready = '1' then
                            router_fsm     <= DETECT_CACHE_PRESSURE;
                            sel_router     <= C_ROUTER_BLOCK;
                            sel_router_out <= C_ROUTER_OUT_ENABLE_BYPASS;
                        else
                            router_fsm     <= CACHE_FORWARD;
                            sel_router     <= C_ROUTER_BLOCK;
                            sel_router_out <= C_ROUTER_OUT_ENABLE_RAM;
                        end if;

                    when ARQ_DISABLED =>
                        sel_router_out <= C_ROUTER_OUT_ENABLE_BYPASS;
                        sel_router     <= C_ROUTER_BYPASS_ENABLE;
                        router_fsm     <= ARQ_DISABLED;

                    when others =>
                        router_fsm <= SET_CONFIG;
                end case;
            end if;
        end if;
    end process;

    -------------------------------------------
    --- Router MUXES:
    -------------------------------------------
    inst_arq_axis_pl_router_out : entity arq_lib.arq_axis_pl_router_2x1
        port map(
            --Input interfaces
            axis_payload_if_in_a_m => axis_bypass_if_out_m, --: inout t_axis_payload_if;
            axis_payload_if_in_a_s => axis_bypass_if_out_s, --: inout t_axis_payload_if;
            axis_payload_if_in_b_m => axis_payload_if_ram_buf_in_m, --: inout t_axis_payload_if;
            axis_payload_if_in_b_s => axis_payload_if_ram_buf_in_s, --: inout t_axis_payload_if;
            sel                  => sel_router_out, --: in    std_logic;
            --Output interfaces
            axis_payload_if_out_m  => axis_router_if_out_m, --: inout t_axis_payload_if;
            axis_payload_if_out_s  => axis_router_if_out_s --: inout t_axis_payload_if;
        );

    inst_arq_axis_pl_router_main : entity arq_lib.arq_axis_pl_router_1x2
        port map(
            --Input interfaces
            axis_payload_if_in_m    => axis_pl_if_bypass_buf_m, -- : inout t_axis_payload_if;
            axis_payload_if_in_s    => axis_pl_if_bypass_buf_s, -- : inout t_axis_payload_if;
            sel                   => (not sel_router(0)), -- : in    std_logic;
            --Output interfaces
            axis_payload_if_out_a_m => axis_router_if_in_bypass_m, -- : inout t_axis_payload_if;
            axis_payload_if_out_a_s => axis_router_if_in_bypass_s, -- : inout t_axis_payload_if;
            axis_payload_if_out_b_m => axis_if_ground_m, -- : inout t_axis_payload_if
            axis_payload_if_out_b_s => axis_if_ground_s -- : inout t_axis_payload_if
        );

    inst_arq_axis_pl_router_bypass : entity arq_lib.arq_axis_pl_router_1x2
        port map(
            --Input interfaces
            axis_payload_if_in_m    => axis_router_if_in_bypass_m, -- : inout t_axis_payload_if;
            axis_payload_if_in_s    => axis_router_if_in_bypass_s, -- : inout t_axis_payload_if;
            sel                   => (not sel_router(1)), -- : in    std_logic;
            --Output interfaces
            axis_payload_if_out_a_m => axis_bypass_if_out_m, -- : inout t_axis_payload_if;
            axis_payload_if_out_a_s => axis_bypass_if_out_s, -- : inout t_axis_payload_if;
            axis_payload_if_out_b_m => axis_payload_if_ram_buf_out_m, -- : inout t_axis_payload_if
            axis_payload_if_out_b_s => axis_payload_if_ram_buf_out_s -- : inout t_axis_payload_if
        );

    -------------------------------------------
    --- FIFO INPUT DUAL BUFFERS 
    -------------------------------------------
    -- Inst_input_buffer_ram : entity sda_lib.axis_payload_fifo
    --     generic map(
    --         DISTR_RAM       => false,
    --         FIFO_DEPTH      => C_FSO_PAYLOAD_SIZE,
    --         FULL_THRESHOLD  => C_FSO_PAYLOAD_SIZE,
    --         EMPTY_THRESHOLD => 0
    --     )
    --     port map(
    --         clk                 => clk,
    --         rst                 => rst,
    --         axis_payload_if_in  => int_axis_pl_if_in,
    --         axis_payload_if_out => axis_pl_if_ram_buf,
    --         input_almost_full   => open,
    --         input_almost_empty  => open,
    --         fifo_max_level      => open
    --     );

    -- Interface assignment to avoid ready signal collision. 
    -- int_axis_pl_if_in.tx_fn      <= axis_payload_if_in.tx_fn;
    -- int_axis_pl_if_in.frame_type <= axis_payload_if_in.frame_type;
    -- int_axis_pl_if_in.tx_fn      <= axis_payload_if_in.tx_fn;

    -- int_axis_pl_if_in.axis_if.tdata  <= axis_payload_if_in.axis_if.tdata;
    -- int_axis_pl_if_in.axis_if.tvalid <= axis_payload_if_in.axis_if.tvalid;
    -- int_axis_pl_if_in.axis_if.tlast  <= axis_payload_if_in.axis_if.tlast;
    -- int_axis_pl_if_in.axis_if.tstart <= axis_payload_if_in.axis_if.tstart;
    -- int_axis_pl_if_in.axis_if.tuser  <= axis_payload_if_in.axis_if.tuser;

    Inst_input_buffer_bypass : entity sda_lib.axis_payload_fifo
        generic map(
            DISTR_RAM       => false,
            FIFO_DEPTH      => C_FSO_PAYLOAD_SIZE,
            FULL_THRESHOLD  => C_FSO_PAYLOAD_SIZE,
            EMPTY_THRESHOLD => 0
        )
        port map(
            clk                 => clk,
            rst                 => rst,
            axis_payload_if_in_m  => axis_payload_if_in_m,
            axis_payload_if_in_s  => axis_payload_if_in_s,
            axis_payload_if_out_m => axis_pl_if_bypass_buf_m,
            axis_payload_if_out_s => axis_pl_if_bypass_buf_s,
            input_almost_full   => open,
            input_almost_empty  => open,
            fifo_max_level      => open
        );

    -------------------------------------------
    --- FIFO OUTPUT BUFFERS 
    -------------------------------------------
    Inst_output_buffer_ram_out : entity sda_lib.axis_payload_fifo
        generic map(
            DISTR_RAM       => false,
            FIFO_DEPTH      => C_FSO_PAYLOAD_SIZE,
            FULL_THRESHOLD  => C_FSO_PAYLOAD_SIZE,
            EMPTY_THRESHOLD => 0
        )
        port map(
            clk                 => clk,
            rst                 => rst,
            axis_payload_if_in_m  => axis_payload_if_ram_buf_out_m,
            axis_payload_if_in_s  => axis_payload_if_ram_buf_out_s,
            axis_payload_if_out_m => axis_payload_if_ram_out_m,
            axis_payload_if_out_s => axis_payload_if_ram_out_s,
            input_almost_full   => open,
            input_almost_empty  => open,
            fifo_max_level      => open
        );

    Inst_output_buffer_ram_in : entity sda_lib.axis_payload_fifo
        generic map(
            DISTR_RAM       => true,
            FIFO_DEPTH      => C_FSO_PAYLOAD_SIZE,
            FULL_THRESHOLD  => C_FSO_PAYLOAD_SIZE,
            EMPTY_THRESHOLD => 0
        )
        port map(
            clk                 => clk,
            rst                 => rst,
            axis_payload_if_in_m  => axis_payload_if_ram_in_m,
            axis_payload_if_in_s  => axis_payload_if_ram_in_s,
            axis_payload_if_out_m => axis_payload_if_ram_buf_in_m,
            axis_payload_if_out_s => axis_payload_if_ram_buf_in_s,
            input_almost_full   => open,
            input_almost_empty  => open,
            fifo_max_level      => open
        );

    Inst_output_buffer : entity sda_lib.axis_payload_fifo
        generic map(
            DISTR_RAM       => true,
            FIFO_DEPTH      => C_FSO_PAYLOAD_SIZE * 10,
            FULL_THRESHOLD  => C_FSO_PAYLOAD_SIZE * 6,
            EMPTY_THRESHOLD => 0
        )
        port map(
            clk                 => clk,
            rst                 => rst,
            -- Input interfaces:
            axis_payload_if_in_m  => axis_router_if_out_m,
            axis_payload_if_in_s  => axis_router_if_out_s,
            -- Output interface:
            axis_payload_if_out_m => axis_payload_if_out_m,
            axis_payload_if_out_s => axis_payload_if_out_s,
            input_almost_full   => int_back_pressure,
            input_almost_empty  => open,
            fifo_max_level      => open
        );

end architecture RTL;
