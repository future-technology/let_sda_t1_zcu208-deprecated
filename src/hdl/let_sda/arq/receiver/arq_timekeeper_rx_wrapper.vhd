-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: arq_request_control_transmitter
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Converts Ethernet packets to SDA Standard FSO frames in AXI-Stream format.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- TODO: The ACK fifo needs to monitor the data and have a timeout.
-- if the ack is not asserted with "ready" after some time, drop it.
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.or_reduce;
library arq_lib;
library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;
library share_let_1g;
use share_let_1g.pkg_support.all;
--library arq_lib;
--use arq_lib.arq_req_control_support_pkg.all;

entity arq_timekeeper_rx_wrapper is
    generic(
        G_MEMORY_BLOCK_SIZE          : natural := 256; -- A low value increases performance at a huge resource cost. Must be power of 2.
        G_CORE_COUNTER_PERIOD_CLOCKS : natural := 100
    );
    port(
        clk                  : in    std_logic;
        rst                  : in    std_logic;
        --Input interfaces:
        arq_config_if        : in    t_arq_config_if;
        ack_data_if          : inout t_ack_data_if;
        req_insertion_if     : inout t_request_insertion_if;
        --Output interfaces;
        tx_fn_ref            : out std_logic_vector(C_TX_FN_WIDTH-1 downto 0);
        req_resend_if        : inout t_request_resend_if;
        req_cache_if         : inout   t_request_cache_if;
        metadata_retrieve_if : out   t_metadata_retrieve_if;
        arq_hw_config        : out   t_arq_hw_config;
        diag_if              : inout t_arq_control_tx_diagnostics_if
    );
end entity arq_timekeeper_rx_wrapper;

architecture RTL of arq_timekeeper_rx_wrapper is

    constant C_BLOCK_NUM       : natural := (C_ARQ_HOLDOFF_NFRAMES_MAX / G_MEMORY_BLOCK_SIZE); -- in uS--SDA T1 req: 2500 ms max time for holdoff 
    constant C_MAX_ACK_TIMEOUT : natural := G_MEMORY_BLOCK_SIZE + 1;

    -- Reference Frame number for receiving:
    signal int_tx_fn_ref : std_logic_vector(C_TX_FN_WIDTH-1 downto 0);

    signal timer_block_index : integer := 0;
    signal block_valid_flags : std_logic_vector(64 - 1 downto 0);
    signal block_full_vector : std_logic_vector(C_BLOCK_NUM - 1 downto 0);
    signal frame_bypassed_vector : std_logic_vector(C_BLOCK_NUM - 1 downto 0);
    signal frame_found : std_logic_vector(C_BLOCK_NUM - 1 downto 0);
    signal seek_frame           : std_logic_vector(C_TX_FN_WIDTH-1 downto 0);
    signal seek_frame_valid     : std_logic;

    --THis array stores the valid flag of each timer entry.
    type t_valid_flag_array is array (0 to (C_ARQ_HOLDOFF_NFRAMES_MAX - 1)) of std_logic;
    signal valid_flag_array : t_valid_flag_array;

    type t_fsm_ack is (WAIT_UNTIL_TIMEOUT, DROP_ACK);
    signal fsm_ack       : t_fsm_ack;
    type t_fsm_insertion is (WAIT_UNTIL_VALID, SEEK_FREE_BLOCK, INSERT, END_INSERTION);
    signal fsm_insertion : t_fsm_insertion;

    signal request_resend_data_out  : std_logic_vector(C_ADDRESS_WIDTH + C_TX_FN_WIDTH + C_TX_NUM_WIDTH + C_FRAME_TYPE_WIDTH - 1 downto 0);
    signal ack_data_in              : std_logic_vector(16 + 1 - 1 downto 0);
    signal ack_data_out             : std_logic_vector(16 + 1 - 1 downto 0);
    signal request_insertion_if_buf : t_request_insertion_if;
    type t_req_insertion_if_array is array (0 to (C_BLOCK_NUM - 1)) of t_request_insertion_if;
    signal req_insertion_if_array   : t_req_insertion_if_array;
    signal req_insertion_if_ready   : std_logic_vector(C_BLOCK_NUM - 1 downto 0);
    signal req_insertion_if_valid   : std_logic_vector(C_BLOCK_NUM - 1 downto 0);

    type t_metadata_retrieve_array is array (0 to (C_BLOCK_NUM - 1)) of t_metadata_retrieve_if;
    signal metadata_retrieve_array : t_metadata_retrieve_array;

    signal request_resend_if_array_out : t_request_resend_if;
    signal ack_data_if_buf             : t_ack_data_if;
    type t_ack_data_if_buf_array is array (0 to (C_BLOCK_NUM - 1)) of t_ack_data_if;
    signal ack_data_if_buf_array       : t_ack_data_if_buf_array;
    signal ack_ready_vector            : std_logic_vector(C_BLOCK_NUM downto 0);
    signal ack_ready_result            : std_logic;
    signal timeout_ack_ready           : std_logic;
    signal block_index                 : natural;
    --Array of buses for output FIFO buffers
    -- Stage 1: 
    type t_request_resend_if_array is array (0 to (C_BLOCK_NUM - 1)) of t_request_resend_if;
    signal request_resend_if_array_in  : t_request_resend_if_array;
    type t_request_resend_buf_array is array (0 to (C_BLOCK_NUM - 1)) of t_request_resend_buf_if;
    signal request_resend_buf_array    : t_request_resend_buf_array;

    type t_request_resend_data_in_array is array (0 to (C_BLOCK_NUM - 1)) of std_logic_vector(C_ADDRESS_WIDTH + C_TX_FN_WIDTH + C_TX_NUM_WIDTH + C_FRAME_TYPE_WIDTH - 1 downto 0);
    signal request_resend_data_in_array : t_request_resend_data_in_array;

    type t_request_resend_buf_subarray is array (0 to (C_BLOCK_NUM / 8) - 1) of t_request_resend_buf_if;
    type t_request_resend_buf_array_2d is array (0 to 8 - 1) of t_request_resend_buf_subarray;
    signal request_resend_buf_array_2d : t_request_resend_buf_array_2d;

    -- Stage 2:
    type t_request_resend_buf2_array is array (0 to 8 - 1) of t_request_resend_buf_if;
    signal request_resend_buf2_array_in  : t_request_resend_buf2_array;
    signal request_resend_buf2_array_out : t_request_resend_buf2_array;

    -- Stage 3:
    signal request_resend_if_buf : t_request_resend_buf_if;

    -- CACHE:
    --type t_request_cache_bus_if is array (0 to G_MEMORY_BLOCK_SIZE-1) of t_request_cache_if;
    -- Stage 1:
    type t_request_cache_if_array is array (0 to (C_BLOCK_NUM - 1)) of t_request_cache_if;
    signal request_cache_if_array_in  : t_request_cache_if_array ;
    type t_request_cache_buf_array is array (0 to (C_BLOCK_NUM - 1)) of t_request_cache_if;
    signal request_cache_buf_array    : t_request_cache_buf_array ;

    -- Stage 2:
    type t_request_cache_buf2_array is array (0 to 8 - 1) of t_request_cache_if;
    signal request_cache_buf2_array_in  : t_request_cache_buf2_array;
    signal request_cache_buf2_array_out : t_request_cache_buf2_array;

    type t_request_cache_buf_subarray is array (0 to (C_BLOCK_NUM / 8) - 1) of t_request_cache_if;
    type t_request_cache_buf_array_2d is array (0 to 8 - 1) of t_request_cache_buf_subarray;
    signal request_cache_buf_array_2d : t_request_cache_buf_array_2d;
    
    -- Stage 3:
    signal request_cache_if_buf : t_request_cache_if;

    -- Counters for FIFO MUXes
    subtype t_mux1_sel is natural range 0 to (C_BLOCK_NUM / 8) - 1;
    subtype t_mux2_sel is natural range 0 to (8 - 1);

    signal mux1_sel : t_mux1_sel;
    signal mux2_sel : t_mux2_sel;

    signal reset_core_counter : std_logic;
    signal r_clear_timeout    : std_logic;

    type t_diag_if_array is array (0 to (C_BLOCK_NUM) - 1) of t_arq_control_block_tx_diagnostics_if;
    signal diag_if_array   : t_diag_if_array;
    signal r_diag_if_array : t_diag_if_array;
    signal result_diag_if  : t_arq_control_tx_diagnostics_if;

    subtype t_diag_index is natural range 0 to C_BLOCK_NUM + 1;
    signal diag_index : t_diag_index;

    ---- signals for Port A
    --signal ram_data_A_tx_fn : std_logic_vector(16-1 downto 0);
    --signal ram_data_A_tx_num : std_logic_vector(3-1 downto 0);
    --signal ram_addr_A : std_logic_vector(no_bits_natural(C_ARQ_HOLDOFF_NFRAMES_MAX - 1) - 1 downto 0);
    --signal ram_wr_ena : std_logic;
    --signal ram_ena_A : std_logic;
    ---- signals for Port B
    --signal ram_data_B_tx_fn : std_logic_vector(16-1 downto 0);
    --signal ram_data_B_tx_num : std_logic_vector(3-1 downto 0);
    --signal ram_addr_B : std_logic_vector(no_bits_natural(C_ARQ_HOLDOFF_NFRAMES_MAX - 1) - 1 downto 0);
    --signal ram_ena_B : std_logic;

begin

    arq_hw_config.rd_memory_block_size   <= std_logic_vector(to_unsigned(G_MEMORY_BLOCK_SIZE, 12));
    arq_hw_config.rd_core_counter_period <= std_logic_vector(to_unsigned(G_CORE_COUNTER_PERIOD_CLOCKS, 8));

    -------------------------------------------
    --- PROCESS TO INSERT A NEW ENTRY
    -------------------------------------------
    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                block_index                    <= 0;
                request_insertion_if_buf.ready <= '0';

                for ii in 0 to (C_BLOCK_NUM - 1) loop
                    req_insertion_if_valid(ii) <= '0';
                end loop;
                metadata_retrieve_if <= C_METADATA_RETRIEVE_INIT;
            else

                --default:
                metadata_retrieve_if.valid <= '0';

                -- FSM: If ACK ready is not asserted by the Blocks, this 
                -- fsm drop the ACK when timeout.
                case fsm_insertion is
                    when SEEK_FREE_BLOCK =>
                        request_insertion_if_buf.ready      <= '0';
                        req_insertion_if_valid(block_index) <= '0';
                        if block_full_vector(block_index) = '0' then
                            fsm_insertion <= WAIT_UNTIL_VALID;
                        else
                            if block_index = C_BLOCK_NUM - 1 then
                                block_index <= 0;
                            else
                                block_index <= block_index + 1;
                            end if;
                            fsm_insertion <= SEEK_FREE_BLOCK;
                        end if;

                    when WAIT_UNTIL_VALID =>
                        request_insertion_if_buf.ready      <= '0';
                        req_insertion_if_valid(block_index) <= '0';
                        if request_insertion_if_buf.valid = '1' and block_full_vector(block_index) = '0' then
                            fsm_insertion <= INSERT;
                        else
                            fsm_insertion <= SEEK_FREE_BLOCK;
                        end if;

                    when INSERT =>
                        if req_insertion_if_ready(block_index) = '1' then
                            req_insertion_if_valid(block_index) <= request_insertion_if_buf.valid;
                            request_insertion_if_buf.ready      <= '1';
                            fsm_insertion                       <= END_INSERTION;
                        else
                            req_insertion_if_valid(block_index) <= request_insertion_if_buf.valid;
                            request_insertion_if_buf.ready      <= '0';
                            fsm_insertion                       <= INSERT;
                        end if;
                    --else
                    --    request_insertion_if_buf.ready   <= '0';
                    --    req_insertion_if_valid(block_index) <= '0';
                    --    fsm_insertion <= SEEK_FREE_BLOCK;
                    --end if;

                    when END_INSERTION =>
                        if metadata_retrieve_array(block_index).valid = '1' then
                            fsm_insertion        <= SEEK_FREE_BLOCK;
                            metadata_retrieve_if <= metadata_retrieve_array(block_index);
                        end if;
                        req_insertion_if_valid(block_index) <= '0';
                        request_insertion_if_buf.ready      <= '0';

                    when others =>
                        fsm_insertion <= SEEK_FREE_BLOCK;
                end case;
            end if;
        end if;
    end process;

    -------------------------------------------
    --- ACK TIMEOUT CONTROL
    -------------------------------------------
    process(clk)
        variable v_counter : natural := 0;
        variable v_ack_fn  : std_logic_vector(C_TX_FN_WIDTH - 1 downto 0);
    begin
        if rising_edge(clk) then
            if rst = '1' then
                timeout_ack_ready <= '0';
                fsm_ack           <= WAIT_UNTIL_TIMEOUT;
                v_counter         := 0;
            else
                -- FSM: If ACK ready is not asserted by the Blocks, this 
                -- fsm drop the ACK when timeout.
                case fsm_ack is
                    when WAIT_UNTIL_TIMEOUT =>
                        timeout_ack_ready <= '0';

                        if ack_data_if_buf.ack_start_fn = v_ack_fn and ack_data_if_buf.ack_valid = '1' then
                            v_counter := v_counter + 1;
                            if v_counter > C_MAX_ACK_TIMEOUT then
                                fsm_ack   <= DROP_ACK;
                                v_counter := 0;
                            end if;
                        else
                            v_counter := 0;
                        end if;
                        v_ack_fn := ack_data_if_buf.ack_start_fn;
                    when DROP_ACK =>
                        timeout_ack_ready <= '1';
                        fsm_ack           <= WAIT_UNTIL_TIMEOUT;

                    when others =>
                        fsm_ack <= WAIT_UNTIL_TIMEOUT;
                end case;
            end if;
        end if;
    end process;

    -------------------------------------------
    --- ASYNC connections
    -------------------------------------------
    -- Conversions for ACK bus IF
    ack_data_in                  <= ack_data_if_data2slv(ack_data_if);
    ack_data_if_buf.ack_start_fn <= ack_data_if_slv2ack_start_fn(ack_data_out);
    ack_data_if_buf.ack          <= ack_data_if_slv2ack(ack_data_out);
    ack_data_if_buf.ack_span     <= (others => '0'); --ack_data_if_slv2ack_span(ack_data_out);   

    -- Conversions for Req Resend bus IF
    req_resend_if.tx_fn         <= request_resend_if_slv2tx_fn(request_resend_data_out);
    req_resend_if.tx_num        <= request_resend_if_slv2tx_num(request_resend_data_out);
    req_resend_if.addr          <= request_resend_if_slv2addr(request_resend_data_out);
    req_resend_if.frame_type    <= request_resend_if_slv2frame_type(request_resend_data_out);
    --request_resend_data_in      <= request_resend_if_data2slv(request_resend_if_buf);

    -- All ready signals for ACK FIFO output are or'ed. This allow that when there is an
    -- ACK match, it is asserted by the block that got the match, so the others dont waste time,
    -- seeking for the tx_fn. There is an extra ready line controlled by the FSM for timeout.

    ack_ready_result <= or_reduce(ack_ready_vector);

    process(all)
    begin
        g_async_logic : for ii in 0 to ((C_BLOCK_NUM) - 1) loop
            req_insertion_if_array(ii).ready        <= 'Z';
            diag_if.clear_all                       <= 'Z';
            ack_data_if_buf_array(ii).rd_ena        <= 'Z';
            req_insertion_if_ready(ii)              <= req_insertion_if_array(ii).ready;
            ack_data_if_buf_array(ii).ack_valid     <= ack_data_if_buf.ack_valid;
            ack_data_if_buf_array(ii).ack_start_fn  <= ack_data_if_buf.ack_start_fn;
            ack_data_if_buf_array(ii).ack           <= ack_data_if_buf.ack;
            ack_ready_vector(ii)                    <= ack_data_if_buf_array(ii).rd_ena;
            req_insertion_if_array(ii).tx_fn        <= request_insertion_if_buf.tx_fn;
            req_insertion_if_array(ii).frame_type   <= request_insertion_if_buf.frame_type;
            req_insertion_if_array(ii).valid        <= req_insertion_if_valid(ii);
            diag_if_array(ii).clear_all             <= diag_if.clear_all;
            diag_if_array(ii).retx_counter          <= (others => 'Z');
            diag_if_array(ii).insertion_req_counter <= (others => 'Z');
            diag_if_array(ii).cache_req_counter     <= (others => 'Z');
            diag_if_array(ii).acked_counter         <= (others => 'Z');
            diag_if_array(ii).max_retx_counter      <= (others => 'Z');
        end loop;
        ack_ready_vector(C_BLOCK_NUM) <= timeout_ack_ready;
    end process;

    g_timer_blocks : for ii in 0 to ((C_BLOCK_NUM) - 1) generate
        -------------------------------------------
        --- TIMER BLOCKS INST
        -------------------------------------------
        inst_timer_block : entity arq_lib.arq_timekeeper_rx
            generic map(
                G_MEMORY_BLOCK_SIZE          => G_MEMORY_BLOCK_SIZE, --: natural := 64; -- A low value increases performance at a huge resource cost. Must be power of 2.
                G_CORE_COUNTER_PERIOD_CLOCKS => G_CORE_COUNTER_PERIOD_CLOCKS, --: natural := 100
                G_BLOCK_NUMBER               => ii
            )
            port map(
                clk                  => clk, --: in std_logic;
                rst                  => rst, --: in std_logic;

                --Input interfaces:
                arq_config_if        => arq_config_if, --: in t_arq_config_if;
                ack_data_if          => ack_data_if_buf_array(ii), --: inout  t_ack_data_if;
                req_insertion_if     => req_insertion_if_array(ii), --: inout t_request_insertion_if;
                tx_fn_ref            => int_tx_fn_ref,
                seek_frame           => seek_frame, --: in std_logic_vector(C_TX_FN_WIDTH-1 downto 0);
                seek_frame_valid     => seek_frame_valid, --: in std_logic;
                --frame_bypassed       => , --: in std_logic; -- this tells timekeeper to increase the tx_fn_ref
                
                --Output interfaces:
                frame_found          => frame_found(ii), --: out std_logic; -- assert when frame seek is found
                --frame_bypassed       => frame_bypassed_vector(ii), --: in std_logic; -- this tells timekeeper to increase the tx_fn_ref
                req_resend_if        => request_resend_if_array_in(ii), --: inout t_request_resend_if;
                req_cache_if         => request_cache_if_array_in(ii), --request_cache_if, --: inout t_request_cache_if;
                metadata_retrieve_if => metadata_retrieve_array(ii),
                diag_if              => diag_if_array(ii), --diag_if --: inout t_arq_control_tx_diagnostics_if
                block_full           => block_full_vector(ii)
            );
    end generate;

    -------------------------------------------
    --- ACK
    -------------------------------------------
    -- Conversions for ACK bus IF
    ack_data_if_buf.ack_start_fn <= ack_data_if_slv2ack_start_fn(ack_data_out);
    ack_data_if_buf.ack          <= ack_data_if_slv2ack(ack_data_out);
    --ack_data_if_buf.ack_span      <= ack_data_if_slv2ack_span(ack_data_out_array); 

    -- FIFO input for ACK
    inst_ack_buffer_in : entity share_let_1g.sync_fifo
        generic map(
            DISTR_RAM       => true,
            FIFO_DEPTH      => 32,
            DATA_WIDTH      => 16 + 1,  --tx_fn+: 16 bits
            FULL_THRESHOLD  => 32,
            EMPTY_THRESHOLD => 0
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => ack_data_in,
            input_valid        => ack_data_if.ack_valid,
            input_ready        => ack_data_if.rd_ena,
            input_almost_full  => open, -- TODO: use this port to inform
            input_almost_empty => open,
            fifo_max_level     => open,
            fifo_data_count    => open,
            -- Output data handling
            -----------------------
            output             => ack_data_out,
            output_valid       => ack_data_if_buf.ack_valid,
            output_ready       => ack_ready_result --ack_data_if_buf.rd_ena 
        );

    -- FIFO input for insertion request
    inst_insertion_req_buffer_in : entity share_let_1g.sync_fifo
        generic map(
            DISTR_RAM       => true,
            FIFO_DEPTH      => 32,
            DATA_WIDTH      => 16,      --tx_fn: 16 bits
            FULL_THRESHOLD  => 32,
            EMPTY_THRESHOLD => 0
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => req_insertion_if.tx_fn,
            input_valid        => req_insertion_if.valid,
            input_ready        => req_insertion_if.ready,
            input_almost_full  => open,
            input_almost_empty => open,
            fifo_max_level     => open,
            fifo_data_count    => open,
            -- Output data handling
            -----------------------
            output             => request_insertion_if_buf.tx_fn,
            output_valid       => request_insertion_if_buf.valid,
            output_ready       => request_insertion_if_buf.ready -- to sync with the payload
        );
    -- FIFO input for insertion request
    inst_insertion_req_frame_type_buffer_in : entity share_let_1g.sync_fifo
        generic map(
            DISTR_RAM       => true,
            FIFO_DEPTH      => 32,
            DATA_WIDTH      => C_FRAME_TYPE_WIDTH,      --tx_fn: 16 bits
            FULL_THRESHOLD  => 32,
            EMPTY_THRESHOLD => 0
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => req_insertion_if.frame_type,
            input_valid        => req_insertion_if.valid,
            input_ready        => open,
            input_almost_full  => open,
            input_almost_empty => open,
            fifo_max_level     => open,
            fifo_data_count    => open,
            -- Output data handling
            -----------------------
            output             => request_insertion_if_buf.frame_type,
            output_valid       => open, --request_insertion_if_buf.valid,
            output_ready       => request_insertion_if_buf.ready -- to sync with the payload
        );

    -- FIFO output for insertion request
    inst_resend_req_buffer_out : entity share_let_1g.sync_fifo
        generic map(
            DISTR_RAM       => true,
            FIFO_DEPTH      => 64,
            DATA_WIDTH      => C_ADDRESS_WIDTH + C_TX_FN_WIDTH + C_TX_NUM_WIDTH + C_FRAME_TYPE_WIDTH, --tx_fn: 16b, tx_num: 3b
            FULL_THRESHOLD  => 64,
            EMPTY_THRESHOLD => 0
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => request_resend_if_buf.data,
            input_valid        => request_resend_if_buf.valid,
            input_ready        => request_resend_if_buf.ready,
            input_almost_full  => open,
            input_almost_empty => req_resend_if.pending_request_n,
            fifo_max_level     => open,
            fifo_data_count    => open,
            -- Output data handling
            -----------------------
            output             => request_resend_data_out,
            output_valid       => req_resend_if.valid,
            output_ready       => req_resend_if.ready -- to sync with the payload
        );

    -- FIFO output for cache request
    inst_resend_req_buffer_txfn_out : entity share_let_1g.sync_fifo
        generic map(
            DISTR_RAM       => true,
            FIFO_DEPTH      => 64,
            DATA_WIDTH      => C_TX_FN_WIDTH,
            FULL_THRESHOLD  => 64,
            EMPTY_THRESHOLD => 0
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => request_cache_if_buf.tx_fn,
            input_valid        => request_cache_if_buf.valid,
            input_ready        => request_cache_if_buf.ready,
            input_almost_full  => open,
            input_almost_empty => open, --req_resend_if.pending_request_n,
            fifo_max_level     => open,
            fifo_data_count    => open,
            -- Output data handling
            -----------------------
            output             => req_cache_if.tx_fn, --request_resend_data_out,
            output_valid       => req_cache_if.valid,
            output_ready       => req_cache_if.ready -- to sync with the payload
        );

    -- FIFO output for cache request
    inst_resend_req_buffer_addr_out : entity share_let_1g.sync_fifo
        generic map(
            DISTR_RAM       => true,
            FIFO_DEPTH      => 64,
            DATA_WIDTH      => C_ADDRESS_WIDTH,
            FULL_THRESHOLD  => 64,
            EMPTY_THRESHOLD => 0
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => request_cache_if_buf.addr,
            input_valid        => request_cache_if_buf.valid,
            input_ready        => open, --request_cache_if_buf.ready,
            input_almost_full  => open,
            input_almost_empty => open, --req_resend_if.pending_request_n,
            fifo_max_level     => open,
            fifo_data_count    => open,
            -- Output data handling
            -----------------------
            output             => req_cache_if.addr, --request_resend_data_out,
            output_valid       => open, --req_cache_if.valid,
            output_ready       => req_cache_if.ready -- to sync with the payload
        );

    -------------------------------------------
    --- FIFO array generator
    -------------------------------------------
    g_prebuffer_fifos : for ii in 0 to ((C_BLOCK_NUM) - 1) generate

        request_resend_data_in_array(ii) <= request_resend_if_data2slv(request_resend_if_array_in(ii));

        -- Pre FIFO output for resend request
        inst_resend_req_prebuffer_out : entity share_let_1g.sync_fifo
            generic map(
                DISTR_RAM       => true,
                FIFO_DEPTH      => 16,
                DATA_WIDTH      => C_ADDRESS_WIDTH + C_TX_FN_WIDTH + C_TX_NUM_WIDTH + C_FRAME_TYPE_WIDTH, --tx_fn: 16b, tx_num: 3b
                FULL_THRESHOLD  => 16,
                EMPTY_THRESHOLD => 0
            )
            port map(
                clk                => clk,
                rst                => rst,
                -- Input data handling
                ----------------------
                input              => request_resend_data_in_array(ii),
                input_valid        => request_resend_if_array_in(ii).valid,
                input_ready        => request_resend_if_array_in(ii).ready,
                input_almost_full  => open,
                input_almost_empty => request_resend_buf_array(ii).pending_request_n,
                fifo_max_level     => open,
                fifo_data_count    => open,
                -- Output data handling
                -----------------------
                output             => request_resend_buf_array(ii).data,
                output_valid       => request_resend_buf_array(ii).valid,
                output_ready       => request_resend_buf_array(ii).ready -- to sync with the payload
            );

        -- Pre FIFO output for cache request
        inst_cache_req_buffer_txfn_out : entity share_let_1g.sync_fifo
            generic map(
                DISTR_RAM         => false,
                FIFO_DEPTH        => 16,
                DATA_WIDTH        => C_TX_FN_WIDTH, --tx_fn: 16b
                FULL_THRESHOLD    => 16,
                EMPTY_THRESHOLD   => 0
            )
            port map(
    
                clk            => clk,
                rst            => rst,
    
                -- Input data handling
                ----------------------
                input             => request_cache_if_array_in(ii).tx_fn,
                input_valid       => request_cache_if_array_in(ii).valid,
                input_ready        => request_cache_if_array_in(ii).ready,
                input_almost_full  => open,
                input_almost_empty => open, --request_cache_buf_array(ii).pending_request_n,
                fifo_max_level     => open,
                fifo_data_count    => open,   
                -- Output data handling
                -----------------------
                output          => request_cache_buf_array(ii).tx_fn,
                output_valid    => request_cache_buf_array(ii).valid,
                output_ready    => request_cache_buf_array(ii).ready -- to sync with the payload
            );

        -- Pre FIFO output for cache request
        inst_cache_req_buffer_addr_out : entity share_let_1g.sync_fifo
            generic map(
                DISTR_RAM         => false,
                FIFO_DEPTH        => 16,
                DATA_WIDTH        => C_ADDRESS_WIDTH,
                FULL_THRESHOLD    => 16,
                EMPTY_THRESHOLD   =>0
            )
            port map(
    
                clk            => clk,
                rst            => rst,
    
                -- Input data handling
                ----------------------
                input             => request_cache_if_array_in(ii).addr,
                input_valid       => request_cache_if_array_in(ii).valid,
                input_ready        => open, --request_cache_if_array_in(ii).ready,
                input_almost_full  => open,
                input_almost_empty => open, -- request_cache_buf_array(ii).pending_request_n,
                fifo_max_level     => open,
                fifo_data_count    => open,   
                -- Output data handling
                -----------------------
                output          => request_cache_buf_array(ii).addr,
                output_valid    => open, --request_cache_buf_array(ii).valid,
                output_ready    => request_cache_buf_array(ii).ready -- to sync with the payload
            );

    end generate g_prebuffer_fifos;

    g_prebuffer2_fifos : for ii in 0 to (8 - 1) generate

        --request_resend_data_in_array(ii) <= request_resend_if_data2slv(request_resend_if_array_in(ii));

        -- Pre FIFO output for insertion request
        inst_resend_req_prebuffer2_out : entity share_let_1g.sync_fifo
            generic map(
                DISTR_RAM       => true,
                FIFO_DEPTH      => 16,
                DATA_WIDTH      => C_ADDRESS_WIDTH + C_TX_FN_WIDTH + C_TX_NUM_WIDTH + C_FRAME_TYPE_WIDTH, --tx_fn: 16b, tx_num: 3b
                FULL_THRESHOLD  => 16,
                EMPTY_THRESHOLD => 0
            )
            port map(
                clk                => clk,
                rst                => rst,
                -- Input data handling
                ----------------------
                input              => request_resend_buf2_array_in(ii).data,
                input_valid        => request_resend_buf2_array_in(ii).valid,
                input_ready        => request_resend_buf2_array_in(ii).ready,
                input_almost_full  => open,
                input_almost_empty => request_resend_buf2_array_out(ii).pending_request_n,
                fifo_max_level     => open,
                fifo_data_count    => open,
                -- Output data handling
                -----------------------
                output             => request_resend_buf2_array_out(ii).data,
                output_valid       => request_resend_buf2_array_out(ii).valid,
                output_ready       => request_resend_buf2_array_out(ii).ready -- to sync with the payload
            );

        -- Pre FIFO output for cache request tx_fn
        inst_cache_req_prebuffer2_txfn_out : entity share_let_1g.sync_fifo
            generic map(
                DISTR_RAM       => true,
                FIFO_DEPTH      => 16,
                DATA_WIDTH      => C_TX_FN_WIDTH, 
                FULL_THRESHOLD  => 16,
                EMPTY_THRESHOLD => 0
            )
            port map(
                clk                => clk,
                rst                => rst,
                -- Input data handling
                ----------------------
                input              => request_cache_buf2_array_in(ii).tx_fn,
                input_valid        => request_cache_buf2_array_in(ii).valid,
                input_ready        => request_cache_buf2_array_in(ii).ready,
                input_almost_full  => open,
                input_almost_empty => open, --request_cache_buf2_array_out(ii).pending_request_n,
                fifo_max_level     => open,
                fifo_data_count    => open,
                -- Output data handling
                -----------------------
                output             => request_cache_buf2_array_out(ii).tx_fn,
                output_valid       => request_cache_buf2_array_out(ii).valid,
                output_ready       => request_cache_buf2_array_out(ii).ready -- to sync with the payload
            );

        -- Pre FIFO output for cache request address
        inst_cache_req_prebuffer2_addr_out : entity share_let_1g.sync_fifo
            generic map(
                DISTR_RAM       => true,
                FIFO_DEPTH      => 16,
                DATA_WIDTH      => C_ADDRESS_WIDTH,
                FULL_THRESHOLD  => 16,
                EMPTY_THRESHOLD => 0
            )
            port map(
                clk                => clk,
                rst                => rst,
                -- Input data handling
                ----------------------
                input              => request_cache_buf2_array_in(ii).addr,
                input_valid        => request_cache_buf2_array_in(ii).valid,
                input_ready        => open, --request_resend_buf2_array_in(ii).ready,
                input_almost_full  => open,
                input_almost_empty => open, --request_resend_buf2_array_out(ii).pending_request_n,
                fifo_max_level     => open,
                fifo_data_count    => open,
                -- Output data handling
                -----------------------
                output             => request_cache_buf2_array_out(ii).addr,
                output_valid       => open, --request_resend_buf2_array_out(ii).valid,
                output_ready       => request_cache_buf2_array_out(ii).ready -- to sync with the payload
            );
    end generate g_prebuffer2_fifos;

    -------------------------------------------
    --- FIFO MUX ASYNC LOGIC
    -------------------------------------------
    --request_resend_if_buf.data              <= request_resend_buf_array(mux_sel).data;
    --request_resend_if_buf.valid             <= request_resend_buf_array(mux_sel).valid;

    --g_mux_ready: for ii in 0 to (G_MEMORY_BLOCK_SIZE/8)-1 generate
    --    request_resend_buf2_array_out(ii).ready <= request_resend_buf2_array_in.ready when mux_sel = ii else '0';
    --end generate g_mux_ready;

    g_mux_stage2 : for ii in 0 to 8 - 1 generate
        request_resend_if_buf.data              <= request_resend_buf2_array_out(mux2_sel).data;
        request_resend_if_buf.valid             <= request_resend_buf2_array_out(mux2_sel).valid;
        request_resend_buf2_array_out(ii).ready <= request_resend_if_buf.ready when mux2_sel = ii else '0';
        request_resend_buf2_array_in(ii).data   <= request_resend_buf_array_2d(ii)(mux1_sel).data;
        request_resend_buf2_array_in(ii).valid  <= request_resend_buf_array_2d(ii)(mux1_sel).valid;
        g_mux_stage1 : for jj in 0 to (C_BLOCK_NUM / 8) - 1 generate
            request_resend_buf_array_2d(ii)(jj).ready     <= request_resend_buf2_array_in(ii).ready when mux1_sel = jj else '0';
            --request_resend_buf_array_out((ii*8)+jj).ready <= request_resend_buf2_array_in.ready when mux1_sel = ii else '0';
            request_resend_buf_array_2d(ii)(jj).data      <= request_resend_buf_array((jj * 8) + ii).data;
            request_resend_buf_array_2d(ii)(jj).valid     <= request_resend_buf_array((jj * 8) + ii).valid;
            request_resend_buf_array((jj * 8) + ii).ready <= request_resend_buf_array_2d(ii)(jj).ready;
        end generate g_mux_stage1;
    end generate g_mux_stage2;

    g_mux_stage2_cache : for ii in 0 to 8 - 1 generate
        request_cache_if_buf.tx_fn             <= request_cache_buf2_array_out(mux2_sel).tx_fn;
        request_cache_if_buf.addr              <= request_cache_buf2_array_out(mux2_sel).addr;
        request_cache_if_buf.valid             <= request_cache_buf2_array_out(mux2_sel).valid;
        request_cache_buf2_array_out(ii).ready <= request_cache_if_buf.ready when mux2_sel = ii else '0';
        request_cache_buf2_array_in(ii).tx_fn  <= request_cache_buf_array_2d(ii)(mux1_sel).tx_fn;
        request_cache_buf2_array_in(ii).addr   <= request_cache_buf_array_2d(ii)(mux1_sel).addr;
        request_cache_buf2_array_in(ii).valid  <= request_cache_buf_array_2d(ii)(mux1_sel).valid;
        g_mux_stage1_cache : for jj in 0 to (C_BLOCK_NUM / 8) - 1 generate
            request_cache_buf_array_2d(ii)(jj).ready     <= request_cache_buf2_array_in(ii).ready when mux1_sel = jj else '0';
            request_cache_buf_array_2d(ii)(jj).tx_fn     <= request_cache_buf_array((jj * 8) + ii).tx_fn;
            request_cache_buf_array_2d(ii)(jj).addr      <= request_cache_buf_array((jj * 8) + ii).addr;
            request_cache_buf_array_2d(ii)(jj).valid     <= request_cache_buf_array((jj * 8) + ii).valid;
            request_cache_buf_array((jj * 8) + ii).ready <= request_cache_buf_array_2d(ii)(jj).ready;
        end generate g_mux_stage1_cache;
    end generate g_mux_stage2_cache;

    -------------------------------------------
    --- FIFO MUX CONTROL
    -------------------------------------------
    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                mux1_sel <= 0;
                mux2_sel <= 0;
            else
                if mux1_sel = (t_mux1_sel'val(t_mux1_sel'high)) then
                    mux1_sel <= 0;
                else
                    mux1_sel <= mux1_sel + 1;
                end if;
                if mux2_sel = (t_mux2_sel'val(t_mux2_sel'high)) then
                    mux2_sel <= 0;
                else
                    mux2_sel <= mux2_sel + 1;
                end if;

            end if;
        end if;
    end process;

    -------------------------------------------
    --- compute diagnostics 
    -- (I know it is not very sophisticated. Will be improved in the future.)
    -------------------------------------------
    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                diag_if.insertion_req_counter        <= (others => '0');
                diag_if.retx_counter                 <= (others => '0');
                diag_if.cache_req_counter            <= (others => '0');
                diag_if.acked_counter                <= (others => '0');
                diag_if.max_retx_counter             <= (others => '0');
                result_diag_if.insertion_req_counter <= (others => '0');
                result_diag_if.retx_counter          <= (others => '0');
                result_diag_if.cache_req_counter     <= (others => '0');
                result_diag_if.acked_counter         <= (others => '0');
                result_diag_if.max_retx_counter      <= (others => '0');
                diag_index                           <= C_BLOCK_NUM + 1;
            else

                if diag_index = C_BLOCK_NUM then
                    diag_index                           <= diag_index + 1;
                    diag_if.insertion_req_counter        <= result_diag_if.insertion_req_counter;
                    diag_if.retx_counter                 <= result_diag_if.retx_counter;
                    diag_if.cache_req_counter            <= result_diag_if.cache_req_counter;
                    diag_if.acked_counter                <= result_diag_if.acked_counter;
                    diag_if.max_retx_counter             <= result_diag_if.max_retx_counter;
                    result_diag_if.insertion_req_counter <= (others => '0');
                    result_diag_if.retx_counter          <= (others => '0');
                    result_diag_if.cache_req_counter     <= (others => '0');
                    result_diag_if.acked_counter         <= (others => '0');
                    result_diag_if.max_retx_counter      <= (others => '0');
                elsif diag_index = C_BLOCK_NUM + 1 then
                    diag_index <= 0;
                    for ii in 0 to C_BLOCK_NUM - 1 loop
                        r_diag_if_array(ii).insertion_req_counter <= diag_if_array(ii).insertion_req_counter;
                        r_diag_if_array(ii).retx_counter          <= diag_if_array(ii).retx_counter;
                        r_diag_if_array(ii).cache_req_counter     <= diag_if_array(ii).cache_req_counter;
                        r_diag_if_array(ii).acked_counter         <= diag_if_array(ii).acked_counter;
                        r_diag_if_array(ii).max_retx_counter      <= diag_if_array(ii).max_retx_counter;
                    end loop;
                else
                    diag_index <= diag_index + 1;

                    result_diag_if.insertion_req_counter <= result_diag_if.insertion_req_counter + r_diag_if_array(diag_index).insertion_req_counter;
                    result_diag_if.retx_counter          <= result_diag_if.retx_counter + r_diag_if_array(diag_index).retx_counter;
                    result_diag_if.cache_req_counter     <= result_diag_if.cache_req_counter + r_diag_if_array(diag_index).cache_req_counter;
                    result_diag_if.acked_counter         <= result_diag_if.acked_counter + r_diag_if_array(diag_index).acked_counter;
                    result_diag_if.max_retx_counter      <= result_diag_if.insertion_req_counter + r_diag_if_array(diag_index).max_retx_counter;
                end if;
            end if;
        end if;
    end process;

end architecture RTL;
