-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: arq_timekeeper_tx
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Stores and controls ARQ frame entries
-- 
-- Dependencies: 
-- 
-- Revision: 
-- TODO: interface to ARQ Router to inform the Address selected to store the frame.
--
-- Revision 0.01 - File Created
-- Additional Comments:
-- Strategy: timestamp stored is the timeout target. So when counter reaches (equal or greater) the timestamp, it 
-- triggers the retransmission.
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;
library share_let_1g;
use share_let_1g.pkg_support.all;
--library arq_lib;
--use arq_lib.arq_req_control_support_pkg.all;

entity arq_timekeeper_tx is
    generic(
        G_MEMORY_BLOCK_SIZE          : natural := 256; -- A low value increases performance at a huge resource cost. Must be power of 2.
        G_CORE_COUNTER_PERIOD_CLOCKS : natural := 100;
        G_BLOCK_NUMBER               : natural := 0
    );
    port(
        clk                  : in    std_logic;
        rst                  : in    std_logic;
        --Input interfaces:
        arq_config_if          : in    t_arq_config_if;
        ack_data_if_m          : in t_ack_data_if_m;
        ack_data_if_s          : out t_ack_data_if_s;
        req_insertion_if_m     : in t_request_insertion_if_m;
        req_insertion_if_s     : out t_request_insertion_if_s;
        --Output interfaces;
        req_resend_if_m        : out t_request_resend_if_m;
        req_resend_if_s        : in t_request_resend_if_s;
        req_cache_if_m         : out t_request_cache_if_m;
        req_cache_if_s         : in t_request_cache_if_s;
        metadata_retrieve_if : out t_metadata_retrieve_if; -- TODO
        clear_diag           : in std_logic;
        diag_if              : out t_arq_control_block_tx_diagnostics_if;
        block_full           : out   std_logic
    );
end entity arq_timekeeper_tx;

architecture RTL of arq_timekeeper_tx is

    constant C_BLOCK_NUM : natural := (C_ARQ_HOLDOFF_NFRAMES_MAX / G_MEMORY_BLOCK_SIZE); -- in uS--SDA T1 req: 2500 ms max time for holdoff 

    constant C_ADDRESS_BEGIN             : natural                                   := G_BLOCK_NUMBER * G_MEMORY_BLOCK_SIZE;
    constant C_ADDRESS_END               : natural                                   := (G_BLOCK_NUMBER * G_MEMORY_BLOCK_SIZE) + G_MEMORY_BLOCK_SIZE - 1;
    constant C_COUNTER_THRESHOLD_NATURAL : natural                                   := 3000;
    constant C_COUNTER_THRESHOLD         : unsigned(C_US_COUNTER_WIDTH - 1 downto 0) := to_unsigned(C_COUNTER_THRESHOLD_NATURAL, C_US_COUNTER_WIDTH);

    --subtype t_us_counter is natural range 0 to C_US_COUNTER_PERIOD_CLOCKS;
    signal us_counter       : unsigned(C_US_COUNTER_WIDTH - 1 downto 0);
    constant C_THRESHOLD_UP : unsigned := (us_counter'high - C_COUNTER_THRESHOLD);
    --constant C_US_COUNTER_CACHE_OFFSET : t_us_counter := 50; --SDA T1 req: 2500 ms max time for holdoff 

    type t_addr_if is record
        addr  : std_logic_vector(C_ADDRESS_WIDTH - 1 downto 0);
        ready : std_logic;
        valid : std_logic;
    end record;

    --type t_entry_if is record
    --    data                : std_logic_vector(C_ENTRY_WIDTH-1 downto 0);
    --    ready               : std_logic;
    --    valid               : std_logic;
    --end record;

    signal addr_if_in   : t_addr_if;
    signal addr_if_out  : t_addr_if;
    signal entry_if_in  : t_entry_if;
    signal entry_if_out : t_entry_if;
    signal r_entry_unit : t_entry_unit;

    subtype t_addr_index is natural range 0 to (C_ARQ_HOLDOFF_NFRAMES_MAX);

    signal core_counter : natural range 0 to G_CORE_COUNTER_PERIOD_CLOCKS; -- Counter to get 1us

    signal addr_index : t_addr_index := 0;
    signal addr_input : t_addr_index := 0;

    type t_fsm_state is (INIT,
                         CHECK_REQ_INSERTION,
                         INSERT_NEW_ENTRY,
                         POP_ENTRY,
                         ENTRY_CHECKS,
                         TIMEOUT_CHECK_STAGE_2,
                         CACHE_TIMEOUT_STAGE2,
                         TIMEOUT_CHECK_STAGE_NORMAL,
                         CACHE_STAGE_NORMAL,
                         TIMEOUT,
                         CACHE_TIMEOUT,
                         PUSH_ENTRY,
                         DROP_ENTRY,
                         RESEND_AND_DROP_ENTRY,
                         RESEND_AND_PUSH_ENTRY,
                         REQ_CACHE_AND_PUSH_ENTRY);
    signal fsm_state : t_fsm_state;

    type t_cache_sub_fsm is (SUB_NO_TIMEOUT, SUB_CACHE_STAGE_NORMAL, SUB_CACHE_TIMEOUT);
    signal cache_subfsm : t_cache_sub_fsm;

    signal request_resend_data_out  : std_logic_vector(16 + 3 - 1 downto 0);
    signal ack_data_in              : std_logic_vector(16 + 1 - 1 downto 0);
    signal ack_data_out             : std_logic_vector(16 + 1 - 1 downto 0);
    --signal request_insertion_if_buf_m : t_request_insertion_if_m;
    --signal request_insertion_if_buf_s : t_request_insertion_if_s;

    --signal request_resend_if_array_out : t_request_resend_if;
    signal ack_data_if_buf_m             : t_ack_data_if_m;
    signal ack_data_if_buf_s             : t_ack_data_if_s;

    signal reset_core_counter   : std_logic;
    signal r_clear_timeout      : std_logic;
    signal r_timeout_stage2     : unsigned(C_US_COUNTER_WIDTH - 1 downto 0);
    signal r_timeout_stage3     : unsigned(C_US_COUNTER_WIDTH - 1 downto 0);
    signal d_counter_ts         : unsigned(C_US_COUNTER_WIDTH - 1 downto 0);
    signal d_counter_ts_cache   : unsigned(C_US_COUNTER_WIDTH - 1 downto 0);
    signal r_d_timestamp        : unsigned(C_US_COUNTER_WIDTH - 1 downto 0);
    signal r_d_timestamp_cache  : unsigned(C_US_COUNTER_WIDTH - 1 downto 0);
    signal r_d_us_counter       : unsigned(C_US_COUNTER_WIDTH - 1 downto 0);
    signal r_d_us_counter_cache : unsigned(C_US_COUNTER_WIDTH - 1 downto 0);
    signal d_time               : unsigned(C_US_COUNTER_WIDTH - 1 downto 0);
    signal fifo_content_count   : std_logic_vector(no_bits_natural(G_MEMORY_BLOCK_SIZE) - 1 downto 0);
    signal cfg_holdoff_nf       : std_logic_vector(no_bits_natural(G_MEMORY_BLOCK_SIZE) - 1 downto 0);

    signal fifo_prog_full : std_logic;
    signal fifo_full      : std_logic;
begin
    -------------------------------------------
    --- ASYNC connections
    -------------------------------------------
    cfg_holdoff_nf      <= arq_config_if.holdoff_nframes(no_bits_natural(G_MEMORY_BLOCK_SIZE) - 1 downto 0);
    fifo_prog_full      <= '0' when fifo_content_count < cfg_holdoff_nf else '1';
    block_full          <= fifo_full or fifo_prog_full;
    -- Conversions for ACK bus IF
    --ack_data_in                   <= ack_data_if_data2slv(ack_data_if);
    --ack_data_if_buf.ack_start_fn  <= ack_data_if_slv2ack_start_fn(ack_data_out);     
    --ack_data_if_buf.ack           <= ack_data_if_slv2ack(ack_data_out);       
    ----ack_data_if_buf.ack_span      <= ack_data_if_slv2ack_span(ack_data_out);   
    --
    ---- Conversions for Req Resend bus IF
    --req_resend_if.tx_fn     <= request_resend_if_slv2tx_fn(request_resend_data_out);
    --req_resend_if.tx_num    <= request_resend_if_slv2tx_num(request_resend_data_out);     
    --request_resend_data_in      <= request_resend_if_data2slv(request_resend_if_buf);

    -------------------------------------------
    --- Timers management
    -------------------------------------------
    process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                core_counter <= 0;
                us_counter   <= x"0000";
            --us_counter <= x"FF00";
            --us_counter <= x"FF86";
            --us_counter <= (others=>'0');
            -- problem : x"FF85"
            else
                if reset_core_counter = '1' then
                    core_counter <= 0;
                else
                    -- Core counter to get 1us:
                    if core_counter < to_integer(unsigned(arq_config_if.core_counter_period)) then
                        core_counter <= core_counter + 1;
                    else
                        core_counter <= 0;
                        us_counter   <= us_counter + 1;
                    end if;

                end if;
            end if;
        end if;
    end process;

    -------------------------------------------
    --- FSM
    -------------------------------------------
    process(clk) is
        --variable v_block_index : natural range 0 to (C_ARQ_HOLDOFF_NFRAMES_MAX/(G_MEMORY_BLOCK_SIZE-1)) := 0;
        --variable v_timer_index : t_vtimer_index := 0;
        variable v_timer_counter         : natural range 0 to (G_MEMORY_BLOCK_SIZE - 1) := 0;
        variable v_ack                   : std_logic;
        variable v_ack_start_fn          : std_logic_vector(15 downto 0);
        variable v_ack_span              : std_logic_vector(2 downto 0);
        variable v_return                : integer;
        variable v_free_entry            : std_logic                                    := '0';
        variable v_halt_timer            : std_logic                                    := '0';
        variable v_insertion_req_counter : unsigned(16 - 1 downto 0);
        variable v_retx_counter          : unsigned(16 - 1 downto 0);
        variable v_cache_req_counter     : unsigned(16 - 1 downto 0);
        variable v_acked_counter         : unsigned(16 - 1 downto 0);
        variable v_max_retx_counter      : unsigned(16 - 1 downto 0);
        variable v_entry_unit            : t_entry_unit;

    begin
        if rising_edge(clk) then
            if rst = '1' then
                fsm_state       <= INIT;
                addr_index      <= C_ADDRESS_BEGIN; -- TODO: check natural bounds
                v_timer_counter := 0;
                --reset_core_counter <= '0';
                --r_clear_timeout <= '0';

                v_insertion_req_counter       := (others => '0');
                v_retx_counter                := (others => '0');
                v_cache_req_counter           := (others => '0');
                v_acked_counter               := (others => '0');
                v_max_retx_counter            := (others => '0');
                diag_if.insertion_req_counter <= (others => '0');
                diag_if.retx_counter          <= (others => '0');
                diag_if.cache_req_counter     <= (others => '0');
                diag_if.acked_counter         <= (others => '0');
                diag_if.max_retx_counter      <= (others => '0');

                --TODO:
                req_resend_if_m.tx_fn        <= (others => '0');
                req_resend_if_m.tx_num       <= (others => '0');
                req_resend_if_m.addr         <= (others => '0');
                req_resend_if_m.frame_type   <= (others => '0');
                req_cache_if_m.tx_fn         <= (others => '0');
                req_cache_if_m.addr          <= (others => '0');
                req_resend_if_m.valid        <= '0';
                req_insertion_if_s.ready     <= '0';
                v_halt_timer               := '0';
                req_cache_if_m.valid         <= '0';
                r_timeout_stage2           <= (others => '0');
                r_timeout_stage3           <= (others => '0');
                r_entry_unit.tx_fn         <= (others => '0');
                r_entry_unit.frame_type    <= (others => '0');
                r_entry_unit.tx_num        <= (others => '0');
                r_entry_unit.addr          <= (others => '0');
                r_entry_unit.timestamp     <= (others => '0');
                r_entry_unit.cached        <= '0';
                addr_if_in.valid           <= '0';
                entry_if_in.data           <= (others => '0');
                addr_if_in.addr            <= (others => '0');
                ack_data_if_s.rd_ena         <= '0';
                metadata_retrieve_if.valid <= '0';
                metadata_retrieve_if.tx_fn <= (others => '0');
                metadata_retrieve_if.addr  <= (others => '0');
                cache_subfsm               <= SUB_NO_TIMEOUT;
            else
                ack_data_if_s.rd_ena         <= '0';
                req_insertion_if_s.ready     <= '0';
                req_cache_if_m.valid         <= '0';
                addr_if_in.valid           <= '0';
                addr_if_out.ready          <= '0';
                entry_if_in.valid          <= '0';
                entry_if_out.ready         <= '0';
                req_resend_if_m.valid        <= '0';
                metadata_retrieve_if.valid <= '0';

                case fsm_state is

                    -- Init of all addresses in the address FIFO
                    when INIT =>

                        cache_subfsm <= SUB_NO_TIMEOUT;
                        if addr_index = C_ADDRESS_END + 1 then
                            fsm_state        <= CHECK_REQ_INSERTION;
                            --fsm_state <= INIT;
                            addr_if_in.valid <= '0';
                        else
                            addr_if_in.valid <= '1';
                            if addr_if_in.ready = '1' then
                                -- insert address in FIFO addr:
                                addr_if_in.addr <= std_logic_vector(to_unsigned(addr_index, C_ADDRESS_WIDTH));
                                addr_index      <= addr_index + 1;
                            end if;
                        end if;

                    when CHECK_REQ_INSERTION =>

                        if req_insertion_if_m.valid = '1' and entry_if_in.ready = '1' and addr_if_out.valid = '1' then
                            -- Take new entry:
                            r_entry_unit.tx_fn           <= req_insertion_if_m.tx_fn;
                            r_entry_unit.frame_type      <= req_insertion_if_m.frame_type;
                            -- take new address:
                            r_entry_unit.addr            <= addr_if_out.addr;
                            addr_if_out.ready            <= '1';
                            r_entry_unit.tx_num          <= "000";
                            r_entry_unit.cached          <= '0';
                            -- Insert timestamp
                            r_entry_unit.timestamp       <= us_counter + unsigned(arq_config_if.holdoff_time);
                            r_entry_unit.cache_timestamp <= us_counter + unsigned(arq_config_if.cache_trigger_time);
                            req_insertion_if_s.ready       <= '1';
                            fsm_state                    <= INSERT_NEW_ENTRY;

                        else
                            fsm_state <= POP_ENTRY;
                        end if;

                    when INSERT_NEW_ENTRY =>
                        -- send back metadata of this insertion:
                        metadata_retrieve_if.tx_fn <= r_entry_unit.tx_fn;
                        metadata_retrieve_if.valid <= '1';
                        metadata_retrieve_if.addr  <= r_entry_unit.addr;

                        entry_if_in.data              <= entry_if_data2slv(r_entry_unit);
                        entry_if_in.valid             <= '1';
                        fsm_state                     <= POP_ENTRY;
                        diag_if.insertion_req_counter <= diag_if.insertion_req_counter + 1;

                    when POP_ENTRY =>
                        -- take entry from fifo
                        if entry_if_out.valid = '1' then
                            r_entry_unit       <= entry_if_slv2record(entry_if_out.data);
                            entry_if_out.ready <= '1';
                            fsm_state          <= ENTRY_CHECKS;
                        else
                            -- Entry fifo empty:
                            fsm_state <= CHECK_REQ_INSERTION;
                        end if;

                    when ENTRY_CHECKS =>
                        fsm_state <= TIMEOUT_CHECK_STAGE_2;
                        -- check timeout  
                        --if r_entry_unit.timestamp <= us_counter then  -- TODO: problem here. rethink!!!

                        -- compute max us counter - Timestamp:
                        r_d_timestamp  <= us_counter'high - r_entry_unit.timestamp;
                        r_d_us_counter <= us_counter'high - us_counter;

                        -- compute max us counter - Timestamp:
                        r_d_timestamp_cache  <= us_counter'high - r_entry_unit.cache_timestamp;
                        r_d_us_counter_cache <= us_counter'high - us_counter;

                        -- check ACK
                        if ack_data_if_m.ack_valid = '1' then
                            if r_entry_unit.tx_fn = ack_data_if_m.ack_start_fn then
                                ack_data_if_s.rd_ena <= '1';
                                if ack_data_if_m.ack = '1' then
                                    -- HIT!
                                    fsm_state <= DROP_ENTRY;
                                end if;
                            end if;
                        end if;

                    --------------
                    --- Timeout corner case check:
                    --- [ ((Max_us_counter - Ts) + us_counter) < cfg.holdoff_time ]
                    --------------
                    when TIMEOUT_CHECK_STAGE_2 =>
                        d_counter_ts       <= us_counter - r_entry_unit.timestamp;
                        d_counter_ts_cache <= us_counter - r_entry_unit.cache_timestamp;
                        --d_time <= r_d_timestamp + us_counter; 

                        if r_d_us_counter < C_COUNTER_THRESHOLD and r_d_timestamp > C_THRESHOLD_UP then
                            -- Not timeout!
                            --fsm_state <= PUSH_ENTRY;
                            fsm_state <= CACHE_TIMEOUT_STAGE2;
                        elsif r_d_timestamp < C_COUNTER_THRESHOLD and r_d_us_counter > C_THRESHOLD_UP then
                            fsm_state <= TIMEOUT;
                        else
                            fsm_state <= TIMEOUT_CHECK_STAGE_NORMAL;
                        end if;

                        if r_d_us_counter < C_COUNTER_THRESHOLD and r_d_timestamp_cache > C_THRESHOLD_UP then
                            -- Not timeout!
                            cache_subfsm <= SUB_NO_TIMEOUT;
                        elsif r_d_timestamp_cache < C_COUNTER_THRESHOLD and r_d_us_counter > C_THRESHOLD_UP then
                            cache_subfsm <= SUB_CACHE_TIMEOUT;
                        else
                            cache_subfsm <= SUB_CACHE_STAGE_NORMAL;
                        end if;

                    when TIMEOUT_CHECK_STAGE_NORMAL =>
                        if us_counter >= r_entry_unit.timestamp and d_counter_ts < C_COUNTER_THRESHOLD then
                            fsm_state <= TIMEOUT;
                        else
                            -- Not timeout!
                            if r_entry_unit.cached = '1' then
                                fsm_state <= PUSH_ENTRY;
                            else
                                case cache_subfsm is
                                    when SUB_NO_TIMEOUT =>
                                        fsm_state <= PUSH_ENTRY;
                                    when SUB_CACHE_TIMEOUT =>
                                        fsm_state <= CACHE_TIMEOUT;
                                    when SUB_CACHE_STAGE_NORMAL =>
                                        fsm_state <= CACHE_STAGE_NORMAL;
                                end case;
                            end if;
                        end if;

                    when CACHE_TIMEOUT_STAGE2 =>
                        if r_entry_unit.cached = '1' then
                            fsm_state <= PUSH_ENTRY;
                        else
                            case cache_subfsm is
                                when SUB_NO_TIMEOUT =>
                                    fsm_state <= PUSH_ENTRY;
                                when SUB_CACHE_TIMEOUT =>
                                    fsm_state <= CACHE_TIMEOUT;
                                when SUB_CACHE_STAGE_NORMAL =>
                                    fsm_state <= CACHE_STAGE_NORMAL;
                            end case;
                        end if;

                    when TIMEOUT =>
                        if to_integer(unsigned(r_entry_unit.tx_num)) < to_integer(unsigned(arq_config_if.max_retx)) - 1 then
                            fsm_state                    <= RESEND_AND_PUSH_ENTRY;
                            -- Increase tx_num
                            r_entry_unit.tx_num          <= std_logic_vector(unsigned(r_entry_unit.tx_num) + 1);
                            -- Insert new timestamp
                            r_entry_unit.timestamp       <= us_counter + unsigned(arq_config_if.holdoff_time);
                            -- Insert new cache timestamp:
                            r_entry_unit.cache_timestamp <= us_counter + unsigned(arq_config_if.cache_trigger_time);
                        else
                            -- Increase tx_num
                            r_entry_unit.tx_num      <= std_logic_vector(unsigned(r_entry_unit.tx_num) + 1);
                            -- max retransmission attempts reached. Drop entry.
                            fsm_state                <= RESEND_AND_DROP_ENTRY;
                            diag_if.max_retx_counter <= diag_if.max_retx_counter + 1;
                        end if;

                    -- (NO_TIMEOUT,CACHE_STAGE2, CACHE_STAGE_NORMAL, CACHE_TIMEOUT)
                    when CACHE_STAGE_NORMAL =>
                        if us_counter >= r_entry_unit.cache_timestamp and d_counter_ts_cache < C_COUNTER_THRESHOLD then
                            fsm_state <= CACHE_TIMEOUT;
                        else
                            -- Not timeout!
                            fsm_state <= PUSH_ENTRY;
                        end if;

                    when CACHE_TIMEOUT =>
                        fsm_state                 <= REQ_CACHE_AND_PUSH_ENTRY;
                        diag_if.cache_req_counter <= diag_if.cache_req_counter + 1;
                        -- set flag cached to avoid cache triggering more than once:
                        r_entry_unit.cached       <= '1';

                    when PUSH_ENTRY =>
                        --Push back entry into FIFO
                        if entry_if_in.ready = '1' then
                            entry_if_in.valid <= '1';
                            v_entry_unit      := r_entry_unit;
                            entry_if_in.data  <= entry_if_data2slv(v_entry_unit);
                            fsm_state         <= CHECK_REQ_INSERTION;
                        end if;
                    --req_resend_if.tx_fn  <= r_entry_unit.tx_fn ;
                    --req_resend_if.addr   <= r_entry_unit.addr  ;
                    --req_resend_if.tx_num <= r_entry_unit.tx_num;
                    --req_resend_if.valid  <= '1';

                    -- From ACK check:
                    when DROP_ENTRY =>
                        -- DROP entry
                        -- Push address into address FIFO
                        if addr_if_in.ready = '1' then
                            addr_if_in.addr       <= r_entry_unit.addr;
                            addr_if_in.valid      <= '1';
                            fsm_state             <= CHECK_REQ_INSERTION;
                            diag_if.acked_counter <= diag_if.acked_counter + 1;
                        end if;

                    when RESEND_AND_DROP_ENTRY =>
                        -- Resend entry to output
                        req_resend_if_m.tx_fn      <= r_entry_unit.tx_fn;
                        req_resend_if_m.frame_type <= r_entry_unit.frame_type;
                        req_resend_if_m.addr       <= r_entry_unit.addr;
                        req_resend_if_m.tx_num     <= r_entry_unit.tx_num;
                        req_resend_if_m.valid      <= '1';
                        r_entry_unit.cached      <= '0'; -- clear flag to enable cache req in the next rtx
                        -- Push address into address FIFO
                        addr_if_in.addr          <= r_entry_unit.addr;
                        addr_if_in.valid         <= '1';
                        fsm_state                <= CHECK_REQ_INSERTION;
                        diag_if.retx_counter     <= diag_if.retx_counter + 1;

                    when RESEND_AND_PUSH_ENTRY =>
                        -- Resend entry to output
                        req_resend_if_m.tx_fn      <= r_entry_unit.tx_fn;
                        req_resend_if_m.frame_type <= r_entry_unit.frame_type;
                        req_resend_if_m.addr       <= r_entry_unit.addr;
                        req_resend_if_m.tx_num     <= r_entry_unit.tx_num;
                        req_resend_if_m.valid      <= '1';

                        --Push back entry into FIFO
                        entry_if_in.valid    <= '1';
                        v_entry_unit         := r_entry_unit;
                        v_entry_unit.cached  := '0'; -- clear flag to enable cache req in the next rtx
                        entry_if_in.data     <= entry_if_data2slv(v_entry_unit);
                        fsm_state            <= CHECK_REQ_INSERTION;
                        diag_if.retx_counter <= diag_if.retx_counter + 1;

                    when REQ_CACHE_AND_PUSH_ENTRY =>
                        -- Resend entry to output
                        --if arq_config_if.cache_enable = '1' then
                        req_cache_if_m.tx_fn <= r_entry_unit.tx_fn;
                        req_cache_if_m.addr  <= r_entry_unit.addr;
                        req_cache_if_m.valid <= '1';
                        --end if;

                        --Push back entry into FIFO
                        entry_if_in.valid <= '1';
                        v_entry_unit      := r_entry_unit;
                        entry_if_in.data  <= entry_if_data2slv(v_entry_unit);
                        fsm_state         <= CHECK_REQ_INSERTION;

                    when others =>
                        fsm_state <= CHECK_REQ_INSERTION;
                        --when CHECK_ACK_DATA =>
                        --when CHECK_ACK_DATA => 

                end case;
                if clear_diag = '1' then
                    diag_if.insertion_req_counter <= (others => '0');
                    diag_if.retx_counter          <= (others => '0');
                    diag_if.cache_req_counter     <= (others => '0');
                    diag_if.acked_counter         <= (others => '0');
                    diag_if.max_retx_counter      <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    -------------------------------------------
    --- FIFOS
    -------------------------------------------

    -- Main FIFO for storing entries
    inst_main : entity share_let_1g.sync_fifo
        generic map(
            DISTR_RAM       => false,
            FIFO_DEPTH      => G_MEMORY_BLOCK_SIZE,
            DATA_WIDTH      => C_ENTRY_WIDTH,
            FULL_THRESHOLD  => G_MEMORY_BLOCK_SIZE,
            EMPTY_THRESHOLD => 0
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => entry_if_in.data,
            input_valid        => entry_if_in.valid,
            input_ready        => entry_if_in.ready,
            input_almost_full  => fifo_full,
            input_almost_empty => open,
            fifo_max_level     => open,
            fifo_data_count    => fifo_content_count,
            -- Output data handling
            -----------------------
            output             => entry_if_out.data,
            output_valid       => entry_if_out.valid,
            output_ready       => entry_if_out.ready
        );

    -- FIFO for ADDRESSES not in use
    inst_addr_fifo : entity share_let_1g.sync_fifo
        generic map(
            DISTR_RAM       => false,
            FIFO_DEPTH      => G_MEMORY_BLOCK_SIZE + 1,
            DATA_WIDTH      => C_ADDRESS_WIDTH,
            FULL_THRESHOLD  => G_MEMORY_BLOCK_SIZE + 1,
            EMPTY_THRESHOLD => 0
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => addr_if_in.addr,
            input_valid        => addr_if_in.valid,
            input_ready        => addr_if_in.ready,
            input_almost_full  => open,
            input_almost_empty => open,
            fifo_max_level     => open,
            fifo_data_count    => open,
            -- Output data handling
            -----------------------
            output             => addr_if_out.addr,
            output_valid       => addr_if_out.valid,
            output_ready       => addr_if_out.ready
        );

        --inst_tx_fn_ram : entity share_let_1g.generic_simple_dp_ram
        --    generic map(
        --    	DISTR_RAM  => true,                         --: boolean := true;
        --    	WORDS      => C_ARQ_HOLDOFF_NFRAMES_MAX,    --: integer := 5;
        --    	BITWIDTH   => 16                            --: integer := 1
        --    )
        --    port map(
        --    	clk     => clk, --: in std_logic;
        --    	rst     => rst, --: in std_logic;   
        --    	wen_A   => ram_wr_ena, --: in  std_logic;
        --    	en_A    => ram_ena_A, --: in  std_logic;
        --    	a_A     => ram_addr_A, --: in  std_logic_vector(no_bits_natural(WORDS - 1) - 1 downto 0);
        --    	d_A     => ram_data_A_tx_fn, --: in  std_logic_vector(BITWIDTH - 1 downto 0 );
        --    	en_B    => ram_ena_B, --: in  std_logic;
        --    	a_B     => ram_addr_B, --: in  std_logic_vector(no_bits_natural(WORDS - 1) - 1 downto 0);
        --    	q_B     => ram_data_B_tx_fn --: out std_logic_vector(BITWIDTH - 1 downto 0)
        --    );
        --
        --    inst_tx_num_ram : entity share_let_1g.generic_simple_dp_ram
        --    generic map(
        --    	DISTR_RAM  => true,                         --: boolean := true;
        --    	WORDS      => C_ARQ_HOLDOFF_NFRAMES_MAX,    --: integer := 5;
        --    	BITWIDTH   => 3                             --: integer := 1
        --    )
        --    port map(
        --    	clk     => clk, --: in std_logic;
        --    	rst     => rst, --: in std_logic;   
        --    	wen_A   => ram_wr_ena, --: in  std_logic;
        --    	en_A    => ram_ena_A, --: in  std_logic;
        --    	a_A     => ram_addr_A, --: in  std_logic_vector(no_bits_natural(WORDS - 1) - 1 downto 0);
        --    	d_A     => ram_data_A_tx_num, --: in  std_logic_vector(BITWIDTH - 1 downto 0 );
        --    	en_B    => ram_ena_B, --: in  std_logic;
        --    	a_B     => ram_addr_B, --: in  std_logic_vector(no_bits_natural(WORDS - 1) - 1 downto 0);
        --    	q_B     => ram_data_B_tx_num --: out std_logic_vector(BITWIDTH - 1 downto 0)
        --    );

end architecture RTL;
