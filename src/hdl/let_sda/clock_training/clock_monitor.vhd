----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 06/09/2021 11:40:33 AM
-- Design Name: 
-- Module Name: clock_monitor - Behavioral
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2018.3
-- Description: Monitors numbers of clock with a reference clock.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
-- Xilinx components
library unisim;
use unisim.vcomponents.all;


entity clock_monitor is
    Port ( 
        -- System Clock
        ----------------------    
        clk            : in  std_logic;
        rst            : in  std_logic;
        
        -- Reference clock (Low Freq)
        ----------------------    
        clk_ref        : in  std_logic;

        -- Training clock (High Freq)
        ----------------------
        clk_monitor    : in std_logic;

        -- Config window: Number of cycles of reference clock to measure.
        ----------------------
        cfg_window     : in unsigned(32-1 downto 0);        

        -- Output ticks counter
        ----------------------
        ticks                : out unsigned(32-1 downto 0);
        ticks_valid          : out std_logic;
        window_ticks         : out unsigned(32-1 downto 0);
        window_ticks_valid   : out std_logic
    );
end clock_monitor;

architecture Behavioral of clock_monitor is

    signal ref_counter              : unsigned(32-1 downto 0);
    signal monitor_counter_window   : unsigned(32-1 downto 0);
    signal monitor_counter          : unsigned(32-1 downto 0);

    signal clk_ref_int : std_logic;
    signal clk_monitor_int : std_logic;
    signal r_clk_ref_int : std_logic;
    signal r_clk_monitor_int : std_logic;

    attribute clock_signal : string;
    attribute clock_signal of clk_ref_int       : signal is "no";
    attribute clock_signal of clk_monitor_int   : signal is "no";
    attribute clock_signal of clk_ref           : signal is "yes";
    attribute clock_signal of clk_monitor       : signal is "yes";

begin

process(clk)
    variable v_ref_counter              : unsigned(32-1 downto 0);
    variable v_monitor_counter_window   : unsigned(32-1 downto 0);
    variable v_monitor_counter          : unsigned(32-1 downto 0);
    variable v_ref_counter_window       : unsigned(32-1 downto 0);
begin
    if rising_edge(clk) then
        if rst = '1' then
            v_monitor_counter        := (others =>'0');
            v_monitor_counter_window := (others =>'0');
            v_ref_counter_window     := (others =>'0');
            ticks                    <= (others =>'0');
            ticks_valid              <= '0';
            window_ticks             <= (others =>'0');
            window_ticks_valid       <= '1';
            r_clk_ref_int            <= '0';
            r_clk_monitor_int        <= '0';
        else
        
            --clk_ref_int         <= clk_ref;
            --clk_monitor_int     <= clk_monitor;
            r_clk_ref_int       <= clk_ref_int;
            r_clk_monitor_int   <= clk_monitor_int;

            ticks_valid         <= '0';
            window_ticks_valid  <= '0';

            -- Detect rising edge of the monitor clock
            if r_clk_monitor_int = '0' and clk_monitor_int = '1' then
                v_monitor_counter           := v_monitor_counter + 1;
                v_monitor_counter_window    := v_monitor_counter_window + 1;
            end if;

            -- Detect rising edge of the reference clock
            if r_clk_ref_int = '0' and clk_ref_int = '1' then
                ticks                   <= v_monitor_counter;
                ticks_valid             <= '1';
                v_ref_counter_window    := v_ref_counter_window + 1;
                v_monitor_counter       := (others =>'0');
                if v_ref_counter_window = cfg_window then
                    window_ticks             <= v_monitor_counter_window;
                    window_ticks_valid       <= '1';    
                    v_monitor_counter_window := (others =>'0');
                    v_ref_counter_window     := (others =>'0');
                end if;
            end if;
        end if;
    end if;
end process;


clk_ref_bufg_inst : BUFG
port map (
   O => clk_ref_int,  				-- Clock buffer output
   I => clk_ref
);

clk_monitor_bufg_inst : BUFG
port map (
   O => clk_monitor_int,  				-- Clock buffer output
   I => clk_monitor
);

end Behavioral;



