----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 06/09/2021 11:40:33 AM
-- Design Name: 
-- Module Name: clock_training - Behavioral
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2018.3
-- Description: Monitors numbers of clock with a reference clock.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;



entity clock_training is
    Port ( 
        -- Main clock
        ----------------------    
        clk            : in  std_logic;
        rst            : in  std_logic;

        -- Reference clock (Low Freq)
        ----------------------    
        clk_ref_a        : in  std_logic;
        clk_ref_b        : in  std_logic;

        -- Training clock (High Freq)
        ----------------------
        clk_monitor_1    : in std_logic;
        clk_monitor_2    : in std_logic;

        -- Config window: Number of cycles of reference clock to measure.
        ----------------------
        cfg_window     : in unsigned(32-1 downto 0);   

        -- Output ticks counter
        ----------------------
        ticks_a_1                : out unsigned(32-1 downto 0);
        ticks_valid_a_1          : out std_logic;
        window_ticks_a_1         : out unsigned(32-1 downto 0);
        window_ticks_valid_a_1   : out std_logic;
        
        ticks_b_1                : out unsigned(32-1 downto 0);
        ticks_valid_b_1          : out std_logic;
        window_ticks_b_1         : out unsigned(32-1 downto 0);
        window_ticks_valid_b_1   : out std_logic;

        ticks_a_2                : out unsigned(32-1 downto 0);
        ticks_valid_a_2          : out std_logic;
        window_ticks_a_2         : out unsigned(32-1 downto 0);
        window_ticks_valid_a_2   : out std_logic;
        
        ticks_b_2                : out unsigned(32-1 downto 0);
        ticks_valid_b_2          : out std_logic;
        window_ticks_b_2         : out unsigned(32-1 downto 0);
        window_ticks_valid_b_2   : out std_logic;

        -- Output phase counter
        ----------------------
        phase_ticks                : out unsigned(8-1 downto 0);
        phase_ticks_valid          : out std_logic

    );
end clock_training;

architecture Behavioral of clock_training is


begin

inst_clock_monitor_a_1 : clock_monitor
    Port map(    
        clk                  => clk,                    --: in  std_logic;
        rst                  => rst,                    --: in  std_logic;
        
        clk_ref              => clk_ref_a,              --: in  std_logic;
        clk_monitor          => clk_monitor_1,            --: in std_logic;
        cfg_window           => cfg_window_a,           --: in unsigned(32-1 downto 0);        

        ticks                => ticks_a_1,                --: out unsigned(32-1 downto 0);
        ticks_valid          => ticks_valid_a_1,          --: out std_logic;
        window_ticks         => window_ticks_a_1,         --: out unsigned(32-1 downto 0);
        window_ticks_valid   => window_ticks_valid_a_1    --: out std_logic
    );

inst_clock_monitor_b_1 : clock_monitor
    Port map(     
        clk                  => clk,                    --: in  std_logic;
        rst                  => rst,                    --: in  std_logic;
        
        clk_ref              => clk_ref_b,              --: in  std_logic;
        clk_monitor          => clk_monitor_1,            --: in std_logic;
        cfg_window           => cfg_window_b,           --: in unsigned(32-1 downto 0);        

        ticks                => ticks_b_1,                --: out unsigned(32-1 downto 0);
        ticks_valid          => ticks_valid_b_1,          --: out std_logic;
        window_ticks         => window_ticks_b_1,         --: out unsigned(32-1 downto 0);
        window_ticks_valid   => window_ticks_valid_b_1    --: out std_logic
    );

    inst_clock_monitor_a_2 : clock_monitor
    Port map(    
        clk                  => clk,                    --: in  std_logic;
        rst                  => rst,                    --: in  std_logic;
        
        clk_ref              => clk_ref_a,              --: in  std_logic;
        clk_monitor          => clk_monitor_1,            --: in std_logic;
        cfg_window           => cfg_window_a,           --: in unsigned(32-1 downto 0);        

        ticks                => ticks_a_1,                --: out unsigned(32-1 downto 0);
        ticks_valid          => ticks_valid_a_1,          --: out std_logic;
        window_ticks         => window_ticks_a_1,         --: out unsigned(32-1 downto 0);
        window_ticks_valid   => window_ticks_valid_a_1    --: out std_logic
    );

inst_clock_monitor_b_2 : clock_monitor
    Port map(     
        clk                  => clk,                      --: in  std_logic;
        rst                  => rst,                      --: in  std_logic;
        
        clk_ref              => clk_ref_b,                --: in  std_logic;
        clk_monitor          => clk_monitor_2,            --: in std_logic;
        cfg_window           => cfg_window_b,             --: in unsigned(32-1 downto 0);        

        ticks                => ticks_b_2,                --: out unsigned(32-1 downto 0);
        ticks_valid          => ticks_valid_b_2,          --: out std_logic;
        window_ticks         => window_ticks_b_2,         --: out unsigned(32-1 downto 0);
        window_ticks_valid   => window_ticks_valid_b_2    --: out std_logic
    );

inst_phase_monitor : phase_monitor
    Port map( 
        -- System Clock
        ----------------------    
        clk              => clk, --: in  std_logic;
        rst              => rst, --: in  std_logic;

        clk_monitor_a    => clk_monitor_1, --: in std_logic;
        rst_monitor_a    => rst_monitor_1, --: in std_logic;
        clk_monitor_b    => clk_monitor_2, --: in std_logic;
        rst_monitor_b    => rst_monitor_1, --: in std_logic;      

        phase_ticks       => phase_ticks, --: out unsigned(8-1 downto 0);
        phase_ticks_valid => phase_ticks_valid, --: out std_logic

    );







end Behavioral;
