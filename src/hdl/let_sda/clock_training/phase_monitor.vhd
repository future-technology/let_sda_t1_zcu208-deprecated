----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 06/09/2021 11:40:33 AM
-- Design Name: 
-- Module Name: phase_monitor - Behavioral
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2018.3
-- Description: Monitors phase shift
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
-- Xilinx components
library unisim;
use unisim.vcomponents.all;


entity phase_monitor is
    Port ( 
        -- System Clock
        ----------------------    
        clk            : in  std_logic;
        rst            : in  std_logic;

        -- Training clock (High Freq)
        ----------------------
        clk_monitor_a    : in std_logic;
        rst_monitor_a    : in std_logic;
        clk_monitor_b    : in std_logic;
        rst_monitor_b    : in std_logic;      

        -- Output phase counter
        ----------------------
        phase_ticks                : out unsigned(8-1 downto 0);
        phase_ticks_valid          : out std_logic

    );
end phase_monitor;

architecture Behavioral of phase_monitor is

    signal phase_counter            : unsigned(8-1 downto 0);


    signal clk_monitor_down_a        : std_logic;
    signal clk_monitor_down_a_int   : std_logic;
    signal r_clk_monitor_down_a      : std_logic;
    signal r_clk_monitor_down_a_int : std_logic;
    signal clk_monitor_down_b       : std_logic;
    signal clk_monitor_down_b_int   : std_logic;
    signal r_clk_monitor_b_int      : std_logic;
    signal r_clk_monitor_down_b_int : std_logic;

    signal phase_monitor_enable : std_logic;
    
    signal locked_a : std_logic;
    signal locked_b : std_logic;
    signal locked : std_logic;

    attribute clock_signal : string;
    attribute clock_signal of clk_monitor_down_a_int    : signal is "no";
    attribute clock_signal of clk_monitor_a             : signal is "yes";
    attribute clock_signal of clk_monitor_down_b_int    : signal is "no";
    attribute clock_signal of clk_monitor_b             : signal is "yes";

    component clk_down_conv
    port
     (-- Clock in ports
      -- Clock out ports
      clk_out1          : out    std_logic;
      -- Status and control signals
      reset             : in     std_logic;
      locked            : out    std_logic;
      clk_in1           : in     std_logic
     );
    end component;


begin

    locked <= locked_a and locked_b;

process(clk)

begin
    if rising_edge(clk) then
        if rst = '1' then
            r_clk_monitor_down_a_int <= '0';
            r_clk_monitor_down_b_int <= '0';
            phase_counter            <= (others =>'0');
            phase_ticks             <= (others =>'0');
            phase_monitor_enable     <= '0';
            phase_ticks_valid       <= '0';

        else
            phase_ticks_valid       <= '0';
            r_clk_monitor_down_a_int    <= clk_monitor_down_a_int;
            r_clk_monitor_down_b_int    <= clk_monitor_down_b_int;

            -- Detect rising edge of the monitor clock
            if r_clk_monitor_down_a_int = '0' and clk_monitor_down_a_int = '1' and locked ='1' then
                phase_monitor_enable      <= '1';

            end if;

            -- Detect rising edge of the reference clock
            if r_clk_monitor_down_b_int = '0' and clk_monitor_down_b_int = '1' and locked = '1' then
                phase_monitor_enable      <= '0';
                phase_ticks               <= phase_counter + 1;
                if phase_monitor_enable = '1' then
                    phase_ticks_valid <= '1';
                end if;
            end if;

            if phase_monitor_enable = '1' then
                phase_counter <= phase_counter + 1;
            else
                phase_counter <= (others =>'0');
            end if;

        end if;
    end if;
end process;

clk_monitor_bufg_inst_a : BUFG
port map (
   O => clk_monitor_down_a_int,  				-- Clock buffer output
   I => clk_monitor_down_a
);

clk_monitor_bufg_inst_b : BUFG
port map (
   O => clk_monitor_down_b_int,  				-- Clock buffer output
   I => clk_monitor_down_b
);

inst_clk_down_conv_a : clk_down_conv
    port map ( 
        clk_in1     => clk_monitor_a,
        reset       => rst_monitor_a,
        locked      => locked_a,
        clk_out1    => clk_monitor_down_a -- 1/10 Freq
  );

inst_clk_down_conv_b : clk_down_conv
  port map ( 
      clk_in1     => clk_monitor_b,
      reset       => rst_monitor_b,
      locked      => locked_b,
      clk_out1    => clk_monitor_down_b -- 1/10 Freq
);

end Behavioral;



