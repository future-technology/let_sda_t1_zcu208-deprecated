library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package pkg_components is

component clock_monitor
	Port ( 
		-- System Clock
		----------------------    
		clk            : in  std_logic;
		rst            : in  std_logic;
		
		-- Reference clock (Low Freq)
		----------------------    
		clk_ref        : in  std_logic;

		-- Training clock (High Freq)
		----------------------
		clk_monitor    : in std_logic;

		-- Config window: Number of cycles of reference clock to measure.
		----------------------
		cfg_window     : in unsigned(32-1 downto 0);        

		-- Output ticks counter
		----------------------
		ticks                : out unsigned(32-1 downto 0);
		ticks_valid          : out std_logic;
		window_ticks         : out unsigned(32-1 downto 0);
		window_ticks_valid   : out std_logic
		);
end component;

component clock_training 
    Port ( 
        -- Main clock
        ----------------------    
        clk            : in  std_logic;
        rst            : in  std_logic;

        -- Reference clock (Low Freq)
        ----------------------    
        clk_ref_a        : in  std_logic;
        clk_ref_b        : in  std_logic;

        -- Training clock (High Freq)
        ----------------------
        clk_monitor    : in std_logic;
        -- Config window: Number of cycles of reference clock to measure.
        ----------------------
        cfg_window     : in unsigned(32-1 downto 0);   

        -- Output ticks counter
        ----------------------
        ticks_a                : out unsigned(32-1 downto 0);
        ticks_valid_a          : out std_logic;
        window_ticks_a         : out unsigned(32-1 downto 0);
        window_ticks_valid_a   : out std_logic;
        
        ticks_b                : out unsigned(32-1 downto 0);
        ticks_valid_b          : out std_logic;
        window_ticks_b         : out unsigned(32-1 downto 0);
        window_ticks_valid_b   : out std_logic

        );
end component;

component phase_monitor
    Port ( 
        -- System Clock
        ----------------------    
        clk            : in  std_logic;
        rst            : in  std_logic;

        -- Training clock (High Freq)
        ----------------------
        clk_monitor_a    : in std_logic;
        rst_monitor_a    : in std_logic;
        clk_monitor_b    : in std_logic;
        rst_monitor_b    : in std_logic;      

        -- Output ticks counter
        ----------------------
        phase_ticks                : out unsigned(8-1 downto 0);
        phase_ticks_valid          : out std_logic

    );
end component;


component clk_down_conv
    port
        (-- Clock in ports
        -- Clock out ports
        clk_out1          : out    std_logic;
        -- Status and control signals
        reset             : in     std_logic;
        locked            : out    std_logic;
        clk_in1           : in     std_logic
    );
end component;

end package pkg_components;
