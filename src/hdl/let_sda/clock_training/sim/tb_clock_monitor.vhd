----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 05/25/2021 10:10:45 AM
-- Design Name: clock_monitor
-- Module Name: tb_clock_monitor - Behavioral
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: 
-- Description: This module uses a reference clock to 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;


entity tb_clock_monitor is
--  Port ( );
end tb_clock_monitor;


architecture Behavioral of tb_clock_monitor is
    component clock_monitor
        Port ( 
            -- System Clock
            ----------------------    
            clk            : in  std_logic;
            rst            : in  std_logic;
            -- Reference clock (Low Freq)
            ----------------------    
            clk_ref        : in  std_logic;
            -- Training clock (High Freq)
            ----------------------
            clk_monitor    : in std_logic;
    
            -- Config window: Number of cycles of reference clock to measure.
            ----------------------
            cfg_window     : in unsigned(32-1 downto 0);        
    
            -- Output ticks counter
            ----------------------
            ticks                : out unsigned(32-1 downto 0);
            ticks_valid          : out std_logic;
            window_ticks         : out unsigned(32-1 downto 0);
            window_ticks_valid   : out std_logic
        );
    end component;

   
signal clk                  :  std_logic := '0';
signal rst                  :  std_logic := '0';  
signal clk_ref              :  std_logic := '0';
signal clk_monitor          : std_logic := '0';
signal cfg_window           : unsigned(32-1 downto 0) := x"0000000A";        
signal ticks                :  unsigned(32-1 downto 0);
signal ticks_valid          :  std_logic;
signal window_ticks         :  unsigned(32-1 downto 0);
signal window_ticks_valid   :  std_logic;


begin

clk         <= not clk          after 2.5 ns; --5/2 ns;
clk_ref     <= not clk_ref      after 50 ns;  --100/2 ns;
clk_monitor <= not clk_monitor  after 5 ns;   --10/2 ns;

process
begin
    rst <= '1';
    wait for 20 ns;
    rst <= '0';
    wait for 300 ns;
    wait;
end process;

inst_clock_monitor : clock_monitor
    Port map( 
        clk                 => clk, --: in  std_logic;
        rst                 => rst, --: in  std_logic;
 
        clk_ref             => clk_ref, --: in  std_logic;
        clk_monitor         => clk_monitor, --: in std_logic;
        cfg_window          => cfg_window, --: in unsigned(32-1 downto 0);        

        ticks                => ticks, --: out unsigned(32-1 downto 0);
        ticks_valid          => ticks_valid, --: out std_logic;
        window_ticks         => window_ticks, --: out unsigned(32-1 downto 0);
        window_ticks_valid   => window_ticks_valid --: out std_logic
    );


end Behavioral;
