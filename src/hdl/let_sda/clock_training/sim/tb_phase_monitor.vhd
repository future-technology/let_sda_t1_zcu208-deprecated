----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 05/25/2021 10:10:45 AM
-- Design Name: phase_monitor
-- Module Name: tb_phase_monitor - Behavioral
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: 
-- Description: This module uses a reference clock to 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;


entity tb_phase_monitor is
--  Port ( );
end tb_phase_monitor;


architecture Behavioral of tb_phase_monitor is
    component phase_monitor
    Port ( 
        -- System Clock
        ----------------------    
        clk            : in  std_logic;
        rst            : in  std_logic;

        -- Training clock (High Freq)
        ----------------------
        clk_monitor_a    : in std_logic;
        rst_monitor_a    : in std_logic;
        clk_monitor_b    : in std_logic;
        rst_monitor_b    : in std_logic;      

        -- Output phase counter
        ----------------------
        phase_ticks                : out unsigned(8-1 downto 0);
        phase_ticks_valid          : out std_logic

    );
    end component;

  
signal clk                  : std_logic := '0';
signal rst                  : std_logic;
signal clk_monitor_a        :std_logic;
signal rst_monitor_a        :std_logic;
signal clk_monitor_b        :std_logic;
signal rst_monitor_b        :std_logic;      
signal phase_ticks          : unsigned(8-1 downto 0);
signal phase_ticks_valid    : std_logic;



begin

clk <= not clk after 1.666 ns; --5/2 ns;


process
begin
	--clk_monitor_a <= '0';
	--wait for ;
    loop
		clk_monitor_a <= '1';
		wait for 6.41 ns;
		clk_monitor_a <= '0';
		wait for 6.41 ns;
	end loop;
end process;

process
begin
	wait for 10 ns;
    loop
        clk_monitor_b <= '1';
		wait for 6.41 ns;
		clk_monitor_b <= '0';
		wait for 6.41 ns;
	end loop;
end process;

process
begin
    rst <= '1';
    rst_monitor_a <= '1';
    rst_monitor_b <= '1';
    wait for 20 ns;
    rst <= '0';
    rst_monitor_a <= '0';
    rst_monitor_b <= '0';
    wait for 300 ns;
    wait;
end process;

inst_clock_monitor : phase_monitor
    Port map( 
        clk                 => clk, --: in  std_logic;
        rst                 => rst, --: in  std_logic;
        clk_monitor_a       => clk_monitor_a, --: in std_logic;
        rst_monitor_a       => rst_monitor_a, --: in std_logic;
        clk_monitor_b       => clk_monitor_b, --: in std_logic;
        rst_monitor_b       => rst_monitor_b, --: in std_logic;      
        phase_ticks         => phase_ticks, --: out unsigned(8-1 downto 0);
        phase_ticks_valid   => phase_ticks_valid  --: out std_logic
    );


end Behavioral;
