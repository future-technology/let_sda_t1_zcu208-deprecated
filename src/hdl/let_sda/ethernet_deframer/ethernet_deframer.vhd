-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: ethernet_deframer
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Converts SDA Standard FSO frames to Ethernet packets in AXI-Stream format.
-- 
-- Dependencies: 
-- 
-- Revision: First version asumes there is an input axi stream fifo in packet mode. So the frame with be delivered without pause of tready, just to 
-- to simplify logic for the moment. The same applies to output axi stream.
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

library ethernet_deframer_lib;
use ethernet_deframer_lib.pkg_components.all;

--library let_sda_lib;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ethernet_deframer is
    Port(
        clk                           : in    std_logic;
        rst                           : in    std_logic;

        -- Input data from encoder
        ----------------------
        axis_payload_if_m               : in t_axis_payload_if_m;
        axis_payload_if_s               : out t_axis_payload_if_s;

        -- Output data to ethernet mac
        ----------------------
        axis_if_m                       : out t_axis_if_m;
        axis_if_s                       : in t_axis_if_s;

        -- MGMT 
        ----------------------
        mgmt_response                 : out   mgmt_payload_t;
        mgmt_response_valid           : out   std_logic;

        -- Statistics
        ----------------------
        clear_stat                    : in    std_logic;
        total_packet_counter          : out   std_logic_vector(31 downto 0);
        total_payload_counter         : out   std_logic_vector(31 downto 0);
        total_packet_splitted_counter : out   std_logic_vector(31 downto 0);
        total_packet_merged_counter   : out   std_logic_vector(31 downto 0);
        total_data_frame_counter      : out   std_logic_vector(31 downto 0);
        total_idle_frame_counter      : out   std_logic_vector(31 downto 0);
        total_payload_error_counter   : out   std_logic_vector(31 downto 0);
        total_packet_error_counter    : out   std_logic_vector(31 downto 0);
        watchdog_reset_counter        : out   std_logic_vector(31 downto 0);
        packet_filter_cnt_in          : out   std_logic_vector(31 downto 0);
        packet_filter_cnt_out         : out   std_logic_vector(31 downto 0);
        sda_debug                     : out   sda_debug_t
    );
end ethernet_deframer;

architecture Behavioral of ethernet_deframer is

    constant C_WD_TIMEOUT : unsigned(31 downto 0) := TO_UNSIGNED(100, 32); --No packet ready Timeout in clock cycles
    constant C_WD_RESET   : unsigned(31 downto 0) := TO_UNSIGNED(16, 32); --No packet ready Timeout in clock cycles

    -- MAIN FSM
    type t_input_fsm is (IDLE, START, READ_PAYLOAD_METADATA, READ_PAYLOAD_HEADER, POST_PAYLOAD_HEADER, CHECK_32_ALIGN, READ_PACKET_HEADER, POST_PACKET_HEADER, WRITE_PACKET, WAIT_READY, BAD_PACKET, NO_PACKET, SEND_PACKET, ABORT_PACKET, ASSERT_LAST_ABORTED, WAIT_FOR_LAST, WD_RESET, READ_MGMT_PAYLOAD, POST_MGMT_PAYLOAD);
    signal input_ready_int : std_logic;
    signal data_in         : std_logic_vector(8 - 1 downto 0);
    signal input_fsm       : t_input_fsm;

    -- READ STATUS
    type t_read_fsm is (NEW_FRAME, PAYLOAD_READING, FRAME_END);
    signal read_fsm : t_read_fsm;

    -- SPLIT STATUS
    type t_packet_fsm is (NO_SPLIT, SPLITTED);
    signal packet_fsm : t_packet_fsm;

    -- ERROR STATUS
    type t_packet_error_fsm is (NO_ERROR, ERROR);
    signal packet_error_fsm : t_packet_error_fsm;

    -- STATISTICS

    -- FRAME HEADER
    --type frame_header_ram_t is array(C_FRAME_HEADER_SIZE-1 downto 0) of std_logic_vector(7 downto 0);
    signal frame_header_ram    : frame_header_ram_t;
    signal frame_header_fields : frame_header_fields_t;

    -- PAYLOAD
    signal payload_header        : std_logic_vector(32 - 1 downto 0);
    signal payload_seq_num       : unsigned(10 - 1 downto 0);
    signal pre_payload_seq_num   : unsigned(10 - 1 downto 0);
    signal payload_len_remaining : unsigned(14 - 1 downto 0);
    signal payload_preamble      : std_logic_vector(7 downto 0);
    -- MGMT PAYLOAD
    signal mgmt_payload          : mgmt_payload_t;
    signal mgmt_payload_ram      : mgmt_payload_ram_t;

    -- PACKET
    signal packet_header        : std_logic_vector(32 - 1 downto 0);
    signal packet_len           : std_logic_vector(14 - 1 downto 0);
    --signal packet_preamble          : std_logic_vector(15 downto 0);
    signal packet_len_remaining : unsigned(14 - 1 downto 0);
    type packet_ram_t is array (1550 - 1 downto 0) of std_logic_vector(7 downto 0);
    signal packet_ram           : packet_ram_t;
    signal packet_ram_ptr       : integer range 0 to 1550;
    signal packet_ram_rd        : integer range 0 to 1550;

    -- COUNTERS:
    signal header_cnt       : unsigned(5 - 1 downto 0);
    signal frame_cnt        : unsigned(12 - 1 downto 0);
    signal packet_len_cnt   : unsigned(14 - 1 downto 0);
    signal frame_header_cnt : unsigned(5 - 1 downto 0); -- Increased 1 bit for SDA V3
    signal mgmt_payload_cnt : unsigned(5 - 1 downto 0);

    -- AXI stream (1 clock delay)
    signal r_s_axis_tdata  : std_logic_vector(8 - 1 downto 0);
    signal r_s_axis_tvalid : std_logic;
    signal r_s_axis_tlast  : std_logic;
    signal fifo_rst        : std_logic;

    signal s_axis_tready_aux : std_logic;
    signal packet_32b_align  : std_logic;

    signal last_flag             : std_logic;
    signal watchdog_timer_enable : std_logic;
    signal timeout_cnt           : unsigned(32 - 1 downto 0);
    signal wd_reset_counter      : unsigned(32 - 1 downto 0);
    signal watchdog_activate     : std_logic;
    signal watchdog_reset        : std_logic;

    -- Statistics counters
    signal r_total_packet_counter          : unsigned(31 downto 0);
    signal r_total_payload_counter         : unsigned(31 downto 0);
    signal r_total_packet_splitted_counter : unsigned(31 downto 0);
    signal r_total_packet_merged_counter   : unsigned(31 downto 0);
    signal r_total_data_frame_counter      : unsigned(31 downto 0);
    signal r_total_mgmt_frame_counter      : unsigned(31 downto 0);
    signal r_total_idle_frame_counter      : unsigned(31 downto 0);
    signal r_total_payload_error_counter   : unsigned(31 downto 0);
    signal r_total_packet_error_counter    : unsigned(31 downto 0);
    signal r_watchdog_reset_counter        : unsigned(31 downto 0);

    signal pkt_data_in          : std_logic_vector(7 downto 0);
    signal pkt_data_combined_in : std_logic_vector(8 downto 0);
    signal pkt_data_out         : std_logic_vector(7 downto 0);
    signal pkt_user_in          : std_logic;
    signal pkt_valid_in         : std_logic;
    signal pkt_ready_in         : std_logic;
    signal pkt_last_in          : std_logic;
    signal pkt_full_in          : std_logic;
    signal pkt_empty_in         : std_logic;

    signal packet_valid            : std_logic;
    signal packet_correct          : std_logic;
    signal last_asserted           : std_logic;
    signal wait_last_counter       : unsigned(3 downto 0) := (others => '0');
    signal mgmt_response_valid_int : std_logic;

    -- SDA DEBUG BUS
    signal sda_debug_int : sda_debug_t;

    -- DEBUG ONLY. WILL BE REMOVED
    --    signal word_data : std_logic_vector(31 downto 0);
    signal error_to_idle : std_logic;
    --
    --    -- Avoid coptimization for ILA - DEBUG ONLY. WILL BE REMOVED 
    attribute keep       : string;

    attribute keep of s_axis_tready_aux : signal is "true";
    attribute keep of error_to_idle : signal is "true";

    attribute keep of r_total_packet_counter : signal is "true";
    attribute keep of r_total_packet_splitted_counter : signal is "true";
    attribute keep of r_total_packet_merged_counter : signal is "true";
    --attribute keep of r_total_frame_counter: signal is "true";  
    --attribute keep of r_total_frame_error_counter: signal is "true";  
    attribute keep of r_total_packet_error_counter : signal is "true";
    attribute keep of input_fsm : signal is "true";
    attribute keep of packet_fsm : signal is "true";

    attribute keep of payload_header : signal is "true";
    attribute keep of payload_seq_num : signal is "true";
    attribute keep of pre_payload_seq_num : signal is "true";
    attribute keep of payload_len_remaining : signal is "true";
    attribute keep of payload_preamble : signal is "true";

    -- PACKET
    attribute keep of packet_header : signal is "true";
    attribute keep of packet_len : signal is "true";
    attribute keep of packet_len_remaining : signal is "true";

    -- COUNTERS:
    attribute keep of header_cnt : signal is "true";
    attribute keep of frame_cnt : signal is "true";
    attribute keep of packet_len_cnt : signal is "true";
    attribute keep of frame_header_cnt : signal is "true";

    -- AXI stream (1 clock delay)
    attribute keep of r_s_axis_tdata : signal is "true";
    attribute keep of r_s_axis_tvalid : signal is "true";
    attribute keep of r_s_axis_tlast : signal is "true";
    --attribute keep of r_total_packet_counter               : signal is "true";  
    attribute keep of r_total_payload_counter : signal is "true";
    --attribute keep of r_total_packet_splitted_counter      : signal is "true";  
    --attribute keep of r_total_packet_merged_counter        : signal is "true";  
    attribute keep of r_total_data_frame_counter : signal is "true";
    attribute keep of r_total_idle_frame_counter : signal is "true";
    attribute keep of r_total_payload_error_counter : signal is "true";
    --attribute keep of r_total_packet_error_counter         : signal is "true";   
    ---------------------------------------------------------------------
begin

    --payload_seq_num         <= payload_header(23 downto 14);
    --payload_len_remaining   <= payload_header(13 downto 0);
    --packet_len              <= packet_header(13 downto 0);
    --s_axis_tready <= s_axis_tready_aux;
    mgmt_response_valid                 <= mgmt_response_valid_int;
    --axis_payload_if_m.tx_num            <= (others => 'Z');
    axis_if_m.tuser                     <= '0';
    --axis_payload_if_m.axis_if_m.tstart  <= 'Z';

    Inst_packet_filter_top : entity ethernet_deframer_lib.packet_filter_top
        Port map(
            clk            => clk,
            rst            => fifo_rst,
            -- Input data from encoder
            ----------------------
            s_axis_tdata   => pkt_data_in,
            s_axis_tvalid  => pkt_valid_in,
            s_axis_tlast   => pkt_last_in,
            s_axis_tready  => pkt_ready_in,
            -- Output data to ethernet mac
            ----------------------
            m_axis_tdata   => axis_if_m.tdata,
            m_axis_tvalid  => axis_if_m.tvalid,
            m_axis_tlast   => axis_if_m.tlast,
            m_axis_tready  => axis_if_s.tready,
            packet_valid   => packet_valid,
            packet_correct => packet_correct,
            last_asserted  => last_asserted,
            packet_cnt_in  => packet_filter_cnt_in,
            packet_cnt_out => packet_filter_cnt_out
        );

    --        packet_filter_top_debug : entity xil_defaultlib.ila_axis_8bit
    --            PORT MAP(
    --                clk       => clk,
    --                probe0    => pkt_data_in,
    --                probe1(0) => pkt_valid_in,
    --                probe2(0) => pkt_ready_in,
    --                probe3(0) => pkt_last_in,
    --                probe4(0)    => packet_valid
    --            );

    -- Process input data FSM
    pr_input_ctrl : process(clk)
        variable v_payload_header : std_logic_vector(32 - 1 downto 0) := (others => '0');
        variable v_packet_header  : std_logic_vector(32 - 1 downto 0) := (others => '0');
        variable v_tx_fn          : std_logic_vector(15 downto 0);
        variable v_frame_type     : std_logic_vector(1 downto 0);

    begin
        if rising_edge(clk) then
            if rst = '1' then
                header_cnt          <= (others => '0');
                frame_cnt           <= (others => '0');
                packet_len_cnt      <= (others => '0');
                frame_header_cnt    <= (others => '0');
                mgmt_payload_cnt    <= (others => '0');
                --r_s_axis_tdata    <= (others => '0');
                input_fsm           <= START;
                packet_fsm          <= NO_SPLIT;
                packet_error_fsm    <= NO_ERROR;
                s_axis_tready_aux   <= '0';
                payload_header      <= (others => '0');
                packet_header       <= (others => '0');
                pre_payload_seq_num <= (others => '0');

                r_total_packet_counter          <= (others => '0');
                r_total_payload_counter         <= (others => '0');
                r_total_packet_splitted_counter <= (others => '0');
                r_total_packet_merged_counter   <= (others => '0');
                r_total_data_frame_counter      <= (others => '0');
                r_total_idle_frame_counter      <= (others => '0');
                r_total_mgmt_frame_counter      <= (others => '0');
                r_total_payload_error_counter   <= (others => '0');
                r_total_packet_error_counter    <= (others => '0');
                r_watchdog_reset_counter        <= (others => '0');
                wd_reset_counter                <= (others => '0');

                sda_debug_int.frame_header   <= C_FRAME_HEADER_INIT;
                sda_debug_int.payload_header <= (others => '0');
                sda_debug_int.packet_header  <= (others => '0');

                wait_last_counter       <= (others => '0');
                pkt_data_in             <= (others => '0');
                pkt_valid_in            <= '0';
                pkt_last_in             <= '0';
                pkt_user_in             <= '0';
                last_flag               <= '0';
                watchdog_reset          <= '0';
                packet_ram_ptr          <= 0;
                packet_ram_rd           <= 0;
                packet_valid            <= '0';
                packet_correct          <= '0';
                read_fsm                <= NEW_FRAME;
                error_to_idle           <= '0';

                mgmt_response_valid_int <= '0';

            else

                pkt_valid_in            <= '0';
                pkt_last_in             <= '0';
                pkt_user_in             <= '0';
                s_axis_tready_aux       <= '0';
                watchdog_reset          <= '0';
                packet_valid            <= '0';
                packet_correct          <= '0';
                wait_last_counter       <= (others => '0');
                --packet_ram_rd <= 0;
                --debug:
                error_to_idle           <= '0';
                mgmt_response_valid_int <= mgmt_response_valid_int;

                case input_fsm is

                    -- Detects beginning of new frame
                    when IDLE =>
                        header_cnt       <= (others => '0');
                        frame_cnt        <= (others => '0');
                        packet_len_cnt   <= (others => '0');
                        frame_header_cnt <= (others => '0');
                        mgmt_payload_cnt <= (others => '0');
                        v_packet_header  := (others => '0');
                        v_payload_header := (others => '0');
                        payload_header   <= (others => '0');
                        packet_header    <= (others => '0');
                        --r_s_axis_tdata    <= (others => '0');
                        input_fsm        <= IDLE;
                        --packet_fsm        <= NO_SPLIT;
                        --packet_error_fsm  <= NO_ERROR;
                        --pre_payload_seq_num <= (others => '0');
                        --packet_len <= (others => '0');

                        s_axis_tready_aux <= '1';
                        if r_s_axis_tvalid = '1' and r_s_axis_tlast = '1' and s_axis_tready_aux = '1' then
                            input_fsm         <= START;
                            s_axis_tready_aux <= '0';
                        end if;

                    when START =>
                        frame_header_cnt  <= (others => '0');
                        mgmt_payload_cnt  <= (others => '0');
                        header_cnt        <= (others => '0');
                        frame_cnt         <= (others => '0');
                        packet_header     <= (others => '0');
                        payload_header    <= (others => '0');
                        wd_reset_counter  <= (others => '0');
                        --packet_len        <= (others => '0');
                        s_axis_tready_aux <= '0';
                        packet_error_fsm  <= NO_ERROR;
                        read_fsm          <= NEW_FRAME;
                        if r_s_axis_tvalid = '1' and pkt_ready_in = '1' then
                            input_fsm               <= READ_PAYLOAD_METADATA;
                            mgmt_response_valid_int <= '0';
                        elsif watchdog_activate = '1' then
                            input_fsm                <= WD_RESET;
                            mgmt_response_valid_int  <= '0';
                            r_watchdog_reset_counter <= r_watchdog_reset_counter + 1;
                        end if;

                    when READ_PAYLOAD_METADATA =>
                        v_tx_fn      := axis_payload_if_m.tx_fn;
                        v_frame_type := axis_payload_if_m.frame_type;

                        --If Frame header type is IDLE, drop payload.
                        if v_frame_type = C_FRAME_IDLE then
                            input_fsm                  <= IDLE;
                            r_total_idle_frame_counter <= r_total_idle_frame_counter + 1;

                        elsif v_frame_type = C_FRAME_DATA then
                            input_fsm                  <= READ_PAYLOAD_HEADER;
                            r_total_data_frame_counter <= r_total_data_frame_counter + 1;

                        elsif v_frame_type = C_FRAME_MGMT then
                            input_fsm                  <= READ_MGMT_PAYLOAD;
                            r_total_mgmt_frame_counter <= r_total_mgmt_frame_counter + 1;
                        else
                            input_fsm                     <= IDLE;
                            r_total_payload_error_counter <= r_total_payload_error_counter + 1;
                            error_to_idle                 <= '1';
                        end if;

                    -- Reads MGMT payload.
                    when READ_MGMT_PAYLOAD =>
                        s_axis_tready_aux <= '0';
                        if r_s_axis_tvalid = '1' then
                            s_axis_tready_aux <= '1';
                        end if;
                        if r_s_axis_tvalid = '1' and r_s_axis_tlast = '0' and s_axis_tready_aux = '1' then

                            if mgmt_payload_cnt < C_MGMT_PAYLOAD_SIZE - 1 then
                                s_axis_tready_aux                              <= '1';
                                mgmt_payload_ram(to_integer(mgmt_payload_cnt)) <= r_s_axis_tdata;
                                mgmt_payload_cnt                               <= mgmt_payload_cnt + 1;
                            else
                                input_fsm                                      <= POST_MGMT_PAYLOAD;
                                mgmt_payload_ram(to_integer(mgmt_payload_cnt)) <= r_s_axis_tdata;
                                mgmt_payload_cnt                               <= (others => '0');
                                s_axis_tready_aux                              <= '0';
                            end if;
                        elsif r_s_axis_tvalid = '1' and r_s_axis_tlast = '1' and s_axis_tready_aux = '1' then
                            input_fsm         <= START;
                            s_axis_tready_aux <= '0';
                        end if;

                    when POST_MGMT_PAYLOAD =>
                        mgmt_response           <= array2mgmt_response(mgmt_payload_ram);
                        input_fsm               <= IDLE; --NO_PACKET; --PROCESS_MGMT_PAYLOAD;
                        mgmt_response_valid_int <= '1';


                    --------------------------------------------------------------------------------------------------
                    -- Reads Payload header, check preamble
                    when READ_PAYLOAD_HEADER =>
                        input_fsm         <= READ_PAYLOAD_HEADER;
                        s_axis_tready_aux <= '0';
                        if r_s_axis_tvalid = '1' then
                            s_axis_tready_aux <= '1';
                        end if;
                        if r_s_axis_tvalid = '1' and r_s_axis_tlast = '0' and s_axis_tready_aux = '1' then

                            if header_cnt < C_HEADER_SIZE - 1 then
                                s_axis_tready_aux                                             <= '1';
                                payload_header(payload_header'high downto payload_header'low) <= r_s_axis_tdata & payload_header(payload_header'high downto payload_header'low + 8);
                                header_cnt                                                    <= header_cnt + 1;
                                frame_cnt                                                     <= frame_cnt + 1;
                            else
                                input_fsm                                                     <= POST_PAYLOAD_HEADER;
                                payload_header(payload_header'high downto payload_header'low) <= r_s_axis_tdata & payload_header(payload_header'high downto payload_header'low + 8);
                                frame_cnt                                                     <= frame_cnt + 1;
                                header_cnt                                                    <= (others => '0');
                                s_axis_tready_aux                                             <= '0';
                            end if;

                        elsif r_s_axis_tvalid = '1' and r_s_axis_tlast = '1' and s_axis_tready_aux = '1' then
                            input_fsm         <= START;
                            s_axis_tready_aux <= '0';
                        end if;

                    -- Logic to cover all the scenarios.   
                    when POST_PAYLOAD_HEADER =>
                        packet_error_fsm  <= NO_ERROR;
                        s_axis_tready_aux <= '0';
                        v_packet_header   := (others => '0');
                        header_cnt        <= (others => '0');

                        if payload_header(31 downto 24) /= C_PAYLOAD_PREAMBLE then
                            input_fsm <= ABORT_PACKET;
                        elsif payload_header(31 downto 24) = C_PAYLOAD_PREAMBLE then
                            input_fsm                    <= POST_PAYLOAD_HEADER;
                            read_fsm                     <= PAYLOAD_READING;
                            pre_payload_seq_num          <= unsigned(payload_header(23 downto 14));
                            r_total_payload_counter      <= r_total_payload_counter + 1;
                            --payload_seq_num     <= unsigned(payload_header(23 downto 14));
                            sda_debug_int.payload_header <= payload_header;
                            if unsigned(payload_header(23 downto 14)) = pre_payload_seq_num + 1 and unsigned(payload_header(13 downto 0)) = 0 then
                                input_fsm  <= READ_PACKET_HEADER;
                                packet_len <= (others => '0');
                                if packet_fsm = SPLITTED then
                                    packet_ram_ptr <= 0;
                                    packet_fsm     <= NO_SPLIT;
                                end if;

                            --unsigned(payload_header(13 downto 0)) >= 60 and unsigned(payload_header(13 downto 0)) <= 1550
                            elsif (unsigned(payload_header(23 downto 14)) = pre_payload_seq_num + 1) and ((unsigned(payload_header(13 downto 0)) > 0) and (unsigned(payload_header(13 downto 0)) < C_ETHERNET_PACKET_MAX_LENGTH_BYTES)) then
                                if (packet_len_remaining - 1) = unsigned(payload_header(13 downto 0)) then
                                    input_fsm                     <= WRITE_PACKET;
                                    r_total_packet_merged_counter <= r_total_packet_merged_counter + 1;

                                else
                                    if packet_fsm = SPLITTED then
                                        packet_ram_ptr <= 0;
                                        input_fsm      <= ASSERT_LAST_ABORTED;
                                        packet_fsm     <= NO_SPLIT;
                                    --                                    assert false report "ASSERT_LAST_ABORTED!"  
                                    --                                    severity failure;
                                    else
                                        input_fsm        <= WRITE_PACKET;
                                        packet_error_fsm <= ERROR;
                                    end if;
                                end if;

                            elsif unsigned(payload_header(23 downto 14)) /= pre_payload_seq_num + 1 and unsigned(payload_header(13 downto 0)) = 0 then
                                -- frame lost but new packet 
                                input_fsm  <= READ_PACKET_HEADER;
                                packet_len <= (others => '0');
                                if packet_fsm = SPLITTED then
                                    input_fsm  <= ASSERT_LAST_ABORTED;
                                    packet_fsm <= NO_SPLIT;
                                end if;

                            elsif unsigned(payload_header(23 downto 14)) /= pre_payload_seq_num + 1 and (unsigned(payload_header(13 downto 0)) > 0 and unsigned(payload_header(13 downto 0)) < C_ETHERNET_PACKET_MAX_LENGTH_BYTES) then
                                -- frame lost and packet splitted lost aswell.
                                --input_fsm  <= WRITE_PACKET;
                                --packet_error_fsm  <= ERROR;
                                packet_ram_ptr               <= 0;
                                packet_len                   <= payload_header(13 downto 0);
                                packet_len_cnt               <= (others => '0');
                                r_total_packet_error_counter <= r_total_packet_error_counter + 1;
                                if packet_fsm = SPLITTED then
                                    packet_ram_ptr <= 0;
                                    input_fsm      <= ASSERT_LAST_ABORTED;
                                    packet_fsm     <= NO_SPLIT;
                                else
                                    input_fsm        <= WRITE_PACKET;
                                    packet_error_fsm <= ERROR;
                                end if;

                            else
                                input_fsm                     <= IDLE;
                                r_total_payload_error_counter <= r_total_payload_error_counter + 1;
                                error_to_idle                 <= '1';
                            end if;
                        end if;

                    -- Read packet header, check preamble
                    when READ_PACKET_HEADER =>
                        input_fsm         <= READ_PACKET_HEADER;
                        packet_len_cnt    <= (others => '0');
                        s_axis_tready_aux <= '0';
                        if r_s_axis_tvalid = '1' then
                            s_axis_tready_aux <= '1';
                        end if;
                        if r_s_axis_tvalid = '1' and r_s_axis_tlast = '0' and s_axis_tready_aux = '1' then

                            if header_cnt < C_HEADER_SIZE - 1 then
                                s_axis_tready_aux <= '1';
                                packet_header     <= r_s_axis_tdata & packet_header(packet_header'high downto packet_header'low + 8);
                                header_cnt        <= header_cnt + 1;
                                frame_cnt         <= frame_cnt + 1;
                            else
                                input_fsm         <= POST_PACKET_HEADER;
                                packet_header     <= r_s_axis_tdata & packet_header(packet_header'high downto packet_header'low + 8);
                                header_cnt        <= (others => '0');
                                s_axis_tready_aux <= '0';
                                frame_cnt         <= frame_cnt + 1;
                            end if;

                        elsif r_s_axis_tvalid = '1' and r_s_axis_tlast = '1' and s_axis_tready_aux = '1' then
                            input_fsm         <= START;
                            s_axis_tready_aux <= '0';
                        end if;

                    when POST_PACKET_HEADER =>
                        s_axis_tready_aux           <= '0';
                        header_cnt                  <= (others => '0');
                        sda_debug_int.packet_header <= packet_header;
                        if packet_header(31 downto 16) /= C_PACKET_PREAMBLE then
                            input_fsm     <= NO_PACKET;
                            packet_header <= (others => '0');
                        elsif packet_header(31 downto 16) = C_PACKET_PREAMBLE then
                            if unsigned(packet_header(13 downto 0)) > 59 and unsigned(packet_header(13 downto 0)) < 1550 then
                                input_fsm              <= WAIT_READY;
                                packet_ram_ptr         <= 0;
                                packet_len             <= packet_header(13 downto 0);
                                packet_header          <= (others => '0');
                                r_total_packet_counter <= r_total_packet_counter + 1;
                            else
                                input_fsm                    <= IDLE;
                                r_total_packet_error_counter <= r_total_packet_error_counter + 1;
                                error_to_idle                <= '1';
                                --packet_len <= packet_header(13 downto 0);
                                packet_header                <= (others => '0');
                            end if;
                        end if;

                    when WAIT_READY =>
                        if pkt_ready_in = '1' then
                            input_fsm <= WRITE_PACKET;
                        end if;

                    -- Write packet data. If finished, check 32bit align for next packet to read. If payload end reached, does not assert tlast
                    -- and sets SPLITTED status so the next payload can merge the rest of the packet.
                    when WRITE_PACKET =>
                        input_fsm         <= WRITE_PACKET;
                        s_axis_tready_aux <= '0';
                        if r_s_axis_tvalid = '1' then
                            s_axis_tready_aux <= '1';
                        end if;

                        if r_s_axis_tvalid = '1' and r_s_axis_tlast = '0' and s_axis_tready_aux = '1' then
                            s_axis_tready_aux <= '1';
                            if packet_error_fsm = NO_ERROR then
                                pkt_data_in  <= r_s_axis_tdata;
                                pkt_valid_in <= '1';
                            end if;
                            packet_len_cnt    <= packet_len_cnt + 1;
                            frame_cnt         <= frame_cnt + 1;
                            -- Check end of packet:
                            -- Packet end reached, go to CHECK_32_ALIGN before
                            if packet_len_cnt = unsigned(packet_len) - 1 then
                                s_axis_tready_aux    <= '0';
                                packet_fsm           <= NO_SPLIT;
                                --input_fsm            <= CHECK_32_ALIGN;
                                input_fsm            <= SEND_PACKET;
                                pkt_last_in          <= '1';
                                --                            if packet_ram_ptr > 0 then
                                --                                input_fsm            <= WAIT_PACKET_SENT;
                                --                                packet_ram_rd <= 0;
                                --                            else
                                --                                input_fsm            <= IDLE;
                                --                            end if;
                                packet_error_fsm     <= NO_ERROR;
                                packet_len_remaining <= (others => '0');
                            end if;

                        -- End of frame, check if packet is splitted
                        elsif r_s_axis_tvalid = '1' and r_s_axis_tlast = '1' and s_axis_tready_aux = '1' then
                            --pkt_data_in    <= r_s_axis_tdata;
                            if packet_error_fsm = NO_ERROR then
                                pkt_data_in  <= r_s_axis_tdata;
                                pkt_valid_in <= '1';
                            end if;
                            packet_len_cnt <= packet_len_cnt + 1;
                            frame_cnt      <= frame_cnt + 1;
                            read_fsm       <= FRAME_END;
                            -- packet not splitted
                            if packet_len_cnt = unsigned(packet_len) - 1 then
                                s_axis_tready_aux <= '0';
                                packet_fsm        <= NO_SPLIT;
                                pkt_last_in       <= '1';
                                input_fsm         <= SEND_PACKET;
                                --                            if packet_ram_ptr > 0 then

                                --                                packet_ram_rd        <= 0;
                                --                            else
                                --                                input_fsm            <= START;
                                --                            end if;
                                packet_error_fsm     <= NO_ERROR;
                                packet_len_remaining <= (others => '0');
                            -- 
                            else
                                if unsigned(packet_len) /= 0 and unsigned(packet_len) > packet_len_cnt then
                                    --hold data:
                                    s_axis_tready_aux               <= '0';
                                    packet_fsm                      <= SPLITTED;
                                    r_total_packet_splitted_counter <= r_total_packet_splitted_counter + 1;
                                    input_fsm                       <= START;
                                    packet_error_fsm                <= NO_ERROR;
                                    -- save packet length remaining for next frame
                                    packet_len_remaining            <= unsigned(packet_len) - packet_len_cnt;
                                else
                                    --hold data:
                                    s_axis_tready_aux <= '0';
                                    packet_fsm        <= NO_SPLIT;
                                    input_fsm         <= START;
                                    packet_error_fsm  <= NO_ERROR;
                                end if;
                            end if;

                        end if;
                    --packet_error_fsm     <= NO_ERROR;
                    when SEND_PACKET =>
                        s_axis_tready_aux <= '0';
                        if last_asserted = '1' then
                            if packet_error_fsm = NO_ERROR then
                                packet_valid   <= '1';
                                packet_correct <= '1';
                            else
                                packet_error_fsm <= NO_ERROR;
                            end if;
                            -- Check if the end of the frame is reached and go to start. If not, 
                            -- continue reading. Fixes bug.
                            if read_fsm = FRAME_END then
                                input_fsm <= START;
                            else
                                input_fsm <= CHECK_32_ALIGN;
                            end if;
                        else
                            wait_last_counter <= wait_last_counter + 1;
                            if wait_last_counter = 4 then
                                input_fsm <= ASSERT_LAST_ABORTED;
                            end if;
                        end if;

                    when ABORT_PACKET =>
                        s_axis_tready_aux <= '0';
                        if last_asserted = '1' then
                            packet_valid   <= '1';
                            packet_correct <= '0';
                            if read_fsm = PAYLOAD_READING then
                                input_fsm <= CHECK_32_ALIGN;
                            else
                                input_fsm                     <= IDLE;
                                s_axis_tready_aux             <= '0';
                                r_total_payload_error_counter <= r_total_payload_error_counter + 1;
                                packet_fsm                    <= NO_SPLIT;
                            end if;

                        else
                            wait_last_counter <= wait_last_counter + 1;
                            if wait_last_counter = 4 then
                                input_fsm <= ASSERT_LAST_ABORTED;
                            end if;
                        end if;

                    when ASSERT_LAST_ABORTED =>
                        s_axis_tready_aux <= '0';
                        pkt_last_in       <= '1';
                        pkt_valid_in      <= '1';
                        input_fsm         <= WAIT_FOR_LAST;

                    when WAIT_FOR_LAST =>
                        s_axis_tready_aux <= '0';
                        input_fsm         <= ABORT_PACKET;

                    --                when WAIT_PACKET_SENT =>
                    --                    input_fsm <= WAIT_PACKET_SENT;
                    --                    if pkt_ready_in = '1' then
                    --                        if packet_ram_rd = packet_ram_ptr - 1  then
                    --                            pkt_data_in <= packet_ram(packet_ram_rd);
                    --                            packet_ram_rd <= packet_ram_rd + 1;
                    --                            pkt_valid_in <= '1';
                    --                            pkt_last_in <= '1';
                    --                        elsif packet_ram_rd = packet_ram_ptr then  
                    --                            --if pkt_empty_in = '1' then
                    --                            input_fsm <= CHECK_32_ALIGN;
                    --                            packet_ram_rd <= 0;
                    --                            packet_ram_ptr <= 0;
                    --                            --end if;                                              
                    --                        else
                    --                            pkt_data_in <= packet_ram(packet_ram_rd);
                    --                            packet_ram_rd <= packet_ram_rd + 1;
                    --                            pkt_valid_in <= '1';
                    --                        end if;
                    --                    end if;

                    -- Checks 32bit alignment before starting to read a new packet
                    when CHECK_32_ALIGN =>
                        input_fsm         <= CHECK_32_ALIGN;
                        s_axis_tready_aux <= '0'; --Hold data 

                        --if r_s_axis_tvalid = '1' then
                        --    s_axis_tready_aux <= '1';
                        --end if;

                        if r_s_axis_tvalid = '1' then
                            -- If alligned to 32bits, go to read packet header
                            if packet_32b_align = '1' then

                                if r_s_axis_tvalid = '1' and r_s_axis_tlast = '1' and s_axis_tready_aux = '1' then
                                    frame_cnt         <= frame_cnt + 1;
                                    input_fsm         <= START;
                                    s_axis_tready_aux <= '0';
                                else
                                    input_fsm         <= READ_PACKET_HEADER;
                                    s_axis_tready_aux <= '0';
                                end if;
                            -- If not alligned to 32bits, dump bytes until aligned. Check end of frame.
                            else
                                if r_s_axis_tvalid = '1' and r_s_axis_tlast = '1' then
                                    frame_cnt         <= frame_cnt + 1;
                                    input_fsm         <= START;
                                    s_axis_tready_aux <= '0';
                                elsif r_s_axis_tvalid = '1' and r_s_axis_tlast = '0' then
                                    frame_cnt         <= frame_cnt + 1;
                                    s_axis_tready_aux <= '1';
                                end if;
                            end if;

                        end if;

                    -- Asserts tlast to end current packet (splitted) after receiving a new with not consecutive SEQ_NUM.
                    -- asserts s_tready to offload the bad packet from the master???
                    --                when BAD_FRAME =>
                    --                    s_axis_tready_aux   <= '0';
                    --                    r_total_payload_error_counter <= r_total_payload_error_counter + 1;
                    --                
                    --                    if packet_fsm = SPLITTED then
                    --                        input_fsm <= IDLE;
                    --                        packet_ram_ptr <= 0;
                    --                    else
                    --                        packet_ram_ptr <= 0;
                    --                        input_fsm <= IDLE;
                    --                    end if;
                    --                    packet_fsm <= NO_SPLIT;

                    -- If no packet header was found, It is asumed than there is no packets left in the rest of payload. Therefore, dump data from master.
                    when NO_PACKET =>
                        s_axis_tready_aux <= '0';
                        input_fsm         <= IDLE;
                        packet_ram_ptr    <= 0;
                        error_to_idle     <= '1';

                    when WD_RESET =>
                        wd_reset_counter <= wd_reset_counter + 1;
                        watchdog_reset   <= '1';
                        if wd_reset_counter > C_WD_RESET then
                            input_fsm        <= START;
                            watchdog_reset   <= '0';
                            wd_reset_counter <= (others => '0');
                        end if;
                        if wd_reset_counter > C_WD_RESET - 5 then
                            watchdog_reset <= '0';
                        end if;

                    when others =>
                        header_cnt        <= (others => '0');
                        frame_cnt         <= (others => '0');
                        packet_len_cnt    <= (others => '0');
                        frame_header_cnt  <= (others => '0');
                        input_fsm         <= IDLE;
                        s_axis_tready_aux <= '0';

                end case;

                --            if r_s_axis_tvalid = '1' and r_s_axis_tlast = '1' and s_axis_tready_aux = '1' then
                --                last_flag <= '1';
                --            else
                --                last_flag <= '0';
                --            end if;
                --            if last_flag = '1' and input_fsm /= IDLE and input_fsm /= START then
                --                input_fsm <= IDLE;
                --            end if;

                --if clear_stat = '1' then
                --    r_total_packet_counter               <= (others =>'0');
                --    r_total_payload_counter              <= (others =>'0');
                --    r_total_packet_splitted_counter      <= (others =>'0');
                --    r_total_packet_merged_counter        <= (others =>'0');
                --    r_total_data_frame_counter           <= (others =>'0'); 
                --    r_total_idle_frame_counter           <= (others =>'0'); 
                --    r_total_payload_error_counter        <= (others =>'0'); 
                --    r_total_packet_error_counter         <= (others =>'0'); 
                --end if;
                -- STATISTICS
                total_packet_counter          <= std_logic_vector(r_total_packet_counter);
                total_payload_counter         <= std_logic_vector(r_total_payload_counter);
                total_packet_splitted_counter <= std_logic_vector(r_total_packet_splitted_counter);
                total_packet_merged_counter   <= std_logic_vector(r_total_packet_merged_counter);
                total_data_frame_counter      <= std_logic_vector(r_total_data_frame_counter);
                total_idle_frame_counter      <= std_logic_vector(r_total_idle_frame_counter);
                total_payload_error_counter   <= std_logic_vector(r_total_payload_error_counter);
                total_packet_error_counter    <= std_logic_vector(r_total_packet_error_counter);
                watchdog_reset_counter        <= std_logic_vector(r_watchdog_reset_counter);

            end if;
        end if;

    end process;

    -- Time out packet process
    pr_watchdog_timeout : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                timeout_cnt       <= (others => '0');
                watchdog_activate <= '0';
            else
                if watchdog_timer_enable = '1' then
                    timeout_cnt <= timeout_cnt + 1;
                else
                    timeout_cnt <= (others => '0');
                end if;

                if timeout_cnt >= C_WD_TIMEOUT then
                    watchdog_activate <= '1';
                else
                    watchdog_activate <= '0';
                end if;

            end if;
        end if;
    end process;

    packet_32b_align      <= '1' when (to_integer(frame_cnt) mod 4 = 0) else '0';
    --watchdog_timer_enable <= '1' when pkt_empty_in = '0' and input_fsm = START else '0';
    watchdog_timer_enable <= '0';
    fifo_rst              <= watchdog_reset or rst;

    r_s_axis_tvalid                     <= axis_payload_if_m.axis_if_m.tvalid;
    axis_payload_if_s.axis_if_s.tready  <= s_axis_tready_aux;
    r_s_axis_tdata                      <= axis_payload_if_m.axis_if_m.tdata;
    r_s_axis_tlast                      <= axis_payload_if_m.axis_if_m.tlast;

    sda_debug <= sda_debug_int;

end Behavioral;
