-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: ethernet_deframer_top
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Converts FSO frames to Ethernet packets in AXI-Stream format.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

library ethernet_deframer_lib;
use ethernet_deframer_lib.pkg_components.all;
library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;
library monitoring_lib;
library share_let_1g;
use share_let_1g.pkg_components.axi4s_fifo_v2;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ethernet_deframer_top is
    Port(
        clk                           : in    std_logic;
        rst                           : in    std_logic;
        -- Input data from encoder
        ----------------------
        axis_payload_if_m               : in t_axis_payload_if_m;
        axis_payload_if_s               : out t_axis_payload_if_s;

        -- Output data to ethernet mac
        ----------------------
        axis_if_m                       : out t_axis_if_m;
        axis_if_s                       : in t_axis_if_s;

        -- Config 
        ----------------------

        -- Statistics
        ----------------------
        clear_stat                    : in    std_logic;
        total_packet_counter          : out   std_logic_vector(31 downto 0);
        total_payload_counter         : out   std_logic_vector(31 downto 0);
        total_packet_splitted_counter : out   std_logic_vector(31 downto 0);
        total_packet_merged_counter   : out   std_logic_vector(31 downto 0);
        total_data_frame_counter      : out   std_logic_vector(31 downto 0);
        total_idle_frame_counter      : out   std_logic_vector(31 downto 0);
        total_payload_error_counter   : out   std_logic_vector(31 downto 0);
        total_packet_error_counter    : out   std_logic_vector(31 downto 0);
        watchdog_reset_counter        : out   std_logic_vector(31 downto 0);
        packet_filter_cnt_in          : out   std_logic_vector(31 downto 0);
        packet_filter_cnt_out         : out   std_logic_vector(31 downto 0);
        rx_fifo_skipped_frame         : out   std_logic_vector(31 downto 0);
        bitrate_out                   : out   std_logic_vector(31 downto 0);
        packet_cnt_out                : out   std_logic_vector(31 downto 0);
        sda_debug                     : out   sda_debug_t
    );
end ethernet_deframer_top;

architecture Behavioral of ethernet_deframer_top is

    constant C_FIFO_INPUT_FULL_DEPTH   : natural := 16384;
    constant C_FIFO_INPUT_FULL_THRESH  : natural := C_FIFO_INPUT_FULL_DEPTH * 3 / 4; -- 
    constant C_FIFO_INPUT_EMPTY_THRESH : natural := C_FIFO_INPUT_FULL_DEPTH * 1 / 5; -- 20%

    constant C_FIFO_OUTPUT_FULL_DEPTH   : natural := 16384;
    constant C_FIFO_OUTPUT_FULL_THRESH  : natural := C_FIFO_INPUT_FULL_DEPTH * 3 / 4; -- 
    constant C_FIFO_OUTPUT_EMPTY_THRESH : natural := C_FIFO_INPUT_FULL_DEPTH * 1 / 5; -- 20%

    constant C_METADATA_LENGTH : natural := axis_payload_if_m.tx_fn'length + axis_payload_if_m.frame_type'length + axis_payload_if_m.tx_num'length;

    -- AXI INPUT FIFO
    signal r_s_axis_tdata  : std_logic_vector(8 - 1 downto 0);
    signal r_s_axis_tvalid : std_logic;
    signal r_s_axis_tlast  : std_logic;
    signal r_s_axis_tready : std_logic;

    -- AXI OUTPUT FIFO
    signal r_m_axis_tdata      : std_logic_vector(8 - 1 downto 0);
    signal r_m_axis_tvalid     : std_logic;
    signal r_m_axis_tvalid_int : std_logic;
    signal r_m_axis_tlast      : std_logic;
    signal r_m_axis_tready     : std_logic;
    signal r_m_axis_tready_int : std_logic;
    signal r_m_axis_tuser      : std_logic;

    signal s_axis_tdata_int  : std_logic_vector(8 - 1 downto 0);
    signal s_axis_tvalid_int : std_logic;
    signal s_axis_tlast_int  : std_logic;
    signal s_axis_tready_int : std_logic;

    signal m_axis_tdata_int  : std_logic_vector(8 - 1 downto 0);
    signal m_axis_tvalid_int : std_logic;
    signal m_axis_tlast_int  : std_logic;
    signal m_axis_tready_int : std_logic;

    signal prebuffer_axis_tdata  : std_logic_vector(8 - 1 downto 0);
    signal prebuffer_axis_tvalid : std_logic;
    signal prebuffer_axis_tlast  : std_logic;
    signal prebuffer_axis_tready : std_logic;

    signal pkt_filter_axis_tdata  : std_logic_vector(8 - 1 downto 0);
    signal pkt_filter_axis_tvalid : std_logic;
    signal pkt_filter_axis_tlast  : std_logic;
    signal pkt_filter_axis_tready : std_logic;
    signal pkt_filter_axis_tuser  : std_logic;

    signal input_almost_full_int  : std_logic;
    signal output_almost_full_int : std_logic;
    signal fifo_1_almost_full_int : std_logic;
    signal input_almost_empty_int : std_logic;
    signal input_read_en          : std_logic;
    signal output_read_en         : std_logic;
    signal mac_valid_int          : std_logic;
    signal rx_fifo_skip_cnt       : unsigned(31 downto 0);
    signal axis_wr_data_count     : std_logic_vector(15 downto 0);
    signal axis_rd_data_count     : std_logic_vector(15 downto 0);

    signal axis_prog_full          : std_logic;
    signal back_pressure_high      : std_logic;
    signal back_pressure_low       : std_logic;
    signal fifo_input_almost_full  : std_logic;
    signal fifo_input_almost_empty : std_logic;

    signal mgmt_response_int       : mgmt_payload_t;
    signal mgmt_response_valid_int : std_logic;

    constant C_COUNTER_DELAY       : unsigned(15 downto 0) := x"3186"; --x"2060"; --4144*2; -- PATH DELAY
    constant C_COUNTER_PERIOD      : unsigned(15 downto 0) := x"103C"; -- DELAY BETWEEN TWO FRAMES
    signal cfg_ranging_sync_delay  : unsigned(15 downto 0);
    signal cfg_ranging_sync_period : unsigned(15 downto 0);
    signal counter_delay           : unsigned(15 downto 0);
    signal counter_period          : unsigned(15 downto 0);
    signal counter_reset           : unsigned(7 downto 0);
    signal start_flag_delay_1      : std_logic;
    signal start_flag_delay_2      : std_logic;
    signal switch_flag             : std_logic;
    signal rst_sync                : std_logic;
    signal rx_ts_rd_en             : std_logic;
    signal timestamp_valid_rx      : std_logic;
    signal timestamp_valid_rx_out  : std_logic;
    signal ts_delay_rd_en          : std_logic;
    signal control_delay_rd_en     : std_logic;
    signal deframer_ts_delay_rd_en : std_logic;
    signal control_input_valid     : std_logic;
    signal deframer_ts_delay_valid : std_logic;

    signal metadata_in        : std_logic_vector(c_metadata_length - 1 downto 0);
    signal metadata_valid     : std_logic;
    signal metadata_buf       : std_logic_vector(c_metadata_length - 1 downto 0);
    signal metadata_buf_valid : std_logic;
    signal metadata_buf_ready : std_logic;

    signal r_m_axis_m         : t_axis_if_m;
    signal r_m_axis_s         : t_axis_if_s;
    signal r_s_axis_payload_m : t_axis_payload_if_m;
    signal r_s_axis_payload_s : t_axis_payload_if_s;

begin
    metadata_buf_ready            <= r_s_axis_payload_m.axis_if_m.tvalid and r_s_axis_payload_m.axis_if_m.tlast and r_s_axis_payload_s.axis_if_s.tready;
    metadata_in                   <= axis_payload_if_m.tx_num & axis_payload_if_m.frame_type & axis_payload_if_m.tx_fn;
    metadata_valid                <= axis_payload_if_m.axis_if_m.tstart;
    r_s_axis_payload_m.tx_fn        <= metadata_buf(15 downto 0);
    r_s_axis_payload_m.frame_type   <= metadata_buf(17 downto 16);
    r_s_axis_payload_m.tx_num       <= metadata_buf(20 downto 18);
    --axis_payload_if_m.axis_if_m.tuser <= '0';

    -- Inst FIFO Input 
    Inst_axi_output_fifo_buffer : entity share_let_1g.axi4s_fifo_v2
        generic map(
            DISTR_RAM         => false,
            FIFO_DEPTH        => C_FIFO_OUTPUT_FULL_DEPTH,
            DATA_WIDTH        => 8,
            FULL_THRESHOLD    => C_FIFO_OUTPUT_FULL_THRESH,
            EMPTY_THRESHOLD   => C_FIFO_OUTPUT_EMPTY_THRESH,
            USE_OUTPUT_BUFFER => false
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => r_m_axis_m.tdata,
            input_valid        => r_m_axis_m.tvalid,
            input_last         => r_m_axis_m.tlast,
            input_user         => r_m_axis_m.tuser,
            input_start        => r_m_axis_m.tstart,
            input_ready        => r_m_axis_s.tready,
            input_almost_full  => open,
            input_almost_empty => open,
            fifo_max_level     => open,
            -- Output data handling
            -----------------------
            output             => axis_if_m.tdata,
            output_valid       => axis_if_m.tvalid,
            output_last        => axis_if_m.tlast,
            output_user        => axis_if_m.tuser,
            output_start       => axis_if_m.tstart,
            output_ready       => axis_if_s.tready
        );

    Inst_axi_input_fifo_buffer : entity share_let_1g.axi4s_fifo_v2
        generic map(
            DISTR_RAM         => false,
            FIFO_DEPTH        => C_FIFO_INPUT_FULL_DEPTH,
            DATA_WIDTH        => 8,
            FULL_THRESHOLD    => C_FIFO_INPUT_FULL_THRESH,
            EMPTY_THRESHOLD   => C_FIFO_INPUT_EMPTY_THRESH,
            USE_OUTPUT_BUFFER => false
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => axis_payload_if_m.axis_if_m.tdata,
            input_valid        => axis_payload_if_m.axis_if_m.tvalid,
            input_last         => axis_payload_if_m.axis_if_m.tlast,
            input_ready        => axis_payload_if_s.axis_if_s.tready,
            input_user         => '0',
            input_start        => axis_payload_if_m.axis_if_m.tstart,
            input_almost_full  => fifo_input_almost_full,
            input_almost_empty => fifo_input_almost_empty,
            fifo_max_level     => open,
            -- Output data handling
            -----------------------
            output             => r_s_axis_payload_m.axis_if_m.tdata,
            output_valid       => r_s_axis_payload_m.axis_if_m.tvalid,
            output_last        => r_s_axis_payload_m.axis_if_m.tlast,
            output_ready       => r_s_axis_payload_s.axis_if_s.tready,
            output_user        => open,
            output_start       => r_s_axis_payload_m.axis_if_m.tstart
        );

    -- FIFO buffer for metadata. Only frame_type is used
    inst_metadata_buffer_output : entity share_let_1g.axi4s_fifo
        generic map(
            DISTR_RAM         => true,
            FIFO_DEPTH        => 64,
            DATA_WIDTH        => C_METADATA_LENGTH, -- tx_fn+frame_type+tx_num
            FULL_THRESHOLD    => 64,
            EMPTY_THRESHOLD   => 0,
            USE_OUTPUT_BUFFER => false
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => metadata_in,
            input_valid        => metadata_valid,
            input_last         => '1',
            input_ready        => open,
            input_almost_full  => open,
            input_almost_empty => open,
            fifo_max_level     => open,
            -- Output data handling
            -----------------------
            output             => metadata_buf,
            output_valid       => metadata_buf_valid,
            output_last        => open,
            output_ready       => metadata_buf_ready -- to sync with the payload
        );

    -- instantiate Deframer here 
    inst_ethernet_deframer : entity ethernet_deframer_lib.ethernet_deframer
        port map(
            clk                           => clk, -- : in  std_logic;
            rst                           => rst, -- : in  std_logic;
            -- Input data from encoder
            ----------------------
            axis_payload_if_m               => r_s_axis_payload_m, --: inout t_axis_payload_if;
            axis_payload_if_s               => r_s_axis_payload_s, --: inout t_axis_payload_if;

            -- Output data to ethernet mac
            ----------------------
            axis_if_m                       => r_m_axis_m, --: inout t_axis_if;
            axis_if_s                       => r_m_axis_s, --: inout t_axis_if;

            mgmt_response                 => open, --mgmt_response_int,
            mgmt_response_valid           => open, --mgmt_response_valid_int,

            -- Statistics
            ----------------------
            clear_stat                    => clear_stat,
            total_packet_counter          => total_packet_counter, --: out std_logic_vector(31 downto 0);
            total_payload_counter         => total_payload_counter, --: out std_logic_vector(31 downto 0);
            total_packet_splitted_counter => total_packet_splitted_counter, --: out std_logic_vector(31 downto 0);
            total_packet_merged_counter   => total_packet_merged_counter, --: out std_logic_vector(31 downto 0);
            total_data_frame_counter      => total_data_frame_counter, --: out std_logic_vector(31 downto 0);
            total_idle_frame_counter      => total_idle_frame_counter, --: out std_logic_vector(31 downto 0);
            total_payload_error_counter   => total_payload_error_counter, --: out std_logic_vector(31 downto 0);
            total_packet_error_counter    => total_packet_error_counter, --: out std_logic_vector(31 downto 0);
            watchdog_reset_counter        => watchdog_reset_counter, --: out std_logic_vector(31 downto 0);
            packet_filter_cnt_in          => packet_filter_cnt_in,
            packet_filter_cnt_out         => packet_filter_cnt_out,
            sda_debug                     => sda_debug
        );

    inst_bitrate_calculator : entity monitoring_lib.bitrate_calculator
        Port map(
            clk              => clk,
            rst              => rst,
            -- Input data from encoder
            ----------------------
            s_axis_tdata     => (others => '0'),
            s_axis_tvalid    => r_m_axis_m.tvalid,
            s_axis_tlast     => r_m_axis_m.tlast,
            s_axis_tready    => r_m_axis_s.tready,
            bitrate          => bitrate_out,
            total_packet_cnt => packet_cnt_out
        );

end Behavioral;
