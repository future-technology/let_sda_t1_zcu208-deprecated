----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 03/09/2021 03:26:52 PM
-- Design Name: 
-- Module Name: pkg_components - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

package pkg_components is

    COMPONENT fifo_frames
        PORT(
            s_aclk         : IN  STD_LOGIC;
            s_aresetn      : IN  STD_LOGIC;
            s_axis_tvalid  : IN  STD_LOGIC;
            s_axis_tready  : OUT STD_LOGIC;
            s_axis_tdata   : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
            s_axis_tlast   : IN  STD_LOGIC;
            s_axis_tuser   : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            m_axis_tvalid  : OUT STD_LOGIC;
            m_axis_tready  : IN  STD_LOGIC;
            m_axis_tdata   : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            m_axis_tlast   : OUT STD_LOGIC;
            m_axis_tuser   : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            axis_prog_full : OUT STD_LOGIC
        );
    END COMPONENT;

    COMPONENT fifo_prebuffer
        PORT(
            m_aclk                 : IN  STD_LOGIC;
            s_aclk                 : IN  STD_LOGIC;
            s_aresetn              : IN  STD_LOGIC;
            s_axis_tvalid          : IN  STD_LOGIC;
            s_axis_tready          : OUT STD_LOGIC;
            s_axis_tdata           : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
            s_axis_tlast           : IN  STD_LOGIC;
            s_axis_tuser           : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            m_axis_tvalid          : OUT STD_LOGIC;
            m_axis_tready          : IN  STD_LOGIC;
            m_axis_tdata           : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            m_axis_tlast           : OUT STD_LOGIC;
            m_axis_tuser           : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            axis_prog_full_thresh  : IN  STD_LOGIC_VECTOR(14 DOWNTO 0);
            axis_prog_empty_thresh : IN  STD_LOGIC_VECTOR(14 DOWNTO 0);
            axis_wr_data_count     : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
            axis_rd_data_count     : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
            axis_prog_full         : OUT STD_LOGIC;
            axis_prog_empty        : OUT STD_LOGIC
        );
    END COMPONENT;

    COMPONENT packet_filter
        PORT(
            clk           : IN  STD_LOGIC;
            rst           : IN  STD_LOGIC;
            s_axis_tvalid : IN  STD_LOGIC;
            s_axis_tready : OUT STD_LOGIC;
            s_axis_tdata  : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
            s_axis_tlast  : IN  STD_LOGIC;
            s_axis_tuser  : IN  STD_LOGIC;
            m_axis_tvalid : OUT STD_LOGIC;
            m_axis_tready : IN  STD_LOGIC;
            m_axis_tdata  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            m_axis_tlast  : OUT STD_LOGIC;
            m_axis_tuser  : OUT STD_LOGIC
        );
    END COMPONENT;

    COMPONENT frame_filter
        PORT(
            clk            : IN  STD_LOGIC;
            rst            : IN  STD_LOGIC;
            s_axis_tvalid  : IN  STD_LOGIC;
            s_axis_tready  : OUT STD_LOGIC;
            s_axis_tdata   : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
            s_axis_tlast   : IN  STD_LOGIC;
            s_axis_tuser   : IN  STD_LOGIC;
            m_axis_tvalid  : OUT STD_LOGIC;
            m_axis_tready  : IN  STD_LOGIC;
            m_axis_tdata   : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            m_axis_tlast   : OUT STD_LOGIC;
            m_axis_tuser   : OUT STD_LOGIC;
            crc16_valid    : in  std_logic;
            crc16_correct  : in  std_logic;
            crc32_valid    : in  std_logic;
            crc32_correct  : in  std_logic;
            packet_cnt_in  : out std_logic_vector(31 downto 0);
            packet_cnt_out : out std_logic_vector(31 downto 0)
        );
    END COMPONENT;
    COMPONENT frame_filter_top
        PORT(
            clk           : IN  STD_LOGIC;
            rst           : IN  STD_LOGIC;
            s_axis_tvalid : IN  STD_LOGIC;
            s_axis_tready : OUT STD_LOGIC;
            s_axis_tdata  : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
            s_axis_tlast  : IN  STD_LOGIC;
            s_axis_tuser  : IN  STD_LOGIC;
            m_axis_tvalid : OUT STD_LOGIC;
            m_axis_tready : IN  STD_LOGIC;
            m_axis_tdata  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            m_axis_tlast  : OUT STD_LOGIC;
            m_axis_tuser  : OUT STD_LOGIC;
            crc16_valid   : in  std_logic;
            crc16_correct : in  std_logic;
            crc32_valid   : in  std_logic;
            crc32_correct : in  std_logic
        );
    END COMPONENT;

    COMPONENT dc_fifo_eth_side
        PORT(
            m_aclk        : IN  STD_LOGIC;
            s_aclk        : IN  STD_LOGIC;
            s_aresetn     : IN  STD_LOGIC;
            s_axis_tvalid : IN  STD_LOGIC;
            s_axis_tready : OUT STD_LOGIC;
            s_axis_tdata  : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
            s_axis_tlast  : IN  STD_LOGIC;
            s_axis_tuser  : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            m_axis_tvalid : OUT STD_LOGIC;
            m_axis_tready : IN  STD_LOGIC;
            m_axis_tdata  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            m_axis_tlast  : OUT STD_LOGIC;
            m_axis_tuser  : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
        );
    END COMPONENT;

end package pkg_components;

