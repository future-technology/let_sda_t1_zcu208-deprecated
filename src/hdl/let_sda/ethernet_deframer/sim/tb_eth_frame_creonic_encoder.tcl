exec xvlog --incr --relax -prj tb_eth_frame_creonic_encoder_vlog.prj -log xvlog.log

exec xvhdl --incr --relax -prj tb_eth_frame_creonic_encoder_vhdl.prj -log xvhdl.log

exec xelab --incr --debug typical --relax --mt 2 -L fifo_generator_v13_2_3 -L xil_defaultlib -L gtwizard_ultrascale_v1_7_5 -L axis_infrastructure_v1_1_0 -L axis_register_slice_v1_1_18 -L axis_dwidth_converter_v1_1_17 -L frame_decoding -L frame_encoding -L dec_rs_239_255 -L enc_rs_239_255 -L ethernet_deframer_lib -L ethernet_framer_lib -L unisims_ver -L unimacro_ver -L secureip -L xpm --snapshot tb_eth_frame_creonic_encoder_behav ethernet_deframer_lib.tb_eth_frame_creonic_encoder ethernet_deframer_lib.glbl -log elaborate.log

#xsim tb_eth_frame_creonic_encoder_behav -key {Behavioral:sim_1:Functional:tb_eth_frame_creonic_encoder} -tclbatch tb_eth_frame_creonic_encoder.tcl -log simulate.log

# run in GUI mode
exec xsim tb_eth_frame_creonic_encoder_behav -wdb frame_top_xsim.wdb -gui

