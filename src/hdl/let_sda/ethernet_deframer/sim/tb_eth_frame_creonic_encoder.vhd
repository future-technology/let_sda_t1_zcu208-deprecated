----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/10/2021 08:19:11 AM
-- Design Name: 
-- Module Name: tb_eth_frame_creonic_encoder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use std.textio.all;
use IEEE.std_logic_textio;
use ieee.math_real.uniform;
use ieee.math_real.floor;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library ethernet_framer_lib;
use ethernet_framer_lib.pkg_components.all;
use ethernet_framer_lib.txt_util.all;
library ethernet_deframer_lib;

library frame_encoding;
library frame_decoding;
library sda_lib;
use sda_lib.pkg_sda.all;
library let_sda_lib;
use let_sda_lib.let_pckg_components.frame_health_monitor;

entity tb_eth_frame_creonic_encoder is
	generic(

	-- Which level has an assertion when it fails?
	ASSERTION_SEVERITY  : severity_level := FAILURE

	);
end tb_eth_frame_creonic_encoder;

architecture Behavioral of tb_eth_frame_creonic_encoder is

constant CLK_PERIOD  : time    := 5 ns;    
constant NUM_FRAME   : natural := 12000;
signal FRAME_SIZE  : natural := 60;

signal clk            : std_logic := '0';
signal rst            : std_logic := '1';
signal rstn           : std_logic;

signal tx_axis_tdata     : std_logic_vector(8 - 1 downto 0);
signal tx_axis_tvalid    : std_logic;
signal tx_axis_tlast     : std_logic;
signal tx_axis_tready    : std_logic;
signal tx_axis_tuser    : std_logic;

signal rx_axis_tdata     : std_logic_vector(8 - 1 downto 0);
signal rx_axis_tvalid    : std_logic;
signal rx_axis_tlast     : std_logic;
signal rx_axis_tready    : std_logic;

signal m_axis_tdata  : std_logic_vector(8 - 1 downto 0);
signal m_axis_tvalid : std_logic;
signal m_axis_tlast  : std_logic;
signal m_axis_tready : std_logic;

signal s_axis_tdata  : std_logic_vector(8 - 1 downto 0);
signal s_axis_tvalid : std_logic;
signal s_axis_tlast  : std_logic;
signal s_axis_tready : std_logic;

signal filter_axis_tdata  : std_logic_vector(8 - 1 downto 0);
signal filter_axis_tvalid : std_logic;
signal filter_axis_tlast  : std_logic;
signal filter_axis_tready : std_logic;

signal r_m_axis_tdata  : std_logic_vector(8 - 1 downto 0);
signal r_m_axis_tvalid : std_logic;
signal r_m_axis_tlast  : std_logic;
signal r_m_axis_tready : std_logic;

signal frame_warning_flag            :   std_logic;
signal frame_critical_error_flag     :   std_logic;
signal deframe_warning_flag            :   std_logic;
signal deframe_critical_error_flag     :   std_logic;

signal frame_health_monitor_statistics :  health_monitor_statistics_t;
signal deframe_health_monitor_statistics :  health_monitor_statistics_t;
        
signal test_cnt : unsigned(15 downto 0) := (others => '0');

signal tx_pause_req : std_logic;
signal enable_noise : std_logic;

file l_file : text; -- text is keyword
file in_file : text; -- text is keyword
file out_file : text; -- text is keyword

-------------------------------------- CREONIC -------------------------------------------
constant PREAMBLE_IN : std_logic_vector(71 downto 0) := X"77AD5B5843641E2E26";
constant C_FRAME_SIZE  : natural := 239 * 8 + 14 - 4;


signal preamble        : std_logic_vector(71 downto 0);
signal disp_sum        : std_logic_vector(23 downto 0);

-- configuration for the preamble sync algorithm
signal cfg_aquisition_length  : std_logic_vector(32 - 1 downto 0);
signal cfg_tracking_length    : std_logic_vector(32 - 1 downto 0);
signal cfg_max_tracking_error : std_logic_vector(32 - 1 downto 0);

signal enc_tx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
signal enc_tx_axis_tvalid   : std_logic;
signal enc_tx_axis_tlast    : std_logic;
signal enc_tx_axis_tready   : std_logic;
signal ch_tx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
signal ch_tx_axis_tvalid   : std_logic;
signal ch_tx_axis_tlast    : std_logic;
signal ch_tx_axis_tready   : std_logic := '0';
signal ch_rx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
signal ch_rx_axis_tdata_noise   : std_logic_vector(8 - 1 downto 0);
signal ch_rx_axis_tvalid   : std_logic;
signal ch_rx_axis_tready   : std_logic := '0';
signal enc_rx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
signal enc_rx_axis_tvalid   : std_logic;
signal enc_rx_axis_tlast    : std_logic;
signal enc_rx_axis_tready   : std_logic := '0';

signal crc16_valid       : std_logic;
signal crc16_correct     : std_logic;
signal crc32_valid       : std_logic;
signal crc32_correct     : std_logic;
signal payload_cerr      : std_logic_vector(4 - 1 downto 0);
signal payload_ncerr     : std_logic;
signal payload_err_valid : std_logic;
signal header_cerr       : std_logic_vector(4 - 1 downto 0);
signal header_ncerr      : std_logic;
signal header_err_valid  : std_logic;

-- More diagnostic informations
-------------------------------------------------------
signal clear_stat          : std_logic := '0';
signal total_frame_counter : std_logic_vector(31 downto 0);
signal crc_error_counter   : std_logic_vector(31 downto 0);
signal crc16_err_counter   : std_logic_vector(31 downto 0);
signal crc32_err_counter   : std_logic_vector(31 downto 0);
signal header_corrections  : std_logic_vector(31 downto 0);
signal payload_corrections : std_logic_vector(31 downto 0);
signal lpc_frame_counter   : std_logic_vector(31 downto 0);
signal lpc_corrections     : std_logic_vector(31 downto 0);

	-- Stats from the preamble sync
signal new_search_counter  : std_logic_vector(31 downto 0);
signal sync_loss_counter   : std_logic_vector(31 downto 0);

signal byte_cnt         : natural;
signal got_frame_id     : std_logic;


signal clk10     : std_logic := '0';
signal rst_clk10 : std_logic := '1';

signal tx_reset  : std_logic;
signal rx_reset  : std_logic;
signal rst_fec  : std_logic;

-- // Differential reference clock inputs
signal mgtrefclk0_x0y4_p : std_logic := '0';
signal mgtrefclk0_x0y4_n : std_logic := '0';

-- // Serial data ports for transceiver channel 0
signal ch0_gthrxn_in  : std_logic;
signal ch0_gthrxp_in  : std_logic;
signal ch0_gthtxn_out : std_logic;
signal ch0_gthtxp_out : std_logic;

signal data_good_in   : std_logic;
signal data_good_in_d : std_logic;
---------------------------------------------------------------------------------


begin
    preamble <= PREAMBLE_IN;
	-------------------------------------------
	-- component instantiation
	-------------------------------------------
    --rst_fec <= (rst or tx_reset);
    rst_fec <= rst;
	FEC_UUT :  entity frame_decoding.frame_fec_top
	port map(
		clk            => clk,
		rst            => rst_fec,

		preamble        => preamble,

		cfg_aquisition_length  => cfg_aquisition_length,
		cfg_tracking_length    => cfg_tracking_length,
		cfg_max_tracking_error => cfg_max_tracking_error,

		-- TX data input - header and payload data
		----------------------
		tx_axis_tdata      => m_axis_tdata,
		tx_axis_tvalid     => m_axis_tvalid,
		tx_axis_tlast      => m_axis_tlast,
		tx_axis_tready     => m_axis_tready,

		disp_sum           => disp_sum,

		-- Output data handling
		-----------------------
		ch_tx_axis_tdata     => ch_tx_axis_tdata,
		ch_tx_axis_tvalid    => ch_tx_axis_tvalid,
		ch_tx_axis_tlast     => ch_tx_axis_tlast,
		ch_tx_axis_tready    => '1',--ch_tx_axis_tready,


		-- RX data input
		----------------------
		ch_rx_axis_tdata      => ch_rx_axis_tdata_noise, --ch_tx_axis_tdata, --ch_rx_axis_tdata_noise,
		ch_rx_axis_tvalid     => ch_tx_axis_tvalid, --ch_rx_axis_tvalid,

		-- Status
		-----------------------
		crc16_valid       => crc16_valid,
		crc16_correct     => crc16_correct,
		crc32_valid       => crc32_valid,
		crc32_correct     => crc32_correct,

		payload_cerr      => payload_cerr,
		payload_ncerr     => payload_ncerr,
		payload_err_valid => payload_err_valid,

		header_cerr       => header_cerr,
		header_ncerr      => header_ncerr,
		header_err_valid  => header_err_valid,

		preamble_synced   => data_good_in,

		-- More diagnostic informations
		-------------------------------------------------------
		clear_stat          => clear_stat,
		total_frame_counter => total_frame_counter,
		crc_error_counter   => crc_error_counter,
		crc16_err_counter   => crc16_err_counter,
		crc32_err_counter   => crc32_err_counter,
		header_corrections  => header_corrections,
		payload_corrections => payload_corrections,
		lpc_frame_counter   => lpc_frame_counter,
		lpc_corrections     => lpc_corrections,

		-- Stats from the preamble sync
		new_search_counter  => new_search_counter,
		sync_loss_counter   => sync_loss_counter,

		-- RX data output - header and payload data
		-----------------------
		rx_axis_tdata     => s_axis_tdata,
		rx_axis_tvalid    => s_axis_tvalid,
		rx_axis_tlast     => s_axis_tlast,
		rx_axis_tready    => s_axis_tready
	);
	
    Inst_frame_health_monitor : frame_health_monitor
    Port map( 
        clk            => clk,
        rst            => rst,

        -- Input data from encoder
        ----------------------
        s_axis_tdata     => m_axis_tdata,
        s_axis_tvalid    => m_axis_tvalid,
        s_axis_tlast     => m_axis_tlast,
        s_axis_tready    => m_axis_tready,

        -- ERROR FLAGS
        warning_flag            => frame_warning_flag,
        critical_error_flag     => frame_critical_error_flag,

        health_monitor_statistics => frame_health_monitor_statistics

        );
        
    Inst_deframe_health_monitor : frame_health_monitor
    Port map( 
        clk            => clk,
        rst            => rst,

        -- Input data from encoder
        ----------------------
        s_axis_tdata     => filter_axis_tdata,
        s_axis_tvalid    => filter_axis_tvalid,
        s_axis_tlast     => filter_axis_tlast,
        s_axis_tready    => filter_axis_tready,

        -- ERROR FLAGS
        warning_flag            => deframe_warning_flag,
        critical_error_flag     => deframe_critical_error_flag,

        health_monitor_statistics => deframe_health_monitor_statistics

        );
        
	-- FSO PHY(for simulaion only)
--	inst_phy:  entity frame_decoding.fso_phy_sim
--	port map(
--		clk   => clk,
--		rst   => rst,

--		clk10     => clk10,
--		rst_clk10 => rst_clk10,

--		tx_reset  => tx_reset,
--		rx_reset  => rx_reset,

--		-- // Differential reference clock inputs
--		mgtrefclk0_x0y4_p => mgtrefclk0_x0y4_p,
--		mgtrefclk0_x0y4_n => mgtrefclk0_x0y4_n,

--		-- // Serial data ports for transceiver channel 0
--		ch0_gthrxn_in  => ch0_gthrxn_in,
--		ch0_gthrxp_in  => ch0_gthrxp_in,
--		ch0_gthtxn_out => ch0_gthtxn_out,
--		ch0_gthtxp_out => ch0_gthtxp_out,

--		tx_axis_tdata  => ch_tx_axis_tdata,
--		tx_axis_tvalid => ch_tx_axis_tvalid,
--		tx_axis_tready => ch_tx_axis_tready,

--		rx_axis_tdata  => ch_rx_axis_tdata,
--		rx_axis_tvalid => ch_rx_axis_tvalid,

--		data_good_in   => data_good_in
--	);

	mgtrefclk0_x0y4_p <= not mgtrefclk0_x0y4_p after 8.333 ns;
	mgtrefclk0_x0y4_n <= not mgtrefclk0_x0y4_p;

	-- Serial loopback
	ch0_gthrxp_in <= ch0_gthtxp_out;
	ch0_gthrxn_in <= ch0_gthtxn_out;

	-- insert some bit flips
	ch_rx_axis_tdata_noise <= (ch_tx_axis_tdata xor "11110000") when enable_noise = '1' else
	                           ch_tx_axis_tdata;


	-- Configuration for the preamble sync
--	cfg_aquisition_length  <= std_logic_vector(to_unsigned(4, 32));
--	cfg_tracking_length    <= std_logic_vector(to_unsigned(16, 32));
--	cfg_max_tracking_error <= std_logic_vector(to_unsigned(4, 32));
    cfg_aquisition_length  <= std_logic_vector(to_unsigned(8, 32));
  cfg_tracking_length    <= std_logic_vector(to_unsigned(32, 32));
  cfg_max_tracking_error <= std_logic_vector(to_unsigned(10, 32));

    UUT: entity ethernet_deframer_lib.ethernet_deframer_top
    port map(
        clk           => clk,
        rst           => rst,
        s_axis_tdata  => filter_axis_tdata,
        s_axis_tvalid => filter_axis_tvalid,
        s_axis_tlast  => filter_axis_tlast, 
        s_axis_tready => filter_axis_tready, 
        m_axis_tdata  => rx_axis_tdata,
        m_axis_tvalid => rx_axis_tvalid,
        m_axis_tlast  => rx_axis_tlast,
        m_axis_tready => rx_axis_tready,
		-- Statistics
		----------------------
		clear_stat                      => clear_stat,
        deframer_diagnostics             => open,
        sda_debug                       => open	
    );


NUT: entity ethernet_framer_lib.ethernet_framer_top
    generic map(
        MINIMUN_FREE_BYTES_TO_SPLIT => 64
    )
    port map(
        clk_let        => clk, -- : in  std_logic;
        rst_let        => rst, -- : in  std_logic;
        clk_eth        => clk, -- : in  std_logic;
        rst_eth        => rst, -- : in  std_logic;
        s_axis_tdata  => tx_axis_tdata,
        s_axis_tvalid => tx_axis_tvalid,
        s_axis_tlast  => tx_axis_tlast, 
        s_axis_tready => tx_axis_tready, 
        s_axis_tuser => tx_axis_tuser, 
        cfg_pause_data => x"0100",
        tx_pause_valid  => tx_pause_req,
        tx_pause_data   => open,
        rx_fifo_skipped_frame => open,
        m_axis_tdata  => m_axis_tdata,
        m_axis_tvalid => m_axis_tvalid,
        m_axis_tlast  => m_axis_tlast,
        m_axis_tready => m_axis_tready,
            
		-- Statistics
		----------------------            
		clear_stat                      => clear_stat,
        framer_diagnostics               => open
    );

NUTFIL: entity ethernet_deframer_lib.frame_filter_top
  PORT map(
    clk 						=> clk,
    rst 						=> rst,
    s_axis_tvalid 						=> s_axis_tvalid,
    s_axis_tready 						=> s_axis_tready,
    s_axis_tdata 						=> s_axis_tdata,
    s_axis_tlast 						=> s_axis_tlast,
    m_axis_tvalid 						=> filter_axis_tvalid,
    m_axis_tready 						=> filter_axis_tready,
    m_axis_tdata 						=> filter_axis_tdata,
    m_axis_tlast 						=> filter_axis_tlast,
    crc16_valid 						=> crc16_valid,
    crc16_correct 						=> crc16_correct,
    crc32_valid 						=> crc32_valid,
    crc32_correct 						=> crc32_correct
  );

	-------------------------------------------
	-- clock generation
	-------------------------------------------
	
	clk <= not clk after CLK_PERIOD/2;
	clk10 <= not clk10 after 10 ns;
	rstn <= not rst;
	--rx_axis_tready <= '1';
	
	pr_ready_control: process
	begin    
	    rx_axis_tready <= '1';
	    wait;
	    wait for 440 us;
	    wait until rising_edge(clk);
        rx_axis_tready <= '0';
        wait for 100.2 us;
        wait until rising_edge(clk);
        rx_axis_tready <= '1';
        wait for 61 us;
        wait until rising_edge(clk);
        rx_axis_tready <= '1';
        wait;
	end process;

	pr_noise: process
	begin    
	    enable_noise <= '0';
	    wait;
	    wait for 272 us;
        enable_noise <= '1';
        wait for 22.2 us;
        enable_noise <= '0';
        wait for 82.2 us;
        enable_noise <= '1';
        wait for 22.2 us;
        enable_noise <= '0';
        wait for 82.2 us;
        enable_noise <= '1';
        wait for 22.2 us;
        enable_noise <= '0';
        --wait;
        
        --wait;
	end process;
	-------------------------------------------
	-- stimuli generation
	-------------------------------------------
	pr_stimuli: process
		variable v_block_cnt        : integer := 0;
		variable v_insert_invalid   : boolean := false;
		variable v_byte_cnt         : natural;
		variable v_frame_cnt        : natural;
		variable v_frame_cnt_word   : std_logic_vector(15 downto 0);
		variable v_size_cnt_word   : std_logic_vector(15 downto 0);
        variable seed1 : positive;
        variable seed2 : positive;
        variable x : real;
        variable y : integer;
		
	begin
	    seed1 := 7878;
        seed2 := 65;
        --rst              <= '0';
        --wait for 5 * CLK_PERIOD;
		-- reset the system
		rst              <= '1';
		rst_clk10        <= '1';

		-- configuration
		tx_axis_tvalid    <= '0';
		tx_axis_tlast      <= '0';
		tx_axis_tdata     <= (others => '0');
		tx_axis_tuser     <= '0';

		wait for 200 * CLK_PERIOD;

		-- release the reset
		wait until rising_edge(clk);
		rst         <= '0';
		rst_clk10   <= '0';
		wait for 20 * CLK_PERIOD;
        --wait until tx_reset = '0';

		wait until rising_edge(clk);
		--wait for 0.1 * CLK_PERIOD;

		wait until rising_edge(clk);
        
		-- send data of one block
		wait for 200 * CLK_PERIOD;
		wait for 300 us;
--		wait until data_good_in = '1';
--		wait for 30 us;
		wait;
        --FRAME_SIZE <= 900;
        for frame in 1 to NUM_FRAME  loop --+ 50
--            if FRAME_SIZE > 1660 then
                
--                assert false report "Frame error! FRAME_SIZE: " & str(FRAME_SIZE) & "." 
--			    severity failure;
--			end if;
            wait until rising_edge(clk);
            
            wait until rising_edge(clk);
            tx_axis_tlast  <= '0';
            for byte_id in 0 to FRAME_SIZE - 1 loop
                byte_cnt <= byte_id;
                tx_axis_tvalid    <= '1';
                v_frame_cnt_word := std_logic_vector(to_unsigned(byte_id, 16));

                tx_axis_tdata <= std_logic_vector(to_unsigned((byte_id), 8));

                -- Two bytes of the header are for frame counters.
                -- This counter is just for comparision at the output and not neccessarily correct frame format
                if byte_id = 1 then
                    --tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                    tx_axis_tdata <= X"FE";
                end if;
                if byte_id = 0 then
                    --tx_axis_tdata <= v_frame_cnt_word(7 downto 0);
                    tx_axis_tdata <= X"CA";
                end if;
                if byte_id = 2 then
                    --tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                    v_frame_cnt_word := std_logic_vector(to_unsigned((frame), 16));
                    tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                end if;
                if byte_id = 3 then
                    --tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                    v_frame_cnt_word := std_logic_vector(to_unsigned((frame), 16));
                    tx_axis_tdata <= v_frame_cnt_word(7 downto 0);
                end if;
                if byte_id = 4 then
                    --tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                    v_size_cnt_word := std_logic_vector(to_unsigned((FRAME_SIZE), 16));
                    tx_axis_tdata <= v_size_cnt_word(15 downto 8);
                end if;
                if byte_id = 5 then
                    --tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                    v_size_cnt_word := std_logic_vector(to_unsigned((FRAME_SIZE), 16));
                    tx_axis_tdata <= v_size_cnt_word(7 downto 0);
                end if;
                if byte_id = FRAME_SIZE - 1 then
                    --tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                    tx_axis_tdata <= X"EF";
                end if;
                if byte_id = FRAME_SIZE - 2 then
                    --tx_axis_tdata <= v_frame_cnt_word(7 downto 0);
                    tx_axis_tdata <= X"BE";
                end if;
                
                if byte_id = FRAME_SIZE - 1 then
                    tx_axis_tlast <= '1';
                else
                    tx_axis_tlast <= '0';
                end if;

                wait until tx_axis_tready = '1' and falling_edge(clk);
                --wait for 0.2 * CLK_PERIOD;



                wait until rising_edge(clk);

            end loop;                
            tx_axis_tvalid <= '0';
            tx_axis_tdata  <= (others => 'U');
            wait for 5 us;
            --tx_axis_tlast  <= '0';
            
--            if frame = 3 and frame = 4 then
--                enable_noise <= '1';
--            end if;
--                if FRAME_SIZE > 1400 then
--                    FRAME_SIZE <= 1550;
--                else
--                    FRAME_SIZE <= FRAME_SIZE + 105 + (frame mod 4);
--                end if;
--            else
--                if FRAME_SIZE < 100 then
--                    FRAME_SIZE <= 99;
--                else
--                    FRAME_SIZE <= FRAME_SIZE - 105 + (frame mod 4);
--                end if;
--            end if;
--            if test_cnt mod 24 = 0 then
--                if FRAME_SIZE < 1000 then
--                    FRAME_SIZE <= FRAME_SIZE + (frame mod 4);
--                else
--                    FRAME_SIZE <= FRAME_SIZE + (frame mod 4);
--                end if;
--            end if;
--            if test_cnt mod 32 = 0 then
--                if FRAME_SIZE < 1000 then
--                    FRAME_SIZE <= 1407;
--                else
--                    FRAME_SIZE <= 137;
--                end if;
--            end if;
--            if test_cnt mod 4 = 0 then
--                if FRAME_SIZE < 1000 then
--                    FRAME_SIZE <= 1450;
--                else
--                    FRAME_SIZE <= 103;
--                end if;
--            end if;
--            if FRAME_SIZE < 1000 then
--                FRAME_SIZE <= 1505;
--            else
--                FRAME_SIZE <= 73;
--            end if;

			uniform(seed1, seed2, x);
            y := integer(floor(x * 1400.0));
            FRAME_SIZE <= y + 100;
            
            if FRAME_SIZE > 1560 then
                FRAME_SIZE <= 1550;
                --assert false report "Frame error! FRAME_SIZE: " & str(FRAME_SIZE) & "." 
			    -- severity failure;
			end if;          
--			if FRAME_SIZE = 996 then
--			 FRAME_SIZE <= 900;
--			 if test_cnt = 5000 then
--			    seed1 := 333;
--                seed2 := 177;
--			 end if;
--			elsif FRAME_SIZE = 900 then
--			 FRAME_SIZE <= 996;
--			end if;
			
			  
            test_cnt <= test_cnt + 1;
            if tx_pause_req = '1' then
                wait for 120 us;        
            end if;
            
            wait until rising_edge(clk);
           
        end loop;
        --m_axis_tready <= '1';
                           
		wait;
	end process pr_stimuli;


	-------------------------------------------
	-- stimuli generation
	-------------------------------------------
	file_open(l_file, "output_file.txt", write_mode);
	pr_output : process(clk) is
		variable v_byte_cnt : natural := 0;
		variable v_frame_id : std_logic_vector(15 downto 0);
		variable v_frame_id_nat : natural;
		variable v_error_cnt : natural := 0;
		variable v_frame_cnt : natural := 0;
		variable v_line_out  : line;
		variable v_seq_num : natural := 0;
		variable v_payload_len : natural := 0;
		variable v_packet_len : natural := 0;
		variable v_row          : line;
        variable v_data_write : integer;
        constant header:    string := "------------FRAME START------------";
        variable packets_ok : natural := 0;

		
	begin
	

	
	if(rising_edge(clk)) then
	
		if rst = '1' then 
          v_byte_cnt := 0;
          packets_ok := 0;
--      		write(v_row,header);	
--        	writeline(l_file,v_row);  
            --m_axis_tready <= '1';    
		else
	       --m_axis_tready <= '1';
	      if m_axis_tlast = '1' and m_axis_tvalid = '1' and m_axis_tready = '1' then
	            packets_ok := packets_ok + 1;
--		      	assert false report "Packet Received! " & str(v_byte_cnt+1) & " Bytes - Total Packets OK: " & str(packets_ok) & "." 
--				severity failure;
				write(v_row,hstr(m_axis_tdata));
				writeline(l_file,v_row);
				writeline(l_file,v_row);
				write(v_row,header);	
                writeline(l_file,v_row);   
				v_byte_cnt := 0;
				
		  elsif m_axis_tvalid = '1' and m_axis_tready = '1' then
		      write(v_row,hstr(m_axis_tdata));
		      --write(v_row,header);
		      --writeline(l_file,v_row); 
		      v_byte_cnt := v_byte_cnt + 1;
		      if (v_byte_cnt mod 4) = 0 then
		          writeline(l_file,v_row);       
--		      elsif v_byte_cnt = 15 then
--		          writeline(l_file,v_row);              
		      end if;
		        
		  end if;
		end if;
    end if;
	end process;
	
	
	-------------------------------------------
	-- record STIMULI input
	-------------------------------------------
	file_open(in_file, "input.txt", write_mode);
	pr_input : process(clk) is
		variable v_byte_cnt : natural := 0;
		variable v_frame_id : std_logic_vector(15 downto 0);
		variable v_frame_id_nat : natural;
		variable v_error_cnt : natural := 0;
		variable v_frame_cnt : natural := 0;
		variable v_line_out  : line;
		variable v_seq_num : natural := 0;
		variable v_payload_len : natural := 0;
		variable v_packet_len : natural := 0;
		variable v_row          : line;
        variable v_data_write : integer;
        constant header:    string := "------------PACKET START------------";
        variable packets_ok : natural := 0;

		
	begin
	

	if(rising_edge(clk)) then
	
		if rst = '1' then 
          v_byte_cnt := 0;
          packets_ok := 0;
--      		write(v_row,header);	
--        	writeline(l_file,v_row);  
            --m_axis_tready <= '1';    
		else
	       --m_axis_tready <= '1';
	      if tx_axis_tlast = '1' and tx_axis_tvalid = '1' then
--	            packets_ok := packets_ok + 1;
--		      	assert false report "Packet Received! " & str(v_byte_cnt+1) & " Bytes - Total Packets OK: " & str(packets_ok) & "." 
--				severity failure;
				write(v_row,hstr(tx_axis_tdata));
				writeline(in_file,v_row);
				writeline(in_file,v_row);
				write(v_row,header);	
                writeline(in_file,v_row);   
				v_byte_cnt := 0;
				
		  elsif tx_axis_tvalid = '1' then
		      write(v_row,hstr(tx_axis_tdata));
		      --write(v_row,header);
		      --writeline(l_file,v_row); 
		      v_byte_cnt := v_byte_cnt + 1;
		      if (v_byte_cnt mod 4) = 0 then
		          writeline(in_file,v_row);       
--		      elsif v_byte_cnt = 15 then
--		          writeline(l_file,v_row);              
		      end if;
		        
		  end if;
		end if;
    end if;
	end process;	
	

	-------------------------------------------
	-- record OUTPUT of deframer 
	-------------------------------------------
	file_open(out_file, "output_deframer.txt", write_mode);
	pr_output_deframer : process(clk) is
		variable v_byte_cnt : natural := 0;
		variable v_frame_id : std_logic_vector(15 downto 0);
		variable v_frame_id_nat : natural;
		variable v_error_cnt : natural := 0;
		variable v_frame_cnt : natural := 0;
		variable v_line_out  : line;
		variable v_seq_num : natural := 0;
		variable v_payload_len : natural := 0;
		variable v_packet_len : natural := 0;
		variable v_row          : line;
        variable v_data_write : integer;
        constant header:    string := "------------PACKET START------------";
        variable packets_ok : natural := 0;

		
	begin
	

	if(rising_edge(clk)) then
	
		if rst = '1' then 
          v_byte_cnt := 0;
          packets_ok := 0;
--      		write(v_row,header);	
--        	writeline(l_file,v_row);  
            --m_axis_tready <= '1';    
		else
	       --m_axis_tready <= '1';
	      if rx_axis_tlast = '1' and rx_axis_tvalid = '1' and rx_axis_tready = '1' then
--	            packets_ok := packets_ok + 1;
--		      	assert false report "Packet Received! " & str(v_byte_cnt+1) & " Bytes - Total Packets OK: " & str(packets_ok) & "." 
--				severity failure;
				write(v_row,hstr(rx_axis_tdata));
				writeline(out_file,v_row);
				writeline(out_file,v_row);
				write(v_row,header);	
                writeline(out_file,v_row);   
				v_byte_cnt := 0;
				
		  elsif rx_axis_tvalid = '1' and rx_axis_tready = '1' then
		      write(v_row,hstr(rx_axis_tdata));
		      --write(v_row,header);
		      --writeline(l_file,v_row); 
		      v_byte_cnt := v_byte_cnt + 1;
		      if (v_byte_cnt mod 4) = 0 then
		          writeline(out_file,v_row);       
--		      elsif v_byte_cnt = 15 then
--		          writeline(l_file,v_row);              
		      end if;
		        
		  end if;
		end if;
    end if;
	end process;
	
	---------------------------
	-- output comparison
	---------------------------

--	pr_output_FSO : process(clk) is
--		variable v_byte_cnt : natural := 0;
--		variable v_frame_id : std_logic_vector(15 downto 0);
--		variable v_frame_id_nat : natural;
--		variable v_error_cnt : natural := 0;
--		variable v_frame_cnt : natural := 0;
--		variable v_line_out  : line;
--	begin
--	if rising_edge(clk) then
--		if rst = '1' then
--			byte_cnt <= 0;
--			got_frame_id <= '0';
--			data_good_in_d <= '0';
--			clear_stat <= '0';
--		else
--			clear_stat <= '0';
--			data_good_in_d <= data_good_in;
--			if data_good_in_d = '0' and data_good_in = '1' then
--				write(v_line_out, string'("Preamble synchronization locked ") );
--				writeline(output, v_line_out);
--				clear_stat <= '1';
--				v_error_cnt := 0;
--			end if;
--			if data_good_in_d = '1' and data_good_in = '0' then
--				write(v_line_out, string'("Preamble synchronization lost") );
--				writeline(output, v_line_out);
--			end if;
--			if ch_rx_axis_tvalid = '1' then
--				byte_cnt <= byte_cnt + 1;
--			end if;
--			if rx_axis_tvalid = '1' and rx_axis_tready = '1' then
--				if v_byte_cnt = 0 then
--					v_frame_id(7 downto 0) := rx_axis_tdata;
--				end if;
--				if v_byte_cnt = 1 then
--					v_frame_id(15 downto 8) := rx_axis_tdata;
--					v_frame_id_nat := to_integer(unsigned(v_frame_id));
--					got_frame_id <= '1';
--				end if;
--				if got_frame_id = '1' then
--					if ((v_frame_id_nat + v_byte_cnt) mod 256) /= to_integer(unsigned(rx_axis_tdata)) then
--						v_error_cnt := v_error_cnt + 1;
--					end if;
--				end if;
--				v_byte_cnt := v_byte_cnt + 1;
--				if v_byte_cnt < C_FRAME_SIZE then
--					assert rx_axis_tlast = '0'
--						report "Signal result_last is asserted but shouldn't be."
--						severity ASSERTION_SEVERITY;
--				end if;
--				if v_byte_cnt = C_FRAME_SIZE then
--					v_byte_cnt := 0;
--					got_frame_id <= '0';
--					v_frame_cnt := v_frame_cnt + 1;
--					write(v_line_out, string'("Frame: ") );
--					write(v_line_out, v_frame_cnt);
--					write(v_line_out, string'(", Received ID: ") );
--					write(v_line_out, v_frame_id_nat);
--					write(v_line_out, string'(", CRC errors: ") );
--					write(v_line_out, to_integer(unsigned(crc_error_counter)));
--					write(v_line_out, string'(", LPC Frames: ") );
--					write(v_line_out, to_integer(unsigned(lpc_frame_counter)));
--					write(v_line_out, string'(", LPC Corrections: ") );
--					write(v_line_out, to_integer(unsigned(lpc_corrections)));
--					write(v_line_out, string'(", Header Corrections: ") );
--					write(v_line_out, to_integer(unsigned(header_corrections)));
--					write(v_line_out, string'(", Payload Corrections: ") );
--					write(v_line_out, to_integer(unsigned(payload_corrections)));
--					write(v_line_out, string'(", Output Errors: ") );
--					write(v_line_out, v_error_cnt);
--					writeline(output, v_line_out);

--					assert rx_axis_tlast = '1'
--						report "result_last must be asserted for the last output ddata."
--						severity ASSERTION_SEVERITY;

--					if v_frame_cnt = NUM_FRAME - 1 then
--						if v_error_cnt = 0 then
--							assert false report "Simulation finished successfully. Errors: " & str(v_error_cnt) & ". "
--							severity failure;
--						else
--							assert false report "Simulation finished with Errors: " & str(v_error_cnt) & ". "
--							severity failure;
--						end if;
--					end if;
--				end if;
--			end if;
--		end if;
--	end if;
--	end process pr_output_FSO;
	
	
end Behavioral;
