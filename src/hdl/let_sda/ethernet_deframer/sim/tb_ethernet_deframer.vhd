----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/10/2021 08:19:11 AM
-- Design Name: 
-- Module Name: tb_ethernet_deframer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use std.textio.all;
use IEEE.std_logic_textio;
use ieee.math_real.uniform;
use ieee.math_real.floor;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library ethernet_framer_lib;
use ethernet_framer_lib.pkg_components.all;
use ethernet_framer_lib.txt_util.all;
library ethernet_deframer_lib;
library share_let_1g;
use share_let_1g.pkg_components.bit_sync;
library sda_lib;
use sda_lib.pkg_sda.all;
library let_sda_lib;

entity tb_ethernet_deframer is
--  Port ( );
end tb_ethernet_deframer;

architecture Behavioral of tb_ethernet_deframer is

constant CLK_PERIOD  : time    := 10 ns;    
constant NUM_FRAME   : natural := 5;
signal FRAME_SIZE  : natural := 1500;

signal clk            : std_logic := '0';
signal mac_rx_clk_out            : std_logic := '0';
signal rst            : std_logic := '1';
signal mac_rx_reset            : std_logic := '1';
signal rstn           : std_logic;

signal tx_axis_tdata     : std_logic_vector(8 - 1 downto 0);
signal tx_axis_tvalid    : std_logic;
signal tx_axis_tlast     : std_logic;
signal tx_axis_tready    : std_logic;
signal tx_axis_tuser    : std_logic;

signal rx_axis_tdata     : std_logic_vector(8 - 1 downto 0);
signal rx_axis_tvalid    : std_logic;
signal rx_axis_tlast     : std_logic;
signal rx_axis_tready    : std_logic;

signal m_axis_tdata  : std_logic_vector(8 - 1 downto 0);
signal m_axis_tvalid : std_logic;
signal m_axis_tlast  : std_logic;
signal m_axis_tready : std_logic;

signal r_m_axis_tdata  : std_logic_vector(8 - 1 downto 0);
signal r_m_axis_tvalid : std_logic;
signal r_m_axis_tvalid_mux : std_logic;
signal r_m_axis_tlast  : std_logic;
signal r_m_axis_tready : std_logic;
signal valid_enable : std_logic := '1';

signal tx_pause_req : std_logic;

signal test_cnt : unsigned(15 downto 0) := (others => '0');

file l_file : text; -- text is keyword
file in_file : text; -- text is keyword
file out_file : text; -- text is keyword
signal cfg_pause_data : std_logic_vector(15 downto 0);
signal tx_pause_valid : std_logic;
signal tx_pause_data : std_logic_vector(15 downto 0);
signal tx_pause_ready : std_logic;
signal rx_fifo_skipped_frame : std_logic_vector(32 - 1 downto 0);
signal tx_ts : std_logic_vector(40-1 downto 0);
signal tx_ts_valid : std_logic;
signal tx_ts_rd_en : std_logic;
signal tx_timing_data : tx_timing_data_t;
signal tx_timing_data_valid : std_logic;
signal send_ranging_req : std_logic;
signal t1_tx_fn : std_logic_vector(15 downto 0);
signal t1_tx_fn_valid : std_logic;
signal send_t3 : std_logic;
signal t3_tx_fn : std_logic_vector(15 downto 0);
signal t3_tx_fn_valid : std_logic;
signal send_mgmt_response : mgmt_payload_t;
signal send_mgmt_response_valid : std_logic;
signal send_mgmt_response_ack : std_logic;
signal tx_fcch_opcode : std_logic_vector(4-1 downto 0);
signal tx_fcch_pl : std_logic_vector(16-1 downto 0);
signal tx_fcch_valid : std_logic;
signal tx_fcch_rd_en : std_logic;
signal clear_stat : std_logic;
signal total_packet_counter : std_logic_vector(31 downto 0);
signal total_packet_splitted_counter : std_logic_vector(31 downto 0);
signal total_data_frame_counter : std_logic_vector(31 downto 0);
signal actual_data_frame_counter : std_logic_vector(31 downto 0);
signal total_idle_frame_counter : std_logic_vector(31 downto 0);
signal total_packet_drop : std_logic_vector(31 downto 0);
signal total_prebuffer_packet_drop : std_logic_vector(31 downto 0);
signal frame_length_error : std_logic_vector(31 downto 0);
signal bitrate_in : std_logic_vector(31 downto 0);
signal packet_cnt_in : std_logic_vector(31 downto 0);
signal ranging_sync_delay : std_logic_vector(15 downto 0);
signal ranging_sync_period : std_logic_vector(15 downto 0);
signal rx_ts : std_logic_vector(40-1 downto 0);
signal rx_ts_valid : std_logic;
signal preamble_sync : std_logic;
signal reset_timer : std_logic;
signal rx_timing_data : rx_timing_data_t;
signal rx_timing_data_valid : std_logic;
signal mgmt_response : mgmt_payload_t;
signal mgmt_response_valid : std_logic;
signal rx_fcch_data : std_logic_vector(20-1 downto 0);
signal rx_fcch_valid : std_logic;
signal total_payload_counter : std_logic_vector(31 downto 0);
signal total_packet_merged_counter : std_logic_vector(31 downto 0);
signal total_payload_error_counter : std_logic_vector(31 downto 0);
signal total_packet_error_counter : std_logic_vector(31 downto 0);
signal watchdog_reset_counter : std_logic_vector(31 downto 0);
signal packet_filter_cnt_in : std_logic_vector(31 downto 0);
signal packet_filter_cnt_out : std_logic_vector(31 downto 0);
signal bitrate_out : std_logic_vector(31 downto 0);
signal packet_cnt_out : std_logic_vector(31 downto 0);
signal sda_debug : sda_debug_t;

begin

    inst_ethernet_framer_top : entity ethernet_framer_lib.ethernet_framer_top
        generic map(
            G_MINIMUN_FREE_BYTES_TO_SPLIT => 64,
            G_STARTUP_DELAY               => 3000
        )
        port map(
            clk_let                       => clk,
            rst_let                       => rst,
            clk_eth                       => clk,
            rst_eth                       => rst,
            s_axis_tdata                  => tx_axis_tdata,
            s_axis_tvalid                 => tx_axis_tvalid,
            s_axis_tlast                  => tx_axis_tlast,
            s_axis_tready                 => tx_axis_tready,
            s_axis_tuser                  => tx_axis_tuser,
            cfg_pause_data                => cfg_pause_data,
            tx_pause_valid                => tx_pause_valid,
            tx_pause_data                 => tx_pause_data,
            tx_pause_ready                => tx_pause_ready,
            rx_fifo_skipped_frame         => rx_fifo_skipped_frame,
            tx_ts                         => tx_ts,
            tx_ts_valid                   => tx_ts_valid,
            tx_ts_rd_en                   => tx_ts_rd_en,
            tx_timing_data                => open,
            tx_timing_data_valid          => tx_timing_data_valid,
            send_ranging_req              => send_ranging_req,
            t1_tx_fn                      => t1_tx_fn,
            t1_tx_fn_valid                => t1_tx_fn_valid,
            send_t3                       => send_t3,
            t3_tx_fn                      => t3_tx_fn,
            t3_tx_fn_valid                => t3_tx_fn_valid,
            send_mgmt_response            => send_mgmt_response,
            send_mgmt_response_valid      => send_mgmt_response_valid,
            send_mgmt_response_ack        => send_mgmt_response_ack,
            m_axis_tdata                  => r_m_axis_tdata,
            m_axis_tvalid                 => r_m_axis_tvalid,
            m_axis_tlast                  => r_m_axis_tlast,
            m_axis_tready                 => r_m_axis_tready,
            tx_fcch_opcode                => tx_fcch_opcode,
            tx_fcch_pl                    => tx_fcch_pl,
            tx_fcch_valid                 => tx_fcch_valid,
            tx_fcch_rd_en                 => tx_fcch_rd_en,
            clear_stat                    => clear_stat,
            total_packet_counter          => total_packet_counter,
            total_packet_splitted_counter => total_packet_splitted_counter,
            total_data_frame_counter      => total_data_frame_counter,
            actual_data_frame_counter     => actual_data_frame_counter,
            total_idle_frame_counter      => total_idle_frame_counter,
            total_packet_drop             => total_packet_drop,
            total_prebuffer_packet_drop   => total_prebuffer_packet_drop,
            frame_length_error            => frame_length_error,
            bitrate_in                    => bitrate_in,
            packet_cnt_in                 => packet_cnt_in
        );


    inst_ethernet_deframer_top : entity ethernet_deframer_lib.ethernet_deframer_top
        port map(
            clk_let                       => clk,
            rst_let                       => rst,
            clk_eth                       => clk,
            rst_eth                       => rst,
            s_axis_tdata                  => r_m_axis_tdata,
            s_axis_tvalid                 => r_m_axis_tvalid_mux,
            s_axis_tlast                  => r_m_axis_tlast,
            s_axis_tready                 => r_m_axis_tready,
            m_axis_tdata                  => rx_axis_tdata,
            m_axis_tvalid                 => rx_axis_tvalid,
            m_axis_tlast                  => rx_axis_tlast,
            m_axis_tready                 => rx_axis_tready,
            ranging_sync_delay            => ranging_sync_delay,
            ranging_sync_period           => ranging_sync_period,
            rx_ts                         => rx_ts,
            rx_ts_valid                   => rx_ts_valid,
            preamble_sync                 => preamble_sync,
            reset_timer                   => reset_timer,
            rx_timing_data                => rx_timing_data,
            rx_timing_data_valid          => rx_timing_data_valid,
            mgmt_response                 => mgmt_response,
            mgmt_response_valid           => mgmt_response_valid,
            rx_fcch_data                  => rx_fcch_data,
            rx_fcch_valid                 => rx_fcch_valid,
            clear_stat                    => clear_stat,
            total_packet_counter          => total_packet_counter,
            total_payload_counter         => total_payload_counter,
            total_packet_splitted_counter => total_packet_splitted_counter,
            total_packet_merged_counter   => total_packet_merged_counter,
            total_data_frame_counter      => total_data_frame_counter,
            total_idle_frame_counter      => total_idle_frame_counter,
            total_payload_error_counter   => total_payload_error_counter,
            total_packet_error_counter    => total_packet_error_counter,
            watchdog_reset_counter        => watchdog_reset_counter,
            packet_filter_cnt_in          => packet_filter_cnt_in,
            packet_filter_cnt_out         => packet_filter_cnt_out,
            rx_fifo_skipped_frame         => rx_fifo_skipped_frame,
            bitrate_out                   => bitrate_out,
            packet_cnt_out                => packet_cnt_out,
            sda_debug                     => sda_debug
        );

------
--    UUT: entity ethernet_deframer_lib.ethernet_deframer_top
--    port map(
--        clk_let       => clk,
--        rst_let       => rst,
--        clk_eth       => clk,
--        rst_eth       => rst,
--        s_axis_tdata  => r_m_axis_tdata,
--        s_axis_tvalid => r_m_axis_tvalid_mux,
--        s_axis_tlast  => r_m_axis_tlast, 
--        s_axis_tready => r_m_axis_tready, 
--        m_axis_tdata  => rx_axis_tdata,
--        m_axis_tvalid => rx_axis_tvalid,
--        m_axis_tlast  => rx_axis_tlast,
--        m_axis_tready => rx_axis_tready,
--		-- Statistics
--		----------------------
--		clear_stat                      => '0',
--        deframer_diagnostics             => open,
--        sda_debug                       => open
--    );
--
--NUT: entity ethernet_framer_lib.ethernet_framer_top
--    generic map(
--        MINIMUN_FREE_BYTES_TO_SPLIT => 64
--    )
--    port map(
--        clk_let        => clk, -- : in  std_logic;
--        rst_let        => rst, -- : in  std_logic;
--        clk_eth        => clk, -- : in  std_logic;
--        rst_eth        => rst, -- : in  std_logic;
--        s_axis_tdata  => tx_axis_tdata,
--        s_axis_tvalid => tx_axis_tvalid,
--        s_axis_tlast  => tx_axis_tlast, 
--        s_axis_tready => tx_axis_tready, 
--		s_axis_tuser  => tx_axis_tuser, 
--		cfg_pause_data        => x"0100",
--        tx_pause_valid        => tx_pause_req,
--        tx_pause_data         => open, 
--        rx_fifo_skipped_frame => open,
--        m_axis_tdata  => r_m_axis_tdata,
--        m_axis_tvalid => r_m_axis_tvalid,
--        m_axis_tlast  => r_m_axis_tlast,
--        m_axis_tready => r_m_axis_tready,
--            
--		-- Statistics
--		----------------------            
--		clear_stat                      => '0',
--		framer_diagnostics               => open
--    );
    
    r_m_axis_tvalid_mux <= r_m_axis_tvalid and valid_enable;
    
--    process is
--    begin
--    valid_enable <= '1';
--    wait for 100 us;
--    wait until r_m_axis_tlast = '1';
--    wait until rising_edge(clk);
--    wait until r_m_axis_tlast = '1';
--    wait until rising_edge(clk);
--    wait until r_m_axis_tlast = '1';
--    wait until rising_edge(clk);
--    valid_enable <= '0';
--    wait until r_m_axis_tlast = '1';
--    wait until rising_edge(clk);
--    valid_enable <= '1';
--    wait;
--    end process;

    -- Inst FIFO 
--    Inst_axi_fifo : axi_fifo
--    port map (
--        s_aresetn => rstn,
--        s_aclk => clk,
--        s_axis_tvalid => r_m_axis_tvalid,
--        s_axis_tready => r_m_axis_tready,
--        s_axis_tdata => r_m_axis_tdata,
--        s_axis_tlast => r_m_axis_tlast,
--        m_axis_tvalid => m_axis_tvalid,
--        m_axis_tready => m_axis_tready,
--        m_axis_tdata => m_axis_tdata,
--        m_axis_tlast => m_axis_tlast
--    );    

	-------------------------------------------
	-- clock generation
	-------------------------------------------
	
	clk <= not clk after CLK_PERIOD/2;
	mac_rx_clk_out <= not mac_rx_clk_out after CLK_PERIOD/2;
	rstn <= not rst;
	--rx_axis_tready <= '0', '1' after 350 us;
	rx_axis_tready <= '1';
--	pr_ready_control: process
--	begin    
--	    m_axis_tready <= '0';
--	    wait for 40 us;
--	    wait until rising_edge(clk);
--        m_axis_tready <= '1';
--        wait for 51.2 us;
--        wait until rising_edge(clk);
--        m_axis_tready <= '0';
--        wait for 61 us;
--        wait until rising_edge(clk);
--        m_axis_tready <= '1';
--	end process;
	-------------------------------------------
	-- stimuli generation
	-------------------------------------------
	pr_stimuli: process
		variable v_block_cnt        : integer := 0;
		variable v_insert_invalid   : boolean := false;
		variable v_byte_cnt         : natural;
		variable v_frame_cnt        : natural;
		variable v_frame_cnt_word   : std_logic_vector(15 downto 0);
		variable v_size_cnt_word   : std_logic_vector(15 downto 0);
        variable seed1 : positive;
        variable seed2 : positive;
        variable x : real;
        variable y : integer;
		
	begin
	    seed1 := 11;
        seed2 := 33;
        --rst              <= '0';
        --wait for 5 * CLK_PERIOD;
		-- reset the system
		rst              <= '1';
		mac_rx_reset     <= '1';
		tx_axis_tuser    <= '0';

		-- configuration
		tx_axis_tvalid    <= '0';
		tx_axis_tlast      <= '0';
		tx_axis_tdata     <= (others => '0');
		

		wait for 16 * CLK_PERIOD;

		-- release the reset
		wait until rising_edge(clk);
		rst         <= '0';
		mac_rx_reset         <= '0';
		wait for 20 * CLK_PERIOD;


		wait until rising_edge(clk);
		--wait for 0.1 * CLK_PERIOD;

		wait until rising_edge(clk);
        
		-- send data of one block
        FRAME_SIZE <= 1500;
        for frame in 1 to NUM_FRAME  loop --+ 50
            if frame = 1 then
                FRAME_SIZE <= 1500;
            elsif frame = 2 then
                FRAME_SIZE <= 500;
            elsif frame = 4 then
                FRAME_SIZE <= 500;
            else
                FRAME_SIZE <= 1500;
            end if;    
--                assert false report "Frame error! FRAME_SIZE: " & str(FRAME_SIZE) & "." 
--			    severity failure;
			
            wait until rising_edge(clk);
            
            wait until rising_edge(clk);
            tx_axis_tlast  <= '0';
            for byte_id in 0 to FRAME_SIZE - 1 loop
                tx_axis_tvalid    <= '1';
                v_frame_cnt_word := std_logic_vector(to_unsigned(byte_id, 16));

                --tx_axis_tdata <= std_logic_vector(to_unsigned((byte_id), 8));
                if frame = 1 then
                    tx_axis_tdata <= x"55";
                elsif frame = 2 then
                    tx_axis_tdata <= x"AA";
                elsif frame = 3 or frame = 5 then
                    tx_axis_tdata <= x"77";
                else
                    tx_axis_tdata <= x"44";
                end if;    
                
                -- Two bytes of the header are for frame counters.
                -- This counter is just for comparision at the output and not neccessarily correct frame format
                if byte_id = 1 then
                    --tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                    tx_axis_tdata <= X"FE";
                end if;
                if byte_id = 0 then
                    --tx_axis_tdata <= v_frame_cnt_word(7 downto 0);
                    tx_axis_tdata <= X"CA";
                end if;
                if byte_id = 2 then
                    --tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                    v_frame_cnt_word := std_logic_vector(to_unsigned((frame), 16));
                    tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                end if;
                if byte_id = 3 then
                    --tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                    v_frame_cnt_word := std_logic_vector(to_unsigned((frame), 16));
                    tx_axis_tdata <= v_frame_cnt_word(7 downto 0);
                end if;
                if byte_id = 4 then
                    --tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                    v_size_cnt_word := std_logic_vector(to_unsigned((FRAME_SIZE), 16));
                    tx_axis_tdata <= v_size_cnt_word(15 downto 8);
                end if;
                if byte_id = 5 then
                    --tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                    v_size_cnt_word := std_logic_vector(to_unsigned((FRAME_SIZE), 16));
                    tx_axis_tdata <= v_size_cnt_word(7 downto 0);
                end if;
                
                if byte_id = FRAME_SIZE - 1 then
                    tx_axis_tlast <= '1';
                    -- test packet drop:
--                    if frame = 2 then
--                        tx_axis_tuser <= '1';
--                    end if;
                else
                    tx_axis_tlast <= '0';
                end if;

                wait until tx_axis_tready = '1' and falling_edge(clk);
                --wait for 0.2 * CLK_PERIOD;



                wait until rising_edge(clk);

            end loop;                
            tx_axis_tvalid <= '0';
            tx_axis_tuser <= '0';
            tx_axis_tdata  <= (others => 'U');
            wait for 1 us;
            --tx_axis_tlast  <= '0';
            
--            if frame < 3000 or frame > 6000 then
--                if FRAME_SIZE > 1400 then
--                    FRAME_SIZE <= 1550;
--                else
--                    FRAME_SIZE <= FRAME_SIZE + 105 + (frame mod 4);
--                end if;
--            else
--                if FRAME_SIZE < 100 then
--                    FRAME_SIZE <= 99;
--                else
--                    FRAME_SIZE <= FRAME_SIZE - 105 + (frame mod 4);
--                end if;
--            end if;
--            if test_cnt mod 24 = 0 then
--                if FRAME_SIZE < 1000 then
--                    FRAME_SIZE <= FRAME_SIZE + (frame mod 4);
--                else
--                    FRAME_SIZE <= FRAME_SIZE + (frame mod 4);
--                end if;
--            end if;
--            if test_cnt mod 32 = 0 then
--                if FRAME_SIZE < 1000 then
--                    FRAME_SIZE <= 1407;
--                else
--                    FRAME_SIZE <= 137;
--                end if;
--            end if;
--            if test_cnt mod 4 = 0 then
--                if FRAME_SIZE < 1000 then
--                    FRAME_SIZE <= 1450;
--                else
--                    FRAME_SIZE <= 103;
--                end if;
--            end if;


			uniform(seed1, seed2, x);
            y := integer(floor(x * 1400.0));
            FRAME_SIZE <= y + 100;
            
            if FRAME_SIZE > 1560 then
                FRAME_SIZE <= 1550;
                --assert false report "Frame error! FRAME_SIZE: " & str(FRAME_SIZE) & "." 
			    -- severity failure;
			end if;          
--			if FRAME_SIZE = 996 then
--			 FRAME_SIZE <= 900;
--			 if test_cnt = 4 then
--			     FRAME_SIZE <= FRAME_SIZE + 2;
--			 end if;
--			elsif FRAME_SIZE = 900 then
--			 FRAME_SIZE <= 996;
--			end if;
			
			  
            test_cnt <= test_cnt + 1;
            if tx_pause_req = '1' then
                wait for 100 us;        
            end if;
            
            wait until rising_edge(clk);
           
        end loop;
        --m_axis_tready <= '1';
                           
		wait;
	end process pr_stimuli;


	-------------------------------------------
	-- stimuli generation
	-------------------------------------------
	file_open(l_file, "output_file.txt", write_mode);
	pr_output : process(clk) is
		variable v_byte_cnt : natural := 0;
		variable v_frame_id : std_logic_vector(15 downto 0);
		variable v_frame_id_nat : natural;
		variable v_error_cnt : natural := 0;
		variable v_frame_cnt : natural := 0;
		variable v_line_out  : line;
		variable v_seq_num : natural := 0;
		variable v_payload_len : natural := 0;
		variable v_packet_len : natural := 0;
		variable v_row          : line;
        variable v_data_write : integer;
        constant header:    string := "------------FRAME START------------";
        variable packets_ok : natural := 0;

		
	begin
	

	
	if(rising_edge(clk)) then
	
		if rst = '1' then 
          v_byte_cnt := 0;
          packets_ok := 0;
--      		write(v_row,header);	
--        	writeline(l_file,v_row);  
            --m_axis_tready <= '1';    
		else
	       --m_axis_tready <= '1';
	      if r_m_axis_tlast = '1' and r_m_axis_tvalid = '1' and r_m_axis_tready = '1' then
	            packets_ok := packets_ok + 1;
		      	--assert false report "Packet Received! " & str(v_byte_cnt+1) & " Bytes - Total Packets OK: " & str(packets_ok) & "." 
				--severity failure;
				write(v_row,hstr(r_m_axis_tdata));
				writeline(l_file,v_row);
				writeline(l_file,v_row);
				write(v_row,header);	
                writeline(l_file,v_row);  
                v_byte_cnt := v_byte_cnt + 1;
                if  v_byte_cnt = 1070 then
                    v_byte_cnt := 0;
                else
                    assert false report "WRONG FRAME SIZE! " & str(v_byte_cnt) & " Bytes - Total Packets OK: " & str(packets_ok) & "." 
                    severity failure;
                    v_byte_cnt := 0;
                end if;

				
		  elsif r_m_axis_tvalid = '1' and r_m_axis_tready = '1' then
		      write(v_row,hstr(r_m_axis_tdata));
		      --write(v_row,header);
		      --writeline(l_file,v_row); 
		      v_byte_cnt := v_byte_cnt + 1;
		      if (v_byte_cnt mod 4) = 0 then
		          writeline(l_file,v_row);       
--		      elsif v_byte_cnt = 15 then
--		          writeline(l_file,v_row);              
		      end if;
		        
		  end if;
		end if;
    end if;
	end process;
	
	
	-------------------------------------------
	-- record STIMULI input
	-------------------------------------------
	file_open(in_file, "input.txt", write_mode);
	pr_input : process(clk) is
		variable v_byte_cnt : natural := 0;
		variable v_frame_id : std_logic_vector(15 downto 0);
		variable v_frame_id_nat : natural;
		variable v_error_cnt : natural := 0;
		variable v_frame_cnt : natural := 0;
		variable v_line_out  : line;
		variable v_seq_num : natural := 0;
		variable v_payload_len : natural := 0;
		variable v_packet_len : natural := 0;
		variable v_row          : line;
        variable v_data_write : integer;
        constant header:    string := "------------PACKET START------------";
        variable packets_ok : natural := 0;

		
	begin
	

	if(rising_edge(clk)) then
	
		if rst = '1' then 
          v_byte_cnt := 0;
          packets_ok := 0;
--      		write(v_row,header);	
--        	writeline(l_file,v_row);  
            --m_axis_tready <= '1';    
		else
	       --m_axis_tready <= '1';
	      if tx_axis_tlast = '1' and tx_axis_tvalid = '1' and tx_axis_tready = '1' then
--	            packets_ok := packets_ok + 1;
--		      	assert false report "Packet Received! " & str(v_byte_cnt+1) & " Bytes - Total Packets OK: " & str(packets_ok) & "." 
--				severity failure;
				write(v_row,hstr(tx_axis_tdata));
				writeline(in_file,v_row);
				writeline(in_file,v_row);
				write(v_row,header);	
                writeline(in_file,v_row);   
				v_byte_cnt := 0;
				
		  elsif tx_axis_tvalid = '1' and tx_axis_tready = '1' then
		      write(v_row,hstr(tx_axis_tdata));
		      --write(v_row,header);
		      --writeline(l_file,v_row); 
		      v_byte_cnt := v_byte_cnt + 1;
		      if (v_byte_cnt mod 4) = 0 then
		          writeline(in_file,v_row);       
--		      elsif v_byte_cnt = 15 then
--		          writeline(l_file,v_row);              
		      end if;
		        
		  end if;
		end if;
    end if;
	end process;	
	

	-------------------------------------------
	-- record OUTPUT of deframer 
	-------------------------------------------
	file_open(out_file, "output_deframer.txt", write_mode);
	pr_output_deframer : process(clk) is
		variable v_byte_cnt : natural := 0;
		variable v_frame_id : std_logic_vector(15 downto 0);
		variable v_frame_id_nat : natural;
		variable v_error_cnt : natural := 0;
		variable v_frame_cnt : natural := 0;
		variable v_line_out  : line;
		variable v_seq_num : natural := 0;
		variable v_payload_len : natural := 0;
		variable v_packet_len : natural := 0;
		variable v_row          : line;
        variable v_data_write : integer;
        constant header:    string := "------------PACKET START------------";
        variable packets_ok : natural := 0;

		
	begin
	

	if(rising_edge(clk)) then
	
		if rst = '1' then 
          v_byte_cnt := 0;
          packets_ok := 0;
--      		write(v_row,header);	
--        	writeline(l_file,v_row);  
            --m_axis_tready <= '1';    
		else
	       --m_axis_tready <= '1';
	      if rx_axis_tlast = '1' and rx_axis_tvalid = '1' and rx_axis_tready = '1' then
--	            packets_ok := packets_ok + 1;
--		      	assert false report "Packet Received! " & str(v_byte_cnt+1) & " Bytes - Total Packets OK: " & str(packets_ok) & "." 
--				severity failure;
				write(v_row,hstr(rx_axis_tdata));
				writeline(out_file,v_row);
				writeline(out_file,v_row);
				write(v_row,header);	
                writeline(out_file,v_row);   
				v_byte_cnt := 0;
				
		  elsif rx_axis_tvalid = '1' and rx_axis_tready = '1' then
		      write(v_row,hstr(rx_axis_tdata));
		      --write(v_row,header);
		      --writeline(l_file,v_row); 
		      v_byte_cnt := v_byte_cnt + 1;
		      if (v_byte_cnt mod 4) = 0 then
		          writeline(out_file,v_row);       
--		      elsif v_byte_cnt = 15 then
--		          writeline(l_file,v_row);              
		      end if;
		        
		  end if;
		end if;
    end if;
	end process;
	
	
	
	
end Behavioral;
