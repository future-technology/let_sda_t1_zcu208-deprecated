----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/10/2021 08:19:11 AM
-- Design Name: 
-- Module Name: tb_ethernet_deframer_payload_header_logic - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use std.textio.all;
use IEEE.std_logic_textio;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library ethernet_framer_lib;
use ethernet_framer_lib.pkg_components.all;
use ethernet_framer_lib.txt_util.all;
library ethernet_deframer_lib;


entity tb_ethernet_deframer_payload_header_logic is
--  Port ( );
end tb_ethernet_deframer_payload_header_logic;

architecture Behavioral of tb_ethernet_deframer_payload_header_logic is

constant CLK_PERIOD  : time    := 10 ns;    
constant NUM_FRAME   : natural := 200;
signal FRAME_SIZE  : natural := 153;

signal clk            : std_logic := '0';
signal rst            : std_logic := '1';
signal rstn           : std_logic;

signal tx_axis_tdata     : std_logic_vector(8 - 1 downto 0);
signal tx_axis_tvalid    : std_logic;
signal tx_axis_tlast     : std_logic;
signal tx_axis_tready    : std_logic;

signal rx_axis_tdata     : std_logic_vector(8 - 1 downto 0);
signal rx_axis_tvalid    : std_logic;
signal rx_axis_tlast     : std_logic;
signal rx_axis_tready    : std_logic;

signal m_axis_tdata  : std_logic_vector(8 - 1 downto 0);
signal m_axis_tvalid : std_logic;
signal m_axis_tlast  : std_logic;
signal m_axis_tready : std_logic;

signal r_m_axis_tdata  : std_logic_vector(8 - 1 downto 0);
signal r_m_axis_tvalid : std_logic;
signal r_m_axis_tlast  : std_logic;
signal r_m_axis_tready : std_logic;

signal test_cnt : unsigned(15 downto 0) := (others => '0');

file l_file : text; -- text is keyword
file in_file : text; -- text is keyword
file out_file : text; -- text is keyword

begin

    UUT: entity ethernet_deframer_lib.ethernet_deframer
    port map(
        clk           => clk,
        rst           => rst,
        s_axis_tdata  => tx_axis_tdata,
        s_axis_tvalid => tx_axis_tvalid,
        s_axis_tlast  => tx_axis_tlast, 
        s_axis_tready => tx_axis_tready, 
        m_axis_tdata  => rx_axis_tdata,
        m_axis_tvalid => rx_axis_tvalid,
        m_axis_tlast  => rx_axis_tlast,
        m_axis_tready => rx_axis_tready,
		-- Statistics
		----------------------
		clear_stat                      => '0',
		total_packet_counter            => open,
		total_packet_splitted_counter   => open,
		total_packet_merged_counter     => open,
		total_frame_counter             => open,
		total_frame_error_counter       => open,
		total_packet_error_counter      => open		
    );

	-------------------------------------------
	-- clock generation
	-------------------------------------------
	
	clk <= not clk after CLK_PERIOD/2;
	rstn <= not rst;
	--rx_axis_tready <= '0', '1' after 350 us;
	rx_axis_tready <= '1';
--	pr_ready_control: process
--	begin    
--	    m_axis_tready <= '0';
--	    wait for 40 us;
--	    wait until rising_edge(clk);
--        m_axis_tready <= '1';
--        wait for 51.2 us;
--        wait until rising_edge(clk);
--        m_axis_tready <= '0';
--        wait for 61 us;
--        wait until rising_edge(clk);
--        m_axis_tready <= '1';
--	end process;
	-------------------------------------------
	-- stimuli generation
	-------------------------------------------
	pr_stimuli: process
		variable v_block_cnt        : integer := 0;
		variable v_insert_invalid   : boolean := false;
		variable v_byte_cnt         : natural;
		variable v_frame_cnt        : natural;
		variable v_frame_cnt_word   : std_logic_vector(15 downto 0);
	begin
        --rst              <= '0';
        --wait for 5 * CLK_PERIOD;
		-- reset the system
		rst              <= '1';

		-- configuration
		tx_axis_tvalid    <= '0';
		tx_axis_tlast      <= '0';
		tx_axis_tdata     <= (others => '0');
		

		wait for 16 * CLK_PERIOD;

		-- release the reset
		wait until rising_edge(clk);
		rst         <= '0';
		wait for 20 * CLK_PERIOD;


		wait until rising_edge(clk);
		--wait for 0.1 * CLK_PERIOD;

		wait until rising_edge(clk);

		-- send data of one block
        
        for frame in 1 to NUM_FRAME  loop --+ 50
            for byte_id in 0 to FRAME_SIZE - 1 loop
                tx_axis_tvalid    <= '1';
                v_frame_cnt_word := std_logic_vector(to_unsigned(byte_id, 16));

                tx_axis_tdata <= std_logic_vector(to_unsigned((byte_id), 8));

                -- Two bytes of the header are for frame counters.
                -- This counter is just for comparision at the output and not neccessarily correct frame format
                if byte_id = 1 then
                    --tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                    tx_axis_tdata <= X"FE";
                end if;
                if byte_id = 0 then
                    --tx_axis_tdata <= v_frame_cnt_word(7 downto 0);
                    tx_axis_tdata <= X"CA";
                end if;
                if byte_id = 2 then
                    --tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                    tx_axis_tdata <= std_logic_vector(to_unsigned((frame), 8));
                end if;
                if byte_id = 3 then
                    --tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                    tx_axis_tdata <= std_logic_vector(to_unsigned((FRAME_SIZE/10), 8));
                end if;

                if byte_id = FRAME_SIZE - 1 then
                    tx_axis_tlast <= '1';
                else
                    tx_axis_tlast <= '0';
                end if;

                wait until tx_axis_tready = '1' and falling_edge(clk);
                --wait for 0.2 * CLK_PERIOD;



                wait until rising_edge(clk);

            end loop;                
            tx_axis_tvalid <= '0';
            tx_axis_tdata  <= (others => 'U');
            tx_axis_tlast  <= '0';
            
            if frame < 30 or frame > 60 then
                if FRAME_SIZE > 1400 then
                    FRAME_SIZE <= 1550;
                else
                    FRAME_SIZE <= FRAME_SIZE + 100;
                end if;
            else
                if FRAME_SIZE < 100 then
                    FRAME_SIZE <= 99;
                else
                    FRAME_SIZE <= FRAME_SIZE - 100;
                end if;
            end if;
            test_cnt <= test_cnt + 1;
            wait until rising_edge(clk);
           
        end loop;
        --m_axis_tready <= '1';
                           
		wait;
	end process pr_stimuli;


	-------------------------------------------
	-- stimuli generation
	-------------------------------------------
	file_open(l_file, "output_file.txt", write_mode);
	pr_output : process(clk) is
		variable v_byte_cnt : natural := 0;
		variable v_frame_id : std_logic_vector(15 downto 0);
		variable v_frame_id_nat : natural;
		variable v_error_cnt : natural := 0;
		variable v_frame_cnt : natural := 0;
		variable v_line_out  : line;
		variable v_seq_num : natural := 0;
		variable v_payload_len : natural := 0;
		variable v_packet_len : natural := 0;
		variable v_row          : line;
        variable v_data_write : integer;
        constant header:    string := "------------FRAME START------------";
        variable packets_ok : natural := 0;

		
	begin
	

	
	if(rising_edge(clk)) then
	
		if rst = '1' then 
          v_byte_cnt := 0;
          packets_ok := 0;
--      		write(v_row,header);	
--        	writeline(l_file,v_row);  
            --m_axis_tready <= '1';    
		else
	       --m_axis_tready <= '1';
	      if m_axis_tlast = '1' and m_axis_tvalid = '1' and m_axis_tready = '1' then
	            packets_ok := packets_ok + 1;
		      	--assert false report "Packet Received! " & str(v_byte_cnt+1) & " Bytes - Total Packets OK: " & str(packets_ok) & "." 
				--severity failure;
				write(v_row,hstr(m_axis_tdata));
				writeline(l_file,v_row);
				writeline(l_file,v_row);
				write(v_row,header);	
                writeline(l_file,v_row);   
				v_byte_cnt := 0;
				
		  elsif m_axis_tvalid = '1' and m_axis_tready = '1' then
		      write(v_row,hstr(m_axis_tdata));
		      --write(v_row,header);
		      --writeline(l_file,v_row); 
		      v_byte_cnt := v_byte_cnt + 1;
		      if (v_byte_cnt mod 4) = 0 then
		          writeline(l_file,v_row);       
--		      elsif v_byte_cnt = 15 then
--		          writeline(l_file,v_row);              
		      end if;
		        
		  end if;
		end if;
    end if;
	end process;
	
	
	-------------------------------------------
	-- record STIMULI input
	-------------------------------------------
	file_open(in_file, "input.txt", write_mode);
	pr_input : process(clk) is
		variable v_byte_cnt : natural := 0;
		variable v_frame_id : std_logic_vector(15 downto 0);
		variable v_frame_id_nat : natural;
		variable v_error_cnt : natural := 0;
		variable v_frame_cnt : natural := 0;
		variable v_line_out  : line;
		variable v_seq_num : natural := 0;
		variable v_payload_len : natural := 0;
		variable v_packet_len : natural := 0;
		variable v_row          : line;
        variable v_data_write : integer;
        constant header:    string := "------------PACKET START------------";
        variable packets_ok : natural := 0;

		
	begin
	

	if(rising_edge(clk)) then
	
		if rst = '1' then 
          v_byte_cnt := 0;
          packets_ok := 0;
--      		write(v_row,header);	
--        	writeline(l_file,v_row);  
            --m_axis_tready <= '1';    
		else
	       --m_axis_tready <= '1';
	      if tx_axis_tlast = '1' and tx_axis_tvalid = '1' and tx_axis_tready = '1' then
--	            packets_ok := packets_ok + 1;
--		      	assert false report "Packet Received! " & str(v_byte_cnt+1) & " Bytes - Total Packets OK: " & str(packets_ok) & "." 
--				severity failure;
				write(v_row,hstr(tx_axis_tdata));
				writeline(in_file,v_row);
				writeline(in_file,v_row);
				write(v_row,header);	
                writeline(in_file,v_row);   
				v_byte_cnt := 0;
				
		  elsif tx_axis_tvalid = '1' and tx_axis_tready = '1' then
		      write(v_row,hstr(tx_axis_tdata));
		      --write(v_row,header);
		      --writeline(l_file,v_row); 
		      v_byte_cnt := v_byte_cnt + 1;
		      if (v_byte_cnt mod 4) = 0 then
		          writeline(in_file,v_row);       
--		      elsif v_byte_cnt = 15 then
--		          writeline(l_file,v_row);              
		      end if;
		        
		  end if;
		end if;
    end if;
	end process;	
	

	-------------------------------------------
	-- record OUTPUT of deframer 
	-------------------------------------------
	file_open(out_file, "output_deframer.txt", write_mode);
	pr_output_deframer : process(clk) is
		variable v_byte_cnt : natural := 0;
		variable v_frame_id : std_logic_vector(15 downto 0);
		variable v_frame_id_nat : natural;
		variable v_error_cnt : natural := 0;
		variable v_frame_cnt : natural := 0;
		variable v_line_out  : line;
		variable v_seq_num : natural := 0;
		variable v_payload_len : natural := 0;
		variable v_packet_len : natural := 0;
		variable v_row          : line;
        variable v_data_write : integer;
        constant header:    string := "------------PACKET START------------";
        variable packets_ok : natural := 0;

		
	begin
	

	if(rising_edge(clk)) then
	
		if rst = '1' then 
          v_byte_cnt := 0;
          packets_ok := 0;
--      		write(v_row,header);	
--        	writeline(l_file,v_row);  
            --m_axis_tready <= '1';    
		else
	       --m_axis_tready <= '1';
	      if rx_axis_tlast = '1' and rx_axis_tvalid = '1' and rx_axis_tready = '1' then
--	            packets_ok := packets_ok + 1;
--		      	assert false report "Packet Received! " & str(v_byte_cnt+1) & " Bytes - Total Packets OK: " & str(packets_ok) & "." 
--				severity failure;
				write(v_row,hstr(rx_axis_tdata));
				writeline(out_file,v_row);
				writeline(out_file,v_row);
				write(v_row,header);	
                writeline(out_file,v_row);   
				v_byte_cnt := 0;
				
		  elsif rx_axis_tvalid = '1' and rx_axis_tready = '1' then
		      write(v_row,hstr(rx_axis_tdata));
		      --write(v_row,header);
		      --writeline(l_file,v_row); 
		      v_byte_cnt := v_byte_cnt + 1;
		      if (v_byte_cnt mod 4) = 0 then
		          writeline(out_file,v_row);       
--		      elsif v_byte_cnt = 15 then
--		          writeline(l_file,v_row);              
		      end if;
		        
		  end if;
		end if;
    end if;
	end process;
	
	
	
	
end Behavioral;
