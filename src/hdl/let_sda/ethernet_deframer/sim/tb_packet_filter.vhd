----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/07/2021 04:50:48 PM
-- Design Name: 
-- Module Name: tb_packet_filter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use std.textio.all;
use IEEE.std_logic_textio;
use ieee.math_real.uniform;
use ieee.math_real.floor;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
library ethernet_deframer_lib;

entity tb_packet_filter is
--  Port ( );
end tb_packet_filter;

architecture Behavioral of tb_packet_filter is

constant CLK_PERIOD  : time    := 5 ns;    
constant NUM_FRAME   : natural := 10;
signal FRAME_SIZE  : natural := 1518;
signal test_cnt : unsigned(15 downto 0) := (others => '0');

signal tx_axis_tdata  : std_logic_vector(8 - 1 downto 0);
signal tx_axis_tvalid : std_logic;
signal tx_axis_tlast  : std_logic;
signal tx_axis_tready : std_logic := '0';
signal tx_axis_tuser : std_logic := '0';

signal m_axis_tdata  : std_logic_vector(8 - 1 downto 0);
signal m_axis_tvalid : std_logic;
signal m_axis_tlast  : std_logic;
signal m_axis_tready : std_logic := '0';
signal m_axis_tuser : std_logic := '0';


signal bypass_en : std_logic := '0';


signal clk            : std_logic := '0';
signal rst            : std_logic := '1';

begin
    
clk <= not clk after CLK_PERIOD/2;


UUT: entity ethernet_deframer_lib.packet_filter
    port map ( 
        clk            => clk,
        rst            => rst,

        -- Input data from encoder
        ----------------------
        s_axis_tdata     => tx_axis_tdata,
        s_axis_tvalid    => tx_axis_tvalid,
        s_axis_tlast     => tx_axis_tlast,
        s_axis_tready    => tx_axis_tready,
        s_axis_tuser     => tx_axis_tuser,

        -- Output data to ethernet mac
        ----------------------
        m_axis_tdata  => m_axis_tdata,
        m_axis_tvalid => m_axis_tvalid,
        m_axis_tlast  => m_axis_tlast,
        m_axis_tready => m_axis_tready,
        m_axis_tuser  => m_axis_tuser


    );


pr_stimuli : process is
    variable v_block_cnt        : integer := 0;
    variable v_insert_invalid   : boolean := false;
    variable v_byte_cnt         : natural;
    variable v_frame_cnt        : natural;
    variable v_frame_cnt_word   : std_logic_vector(15 downto 0);
    variable v_size_cnt_word   : std_logic_vector(15 downto 0);
    variable seed1 : positive;
    variable seed2 : positive;
    variable x : real;
    variable y : integer;
begin
    rst <= '1';
    tx_axis_tuser <= '0';
    m_axis_tready <= '1';
    tx_axis_tlast <= '0';
    tx_axis_tvalid <= '0';
    tx_axis_tdata <= (others=>'0');
    wait for 16*CLK_PERIOD;
    rst <= '0';
    wait until rising_edge(clk);
    wait for 200 ns;
    wait until rising_edge(clk);

    for frame in 1 to NUM_FRAME  loop --+ 50
    --            if FRAME_SIZE > 1660 then
            
    --                assert false report "Frame error! FRAME_SIZE: " & str(FRAME_SIZE) & "." 
    --			    severity failure;
    --			end if;
        wait until rising_edge(clk);
        
        wait until rising_edge(clk);
        tx_axis_tlast  <= '0';
        tx_axis_tuser <= '0';
        for byte_id in 0 to FRAME_SIZE - 1 loop
            tx_axis_tvalid    <= '1';
            v_frame_cnt_word := std_logic_vector(to_unsigned(byte_id, 16));

            tx_axis_tdata <= std_logic_vector(to_unsigned((byte_id), 8));

            -- Two bytes of the header are for frame counters.
            -- This counter is just for comparision at the output and not neccessarily correct frame format
            if byte_id = 1 then
                --tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                tx_axis_tdata <= X"FE";
            end if;
            if byte_id = 0 then
                --tx_axis_tdata <= v_frame_cnt_word(7 downto 0);
                tx_axis_tdata <= X"CA";
            end if;
            if byte_id = 2 then
                --tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                v_frame_cnt_word := std_logic_vector(to_unsigned((frame), 16));
                tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
            end if;
            if byte_id = 3 then
                --tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                v_frame_cnt_word := std_logic_vector(to_unsigned((frame), 16));
                tx_axis_tdata <= v_frame_cnt_word(7 downto 0);
            end if;
            if byte_id = 4 then
                --tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                v_size_cnt_word := std_logic_vector(to_unsigned((FRAME_SIZE), 16));
                tx_axis_tdata <= v_size_cnt_word(15 downto 8);
            end if;
            if byte_id = 5 then
                --tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                v_size_cnt_word := std_logic_vector(to_unsigned((FRAME_SIZE), 16));
                tx_axis_tdata <= v_size_cnt_word(7 downto 0);
            end if;
            
            if byte_id = FRAME_SIZE - 1 then
                tx_axis_tlast <= '1';
                if frame = 2 or frame = 4 or frame = 5 or frame = 8 then
                    tx_axis_tuser <= '1';
                end if;
            else
                tx_axis_tlast <= '0';
            end if;

            wait until tx_axis_tready = '1' and falling_edge(clk);
            --wait for 0.2 * CLK_PERIOD;



            wait until rising_edge(clk);

        end loop;                
        tx_axis_tvalid <= '0';
        tx_axis_tdata  <= (others => 'U');
        wait for 20 us;
        --tx_axis_tlast  <= '0';
        
    --            if frame < 3000 or frame > 6000 then
    --                if FRAME_SIZE > 1400 then
    --                    FRAME_SIZE <= 1550;
    --                else
    --                    FRAME_SIZE <= FRAME_SIZE + 105 + (frame mod 4);
    --                end if;
    --            else
    --                if FRAME_SIZE < 100 then
    --                    FRAME_SIZE <= 99;
    --                else
    --                    FRAME_SIZE <= FRAME_SIZE - 105 + (frame mod 4);
    --                end if;
    --            end if;
    --            if test_cnt mod 24 = 0 then
    --                if FRAME_SIZE < 1000 then
    --                    FRAME_SIZE <= FRAME_SIZE + (frame mod 4);
    --                else
    --                    FRAME_SIZE <= FRAME_SIZE + (frame mod 4);
    --                end if;
    --            end if;
    --            if test_cnt mod 32 = 0 then
    --                if FRAME_SIZE < 1000 then
    --                    FRAME_SIZE <= 1407;
    --                else
    --                    FRAME_SIZE <= 137;
    --                end if;
    --            end if;
    --            if test_cnt mod 4 = 0 then
    --                if FRAME_SIZE < 1000 then
    --                    FRAME_SIZE <= 1450;
    --                else
    --                    FRAME_SIZE <= 103;
    --                end if;
    --            end if;


        uniform(seed1, seed2, x);
        y := integer(floor(x * 1400.0));
        FRAME_SIZE <= y + 100;
        
        if FRAME_SIZE > 1560 then
            FRAME_SIZE <= 1550;
            --assert false report "Frame error! FRAME_SIZE: " & str(FRAME_SIZE) & "." 
            -- severity failure;
        end if;          
    --			if FRAME_SIZE = 996 then
    --			 FRAME_SIZE <= 900;
    --			 if test_cnt = 4 then
    --			     FRAME_SIZE <= FRAME_SIZE + 2;
    --			 end if;
    --			elsif FRAME_SIZE = 900 then
    --			 FRAME_SIZE <= 996;
    --			end if;
        
        
        test_cnt <= test_cnt + 1;

        wait until rising_edge(clk);
    
    end loop;
    --m_axis_tready <= '1';
                    
    wait;
end process pr_stimuli;


end Behavioral;
