
library sda_lib
analyze ../sda/pkg_sda.vhd

library share_let_1g
analyze ../share_let_1g/packages/pkg_support.vhd
analyze ../share_let_1g/packages/pkg_param.vhd
analyze ../share_let_1g/packages/pkg_components.vhd
analyze ../share_let_1g/packages/pkg_types.vhd

analyze ../share_let_1g/src/axi4s_buffer.vhd
analyze ../share_let_1g/src/axi4s_fifo.vhd
analyze ../share_let_1g/src/bit_sync.vhd
analyze ../share_let_1g/src/generic_simple_dp_ram.vhd

library ethernet_framer_lib
analyze pkg_components.vhd
analyze prbs_gen.vhd
analyze ethernet_framer.vhd

