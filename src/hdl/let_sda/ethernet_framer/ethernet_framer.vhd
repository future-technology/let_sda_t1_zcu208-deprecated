-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: ethernet_framer
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Converts Ethernet packets to SDA Standard FSO frames in AXI-Stream format.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library ethernet_framer_lib;
use ethernet_framer_lib.pkg_components.all;

--library frame_decoding;
--use frame_decoding.pkg_components.all;
library share_let_1g;
library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

entity ethernet_framer is
    generic(
        G_MINIMUN_FREE_BYTES_TO_SPLIT : NATURAL := 8
    );
    port(
        clk                           : in    std_logic;
        rst                           : in    std_logic;
        -- Input data 
        ----------------------
        axis_if_m                       : in t_axis_if_m;
        axis_if_s                       : out t_axis_if_s;
        -- Output data 
        ----------------------
        axis_payload_if_m               : out t_axis_payload_if_m;
        axis_payload_if_s               : in t_axis_payload_if_s;

        -- Control (subject to change in the future)
        ---------------------- 
        overload                      : in    std_logic;
        back_pressure_high            : out   std_logic;
        back_pressure_low             : out   std_logic;

        send_ranging_req              : in    std_logic;
        send_mgmt_response            : in    mgmt_payload_t;
        send_mgmt_response_valid      : in    std_logic;
        send_mgmt_response_ack        : out   std_logic;

        -- Statistics
        ----------------------
        clear_stat                    : in    std_logic;
        total_packet_counter          : out   std_logic_vector(31 downto 0);
        total_packet_splitted_counter : out   std_logic_vector(31 downto 0);
        total_data_frame_counter      : out   std_logic_vector(31 downto 0);
        actual_data_frame_counter     : out   std_logic_vector(31 downto 0);
        total_idle_frame_counter      : out   std_logic_vector(31 downto 0);
        total_packet_drop             : out   std_logic_vector(31 downto 0);
        frame_length_error            : out   std_logic_vector(31 downto 0)
    );
end ethernet_framer;

architecture Behavioral of ethernet_framer is

    constant C_FIFO_INPUT_FULL_DEPTH   : natural := 16384;
    constant C_FIFO_INPUT_FULL_THRESH  : natural := C_FIFO_INPUT_FULL_DEPTH * 3 / 4; -- 75%
    constant C_FIFO_INPUT_EMPTY_THRESH : natural := C_FIFO_INPUT_FULL_DEPTH * 1 / 4; -- 25%

    type t_input_fsm is (IDLE, START, SET_FRAME_HEADER, APPEND_FRAME_HEADER, APPEND_PAYLOAD_HEADER, ALIGN_PACKET_32, WAIT_PACKET, SET_PACKET_HEADER, APPEND_PACKET_HEADER, WRITE_DATA, APPEND_PADDING, SEND_FRAME, DROP_PACKET, APPEND_PRBS, APPEND_MGMT);
    signal input_ready_int : std_logic;
    signal data_in         : std_logic_vector(8 - 1 downto 0);
    signal input_fsm       : t_input_fsm;

    type t_packet_fsm is (NO_SPLIT, SPLITTED);
    signal packet_fsm : t_packet_fsm;

    constant C_TIMEOUT : natural := 10; --No packet ready Timeout in clock cycles

    --FRAME:
    -- Frame shift register to construct frame data and push it out.
    signal frame_data_in       : std_logic_vector(7 downto 0);
    signal frame_valid_in      : std_logic;
    signal frame_ready_in      : std_logic;
    signal frame_last_in       : std_logic;
    signal frame_start_in      : std_logic;
    signal frame_full_in       : std_logic;
    signal frame_empty_in      : std_logic;
    signal frame_data_out      : std_logic_vector(7 downto 0);
    signal frame_header_fields : frame_header_fields_t;
    signal fh_txfn_cnt         : unsigned(15 downto 0);
    signal prbs_seed           : std_logic_vector(15 downto 0);

    -- PAYLOAD:
    signal payload_header        : std_logic_vector(32 - 1 downto 0);
    signal payload_seq_num       : unsigned(10 - 1 downto 0);
    signal payload_len_remaining : unsigned(14 - 1 downto 0);

    -- PACKET:
    signal packet_header    : std_logic_vector(32 - 1 downto 0);
    signal packet_len       : std_logic_vector(14 - 1 downto 0);
    signal packet_id        : std_logic_vector(14 - 1 downto 0);
    signal packet_id_last   : std_logic_vector(14 - 1 downto 0);
    signal packet_32b_align : std_logic;

    --COUNTERS:
    signal timeout_cnt            : unsigned(32 - 1 downto 0);
    signal header_cnt             : unsigned(4 - 1 downto 0);
    signal frame_cnt              : unsigned(12 - 1 downto 0);
    signal packet_len_cnt         : unsigned(14 - 1 downto 0);
    signal packet_id_cnt          : unsigned(14 - 1 downto 0);
    signal frame_header_cnt       : unsigned(5 - 1 downto 0);
    signal mgmt_payload_cnt       : unsigned(5 - 1 downto 0);
    signal frame_len_limit        : std_logic;
    signal r_frame_len_limit      : std_logic;
    signal allow_packet_insert    : std_logic;
    signal timeout_en             : std_logic;
    signal current_packet_len_cnt : unsigned(14 - 1 downto 0);
    signal frame_sr_cnt           : unsigned(12 - 1 downto 0);

    -- AXI INPUT FIFO
    signal r_s_axis_tdata  : std_logic_vector(8 - 1 downto 0);
    signal r_s_axis_tvalid : std_logic;
    signal r_s_axis_tlast  : std_logic;
    signal r_s_axis_tready : std_logic;

    -- AXI OUTPUT FIFO
    signal r_m_axis_tdata          : std_logic_vector(8 - 1 downto 0);
    signal r_m_axis_tvalid         : std_logic;
    signal r_m_axis_tlast          : std_logic;
    signal r_m_axis_tstart         : std_logic;
    signal r_m_axis_tready         : std_logic;
    signal cache_fifo_actual_empty : std_logic;

    signal s_axis_tready_fifo : std_logic;
    signal s_axis_tready_aux  : std_logic;
    signal resetn             : std_logic;

    -- Packet lenght fifo
    signal packet_len_fifo_din         : std_logic_vector(13 DOWNTO 0);
    signal packet_len_fifo_wr_en       : std_logic;
    signal packet_len_fifo_rd_en       : std_logic;
    --signal packet_len_fifo_dout               : std_logic_vector(13 DOWNTO 0);
    signal packet_len_fifo_full        : std_logic;
    signal packet_len_fifo_overflow    : std_logic;
    signal packet_len_fifo_empty       : std_logic;
    signal packet_len_fifo_valid       : std_logic;
    signal packet_len_fifo_data_count  : std_logic_vector(10 DOWNTO 0);
    signal packet_len_fifo_wr_rst_busy : std_logic;
    signal packet_len_fifo_rd_rst_busy : std_logic;

    -- Packet lenght fifo
    signal packet_id_fifo_din         : std_logic_vector(13 DOWNTO 0);
    signal packet_id_fifo_wr_en       : std_logic;
    signal packet_id_fifo_rd_en       : std_logic;
    --signal packet_len_fifo_dout               : std_logic_vector(13 DOWNTO 0);
    signal packet_id_fifo_full        : std_logic;
    signal packet_id_fifo_overflow    : std_logic;
    signal packet_id_fifo_empty       : std_logic;
    signal packet_id_fifo_valid       : std_logic;
    signal packet_id_fifo_data_count  : std_logic_vector(10 DOWNTO 0);
    signal packet_id_fifo_wr_rst_busy : std_logic;
    signal packet_id_fifo_rd_rst_busy : std_logic;

    signal axis_prog_full : std_logic;

    -- PRBS:
    signal prbs_input_valid  : std_logic;
    signal prbs_input_last   : std_logic;
    signal prbs_input_ready  : std_logic;
    signal prbs_scramble_rst : std_logic;

    signal prbs_output_data  : std_logic_vector(8 - 1 downto 0);
    signal prbs_output_valid : std_logic;
    signal prbs_output_last  : std_logic;
    signal prbs_output_ready : std_logic;

    -- Statistics counters
    signal r_total_packet_counter          : unsigned(31 downto 0);
    signal r_total_packet_splitted_counter : unsigned(31 downto 0);
    signal r_total_data_frame_counter      : unsigned(31 downto 0);
    signal r_total_mgmt_frame_counter      : unsigned(31 downto 0);
    signal r_total_mgmt_resp_frame_counter : unsigned(31 downto 0);
    signal r_actual_data_frame_counter     : unsigned(31 downto 0);
    signal r_total_idle_frame_counter      : unsigned(31 downto 0);
    signal r_total_packet_drop             : unsigned(31 downto 0);
    signal r_frame_length_error            : unsigned(31 downto 0);

    signal r_tx_ts : std_logic_vector(40 - 1 downto 0);

    signal mgmt_payload   : mgmt_payload_t;
    signal mgmt_seq       : unsigned(16 - 1 downto 0);
    signal tx_fcch_pl_aux : std_logic_vector(48 - 1 downto 0);

    -- DEBUG ONLY. WILL BE REMOVED
    --    signal word_data : std_logic_vector(31 downto 0);
    signal c_header_vector : std_logic_vector((14 * 8) - 1 downto 0) := x"FFFFCFD118F8687182F4E7E1D091";

    --
    --    -- Avoid coptimization for ILA 
    --    -- DEBUG ONLY. WILL BE REMOVED
    --    attribute keep : string; 
    --    attribute keep of input_fsm: signal is "true"; 
    --    attribute keep of frame_cnt: signal is "true"; 
    --    attribute keep of packet_fsm: signal is "true"; 
    --    attribute keep of payload_header: signal is "true"; 
    --    attribute keep of payload_seq_num: signal is "true"; 
    --    attribute keep of payload_len_remaining: signal is "true"; 
    --    attribute keep of current_packet_len_cnt: signal is "true"; 
    --    attribute keep of frame_len_limit: signal is "true"; 
    --    attribute keep of r_s_axis_tdata     : signal is "true"; 
    --    attribute keep of r_s_axis_tvalid    : signal is "true"; 
    --    attribute keep of r_s_axis_tlast     : signal is "true"; 
    --    attribute keep of r_s_axis_tready    : signal is "true";   
    --    attribute keep of r_m_axis_tdata     : signal is "true"; 
    --    attribute keep of r_m_axis_tvalid    : signal is "true"; 
    --    attribute keep of r_m_axis_tlast     : signal is "true"; 
    --    attribute keep of r_m_axis_tready    : signal is "true"; 
    --    attribute keep of r_total_packet_counter            : signal is "true"; 
    --    attribute keep of r_total_packet_splitted_counter   : signal is "true"; 
    --    attribute keep of r_total_data_frame_counter        : signal is "true"; 
    --    attribute keep of r_actual_data_frame_counter       : signal is "true"; 
    --    attribute keep of r_total_idle_frame_counter        : signal is "true"; 
    --    attribute keep of r_total_packet_drop               : signal is "true"; 
    --    attribute keep of r_frame_length_error              : signal is "true"; 
begin

    resetn         <= not rst;
    axis_if_s.tready <= s_axis_tready_aux;
    --axis_if_m.tuser  <= 'Z';
    --tx_fcch_pl_aux <= X"00000000" & tx_fcch_pl;

    -- Inst FIFO Input (acts like a cache, only to count packet size)
    Inst_axi_input_fifo : entity share_let_1g.axi4s_fifo
        generic map(
            DISTR_RAM         => false,
            FIFO_DEPTH        => C_FIFO_INPUT_FULL_DEPTH,
            DATA_WIDTH        => 8,
            FULL_THRESHOLD    => C_FIFO_INPUT_FULL_THRESH,
            EMPTY_THRESHOLD   => C_FIFO_INPUT_EMPTY_THRESH,
            USE_OUTPUT_BUFFER => false
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => axis_if_m.tdata,
            input_valid        => axis_if_m.tvalid,
            input_last         => axis_if_m.tlast,
            input_ready        => s_axis_tready_aux,
            input_almost_full  => back_pressure_high,
            input_almost_empty => back_pressure_low,
            fifo_max_level     => open,
            -- Output data handling
            -----------------------
            output             => r_s_axis_tdata,
            output_valid       => r_s_axis_tvalid,
            output_last        => r_s_axis_tlast,
            output_ready       => r_s_axis_tready
        );

    axis_payload_if_m.axis_if_m.tvalid <= r_m_axis_tvalid;
    r_m_axis_tready                <= axis_payload_if_s.axis_if_s.tready;
    axis_payload_if_m.axis_if_m.tdata  <= r_m_axis_tdata;
    axis_payload_if_m.axis_if_m.tlast  <= r_m_axis_tlast;
    axis_payload_if_m.axis_if_m.tstart <= r_m_axis_tstart;
    axis_payload_if_m.axis_if_m.tuser  <= '0';

    packet_len_fifo_rd_en <= r_s_axis_tlast and r_s_axis_tvalid and r_s_axis_tready;
    packet_id_fifo_rd_en  <= packet_len_fifo_rd_en;

    Inst_packet_len_fifo : entity share_let_1g.axi4s_fifo
        generic map(
            DISTR_RAM         => true,
            FIFO_DEPTH        => 1024,
            DATA_WIDTH        => 14,
            FULL_THRESHOLD    => 1024,
            EMPTY_THRESHOLD   => 0,
            USE_OUTPUT_BUFFER => false
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => packet_len_fifo_din,
            input_valid        => packet_len_fifo_wr_en,
            input_last         => '1',
            input_ready        => open,
            input_almost_full  => packet_len_fifo_full,
            input_almost_empty => packet_len_fifo_empty,
            fifo_max_level     => open, --packet_len_fifo_overflow,

            -- Output data handling
            -----------------------
            output             => packet_len,
            output_valid       => packet_len_fifo_valid,
            output_last        => open,
            output_ready       => packet_len_fifo_rd_en
        );

    Inst_packet_id_fifo : entity share_let_1g.axi4s_fifo
        generic map(
            DISTR_RAM         => true,
            FIFO_DEPTH        => 1024,
            DATA_WIDTH        => 14,
            FULL_THRESHOLD    => 1024,
            EMPTY_THRESHOLD   => 0,
            USE_OUTPUT_BUFFER => false
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => packet_id_fifo_din,
            input_valid        => packet_id_fifo_wr_en,
            input_last         => '1',
            input_ready        => open,
            input_almost_full  => packet_id_fifo_full,
            input_almost_empty => packet_id_fifo_empty,
            fifo_max_level     => open, --packet_id_fifo_overflow,

            -- Output data handling
            -----------------------
            output             => packet_id,
            output_valid       => packet_id_fifo_valid,
            output_last        => open,
            output_ready       => packet_id_fifo_rd_en
        );

    -- FIFO for storing the frame before send
    inst_frame_output_buf : entity share_let_1g.axi4s_fifo_v2
        generic map(
            DISTR_RAM         => false,
            FIFO_DEPTH        => C_FRAME_FULL_SIZE_BYTES,
            DATA_WIDTH        => 8,
            FULL_THRESHOLD    => C_FRAME_FULL_SIZE_BYTES,
            EMPTY_THRESHOLD   => 0,
            USE_OUTPUT_BUFFER => false
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => frame_data_in,
            input_valid        => frame_valid_in,
            input_last         => frame_last_in,
            input_start        => frame_start_in,
            input_user         => '0',
            input_ready        => frame_ready_in,
            input_almost_full  => open,
            input_almost_empty => frame_empty_in,
            fifo_max_level     => open,
            -- Output data handling
            -----------------------
            output             => r_m_axis_tdata,
            output_valid       => r_m_axis_tvalid,
            output_last        => r_m_axis_tlast,
            output_start       => r_m_axis_tstart,
            output_user        => open,
            output_ready       => r_m_axis_tready
        );

    Inst_prbs_gen : entity work.prbs_gen
        port map(
            clk          => clk,        --: in std_logic;
            rst          => rst,        --: in std_logic;
            seed         => prbs_seed,  --frame_header_fields.txfn, --: in std_logic_vector(15 downto 0);
            scramble_rst => prbs_scramble_rst, --: in  std_logic;
            input_valid  => prbs_input_valid, --: in  std_logic;
            input_last   => prbs_input_last, --: in  std_logic;
            input_ready  => prbs_input_ready, --: out std_logic;
            output_data  => prbs_output_data, --: out std_logic_vector(8 - 1 downto 0);
            output_valid => prbs_output_valid, --: out std_logic;
            output_last  => prbs_output_last, --: out std_logic;
            output_ready => '1'         --prbs_output_ready --: in  std_logic
        );
    prbs_seed <= std_logic_vector(unsigned(frame_header_fields.txfn) + 1);

    -- Process input data FSM
    pr_input_ctrl : process(clk)
        variable v_data_in             : std_logic_vector(7 downto 0);
        variable v_frame_header_vector : std_logic_vector((C_FRAME_HEADER_SIZE * 8) - 1 downto 0);
        variable v_mgmt_header_vector  : std_logic_vector((C_MGMT_PAYLOAD_SIZE * 8) - 1 downto 0);

    begin
        if rising_edge(clk) then
            if rst = '1' then
                input_fsm                       <= IDLE;
                input_ready_int                 <= '0';
                data_in                         <= (others => '0');
                packet_fsm                      <= NO_SPLIT;
                frame_cnt                       <= (others => '0');
                frame_header_cnt                <= (others => '0');
                mgmt_payload_cnt                <= (others => '0');
                r_s_axis_tready                 <= '0';
                header_cnt                      <= (others => '0');
                payload_seq_num                 <= (others => '0');
                packet_id_last                  <= (others => '1');
                r_total_packet_counter          <= (others => '0');
                r_total_packet_splitted_counter <= (others => '0');
                r_total_data_frame_counter      <= (others => '0');
                r_total_mgmt_frame_counter      <= (others => '0');
                r_total_mgmt_resp_frame_counter <= (others => '0');
                r_actual_data_frame_counter     <= (others => '0');
                r_total_idle_frame_counter      <= (others => '0');
                r_total_packet_drop             <= (others => '0');
                r_frame_length_error            <= (others => '0');
                timeout_en                      <= '0';
                packet_header                   <= (others => '0');
                payload_header                  <= (others => '0');
                payload_len_remaining           <= (others => '0');
                current_packet_len_cnt          <= (others => '0');
                frame_sr_cnt                    <= (others => '0');
                frame_data_in                   <= (others => '0');
                frame_header_fields             <= C_FRAME_HEADER_INIT;
                prbs_scramble_rst               <= '0';
                prbs_input_valid                <= '0';
                prbs_input_last                 <= '0';
                frame_valid_in                  <= '0';
                frame_last_in                   <= '0';
                frame_start_in                  <= '0';
                r_tx_ts                         <= (others => '0');
                --tx_ts_rd_en <= '0';
                --            tx_timing_data.tx_fn         <= (others =>'0');
                --            tx_timing_data.tx_ts        <= (others =>'0');
                --            --tx_timing_data.ts_applies   <= (others =>'0');
                --            tx_timing_data_valid        <= '0';
                send_mgmt_response_ack          <= '0';
                mgmt_payload                    <= C_MGMT_RESPONSE_INIT;
                mgmt_seq                        <= (others => '0');
                axis_payload_if_m.tx_fn           <= (others => '0');
                axis_payload_if_m.frame_type      <= (others => '0');
                axis_payload_if_m.tx_num          <= (others => '0');

            else

                r_s_axis_tready        <= '0';
                timeout_en             <= '0';
                prbs_input_valid       <= '0';
                prbs_scramble_rst      <= '0';
                frame_valid_in         <= '0';
                frame_start_in         <= '0';
                --tx_ts_rd_en             <= '0';
                --tx_timing_data_valid    <= '0';
                send_mgmt_response_ack <= '0';

                --debug:
                --word_data <= word_data(32-8-1 downto 0) & r_s_axis_tdata;

                case input_fsm is

                    -- Wait for the r_m_axis_tready
                    when IDLE =>
                        input_fsm        <= IDLE;
                        frame_cnt        <= (others => '0');
                        header_cnt       <= (others => '0');
                        frame_header_cnt <= (others => '0');
                        mgmt_payload_cnt <= (others => '0');
                        frame_sr_cnt     <= (others => '0');
                        if r_m_axis_tready = '1' and overload = '0' and frame_empty_in = '1' and frame_ready_in = '1' then
                            input_fsm <= SET_FRAME_HEADER;
                        end if;

                    when SET_FRAME_HEADER =>
                        -- Enable timeout. If new packet available, set frame header to DATA, if not, IDLE.
                        timeout_en <= '1';
                        --tx_fcch_rd_en   <= '0'; 
                        if send_ranging_req = '1' and packet_fsm = NO_SPLIT then
                            timeout_en                          <= '0';
                            input_fsm                           <= APPEND_FRAME_HEADER;
                            mgmt_payload.mgmt_type              <= C_MGMT_REQ;
                            mgmt_payload.mgmt_seq               <= std_logic_vector(mgmt_seq);
                            mgmt_payload.response_valid         <= '0';
                            mgmt_payload.ranging_meas_valid     <= '0';
                            mgmt_payload.ranging_measure_meters <= x"ABCD7777";

                            --                        frame_header_fields.frame_type  <= C_FRAME_MGMT;  
                            --                        frame_header_fields.txfn        <= std_logic_vector(fh_txfn_cnt); 
                            --frame_header_fields.ts_applies  <= "011";   

                            r_total_mgmt_frame_counter <= r_total_mgmt_frame_counter + 1;

                        elsif send_mgmt_response_valid = '1' and packet_fsm = NO_SPLIT then
                            timeout_en                     <= '0';
                            input_fsm                      <= APPEND_FRAME_HEADER;
                            send_mgmt_response_ack         <= '1';
                            mgmt_payload                   <= send_mgmt_response;
                            frame_header_fields.frame_type <= C_FRAME_MGMT;
                            frame_header_fields.txfn       <= std_logic_vector(fh_txfn_cnt);
                            --frame_header_fields.ts_applies  <= "011";   

                            r_total_mgmt_resp_frame_counter <= r_total_mgmt_resp_frame_counter + 1;
                        

                        elsif r_s_axis_tvalid = '1' and packet_len_fifo_valid = '1' and unsigned(packet_len) > 0 then
                            timeout_en                     <= '0';
                            input_fsm                      <= APPEND_FRAME_HEADER;
                            frame_header_fields.frame_type <= C_FRAME_DATA;
                            frame_header_fields.txfn       <= std_logic_vector(fh_txfn_cnt);
                            --frame_header_fields.ts_applies <= "011";   
                            r_total_data_frame_counter <= r_total_data_frame_counter + 1;

                        elsif timeout_cnt >= C_TIMEOUT then
                            input_fsm                      <= APPEND_FRAME_HEADER;
                            frame_header_fields.frame_type <= C_FRAME_IDLE;
                            frame_header_fields.txfn       <= std_logic_vector(fh_txfn_cnt);
                            --frame_header_fields.ts_applies <= "011";

                            r_total_idle_frame_counter <= r_total_idle_frame_counter + 1;
                            packet_fsm                 <= NO_SPLIT;
                            timeout_en                 <= '0';
                        end if;

                    -- DEPRECATED: 
                    when APPEND_FRAME_HEADER =>

                        frame_header_cnt  <= (others => '0');
                        prbs_scramble_rst <= '1';
                        if frame_header_fields.frame_type = C_FRAME_IDLE then
                            input_fsm <= APPEND_PRBS;
                        elsif frame_header_fields.frame_type = C_FRAME_MGMT then
                            input_fsm <= APPEND_MGMT;
                        else
                            input_fsm <= START;
                        end if;

                        axis_payload_if_m.tx_fn      <= frame_header_fields.txfn;
                        axis_payload_if_m.frame_type <= frame_header_fields.frame_type;         

                    when APPEND_MGMT =>
                        if mgmt_payload_cnt < C_MGMT_PAYLOAD_SIZE then
                            v_mgmt_header_vector := mgmt_response2slv(mgmt_payload);
                            --v_frame_header_vector := c_header_vector;
                            v_data_in            := v_mgmt_header_vector(((to_integer(mgmt_payload_cnt) + 1) * 8) - 1 downto to_integer(mgmt_payload_cnt) * 8);
                            frame_data_in        <= v_data_in;
                            frame_valid_in       <= '1';
                            if mgmt_payload_cnt = 0 then
                                frame_start_in <= '1';
                            end if;
                            frame_cnt            <= frame_cnt + 1;
                            mgmt_payload_cnt     <= mgmt_payload_cnt + 1;
                            prbs_scramble_rst    <= '1';
                        else
                            mgmt_payload_cnt <= (others => '0');
                            input_fsm        <= APPEND_PRBS;
                            mgmt_seq         <= mgmt_seq + 1;
                        end if;

                    -- 
                    when START =>
                        if packet_fsm = NO_SPLIT then
                            input_fsm              <= APPEND_PAYLOAD_HEADER;
                            payload_header         <= C_PAYLOAD_PREAMBLE & std_logic_vector(payload_seq_num) & "00000000000000";
                            current_packet_len_cnt <= (others => '0');
                            payload_len_remaining  <= (others => '0');
                        elsif packet_fsm = SPLITTED and payload_len_remaining /= 0 then
                            input_fsm      <= APPEND_PAYLOAD_HEADER;
                            payload_header <= C_PAYLOAD_PREAMBLE & std_logic_vector(payload_seq_num) & std_logic_vector(payload_len_remaining);
                        elsif packet_fsm = SPLITTED and payload_len_remaining = 0 then
                            input_fsm              <= DROP_PACKET;
                            current_packet_len_cnt <= (others => '0');
                            packet_fsm             <= NO_SPLIT;
                        end if;

                    when APPEND_PAYLOAD_HEADER =>
                        input_fsm <= APPEND_PAYLOAD_HEADER;
                        if header_cnt < C_HEADER_SIZE then
                            v_data_in      := payload_header(((to_integer(header_cnt) + 1) * 8) - 1 downto to_integer(header_cnt) * 8);
                            frame_data_in  <= v_data_in;
                            frame_valid_in <= '1';
                            if header_cnt = 0 then
                                frame_start_in <= '1';
                            end if;
                            frame_cnt      <= frame_cnt + 1;
                            header_cnt     <= header_cnt + 1;
                        elsif header_cnt > C_HEADER_SIZE - 1 then
                            --                        if frame_header_fields.frame_type = C_FRAME_IDLE then
                            --                            input_fsm <= APPEND_PRBS; 
                            --                            header_cnt <= (others => '0');
                            --                            payload_seq_num <= payload_seq_num + 1;
                            if packet_fsm = NO_SPLIT then
                                input_fsm       <= WAIT_PACKET;
                                header_cnt      <= (others => '0');
                                payload_seq_num <= payload_seq_num + 1;
                            elsif packet_fsm = SPLITTED then
                                input_fsm       <= WRITE_DATA;
                                r_s_axis_tready <= '0';
                                packet_fsm      <= NO_SPLIT;
                                header_cnt      <= (others => '0');
                                payload_seq_num <= payload_seq_num + 1;
                            end if;

                        end if;

                    -- Be sure there is a packet to be read. If timeout, go to padding.
                    when WAIT_PACKET =>
                        timeout_en <= '1';

                        if allow_packet_insert = '0' then
                            input_fsm  <= APPEND_PADDING;
                            timeout_en <= '0';
                        elsif r_s_axis_tvalid = '1' and allow_packet_insert = '1' then
                            timeout_en <= '0';
                            input_fsm  <= SET_PACKET_HEADER;
                        --read_en packet lenght fifo                  
                        elsif timeout_cnt >= C_TIMEOUT then
                            input_fsm  <= APPEND_PADDING;
                            timeout_en <= '0';
                        end if;

                    when SET_PACKET_HEADER =>
                        if allow_packet_insert = '0' then
                            input_fsm <= APPEND_PADDING;
                        else
                            --read packet lenght fifo 
                            if packet_len_fifo_valid = '1' and unsigned(packet_len) > 0 then
                                packet_header <= C_PACKET_PREAMBLE & "00" & packet_len; --std_logic_vector(packet_len);
                                input_fsm     <= ALIGN_PACKET_32;
                            elsif packet_len_fifo_valid = '1' and unsigned(packet_len) = 0 then
                                input_fsm <= DROP_PACKET;
                            else
                                input_fsm <= APPEND_PADDING;
                            end if;
                        end if;

                    when DROP_PACKET =>
                        r_total_packet_drop <= r_total_packet_drop + 1;
                        r_s_axis_tready     <= '1';
                        if r_s_axis_tvalid = '1' and r_s_axis_tlast = '1' and r_s_axis_tready = '1' then
                            r_s_axis_tready <= '0';
                            input_fsm       <= WAIT_PACKET;
                        end if;

                    when ALIGN_PACKET_32 =>
                        input_fsm <= ALIGN_PACKET_32;
                        if packet_32b_align = '1' then
                            if allow_packet_insert = '1' then
                                input_fsm <= APPEND_PACKET_HEADER;
                            else
                                if frame_len_limit = '1' then
                                    frame_data_in  <= x"00";
                                    frame_valid_in <= '1';
                                    frame_cnt      <= frame_cnt + 1;
                                    input_fsm      <= SEND_FRAME;
                                else
                                    input_fsm <= APPEND_PADDING;
                                end if;
                            end if;
                        else
                            if frame_len_limit = '1' then
                                frame_data_in  <= x"00";
                                frame_valid_in <= '1';
                                frame_cnt      <= frame_cnt + 1;
                                input_fsm      <= SEND_FRAME;
                            else
                                frame_data_in  <= x"00";
                                frame_valid_in <= '1';
                                frame_cnt      <= frame_cnt + 1;
                            end if;
                        end if;

                    -- There is a packet waiting, append the header first
                    when APPEND_PACKET_HEADER =>
                        input_fsm <= APPEND_PACKET_HEADER;
                        if header_cnt < C_HEADER_SIZE then
                            v_data_in      := packet_header(((to_integer(header_cnt) + 1) * 8) - 1 downto to_integer(header_cnt) * 8);
                            frame_data_in  <= v_data_in;
                            frame_valid_in <= '1';
                            frame_cnt      <= frame_cnt + 1;
                            header_cnt     <= header_cnt + 1;
                        elsif header_cnt > C_HEADER_SIZE - 1 then
                            input_fsm              <= WRITE_DATA;
                            header_cnt             <= (others => '0');
                            r_total_packet_counter <= r_total_packet_counter + 1;
                        end if;

                    -- Write packet data
                    when WRITE_DATA =>
                        input_fsm       <= WRITE_DATA;
                        r_s_axis_tready <= '0';

                        if r_s_axis_tvalid = '1' then
                            r_s_axis_tready <= '1';
                        end if;

                        if r_s_axis_tvalid = '1' and r_s_axis_tlast = '0' and r_s_axis_tready = '1' then
                            r_s_axis_tready        <= '1';
                            frame_data_in          <= r_s_axis_tdata;
                            frame_valid_in         <= '1';
                            frame_cnt              <= frame_cnt + 1;
                            current_packet_len_cnt <= current_packet_len_cnt + 1;
                            if frame_len_limit = '1' then
                                r_s_axis_tready <= '0';

                                if current_packet_len_cnt < (unsigned(packet_len) - 1) then
                                    packet_fsm                      <= SPLITTED;
                                    payload_len_remaining           <= unsigned(packet_len) - current_packet_len_cnt - 1;
                                    r_total_packet_splitted_counter <= r_total_packet_splitted_counter + 1;
                                else
                                    payload_len_remaining <= (others => '0');
                                    packet_fsm            <= NO_SPLIT;
                                end if;

                                input_fsm <= SEND_FRAME;

                            end if;
                        elsif r_s_axis_tvalid = '1' and r_s_axis_tlast = '1' and r_s_axis_tready = '1' then
                            frame_data_in          <= r_s_axis_tdata;
                            frame_valid_in         <= '1';
                            frame_cnt              <= frame_cnt + 1;
                            r_s_axis_tready        <= '0';
                            current_packet_len_cnt <= (others => '0');
                            packet_fsm             <= NO_SPLIT;
                            input_fsm              <= WAIT_PACKET;
                            if frame_len_limit = '1' then
                                r_s_axis_tready <= '0';

                                if current_packet_len_cnt < (unsigned(packet_len) - 1) then
                                    packet_fsm                      <= SPLITTED;
                                    payload_len_remaining           <= unsigned(packet_len) - current_packet_len_cnt - 1;
                                    r_total_packet_splitted_counter <= r_total_packet_splitted_counter + 1;
                                else
                                    payload_len_remaining <= (others => '0');
                                    packet_fsm            <= NO_SPLIT;
                                end if;

                                input_fsm <= SEND_FRAME;

                            else
                                -- if end of packet (not splitted) and ranging service request are active, 
                                -- append padding so the next frame can provide ranging service without 
                                -- spliting packets.
                                if send_ranging_req = '1' or send_mgmt_response_valid = '1' then
                                    r_s_axis_tready <= '0';
                                    input_fsm       <= APPEND_PADDING;
                                end if;
                            end if;
                        end if;

                    --                    if frame_len_limit = '1' then
                    --                        r_s_axis_tready <= '0';
                    --                
                    --                        if current_packet_len_cnt < (unsigned(packet_len) - 1) then
                    --                            packet_fsm <= SPLITTED;
                    --                            payload_len_remaining <= unsigned(packet_len) - current_packet_len_cnt - 1;
                    --                            r_total_packet_splitted_counter <= r_total_packet_splitted_counter + 1;
                    --                        else
                    --                            payload_len_remaining <= (others => '0');
                    --                            packet_fsm <= NO_SPLIT;
                    --                        end if;
                    --                        
                    --                        input_fsm <= SEND_FRAME;
                    --
                    --                    end if;

                    -- Fill with 0's
                    when APPEND_PADDING =>
                        if frame_len_limit = '0' then
                            frame_data_in  <= x"00";
                            frame_valid_in <= '1';
                            frame_cnt      <= frame_cnt + 1;
                            input_fsm      <= APPEND_PADDING;
                        elsif frame_len_limit = '1' then
                            frame_data_in  <= x"00";
                            frame_valid_in <= '1';
                            frame_cnt      <= frame_cnt + 1;
                            input_fsm      <= SEND_FRAME;
                        end if;

                    when APPEND_PRBS =>
                        prbs_input_valid <= '1';
                        if frame_cnt = 0 then
                            frame_start_in <= '1';
                        end if;
                        if frame_len_limit = '0' then
                            if prbs_output_valid = '1' then
                                frame_data_in  <= prbs_output_data;
                                frame_valid_in <= '1';
                                frame_cnt      <= frame_cnt + 1;
                                input_fsm      <= APPEND_PRBS;
                            end if;
                        elsif frame_len_limit = '1' then
                            if prbs_output_valid = '1' then
                                frame_data_in    <= prbs_output_data;
                                frame_valid_in   <= '1';
                                frame_cnt        <= frame_cnt + 1;
                                input_fsm        <= SEND_FRAME;
                                prbs_input_valid <= '0';
                            end if;
                        end if;

                    when SEND_FRAME =>
                        r_s_axis_tready <= '0';
                        if frame_cnt = C_FSO_FRAME_SIZE then
                            input_fsm <= SEND_FRAME;
                            if frame_empty_in = '1' and cache_fifo_actual_empty = '1' then
                                input_fsm                   <= IDLE;
                                r_actual_data_frame_counter <= r_actual_data_frame_counter + 1;
                            end if;
                        else
                            --Error! if data didnt reach full frame length, insert data to not desyncronize Creonic encoder.
                            report "Error, C_FSO_FRAME_SIZE";
                            frame_data_in        <= x"00";
                            frame_valid_in       <= '1';
                            frame_cnt            <= frame_cnt + 1;
                            input_fsm            <= SEND_FRAME;
                            r_frame_length_error <= r_frame_length_error + 1;
                        end if;

                    when others =>
                        input_fsm <= IDLE;

                end case;

                if frame_len_limit = '1' then
                    frame_last_in <= '1';
                else
                    frame_last_in <= '0';
                end if;

                if clear_stat = '1' then
                    r_total_packet_counter          <= (others => '0');
                    r_total_packet_splitted_counter <= (others => '0');
                    r_total_data_frame_counter      <= (others => '0');
                    r_total_mgmt_frame_counter      <= (others => '0');
                    r_total_mgmt_resp_frame_counter <= (others => '0');
                    r_total_idle_frame_counter      <= (others => '0');
                    r_actual_data_frame_counter     <= (others => '0');
                    r_frame_length_error            <= (others => '0');

                end if;
                -- STATISTICS
                --total_packet_counter            <= std_logic_vector(r_total_packet_counter);
                --total_packet_splitted_counter   <= std_logic_vector(r_total_packet_splitted_counter);
                --total_data_frame_counter        <= std_logic_vector(r_total_data_frame_counter);
                --actual_data_frame_counter       <= std_logic_vector(r_actual_data_frame_counter);
                --total_idle_frame_counter        <= std_logic_vector(r_total_idle_frame_counter);
                --total_packet_drop               <= std_logic_vector(r_total_packet_drop);
                --frame_length_error              <= std_logic_vector(r_frame_length_error);

            end if;
        end if;

    end process;

    -- Process to calculate input packet lenght
    pr_cacl_packet_len : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                packet_len_cnt        <= (others => '0');
                packet_id_cnt         <= (others => '0');
                packet_len_fifo_din   <= (others => '0');
                packet_len_fifo_wr_en <= '0';
                packet_id_fifo_din    <= (others => '0');
                packet_id_fifo_wr_en  <= '0';
            else
                packet_len_fifo_wr_en <= '0';
                packet_id_fifo_wr_en  <= '0';
                if axis_if_m.tvalid = '1' and s_axis_tready_aux = '1' and axis_if_m.tlast = '0' then
                    packet_len_cnt <= packet_len_cnt + 1;
                elsif axis_if_m.tvalid = '1' and s_axis_tready_aux = '1' and axis_if_m.tlast = '1' then
                    -- tuser = '1' , means error. Set packet length to 0 so the FSM will dropt it.
                    if axis_if_m.tuser = '1' then
                        packet_len_fifo_din   <= (others => '0');
                        packet_len_fifo_wr_en <= '1';
                        packet_len_cnt        <= (others => '0');
                    else
                        packet_len_fifo_din   <= std_logic_vector(packet_len_cnt + 1);
                        packet_len_fifo_wr_en <= '1';
                        packet_len_cnt        <= (others => '0');
                    end if;
                    --track id of packet. Internal use only.
                    packet_id_fifo_din   <= std_logic_vector(packet_id_cnt);
                    packet_id_fifo_wr_en <= '1';
                    packet_id_cnt        <= packet_id_cnt + 1;
                end if;
            end if;
        end if;
    end process;

    -- Combinational:
    allow_packet_insert <= '1' when (frame_cnt < C_FSO_FRAME_SIZE - G_MINIMUN_FREE_BYTES_TO_SPLIT) else '0';
    frame_len_limit     <= '1' when (frame_cnt = C_FSO_FRAME_SIZE - 1) else '0';
    packet_32b_align    <= '1' when (to_integer(frame_cnt) mod 4 = 0) else '0';

    -- Time out packet process
    pr_packet_timeout : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                timeout_cnt <= (others => '0');
            else
                if timeout_en = '1' then
                    timeout_cnt <= timeout_cnt + 1;
                else
                    timeout_cnt <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    -- Counters managing process
    pr_counter : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                fh_txfn_cnt       <= (others => '0');
                r_frame_len_limit <= '0';
            else
                r_frame_len_limit <= frame_len_limit;
                if frame_len_limit = '1' and r_frame_len_limit = '0' then
                    fh_txfn_cnt <= fh_txfn_cnt + 1;
                end if;
            end if;
        end if;
    end process;

    -- Counters managing process
    pr_cache_empty : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                cache_fifo_actual_empty <= '1';
            else
                if r_m_axis_tvalid = '1' and r_m_axis_tready = '1' and r_m_axis_tlast = '1' then
                    cache_fifo_actual_empty <= '1';
                elsif frame_valid_in = '1' and frame_ready_in = '1' and frame_last_in = '0' then
                    cache_fifo_actual_empty <= '0';
                end if;
            end if;
        end if;
    end process;

    -- STATISTICS
    total_packet_counter          <= std_logic_vector(r_total_packet_counter);
    total_packet_splitted_counter <= std_logic_vector(r_total_packet_splitted_counter);
    total_data_frame_counter      <= std_logic_vector(r_total_data_frame_counter);
    actual_data_frame_counter     <= std_logic_vector(r_actual_data_frame_counter);
    total_idle_frame_counter      <= std_logic_vector(r_total_idle_frame_counter);
    total_packet_drop             <= std_logic_vector(r_total_packet_drop);
    frame_length_error            <= std_logic_vector(r_frame_length_error);

end Behavioral;
