-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: ethernet_framer_top
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
library share_let_1g;
library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;
library monitoring_lib;
--use let_sda_lib.let_pckg_components.bitrate_calculator;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library ethernet_framer_lib;
use ethernet_framer_lib.pkg_components.all;

entity ethernet_framer_top is
    generic(
        G_MINIMUN_FREE_BYTES_TO_SPLIT : NATURAL := 8;
        G_STARTUP_DELAY               : INTEGER := 30000
    );
    port(
        clk                           : in    std_logic;
        rst                           : in    std_logic;
        -- Input data 
        ----------------------
        axis_if_m                       : in t_axis_if_m;
        axis_if_s                       : out t_axis_if_s;

        cfg_pause_data                : in    std_logic_vector(15 downto 0);
        tx_pause_valid                : out   std_logic;
        tx_pause_data                 : out   std_logic_vector(15 downto 0);
        tx_pause_ready                : in    std_logic;
        rx_fifo_skipped_frame         : out   std_logic_vector(32 - 1 downto 0);
        -- Control (subject to change in the future)
        ---------------------- 
        --tx_ts         : in std_logic_vector(40-1 downto 0);
        --tx_ts_valid   : in std_logic;
        --tx_ts_rd_en   : out std_logic;

        -- To frame tof calculation (subject to change in the future)
        ---------------------- 
        --tx_timing_data          : out tx_timing_data_t;
        --tx_timing_data_valid    : out std_logic;

        --To frame tof calculation:
        --send_ranging_req            : in std_logic;
        --t1_tx_fn                    : out std_logic_vector(15 downto 0);
        --t1_tx_fn_valid              : out std_logic;
        --
        --send_t3                     : in std_logic;
        --t3_tx_fn                    : out std_logic_vector(15 downto 0);
        --t3_tx_fn_valid              : out std_logic;
        --send_mgmt_response          : in mgmt_payload_t;
        --send_mgmt_response_valid    : in std_logic;
        --send_mgmt_response_ack      : out std_logic;

        -- Output data 
        ----------------------
        axis_payload_if_m               : out t_axis_payload_if_m;
        axis_payload_if_s               : in t_axis_payload_if_s;

        -- Statistics
        ----------------------
        clear_stat                    : in    std_logic;
        total_packet_counter          : out   std_logic_vector(31 downto 0);
        total_packet_splitted_counter : out   std_logic_vector(31 downto 0);
        total_data_frame_counter      : out   std_logic_vector(31 downto 0);
        actual_data_frame_counter     : out   std_logic_vector(31 downto 0);
        total_idle_frame_counter      : out   std_logic_vector(31 downto 0);
        total_packet_drop             : out   std_logic_vector(31 downto 0);
        total_prebuffer_packet_drop   : out   std_logic_vector(31 downto 0);
        frame_length_error            : out   std_logic_vector(31 downto 0);
        bitrate_in                    : out   std_logic_vector(31 downto 0);
        packet_cnt_in                 : out   std_logic_vector(31 downto 0)
    );
end ethernet_framer_top;

architecture Behavioral of ethernet_framer_top is

    constant C_FIFO_INPUT_FULL_DEPTH   : natural := 16384;
    constant C_FIFO_INPUT_FULL_THRESH  : natural := C_FIFO_INPUT_FULL_DEPTH - 4000; -- 
    constant C_FIFO_INPUT_EMPTY_THRESH : natural := C_FIFO_INPUT_FULL_DEPTH * 1 / 5; -- 20%

    constant C_FIFO_OUTPUT_FULL_DEPTH   : natural := 16384;
    constant C_FIFO_OUTPUT_FULL_THRESH  : natural := C_FIFO_INPUT_FULL_DEPTH * 3 / 4; -- 
    constant C_FIFO_OUTPUT_EMPTY_THRESH : natural := C_FIFO_INPUT_FULL_DEPTH * 1 / 5; -- 20%

    constant C_METADATA_LENGTH : natural := axis_payload_if_m.tx_fn'length + axis_payload_if_m.frame_type'length + axis_payload_if_m.tx_num'length;

    --constant axis_prog_full_thresh : std_logic_vector(14 DOWNTO 0) := std_logic_vector(to_unsigned(32768-4000,15));
    --constant axis_prog_empty_thresh : std_logic_vector(14 DOWNTO 0):= std_logic_vector(to_unsigned(5000,15));

    constant C_WAIT_CYCLES_AFTER_PAUSE_REQ : unsigned(32 - 1 downto 0)     := x"00001900"; -- 60us  old:-- x"00000190" 2us @ 200 kHz
    --constant C_WAIT_CYCLES_AFTER_PAUSE_REQ  : unsigned(32-1 downto 0) := x"00009C40";-- x100 
    constant C_THRESH_PAUSE_ACTIVATE       : std_logic_vector(15 DOWNTO 0) := std_logic_vector(to_unsigned(14000, 16));

    signal s_axis_tdata_int  : std_logic_vector(8 - 1 downto 0);
    signal s_axis_tvalid_int : std_logic;
    signal s_axis_tlast_int  : std_logic;
    signal s_axis_tready_int : std_logic;

    -- AXI INPUT FIFO
    signal r_s_axis_tdata  : std_logic_vector(8 - 1 downto 0);
    signal r_s_axis_tvalid : std_logic;
    signal r_s_axis_tlast  : std_logic;
    signal r_s_axis_tready : std_logic;
    signal r_s_axis_tuser  : std_logic;

    signal input_almost_full_int_let_clk : std_logic;
    signal input_almost_full_int         : std_logic;
    signal input_almost_empty_int        : std_logic;
    signal r_input_almost_empty_int      : std_logic;
    signal input_read_en                 : std_logic;
    signal mac_valid_int                 : std_logic;
    signal rx_fifo_skip_cnt              : unsigned(31 downto 0);
    signal axis_wr_data_count            : std_logic_vector(15 downto 0);
    signal axis_rd_data_count            : std_logic_vector(15 downto 0);
    signal pause_req_wait_cnt            : unsigned(32 - 1 downto 0);
    type t_pause_ctrl_fsm_state is (IDLE, SEND_REQ_PAUSE, SEND_REQ_NULL, WAIT_REQ);
    signal pause_ctl_state               : t_pause_ctrl_fsm_state;
    signal clear_pause_flag              : std_logic;
    signal r_cfg_pause_data              : std_logic_vector(15 downto 0);

    signal activate_pause_req : std_logic;
    signal pause_requested    : std_logic;

    signal r_s_axis_m         : t_axis_if_m;
    signal r_s_axis_s         : t_axis_if_s;
    -- AXI OUTPUT FIFO
    signal r_m_axis_payload_m : t_axis_payload_if_m;
    signal r_m_axis_payload_s : t_axis_payload_if_s;

    signal overload_control : std_logic;
    signal startup_delay    : std_logic;
    signal axis_prog_full   : std_logic;
    signal delay_cnt        : integer range 0 to G_STARTUP_DELAY; -- aproximately 1.4 us of delay needed. @200Mhz -> 300 clocks is good enough.

    signal resetn_eth                    : std_logic;
    signal resetn_let                    : std_logic;
    signal r_activate_pause_req          : std_logic;
    signal back_pressure_high            : std_logic;
    signal back_pressure_low             : std_logic;
    signal fifo_input_almost_full        : std_logic;
    signal fifo_input_almost_empty       : std_logic;
    signal r_total_prebuffer_packet_drop : unsigned(31 downto 0);

    signal metadata_in        : std_logic_vector(c_metadata_length - 1 downto 0);
    signal metadata_valid     : std_logic;
    signal metadata_buf       : std_logic_vector(c_metadata_length - 1 downto 0);
    signal metadata_buf_valid : std_logic;
    signal metadata_buf_ready : std_logic;
    signal tx_fn_buf          : std_logic_vector(15 downto 0); -- Sequence number of this Tx Frame
    signal frame_type_buf     : std_logic_vector(1 downto 0); -- Frame Type
    signal tx_num_buf         : std_logic_vector(2 downto 0); -- Transmission atempt

begin

    metadata_buf_ready            <= axis_payload_if_m.axis_if_m.tvalid and axis_payload_if_m.axis_if_m.tlast and axis_payload_if_s.axis_if_s.tready;
    metadata_in                   <= r_m_axis_payload_m.tx_num & r_m_axis_payload_m.frame_type & r_m_axis_payload_m.tx_fn;
    metadata_valid                <= r_m_axis_payload_m.axis_if_m.tstart;
    axis_payload_if_m.tx_fn         <= metadata_buf(15 downto 0);
    axis_payload_if_m.frame_type    <= metadata_buf(17 downto 16);
    axis_payload_if_m.tx_num        <= metadata_buf(20 downto 18);
    axis_payload_if_m.axis_if_m.tuser <= '0';

    -- Inst FIFO Input 
    Inst_axi_input_fifo : entity share_let_1g.axi4s_fifo_v2
        generic map(
            DISTR_RAM         => false,
            FIFO_DEPTH        => C_FIFO_INPUT_FULL_DEPTH,
            DATA_WIDTH        => 8,
            FULL_THRESHOLD    => C_FIFO_INPUT_FULL_THRESH,
            EMPTY_THRESHOLD   => C_FIFO_INPUT_EMPTY_THRESH,
            USE_OUTPUT_BUFFER => false
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => axis_if_m.tdata,
            input_valid        => s_axis_tvalid_int,
            input_last         => axis_if_m.tlast,
            input_user         => axis_if_m.tuser,
            input_start        => axis_if_m.tstart,
            input_ready        => s_axis_tready_int,
            input_almost_full  => fifo_input_almost_full,
            input_almost_empty => fifo_input_almost_empty,
            fifo_max_level     => open,
            -- Output data handling
            -----------------------
            output             => r_s_axis_m.tdata,
            output_valid       => r_s_axis_m.tvalid,
            output_last        => r_s_axis_m.tlast,
            output_user        => r_s_axis_m.tuser,
            output_start       => r_s_axis_m.tstart,
            output_ready       => r_s_axis_s.tready
        );

    Inst_axi_output_fifo : entity share_let_1g.axi4s_fifo_v2
        generic map(
            DISTR_RAM         => false,
            FIFO_DEPTH        => C_FIFO_OUTPUT_FULL_DEPTH,
            DATA_WIDTH        => 8,
            FULL_THRESHOLD    => C_FIFO_OUTPUT_FULL_THRESH,
            EMPTY_THRESHOLD   => C_FIFO_OUTPUT_EMPTY_THRESH,
            USE_OUTPUT_BUFFER => false
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => r_m_axis_payload_m.axis_if_m.tdata,
            input_valid        => r_m_axis_payload_m.axis_if_m.tvalid,
            input_last         => r_m_axis_payload_m.axis_if_m.tlast,
            input_ready        => r_m_axis_payload_s.axis_if_s.tready,
            input_user         => '0',
            input_start        => r_m_axis_payload_m.axis_if_m.tstart,
            input_almost_full  => axis_prog_full,
            input_almost_empty => open,
            fifo_max_level     => open,
            -- Output data handling
            -----------------------
            output             => axis_payload_if_m.axis_if_m.tdata,
            output_valid       => axis_payload_if_m.axis_if_m.tvalid,
            output_last        => axis_payload_if_m.axis_if_m.tlast,
            output_ready       => axis_payload_if_s.axis_if_s.tready,
            output_user        => open,
            output_start       => axis_payload_if_m.axis_if_m.tstart
        );

    -- FIFO buffer for metadata. Only frame_type is used
    inst_metadata_buffer_output : entity share_let_1g.axi4s_fifo
        generic map(
            DISTR_RAM         => true,
            FIFO_DEPTH        => 64,
            DATA_WIDTH        => C_METADATA_LENGTH, -- tx_fn+frame_type+tx_num
            FULL_THRESHOLD    => 64,
            EMPTY_THRESHOLD   => 0,
            USE_OUTPUT_BUFFER => false
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => metadata_in,
            input_valid        => metadata_valid,
            input_last         => '1',
            input_ready        => open,
            input_almost_full  => open,
            input_almost_empty => open,
            fifo_max_level     => open,
            -- Output data handling
            -----------------------
            output             => metadata_buf,
            output_valid       => metadata_buf_valid,
            output_last        => open,
            output_ready       => metadata_buf_ready -- to sync with the payload
        );

    inst_ethernet_framer : entity ethernet_framer_lib.ethernet_framer
        generic map(
            G_MINIMUN_FREE_BYTES_TO_SPLIT => G_MINIMUN_FREE_BYTES_TO_SPLIT
        )
        port map(
            clk                           => clk, -- : in  std_logic;
            rst                           => rst, -- : in  std_logic;

            -- Input data 
            ----------------------
            axis_if_m                       => r_s_axis_m,
            axis_if_s                       => r_s_axis_s,
            
            -- Output data 
            ----------------------
            axis_payload_if_m               => r_m_axis_payload_m, --: inout t_axis_payload_if;
            axis_payload_if_s               => r_m_axis_payload_s, --: inout t_axis_payload_if;

            -- Control
            ---------------------- 
            overload                      => overload_control,
            back_pressure_high            => back_pressure_high, -- : out std_logic;
            back_pressure_low             => back_pressure_low, -- : out std_logic;

            send_ranging_req              => '0', --: in    std_logic;
            send_mgmt_response            => C_MGMT_RESPONSE_INIT, --: in    mgmt_payload_t;
            send_mgmt_response_valid      => '0', --: in    std_logic;
            send_mgmt_response_ack        => open, --: out   std_logic;

            -- Statistics
            ----------------------            
            clear_stat                    => clear_stat,
            total_packet_counter          => total_packet_counter, --: out std_logic_vector(31 downto 0);
            total_packet_splitted_counter => total_packet_splitted_counter, --: out std_logic_vector(31 downto 0);
            total_data_frame_counter      => total_data_frame_counter, --: out std_logic_vector(31 downto 0);
            actual_data_frame_counter     => actual_data_frame_counter, --: out std_logic_vector(31 downto 0);
            total_idle_frame_counter      => total_idle_frame_counter, --: out std_logic_vector(31 downto 0);
            total_packet_drop             => total_packet_drop, --: out std_logic_vector(31 downto 0);
            frame_length_error            => frame_length_error --: out std_logic_vector(31 downto 0)
        );

    overload_control      <= startup_delay or axis_prog_full;
    axis_if_s.tready        <= '1';
    s_axis_tvalid_int     <= axis_if_m.tvalid and input_read_en;
    rx_fifo_skipped_frame <= std_logic_vector(rx_fifo_skip_cnt);

    -- Start up control necessary for Creonic. (header encoder)
    pr_packet_timeout : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                delay_cnt     <= 0;
                startup_delay <= '1';
            else
                if delay_cnt = G_STARTUP_DELAY then -- aproximately 10.4 us of delay needed. @200Mhz -> 300 clocks is good enough.
                    delay_cnt     <= G_STARTUP_DELAY;
                    startup_delay <= '0';
                else
                    delay_cnt     <= delay_cnt + 1;
                    startup_delay <= '1';
                end if;
            end if;
        end if;
    end process;

    -- Manage the input buffer, discard the next frame if buffer almost full
    pr_input_buf : process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                input_read_en    <= '1';
                rx_fifo_skip_cnt <= (others => '0');
            --r_total_prebuffer_packet_drop   <= (others => '0');
            else
                -- Drop packet if first FIFO doesn't have enough space
                if axis_if_m.tvalid = '1' and s_axis_tready_int = '1' and axis_if_m.tlast = '1' then
                    if input_read_en = '1' then
                        if fifo_input_almost_full = '1' then
                            input_read_en <= '0'; -- drop packet
                            --r_total_prebuffer_packet_drop   <= r_total_prebuffer_packet_drop + 1;
                        end if;
                    else
                        rx_fifo_skip_cnt <= rx_fifo_skip_cnt + 1;
                        if fifo_input_almost_full = '0' then
                            input_read_en <= '1';
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process pr_input_buf;

    -- Manage the pause frame for Ethernet Tx
    pr_pause_frame : process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                pause_req_wait_cnt       <= (others => '0');
                pause_ctl_state          <= IDLE;
                clear_pause_flag         <= '0';
                activate_pause_req       <= '0';
                pause_requested          <= '0';
                tx_pause_valid           <= '0';
                tx_pause_data            <= (others => '0');
                r_activate_pause_req     <= '0';
                r_input_almost_empty_int <= '0';
            else
                r_cfg_pause_data         <= cfg_pause_data;
                r_input_almost_empty_int <= back_pressure_low; --input_almost_full_int_let_clk;
                tx_pause_valid           <= '0';

                case pause_ctl_state is
                    when IDLE =>
                        pause_ctl_state    <= IDLE;
                        pause_req_wait_cnt <= (others => '0');
                        tx_pause_valid     <= '0';
                        --if axis_wr_data_count > C_THRESH_PAUSE_ACTIVATE then
                        if activate_pause_req = '1' then
                            pause_ctl_state <= SEND_REQ_PAUSE;
                            --activate_pause_req      <= '0';
                            pause_requested <= '1';
                        elsif clear_pause_flag = '1' then
                            pause_ctl_state  <= SEND_REQ_NULL;
                            clear_pause_flag <= '0';
                            pause_requested  <= '0';
                        end if;

                    when SEND_REQ_PAUSE =>
                        tx_pause_valid  <= '1';
                        tx_pause_data   <= r_cfg_pause_data;
                        pause_ctl_state <= WAIT_REQ;

                    when SEND_REQ_NULL =>
                        tx_pause_data   <= (others => '0');
                        tx_pause_valid  <= '1';
                        pause_ctl_state <= WAIT_REQ;

                    when WAIT_REQ =>
                        tx_pause_valid     <= '0';
                        pause_req_wait_cnt <= pause_req_wait_cnt + 1;
                        if pause_req_wait_cnt = C_WAIT_CYCLES_AFTER_PAUSE_REQ then
                            pause_ctl_state    <= IDLE;
                            pause_req_wait_cnt <= (others => '0');
                        end if;
                    when others =>
                        pause_ctl_state <= IDLE;
                end case;

                if back_pressure_high = '1' then --axis_rd_data_count > C_THRESH_PAUSE_ACTIVATE then
                    activate_pause_req <= '1';
                else
                    activate_pause_req <= '0';
                end if;
                if back_pressure_low = '1' and r_input_almost_empty_int = '0' and pause_requested = '1' and clear_pause_flag = '0' then
                    --Disables just for testing:
                    clear_pause_flag <= '1';
                end if;
                --r_activate_pause_req <= activate_pause_req;

            end if;
        end if;
    end process pr_pause_frame;

    inst_bitrate_calculator : entity monitoring_lib.bitrate_calculator
        Port map(
            clk              => clk,
            rst              => rst,
            -- Input data from encoder
            ----------------------
            s_axis_tdata     => (others => '0'),
            s_axis_tvalid    => r_s_axis_m.tvalid,
            s_axis_tlast     => r_s_axis_m.tlast,
            s_axis_tready    => r_s_axis_s.tready,
            bitrate          => bitrate_in,
            total_packet_cnt => packet_cnt_in
        );

end Behavioral;
