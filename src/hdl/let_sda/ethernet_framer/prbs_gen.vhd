--!
--! Copyright (C) 2020 Creonic GmbH
--!
--! @file
--! @brief  This entity contains the scrambling for the FSO frame.
--! @author Nhan Nguyen
--! @date   2020/02/28
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--library frame_encoding;
--use frame_encoding.pkg_support.all;
--use frame_encoding.pkg_components.all;

entity prbs_gen is
	port(
		clk : in std_logic;
		rst : in std_logic;
        
        seed    : in std_logic_vector(15 downto 0);

		--input_data  : in  std_logic_vector(8 - 1 downto 0);
		scramble_rst : in  std_logic;
		input_valid  : in  std_logic;
		input_last   : in  std_logic;
		input_ready  : out std_logic;

		output_data  : out std_logic_vector(8 - 1 downto 0);
		output_valid : out std_logic;
		output_last  : out std_logic;
		output_ready : in  std_logic
	);
end entity prbs_gen;

architecture rtl of prbs_gen is

	----------------------------
	-- Code parameter signals --
	----------------------------

	constant NUM_SHIFTER_BITS : natural := 25;
	--constant INIT_STATE       : std_logic_vector(NUM_SHIFTER_BITS - 1 downto 0) := "1000000000000000000011111";  --"0000000000000000000000111"; --"0000000000000000000000111";

	signal input_cnt         : unsigned(1 downto 0);
	signal scramble_en       : std_logic;
	--signal scramble_rst      : std_logic;
	signal shift_register    : std_logic_vector(NUM_SHIFTER_BITS - 1 downto 0);
	signal shift_reg_reverse : std_logic_vector(24 - 1 downto 0);
	signal buffer_data       : std_logic_vector(8 - 1 downto 0);
	signal buffer_valid      : std_logic;
	signal buffer_last       : std_logic;
	signal buffer_ready      : std_logic;

begin

	--input_ready     <= buffer_ready;
	input_ready     <= '1';
	--scramble_en     <= input_valid and buffer_ready;
	--scramble_rst    <= '1' when (scramble_en = '1' and input_last = '1') else '0';

	buffer_valid   <= input_valid;
	buffer_last    <= input_last;

	-- The scrambling operation happens here
	pr_scrambling : process(shift_register, input_cnt, shift_reg_reverse) is
	begin
		--for i in 0 to 23 loop
		--	shift_reg_reverse(i) <= shift_register(23 - i);
		--end loop;
		shift_reg_reverse <= shift_register(23 downto 0);

		case to_integer(input_cnt) is
		--when      0 => buffer_data <= shift_reg_reverse( 7 downto  0) xor input_data;
		--when      1 => buffer_data <= shift_reg_reverse(15 downto  8) xor input_data;
		--when others => buffer_data <= shift_reg_reverse(23 downto 16) xor input_data;		
		when      0 => buffer_data <= shift_reg_reverse( 23 downto 16);-- xor input_data;
		when      1 => buffer_data <= shift_reg_reverse(15 downto  8);-- xor input_data;
		when others => buffer_data <= shift_reg_reverse(7 downto  0);-- xor input_data;		
		end case;

	end process pr_scrambling;

	-- compute the parallel PRBS as in the standard
	pr_pbrs: process(clk) is
		variable v_shift_register    : std_logic_vector(NUM_SHIFTER_BITS - 1 downto 0);
	begin
	if rising_edge(clk) then
		if rst = '1' then
			input_cnt        <= (others => '0');
			shift_register   <= (others => '0');
		else
			-- Register output for cutting the combinational logic path
			output_valid   <= buffer_valid;
			output_last    <= buffer_last;
			output_data	   <= buffer_data;

--			if input_valid = '1' and buffer_ready = '1' then
--				input_cnt <= input_cnt + 1;
--			end if;

			if input_valid = '1' then --and buffer_ready = '1' then
				input_cnt <= input_cnt + 1;
				if input_cnt = 2 then
					input_cnt <= (others => '0');
					v_shift_register := shift_register;
					shift_register(0) <= v_shift_register(1) xor v_shift_register(20) xor v_shift_register(23);
					shift_register(1) <= v_shift_register(2) xor v_shift_register(21) xor v_shift_register(24);
					for i in 2 to 23 loop
						shift_register(i) <= v_shift_register(i - 2) xor v_shift_register(i + 1);
					end loop;
					shift_register(24) <=  v_shift_register(0);
				end if;
			end if;

			-- Parallel LFSR as described in the CCSDS standard
			if scramble_rst = '1' then
				input_cnt      <= (others => '0');
				shift_register(24 downto 16)  <= (others => '0');
				shift_register(15 downto 0)  <= seed;

			end if;
		end if;
	end if;
	end process pr_pbrs;

	-- Add axis buffer for cutting the combinational logic path
--	inst_axis_buf : axi4s_buffer
--	generic map(
--		DATA_WIDTH => 8
--	)
--	port map(
--
--		clk            => clk,
--		rst            => rst,
--
--		-- Input data handling
--		----------------------
--
--		input          => buffer_data,
--		input_valid    => buffer_valid,
--		input_last     => buffer_last,
--		input_ready    => buffer_ready,
--
--
--		-- Output data handling
--		-----------------------
--		output         => output_data,
--		output_valid   => output_valid,
--		output_last    => output_last,
--		output_ready   => output_ready
--	);

end architecture rtl;
