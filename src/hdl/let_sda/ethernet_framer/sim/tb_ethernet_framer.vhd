----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/10/2021 08:19:11 AM
-- Design Name: 
-- Module Name: tb_ethernet_framer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;
use std.textio.all;

library ethernet_framer_lib;
use ethernet_framer_lib.txt_util.all;

library sda_lib;
use sda_lib.pkg_sda.all;

library sda_test_lib;
use sda_test_lib.payload_test_pkg.all;
use sda_test_lib.packet_test_pkg.all;
use sda_test_lib.eth_packet_test_pkg.all;

entity tb_ethernet_framer is
--  Port ( );
end tb_ethernet_framer;

architecture Behavioral of tb_ethernet_framer is

constant CLK_PERIOD  : time    := 10 ns;    
constant NUM_FRAME   : natural := 1;
signal FRAME_SIZE  : natural := 64;

signal clk_let            : std_logic := '0';
signal clk_eth            : std_logic := '0';
signal rst_let            : std_logic := '1';
signal rst_eth            : std_logic := '1';

signal tx_pause_req : std_logic;
signal rx_fifo_skipped_frame : std_logic_vector(32 - 1 downto 0);
signal tx_fn : std_logic_vector(16 - 1 downto 0);
signal frame_type : std_logic_vector(2 - 1 downto 0);

signal tx_axis_tdata     : std_logic_vector(8 - 1 downto 0);
signal tx_axis_tvalid    : std_logic;
signal tx_axis_tlast     : std_logic;
signal tx_axis_tready    : std_logic;

signal m_axis_tdata  : std_logic_vector(8 - 1 downto 0);
signal m_axis_tvalid : std_logic;
signal m_axis_tlast  : std_logic;
signal m_axis_tready : std_logic := '0';

signal test_cnt : unsigned(15 downto 0) := (others => '0');

file l_file : text; -- text is keyword

begin

    DUT : entity ethernet_framer_lib.ethernet_framer
    generic map(
        MINIMUN_FREE_BYTES_TO_SPLIT => 64
    )
	port map(
        clk            => clk_let,
        rst            => rst_let,

        -- Input data 
        ----------------------
        s_axis_tdata     => tx_axis_tdata, --
        s_axis_tvalid    => tx_axis_tvalid,
        s_axis_tlast     => tx_axis_tlast,
        s_axis_tready    => tx_axis_tready,
        s_axis_tuser     => '0',


        -- Output data 
        ----------------------
        m_axis_tdata  => m_axis_tdata, --: out  std_logic_vector(8 - 1 downto 0);
        m_axis_tvalid => m_axis_tvalid, --: out std_logic;
        m_axis_tlast  => m_axis_tlast, --: out std_logic;
        m_axis_tready => '1', --m_axis_tready, --: in std_logic;
        m_tx_fn       => tx_fn, --: out std_logic_vector(16 - 1 downto 0);
        m_frame_type  => frame_type, --: out std_logic_vector(1 downto 0);
        
        -- Control (subject to change in the future)
        ---------------------- 
        overload      => '0', --: in std_logic;
--        tx_ts         : in std_logic_vector(40-1 downto 0);
--        tx_ts_valid   : in std_logic;
--        tx_ts_rd_en   : out std_logic;

        -- To TOF Calculator
        ---------------------- 
--        tx_timing_data          : out tx_timing_data_t;
--        tx_timing_data_valid    : out std_logic;
        
        -- Interrogate
        send_ranging_req            => '0', --: in std_logic;
        t1_tx_fn                    => open, --: out std_logic_vector(15 downto 0);
        t1_tx_fn_valid              => open, --: out std_logic;
--
--        -- Respond
        send_t3                     => '0', --: in std_logic;
        t3_tx_fn                    => open, --: out std_logic_vector(15 downto 0);
        t3_tx_fn_valid              => open, --: out std_logic;
        send_mgmt_response          => C_MGMT_RESPONSE_INIT, --: in mgmt_payload_t;
        send_mgmt_response_valid    => '0', --: in std_logic;
        send_mgmt_response_ack      => open, --: out std_logic;


        -- Statistics
        ----------------------
	clear_stat                      => '0', --: in  std_logic;
        total_packet_counter            => open, --: out std_logic_vector(31 downto 0);
	total_packet_splitted_counter   => open, --: out std_logic_vector(31 downto 0);
	total_data_frame_counter        => open, --: out std_logic_vector(31 downto 0);
	actual_data_frame_counter       => open, --: out std_logic_vector(31 downto 0);
	total_idle_frame_counter        => open, --: out std_logic_vector(31 downto 0);
	total_packet_drop		=> open, --		: out std_logic_vector(31 downto 0);
	frame_length_error		=> open --		: out std_logic_vector(31 downto 0)

    );
	-------------------------------------------
	-- clock generation
	-------------------------------------------
	
	clk_let <= not clk_let after CLK_PERIOD/2;
	clk_eth <= clk_let; --not clk_eth after CLK_PERIOD/2;
	
	
	pr_ready_control: process
	begin    
	    m_axis_tready <= '0';
	    wait for 600 us;
	    wait until rising_edge(clk_let);
        m_axis_tready <= '1';
        wait;
        wait for 90 us;
        wait until rising_edge(clk_let);
        m_axis_tready <= '0';
--        wait for 500 us;
--        wait until rising_edge(clk_let);
--        m_axis_tready <= '1';
--        wait;
	end process;

--    m_axis_tready <= '0';


	-------------------------------------------
	-- stimuli generation
	-------------------------------------------
	pr_stimuli: process
		variable v_block_cnt        : integer := 0;
		variable v_insert_invalid   : boolean := false;
		variable v_byte_cnt         : natural;
		variable v_frame_cnt        : natural;
		variable v_frame_cnt_word   : std_logic_vector(15 downto 0);
	begin
        --rst              <= '0';
        --wait for 5 * CLK_PERIOD;
		-- reset the system
		rst_eth              <= '1';
		rst_let              <= '1';

		-- configuration
		tx_axis_tvalid    <= '0';
		tx_axis_tlast      <= '0';
		tx_axis_tdata     <= (others => '0');
		

		wait for 16 * CLK_PERIOD;

		-- release the reset
		wait until rising_edge(clk_eth);
		rst_eth         <= '0';
		rst_let         <= '0';
		wait for 1 * CLK_PERIOD;


		wait until rising_edge(clk_eth);
		--wait for 0.1 * CLK_PERIOD;

		wait until rising_edge(clk_eth);

		-- send data of one block
        
        for frame in 1 to NUM_FRAME  loop --+ 50
            for byte_id in 0 to FRAME_SIZE - 1 loop
                tx_axis_tvalid    <= '1';
                v_frame_cnt_word := std_logic_vector(to_unsigned(frame, 16));
                tx_axis_tdata <= std_logic_vector(to_unsigned((byte_id), 8));
                
                
                
                if byte_id = 1 then
                    --tx_axis_tdata <= v_frame_cnt_word(15 downto 8);
                    tx_axis_tdata <= X"FE";
                end if;
                if byte_id = 0 then
                    --tx_axis_tdata <= v_frame_cnt_word(7 downto 0);
                    tx_axis_tdata <= X"CA";
                end if;

                if byte_id = FRAME_SIZE - 1 then
                    tx_axis_tlast <= '1';
                else
                    tx_axis_tlast <= '0';
                end if;

                wait until tx_axis_tready = '1' and falling_edge(clk_eth);
                --wait for 0.2 * CLK_PERIOD;




                wait until rising_edge(clk_eth);

            end loop;                
            tx_axis_tvalid <= '0';
            tx_axis_tdata  <= (others => 'U');
            tx_axis_tlast  <= '0';
            
--            if frame < 30 then
--                if FRAME_SIZE > 2000 then
--                    FRAME_SIZE <= 2000;
--                else
--                    FRAME_SIZE <= FRAME_SIZE + 100;
--                end if;
--            else
--                if FRAME_SIZE < 100 then
--                    FRAME_SIZE <= 99;
--                else
--                    FRAME_SIZE <= FRAME_SIZE - 100;
--                end if;
--            end if;
            test_cnt <= test_cnt + 1;
            if test_cnt = 45 then
                wait for 500 us;
            end if;
            wait until rising_edge(clk_eth);
           
        end loop;
        --m_axis_tready <= '1';
                           
		wait;
	end process pr_stimuli;


	-------------------------------------------
	-- stimuli generation
	-------------------------------------------
	file_open(l_file, "output_file.txt", write_mode);
	pr_output : process(clk_let) is
		variable v_byte_cnt : natural := 0;
		variable v_frame_id : std_logic_vector(15 downto 0);
		variable v_frame_id_nat : natural;
		variable v_error_cnt : natural := 0;
		variable v_frame_cnt : natural := 0;
		variable v_line_out  : line;
		variable v_seq_num : natural := 0;
		variable v_payload_len : natural := 0;
		variable v_packet_len : natural := 0;
		variable v_row          : line;
        variable v_data_write : integer;
        constant header:    string := "------------FRAME START------------";
        variable packets_ok : natural := 0;

		
	begin
	

	
	if(rising_edge(clk_let)) then
	
		if rst_let = '1' then 
          v_byte_cnt := 0;
          packets_ok := 0;
--      		write(v_row,header);	
--        	writeline(l_file,v_row);  
            --m_axis_tready <= '1';    
		else
	       --m_axis_tready <= '1';
	      if m_axis_tlast = '1' and m_axis_tvalid = '1' and m_axis_tready = '1' then
	            packets_ok := packets_ok + 1;
--		      	assert false report "Packet Received! " & str(v_byte_cnt+1) & " Bytes - Total Packets OK: " & str(packets_ok) & "." 
--				severity failure;
				write(v_row,hstr(m_axis_tdata));
				writeline(l_file,v_row);
				writeline(l_file,v_row);
				write(v_row,header);	
                writeline(l_file,v_row);   
				v_byte_cnt := 0;
				
		  elsif m_axis_tvalid = '1' and m_axis_tready = '1' then
		      write(v_row,hstr(m_axis_tdata));
		      --write(v_row,header);
		      --writeline(l_file,v_row); 
		      v_byte_cnt := v_byte_cnt + 1;
		      if (v_byte_cnt mod 4) = 0 then
		          writeline(l_file,v_row);       
--		      elsif v_byte_cnt = 15 then
--		          writeline(l_file,v_row);              
		      end if;
		        
		  end if;
		end if;
    end if;
	end process;
	
	
	
end Behavioral;
