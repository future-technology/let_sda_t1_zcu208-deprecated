----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 21/09/2021 11:40:33 AM
-- Design Name: 
-- Module Name: fcch_receiver.vhd - Behavioral
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2018.3
-- Description: Wrapper for the FCCH comms through SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use ieee.std_logic_misc.all;
library sda_lib;
use sda_lib.pkg_sda.all;
library fcch_lib;
use fcch_lib.pkg_fcch.all;


entity fcch_receiver is
    Port ( 
        clk                         : in  std_logic;
        rst                         : in  std_logic;

        -- From deFramer
        rx_fcch_data                : in std_logic_vector(20-1 downto 0);
        rx_fcch_valid               : in std_logic;

        -- To Register Map
        fifo_fcch_data                : out std_logic_vector(20-1 downto 0);
        fifo_fcch_valid               : out std_logic;
        fifo_fcch_rd_en               : in std_logic

    );
end fcch_receiver;

architecture Behavioral of fcch_receiver is


begin


inst_fcch_fifo : fcch_fifo
PORT MAP (
    clk => clk,
    srst => rst,
    din => rx_fcch_data,
    wr_en => rx_fcch_valid,
    rd_en => fifo_fcch_rd_en,
    dout => fifo_fcch_data,
    full => open,
    empty => open,
    valid => fifo_fcch_valid,
    wr_rst_busy => open,
    rd_rst_busy => open
);


end Behavioral;





















