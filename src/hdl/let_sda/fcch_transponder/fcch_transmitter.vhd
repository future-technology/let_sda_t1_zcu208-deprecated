----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 21/09/2021 11:40:33 AM
-- Design Name: 
-- Module Name: fcch_transmiter.vhd - Behavioral
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2018.3
-- Description: Transmiter for the FCCH comms through SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use ieee.std_logic_misc.all;
library sda_lib;
use sda_lib.pkg_sda.all;
library fcch_lib;
use fcch_lib.pkg_fcch.all;


entity fcch_transmiter is
    Port ( 
        clk                         : in  std_logic;
        rst                         : in  std_logic;

        pps_clk                     : in std_logic;
        preamble_synced             : in std_logic;
        crc_errors_per_second       : in std_logic_vector(31 downto 0);
        
        -- From Register Map
        LAPC_RPT_RSSI_FAST          : in std_logic_vector(14-1 downto 0);
        LAPC_RPT_RSSI_MEAN          : in std_logic_vector(8-1 downto 0);
        LAPC_RPT_RSSI_SDEV          : in std_logic_vector(8-1 downto 0);
        OISL_PMIN                   : in std_logic_vector(8-1 downto 0);
        OISL_PMAX                   : in std_logic_vector(8-1 downto 0);

        -- To Framer
        tx_fcch_opcode              : out std_logic_vector(4-1 downto 0);
		tx_fcch_pl                  : out std_logic_vector(16-1 downto 0);
        tx_fcch_valid               : out std_logic;
        tx_fcch_rd_en               : in std_logic

    );
end fcch_transmiter;

architecture Behavioral of fcch_transmiter is

constant C_100HZ : unsigned(32-1 downto 0) := x"001E8480";
--constant C_100HZ : unsigned(32-1 downto 0) := x"0000A7DA";  -- just for testing
constant C_1HZ : unsigned(32-1 downto 0) := x"00989680"; -- For 1Hz at pps clock (10Mhz)


signal counter_100Mhz : unsigned(32-1 downto 0);
signal counter_1hz : unsigned(32-1 downto 0);

signal pps_clk_vector           : std_logic_vector(3 downto 0); 
signal r_pps_clk_vector         : std_logic;
signal r_preamble_synced        : std_logic;
signal preamble_synced_trailing : std_logic;
signal preamble_synced_down : std_logic;

signal sync_lost_counter : unsigned(14-1 downto 0);
signal r_sync_lost_counter : unsigned(14-1 downto 0);

signal fcch_code        : std_logic_vector(4-1 downto 0);
signal fcch_payload     : std_logic_vector(16-1 downto 0);

signal fifo_tx_din      : std_logic_vector(20-1 downto 0); 
signal fifo_tx_dout     : std_logic_vector(20-1 downto 0); 
signal fifo_tx_rd_en    : std_logic;
signal fifo_tx_wr_en    : std_logic;
signal fifo_tx_valid    : std_logic;
signal fcch_fifo_full    : std_logic;

signal flag_link_acq    : std_logic;
signal clear_link_acq   : std_logic;

signal flag_1hz         : std_logic;
signal clear_1hz        : std_logic;

signal flag_100hz         : std_logic;
signal clear_100hz        : std_logic;

type t_tx_fsm is (IDLE, WR_LINK_ACQ, WR_1HZ_1, WR_1HZ_2, WR_1HZ_3, WR_100HZ, WAIT_1);
signal tx_fsm : t_tx_fsm;


begin
    fifo_tx_din <= fcch_code & fcch_payload;
    tx_fcch_opcode  <= fifo_tx_dout(20-1 downto 16);
    tx_fcch_pl      <= fifo_tx_dout(16-1 downto 0);

	-- Generate the triggers for all frequencies to send FCCH messages.
	fcch_tx : process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                flag_1hz <= '0';
                flag_link_acq <= '0';
                flag_100hz <= '0';
                preamble_synced_down <= '0';
                counter_100Mhz      <= (others=>'0');
                counter_1hz         <= (others=>'0');
                sync_lost_counter   <= (others=>'0');
                r_sync_lost_counter <= (others=>'0');
            else
                pps_clk_vector(0) <= pps_clk;
                pps_clk_vector(1) <= pps_clk_vector(0);
                pps_clk_vector(2) <= pps_clk_vector(1);
                pps_clk_vector(3) <= pps_clk_vector(2);
                r_pps_clk_vector  <= pps_clk_vector(3);
                r_preamble_synced <= preamble_synced;

                -- Sync lost
                if preamble_synced = '0' and r_preamble_synced = '1' then
                    sync_lost_counter <= sync_lost_counter + 1;
                end if;

                -- Link Acquisition
                if preamble_synced = '1' and r_preamble_synced = '0' then
                    flag_link_acq <= '1';
                end if;

                if preamble_synced = '0' then 
                    preamble_synced_down <= '0';
                end if;

                if r_pps_clk_vector = '0' and pps_clk_vector(3) = '1' then -- 100 ns
                    r_sync_lost_counter <= sync_lost_counter;
                    sync_lost_counter <= (others => '0');

                    if counter_1hz = C_1HZ then
                        counter_1hz <= (others=>'0');
                        flag_1hz <= '1';
                    else  
                        counter_1hz <= counter_1hz + 1;
                    end if;

                end if;

                if counter_100Mhz = C_100HZ then
                    counter_100Mhz <= (others=>'0');
                    flag_100hz <= '1';
                else  
                    counter_100Mhz <= counter_100Mhz + 1;
                end if;

                if clear_link_acq = '1' then
                    flag_link_acq <= '0';
                end if;
                if clear_1hz = '1' then
                    flag_1hz <= '0';
                end if;
                if clear_100hz = '1' then
                    flag_100hz <= '0';
                end if;

            end if;
        end if; 
    end process;

	-- FSM to send FCCH messages
	t_tx_fsm_proc : process(clk) is
        begin
            if rising_edge(clk) then
                if rst = '1' then
                    tx_fsm <= IDLE;
                    clear_link_acq  <= '0';
                    clear_1hz       <= '0';
                    clear_100hz     <= '0';
                    fifo_tx_wr_en   <= '0';
                    preamble_synced_trailing   <= '0';
                    fcch_code       <= (others =>'0');
                    fcch_payload    <= (others =>'0');
                else
                    fifo_tx_wr_en    <= '0';
                    clear_link_acq   <= '0';
                    clear_1hz        <= '0';
                    clear_100hz      <= '0';
                    if preamble_synced = '0' then 
                        preamble_synced_trailing <= '0';
                    end if;
                    
                    case tx_fsm is
                        when IDLE =>
                            if flag_link_acq = '1' and fcch_fifo_full = '0' then
                                tx_fsm          <= WR_LINK_ACQ;
                                clear_link_acq  <= '1';
                            elsif flag_1hz = '1' and fcch_fifo_full = '0' then                                
                                tx_fsm          <= WR_1HZ_1;
                                clear_1hz       <= '1';
                            elsif flag_100hz = '1' and fcch_fifo_full = '0' then
                                tx_fsm          <= WR_100HZ;
                                clear_100hz     <= '1';
                            end if;                           

                        when WR_LINK_ACQ =>
                            tx_fsm        <= WAIT_1;
                            fcch_code       <= C_FCCH_OISL_SENSITIVITY;
                            fcch_payload    <= OISL_PMAX & OISL_PMIN;
                            --fcch_payload    <= x"ACAC";
                            fifo_tx_wr_en   <= '1';

                        when WR_1HZ_1 =>
                            tx_fsm <= WR_1HZ_2;
                            fcch_code       <= C_FCCH_LAPC_BLER_REPORT;
                            fcch_payload    <= crc_errors_per_second(16-1 downto 0);
                            --fcch_payload    <= x"01AB";
                            fifo_tx_wr_en   <= '1';
                            
                        when WR_1HZ_2 =>
                            tx_fsm <= WR_1HZ_3;
                            fcch_code       <= C_FCCH_LAPC_RSSI_SLOW;
                            fcch_payload    <= LAPC_RPT_RSSI_SDEV & LAPC_RPT_RSSI_MEAN;
                            --fcch_payload    <= x"02EE";
                            fifo_tx_wr_en   <= '1';

                        when WR_1HZ_3 =>
                            tx_fsm <= WAIT_1;
                            fcch_code       <= C_FCCH_LAPC_SYNC_REPORT;
                            fcch_payload    <= '0' & preamble_synced & std_logic_vector(r_sync_lost_counter);
                            --fcch_payload    <= x"03CA";
                            fifo_tx_wr_en   <= '1';

                        when WR_100HZ =>
                            tx_fsm <= WAIT_1;
                            fcch_code       <= C_FCCH_LAPC_RSSI_FAST;
                            fcch_payload    <= '0' & preamble_synced_trailing & LAPC_RPT_RSSI_FAST;
                            --fcch_payload    <= x"DEDE";
                            fifo_tx_wr_en   <= '1';
                            

                        WHEN WAIT_1 =>
                            tx_fsm <= IDLE;
                            preamble_synced_trailing <= preamble_synced;
                        
                        when others => 
                            tx_fsm <= IDLE;
                    end case;
                end if;
            end if;
        end process;


    inst_fcch_fifo : fcch_fifo
    PORT MAP (
        clk => clk,
        srst => rst,
        din => fifo_tx_din,
        wr_en => fifo_tx_wr_en,
        rd_en => tx_fcch_rd_en,
        dout => fifo_tx_dout,
        full => fcch_fifo_full,
        empty => open,
        valid => tx_fcch_valid,
        wr_rst_busy => open,
        rd_rst_busy => open
    );

end Behavioral;





















