----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 21/09/2021 11:40:33 AM
-- Design Name: 
-- Module Name: fcch_transponder.vhd - Behavioral
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2018.3
-- Description: Wrapper for the FCCH comms through SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use ieee.std_logic_misc.all;
library sda_lib;
use sda_lib.pkg_sda.all;
library fcch_lib;
use fcch_lib.pkg_fcch.all;

entity fcch_transponder is
    Generic (
        PPS_SIMULATED               : boolean := false 
    );
    Port ( 
        clk                         : in  std_logic;
        rst                         : in  std_logic;

        pps_clk                     : in std_logic;
        preamble_synced             : in std_logic;
        crc_errors_per_second       : in std_logic_vector(31 downto 0);
        
        -- From Register Map
        LAPC_RPT_RSSI_FAST          : in std_logic_vector(14-1 downto 0);
        LAPC_RPT_RSSI_MEAN          : in std_logic_vector(8-1 downto 0);
        LAPC_RPT_RSSI_SDEV          : in std_logic_vector(8-1 downto 0);
        OISL_PMIN                   : in std_logic_vector(8-1 downto 0);
        OISL_PMAX                   : in std_logic_vector(8-1 downto 0);

        -- To Register Map
        fifo_fcch_data                : out std_logic_vector(20-1 downto 0);
        fifo_fcch_valid               : out std_logic;
        fifo_fcch_rd_en               : in std_logic;

        -- To Framer
        tx_fcch_opcode              : out std_logic_vector(4-1 downto 0);
		tx_fcch_pl                  : out std_logic_vector(16-1 downto 0);
        tx_fcch_valid               : out std_logic;
        tx_fcch_rd_en               : in std_logic;

        -- From Deframer
        rx_fcch_data                : in std_logic_vector(20-1 downto 0);
        rx_fcch_valid               : in std_logic


    );
end fcch_transponder;

architecture Behavioral of fcch_transponder is

    constant C_PPS_SIM_CNT  : unsigned(32-1 downto 0) := x"00000009"; -- For PPS 10MHz
    signal counter_200Mhz   : unsigned(32-1 downto 0);
    signal pps_clk_internal : std_logic;
    signal r_fifo_fcch_rd_en : std_logic;
    signal fifo_fcch_rd_en_sync : std_logic;

begin

inst_fcch_transmiter : fcch_transmiter
    Port map( 
        clk                         => clk, -- : in  std_logic;
        rst                         => rst, --: in  std_logic;

        pps_clk                     => pps_clk_internal, --: in std_logic;
        preamble_synced             => preamble_synced, --: in std_logic;
        crc_errors_per_second       => crc_errors_per_second, --crc_errors_per_second, --: in std_logic_vector(31 downto 0);
        
        -- From Register Map
        LAPC_RPT_RSSI_FAST          => LAPC_RPT_RSSI_FAST, --: in std_logic_vector(14-1 downto 0);
        LAPC_RPT_RSSI_MEAN          => LAPC_RPT_RSSI_MEAN, --: in std_logic_vector(8-1 downto 0);
        LAPC_RPT_RSSI_SDEV          => LAPC_RPT_RSSI_SDEV, --: in std_logic_vector(8-1 downto 0);
        OISL_PMIN                   => OISL_PMIN, --: in std_logic_vector(8-1 downto 0);
        OISL_PMAX                   => OISL_PMAX, --: in std_logic_vector(8-1 downto 0);

        -- To Framer
        tx_fcch_opcode              => tx_fcch_opcode, --: out std_logic_vector(4-1 downto 0);
        tx_fcch_pl                  => tx_fcch_pl, --: out std_logic_vector(16-1 downto 0);
        tx_fcch_valid               => tx_fcch_valid, --: out std_logic;
        tx_fcch_rd_en               => tx_fcch_rd_en --: in std_logic

    );

inst_fcch_receiver : fcch_receiver
    Port map( 
        clk                         => clk,                 -- : in  std_logic;
        rst                         => rst,                 -- : in  std_logic;

        -- From deFramer
        rx_fcch_data                => rx_fcch_data,        -- : in std_logic_vector(20-1 downto 0);
        rx_fcch_valid               => rx_fcch_valid,       -- : in std_logic;

        -- To Register Map
        fifo_fcch_data                => fifo_fcch_data,    -- : in std_logic_vector(20-1 downto 0);
        fifo_fcch_valid               => fifo_fcch_valid,   -- : in std_logic;
        fifo_fcch_rd_en               => fifo_fcch_rd_en_sync    -- : in std_logic;

    );

    process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                r_fifo_fcch_rd_en <= '0';
                fifo_fcch_rd_en_sync <= '0';
            else             
                r_fifo_fcch_rd_en <= fifo_fcch_rd_en;       
                if fifo_fcch_rd_en = '1' and r_fifo_fcch_rd_en = '0' then
                    fifo_fcch_rd_en_sync <= '1';
                else 
                    fifo_fcch_rd_en_sync <= '0';
                end if;
            end if;
        end if;  
    end process;

    -- PPS internal simulation when actual PPS signal is not available  
    PPS_GENERATION: if (PPS_SIMULATED) generate
        process(clk) is
        begin
            if rising_edge(clk) then
                if rst = '1' then
                    pps_clk_internal <= '0';
                    counter_200Mhz   <= (others => '0');
                else                    
                    if counter_200Mhz = C_PPS_SIM_CNT then
                        pps_clk_internal <= not pps_clk_internal;
                        counter_200Mhz   <= (others => '0');
                    else 
                        counter_200Mhz   <= counter_200Mhz + 1;
                    end if;
                end if;
            end if;
        end process;
    -- PPS external source
    else generate
        pps_clk_internal <= pps_clk;
    end generate PPS_GENERATION;


end Behavioral;





















