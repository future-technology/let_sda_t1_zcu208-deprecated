library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library sda_lib;
use sda_lib.pkg_sda.all;


package pkg_fcch is

    component fcch_transponder 
        Generic (
            PPS_SIMULATED               : boolean := false
        );
        Port ( 
            clk                         : in  std_logic;
            rst                         : in  std_logic;
    
            pps_clk                     : in std_logic;
            preamble_synced             : in std_logic;
            crc_errors_per_second       : in std_logic_vector(31 downto 0);
            
            -- From Register Map
            LAPC_RPT_RSSI_FAST          : in std_logic_vector(14-1 downto 0);
            LAPC_RPT_RSSI_MEAN          : in std_logic_vector(8-1 downto 0);
            LAPC_RPT_RSSI_SDEV          : in std_logic_vector(8-1 downto 0);
            OISL_PMIN                   : in std_logic_vector(8-1 downto 0);
            OISL_PMAX                   : in std_logic_vector(8-1 downto 0);
    
            -- To Register Map
            fifo_fcch_data                : out std_logic_vector(20-1 downto 0);
            fifo_fcch_valid               : out std_logic;
            fifo_fcch_rd_en               : in std_logic;
    
            -- To Framer
            tx_fcch_opcode              : out std_logic_vector(4-1 downto 0);
            tx_fcch_pl                  : out std_logic_vector(16-1 downto 0);
            tx_fcch_valid               : out std_logic;
            tx_fcch_rd_en               : in std_logic;
    
            -- From DeFramer
            rx_fcch_data                : in std_logic_vector(20-1 downto 0);
            rx_fcch_valid               : in std_logic
    
    
        );
    end component;


COMPONENT fcch_transmiter
    Port ( 
        clk                         : in  std_logic;
        rst                         : in  std_logic;

        pps_clk                     : in std_logic;
        preamble_synced             : in std_logic;
        crc_errors_per_second       : in std_logic_vector(31 downto 0);
        
        -- From Register Map
        LAPC_RPT_RSSI_FAST          : in std_logic_vector(14-1 downto 0);
        LAPC_RPT_RSSI_MEAN          : in std_logic_vector(8-1 downto 0);
        LAPC_RPT_RSSI_SDEV          : in std_logic_vector(8-1 downto 0);
        OISL_PMIN                   : in std_logic_vector(8-1 downto 0);
        OISL_PMAX                   : in std_logic_vector(8-1 downto 0);

        -- To Framer
        tx_fcch_opcode              : out std_logic_vector(4-1 downto 0);
        tx_fcch_pl                  : out std_logic_vector(16-1 downto 0);
        tx_fcch_valid               : out std_logic;
        tx_fcch_rd_en               : in std_logic

    );
END COMPONENT;


COMPONENT fcch_receiver
    Port ( 
        clk                         : in  std_logic;
        rst                         : in  std_logic;

        -- From deFramer
        rx_fcch_data                : in std_logic_vector(20-1 downto 0);
        rx_fcch_valid               : in std_logic;

        -- To Register Map
        fifo_fcch_data                : out std_logic_vector(20-1 downto 0);
        fifo_fcch_valid               : out std_logic;
        fifo_fcch_rd_en               : in std_logic
    );
END COMPONENT;
    

COMPONENT fcch_fifo
PORT (
    clk : IN STD_LOGIC;
    srst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    valid : OUT STD_LOGIC;
    wr_rst_busy : OUT STD_LOGIC;
    rd_rst_busy : OUT STD_LOGIC
);
END COMPONENT;

end package pkg_fcch;
