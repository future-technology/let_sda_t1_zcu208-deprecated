----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 08/02/2021 10:10:45 AM
-- Design Name: 
-- Module Name: tb_fcch_transponder - Behavioral
-- Project Name: Condor Mark II
-- Target Devices: 
-- Tool Versions: 
-- Description: Testbench for the SDA FCCH module
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

library let_sda_lib;
library sda_lib;
use sda_lib.pkg_sda.all;
library fcch_lib;
use fcch_lib.pkg_fcch.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_fcch_transponder is
--  Port ( );
end tb_fcch_transponder;

architecture Behavioral of tb_fcch_transponder is

 
signal clk                         :  std_logic := '0';
signal rst                         :  std_logic := '1';
signal pps_clk                     : std_logic := '0';
signal preamble_synced             : std_logic := '0';
signal crc_errors_per_second       : std_logic_vector(31 downto 0) := x"0000AAAA";
signal LAPC_RPT_RSSI_FAST          : std_logic_vector(14-1 downto 0) := "00111110000000";
signal LAPC_RPT_RSSI_MEAN          : std_logic_vector(8-1 downto 0) := x"23";
signal LAPC_RPT_RSSI_SDEV          : std_logic_vector(8-1 downto 0) := x"56";
signal OISL_PMIN                   : std_logic_vector(8-1 downto 0) := x"78";
signal OISL_PMAX                   : std_logic_vector(8-1 downto 0) := x"99";
signal fifo_fcch_data              : std_logic_vector(20-1 downto 0);
signal fifo_fcch_valid             : std_logic;
signal fifo_fcch_rd_en             : std_logic := '0';
signal tx_fcch_opcode              :  std_logic_vector(4-1 downto 0);
signal tx_fcch_pl                  :  std_logic_vector(16-1 downto 0);
signal tx_fcch_valid               :  std_logic;
signal tx_fcch_rd_en               : std_logic := '0';
signal rx_fcch_data              : std_logic_vector(20-1 downto 0);
signal rx_fcch_valid               : std_logic;

begin 

clk         <= not clk after 2.5 ns;
pps_clk     <= not pps_clk after 1 ms;


process
begin
    rst                     <= '1';
    preamble_synced         <= '0';
    rx_fcch_data            <= (others=>'0');
    rx_fcch_valid            <= '0';
    wait for 300 ns;
    rst <= '0';
    wait for 300 ns;
    wait for 21*5 us;
    wait until rising_edge(clk);
    rx_fcch_data            <= "00110000111110000000";
    rx_fcch_valid            <= '1';
    wait until rising_edge(clk);
    rx_fcch_valid            <= '0';
        
    wait for 150 us;
    wait until rising_edge(clk);
    preamble_synced <= '1';
    wait for 150 us;
    wait until rising_edge(clk);
    preamble_synced <= '0';
    wait for 150 us;
    wait until rising_edge(clk);
    preamble_synced <= '1';
    wait for 130 us;
    
    wait for 200 us;
    wait until rising_edge(clk);
    rx_fcch_data            <= "00110000111110000000";
    rx_fcch_valid            <= '1';
    wait until rising_edge(clk);
    rx_fcch_valid            <= '0';
    wait for 200 us;
    wait until rising_edge(clk);
    rx_fcch_data            <= "00110000111110000000";
    rx_fcch_valid            <= '1';
    wait until rising_edge(clk);
    rx_fcch_valid            <= '0';
    wait for 200 us;
    wait until rising_edge(clk);
    rx_fcch_data            <= "00110000111110000000";
    rx_fcch_valid            <= '1';
    wait until rising_edge(clk);
    rx_fcch_valid            <= '0';
    
    wait;

end process;

process
begin
    wait until fifo_fcch_valid = '1';
    wait until rising_edge(clk);
    fifo_fcch_rd_en <= '1';
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    fifo_fcch_rd_en <= '0';

end process;
--fifo_fcch_rd_en <= fifo_fcch_valid;
--process
--begin
--    wait until tx_fcch_valid = '1';
--    wait until rising_edge(clk);
--    tx_fcch_rd_en <= '1';
--    wait until rising_edge(clk);
--    tx_fcch_rd_en <= '0';
--end process;
tx_fcch_rd_en <= tx_fcch_valid;


inst_fcch_transponder : fcch_transponder
    generic map(
        PPS_SIMULATED               => true
    )
    Port map ( 
        clk                         => clk , --: in  std_logic;
        rst                         => rst , --: in  std_logic;

        pps_clk                     => '0', --pps_clk               , --: in std_logic;
        preamble_synced             => preamble_synced       , --: in std_logic;
        crc_errors_per_second       => crc_errors_per_second , --: in std_logic_vector(31 downto 0);
        
        -- From Register Map
        LAPC_RPT_RSSI_FAST          => LAPC_RPT_RSSI_FAST , --: in std_logic_vector(14-1 downto 0);
        LAPC_RPT_RSSI_MEAN          => LAPC_RPT_RSSI_MEAN , --: in std_logic_vector(8-1 downto 0);
        LAPC_RPT_RSSI_SDEV          => LAPC_RPT_RSSI_SDEV , --: in std_logic_vector(8-1 downto 0);
        OISL_PMIN                   => OISL_PMIN          , --: in std_logic_vector(8-1 downto 0);
        OISL_PMAX                   => OISL_PMAX          , --: in std_logic_vector(8-1 downto 0);

        -- To Register Map
        fifo_fcch_data                => fifo_fcch_data  , --: in std_logic_vector(20-1 downto 0);
        fifo_fcch_valid               => fifo_fcch_valid , --: in std_logic;
        fifo_fcch_rd_en               => fifo_fcch_rd_en , --: in std_logic;

        -- To Framer
        tx_fcch_opcode              => tx_fcch_opcode , --: out std_logic_vector(4-1 downto 0);
        tx_fcch_pl                  => tx_fcch_pl     , --: out std_logic_vector(16-1 downto 0);
        tx_fcch_valid               => tx_fcch_valid  , --: out std_logic;
        tx_fcch_rd_en               => tx_fcch_rd_en  , --: in std_logic;

        -- From Framer
        rx_fcch_data                => rx_fcch_data , --: in std_logic_vector(4-1 downto 0);
        rx_fcch_valid               => rx_fcch_valid   --: in std_logic


    );


end Behavioral;




