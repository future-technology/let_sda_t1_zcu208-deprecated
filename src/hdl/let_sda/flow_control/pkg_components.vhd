--!
--! Copyright (c) 2015 Creonic GmbH
--!
--! @file
--! @brief  IP component declarations.
--! @author Nhan Nguyen
--! @date   2020/03/02
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library share_let_1g;
use share_let_1g.pkg_support.all;
use share_let_1g.pkg_param.all;
use share_let_1g.pkg_types.all;

package pkg_components is

	component rx_fifo is
	port(
		clk_mac : in std_logic;
		rst_mac : in std_logic;

		mac_data  : in std_logic_vector(64 - 1 downto 0);
		mac_valid : in std_logic;
		mac_keep  : in std_logic_vector(7 downto 0);
		mac_error : in std_logic;
		mac_ready : out std_logic;
		mac_last  : in  std_logic;

		clk_let : in std_logic;
		rst_let : in std_logic;

		tx_pause_req          : out std_logic;
		rx_fifo_skipped_frame : out std_logic_vector(32 - 1 downto 0);

		mac_out_data  : out std_logic_vector(64 - 1 downto 0);
		mac_out_valid : out std_logic;
		mac_out_keep  : out std_logic_vector(7 downto 0);
		mac_out_ready : in  std_logic;
		mac_out_last  : out std_logic;
		mac_out_error  : out std_logic;
		mac_out_length : out std_logic_vector(14 - 1 downto 0)
	);
	end component rx_fifo;

end package pkg_components;
