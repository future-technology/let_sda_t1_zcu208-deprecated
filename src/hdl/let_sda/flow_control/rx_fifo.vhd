--!
--! Copyright (C) 2020 Creonic GmbH
--!
--! @file
--! @brief  A dual clock fifo, which connects MAC and LET framer.
--! @author Markus Fehrenz/Nhan Nguyen
--! @date   2020/03/02
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library share_let_1g;
use share_let_1g.pkg_components.all;

library flow_control_lib;
use flow_control_lib.pkg_components.all;

entity rx_fifo is
	port(
		clk_mac : in std_logic;
		rst_mac : in std_logic;

		mac_data  : in std_logic_vector(8 - 1 downto 0);
		mac_valid : in std_logic;
		mac_keep  : in std_logic_vector(7 downto 0);
		mac_error : in std_logic;
		mac_ready : out std_logic;
		mac_last  : in  std_logic;

		clk_let : in std_logic;
		rst_let : in std_logic;

		tx_pause_req          : out std_logic;
		rx_fifo_skipped_frame : out std_logic_vector(32 - 1 downto 0);

		mac_out_data  : out std_logic_vector(8 - 1 downto 0);
		mac_out_valid : out std_logic;
		mac_out_keep  : out std_logic_vector(7 downto 0);
		mac_out_ready : in  std_logic;
		mac_out_last   : out std_logic;
		mac_out_error  : out std_logic;
		mac_out_length : out std_logic_vector(14 - 1 downto 0)
	);
end entity rx_fifo;

architecture rtl of rx_fifo is

	signal mac_ready_int : std_logic;
	signal rst_mac_n : std_logic;

	constant BW_INPUT_FIFO       : natural := 8 + 1; -- 8bits data + 1bit user
	signal input_fifo_in_data    : std_logic_vector(BW_INPUT_FIFO - 1 downto 0);
	signal input_fifo_out_valid  : std_logic;
	signal input_fifo_out_ready  : std_logic;
	signal input_fifo_out_last   : std_logic;
	signal input_fifo_out_data   : std_logic_vector(BW_INPUT_FIFO - 1 downto 0);
	signal input_almost_full_int : std_logic;
	signal input_almost_empty_int : std_logic;
	signal input_read_en         : std_logic;
	signal mac_valid_int         : std_logic;
	signal rx_fifo_skip_cnt      : unsigned(31 downto 0);
	signal pause_req_wait_cnt    : unsigned(4 downto 0);
	signal pause_req_trigger     : std_logic;
	signal tx_pause_req_int      : std_logic;
	type t_pause_ctrl_fsm_state is (IDLE, SEND_REQ, WAIT_REQ);
	signal pause_ctl_state : t_pause_ctrl_fsm_state;

	signal input_fifo_0_out_data  : std_logic_vector(BW_INPUT_FIFO - 1 downto 0);
	signal input_fifo_0_out_valid : std_logic;
	signal input_fifo_0_out_last  : std_logic;
	signal input_fifo_0_out_ready : std_logic;
	signal fifo_1_almost_full_int : std_logic;

	signal input_error       : std_logic;
	signal input_error_vec   : std_logic_vector(0 downto 0);
	signal input_error_valid : std_logic;
	signal input_error_ready : std_logic;
	signal read_state        : std_logic;
	signal output_error       : std_logic;
	signal output_error_vec   : std_logic_vector(0 downto 0);
	signal output_error_reg   : std_logic;
	signal output_error_ready : std_logic;
	signal output_error_valid : std_logic;


	-- Input mac sc fifo output.
	signal mac_dc_fifo_in_data  : std_logic_vector(8 - 1 downto 0);
	signal mac_dc_fifo_in_valid : std_logic;
	signal mac_dc_fifo_in_keep  : std_logic_vector(7 downto 0);
	signal mac_dc_fifo_in_ready : std_logic;
	signal mac_dc_fifo_in_user : std_logic;
	signal mac_dc_fifo_in_last  : std_logic;

	-- Dual clock fifo output
	signal mac_out_valid_int : std_logic;
	signal mac_out_last_int  : std_logic;

	-- Frame length calculation
	signal mac_incr        : unsigned(3 downto 0);
	signal mac_incr_valid  : std_logic;
	signal mac_incr_last   : std_logic;
	signal mac_incr_last_d : std_logic;
	signal mac_cnt         : unsigned(14 - 1  downto 0);
	signal mac_cnt_valid   : std_logic;
	signal mac_cnt_ready   : std_logic;

	signal mac_length_fifo_in    : std_logic_vector(15 downto 0);
	signal mac_length_fifo_out   : std_logic_vector(15 downto 0);
	signal mac_length_valid : std_logic;
	signal mac_length_ready : std_logic;

	constant ZERO_32BIT : std_logic_vector(31 downto 0) := (others => '0');

	-- Avoid coptimization for ILA 
	attribute keep : string; 
	attribute keep of input_almost_full_int: signal is "true"; 
	attribute keep of input_almost_empty_int: signal is "true"; 
	attribute keep of pause_req_trigger: signal is "true"; 
	attribute keep of pause_ctl_state: signal is "true"; 
	attribute keep of mac_out_last_int: signal is "true"; 
	attribute keep of mac_out_valid_int: signal is "true"; 
	attribute keep of output_error_valid: signal is "true"; 
	attribute keep of output_error: signal is "true"; 
	attribute keep of input_read_en: signal is "true"; 
	attribute keep of input_fifo_in_data: signal is "true"; 
	attribute keep of input_fifo_out_valid: signal is "true"; 
	attribute keep of input_fifo_out_ready: signal is "true"; 

	attribute keep of input_fifo_out_last: signal is "true"; 
	attribute keep of input_fifo_out_data: signal is "true"; 
	attribute keep of fifo_1_almost_full_int: signal is "true";
	attribute keep of tx_pause_req_int: signal is "true"; 
	

	component dc_fifo_eth_side
	port (
		m_aclk : in std_logic;
		s_aclk : in std_logic;
		s_aresetn : in std_logic;
		s_axis_tvalid : in std_logic;
		s_axis_tready : out std_logic;
		s_axis_tdata : in std_logic_vector(7 downto 0);
		s_axis_tuser : in std_logic;
		s_axis_tlast : in std_logic;
		m_axis_tvalid : out std_logic;
		m_axis_tready : in std_logic;
		m_axis_tdata : out std_logic_vector(7 downto 0);
		m_axis_tuser : out std_logic;
		m_axis_tlast : out std_logic
	);
	end component;

--	component dc_rx_cnt_fifo_0
--	port (
--		m_aclk : in std_logic;
--		s_aclk : in std_logic;
--		s_aresetn : in std_logic;
--		s_axis_tvalid : in std_logic;
--		s_axis_tready : out std_logic;
--		s_axis_tdata : in std_logic_vector(15 downto 0);
--		m_axis_tvalid : out std_logic;
--		m_axis_tready : in std_logic;
--		m_axis_tdata : out std_logic_vector(15 downto 0)
--	);
--	end component;

begin

	rst_mac_n <= not(rst_mac);
	mac_ready <= mac_ready_int;
	mac_valid_int <= mac_valid and input_read_en;

	mac_out_valid <= mac_out_valid_int;
	mac_out_last  <= mac_out_last_int;
	input_fifo_in_data <= mac_error & mac_data;

	tx_pause_req <= tx_pause_req_int;
	tx_pause_req <= '0';
	rx_fifo_skipped_frame <= std_logic_vector(rx_fifo_skip_cnt);

	-- Manage the input buffer, discard the next frame if buffer almost full
	pr_input_buf : process(clk_mac) is
	begin
	if rising_edge(clk_mac) then
		if rst_mac = '1' then
			input_read_en      <= '1';
			rx_fifo_skip_cnt   <= (others => '0');
			pause_req_wait_cnt <= (others => '0');
			pause_req_trigger  <= '0';
			pause_ctl_state    <= IDLE;
			tx_pause_req_int   <= '0';
		else

			-- Drop packet if first FIFO doesn't have enough space
			if mac_valid = '1' and mac_ready_int = '1' and mac_last = '1' then
				if input_read_en = '1' then
					if input_almost_full_int = '1' then
						input_read_en <= '0';
					end if;
				else
					rx_fifo_skip_cnt <= rx_fifo_skip_cnt + 1;
					if input_almost_full_int = '0' then
						input_read_en <= '1';
					end if;
				end if;
			end if;

			-- Process the pause request
			if fifo_1_almost_full_int = '1' or input_almost_empty_int = '0' then
				pause_req_trigger <= '1';
			else
				pause_req_trigger <= '0';
			end if;

			case pause_ctl_state is
			when IDLE =>
				pause_req_wait_cnt <= (others => '0');
				if pause_req_trigger = '1' then
					pause_ctl_state  <= SEND_REQ;
					tx_pause_req_int <= '1';
				end if;

			when SEND_REQ =>
				pause_req_wait_cnt <= pause_req_wait_cnt + 1;

				-- Wait for at least 16 cycles as in the spec, set to 20
				if pause_req_wait_cnt = 19 then
					tx_pause_req_int   <= '0';
					pause_req_wait_cnt <= (others => '0');
					pause_ctl_state    <= WAIT_REQ;
				end if;

			when WAIT_REQ =>
				pause_req_wait_cnt <= pause_req_wait_cnt + 1;
				if pause_req_wait_cnt = 19 then
					pause_ctl_state <= IDLE;
				end if;
			end case;

		end if;
	end if;
	end process pr_input_buf;


	--
	-- This FIFO is connected to the RX of the MAC.
	-- One packet is collected and and forwarded afer complete reception.
	--
	inst_input_fifo_0 : entity share_let_1g.axi4s_fifo
	generic map(
		DISTR_RAM         => false,
		FIFO_DEPTH        => 512,--16384,
		DATA_WIDTH        => BW_INPUT_FIFO,
		FULL_THRESHOLD    => 256, --16384 - 512,
		EMPTY_THRESHOLD    => 128,--8192,
		USE_OUTPUT_BUFFER => true
	)
	port map(

		clk            => clk_mac,
		rst            => rst_mac,

		-- Input data handling
		----------------------
		input              => input_fifo_in_data,
		input_valid        => mac_valid_int,
		input_last         => mac_last,
		input_ready        => mac_ready_int,
		input_almost_full  => input_almost_full_int,
		input_almost_empty => input_almost_empty_int,
		fifo_max_level     => open,

		-- Output data handling
		-----------------------
		output          => input_fifo_0_out_data,
		output_valid    => input_fifo_0_out_valid,
		output_last     => input_fifo_0_out_last,
		output_ready    => input_fifo_0_out_ready
	);

	-- Second input FIFO, when this FIFO is full, send pause request
	inst_input_fifo_1 : entity share_let_1g.axi4s_fifo
	generic map(
		DISTR_RAM         => false,
		FIFO_DEPTH        => 512, --8192,
		DATA_WIDTH        => BW_INPUT_FIFO,
		FULL_THRESHOLD    => 256, --4096,
		EMPTY_THRESHOLD    => 128, --0,
		USE_OUTPUT_BUFFER => true
	)
	port map(

		clk            => clk_mac,
		rst            => rst_mac,

		-- Input data handling
		----------------------
		input              => input_fifo_0_out_data,
		input_valid        => input_fifo_0_out_valid,
		input_last         => input_fifo_0_out_last,
		input_ready        => input_fifo_0_out_ready,
		input_almost_full  => fifo_1_almost_full_int,
		input_almost_empty => open,
		fifo_max_level     => open,

		-- Output data handling
		-----------------------
		output          => input_fifo_out_data,
		output_valid    => input_fifo_out_valid,
		output_last     => input_fifo_out_last,
		output_ready    => input_fifo_out_ready
	);

	inst_error_flag_fifo : entity share_let_1g.axi4s_fifo
	generic map(
		DISTR_RAM         => false,
		FIFO_DEPTH        => 512,
		DATA_WIDTH        => 1,
		FULL_THRESHOLD    => 511,
		EMPTY_THRESHOLD   => 0,
		USE_OUTPUT_BUFFER => true
	)
	port map(

		clk            => clk_mac,
		rst            => rst_mac,

		-- Input data handling
		----------------------
		input              => input_error_vec,
		input_valid        => input_error_valid,
		input_last         => '0',
		input_ready        => input_error_ready,
		input_almost_full  => open,
		input_almost_empty => open,
		fifo_max_level     => open,

		-- Output data handling
		-----------------------
		output          => output_error_vec,
		output_valid    => output_error_valid,
		output_last     => open,
		output_ready    => output_error_ready
	);

	-- convert type
	input_error_vec(0) <= input_error;
	output_error       <= output_error_vec(0);

	-- Generate the frame length
	pr_count_frame_length: process(clk_mac) is
	begin
	if rising_edge(clk_mac) then
		if rst_mac = '1' then
			mac_cnt            <= (others => '0');
			mac_incr           <= (others => '0');
			mac_incr_valid     <= '0';
			mac_incr_last      <= '0';
			mac_incr_last_d    <= '0';
			mac_cnt_valid      <= '0';
			input_error        <= '0';
			input_error_valid  <= '0';
			read_state         <= '0';
			output_error_ready <= '0';
			output_error_reg   <= '0';
		else
			--assert not(mac_length_valid = '0' and mac_length_ready = '1') report
			--	"RX FIFO: mac_length_valid always have to be one if mac_length_ready turns one!" severity warning; --failure;
--
			--assert mac_cnt_ready /= '0' report "RX FIFO: mac_cnt_ready should never go to zero!" severity warning; --failure;

			mac_incr_valid <= '0';
			mac_incr_last  <= '0';
			if input_fifo_0_out_valid = '1' and input_fifo_0_out_ready = '1' then
				mac_incr_valid <= '1';
				mac_incr_last  <= input_fifo_0_out_last;
				mac_incr <= to_unsigned(1, 4);
--				case input_fifo_0_out_data(7 downto 0) is
--				when "00000001" => mac_incr <= to_unsigned(1, 4);
--				when "00000011" => mac_incr <= to_unsigned(2, 4);
--				when "00000111" => mac_incr <= to_unsigned(3, 4);
--				when "00001111" => mac_incr <= to_unsigned(4, 4);
--				when "00011111" => mac_incr <= to_unsigned(5, 4);
--				when "00111111" => mac_incr <= to_unsigned(6, 4);
--				when "01111111" => mac_incr <= to_unsigned(7, 4);
--				when others     => mac_incr <= to_unsigned(8, 4);
--				end case;
			end if;

			mac_cnt_valid <= '0';
			if mac_incr_valid = '1' then
				mac_cnt <= mac_cnt + mac_incr;
				if mac_incr_last = '1' then
					if input_error = '0' then
						mac_cnt_valid   <= '1';
					end if;
					mac_incr_last_d <= '1';
				end if;
				if mac_incr_last_d = '1' then
					mac_incr_last_d <= '0';
					mac_cnt <= to_unsigned(8, 14);
				end if;
			end if;

			assert input_error_ready /= '0' report "RX FIFO: input_error_ready should never go to zero!" severity warning; -- failure;

			-- control signals for the fifo output
			input_error_valid <= '0';
			input_error <= '0';
			if input_fifo_0_out_valid = '1' and input_fifo_0_out_ready = '1' and input_fifo_0_out_last = '1' then
				input_error       <= input_fifo_0_out_data(BW_INPUT_FIFO - 1);
				input_error_valid <= '1';
			end if;

			if read_state = '0' then
				output_error_ready <= '1';
				if output_error_ready = '1' and output_error_valid ='1' then
					output_error_ready <= '0';
					output_error_reg   <= output_error;
					read_state <= '1';
				end if;
			else
				if input_fifo_out_valid = '1' and input_fifo_out_ready = '1' and input_fifo_out_last = '1' then
					output_error_ready <= '1';
					output_error_reg <= '0';
					read_state <= '0';
				end if;
			end if;
		end if;
	end if;
	end process pr_count_frame_length;

	input_fifo_out_ready <= '1'                   when output_error_reg = '1' else
		                     mac_dc_fifo_in_ready when read_state = '1'       else
		                     '0';

	mac_dc_fifo_in_valid <= input_fifo_out_valid when (read_state = '1' and output_error_reg = '0') else '0';
	mac_dc_fifo_in_data  <= input_fifo_out_data(8 - 1 downto 0);
	mac_dc_fifo_in_last  <= input_fifo_out_last;
	mac_dc_fifo_in_user	 <= input_fifo_out_data(BW_INPUT_FIFO - 1);			
	--
	-- The dual clock fifo performs is used for the clock
	-- conversion from MAC to LET framer.
	--

	inst_dc_rx_fifo_0 : dc_fifo_eth_side
	port map(
		m_aclk => clk_let,
		s_aclk => clk_mac,
		s_aresetn => rst_mac_n,

		s_axis_tvalid => mac_dc_fifo_in_valid,
		s_axis_tready => mac_dc_fifo_in_ready,
		s_axis_tdata  => mac_dc_fifo_in_data,
		s_axis_tuser  => mac_dc_fifo_in_user,
		s_axis_tlast  => mac_dc_fifo_in_last,

		m_axis_tvalid => mac_out_valid_int,
		m_axis_tready => mac_out_ready,
		m_axis_tdata  => mac_out_data,
		m_axis_tuser  => mac_out_error,
		m_axis_tlast  => mac_out_last_int
	);

	--
	-- This dual clock FIFO stores the frame length.
	-- The frame length correspond to the length inside the dc fifo
	--
	mac_length_fifo_in    <= "00" & std_logic_vector(mac_cnt);
--	inst_dc_rx_cnt_fifo_0 : dc_rx_cnt_fifo_0
--	port map(
--		m_aclk => clk_let,
--		s_aclk => clk_mac,
--		s_aresetn => rst_mac_n,
--
--		s_axis_tvalid => mac_cnt_valid,
--		s_axis_tready => mac_cnt_ready,
--		s_axis_tdata  => mac_length_fifo_in,
--
--		m_axis_tvalid => mac_length_valid,
--		m_axis_tready => mac_length_ready,
--		m_axis_tdata  => mac_length_fifo_out
--	);

	mac_out_length   <= mac_length_fifo_out(14 - 1 downto 0);
	mac_length_ready <= '1' when mac_out_valid_int = '1' and mac_out_ready = '1' and mac_out_last_int = '1' else
	                    '0';

end architecture rtl;
