----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 04/04/2021 11:40:33 AM
-- Design Name: 
-- Module Name: frame_filter - Behavioral
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2018.3
-- Description: Filters packets with errors signaled in tuser in AXI-Stream format.
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- TODO: Check frame length
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

--library frame_decoding;
--use frame_decoding.pkg_components.all;
library share_let_1g;
library sda_lib;
use sda_lib.pkg_sda.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity frame_filter is
    Port(
        clk           : in  std_logic;
        rst           : in  std_logic;
        -- Input data from encoder
        ----------------------
        s_axis_tdata  : in  std_logic_vector(8 - 1 downto 0);
        s_axis_tvalid : in  std_logic;
        s_axis_tlast  : in  std_logic;
        s_axis_tready : out std_logic;
        -- Output data to ethernet mac
        ----------------------
        m_axis_tdata  : out std_logic_vector(8 - 1 downto 0);
        m_axis_tvalid : out std_logic;
        m_axis_tstart : out std_logic;
        m_axis_tlast  : out std_logic;
        m_axis_tready : in  std_logic;
        crc16_valid   : in  std_logic;
        crc16_correct : in  std_logic;
        crc32_valid   : in  std_logic;
        crc32_correct : in  std_logic;
        finished      : out std_logic;
        empty         : out std_logic
    );
end frame_filter;

architecture Behavioral of frame_filter is

    type t_mux_fsm is (IDLE, PASS_PACKET, FILTER_PACKET, WAIT_CHECKS, CLEAR);
    signal mux_fsm : t_mux_fsm;

    signal input_data         : std_logic_vector(8 - 1 downto 0);
    signal input_valid        : std_logic;
    signal input_last         : std_logic;
    signal input_ready        : std_logic;
    signal input_almost_full  : std_logic;
    signal input_almost_empty : std_logic;

    -- Output data handling
    -----------------------
    signal output_data  : std_logic_vector(8 - 1 downto 0);
    signal output_valid : std_logic;
    signal output_last  : std_logic;
    signal output_ready : std_logic;

    signal output_ready_unload : std_logic;

    signal mux_enable_in     : std_logic;
    signal mux_enable_out    : std_logic;
    signal s_axis_tready_int : std_logic;

    signal lpc_frame_correct_flag : std_logic;
    signal payload_ncerr_flag     : std_logic;
    signal header_ncerr_flag      : std_logic;
    signal crc16_correct_flag     : std_logic;
    signal crc32_correct_flag     : std_logic;

    signal lpc_frame_correct_set : std_logic;
    signal payload_ncerr_set     : std_logic;
    signal header_ncerr_set      : std_logic;
    signal crc16_correct_set     : std_logic;
    signal crc32_correct_set     : std_logic;

    signal frame_good      : std_logic;
    signal all_checks_done : std_logic;
    signal empty_int       : std_logic;
    signal start_flag_int : std_logic;

begin

    inst_packet_output_buf : entity share_let_1g.axi4s_fifo
        generic map(
            DISTR_RAM         => false,
            FIFO_DEPTH        => C_FRAME_FULL_SIZE_BYTES + 8,
            DATA_WIDTH        => 8,
            FULL_THRESHOLD    => C_FRAME_FULL_SIZE_BYTES + 8,
            EMPTY_THRESHOLD   => 0,
            USE_OUTPUT_BUFFER => false
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => input_data,
            input_valid        => input_valid,
            input_last         => input_last,
            input_ready        => input_ready,
            input_almost_full  => input_almost_full,
            input_almost_empty => input_almost_empty,
            fifo_max_level     => open,
            -- Output data handling
            -----------------------
            output             => output_data,
            output_valid       => output_valid,
            output_last        => output_last,
            output_ready       => output_ready
        );

    frame_good      <= crc16_correct_flag and crc32_correct_flag;
    all_checks_done <= crc16_correct_set and crc32_correct_set;
    empty           <= input_almost_empty and empty_int;

    pr_fsm_control : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                mux_fsm                <= IDLE;
                mux_enable_out         <= '0';
                output_ready_unload    <= '0';
                mux_enable_in          <= '0';
                lpc_frame_correct_flag <= '0';
                payload_ncerr_flag     <= '0';
                header_ncerr_flag      <= '0';
                crc16_correct_flag     <= '0';
                crc32_correct_flag     <= '0';
                lpc_frame_correct_set  <= '0';
                payload_ncerr_set      <= '0';
                header_ncerr_set       <= '0';
                crc16_correct_set      <= '0';
                crc32_correct_set      <= '0';
                finished               <= '0';
                empty_int              <= '1';
                start_flag_int          <= '0';
                m_axis_tstart           <= '0';
            else

                empty_int <= '0';
                m_axis_tstart <= '0';

                case mux_fsm is

                    when IDLE =>
                        finished            <= '0';
                        mux_enable_out      <= '0';
                        output_ready_unload <= '0';
                        mux_enable_in       <= '1';
                        empty_int           <= '1';
                        if s_axis_tvalid = '1' and s_axis_tlast = '1' and s_axis_tready_int = '1' then
                            mux_enable_in <= '0';
                            mux_fsm       <= WAIT_CHECKS;
                        end if;
                        if crc16_valid = '1' and crc16_correct_set = '0' then
                            crc16_correct_flag <= crc16_correct;
                            crc16_correct_set  <= '1';
                        end if;
                        if crc32_valid = '1' and crc32_correct_set = '0' then
                            crc32_correct_flag <= crc32_correct;
                            crc32_correct_set  <= '1';
                        end if;

                    when WAIT_CHECKS =>

                        if crc16_valid = '1' and crc16_correct_set = '0' then
                            crc16_correct_flag <= crc16_correct;
                            crc16_correct_set  <= '1';
                        end if;
                        if crc32_valid = '1' and crc32_correct_set = '0' then
                            crc32_correct_flag <= crc32_correct;
                            crc32_correct_set  <= '1';
                        end if;

                        if all_checks_done = '1' then
                            if frame_good = '1' then
                                mux_fsm <= PASS_PACKET;
                                start_flag_int <= '1';
                            else
                                mux_fsm <= FILTER_PACKET;
                            end if;
                        end if;

                    when PASS_PACKET =>
                        mux_enable_out      <= '1';
                        output_ready_unload <= '0';
                        mux_enable_in       <= '0';
                        mux_fsm             <= PASS_PACKET;
                        --if input_almost_empty = '1' then
                        if output_valid = '1' and output_last = '1' and output_ready = '1' then
                            mux_enable_out <= '0';
                            mux_fsm        <= CLEAR;
                        end if;
                        if start_flag_int = '1' then
                            start_flag_int <= '0';
                            m_axis_tstart <= '1';
                        else
                            m_axis_tstart <= '0';
                        end if;

                    when FILTER_PACKET =>
                        mux_fsm             <= FILTER_PACKET;
                        mux_enable_out      <= '0';
                        output_ready_unload <= '1';
                        mux_enable_in       <= '0';
                        --if input_almost_empty = '1' then
                        if output_valid = '1' and output_last = '1' and output_ready = '1' then
                            output_ready_unload <= '0';
                            mux_fsm             <= CLEAR;
                        end if;

                    when CLEAR =>
                        lpc_frame_correct_flag <= '0';
                        payload_ncerr_flag     <= '0';
                        header_ncerr_flag      <= '0';
                        crc16_correct_flag     <= '0';
                        crc32_correct_flag     <= '0';
                        lpc_frame_correct_set  <= '0';
                        payload_ncerr_set      <= '0';
                        header_ncerr_set       <= '0';
                        crc16_correct_set      <= '0';
                        crc32_correct_set      <= '0';
                        finished               <= '1';
                        mux_fsm                <= IDLE;
                    when others =>
                        mux_fsm <= IDLE;

                end case;

            end if;
        end if;
    end process;

    -- Combinational logic to define MUXES:

    m_axis_tdata <= output_data when mux_enable_out = '1' else
                    x"00";

    m_axis_tvalid <= output_valid when mux_enable_out = '1' else
                     '0';

    m_axis_tlast <= output_last when mux_enable_out = '1' else
                    '0';

    output_ready <= m_axis_tready when mux_enable_out = '1' else
                    '1' when output_ready_unload = '1' else
                    '0';
    ----------------------------------------
    input_data   <= s_axis_tdata when mux_enable_in = '1' else
                    x"00";

    input_valid <= s_axis_tvalid when mux_enable_in = '1' else
                   '0';

    input_last <= s_axis_tlast when mux_enable_in = '1' else
                  '0';

    s_axis_tready_int <= input_ready when mux_enable_in = '1' else
                         '0';
    ----------------------------------------
    s_axis_tready     <= s_axis_tready_int;

end Behavioral;

