----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 04/04/2021 11:40:33 AM
-- Design Name: 
-- Module Name: frame_filter_top - Behavioral
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2018.3
-- Description: Filters packets with double buffer implementation user frame_filter.
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

--library frame_decoding;
--use frame_decoding.pkg_components.all;
library frame_filter_lib;
--library let_sda_lib;
library monitoring_lib;

--use let_sda_lib.let_pckg_components.bitrate_calculator;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity frame_filter_top is
    Port(
        clk            : in  std_logic;
        rst            : in  std_logic;
        -- Input data from encoder
        ----------------------
        s_axis_tdata   : in  std_logic_vector(8 - 1 downto 0);
        s_axis_tvalid  : in  std_logic;
        s_axis_tlast   : in  std_logic;
        s_axis_tready  : out std_logic;
        -- Output data to ethernet mac
        ----------------------
        m_axis_tdata   : out std_logic_vector(8 - 1 downto 0);
        m_axis_tvalid  : out std_logic;
        m_axis_tstart  : out std_logic;
        m_axis_tlast   : out std_logic;
        m_axis_tready  : in  std_logic;
        crc16_valid    : in  std_logic;
        crc16_correct  : in  std_logic;
        crc32_valid    : in  std_logic;
        crc32_correct  : in  std_logic;
        packet_cnt_in  : out std_logic_vector(31 downto 0);
        packet_cnt_out : out std_logic_vector(31 downto 0)
    );
end frame_filter_top;

architecture Behavioral of frame_filter_top is

    type t_axis_out_mux_fsm is (OUT_FILTER_A, OUT_FILTER_B, OUT_FILTER_C, OUT_FILTER_D);
    signal axis_out_mux_fsm : t_axis_out_mux_fsm;

    type t_axis_in_mux_fsm is (WAIT_EMPTY_A, FILTER_A, FILTER_B, WAIT_EMPTY_B, FILTER_C, WAIT_EMPTY_C, FILTER_D, WAIT_EMPTY_D);
    signal axis_in_mux_fsm : t_axis_in_mux_fsm;

    type t_crc_mux_fsm is (CRC_A, CRC_B, CRC_C, CRC_D);
    signal crc_mux_fsm : t_crc_mux_fsm;

    signal input_data_a  : std_logic_vector(8 - 1 downto 0);
    signal input_valid_a : std_logic;
    signal input_last_a  : std_logic;
    signal input_ready_a : std_logic;

    signal input_data_b  : std_logic_vector(8 - 1 downto 0);
    signal input_valid_b : std_logic;
    signal input_last_b  : std_logic;
    signal input_ready_b : std_logic;

    signal input_data_c  : std_logic_vector(8 - 1 downto 0);
    signal input_valid_c : std_logic;
    signal input_last_c  : std_logic;
    signal input_ready_c : std_logic;

    signal input_data_d  : std_logic_vector(8 - 1 downto 0);
    signal input_valid_d : std_logic;
    signal input_last_d  : std_logic;
    signal input_ready_d : std_logic;

    signal input_almost_full  : std_logic;
    signal input_almost_empty : std_logic;

    -- Output data handling
    -----------------------
    signal output_data_a  : std_logic_vector(8 - 1 downto 0);
    signal output_valid_a : std_logic;
    signal output_start_a : std_logic;
    signal output_last_a  : std_logic;
    signal output_ready_a : std_logic;

    signal output_data_b  : std_logic_vector(8 - 1 downto 0);
    signal output_valid_b : std_logic;
    signal output_start_b : std_logic;
    signal output_last_b  : std_logic;
    signal output_ready_b : std_logic;

    signal output_data_c  : std_logic_vector(8 - 1 downto 0);
    signal output_valid_c : std_logic;
    signal output_start_c : std_logic;
    signal output_last_c  : std_logic;
    signal output_ready_c : std_logic;

    signal output_data_d  : std_logic_vector(8 - 1 downto 0);
    signal output_valid_d : std_logic;
    signal output_start_d : std_logic;
    signal output_last_d  : std_logic;
    signal output_ready_d : std_logic;

    signal crc16_valid_a   : std_logic;
    signal crc16_correct_a : std_logic;
    signal crc32_valid_a   : std_logic;
    signal crc32_correct_a : std_logic;

    signal crc16_valid_b   : std_logic;
    signal crc16_correct_b : std_logic;
    signal crc32_valid_b   : std_logic;
    signal crc32_correct_b : std_logic;

    signal crc16_valid_c   : std_logic;
    signal crc16_correct_c : std_logic;
    signal crc32_valid_c   : std_logic;
    signal crc32_correct_c : std_logic;

    signal crc16_valid_d   : std_logic;
    signal crc16_correct_d : std_logic;
    signal crc32_valid_d   : std_logic;
    signal crc32_correct_d : std_logic;

    signal finished_a          : std_logic;
    signal finished_b          : std_logic;
    signal finished_c          : std_logic;
    signal finished_d          : std_logic;
    signal empty_a             : std_logic;
    signal empty_b             : std_logic;
    signal empty_c             : std_logic;
    signal empty_d             : std_logic;
    signal output_ready_unload : std_logic;

    signal s_axis_tready_int : std_logic;

    signal mux_enable_in  : std_logic;
    signal mux_enable_out : std_logic;
    signal crc_enable_in  : std_logic;

    signal m_axis_tvalid_int : std_logic;
    signal m_axis_tstart_int : std_logic;
    signal m_axis_tlast_int  : std_logic;

begin
    m_axis_tvalid <= m_axis_tvalid_int;
    m_axis_tlast  <= m_axis_tlast_int;
    m_axis_tstart  <= m_axis_tstart_int;

    Inst_FILTER_A : entity frame_filter_lib.frame_filter
        PORT map(
            clk           => clk,
            rst           => rst,
            s_axis_tvalid => input_valid_a,
            s_axis_tready => input_ready_a,
            s_axis_tdata  => input_data_a,
            s_axis_tlast  => input_last_a,
            m_axis_tvalid => output_valid_a,
            m_axis_tready => output_ready_a,
            m_axis_tdata  => output_data_a,
            m_axis_tlast  => output_last_a,
            m_axis_tstart  => output_start_a,
            crc16_valid   => crc16_valid_a,
            crc16_correct => crc16_correct_a,
            crc32_valid   => crc32_valid_a,
            crc32_correct => crc32_correct_a,
            finished      => finished_a,
            empty         => empty_a
        );

    Inst_FILTER_B : entity frame_filter_lib.frame_filter
        PORT map(
            clk           => clk,
            rst           => rst,
            s_axis_tvalid => input_valid_b,
            s_axis_tready => input_ready_b,
            s_axis_tdata  => input_data_b,
            s_axis_tlast  => input_last_b,
            m_axis_tvalid => output_valid_b,
            m_axis_tready => output_ready_b,
            m_axis_tdata  => output_data_b,
            m_axis_tlast  => output_last_b,
            m_axis_tstart  => output_start_b,
            crc16_valid   => crc16_valid_b,
            crc16_correct => crc16_correct_b,
            crc32_valid   => crc32_valid_b,
            crc32_correct => crc32_correct_b,
            finished      => finished_b,
            empty         => empty_b
        );

    Inst_FILTER_C : entity frame_filter_lib.frame_filter
        PORT map(
            clk           => clk,
            rst           => rst,
            s_axis_tvalid => input_valid_c,
            s_axis_tready => input_ready_c,
            s_axis_tdata  => input_data_c,
            s_axis_tlast  => input_last_c,
            m_axis_tvalid => output_valid_c,
            m_axis_tready => output_ready_c,
            m_axis_tdata  => output_data_c,
            m_axis_tlast  => output_last_c,
            m_axis_tstart  => output_start_c,
            crc16_valid   => crc16_valid_c,
            crc16_correct => crc16_correct_c,
            crc32_valid   => crc32_valid_c,
            crc32_correct => crc32_correct_c,
            finished      => finished_c,
            empty         => empty_c
        );

    Inst_FILTER_D : entity frame_filter_lib.frame_filter
        PORT map(
            clk           => clk,
            rst           => rst,
            s_axis_tvalid => input_valid_d,
            s_axis_tready => input_ready_d,
            s_axis_tdata  => input_data_d,
            s_axis_tlast  => input_last_d,
            m_axis_tvalid => output_valid_d,
            m_axis_tready => output_ready_d,
            m_axis_tdata  => output_data_d,
            m_axis_tlast  => output_last_d,
            m_axis_tstart  => output_start_d,
            crc16_valid   => crc16_valid_d,
            crc16_correct => crc16_correct_d,
            crc32_valid   => crc32_valid_d,
            crc32_correct => crc32_correct_d,
            finished      => finished_d,
            empty         => empty_d
        );

    inst_bitrate_calculator_in : entity monitoring_lib.bitrate_calculator
        Port map(
            clk              => clk,
            rst              => rst,
            -- Input data from encoder
            ----------------------
            s_axis_tdata     => (others => '0'),
            s_axis_tvalid    => s_axis_tvalid,
            s_axis_tlast     => s_axis_tlast,
            s_axis_tready    => s_axis_tready_int,
            bitrate          => open,
            total_packet_cnt => packet_cnt_in
        );

    inst_bitrate_calculator_out : entity monitoring_lib.bitrate_calculator
        Port map(
            clk              => clk,
            rst              => rst,
            -- Input data from encoder
            ----------------------
            s_axis_tdata     => (others => '0'),
            s_axis_tvalid    => m_axis_tvalid_int,
            s_axis_tlast     => m_axis_tlast_int,
            s_axis_tready    => m_axis_tready,
            bitrate          => open,
            total_packet_cnt => packet_cnt_out
        );

    pr_fsm_control : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                axis_in_mux_fsm  <= WAIT_EMPTY_A;
                axis_out_mux_fsm <= OUT_FILTER_A;
                crc_mux_fsm      <= CRC_A;
            else

                case axis_in_mux_fsm is

                    when WAIT_EMPTY_A =>
                        if empty_a = '1' then
                            axis_in_mux_fsm <= FILTER_A;
                        end if;

                    when WAIT_EMPTY_B =>
                        if empty_b = '1' then
                            axis_in_mux_fsm <= FILTER_B;
                        end if;

                    when WAIT_EMPTY_C =>
                        if empty_c = '1' then
                            axis_in_mux_fsm <= FILTER_C;
                        end if;

                    when WAIT_EMPTY_D =>
                        if empty_d = '1' then
                            axis_in_mux_fsm <= FILTER_D;
                        end if;

                    when FILTER_A =>
                        if s_axis_tvalid = '1' and s_axis_tlast = '1' and s_axis_tready_int = '1' then
                            axis_in_mux_fsm <= WAIT_EMPTY_B;
                        end if;

                    when FILTER_B =>
                        if s_axis_tvalid = '1' and s_axis_tlast = '1' and s_axis_tready_int = '1' then
                            axis_in_mux_fsm <= WAIT_EMPTY_C;
                        end if;

                    when FILTER_C =>
                        if s_axis_tvalid = '1' and s_axis_tlast = '1' and s_axis_tready_int = '1' then
                            axis_in_mux_fsm <= WAIT_EMPTY_D;
                        end if;

                    when FILTER_D =>
                        if s_axis_tvalid = '1' and s_axis_tlast = '1' and s_axis_tready_int = '1' then
                            axis_in_mux_fsm <= WAIT_EMPTY_A;
                        end if;

                    when others =>
                        axis_in_mux_fsm <= WAIT_EMPTY_A;

                end case;

                case axis_out_mux_fsm is

                    when OUT_FILTER_A =>
                        if finished_a = '1' then
                            axis_out_mux_fsm <= OUT_FILTER_B;
                        end if;

                    when OUT_FILTER_B =>
                        if finished_b = '1' then
                            axis_out_mux_fsm <= OUT_FILTER_C;
                        end if;

                    when OUT_FILTER_C =>
                        if finished_c = '1' then
                            axis_out_mux_fsm <= OUT_FILTER_D;
                        end if;

                    when OUT_FILTER_D =>
                        if finished_d = '1' then
                            axis_out_mux_fsm <= OUT_FILTER_A;
                        end if;

                    when others =>
                        axis_out_mux_fsm <= OUT_FILTER_A;

                end case;

                if crc32_valid = '1' then
                    if axis_in_mux_fsm = FILTER_A or axis_in_mux_fsm = WAIT_EMPTY_A then
                        crc_mux_fsm <= CRC_A;
                    elsif axis_in_mux_fsm = FILTER_B or axis_in_mux_fsm = WAIT_EMPTY_B then
                        crc_mux_fsm <= CRC_B;
                    elsif axis_in_mux_fsm = FILTER_C or axis_in_mux_fsm = WAIT_EMPTY_C then
                        crc_mux_fsm <= CRC_C;
                    elsif axis_in_mux_fsm = FILTER_D or axis_in_mux_fsm = WAIT_EMPTY_D then
                        crc_mux_fsm <= CRC_D;
                    end if;
                end if;

            end if;
        end if;
    end process;

    s_axis_tready <= s_axis_tready_int;

    -- Combinational logic to define MUXES:

    m_axis_tdata <= output_data_a when axis_out_mux_fsm = OUT_FILTER_A else
                    output_data_b when axis_out_mux_fsm = OUT_FILTER_B else
                    output_data_c when axis_out_mux_fsm = OUT_FILTER_C else
                    output_data_d when axis_out_mux_fsm = OUT_FILTER_D else
                    x"00";

    m_axis_tvalid_int <= output_valid_a when axis_out_mux_fsm = OUT_FILTER_A else
                         output_valid_b when axis_out_mux_fsm = OUT_FILTER_B else
                         output_valid_c when axis_out_mux_fsm = OUT_FILTER_C else
                         output_valid_d when axis_out_mux_fsm = OUT_FILTER_D else
                         '0';

    m_axis_tstart_int <= output_start_a when axis_out_mux_fsm = OUT_FILTER_A else
                         output_start_b when axis_out_mux_fsm = OUT_FILTER_B else
                         output_start_c when axis_out_mux_fsm = OUT_FILTER_C else
                         output_start_d when axis_out_mux_fsm = OUT_FILTER_D else
                         '0';

    m_axis_tlast_int <= output_last_a when axis_out_mux_fsm = OUT_FILTER_A else
                        output_last_b when axis_out_mux_fsm = OUT_FILTER_B else
                        output_last_c when axis_out_mux_fsm = OUT_FILTER_C else
                        output_last_d when axis_out_mux_fsm = OUT_FILTER_D else
                        '0';

    output_ready_a <= m_axis_tready when axis_out_mux_fsm = OUT_FILTER_A else
                      '0';

    output_ready_b <= m_axis_tready when axis_out_mux_fsm = OUT_FILTER_B else
                      '0';

    output_ready_c <= m_axis_tready when axis_out_mux_fsm = OUT_FILTER_C else
                      '0';

    output_ready_d <= m_axis_tready when axis_out_mux_fsm = OUT_FILTER_D else
                      '0';
    ----------------------------------------
    input_data_a   <= s_axis_tdata when axis_in_mux_fsm = FILTER_A else
                      x"00";
    input_data_b   <= s_axis_tdata when axis_in_mux_fsm = FILTER_B else
                      x"00";
    input_data_c   <= s_axis_tdata when axis_in_mux_fsm = FILTER_C else
                      x"00";
    input_data_d   <= s_axis_tdata when axis_in_mux_fsm = FILTER_D else
                      x"00";

    input_valid_a <= s_axis_tvalid when axis_in_mux_fsm = FILTER_A else
                     '0';
    input_valid_b <= s_axis_tvalid when axis_in_mux_fsm = FILTER_B else
                     '0';
    input_valid_c <= s_axis_tvalid when axis_in_mux_fsm = FILTER_C else
                     '0';
    input_valid_d <= s_axis_tvalid when axis_in_mux_fsm = FILTER_D else
                     '0';

    input_last_a <= s_axis_tlast when axis_in_mux_fsm = FILTER_A else
                    '0';
    input_last_b <= s_axis_tlast when axis_in_mux_fsm = FILTER_B else
                    '0';
    input_last_c <= s_axis_tlast when axis_in_mux_fsm = FILTER_C else
                    '0';
    input_last_d <= s_axis_tlast when axis_in_mux_fsm = FILTER_D else
                    '0';

    s_axis_tready_int <= input_ready_a when axis_in_mux_fsm = FILTER_A else
                         input_ready_b when axis_in_mux_fsm = FILTER_B else
                         input_ready_c when axis_in_mux_fsm = FILTER_C else
                         input_ready_d when axis_in_mux_fsm = FILTER_D else
                         '0';
    ----------------------------------------

    crc16_valid_a   <= crc16_valid when axis_in_mux_fsm = FILTER_A else '0';
    crc16_correct_a <= crc16_correct when axis_in_mux_fsm = FILTER_A else '0';
    crc32_valid_a   <= crc32_valid when crc_mux_fsm = CRC_A else '0';
    crc32_correct_a <= crc32_correct when crc_mux_fsm = CRC_A else '0';

    crc16_valid_b   <= crc16_valid when axis_in_mux_fsm = FILTER_B else '0';
    crc16_correct_b <= crc16_correct when axis_in_mux_fsm = FILTER_B else '0';
    crc32_valid_b   <= crc32_valid when crc_mux_fsm = CRC_B else '0';
    crc32_correct_b <= crc32_correct when crc_mux_fsm = CRC_B else '0';

    crc16_valid_c   <= crc16_valid when axis_in_mux_fsm = FILTER_C else '0';
    crc16_correct_c <= crc16_correct when axis_in_mux_fsm = FILTER_C else '0';
    crc32_valid_c   <= crc32_valid when crc_mux_fsm = CRC_C else '0';
    crc32_correct_c <= crc32_correct when crc_mux_fsm = CRC_C else '0';

    crc16_valid_d   <= crc16_valid when axis_in_mux_fsm = FILTER_D else '0';
    crc16_correct_d <= crc16_correct when axis_in_mux_fsm = FILTER_D else '0';
    crc32_valid_d   <= crc32_valid when crc_mux_fsm = CRC_D else '0';
    crc32_correct_d <= crc32_correct when crc_mux_fsm = CRC_D else '0';

end Behavioral;

