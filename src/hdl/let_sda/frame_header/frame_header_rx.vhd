-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: frame_header_rx
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library share_let_1g;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

entity frame_header_rx is
    port(
        clk             : in    std_logic;
        reset           : in    std_logic;
        axis_if_m         : in t_axis_if_m; -- Output axi-s
        axis_if_s         : out t_axis_if_s; -- Output axi-s
        axis_payload_if_m : out t_axis_payload_if_m; -- Input axi-s
        axis_payload_if_s : in t_axis_payload_if_s; -- Input axi-s
        ack_data_if_m     : out t_ack_data_if_m; -- ACK data interface
        ack_data_if_s     : in t_ack_data_if_s; -- ACK data interface
        fcch_if_m         : out t_fcch_if_m; -- FCCH interface
        fcch_if_s         : in t_fcch_if_s; -- FCCH interface
        ts_record_en      : in std_logic;
        ts_data_valid     : out std_logic;
        ts_data_if        : out t_ts_insertion_if;
        ts_insertion_ready : in std_logic;
        pl_rate           : out   std_logic_vector(3 downto 0) -- Payload data rate
    );
end entity frame_header_rx;

architecture RTL of frame_header_rx is
    type fsm_type is (START, CHECK_BUFFER_NOT_FULL, SET_HEADER_DATA, READ_HEADER, PUSH_PAYLOAD);
    signal fsm_state : fsm_type;

    type mux_sel_type is (PAYLOAD_SEL, HEADER_SEL, NO_SEL);
    signal mux_sel : mux_sel_type;

    type metadata_fsm_type is (WAIT_FOR_LAST, ASSERT_METADATA);
    signal metadata_fsm : metadata_fsm_type;

    constant c_metadata_length : natural := axis_payload_if_m.tx_fn'length + axis_payload_if_m.frame_type'length + axis_payload_if_m.tx_num'length;
    signal metadata_in         : std_logic_vector(c_metadata_length - 1 downto 0);
    signal metadata_valid      : std_logic;
    signal metadata_buf        : std_logic_vector(c_metadata_length - 1 downto 0);
    signal metadata_buf_valid  : std_logic;
    signal metadata_buf_ready  : std_logic;
    signal tx_fn_buf           : std_logic_vector(15 downto 0); -- Sequence number of this Tx Frame
    signal frame_type_buf      : std_logic_vector(1 downto 0); -- Frame Type
    signal tx_num_buf          : std_logic_vector(2 downto 0); -- Transmission atempt

    signal frame_buf_if_m   : t_axis_if_m;
    signal frame_buf_if_s   : t_axis_if_s;
    signal payload_buf_if_m : t_axis_payload_if_m;
    signal payload_buf_if_s : t_axis_payload_if_s;
    signal header_buf_if_m  : t_axis_if_m;
    signal header_buf_if_s  : t_axis_if_s;
    signal mux_out_if_m     : t_axis_if_m;
    signal mux_out_if_s     : t_axis_if_s;

    signal payload_full_in  : std_logic;
    signal payload_empty_in : std_logic;
    signal frame_full_in    : std_logic;
    signal frame_empty_in   : std_logic;

    signal start_flag_enable : std_logic;
    signal payload_buf_valid : std_logic;

    signal frame_header_fields : frame_header_fields_t;

begin

    --axis_if.tuser                <= 'Z';
    --axis_if.tstart               <= 'Z';
    payload_buf_if_m.axis_if_m.tuser <= '0';
    --ack_data_if.rd_ena           <= 'Z';
    --fcch_if.rd_ena               <= 'Z';
    metadata_buf_ready           <= axis_payload_if_m.axis_if_m.tvalid and axis_payload_if_m.axis_if_m.tlast and axis_payload_if_s.axis_if_s.tready;
    metadata_in                  <= payload_buf_if_m.tx_num & payload_buf_if_m.frame_type & payload_buf_if_m.tx_fn;
    axis_payload_if_m.tx_fn        <= metadata_buf(15 downto 0);
    axis_payload_if_m.frame_type   <= metadata_buf(17 downto 16);
    axis_payload_if_m.tx_num       <= metadata_buf(20 downto 18);

    -- FIFO buffer for incoming Payload
    inst_full_frame_buffer_input : entity share_let_1g.axi4s_fifo_v2
        generic map(
            DISTR_RAM         => false,
            FIFO_DEPTH        => C_FRAME_FULL_SIZE_BYTES * 10,
            DATA_WIDTH        => 8,
            FULL_THRESHOLD    => C_FRAME_FULL_SIZE_BYTES * 7,
            EMPTY_THRESHOLD   => 0,
            USE_OUTPUT_BUFFER => false
        )
        port map(
            clk                => clk,
            rst                => reset,
            -- Input data handling
            ----------------------
            input              => axis_if_m.tdata,
            input_valid        => axis_if_m.tvalid,
            input_last         => axis_if_m.tlast,
            input_user         => axis_if_m.tuser,
            input_start        => axis_if_m.tstart,
            input_ready        => axis_if_s.tready,
            input_almost_full  => frame_full_in,
            input_almost_empty => frame_empty_in,
            fifo_max_level     => open,
            -- Output data handling
            -----------------------
            output             => frame_buf_if_m.tdata,
            output_valid       => frame_buf_if_m.tvalid,
            output_last        => frame_buf_if_m.tlast,
            output_user        => frame_buf_if_m.tuser,
            output_start       => frame_buf_if_m.tstart,
            output_ready       => frame_buf_if_s.tready
        );

    -- FIFO for storing the frame before send
    inst_payload_output_buf : entity share_let_1g.axi4s_fifo_v2
        generic map(
            DISTR_RAM         => false,
            FIFO_DEPTH        => C_FSO_PAYLOAD_SIZE * 10,
            DATA_WIDTH        => 8,
            FULL_THRESHOLD    => C_FSO_PAYLOAD_SIZE * 7,
            EMPTY_THRESHOLD   => 0,
            USE_OUTPUT_BUFFER => false
        )
        port map(
            clk                => clk,
            rst                => reset,
            -- Input data handling
            ----------------------
            input              => payload_buf_if_m.axis_if_m.tdata,
            input_valid        => payload_buf_if_m.axis_if_m.tvalid,
            input_last         => payload_buf_if_m.axis_if_m.tlast,
            input_user         => payload_buf_if_m.axis_if_m.tuser,
            input_ready        => payload_buf_if_s.axis_if_s.tready,
            input_start        => payload_buf_if_m.axis_if_m.tstart,
            input_almost_full  => payload_full_in,
            input_almost_empty => payload_empty_in,
            fifo_max_level     => open,
            -- Output data handling
            -----------------------
            output             => axis_payload_if_m.axis_if_m.tdata,
            output_valid       => axis_payload_if_m.axis_if_m.tvalid,
            output_last        => axis_payload_if_m.axis_if_m.tlast,
            output_start       => axis_payload_if_m.axis_if_m.tstart,
            output_user        => axis_payload_if_m.axis_if_m.tuser,
            output_ready       => axis_payload_if_s.axis_if_s.tready
        );

    -- FIFO buffer for incoming metadata 
    inst_metadata_buffer_output : entity share_let_1g.axi4s_fifo
        generic map(
            DISTR_RAM         => true,
            FIFO_DEPTH        => 32,
            DATA_WIDTH        => c_metadata_length, -- tx_fn+frame_type+tx_num
            FULL_THRESHOLD    => 32,
            EMPTY_THRESHOLD   => 0,
            USE_OUTPUT_BUFFER => false
        )
        port map(
            clk                => clk,
            rst                => reset,
            -- Input data handling
            ----------------------
            input              => metadata_in,
            input_valid        => metadata_valid,
            input_last         => '1',
            input_ready        => open,
            input_almost_full  => open,
            input_almost_empty => open,
            fifo_max_level     => open,
            -- Output data handling
            -----------------------
            output             => metadata_buf,
            output_valid       => metadata_buf_valid,
            output_last        => open,
            output_ready       => metadata_buf_ready -- to sync with the payload
        );

    process(clk) is
        variable frame_header_ram : frame_header_ram_t;
        variable v_cnt            : natural := 0;
        variable v_start_asserted : std_logic;

    begin
        if rising_edge(clk) then
            if reset = '1' then
                fsm_state           <= START;
                mux_sel             <= NO_SEL;
                frame_header_fields <= C_FRAME_HEADER_INIT;
                ack_data_if_m         <= C_ACK_DATA_IF_MASTER_INIT;
                fcch_if_m             <= C_FCCH_IF_MASTER_INIT;
                v_cnt               := 0;
                metadata_valid      <= '0';
                frame_header_ram    := (others => (others => '0'));
                pl_rate             <= (others => '0');
                v_start_asserted    := '0';
                start_flag_enable   <= '0';
            else

                header_buf_if_s.tready  <= '0';
                ack_data_if_m.ack_valid <= '0';
                fcch_if_m.valid         <= '0';
                ts_data_valid           <= '0';
                metadata_valid        <= '0';

                case fsm_state is
                    when START =>
                        fsm_state         <= START;
                        mux_sel           <= NO_SEL;
                        --frame_header_fields <= C_FRAME_HEADER_INIT;
                        v_cnt             := 0;
                        v_start_asserted  := '0';
                        start_flag_enable <= '0';
                        if frame_buf_if_m.tvalid = '1' then
                            fsm_state <= CHECK_BUFFER_NOT_FULL;
                        end if;

                    when CHECK_BUFFER_NOT_FULL =>
                        fsm_state <= CHECK_BUFFER_NOT_FULL;
                        mux_sel   <= NO_SEL;
                        if payload_full_in = '0' then
                            fsm_state <= READ_HEADER;
                            --frame_header_ram := frame_header2ram(frame_header_fields);
                        end if;
                        v_cnt := 0;
                    --mux_sel <= HEADER_SEL;

                    when READ_HEADER =>
                        fsm_state <= READ_HEADER;
                        mux_sel   <= HEADER_SEL;
                        if v_cnt = C_FRAME_HEADER_SIZE - 1 then
                            frame_header_ram(v_cnt) := header_buf_if_m.tdata;
                            frame_header_fields     <= array2frame_header(frame_header_ram);
                            fsm_state               <= SET_HEADER_DATA;
                            mux_sel                 <= NO_SEL;
                            header_buf_if_s.tready    <= '0';
                        else
                            -- 
                            header_buf_if_s.tready <= '1';
                            if header_buf_if_m.tvalid = '1' and header_buf_if_m.tlast = '0' then
                                frame_header_ram(v_cnt) := header_buf_if_m.tdata;
                                v_cnt                   := v_cnt + 1;
                            end if;
                        end if;

                    when SET_HEADER_DATA =>
                        fsm_state         <= PUSH_PAYLOAD;
                        start_flag_enable <= '1';
                        -- update ACK data if valid
                        if frame_header_fields.ack_valid = '1' then
                            ack_data_if_m.ack_start_fn <= frame_header_fields.ack_start_fn;
                            ack_data_if_m.ack_span     <= frame_header_fields.ack_span;
                            ack_data_if_m.ack          <= frame_header_fields.ack;
                            ack_data_if_m.ack_valid    <= '1';
                        else
                            ack_data_if_m.ack_valid <= '0';
                        end if;

                        --update FCCH data if valid
                        if frame_header_fields.fcch_opcode /= C_FCCH_NOT_PRESENT then
                            fcch_if_m.opcode  <= frame_header_fields.fcch_opcode;
                            fcch_if_m.payload <= frame_header_fields.fcch_pl;
                            fcch_if_m.valid   <= '1';
                        else
                            fcch_if_m.valid <= '0';
                        end if;

                        --Update metadata for the payload interface:
                        payload_buf_if_m.tx_fn      <= frame_header_fields.txfn;
                        payload_buf_if_m.tx_num     <= frame_header_fields.tx_num;
                        payload_buf_if_m.frame_type <= frame_header_fields.frame_type;
                        metadata_valid            <= '1';
                        pl_rate                   <= frame_header_fields.pl_rate; -- TODO: need interface for this

                        --timestamp data:
                        if ts_insertion_ready = '1' then
                            ts_data_if.tx_ts       <= frame_header_fields.tx_ts;  
                            ts_data_if.tod_seconds <= frame_header_fields.tod_seconds;
                            ts_data_if.ts_applies  <= frame_header_fields.ts_applies;
                            ts_data_valid <= '1';
                        end if;

                    when PUSH_PAYLOAD =>

                        fsm_state <= PUSH_PAYLOAD;
                        -- Switch MUX to payload buffer
                        mux_sel   <= PAYLOAD_SEL;
                        -- Detect LAST, disable MUX and go to START
                        if payload_buf_if_m.axis_if_m.tvalid = '1' and payload_buf_if_m.axis_if_m.tlast = '1' and payload_buf_if_s.axis_if_s.tready = '1' then
                            mux_sel   <= NO_SEL;
                            fsm_state <= START;
                        end if;
                        if payload_buf_if_m.axis_if_m.tvalid = '1' and payload_buf_if_m.axis_if_m.tlast = '0' and payload_buf_if_s.axis_if_s.tready = '1' and v_start_asserted = '0' then
                            start_flag_enable <= '0';
                            v_start_asserted  := '1';

                        end if;

                    when others =>
                        fsm_state <= START;
                end case;
            end if;
        end if;
    end process;

    -- Logic for start flag:
    payload_buf_if_m.axis_if_m.tstart <= payload_buf_if_m.axis_if_m.tvalid and payload_buf_if_s.axis_if_s.tready and (not payload_buf_if_m.axis_if_m.tlast) and start_flag_enable;

    -- Combinational logic to define MUX:

    header_buf_if_m.tdata          <= frame_buf_if_m.tdata when mux_sel = HEADER_SEL else
                                    x"00";
    payload_buf_if_m.axis_if_m.tdata <= frame_buf_if_m.tdata when mux_sel = PAYLOAD_SEL else
                                    x"00";

    header_buf_if_m.tvalid          <= frame_buf_if_m.tvalid when mux_sel = HEADER_SEL else
                                     '0';
    payload_buf_if_m.axis_if_m.tvalid <= frame_buf_if_m.tvalid when mux_sel = PAYLOAD_SEL else
                                     '0';

    header_buf_if_m.tlast          <= frame_buf_if_m.tlast when mux_sel = HEADER_SEL else
                                    '0';
    payload_buf_if_m.axis_if_m.tlast <= frame_buf_if_m.tlast when mux_sel = PAYLOAD_SEL else
                                    '0';

    frame_buf_if_s.tready <= header_buf_if_s.tready when mux_sel = HEADER_SEL else
                           payload_buf_if_s.axis_if_s.tready when mux_sel = PAYLOAD_SEL else
                           '0';

end architecture RTL;
