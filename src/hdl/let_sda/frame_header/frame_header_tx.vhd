-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: frame_header_tx
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library share_let_1g;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

entity frame_header_tx is
    port(
        clk             : in    std_logic;
        reset           : in    std_logic;
        axis_payload_if_m : in t_axis_payload_if_m; -- Input axi-s
        axis_payload_if_s : out t_axis_payload_if_s; -- Input axi-s
        axis_if_m         : out t_axis_if_m; -- Output axi-s
        axis_if_s         : in t_axis_if_s; -- Output axi-s
        ack_data_if_m     : in t_ack_data_if_m; -- ACK data interface
        ack_data_if_s     : out t_ack_data_if_s; -- ACK data interface
        fcch_if_m         : in t_fcch_if_m; -- FCCH interface
        fcch_if_s         : out t_fcch_if_s; -- FCCH interface
        pl_rate         : in    std_logic_vector(3 downto 0) -- Payload data rate
    );
end entity frame_header_tx;

architecture RTL of frame_header_tx is
    type fsm_type is (START, WAIT_1CLK, SET_HEADER_DATA, CHECK_BUFFER_NOT_FULL, PUSH_HEADER, PUSH_PAYLOAD);
    signal fsm_state : fsm_type;

    type metadata_fsm_type is (WAIT_FOR_LAST, ASSERT_METADATA);
    signal metadata_fsm : metadata_fsm_type;

    type mux_sel_type is (PAYLOAD_SEL, HEADER_SEL, NO_SEL);
    signal mux_sel : mux_sel_type;

    signal frame_buf_if_m   : t_axis_if_m;
    signal frame_buf_if_s   : t_axis_if_s;
    signal payload_buf_if_m : t_axis_if_m;
    signal payload_buf_if_s : t_axis_if_s;
    signal header_buf_if_m  : t_axis_if_m;
    signal header_buf_if_s  : t_axis_if_s;
    signal mux_out_if_m     : t_axis_if_m;
    signal mux_out_if_s     : t_axis_if_s;

    signal payload_full_in     : std_logic;
    signal payload_empty_in    : std_logic;
    signal frame_full_in       : std_logic;
    signal frame_empty_in      : std_logic;
    signal tx_fn               : std_logic_vector(15 downto 0); -- Sequence number of this Tx Frame
    signal frame_type          : std_logic_vector(1 downto 0); -- Frame Type
    signal tx_num              : std_logic_vector(2 downto 0); -- Transmission atempt
    constant c_metadata_length : natural := tx_fn'length + frame_type'length + tx_num'length;
    signal metadata_in         : std_logic_vector(c_metadata_length - 1 downto 0);
    signal metadata_valid      : std_logic;
    signal metadata_buf        : std_logic_vector(c_metadata_length - 1 downto 0);
    signal metadata_buf_valid  : std_logic;
    signal metadata_buf_ready  : std_logic;
    signal tx_fn_buf           : std_logic_vector(15 downto 0); -- Sequence number of this Tx Frame
    signal frame_type_buf      : std_logic_vector(1 downto 0); -- Frame Type
    signal tx_num_buf          : std_logic_vector(2 downto 0); -- Transmission atempt
    signal frame_header_fields : frame_header_fields_t;

    signal cnt : natural := 0;

begin
    axis_if_m.tuser        <= '0';
    metadata_buf_ready   <= payload_buf_if_m.tlast and payload_buf_if_m.tvalid and payload_buf_if_s.tready;
    metadata_in          <= axis_payload_if_m.tx_num & axis_payload_if_m.frame_type & axis_payload_if_m.tx_fn;
    tx_fn_buf            <= metadata_buf(15 downto 0);
    frame_type_buf       <= metadata_buf(17 downto 16);
    tx_num_buf           <= metadata_buf(20 downto 18);
    metadata_valid       <= axis_payload_if_m.axis_if_m.tstart;

    -- FIFO buffer for incoming Payload
    inst_payload_buffer_input : entity share_let_1g.axi4s_fifo_v2
        generic map(
            DISTR_RAM         => true,
            FIFO_DEPTH        => C_FSO_FRAME_SIZE,
            DATA_WIDTH        => 8,
            FULL_THRESHOLD    => C_FSO_FRAME_SIZE,
            EMPTY_THRESHOLD   => 0,
            USE_OUTPUT_BUFFER => false
        )
        port map(
            clk                => clk,
            rst                => reset,
            -- Input data handling
            ----------------------
            input              => axis_payload_if_m.axis_if_m.tdata,
            input_valid        => axis_payload_if_m.axis_if_m.tvalid,
            input_last         => axis_payload_if_m.axis_if_m.tlast,
            input_start        => axis_payload_if_m.axis_if_m.tstart,
            input_user         => axis_payload_if_m.axis_if_m.tuser,
            input_ready        => axis_payload_if_s.axis_if_s.tready,
            input_almost_full  => payload_full_in,
            input_almost_empty => payload_empty_in,
            fifo_max_level     => open,
            -- Output data handling
            -----------------------
            output             => payload_buf_if_m.tdata,
            output_valid       => payload_buf_if_m.tvalid,
            output_last        => payload_buf_if_m.tlast,
            output_start       => payload_buf_if_m.tstart,
            output_user        => payload_buf_if_m.tuser,
            output_ready       => payload_buf_if_s.tready
        );

    -- FIFO buffer for incoming metadata 
    inst_metadata_buffer_input : entity share_let_1g.axi4s_fifo
        generic map(
            DISTR_RAM         => true,
            FIFO_DEPTH        => 32,
            DATA_WIDTH        => c_metadata_length, -- tx_fn+frame_type+tx_num
            FULL_THRESHOLD    => 32,
            EMPTY_THRESHOLD   => 0,
            USE_OUTPUT_BUFFER => false
        )
        port map(
            clk                => clk,
            rst                => reset,
            -- Input data handling
            ----------------------
            input              => metadata_in,
            input_valid        => metadata_valid,
            input_last         => '1',
            input_ready        => open,
            input_almost_full  => open,
            input_almost_empty => open,
            fifo_max_level     => open,
            -- Output data handling
            -----------------------
            output             => metadata_buf,
            output_valid       => metadata_buf_valid,
            output_last        => open,
            output_ready       => metadata_buf_ready -- to sync with the payload
        );

    -- FIFO for storing the frame before send
    inst_frame_output_buf : entity share_let_1g.axi4s_fifo_v2
        generic map(
            DISTR_RAM         => false,
            FIFO_DEPTH        => C_FRAME_FULL_SIZE_BYTES * 10,
            DATA_WIDTH        => 8,
            FULL_THRESHOLD    => C_FRAME_FULL_SIZE_BYTES * 7,
            EMPTY_THRESHOLD   => 0,
            USE_OUTPUT_BUFFER => true
        )
        port map(
            clk                => clk,
            rst                => reset,
            -- Input data handling
            ----------------------
            input              => mux_out_if_m.tdata,
            input_valid        => mux_out_if_m.tvalid,
            input_start        => mux_out_if_m.tstart,
            input_user        => mux_out_if_m.tuser,
            input_last         => mux_out_if_m.tlast,
            input_ready        => mux_out_if_s.tready,
            input_almost_full  => frame_full_in,
            input_almost_empty => frame_empty_in,
            fifo_max_level     => open,
            -- Output data handling
            -----------------------
            output             => axis_if_m.tdata,
            output_valid       => axis_if_m.tvalid,
            output_last        => axis_if_m.tlast,
            output_user        => axis_if_m.tuser,
            output_start       => axis_if_m.tstart,
            output_ready       => axis_if_s.tready
        );

    MAIN_FSM : process(clk) is
        variable frame_header_ram : frame_header_ram_t;
    begin
        if rising_edge(clk) then
            if reset = '1' then
                fsm_state                <= START;
                mux_sel                  <= NO_SEL;
                frame_header_fields      <= C_FRAME_HEADER_INIT;
                ack_data_if_s.rd_ena       <= '0';
                fcch_if_s.rd_ena           <= '0';
                cnt                      <= 0;
                header_buf_if_m.tdata      <= (others => '0');
                header_buf_if_m.tvalid     <= '0';
                header_buf_if_m.tlast      <= '0';
            else

                ack_data_if_s.rd_ena   <= '0';
                fcch_if_s.rd_ena       <= '0';
                --metadata_valid          <= '0';
                header_buf_if_m.tdata  <= (others => '0');
                header_buf_if_m.tvalid <= '0';
                header_buf_if_m.tlast  <= '0';

                case fsm_state is
                    when START =>
                        fsm_state           <= START;
                        mux_sel             <= NO_SEL;
                        frame_header_fields <= C_FRAME_HEADER_INIT;
                        cnt                 <= 0;
                        if payload_buf_if_m.tvalid = '1' then
                            fsm_state <= WAIT_1CLK;
                            --metadata_valid  <= '1';
                        end if;
                    when WAIT_1CLK =>
                        if metadata_buf_valid = '1' then
                            fsm_state <= SET_HEADER_DATA;
                        else
                            fsm_state <= WAIT_1CLK;
                        end if;

                    when SET_HEADER_DATA =>
                        fsm_state <= CHECK_BUFFER_NOT_FULL;
                        if ack_data_if_m.ack_valid = '1' then
                            frame_header_fields.ack_start_fn <= ack_data_if_m.ack_start_fn;
                            frame_header_fields.ack_span     <= ack_data_if_m.ack_span;
                            frame_header_fields.ack          <= ack_data_if_m.ack;
                            frame_header_fields.ack_valid    <= '1';
                            ack_data_if_s.rd_ena               <= '1';
                        else
                            frame_header_fields.ack_valid <= '0';
                        end if;

                        if fcch_if_m.valid = '1' then
                            frame_header_fields.fcch_opcode  <= fcch_if_m.opcode;
                            frame_header_fields.fcch_pl      <= fcch_if_m.payload;
                            fcch_if_s.rd_ena                 <= '1';
                        else
                            frame_header_fields.fcch_opcode <= C_FCCH_NOT_PRESENT;
                        end if;

                        frame_header_fields.txfn       <= tx_fn_buf;
                        frame_header_fields.tx_num     <= tx_num_buf;
                        frame_header_fields.frame_type <= frame_type_buf;
                        frame_header_fields.pl_rate    <= pl_rate; -- TODO: need interface for this

                    when CHECK_BUFFER_NOT_FULL =>
                        fsm_state <= CHECK_BUFFER_NOT_FULL;
                        mux_sel   <= NO_SEL;
                        if frame_full_in = '0' then
                            fsm_state        <= PUSH_HEADER;
                            mux_sel          <= HEADER_SEL;
                            frame_header_ram := frame_header2ram(frame_header_fields);

                        end if;
                        cnt       <= 0;
                    --mux_sel <= HEADER_SEL;

                    when PUSH_HEADER =>
                        fsm_state            <= PUSH_HEADER;
                        header_buf_if_m.tvalid <= '0';
                        header_buf_if_m.tlast  <= '0';
                        mux_sel              <= HEADER_SEL;
                        if cnt = C_FRAME_HEADER_SIZE then
                            cnt       <= 0;
                            fsm_state <= PUSH_PAYLOAD;
                            mux_sel   <= PAYLOAD_SEL;

                        else
                            -- assuming tready is '1' because the fifo load is checked before.
                            header_buf_if_m.tdata  <= frame_header_ram(cnt);
                            header_buf_if_m.tvalid <= '1';
                            header_buf_if_m.tlast  <= '0';
                            cnt                  <= cnt + 1;
                            if cnt = 0 then
                                header_buf_if_m.tstart <= '1';
                            end if;
                        end if;

                    when PUSH_PAYLOAD =>

                        fsm_state <= PUSH_PAYLOAD;
                        -- Switch MUX to payload buffer
                        mux_sel   <= PAYLOAD_SEL;
                        -- Detect LAST, disable MUX and go to START
                        if payload_buf_if_m.tvalid = '1' and payload_buf_if_m.tlast = '1' and payload_buf_if_s.tready = '1' then
                            mux_sel   <= NO_SEL;
                            fsm_state <= START;
                        end if;

                    when others =>
                        fsm_state <= START;
                end case;
            end if;
        end if;
    end process;

    -- Combinational logic to define MUX:

    mux_out_if_m.tdata <= payload_buf_if_m.tdata when mux_sel = PAYLOAD_SEL else
                        header_buf_if_m.tdata when mux_sel = HEADER_SEL else
                        x"00";

    mux_out_if_m.tvalid <= payload_buf_if_m.tvalid when mux_sel = PAYLOAD_SEL else
                         header_buf_if_m.tvalid when mux_sel = HEADER_SEL else
                         '0';

    mux_out_if_m.tstart <= header_buf_if_m.tstart when mux_sel = HEADER_SEL else
                         '0';

    mux_out_if_m.tlast <= payload_buf_if_m.tlast when mux_sel = PAYLOAD_SEL else
                        header_buf_if_m.tlast when mux_sel = HEADER_SEL else
                        '0';

    mux_out_if_m.tuser <= '0';

    payload_buf_if_s.tready <= mux_out_if_s.tready when mux_sel = PAYLOAD_SEL else
                             '0';

    header_buf_if_s.tready <= mux_out_if_s.tready when mux_sel = HEADER_SEL else
                            '0';

end architecture RTL;
