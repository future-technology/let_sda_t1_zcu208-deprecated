-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: timestamp_association
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2022.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library share_let_1g;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

entity timestamp_association is
    port(
        clk             : in    std_logic;
        reset           : in    std_logic;

        frame_ts_valid  : in std_logic;
        frame_ts_if     : in t_frame_ts_if;
        axis_if_in_m    : in t_axis_if_m; -- Input axi-s
        axis_if_in_s    : in t_axis_if_s; -- Input axi-s

        ts_association_if : out t_ts_association_if;
        ts_association_valid : out std_logic
    );
end entity timestamp_association;

architecture RTL of timestamp_association is
    type fsm_type is (SEEK_START, GET_TX_FN,REGISTER_TS);
    signal fsm_state : fsm_type;

    signal tx_fn_int : std_logic_vector(16-1 downto 0);

begin

    -- ASYNC

    process(clk) is
        variable frame_header_ram : frame_header_ram_t;
        variable v_offset_byte_cnt : natural;

    begin
        if rising_edge(clk) then
            if reset = '1' then
                fsm_state <= SEEK_START;
                tx_fn_int <= (others=>'0');
                ts_association_if.fn <= (others=>'0');
                ts_association_if.ts    <= (others=>'0');
                ts_association_valid    <= '0';
            else
                --default:
                ts_association_valid    <= '0';

                case fsm_state is
                    when SEEK_START =>
                        fsm_state       <= SEEK_START;
                        if axis_if_in_m.tstart = '1' and axis_if_in_m.tvalid = '1' and axis_if_in_s.tready = '1' then
                            fsm_state <= GET_TX_FN;
                            tx_fn_int(8-1 downto 0) <= axis_if_in_m.tdata;
                        end if;

                    when GET_TX_FN =>
                        if axis_if_in_m.tvalid = '1' and axis_if_in_s.tready = '1' then
                            fsm_state <= REGISTER_TS;
                            tx_fn_int(16-1 downto 8) <= axis_if_in_m.tdata;
                        end if;

                    when REGISTER_TS =>
                        ts_association_if.fn <= tx_fn_int;
                        ts_association_if.ts    <= frame_ts_if.ts;
                        ts_association_valid    <= '1';
                        fsm_state <= SEEK_START;

                    when others =>
                        fsm_state <= SEEK_START;
                end case;
            end if;
        end if;
    end process;



end architecture RTL;
