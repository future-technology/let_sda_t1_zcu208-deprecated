-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: timestamp_insertion
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2022.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library share_let_1g;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

entity timestamp_insertion is
    generic(
        G_INSERTION_BYTE_OFFSET : natural := 7;
        G_INSERTION_BIT_OFFSET : natural := 1
    );
    port(
        clk             : in    std_logic;
        reset           : in    std_logic;

        ts_insertion_if : in t_ts_insertion_if; -- Ts parameters: tod_seconds and ts applies
        ts_valid        : in std_logic;
        
        --frame_ts_valid  : in std_logic;
        --frame_ts_if     : in t_frame_ts_if; -- Just ts comming from the correlator

        axis_if_in_m         : in t_axis_if_m; -- Input axi-s
        axis_if_in_s         : out t_axis_if_s; -- Input axi-s

        axis_if_out_m         : out t_axis_if_m; -- Output axi-s
        axis_if_out_s         : in t_axis_if_s;  -- Output axi-s
        ts_association_if       : out t_ts_association_if;
        ts_association_valid    : out std_logic
    );
end entity timestamp_insertion;

architecture RTL of timestamp_insertion is
    type fsm_type is (SEEK_START, SEEK_TIMESTAMP, GET_TX_FN, REGISTER_TS, INSERT_TIMESTAMP,INSERT_TIMESTAMP_FIRST_BYTE, INSERT_TIMESTAMP_LAST_BYTE);
    signal fsm_state : fsm_type;

    constant C_INSERTION_BYTE_OFFSET : unsigned(8-1 downto 0) := TO_UNSIGNED(G_INSERTION_BYTE_OFFSET, 8);
    constant C_INSERTION_BIT_OFFSET : unsigned(8-1 downto 0)  := TO_UNSIGNED(G_INSERTION_BIT_OFFSET , 8);

    constant C_INSERTION_LENGTH : natural := 7;
    signal offset_insertion_cnt : natural := 0;
    signal offset_byte_cnt : unsigned(8-1 downto 0);
    signal offset_bit_cnt : unsigned(8-1 downto 0);

    constant C_FIRST_BYTE_MASK : std_logic_vector(8-1 downto 0) := "11111110";
    constant C_LAST_BYTE_MASK : std_logic_vector(8-1 downto 0) := "00000011";
    signal bit_mux : std_logic_vector(8-1 downto 0);
    signal mux_data_in : std_logic_vector(8-1 downto 0);
    signal mux_data_last_in : std_logic_vector(8-1 downto 0);
    signal insertion_data : std_logic_vector(8-1 downto 0);

    type mux_sel_type is (BYPASS, MUX_INJECTION, FULL_INJECTION,MUX_INJECTION_LAST);
    signal mux_sel : mux_sel_type;
    signal ts_data_array : t_timestamp_data_array;

    signal r_ts_insertion : t_ts_insertion_if;
    signal tx_fn_int : std_logic_vector(16-1 downto 0);
begin

    -- ASYNC
    axis_if_out_m.tvalid <= axis_if_in_m.tvalid ;
    axis_if_out_m.tstart <= axis_if_in_m.tstart ;
    axis_if_out_m.tlast  <= axis_if_in_m.tlast  ;
    axis_if_out_m.tuser  <= axis_if_in_m.tuser  ;
    axis_if_in_s.tready  <= axis_if_out_s.tready;

    axis_if_out_m.tdata <= axis_if_in_m.tdata when mux_sel = BYPASS else
                           mux_data_in when mux_sel = MUX_INJECTION else
                           mux_data_last_in when mux_sel = MUX_INJECTION_LAST else
                           insertion_data when mux_sel = FULL_INJECTION else
                           axis_if_in_m.tdata;

    mux_data_in <= (axis_if_in_m.tdata and (not C_FIRST_BYTE_MASK)) or (ts_data_array(0) and C_FIRST_BYTE_MASK);
    mux_data_last_in <= (axis_if_in_m.tdata and (not C_LAST_BYTE_MASK)) or (ts_data_array(6) and C_LAST_BYTE_MASK);

    -- converts the interface to 8bit data array.
    ts_data_array <= ts_insertion_if2array(r_ts_insertion);

    process(clk) is
        variable frame_header_ram : frame_header_ram_t;
        variable v_offset_byte_cnt : natural;

    begin
        if rising_edge(clk) then
            if reset = '1' then
                fsm_state       <= SEEK_START;
                mux_sel         <= BYPASS;
                offset_insertion_cnt <= 0;
                offset_byte_cnt <= (others=>'0');
                offset_bit_cnt  <= (others=>'0');
                insertion_data  <= (others=>'0');
                v_offset_byte_cnt := 0;
                ts_association_if.fn <= (others=>'0');
                ts_association_if.ts <= (others=>'0');
                ts_association_valid <= '0';
                tx_fn_int <= (others=>'0');
            else
                ts_association_valid <= '0';
                offset_byte_cnt <= to_unsigned(v_offset_byte_cnt, 8);
                if ts_valid = '1' then
                    r_ts_insertion <= ts_insertion_if;
                end if;

                case fsm_state is
                    when SEEK_START =>
                        offset_byte_cnt <= (others=>'0');
                        offset_bit_cnt  <= (others=>'0');
                        offset_insertion_cnt <= 0;
                        fsm_state       <= SEEK_START;
                        mux_sel         <= BYPASS;
                        if axis_if_in_m.tstart = '1' and axis_if_in_m.tvalid = '1' and axis_if_in_s.tready = '1' then
                            fsm_state         <= GET_TX_FN;
                            offset_byte_cnt   <= offset_byte_cnt + 1;
                            tx_fn_int(8-1 downto 0) <= axis_if_in_m.tdata;
                        end if;

                    when GET_TX_FN =>
                        if axis_if_in_m.tvalid = '1' and axis_if_in_s.tready = '1' then
                            fsm_state <= REGISTER_TS;
                            tx_fn_int(16-1 downto 8) <= axis_if_in_m.tdata;
                            offset_byte_cnt   <= offset_byte_cnt + 1;
                        end if;

                    when REGISTER_TS =>
                        if axis_if_in_m.tvalid = '1' and axis_if_in_s.tready = '1' then
                            ts_association_if.fn <= tx_fn_int;
                            ts_association_if.ts <= ts_insertion_if.tx_ts;
                            ts_association_valid <= '1';
                            fsm_state <= SEEK_TIMESTAMP;
                            offset_byte_cnt   <= offset_byte_cnt + 1;
                        end if;

                    when SEEK_TIMESTAMP =>
                        fsm_state <= SEEK_TIMESTAMP;
                        mux_sel   <= BYPASS;
                        if axis_if_in_m.tvalid = '1' and axis_if_in_s.tready = '1' then
                            offset_byte_cnt   <= offset_byte_cnt + 1;
                            if offset_byte_cnt = C_INSERTION_BYTE_OFFSET-1 then
                                fsm_state         <= INSERT_TIMESTAMP_FIRST_BYTE;
                                mux_sel         <= MUX_INJECTION;
                                offset_insertion_cnt   <= offset_insertion_cnt + 1;
                            end if;
                        end if;

                    when INSERT_TIMESTAMP_FIRST_BYTE =>
                        if axis_if_in_m.tvalid = '1' and axis_if_in_s.tready = '1' then
                            fsm_state <= INSERT_TIMESTAMP;
                            mux_sel   <= FULL_INJECTION;
                            insertion_data <= ts_data_array(offset_insertion_cnt);
                            offset_byte_cnt   <= offset_byte_cnt + 1;
                            offset_insertion_cnt   <= offset_insertion_cnt + 1;
                        end if;

                    when INSERT_TIMESTAMP =>
                        fsm_state <= INSERT_TIMESTAMP;
                        if axis_if_in_m.tvalid = '1' and axis_if_in_s.tready = '1' then

                            if offset_insertion_cnt = C_INSERTION_LENGTH-1 then
                                offset_insertion_cnt <= 0;
                                offset_byte_cnt   <= offset_byte_cnt + 1;
                                offset_insertion_cnt   <= offset_insertion_cnt + 1;
                                fsm_state <= INSERT_TIMESTAMP_LAST_BYTE;
                                mux_sel   <= MUX_INJECTION_LAST;
                                
                            else
                                mux_sel   <= FULL_INJECTION;
                                insertion_data <= ts_data_array(offset_insertion_cnt);
                                offset_byte_cnt   <= offset_byte_cnt + 1;
                                offset_insertion_cnt   <= offset_insertion_cnt + 1;                                
                            end if;
                        end if;
                    
                    when INSERT_TIMESTAMP_LAST_BYTE =>
                        if axis_if_in_m.tvalid = '1' and axis_if_in_s.tready = '1' then
                            fsm_state <= SEEK_START;
                            mux_sel   <= BYPASS;
                            offset_insertion_cnt <= 0;
                        end if;
                    when others =>
                        fsm_state <= SEEK_START;
                end case;
            end if;
        end if;
    end process;



end architecture RTL;
