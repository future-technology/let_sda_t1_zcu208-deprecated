----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 19/05/2021 11:40:33 AM
-- Design Name: 
-- Module Name: frame_timing_detector - Behavioral
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2018.3
-- Description: Calculates the timestamp of every frame by detecting the preamble.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
--          0.02 - modifications for T1 
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_misc.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use ieee.std_logic_misc.all;
--library frame_decoding;
library share_let_1g;
use share_let_1g.pkg_support.all;
--use frame_decoding.pkg_types.all;


entity frame_timing_detector is
    Port ( 
        clk            : in  std_logic;
        rst            : in  std_logic;
        clk_ref 	   : in  std_logic;
        rst_ref 	   : in  std_logic;
        
        -- Input data from FSO PHY
        ----------------------
        s_axis_data    	 : in  std_logic_vector(32 - 1 downto 0);
        s_axis_valid     : in  std_logic;

        reset_timer      : in std_logic;
        reset_timer_ref  : in std_logic;

        -- Output data 
        ----------------------
		preamble_valid   : out std_logic;
        timestamp        : out std_logic_vector(40-1 downto 0);
        timestamp_ref    : out std_logic_vector(40-1 downto 0);
        timestamp_valid  : out std_logic
        );
end frame_timing_detector;

architecture Behavioral of frame_timing_detector is

    constant PREAMBLE_IN : std_logic_vector(63 downto 0) := X"53225B1D0D73DF03";
	constant PREMABLE_MAX_CORR    	: natural := 58;  -- we can meke this comandable later 
	--constant C_CLK_PERIOD_NS	    : natural := 20;
	--constant C_CLK_PERIOD_FS	    : natural := C_CLK_PERIOD_NS*1000;
	--constant C_CLK_PERIOD_FS	    : natural := 13333; -- -13.333 ns
	--constant C_CLK_SCALE			    : natural := 16; --10000; -- Clock cycle scale up. 
	--constant C_BIT_SCALE			    : natural := 625; -- BIT scale (1/16 * C_CLK_SCALE) 
	
	type trec_stat_preamble_sync is record
		new_search_counter : unsigned(32 - 1 downto 0);
		sync_loss_counter  : unsigned(32 - 1 downto 0);
		sync_locked        : std_logic;
	end record;

	signal s_axis_txdata_int  : std_logic_vector(32 - 1 downto 0);
	signal r_axis_data  : std_logic_vector(32 - 1 downto 0);
	type t_input_vector is array (0 to 31) of std_logic_vector(32-1 downto 0);
	signal axis_data_vector  : t_input_vector;
	signal valid_vector : std_logic_vector(7 downto 0);

	type t_stage_0_correlation is array (0 to 8) of unsigned(3 downto 0);
	type t_stage_0_correlation_array is array (0 to 7) of t_stage_0_correlation;
	signal stage_0_corr_arr : t_stage_0_correlation_array;
	type t_stage_1_correlation is array (0 to 3) of unsigned(5 downto 0);
	type t_stage_1_correlation_array is array (0 to 7) of t_stage_1_correlation;
	signal stage_1_corr_arr : t_stage_1_correlation_array;

	type t_pre_corr_0 is array (0 to 31) of  t_stage_0_correlation;
	signal pre_corr_0     : t_pre_corr_0;
	type t_pre_corr_1 is array (0 to 31) of  t_stage_1_correlation;
	signal pre_corr_1     : t_pre_corr_1;

	type   t_pre_corr 		is array (0 to 31) of  unsigned(6 downto 0);
	signal pre_corr      	: t_pre_corr;
	signal sync_input_data  : std_logic_vector(16-1 downto 0);
	signal sync_input_valid : std_logic;
	signal sync_input_start : std_logic;
	signal sync_input_last  : std_logic;
	signal pre_corr_detected : std_logic_vector(31 downto 0);
	signal pre_corr_detected_index : unsigned(31 downto 0);
	signal timer                   : unsigned(39 downto 0);
	signal timer_ref : unsigned(39 downto 0);
	signal r_timer : unsigned(39 downto 0);
	--signal timer_debug : unsigned(35 downto 0);
	signal pre_time_stamp 			: unsigned(39 downto 0);
	signal pre_time_stamp_ref 		: unsigned(39 downto 0);
	signal pre_bit_time_stamp_ps 	: unsigned(39 downto 0);
	signal pre_time_stamp_ps 		: unsigned(39 downto 0);
	signal pre_time_stamp_ps_ref 	: unsigned(39 downto 0);
	signal bit_time_stamp_ps 		: unsigned(39 downto 0);
	signal time_stamp_ps 			: unsigned(39 downto 0);
	signal time_stamp_ps_ref 		: unsigned(39 downto 0);
	signal time_stamp_ps_valid      : std_logic;

--	constant sync_cfg : trec_cfg_preamble_sync := (aquisition_length  => to_unsigned(32, 32),
--										tracking_length    => to_unsigned(64, 32),
--										max_tracking_error => to_unsigned(16, 32));
	signal sync_stat : trec_stat_preamble_sync;

	signal preamble_valid_vec : std_logic_vector(5 downto 0);

    signal input_sreg     : std_logic_vector(64 + 31 downto 0);

	type t_time_adjustments is array (0 to 31) of natural;
	signal time_adj_vector : t_time_adjustments;
    
begin

	g_GENERATE_FOR: for ii in 0 to 31 generate
		time_adj_vector(ii) <= ii*400;
  	end generate g_GENERATE_FOR;

	-- Xilinx Gigabit Tranceiver send the LSB first
	pr_data : process(s_axis_data) is
		begin
			for i in 0 to 31 loop
				s_axis_txdata_int(i) <= s_axis_data(i);
			end loop;
		end process;

	process(clk) is
		variable v_input_vec : t_input_vector;
		type t_v_input_data is array(31 downto 0) of std_logic_vector(63 downto 0);
		variable v_input_data : t_v_input_data;
		variable v_tmp_byte : std_logic_vector(7 downto 0);
		variable slip_index : integer range 0 to 39;
	begin

		if rst = '1' then
			timer 					<= (others =>'0');
			--timer_debug 			<= (others =>'0');
			valid_vector 			<= (others =>'0');
			
			pre_corr_detected	 	<= (others =>'0');
			pre_corr_detected_index	<= (others =>'0');
			pre_time_stamp	 		<= (others =>'0');
			pre_time_stamp_ref 		<= (others =>'0');
			bit_time_stamp_ps	 	<= (others =>'0');
			time_stamp_ps	 		<= (others =>'0');
			time_stamp_ps_ref 		<= (others =>'0');
			time_stamp_ps_valid	 	<= '0';
	
			--pre_corr_detected 	<= (others=>(others=>'0'));
			axis_data_vector 		<= (others=>(others=>'0'));
			--frame_preamble			<= (others=>(others=>'0'));
			pre_corr_0				<= (others=>(others=>(others=>'0')));
			pre_corr_1				<= (others=>(others=>(others=>'0')));
			pre_corr	 			<= (others=>(others=>'0'));
			input_sreg              <= (others => '0');
		else
			if rising_edge(clk) then
				
				if reset_timer = '1' then
					timer <= (others =>'0');
				else
					timer <= timer + 12800; --clk=78.125Mhz or 12800ps
				end if;

				valid_vector(valid_vector'high downto 1) <= valid_vector(valid_vector'high-1 downto 0);
				valid_vector(0) <= '0';
				time_stamp_ps_valid <= '0';
				
				if s_axis_valid = '1' then 

					input_sreg(31 downto 0)  <= s_axis_txdata_int;
					input_sreg(64  + 31 downto 32) <= input_sreg(64 - 1 downto 0);
					
					for k in 0 to 31 loop 			
						v_input_data(k) := input_sreg(64+31-k downto 32 - k ) xor PREAMBLE_IN;
						for i in 0 to 8-1 loop -- bytes in preamble
							v_tmp_byte := v_input_data(k)(8 * (i + 1) - 1 downto 8 * i);
							pre_corr_0(k)(i) <= calc_zeros(v_tmp_byte);
						end loop;
					end loop;
		
					for k in 0 to 31 loop
						pre_corr_1(k)(0) <= ("00" & pre_corr_0(k)(0)) + ("00" & pre_corr_0(k)(1)) + 
						                    ("00" & pre_corr_0(k)(2)) + ("00" & pre_corr_0(k)(3));
						pre_corr_1(k)(1) <= ("00" & pre_corr_0(k)(4)) + ("00" & pre_corr_0(k)(5)) + 
						                    ("00" & pre_corr_0(k)(6)) + ("00" & pre_corr_0(k)(7));
						pre_corr_1(k)(2) <= ("00" & pre_corr_0(k)(8));
					end loop;
		
					for k in 0 to 31 loop
						pre_corr(k) <= ('0' & pre_corr_1(k)(0)) + ('0' & pre_corr_1(k)(1)) + ('0' & pre_corr_1(k)(2));
					end loop;
					
					for k in 0 to 31 loop
						if pre_corr(k) >= to_unsigned(integer(PREMABLE_MAX_CORR), 7) then 
							pre_corr_detected(k) <= '1';
							pre_corr_detected_index <= to_unsigned(integer(k), 32);
							pre_time_stamp_ref <= timer; --timer_ref; --TBD!!!
							valid_vector(0)  	<= '1';
						else
							pre_corr_detected(k) <= '0';
						end if;
					end loop;
					--end if;
				end if; --end if valid

				if valid_vector(1) = '1' then
					pre_time_stamp_ps_ref 	<=  pre_time_stamp_ref-76800; --adjust delay first bit of preamble
					slip_index := to_integer(unsigned(pre_corr_detected_index));
					pre_bit_time_stamp_ps <=  to_unsigned(time_adj_vector(slip_index), 40);

				end if;
				if valid_vector(2) = '1' then
					time_stamp_ps_ref 	<= pre_time_stamp_ps_ref + pre_bit_time_stamp_ps;
				end if;
				if valid_vector(3) = '1' then
					time_stamp_ps 		<= pre_time_stamp_ps_ref;

					time_stamp_ps_valid <= '1';
					valid_vector 		<= (others =>'0');
				end if;			
			end if;
		end if;

	end process;

	preamble_valid 		<= or_reduce(pre_corr_detected);
	timestamp 			<= std_logic_vector(time_stamp_ps(40-1 downto 0));
	timestamp_ref		<= std_logic_vector(time_stamp_ps_ref(40-1 downto 0));
	timestamp_valid 	<= time_stamp_ps_valid; 


process(clk_ref) is 
begin

	if rising_edge(clk_ref) then

		if rst_ref = '1' then
			timer_ref <= (others =>'0');
		else
			if reset_timer = '1' or reset_timer_ref = '1' then
				timer_ref <= (others =>'0');
			else
				timer_ref <= timer_ref + 1;

			end if;
		end if;
	end if;

end process;

end Behavioral;