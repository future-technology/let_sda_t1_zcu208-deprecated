----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 19/05/2021 11:40:33 AM
-- Design Name: 
-- Module Name: frame_timing_detector - Behavioral
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2018.3
-- Description: Calculates the timestamp of every frame by detecting the preamble.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
library ranging_lib;
use ranging_lib.pkg_components.all;
library share_let_1g;
use share_let_1g.pkg_components.all;
library let_sda_lib;

entity frame_timing_detector_top is
    Port ( 
        clk_let           : in  std_logic;
        rst_let           : in  std_logic;
        clk_tx            : in  std_logic;
        clk_rx            : in  std_logic;
        rst_tx            : in  std_logic;
        rst_rx            : in  std_logic;

        -- Input data from FSO PHY
        ----------------------
        s_axis_data_tx     : in  std_logic_vector(32 - 1 downto 0);
        s_axis_valid_tx    : in  std_logic;
		s_axis_data_rx     : in  std_logic_vector(32 - 1 downto 0);
		s_axis_valid_rx    : in  std_logic;

        reset_timer         : in std_logic;

        -- Output data 
        ----------------------
        let_timestamp_tx        : out std_logic_vector(40-1 downto 0);
        let_timestamp_valid_tx  : out std_logic;
        let_timestamp_rd_en_tx  : in std_logic;
        let_timestamp_rx        : out std_logic_vector(40-1 downto 0);
        let_timestamp_valid_rx  : out std_logic;
        let_timestamp_rd_en_rx  : in std_logic
    );
end frame_timing_detector_top;

architecture Behavioral of frame_timing_detector_top is

    component frame_timing_detector is
    Port ( 
        clk            : in  std_logic;
        rst            : in  std_logic;
        clk_ref 	   : in  std_logic;
        rst_ref 	   : in  std_logic;
        
        -- Input data from FSO PHY
        ----------------------
        s_axis_data    	 : in  std_logic_vector(32 - 1 downto 0);
        s_axis_valid     : in  std_logic;

        reset_timer      : in std_logic;
        reset_timer_ref  : in std_logic;

        -- Output data 
        ----------------------
		preamble_valid   : out std_logic;
        timestamp        : out std_logic_vector(40-1 downto 0);
        timestamp_ref    : out std_logic_vector(40-1 downto 0);
        timestamp_valid  : out std_logic
        );
    end component;


--	signal timestamp_tx        : std_logic_vector(31 downto 0);
signal preamble_valid_tx  : std_logic;
--	signal timestamp_rx        : std_logic_vector(31 downto 0);
signal preamble_valid_rx  : std_logic;

signal timestamp_tx        : std_logic_vector(40-1 downto 0);
signal timestamp_ref_tx    : std_logic_vector(40-1 downto 0);
signal timestamp_valid_tx  : std_logic;
signal timestamp_rx        : std_logic_vector(40-1 downto 0);
signal timestamp_ref_rx    : std_logic_vector(40-1 downto 0);
signal timestamp_valid_rx  : std_logic;
signal let_timestamp_valid_rx_int  : std_logic;
--signal counter_delay       : unsigned(31 downto 0);

constant C_COUNTER_DELAY   : integer := 4154;
signal counter_delay       : integer range 0 to 5000;
signal reset_timer_tx      : std_logic;
signal reset_timer_rx      : std_logic;

begin

    let_timestamp_valid_rx <= let_timestamp_valid_rx_int;

	inst_frame_timing_detector_tx : frame_timing_detector
	port map(
		clk         		=> clk_tx,--clk_tx,
		rst         		=> (rst_tx or rst_rx),
        clk_ref 	        => clk_tx,
        rst_ref 	        => (rst_tx or rst_rx),
		s_axis_data  		=> s_axis_data_tx,
		s_axis_valid  		=> s_axis_valid_tx,
        reset_timer         => reset_timer_tx,
        reset_timer_ref     => reset_timer_tx,
		preamble_valid 		=> preamble_valid_tx,
		timestamp      		=> timestamp_tx,
		timestamp_ref       => timestamp_ref_tx,
		timestamp_valid    	=> timestamp_valid_tx
	);

	inst_frame_timing_detector_rx : frame_timing_detector
	port map(
		clk         		=> clk_rx,--clk_rx,
		rst         		=> (rst_tx or rst_rx),
        clk_ref 	        => clk_tx,
        rst_ref 	        => (rst_tx or rst_rx),
		s_axis_data  		=> s_axis_data_rx,
		s_axis_valid  		=> s_axis_valid_rx,
        reset_timer         => reset_timer_rx,
        reset_timer_ref     => reset_timer_tx,
		preamble_valid 		=> preamble_valid_rx,
		timestamp           => timestamp_rx,
		timestamp_ref       => timestamp_ref_rx,
		timestamp_valid    	=> timestamp_valid_rx
	);
    
--    frame_timing_detector_rx_debug : ila_axis_8bit
--        PORT MAP (
--        	clk       => clk_rx,
--        	probe0    => s_axis_data_rx, 
--        	probe1(0) => preamble_valid_rx, 
--        	probe2(0) => s_axis_valid_rx, 
--        	probe3(0) => preamble_valid_tx,
--        	probe4(0) => '0'
--        );

    fifo_timestamp_tx : fifo_timestamp
    PORT MAP (
        rst         => (rst_tx or rst_rx), --rst_tx,
        wr_clk      => clk_tx,
        rd_clk      => clk_let,
        din         => timestamp_tx,
        wr_en       => timestamp_valid_tx,
        rd_en       => let_timestamp_rd_en_tx,
        dout        => let_timestamp_tx,
        full        => open,
        empty       => open,
        valid       => let_timestamp_valid_tx
    );

    fifo_timestamp_rx : fifo_timestamp
    PORT MAP (
        rst         => (rst_tx or rst_rx), --rst_rx,
        wr_clk      => clk_rx,
        rd_clk      => clk_let,
        din         => timestamp_rx,
        wr_en       => timestamp_valid_rx,
        rd_en       => let_timestamp_rd_en_rx,
        dout        => let_timestamp_rx,
        full        => open,
        empty       => open,
        valid       => let_timestamp_valid_rx_int
    );

    Inst_bit_sync : bit_sync
    port map(
        clk_in   => clk_tx,
        data_in  => reset_timer,
        data_out => reset_timer_tx
    );

    Inst_bit_sync_rx : bit_sync
    port map(
        clk_in   => clk_rx,
        data_in  => reset_timer,
        data_out => reset_timer_rx
    );

--process(clk_let)
--begin
--
--    if rising_edge(clk_let) then
--        if rst_let = '1' then
--            counter_delay <= (others=>'0');
--        else
--            counter_delay <= counter_delay + 1;
--            if counter_delay = C_COUNTER_DELAY then 
--            
--            
--            end if;
----            if let_timestamp_valid_rx_int = '1' then
----                counter_delay <= (others=>'0');
----            end if;
--        end if;
--    end if;

--end process;


end Behavioral;