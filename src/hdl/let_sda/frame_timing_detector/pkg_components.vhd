library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library share_let_1g;
use share_let_1g.pkg_support.all;
use share_let_1g.pkg_param.all;
--use share_let_1g.pkg_types.all;

library sda_lib;
use sda_lib.pkg_sda.all;

package pkg_components is

	component frame_timing_detector_top is
		Port ( 
			clk_let           : in  std_logic;
			rst_let           : in  std_logic;
			clk_tx            : in  std_logic;
			clk_rx            : in  std_logic;
			rst_tx            : in  std_logic;
			rst_rx            : in  std_logic;
	
			-- Input data from FSO PHY
			----------------------
			s_axis_data_tx     : in  std_logic_vector(32 - 1 downto 0);
			s_axis_valid_tx    : in  std_logic;
			s_axis_data_rx     : in  std_logic_vector(32 - 1 downto 0);
			s_axis_valid_rx    : in  std_logic;
			reset_timer 	   : in  std_logic;
	
			-- Output data 
			----------------------
			let_timestamp_tx        : out std_logic_vector(40-1 downto 0);
			let_timestamp_valid_tx  : out std_logic;
			let_timestamp_rx        : out std_logic_vector(40-1 downto 0);
			let_timestamp_valid_rx  : out std_logic
			);
	end component;

	component frame_timing_detector is
		Port ( 
			clk            : in  std_logic;
			rst            : in  std_logic;
			clk_ref 	   : in  std_logic;
			rst_ref 	   : in  std_logic;
			  
			-- Input data from FSO PHY
			----------------------
			s_axis_data    	 : in  std_logic_vector(32 - 1 downto 0);
			s_axis_valid     : in  std_logic;
			reset_timer 	 : in  std_logic;
			reset_timer_ref  : in  std_logic;
			-- Output data 
			----------------------
			preamble_valid   : out std_logic;
			timestamp        : out std_logic_vector(40-1 downto 0);
			timestamp_ref    : out std_logic_vector(40-1 downto 0);
			timestamp_valid  : out std_logic
			);
	end component;

	COMPONENT fifo_timestamp
	PORT (
	  rst : IN STD_LOGIC;
	  wr_clk : IN STD_LOGIC;
	  rd_clk : IN STD_LOGIC;
	  din : IN STD_LOGIC_VECTOR(39 DOWNTO 0);
	  wr_en : IN STD_LOGIC;
	  rd_en : IN STD_LOGIC;
	  dout : OUT STD_LOGIC_VECTOR(39 DOWNTO 0);
	  full : OUT STD_LOGIC;
	  empty : OUT STD_LOGIC;
	  valid : OUT STD_LOGIC
	);
  END COMPONENT;
  COMPONENT tof_fifo_rx
  PORT (
    clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(98 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(98 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    valid : OUT STD_LOGIC
  );
	END COMPONENT;

	COMPONENT tof_fifo_tx
	PORT (
	clk : IN STD_LOGIC;
	rst : IN STD_LOGIC;
	din : IN STD_LOGIC_VECTOR(58 DOWNTO 0);
	wr_en : IN STD_LOGIC;
	rd_en : IN STD_LOGIC;
	dout : OUT STD_LOGIC_VECTOR(58 DOWNTO 0);
	full : OUT STD_LOGIC;
	empty : OUT STD_LOGIC;
	valid : OUT STD_LOGIC
	);
	END COMPONENT;

  	COMPONENT tof_transponder 
	Port ( 
		clk            : in  std_logic;
		rst            : in  std_logic;

		-- From Framer
		----------------------
		preamble_synced          : in std_logic;

		-- From Framer
		---------------------- 
		tx_timing_data          : in tx_timing_data_t;
		tx_timing_data_valid    : in std_logic;

		-- From deframer
		---------------------- 
		rx_timing_data          : in rx_timing_data_t;
		rx_timing_data_valid    : in std_logic;

		-- Output data: Time of flight in 1/16 clk
		----------------------
		tof_counter_valid   : out std_logic;
		tof_counter         : out std_logic_vector(40-1 downto 0);

		-- Config
		----------------------
		--cfg_mode           : in std_logic;  -- 0: loopback test  1: SDA mode

		-- SDA compliant ranging interface
		----------------------
		trigger            : in std_logic;

		--Framer:
		t1_tx_fn           : in std_logic_vector(15 downto 0);
		t1_tx_fn_valid     : in std_logic;
		send_ranging_req   : out std_logic;
		send_mgmt_response       : out mgmt_payload_t;
		send_mgmt_response_valid : out std_logic;
		send_mgmt_response_ack   : in std_logic;
		send_t3                  : out std_logic;
		t3_tx_fn                : in std_logic_vector(15 downto 0);
		t3_tx_fn_valid          : in std_logic;

		--Deframer:
		t2t3_response       : in mgmt_payload_t;
		t2t3_response_valid : in std_logic
	);
	END COMPONENT;

	component tof_interrogator
		Port ( 
			clk            : in  std_logic;
			rst            : in  std_logic;
	
			-- 
			----------------------
			preamble_synced          : in std_logic;
	
			ram_tx_en_B      : out std_logic;
			ram_tx_addr_B    : out std_logic_vector(no_bits_natural(C_RAM_TX_WORDS - 1) - 1 downto 0);
			ram_tx_data_B    : in std_logic_vector(C_RAM_TX_DATA_BITWIDTH - 1 downto 0);
			ram_tx_busy 	 : in std_logic;

			ram_rx_en_B      : out std_logic;
			ram_rx_addr_B    : out std_logic_vector(no_bits_natural(C_RAM_RX_WORDS - 1) - 1 downto 0);
			ram_rx_data_B    : in std_logic_vector(C_RAM_RX_DATA_BITWIDTH - 1 downto 0);
	
	
			-- Output data: Time of flight in 1/16 clk
			----------------------
			tof_counter_valid   : out std_logic;
			tof_counter         : out std_logic_vector(40-1 downto 0);
	
			-- SDA compliant ranging interface
			----------------------
			trigger            : in std_logic;
			rx_timing_data_valid : in std_logic;

			--Framer:
			t1_tx_fn           : in std_logic_vector(15 downto 0);
			t1_tx_fn_valid     : in std_logic;
			send_ranging_req   : out std_logic;
	
			--Deframer:
			mgmt_response       : in mgmt_payload_t;
			mgmt_response_valid : in std_logic;
	
			-- Debug
			----------------------
			debug_t1_ts               : out std_logic_vector(40-1 downto 0);
			debug_t2_ts               : out std_logic_vector(40-1 downto 0);
			debug_t3_ts               : out std_logic_vector(40-1 downto 0);
			debug_t4_ts               : out std_logic_vector(40-1 downto 0);
			debug_error               : out std_logic;
			debug_valid               : out std_logic
	
		);
	end COMPONENT;

	component tof_responder
		Port ( 
			clk            : in  std_logic;
			rst            : in  std_logic;
	
			-- 
			----------------------
			--preamble_synced          : in std_logic;
	
			ram_tx_en_B      : out std_logic;
			ram_tx_addr_B    : out std_logic_vector(no_bits_natural(C_RAM_TX_WORDS - 1) - 1 downto 0);
			ram_tx_data_B    : in std_logic_vector(C_RAM_TX_DATA_BITWIDTH - 1 downto 0);
			ram_tx_busy      : out std_logic;
			-- SDA compliant ranging interface
			----------------------
			--Framer:
			send_t3                 : out std_logic;
			t3_tx_fn                : in std_logic_vector(15 downto 0);
			t3_tx_fn_valid          : in std_logic;
			send_t2t3_response       : out mgmt_payload_t;
			send_t2t3_response_valid : out std_logic;
			send_t2t3_response_ack : in std_logic;
	
			--Deframer:
			--t2_tx_fn_recv           : in std_logic_vector(15 downto 0);
			--t2_tx_fn_recv_valid     : in std_logic;
			--t3_tx_fn_sent           : in std_logic_vector(15 downto 0);
			--t3_tx_fn_sent_valid     : in std_logic;
			rx_timing_data          : in rx_timing_data_t;
			rx_timing_data_valid    : in std_logic
	
		);
	end component;
	

end package pkg_components;
