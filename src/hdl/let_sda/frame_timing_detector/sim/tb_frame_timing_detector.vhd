----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/25/2021 10:10:45 AM
-- Design Name: 
-- Module Name: tb_frame_timing_detector - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
library ethernet_framer_lib;
use ethernet_framer_lib.pkg_components.all;
library frame_decoding;
library let_sda_lib;
use let_sda_lib.let_pckg.all;
use let_sda_lib.let_pckg_components.all; 

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_frame_timing_detector is
--  Port ( );
end tb_frame_timing_detector;

architecture Behavioral of tb_frame_timing_detector is


component frame_timing_detector
    Port ( 
        clk            : in  std_logic;
        rst            : in  std_logic;
        -- Input data from FSO PHY
        ----------------------
        s_axis_data     : in  std_logic_vector(16 - 1 downto 0);
        s_axis_valid    : in  std_logic;
        -- Output data 
        ----------------------
        timestamp        : out std_logic_vector(31 downto 0);
        timestamp_valid  : out std_logic
        );
end component;

--constant HEADER_PREAMBLE      : std_logic_vector(71 downto 0) := x"77AD_5B58_4364_1E2E_26" ; -- 0x77AD5B5843641E2E26
constant HEADER_PREAMBLE      : std_logic_vector(71 downto 0) := x"26_2E1E_6443_585B_AD77"; -- 0x77AD5B5843641E2E26
--constant HEADER_PREAMBLE      : std_logic_vector(71 downto 0) := x"FF_FFFF_FFFF_FFFF_FFFF" ; -- 0x77AD5B5843641E2E26
--constant HEADER_PREAMBLE      : std_logic_vector(71 downto 0) := x"FFFF_FFFF_FFFF_FFFF_FF" ; -- 0x77AD5B5843641E2E26
--constant HEADER_PREAMBLE      : std_logic_vector(71 downto 0) := x"62_E2E1_4634_85B5_DA77" ; -- 0x77AD5B5843641E2E26
constant PREAMBLE_IN : std_logic_vector(71 downto 0) := X"77AD5B5843641E2E26";
constant BIT_SHIFT : natural := 0;

signal clk            : std_logic := '0';
signal clk_x2         : std_logic := '0';
signal rst            : std_logic := '1';
signal rst_n            : std_logic := '0';
-- Input data from FSO PHY
----------------------
signal s_axis_data     : std_logic_vector(8 - 1 downto 0);
signal s_axis_valid : std_logic;
signal s_axis_data_16b     : std_logic_vector(16 - 1 downto 0);
-- Output data 
----------------------
signal timestamp        : std_logic_vector(31 downto 0);
signal timestamp_valid  : std_logic;

signal m_axis_tdata  : std_logic_vector(8 - 1 downto 0);
signal m_axis_tvalid : std_logic;
signal m_axis_tlast  : std_logic;
signal m_axis_tready : std_logic;
signal fifo8x16_tready : std_logic;

--signal ch_tx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
--signal ch_tx_axis_tvalid   : std_logic;
--signal r_ch_tx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
--signal r_ch_tx_axis_tvalid   : std_logic;
--signal ch_tx_sr_tdata    : std_logic_vector(8 - 1 downto 0);
--signal ch_tx_sr_tvalid   : std_logic;

signal ch_tx_axis_tdata    : std_logic_vector(16 - 1 downto 0);
signal ch_tx_axis_tvalid   : std_logic;
signal r_ch_tx_axis_tdata    : std_logic_vector(16 - 1 downto 0);
signal r_ch_tx_axis_tvalid   : std_logic;
signal ch_tx_sr_tdata    : std_logic_vector(16 - 1 downto 0);
signal ch_tx_sr_tvalid   : std_logic;

signal fso_tx_axis_tdata   : std_logic_vector(8 - 1 downto 0);    -- interface  FEC FEC -> FSO data stream
signal fso_tx_axis_tvalid  : std_logic;
signal fso_tx_fifo_tdata   : std_logic_vector(16 - 1 downto 0);    -- interface  FEC FEC -> FSO data stream
signal fso_tx_fifo_tvalid  : std_logic;
signal r_fso_tx_fifo_tvalid  : std_logic;

constant cfg_aquisition_length  : std_logic_vector(32 - 1 downto 0) := std_logic_vector(to_unsigned(8, 32));
constant cfg_tracking_length    : std_logic_vector(32 - 1 downto 0) := std_logic_vector(to_unsigned(32, 32));
constant cfg_max_tracking_error : std_logic_vector(32 - 1 downto 0) := std_logic_vector(to_unsigned(10, 32));


begin

    clk <= not clk after 10 ns;
	clk_x2 <= not clk_x2 after 5 ns;
    rst_n <= not rst;
    
    NUT: entity ethernet_framer_lib.ethernet_framer_top
    generic map(
        MINIMUN_FREE_BYTES_TO_SPLIT => 64
    )
    port map(
        clk_let        => clk, -- : in  std_logic;
        rst_let        => rst, -- : in  std_logic;
        clk_eth        => clk, -- : in  std_logic;
        rst_eth        => rst, -- : in  std_logic;
        s_axis_tdata  => (others=>'0'),
        s_axis_tvalid => '0',
        s_axis_tlast  => '0', 
        s_axis_tready => open, 
        s_axis_tuser => '0', 
        cfg_pause_data => x"0100",
        tx_pause_valid  => open,
        tx_pause_data   => open,
        rx_fifo_skipped_frame => open,
        m_axis_tdata  => m_axis_tdata,
        m_axis_tvalid => m_axis_tvalid,
        m_axis_tlast  => m_axis_tlast,
        m_axis_tready => m_axis_tready,
            
		-- Statistics
		----------------------            
		clear_stat                      => '0',
        framer_diagnostics               => open
    );

	inst_8_to_16_tx : dwidth_8_to_16 -- for both waves a and b
    port map(
      aclk    => clk,
      aresetn => rst_n, 
  
      s_axis_tdata  => fso_tx_axis_tdata, --ch_tx_sr_tdata,--ch_tx_axis_tdata,
      s_axis_tvalid => fso_tx_axis_tvalid,
      s_axis_tready => open,
  
      m_axis_tdata  => fso_tx_fifo_tdata,
      m_axis_tvalid => fso_tx_fifo_tvalid,
      m_axis_tready => fifo8x16_tready
    );


	FEC_UUT :  entity frame_decoding.frame_fec_top
	port map(
		clk            => clk,
		rst            => rst,

		preamble        => PREAMBLE_IN,

		cfg_aquisition_length  => cfg_aquisition_length,
		cfg_tracking_length    => cfg_tracking_length,
		cfg_max_tracking_error => cfg_max_tracking_error,

		-- TX data input - header and payload data
		----------------------
		tx_axis_tdata      => m_axis_tdata,
		tx_axis_tvalid     => m_axis_tvalid,
		tx_axis_tlast      => m_axis_tlast,
		tx_axis_tready     => m_axis_tready,

		disp_sum           => open,

		-- Output data handling
		-----------------------
		ch_tx_axis_tdata     => s_axis_data,--fso_tx_axis_tdata,
		ch_tx_axis_tvalid    => s_axis_valid, --fso_tx_axis_tvalid,
		ch_tx_axis_tlast     => open,
		ch_tx_axis_tready    => '1',--ch_tx_axis_tready,


		-- RX data input
		----------------------
		ch_rx_axis_tdata      => s_axis_data, --fso_tx_axis_tdata, --ch_rx_axis_tdata_noise,
		ch_rx_axis_tvalid     => s_axis_valid,--fso_tx_axis_tvalid,

		-- Status
		-----------------------
		crc16_valid       => open,
		crc16_correct     => open,
		crc32_valid       => open,
		crc32_correct     => open,

		payload_cerr      => open,
		payload_ncerr     => open,
		payload_err_valid => open,

		header_cerr       => open,
		header_ncerr      => open,
		header_err_valid  => open,

		preamble_synced   => open,

		-- More diagnostic informations
		-------------------------------------------------------
		clear_stat          => '0',
		total_frame_counter => open,
		crc_error_counter   => open,
		crc16_err_counter   => open,
		crc32_err_counter   => open,
		header_corrections  => open,
		payload_corrections => open,
		lpc_frame_counter   => open,
		lpc_corrections     => open,

		-- Stats from the preamble sync
		new_search_counter  => open,
		sync_loss_counter   => open,

		-- RX data output - header and payload data
		-----------------------
		rx_axis_tdata     => open,
		rx_axis_tvalid    => open,
		rx_axis_tlast     => open,
		rx_axis_tready    => '1'
	);

    inst_frame_timing_detector : frame_timing_detector
    Port map( 
        clk            => clk,
        rst            => rst,
        -- Input data from FSO PHY
        ----------------------
        s_axis_data     => ch_tx_sr_tdata, --fso_tx_fifo_tdata, ch_tx_sr_tdata
        s_axis_valid     => r_fso_tx_fifo_tvalid,
        -- Output data 
        ----------------------
        timestamp        => timestamp,
        timestamp_valid  => timestamp_valid
        );

	pr_data : process(s_axis_data) is
		begin
			for i in 0 to 8-1 loop
				fso_tx_axis_tdata(i) <= s_axis_data(7 - i);
				--s_axis_txdata_int(i) <= s_axis_data(i);
			end loop;
		end process;
fso_tx_axis_tvalid <= s_axis_valid;
--fso_tx_axis_tdata <= s_axis_data;
--    process(clk)
--    begin
--        if rising_edge(clk) then
--            if BIT_SHIFT /= 0 then
--                r_ch_tx_axis_tdata  <= ch_tx_axis_tdata;
--                ch_tx_sr_tdata(7-BIT_SHIFT downto 0)  <= ch_tx_axis_tdata(7 downto BIT_SHIFT);
--                ch_tx_sr_tdata(7 downto 7-BIT_SHIFT+1)  <= r_ch_tx_axis_tdata(BIT_SHIFT-1 downto 0);
--            else
--                ch_tx_sr_tdata <= ch_tx_axis_tdata;
--            end if;
--        end if;
--    end process;

    process(clk)
        variable tx_data_16 : std_logic_vector(15 downto 0);
    begin
        if rising_edge(clk) then
            r_fso_tx_fifo_tvalid <= '0';
            if fso_tx_fifo_tvalid = '1' then
                r_fso_tx_fifo_tvalid <= '1';
                if BIT_SHIFT /= 0 then
                	tx_data_16(7 downto 0) := fso_tx_fifo_tdata(15 downto 8);
				    tx_data_16(15 downto 8) := fso_tx_fifo_tdata(7 downto 0); 
                    --r_ch_tx_axis_tdata  <= fso_tx_fifo_tdata;
                    r_ch_tx_axis_tdata  <= tx_data_16;
                    ch_tx_sr_tdata(15-BIT_SHIFT downto 0)  <= tx_data_16(15 downto BIT_SHIFT); --fso_tx_fifo_tdata(15 downto BIT_SHIFT);
                    --tx_data_16(15-BIT_SHIFT downto 0)  := fso_tx_fifo_tdata(15 downto BIT_SHIFT);
                    ch_tx_sr_tdata(15 downto 15-BIT_SHIFT+1)  <= r_ch_tx_axis_tdata(BIT_SHIFT-1 downto 0);
                    --tx_data_16(15 downto 15-BIT_SHIFT+1)  := r_ch_tx_axis_tdata(BIT_SHIFT-1 downto 0);
				    --ch_tx_sr_tdata(7 downto 0) <= tx_data_16(15 downto 8);
				    --ch_tx_sr_tdata(15 downto 8) <= tx_data_16(7 downto 0);     
                else
                    --ch_tx_sr_tdata <= fso_tx_fifo_tdata;
                    ch_tx_sr_tdata(7 downto 0) <= fso_tx_fifo_tdata(15 downto 8);
				    ch_tx_sr_tdata(15 downto 8) <= fso_tx_fifo_tdata(7 downto 0);
                    --tx_data_16 := fso_tx_fifo_tdata;
                end if;
            end if;
        end if;
    end process;

    process
    begin
        rst <= '1';
        fifo8x16_tready <= '1';
        wait for 300 ns;
        rst <= '0';
        wait for 300 ns;
        --wait for 200 us;
        --fifo8x16_tready <= '1';
        wait;


--        for test in 1 to 20000  loop
--            for ii in 1 to 1922 loop
--                wait until rising_edge(clk);
--                --s_axis_data <= std_logic_vector(to_unsigned(integer(ii)/10, 8));
--                s_axis_data <= x"00";
--                s_axis_data_16b <= x"0000";
--            end loop;

--            for ii in 1 to 9 loop
--                wait until rising_edge(clk);
--                s_axis_data <= HEADER_PREAMBLE((ii*8)-1 downto (ii-1)*8);

                
--            end loop;

----            for ii in 0 to 4 loop
----                wait until rising_edge(clk);
----                if (ii < 4) then
----                    s_axis_data_16b <= HEADER_PREAMBLE((ii*8)-1 downto ii*8);
----                else
----                    s_axis_data_16b <= HEADER_PREAMBLE(71 downto 71-7) & x"00";
----                end if;
----                
----            end loop;
--        end loop;
        wait;

    end process;

end Behavioral;




