----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 08/02/2021 10:10:45 AM
-- Design Name: 
-- Module Name: tb_tof_transponder_interrogator - Behavioral
-- Project Name: Condor Mark II
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
library ethernet_framer_lib;
use ethernet_framer_lib.pkg_components.all;
library frame_decoding;
library let_sda_lib;
use let_sda_lib.let_pckg.all;
use let_sda_lib.let_pckg_components.all; 
library sda_lib;
use sda_lib.pkg_sda.all;
library ranging_lib;
use ranging_lib.pkg_components.all;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_tof_transponder_interrogator is
--  Port ( );
end tb_tof_transponder_interrogator;

architecture Behavioral of tb_tof_transponder_interrogator is

 
    signal clk            :   std_logic := '0';
    signal rst            :   std_logic := '1';

        -- From Framer
        ----------------------
    signal preamble_synced          :  std_logic;

    -- From Framer
    ---------------------- 
    signal tx_timing_data          :  tx_timing_data_t := C_TX_TIMING_DATA_INIT;
    signal tx_timing_data_valid    :  std_logic := '0';

    -- From deframer
    ---------------------- 
    signal rx_timing_data          :  rx_timing_data_t := C_RX_TIMING_DATA_INIT;
    signal rx_timing_data_valid    :  std_logic := '0';

    signal rx_timing_data_delay          :  rx_timing_data_t := C_RX_TIMING_DATA_INIT;
    signal rx_timing_data_valid_delay    :  std_logic := '0';

    -- Output data: Time of flight in 1/16 clk
    ----------------------
    signal tof_counter_valid   :  std_logic;
    signal tof_counter         :  std_logic_vector(40-1 downto 0);

    -- Config
    ----------------------
    signal cfg_mode           :  std_logic;  -- 0: loopback test  1: SDA mode

    -- SDA compliant ranging interface
    ----------------------
    signal trigger            :  std_logic;

    --Framer:
    signal t1_tx_fn           :  std_logic_vector(15 downto 0) := (others=>'0');
    signal t1_tx_fn_valid     :  std_logic;
    signal send_ranging_req   :  std_logic;
    signal send_t2t3_response       :  t2t3_response_t;
    signal send_t2t3_response_valid :  std_logic;
    signal send_t3                  :  std_logic;
    signal t3_tx_fn                :  std_logic_vector(15 downto 0):= (others=>'0');
    signal t3_tx_fn_valid          :  std_logic;

    --Deframer:
    signal t2t3_response       :  t2t3_response_t := C_T2T3_RESPONSE_INIT;
    signal t2t3_response_valid :  std_logic := '0';

    type resp_mode_t is (NORMAL, MGMT_REQ, MGMT_RESP);
    signal resp_mode : resp_mode_t;
    signal enable_rx : std_logic := '0';

begin 


inst_tof_transponder : entity ranging_lib.tof_transponder
    Port map( 
 
        clk            => clk, --: in  std_logic;
        rst            => rst, --: in  std_logic;

        -- From Framer
        ----------------------
        preamble_synced          => preamble_synced, --: in std_logic;

        -- From Framer
        ---------------------- 
        tx_timing_data          => tx_timing_data, --: in tx_timing_data_t;
        tx_timing_data_valid    => tx_timing_data_valid, --: in std_logic;

        -- From deframer
        ---------------------- 
        rx_timing_data          => rx_timing_data_delay, --: in rx_timing_data_t;
        rx_timing_data_valid    => rx_timing_data_valid_delay, --: in std_logic;

        -- Output data: Time of flight in 1/16 clk
        ----------------------
		tof_counter_valid   => tof_counter_valid, --: out std_logic;
        tof_counter         => tof_counter, --: out std_logic_vector(40-1 downto 0);

        -- Config
        ----------------------
		--cfg_mode           => cfg_mode, --: in std_logic;  -- 0: loopback test  1: SDA mode

        -- SDA compliant ranging interface
        ----------------------
		trigger            => trigger, --: in std_logic;

        --Framer:
        t1_tx_fn           => t1_tx_fn, --: in std_logic_vector(15 downto 0);
        t1_tx_fn_valid     => t1_tx_fn_valid, --: in std_logic;
        send_ranging_req   => send_ranging_req, --: out std_logic;
        send_t2t3_response       => send_t2t3_response, --: out t2t3_response_t;
        send_t2t3_response_valid => send_t2t3_response_valid, --: out std_logic;
        send_t3                  => send_t3, --: out std_logic;
        t3_tx_fn                => t3_tx_fn, --: in std_logic_vector(15 downto 0);
        t3_tx_fn_valid          => t3_tx_fn_valid, --: in std_logic;

        --Deframer:
        t2t3_response       => t2t3_response, --: in t2t3_response_t;
        t2t3_response_valid => t2t3_response_valid --: in std_logic

        );


clk <= not clk after 10 ns;

rx_timing_data_valid_delay  <= transport rx_timing_data_valid after 10 us;
rx_timing_data_delay        <= transport rx_timing_data after 10 us;


process
begin
    rst                     <= '1';
    preamble_synced         <= '1';
    trigger                 <= '0';
    t1_tx_fn_valid          <= '0';
    resp_mode               <= NORMAL;
    t2t3_response_valid     <= '0';
    wait for 300 ns;
    rst <= '0';
    wait for 300 ns;
    wait for 21*5 us;
    wait until rising_edge(clk);
    trigger <= '1';
    wait until rising_edge(clk);
    trigger <= '0';
    wait until send_ranging_req = '1';
    wait for 300 ns;
    wait until rising_edge(clk);
    t1_tx_fn_valid <= '1';
    t1_tx_fn       <= x"0009"; 
    wait until rising_edge(clk);
    t1_tx_fn_valid <= '0';

    
    wait for 150 us;
    resp_mode <= MGMT_RESP;
    wait until (rx_timing_data_valid = '1');
    wait until rising_edge(clk);
    resp_mode <= NORMAL;
    --enable_mgmt_resp <= '1';

    wait until (rx_timing_data_valid_delay = '1' and rx_timing_data_delay.frame_type = C_FRAME_MGMT and rx_timing_data_delay.mgmt_type = C_MGMT_RESP);
    t2t3_response_valid                 <= '1';
    t2t3_response.mgmt_seq				<= (others=>'0');   
    t2t3_response.mgmt_type			    <= C_MGMT_RESP;	
    t2t3_response.response_valid		<= '1';	
    t2t3_response.ranging_meas_valid	<= '0';  
    t2t3_response.egress_ts             <= x"00_0000_0021";   
    t2t3_response.ingress_ts            <= x"00_0000_0020";
    t2t3_response.egress_tx_fn			<= x"000E";   
    wait until rising_edge(clk);
    t2t3_response_valid                 <= '0';
    resp_mode                           <= NORMAL;
    wait for 500 us;
    -- TEST CASE: SEND MGMT BUT NO RESPONSE:
    wait for 21*7 us;
    wait until rising_edge(clk);
    trigger <= '1';
    wait until rising_edge(clk);
    trigger <= '0';
    wait until send_ranging_req = '1';
    wait for 300 ns;
    wait until rising_edge(clk);
    t1_tx_fn_valid <= '1';
    t1_tx_fn       <= x"0020"; 
    wait until rising_edge(clk);
    t1_tx_fn_valid <= '0';

    
    wait for 150 us;    

    -- TEST CASE: SEND MGMT AGAIN

    wait for 21*15 us;
    wait until rising_edge(clk);
    trigger <= '1';
    wait until rising_edge(clk);
    trigger <= '0';
    wait until send_ranging_req = '1';
    wait for 300 ns;
    wait until rising_edge(clk);
    t1_tx_fn_valid <= '1';
    t1_tx_fn       <= x"0020"; 
    wait until rising_edge(clk);
    t1_tx_fn_valid <= '0';

    
    wait for 150 us;
    resp_mode <= MGMT_RESP;
    wait until (rx_timing_data_valid = '1');
    wait until rising_edge(clk);
    resp_mode <= NORMAL;
    --enable_mgmt_resp <= '1';

    wait until (rx_timing_data_valid_delay = '1' and rx_timing_data_delay.frame_type = C_FRAME_MGMT and rx_timing_data_delay.mgmt_type = C_MGMT_RESP);
    t2t3_response_valid                 <= '1';
    t2t3_response.mgmt_seq				<= (others=>'0');   
    t2t3_response.mgmt_type			    <= C_MGMT_RESP;	
    t2t3_response.response_valid		<= '1';	
    t2t3_response.ranging_meas_valid	<= '0';  
    t2t3_response.egress_ts             <= x"00_0000_0021";   
    t2t3_response.ingress_ts            <= x"00_0000_0020";
    t2t3_response.egress_tx_fn			<= x"0032";   
    wait until rising_edge(clk);
    t2t3_response_valid                 <= '0';
    resp_mode                           <= NORMAL;



    wait;

end process;

pr_tx_timing : process
begin
    wait for 1000 ns;
    wait until rising_edge(clk);
    tx_timing_data_valid        <= '1';
    tx_timing_data.tx_fn        <= std_logic_vector(unsigned(tx_timing_data.tx_fn) + 1);  --: std_logic_vector(16-1 downto 0);
	tx_timing_data.tx_ts        <= std_logic_vector(unsigned(tx_timing_data.tx_ts) + 1);         --: std_logic_vector(40-1 downto 0);
	tx_timing_data.ts_applies   <= "011";          --: std_logic_vector(3-1 downto 0);

    wait until rising_edge(clk);
    tx_timing_data_valid        <= '0';
    wait for 19 us;
    --wait;
end process;

pr_rx_timing : process
begin
    --wait until enable_rx = '1';
    wait for 19000 ns;
    wait until rising_edge(clk);
    rx_timing_data_valid                <= '1';
    rx_timing_data.tx_data.tx_fn        <= std_logic_vector(unsigned(tx_timing_data.tx_fn) + 1);  --: std_logic_vector(16-1 downto 0);
	rx_timing_data.tx_data.tx_ts        <= std_logic_vector(unsigned(tx_timing_data.tx_ts) + 1);         --: std_logic_vector(40-1 downto 0);
	rx_timing_data.tx_data.ts_applies   <= "011";          --: std_logic_vector(3-1 downto 0);
    rx_timing_data.rx_ts                <= std_logic_vector(unsigned(rx_timing_data.rx_ts) + 1);
    if resp_mode = NORMAL then
        rx_timing_data.frame_type           <= C_FRAME_IDLE;
        rx_timing_data.mgmt_type            <= C_MGMT_REQ;
    elsif resp_mode = MGMT_REQ then
        rx_timing_data.frame_type           <= C_FRAME_MGMT;
        rx_timing_data.mgmt_type            <= C_MGMT_REQ;
    elsif resp_mode = MGMT_RESP then
        rx_timing_data.frame_type           <= C_FRAME_MGMT;
        rx_timing_data.mgmt_type            <= C_MGMT_RESP;        
    end if;

    wait until rising_edge(clk);
        rx_timing_data_valid        <= '0';
    wait for 1 us;
    --wait;
end process;

end Behavioral;




