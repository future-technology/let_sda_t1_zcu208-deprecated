----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 17/06/2021 11:40:33 AM
-- Design Name: 
-- Module Name: tof_interrogator.vhd - Behavioral
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2018.3
-- Description: [Lab PROTOTYPE] Interrogates the terminal B and waits for a MGMT response.
-- Calculates the time of flight of the frames arrived to the terminal.
-- 
-- Dependencies: 
-- 
-- Revision: Preeliminary Lab test version. Not SDA compliant. 
-- Revision 0.01 - File Created
-- Additional Comments: One terminal in fiber loopback only.
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use ieee.std_logic_misc.all;

library share_let_1g;
use share_let_1g.pkg_support.all;
library sda_lib;
use sda_lib.pkg_sda.all;
library ranging_lib;
use ranging_lib.pkg_components.all;

entity tof_interrogator is
    Port ( 
        clk            : in  std_logic;
        rst            : in  std_logic;

        -- 
        ----------------------
        preamble_synced          : in std_logic;

        ram_tx_en_B      : out std_logic;
        ram_tx_addr_B    : out std_logic_vector(no_bits_natural(C_RAM_TX_WORDS - 1) - 1 downto 0);
        ram_tx_data_B    : in std_logic_vector(C_RAM_TX_DATA_BITWIDTH - 1 downto 0);
        ram_tx_busy      : in std_logic;

        ram_rx_en_B      : out std_logic;
        ram_rx_addr_B    : out std_logic_vector(no_bits_natural(C_RAM_RX_WORDS - 1) - 1 downto 0);
        ram_rx_data_B    : in std_logic_vector(C_RAM_RX_DATA_BITWIDTH - 1 downto 0);


        -- Output data: Time of flight in 1/16 clk
        ----------------------
		tof_counter_valid   : out std_logic;
        tof_counter         : out std_logic_vector(40-1 downto 0);

        -- SDA compliant ranging interface
        ----------------------
		trigger            : in std_logic;
		rx_timing_data_valid : in std_logic;

        --Framer:
        t1_tx_fn           : in std_logic_vector(15 downto 0);
        t1_tx_fn_valid     : in std_logic;
        send_ranging_req   : out std_logic;

        --Deframer:
        mgmt_response       : in mgmt_payload_t;
        mgmt_response_valid : in std_logic;

        -- Debug
        ----------------------
        debug_t1_ts               : out std_logic_vector(40-1 downto 0);
        debug_t2_ts               : out std_logic_vector(40-1 downto 0);
        debug_t3_ts               : out std_logic_vector(40-1 downto 0);
        debug_t4_ts               : out std_logic_vector(40-1 downto 0);
        debug_error               : out std_logic;
        debug_valid               : out std_logic

    );
end tof_interrogator;

architecture Behavioral of tof_interrogator is

constant C_SYNC_THRESHOLD    : unsigned(7 downto 0) := x"05"; --32768;
constant C_T2T3_TIMEOUT      : unsigned(23 downto 0) := x"B71B00";-- 12mill = 60 ms @ 200MHz -> More than 8km of distance , (8km = 27ms, 16km = 54ms)
constant C_WAIT_DELAY        : unsigned(15 downto 0) := x"3E80"; -- 16000 at 200MHz, aprox 3-4 frames of delay


type t_tof_interrogator_proc_fsm is (IDLE, WAIT_RAM_NOT_BUSY, WAIT_T1_TX_FN, 
                                    WAIT_T2T3_RESPONSE, SEEK_T1_INIT, WAIT_T1_DELAY,
                                    SEEK_T1_READ_OFFSET, SEEK_T1_READ_TS, T1_REGISTER, 
                                    SEEK_T4_INIT, SEEK_T4_WAIT, T4_REGISTER, 
                                    COMPUTE_1, COMPUTE_2, HEALTH_CHECK, ASSERT_RESULT,
                                    SEEK_T1_WAIT_1, SEEK_T1_WAIT_2);
signal tof_proc_fsm       : t_tof_interrogator_proc_fsm;

signal ram_rx_data_ts    : std_logic_vector(40-1 downto 0);
signal ram_rx_data_valid : std_logic;
signal rx_ts_valid       : std_logic;
signal r_rx_ts_valid       : std_logic;

signal r_t2t3_response      : mgmt_payload_t;
signal r_t1_tx_fn           : std_logic_vector(15 downto 0);


signal txfn          : std_logic_vector(16-1 downto 0);
signal r_txfn          : std_logic_vector(16-1 downto 0);
signal r_t4_tx_fn          : std_logic_vector(16-1 downto 0);
signal tx_ts         : std_logic_vector(40-1 downto 0);
signal rx_ts         : std_logic_vector(40-1 downto 0);
signal r_t1_tx_ts         : std_logic_vector(40-1 downto 0);
signal r_t4_tx_ts         : std_logic_vector(40-1 downto 0);
signal t2t3_time         : unsigned(40-1 downto 0);
signal t1t4_time         : unsigned(40-1 downto 0);
signal r_tof_counter         : unsigned(40-1 downto 0);
signal tx_ts_result  : std_logic_vector(40-1 downto 0);
signal ts_applies    : std_logic_vector(3-1 downto 0);
signal r_ts_applies    : std_logic_vector(3-1 downto 0);
signal data_in       : std_logic_vector(40+3-1 downto 0);
signal r_data_in       : std_logic_vector(40+3-1 downto 0);
signal tx_data       : std_logic_vector(40+3-1 downto 0);

signal r_rx_timing_data : rx_timing_data_t;
signal preamble_syncs : unsigned(7 downto 0); 
signal debug_tx_ts_int  :  std_logic_vector(40-1 downto 0);
signal debug_rx_ts_int  :  std_logic_vector(40-1 downto 0);
signal debug_valid_int  :  std_logic;

--Response timeout:
signal t2t3_resp_timer  : unsigned(23 downto 0);

signal delay_counter      : unsigned(15 downto 0);

--    -- Avoid coptimization for ILA - DEBUG ONLY. WILL BE REMOVED 
attribute keep : string; 
attribute keep of debug_tx_ts_int: signal is "true"; 
attribute keep of debug_rx_ts_int: signal is "true"; 
attribute keep of debug_valid_int: signal is "true"; 

begin

    debug_valid <= debug_valid_int;  

    tx_ts       <= ram_tx_data_B(ram_tx_data_B'left downto ram_tx_data_B'left-40+1);
    ts_applies  <= ram_tx_data_B(ram_tx_data_B'left-40 downto ram_tx_data_B'right);

    rx_ts_valid  <= ram_rx_data_B(ram_rx_data_B'left);
    rx_ts        <= ram_rx_data_B(ram_rx_data_B'left-1 downto ram_rx_data_B'right);

	-- Manage the input buffer, discard the next frame if buffer almost full
	tx_wr_proc : process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                tof_proc_fsm <= IDLE;
                ram_tx_en_B     <= '0';
                ram_tx_addr_B   <= (others =>'0');
                ram_rx_en_B     <= '0';
                ram_rx_addr_B   <= (others =>'0');
                tof_counter     <= (others =>'0');
                preamble_syncs  <= (others =>'0');
                tx_data         <= (others =>'0');
                ram_tx_en_B     <= '0'; 
                debug_tx_ts_int <= (others =>'0');
                debug_rx_ts_int <= (others =>'0');
                debug_valid_int <= '0';
                send_ranging_req    <= '0';
                debug_t1_ts     <= (others =>'0');
                debug_t2_ts     <= (others =>'0');
                debug_t3_ts     <= (others =>'0');
                debug_t4_ts     <= (others =>'0');
                debug_error     <= '0'; 
                r_rx_ts_valid     <= '0'; 
                t2t3_resp_timer <= (others =>'0');
                delay_counter <= (others =>'0');
            else
                ram_tx_en_B         <= '0';
                ram_rx_en_B         <= '0'; 
                tof_counter_valid   <= '0';
                debug_valid_int     <= '0';
                send_ranging_req    <= '0';
                t2t3_resp_timer     <= (others =>'0');
                delay_counter <= (others =>'0');

                case tof_proc_fsm is
                    when IDLE =>
                        if trigger = '1' then
                            tof_proc_fsm <= WAIT_T1_TX_FN;
                            send_ranging_req <= '1';
                        end if;

                    when WAIT_T1_TX_FN =>
                        send_ranging_req <= '1';
                        if t1_tx_fn_valid = '1' then
                            r_t1_tx_fn <= t1_tx_fn;
                            tof_proc_fsm  <= WAIT_T1_DELAY; --WAIT_T2T3_RESPONSE;
                            send_ranging_req <= '0';
                        end if;
                        --TODO: TIMEOUT
                        t2t3_resp_timer <= t2t3_resp_timer + 1;
                        if t2t3_resp_timer = C_T2T3_TIMEOUT then
                            tof_proc_fsm  <= IDLE;
                        end if;

                    when WAIT_T1_DELAY => --TODO: time delay before searching
                        delay_counter   <= delay_counter + 1;
                        if delay_counter = C_WAIT_DELAY then
                            tof_proc_fsm   <= WAIT_RAM_NOT_BUSY;
                        end if;                     
                        
                    when WAIT_T2T3_RESPONSE =>
                        if mgmt_response_valid = '1' then
                            r_t2t3_response <= mgmt_response;
                            tof_proc_fsm    <= SEEK_T4_INIT; --WAIT_RAM_NOT_BUSY;
                            r_t4_tx_fn      <= mgmt_response.egress_tx_fn;
                        else
                            --TODO: TIMEOUT
                            t2t3_resp_timer <= t2t3_resp_timer + 1;
                            tof_proc_fsm  <= WAIT_T2T3_RESPONSE;
                            if t2t3_resp_timer = C_T2T3_TIMEOUT then
                                tof_proc_fsm  <= IDLE;
                            end if;
                        end if;

                    when WAIT_RAM_NOT_BUSY =>
                        if ram_tx_busy = '0' then
                            tof_proc_fsm  <= SEEK_T1_INIT;
                        end if;
                    
                    when SEEK_T1_INIT =>
                        ram_tx_en_B <= '1';
                        --ram_tx_addr_B <= r_rx_timing_data.tx_data.txfn(no_bits_natural(WORDS - 1) - 1 downto 0);
                        ram_tx_addr_B <= r_t1_tx_fn(no_bits_natural(C_RAM_TX_WORDS - 1) - 1 downto 0);
                        tof_proc_fsm <= SEEK_T1_WAIT_1;
                    when SEEK_T1_WAIT_1 =>
                        tof_proc_fsm <= SEEK_T1_READ_OFFSET;

                    when SEEK_T1_READ_OFFSET =>
                        tx_data <= ram_tx_data_B;
                        r_ts_applies <= ts_applies;
                        tof_proc_fsm <= SEEK_T1_READ_TS;

                    when SEEK_T1_READ_TS =>
                        ram_tx_en_B     <= '1';
                        ram_tx_addr_B   <= std_logic_vector(unsigned(r_t1_tx_fn(no_bits_natural(C_RAM_TX_WORDS - 1) - 1 downto 0)) + unsigned(r_ts_applies));
                        tof_proc_fsm      <= SEEK_T1_WAIT_2;
                    when SEEK_T1_WAIT_2 =>
                        tof_proc_fsm <= T1_REGISTER;

                    when T1_REGISTER =>
                        r_t1_tx_ts         <= tx_ts;
                        tof_proc_fsm      <= WAIT_T2T3_RESPONSE; --SEEK_T4_INIT;

                    ---------------
                    when SEEK_T4_INIT =>
                        ram_rx_en_B <= '1';
                        --ram_tx_addr_B <= r_rx_timing_data.tx_data.txfn(no_bits_natural(WORDS - 1) - 1 downto 0);
                        ram_rx_addr_B <= r_t4_tx_fn(no_bits_natural(C_RAM_RX_WORDS - 1) - 1 downto 0);
                        tof_proc_fsm <= SEEK_T4_WAIT;

                    when SEEK_T4_WAIT =>
                        tof_proc_fsm <= T4_REGISTER;

    --                when SEEK_T4_READ_OFFSET =>
    --                    tx_data <= ram_tx_data_B;
    --                    r_ts_applies <= ts_applies;
    --                    tof_proc_fsm <= SEEK_T4_READ_TS;

    --                when SEEK_T4_READ_TS =>
    --                    ram_tx_en_B     <= '1';
    --                    ram_tx_addr_B   <= std_logic_vector(unsigned(r_t4_tx_fn) + unsigned(r_ts_applies));
    --                    tof_proc_fsm      <= T4_REGISTER;

                    when T4_REGISTER =>
                        r_rx_ts_valid     <= rx_ts_valid;
                        r_t4_tx_ts        <= rx_ts;
                        tof_proc_fsm      <= COMPUTE_1;

                    when COMPUTE_1 =>
                        t2t3_time <= unsigned(r_t2t3_response.egress_ts) - unsigned(r_t2t3_response.ingress_ts);
                        t1t4_time <= unsigned(r_t4_tx_ts) - unsigned(r_t1_tx_ts);
                        tof_proc_fsm      <= COMPUTE_2;

                    when COMPUTE_2 =>
                        r_tof_counter     <= (t1t4_time - t2t3_time);
                        tof_proc_fsm      <= HEALTH_CHECK;

                    when HEALTH_CHECK => 
                        -- It is not necessary to check if the result of the operation is negative
                        -- because the vector has the same size, so the negative part will be removed automatically.
                        --if ((unsigned(r_t2t3_response.egress_ts) < unsigned(r_t2t3_response.ingress_ts)) or 
                        --    (unsigned(r_t4_tx_ts) < unsigned(r_t1_tx_ts)) or 
                        --    (t1t4_time < t2t3_time) or
                        --    r_rx_ts_valid = '0' ) then
                        if ((t1t4_time < t2t3_time) or
                            r_rx_ts_valid = '0' ) then
                            -- Error!:
                            tof_proc_fsm    <= IDLE;
                            debug_error     <= '1';
                        else
                            tof_proc_fsm    <= ASSERT_RESULT;
                            debug_error     <= '0'; 
                        end if;
                        --debug:
                        debug_t1_ts     <= r_t1_tx_ts;
                        debug_t2_ts     <= r_t2t3_response.ingress_ts;
                        debug_t3_ts     <= r_t2t3_response.egress_ts;
                        debug_t4_ts     <= r_t4_tx_ts;
                                 
                        debug_valid_int <= '1';

                    when ASSERT_RESULT => 
                        tof_counter          <= std_logic_vector(r_tof_counter);
                        tof_proc_fsm         <= IDLE;
                        if preamble_syncs = C_SYNC_THRESHOLD then
                            tof_counter_valid    <= '1';
                        else
                            tof_counter_valid    <= '0';
                        end if;
                        
                    when others => 
                        tof_proc_fsm         <= IDLE;
                        
--                    when COMPUTE =>
--                        --TODO:
--                        tof_counter_valid    <= '1';
--                        tof_proc_fsm         <= IDLE;
--                        --else
--                        tof_counter          <= std_logic_vector(unsigned(r_rx_timing_data.rx_ts) - unsigned(tx_ts));
--                        tof_counter_valid    <= '1';
--                        tof_proc_fsm      <= IDLE;
--                        --end if;
--                        if r_rx_timing_data.rx_ts < tx_ts then
--                            --tx_ts_result 
--                            tof_counter          <= std_logic_vector(unsigned(r_rx_timing_data.rx_ts) + (X"FF_FFFF_FFFF" - unsigned(tx_ts)));
--                            tof_counter_valid    <= '1';
--                        end if;
--                        if preamble_syncs = C_SYNC_THRESHOLD then
--                            tof_counter_valid    <= '1';
--                        else
--                            tof_counter_valid    <= '0';
--                        end if;
--
--                        --debug:
--                        debug_rx_ts_int <= r_rx_timing_data.rx_ts;
--                        debug_tx_ts_int <= tx_ts;
--                        debug_valid_int <= '1';
                end case;

                if preamble_synced = '0' then
                    preamble_syncs <= (others=>'0');
                elsif preamble_synced = '1' and rx_timing_data_valid = '1' then
                    if preamble_syncs = C_SYNC_THRESHOLD then
                        preamble_syncs <= C_SYNC_THRESHOLD;
                    else
                        preamble_syncs <= preamble_syncs + 1;
                    end if;
                end if;

            end if;
        end if; 
    end process;


end Behavioral;





















