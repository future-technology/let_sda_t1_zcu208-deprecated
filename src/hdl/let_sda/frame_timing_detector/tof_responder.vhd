----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 30/07/2021 11:40:33 AM
-- Design Name: 
-- Module Name: tof_responder.vhd - Behavioral
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2018.3
-- Description: [Lab PROTOTYPE] TOF Responder module implements the logic to respond to MGMT frames 
-- for calculating range.
-- 
-- Dependencies: 
-- 
-- Revision: Preeliminary Lab test version. Not SDA compliant. 
-- Revision 0.01 - File Created
-- Additional Comments: One terminal in fiber loopback only.
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use ieee.std_logic_misc.all;

library share_let_1g;
use share_let_1g.pkg_support.all;
library sda_lib;
use sda_lib.pkg_sda.all;
library ranging_lib;
use ranging_lib.pkg_components.all;

entity tof_responder is
    Port ( 
        clk            : in  std_logic;
        rst            : in  std_logic;

        -- 
        ----------------------
        --preamble_synced          : in std_logic;

        ram_tx_en_B      : out std_logic;
        ram_tx_addr_B    : out std_logic_vector(no_bits_natural(C_RAM_TX_WORDS - 1) - 1 downto 0);
        ram_tx_data_B    : in std_logic_vector(C_RAM_TX_DATA_BITWIDTH - 1 downto 0);
        ram_tx_busy      : out std_logic;
        -- SDA compliant ranging interface
        ----------------------
        --Framer:
        send_t3                 : out std_logic;
        t3_tx_fn                : in std_logic_vector(15 downto 0);
        t3_tx_fn_valid          : in std_logic;
        send_mgmt_response       : out mgmt_payload_t;
        send_mgmt_response_valid : out std_logic;
        send_mgmt_response_ack   : in std_logic;

        --Deframer:
        --t2_tx_fn_recv           : in std_logic_vector(15 downto 0);
        --t2_tx_fn_recv_valid     : in std_logic;
        --t3_tx_fn_sent           : in std_logic_vector(15 downto 0);
        --t3_tx_fn_sent_valid     : in std_logic;
        rx_timing_data          : in rx_timing_data_t;
        rx_timing_data_valid    : in std_logic

    );
end tof_responder;

architecture Behavioral of tof_responder is

constant BITWIDTH : natural := 16+40+3;
constant TX_DATA_BITWIDTH : natural := 40+3;
constant RX_DATA_BITWIDTH : natural := 40+1; -- ts+valid
constant TX_WORDS            : natural := 256; --32768;
constant RX_WORDS            : natural := 64; --32768;
constant C_SYNC_THRESHOLD : unsigned(7 downto 0) := x"05"; --32768;
constant C_WAIT_DELAY        : unsigned(15 downto 0) := x"3E80"; -- 16000 at 200MHz, aprox 3-4 frames of delay
constant C_TIMEOUT           : unsigned(15 downto 0) := x"0900"; -- TBD

type t_responder_fsm is (IDLE, SEND_T3_SIGNAL, GET_T3_FN, WAIT_DELAY, 
                        SEEK_T3_INIT, SEEK_T3_WAIT_1, SEEK_T3_WAIT_2, 
                        SEEK_T3_READ_OFFSET, SEEK_T3_READ_TS, DATA_REGISTER, 
                        SEND_MGMT_RESP,WAIT_MGMT_ACK);
signal responder_fsm       : t_responder_fsm;

signal txfn          : std_logic_vector(16-1 downto 0);
signal r_txfn          : std_logic_vector(16-1 downto 0);
signal r_t3_tx_fn          : std_logic_vector(16-1 downto 0);
signal tx_ts         : std_logic_vector(40-1 downto 0);
signal r_tx_ts         : std_logic_vector(40-1 downto 0);
signal tx_ts_result  : std_logic_vector(40-1 downto 0);
signal ts_applies    : std_logic_vector(3-1 downto 0);
signal r_ts_applies    : std_logic_vector(3-1 downto 0);
signal data_in       : std_logic_vector(40+3-1 downto 0);
signal r_data_in       : std_logic_vector(40+3-1 downto 0);
signal tx_data       : std_logic_vector(40+3-1 downto 0);

signal r_rx_timing_data : rx_timing_data_t;
signal preamble_syncs : unsigned(7 downto 0); 
signal debug_tx_ts_int  :  std_logic_vector(40-1 downto 0);
signal debug_rx_ts_int  :  std_logic_vector(40-1 downto 0);
signal debug_valid_int  :  std_logic;

signal delay_counter      : unsigned(15 downto 0);
signal timeout_counter    : unsigned(15 downto 0);

--    -- Avoid coptimization for ILA - DEBUG ONLY. WILL BE REMOVED 
attribute keep : string; 
attribute keep of debug_tx_ts_int: signal is "true"; 
attribute keep of debug_rx_ts_int: signal is "true"; 
attribute keep of debug_valid_int: signal is "true"; 

begin

    --debug_tx_ts <= debug_tx_ts_int;  
    --debug_rx_ts <= debug_rx_ts_int;  
    --debug_valid <= debug_valid_int;  

    tx_ts       <= ram_tx_data_B(ram_tx_data_B'left downto ram_tx_data_B'left-40+1);
    ts_applies  <= ram_tx_data_B(ram_tx_data_B'left-40 downto ram_tx_data_B'right);

	-- Manage the input buffer, discard the next frame if buffer almost full
	responder_proc : process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                responder_fsm   <= IDLE;
                ram_tx_addr_B   <= (others =>'0');
                tx_data         <= (others =>'0');
                ram_tx_en_B     <= '0'; 
                debug_tx_ts_int <= (others =>'0');
                debug_rx_ts_int <= (others =>'0');
                debug_valid_int <= '0';
                send_t3         <= '0';
                r_t3_tx_fn      <= (others =>'0');
                delay_counter   <= (others =>'0');
                timeout_counter   <= (others =>'0');
                send_mgmt_response <= C_MGMT_RESPONSE_INIT;
                send_mgmt_response_valid <= '0';
                ram_tx_busy <= '0';
            else
                ram_tx_en_B     <= '0'; 
                debug_valid_int <= '0';
                send_t3         <= '0';
                send_mgmt_response_valid <= '0';
                debug_valid_int <= '0';
                delay_counter   <= (others =>'0');
                timeout_counter   <= (others =>'0');
                ram_tx_busy <= '0';

                case responder_fsm is
                    when IDLE =>
                        if rx_timing_data_valid = '1' then
                            if rx_timing_data.mgmt_valid = '1' and rx_timing_data.mgmt_type = C_MGMT_REQ then
                                responder_fsm       <= SEND_T3_SIGNAL;
                                r_rx_timing_data    <= rx_timing_data;
                                ram_tx_busy         <= '1';
                            else
                                responder_fsm   <= IDLE;
                            end if;
                            --responder_fsm <= SEND_T3;
                            --
                        end if;

                    when SEND_T3_SIGNAL =>
                        send_t3 <= '1';
                        responder_fsm <= GET_T3_FN;
                        ram_tx_busy         <= '1';

                    when GET_T3_FN =>
                        --TODO: TIME OUT!
                        send_t3         <= '1';
                        ram_tx_busy     <= '1';
                        timeout_counter <= timeout_counter + 1;
                        if timeout_counter = C_TIMEOUT then
                            responder_fsm <= IDLE;
                        end if;
                        if t3_tx_fn_valid = '1' then
                            r_t3_tx_fn <= t3_tx_fn;
                            responder_fsm <= WAIT_DELAY;
                        end if;
                        

                    when WAIT_DELAY => --TODO: time delay before searching
                        ram_tx_busy         <= '1'; 
                        delay_counter   <= delay_counter + 1;
                        if delay_counter = C_WAIT_DELAY then
                            responder_fsm   <= SEEK_T3_INIT;
                        end if;
                        
                    
                    when SEEK_T3_INIT =>
                        ram_tx_en_B <= '1';
                        ram_tx_addr_B <= r_t3_tx_fn(no_bits_natural(C_RAM_TX_WORDS - 1) - 1 downto 0);
                        responder_fsm <= SEEK_T3_WAIT_1;
                        ram_tx_busy         <= '1';
                    when SEEK_T3_WAIT_1 =>
                        responder_fsm <= SEEK_T3_READ_OFFSET;
                        ram_tx_busy         <= '1';
                    when SEEK_T3_READ_OFFSET =>
                        tx_data <= ram_tx_data_B;
                        r_ts_applies <= ts_applies;
                        responder_fsm <= SEEK_T3_READ_TS;
                        ram_tx_busy         <= '1';

                    when SEEK_T3_READ_TS =>
                        ram_tx_en_B     <= '1';
                        ram_tx_addr_B   <= std_logic_vector(unsigned(r_t3_tx_fn(no_bits_natural(C_RAM_TX_WORDS - 1) - 1 downto 0)) + unsigned(r_ts_applies));
                        responder_fsm      <= SEEK_T3_WAIT_2;
                        ram_tx_busy         <= '1';

                    when SEEK_T3_WAIT_2 =>
                        responder_fsm <= DATA_REGISTER;
                        ram_tx_busy         <= '1';
                    when DATA_REGISTER =>
                        r_tx_ts         <= tx_ts;
                        responder_fsm      <= SEND_MGMT_RESP;
                        ram_tx_busy         <= '1';
                    when SEND_MGMT_RESP =>
                        responder_fsm                               <= WAIT_MGMT_ACK;
                        --TODO:
                        send_mgmt_response.mgmt_seq 				<= x"ABC7"; 
                        send_mgmt_response.mgmt_type				<= C_MGMT_RESP;   
                        send_mgmt_response.response_valid			<= '1';   
                        send_mgmt_response.ranging_meas_valid		<= '0';   
                        send_mgmt_response.reserved1 				<= (others=>'0');  
                        send_mgmt_response.egress_ts                <= r_tx_ts; 
                        send_mgmt_response.ingress_ts               <= r_rx_timing_data.rx_ts; 
                        send_mgmt_response.egress_tx_fn			    <= r_t3_tx_fn;
                        send_mgmt_response.ranging_measure_meters   <= x"12345678";
                        send_mgmt_response_valid                    <= '1';  
                        --debug:
                        debug_rx_ts_int <= r_rx_timing_data.rx_ts;
                        debug_tx_ts_int <= r_tx_ts;
                        debug_valid_int <= '1';
                        ram_tx_busy     <= '0';

                    when WAIT_MGMT_ACK =>
                        send_mgmt_response_valid <= '1';
                        if send_mgmt_response_ack = '1' then
                            responder_fsm <= IDLE;
                            send_mgmt_response_valid <= '0';
                        end if;

                    when others =>
                        responder_fsm <= IDLE;
                        responder_fsm   <= IDLE;
                        ram_tx_addr_B   <= (others =>'0');
                        tx_data         <= (others =>'0');
                        ram_tx_en_B     <= '0'; 
                        debug_tx_ts_int <= (others =>'0');
                        debug_rx_ts_int <= (others =>'0');
                        debug_valid_int <= '0';
                        send_t3         <= '0';
                        r_t3_tx_fn      <= (others =>'0');
                        delay_counter   <= (others =>'0');
                        timeout_counter   <= (others =>'0');
                        send_mgmt_response <= C_MGMT_RESPONSE_INIT;
                        send_mgmt_response_valid <= '0';
                end case;

            end if;
        end if; 
    end process;


end Behavioral;

