----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 17/06/2021 11:40:33 AM
-- Design Name: 
-- Module Name: tof_transponder.vhd - Behavioral
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2018.3
-- Description: [Lab PROTOTYPE] Calculates the time of flight of the frames arrived to the terminal
-- 
-- Dependencies: tof_interrogator.vhd, tof_responder.vhd, tof_fifo_tx
-- 
-- Revision: Preeliminary Lab test version. Not SDA compliant. 
-- Revision 0.01 - File Created
-- Additional Comments: One terminal in fiber loopback only.
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use ieee.std_logic_misc.all;

library share_let_1g;
use share_let_1g.pkg_support.all;
library sda_lib;
use sda_lib.pkg_sda.all;
library ranging_lib;
use ranging_lib.pkg_components.all;

entity tof_transponder is
    Port ( 
        clk            : in  std_logic;
        rst            : in  std_logic;

        -- From Framer
        ----------------------
        preamble_synced          : in std_logic;

        -- From Framer
        ---------------------- 
        tx_timing_data          : in tx_timing_data_t;
        tx_timing_data_valid    : in std_logic;

        -- From deframer
        ---------------------- 
        rx_timing_data          : in rx_timing_data_t;
        rx_timing_data_valid    : in std_logic;

        -- Output data: Time of flight in 1/16 clk
        ----------------------
		tof_counter_valid   : out std_logic;
        tof_counter         : out std_logic_vector(40-1 downto 0);

        -- Config
        ----------------------
		--cfg_mode           : in std_logic;  -- 0: loopback test  1: SDA mode

        -- SDA compliant ranging interface
        ----------------------
		trigger            : in std_logic;

        --Framer:
        t1_tx_fn                 : in std_logic_vector(15 downto 0);
        t1_tx_fn_valid           : in std_logic;
        send_ranging_req         : out std_logic;
        send_mgmt_response       : out mgmt_payload_t;
        send_mgmt_response_valid : out std_logic;
        send_mgmt_response_ack   : in std_logic;
        send_t3                  : out std_logic;
        t3_tx_fn                 : in std_logic_vector(15 downto 0);
        t3_tx_fn_valid           : in std_logic;

        --Deframer:
        mgmt_response       : in mgmt_payload_t;
        mgmt_response_valid : in std_logic

        -- Debug
        ----------------------
--        debug_tx_ts               : out std_logic_vector(40-1 downto 0);
--        debug_rx_ts               : out std_logic_vector(40-1 downto 0);
--        debug_valid               : out std_logic

    );
end tof_transponder;

architecture Behavioral of tof_transponder is

constant BITWIDTH : natural := 16+40+3;
--constant TX_DATA_BITWIDTH : natural := 40+3;
--constant RX_DATA_BITWIDTH : natural := 40+1; -- ts+valid
--constant TX_WORDS            : natural := 256; --32768;
--constant RX_WORDS            : natural := 64; --32768;
constant C_SYNC_THRESHOLD : unsigned(7 downto 0) := x"05"; --32768;


type t_tx_wr_proc_fsm is (IDLE, WR_RAM, FIFO_REG, SEEK_INIT, SEEK_READ_OFFSET, SEEK_READ_TS, DATA_REGISTER, COMPUTE);
signal tx_wr_proc_fsm       : t_tx_wr_proc_fsm;

signal ram_tx_wen_A     : std_logic;
signal ram_tx_en_A      : std_logic;
signal ram_tx_addr_A    : std_logic_vector(no_bits_natural(C_RAM_TX_WORDS - 1) - 1 downto 0);
signal ram_tx_data_A    : std_logic_vector(C_RAM_TX_DATA_BITWIDTH - 1 downto 0 );
signal ram_tx_en_B_int      : std_logic;
signal ram_tx_addr_B_int    : std_logic_vector(no_bits_natural(C_RAM_TX_WORDS - 1) - 1 downto 0);
signal ram_tx_data_B_int    : std_logic_vector(C_RAM_TX_DATA_BITWIDTH - 1 downto 0);
signal ram_tx_en_B_resp      : std_logic;
signal ram_tx_addr_B_resp    : std_logic_vector(no_bits_natural(C_RAM_TX_WORDS - 1) - 1 downto 0);
signal ram_tx_data_B_resp    : std_logic_vector(C_RAM_TX_DATA_BITWIDTH - 1 downto 0);
signal ram_tx_en_B      : std_logic;
signal ram_tx_addr_B    : std_logic_vector(no_bits_natural(C_RAM_TX_WORDS - 1) - 1 downto 0);
signal ram_tx_data_B    : std_logic_vector(C_RAM_TX_DATA_BITWIDTH - 1 downto 0);

signal ram_tx_B_mux      : std_logic;

type t_rx_wr_proc_fsm is (IDLE, WR_RAM, RESET_RAM);
signal rx_wr_proc_fsm   : t_rx_wr_proc_fsm;

signal ram_rx_wen_A     : std_logic;
signal ram_rx_en_A      : std_logic;
signal ram_rx_addr_A    : std_logic_vector(no_bits_natural(C_RAM_RX_WORDS - 1) - 1 downto 0);
signal ram_rx_data_A    : std_logic_vector(C_RAM_RX_DATA_BITWIDTH - 1 downto 0 );
signal ram_rx_en_B      : std_logic;
signal ram_rx_addr_B    : std_logic_vector(no_bits_natural(C_RAM_RX_WORDS - 1) - 1 downto 0);
signal ram_rx_data_B    : std_logic_vector(C_RAM_RX_DATA_BITWIDTH - 1 downto 0);

signal ram_rx_data_ts    : std_logic_vector(40-1 downto 0);
signal ram_rx_data_valid : std_logic;

signal fifo_tx_rd_en  : std_logic;
signal fifo_tx_valid  : std_logic;
signal rst_ram_rx_mux : std_logic;
signal rst_ram_rx     : std_logic;
signal fifo_tx_din    : std_logic_vector(BITWIDTH - 1 downto 0 );
signal fifo_tx_dout   : std_logic_vector(BITWIDTH - 1 downto 0 );

signal txfn          : std_logic_vector(16-1 downto 0);
signal r_txfn          : std_logic_vector(16-1 downto 0);
signal tx_ts         : std_logic_vector(40-1 downto 0);
signal r_tx_ts         : std_logic_vector(40-1 downto 0);
signal tx_ts_result  : std_logic_vector(40-1 downto 0);
signal ts_applies    : std_logic_vector(3-1 downto 0);
signal r_ts_applies    : std_logic_vector(3-1 downto 0);
signal data_in       : std_logic_vector(40+3-1 downto 0);
signal r_data_in       : std_logic_vector(40+3-1 downto 0);
signal tx_data       : std_logic_vector(40+3-1 downto 0);
signal extend_rx_ram_wr_counter : unsigned(7 downto 0);
signal extend_rx_ram_wr         : std_logic;


signal r_rx_timing_data : rx_timing_data_t;
signal preamble_syncs : unsigned(7 downto 0); 
signal debug_tx_ts_int  :  std_logic_vector(40-1 downto 0);
signal debug_rx_ts_int  :  std_logic_vector(40-1 downto 0);
signal debug_valid_int  :  std_logic;

signal debug_t1_ts               :  std_logic_vector(40-1 downto 0);
signal debug_t2_ts               :  std_logic_vector(40-1 downto 0);
signal debug_t3_ts               :  std_logic_vector(40-1 downto 0);
signal debug_t4_ts               :  std_logic_vector(40-1 downto 0);
signal debug_error               :  std_logic;
signal debug_valid               :  std_logic;

--    -- Avoid coptimization for ILA - DEBUG ONLY. WILL BE REMOVED 
attribute keep : string; 
attribute keep of debug_t1_ts: signal is "true"; 
attribute keep of debug_t2_ts: signal is "true"; 
attribute keep of debug_t3_ts: signal is "true"; 
attribute keep of debug_t4_ts: signal is "true"; 
attribute keep of debug_error: signal is "true"; 
attribute keep of debug_valid: signal is "true"; 

begin

    --debug_tx_ts <= debug_tx_ts_int;  
    --debug_rx_ts <= debug_rx_ts_int;  
    --debug_valid <= debug_valid_int;  

    fifo_tx_din <= tx_timing_data.tx_fn & tx_timing_data.tx_ts & tx_timing_data.ts_applies;
    txfn        <= fifo_tx_dout(fifo_tx_dout'left downto fifo_tx_dout'left-16+1);
    data_in     <= fifo_tx_dout(fifo_tx_dout'left-16 downto fifo_tx_dout'right);

    tx_ts       <= ram_tx_data_B(ram_tx_data_B'left downto ram_tx_data_B'left-40+1);
    ts_applies  <= ram_tx_data_B(ram_tx_data_B'left-40 downto ram_tx_data_B'right);

    ram_tx_en_B    <= ram_tx_en_B_resp when ram_tx_B_mux = '1' else ram_tx_en_B_int;  
    ram_tx_addr_B  <= ram_tx_addr_B_resp when ram_tx_B_mux = '1' else ram_tx_addr_B_int;  


	-- Manage the input buffer, discard the next frame if buffer almost full
	tx_wr_proc : process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                tx_wr_proc_fsm <= IDLE;
                ram_tx_wen_A    <= '0';
                ram_tx_en_A     <= '0';
                ram_tx_addr_A   <= (others =>'0');
                ram_tx_data_A   <= (others =>'0');
                preamble_syncs  <= (others =>'0');
                tx_data         <= (others =>'0');
                fifo_tx_rd_en   <= '0';
                --debug_tx_ts_int <= (others =>'0');
                --debug_rx_ts_int <= (others =>'0');
                debug_valid_int <= '0';
            else

                ram_tx_wen_A    <= '0';
                ram_tx_en_A     <= '0'; 
                fifo_tx_rd_en   <= '0';
                --debug_valid_int     <= '0';

                case tx_wr_proc_fsm is
                    when IDLE =>
                        if fifo_tx_valid = '1' then
                            tx_wr_proc_fsm <= FIFO_REG;
                        end if;

                    when FIFO_REG =>
                        if fifo_tx_valid = '1' then
                            r_txfn          <= txfn;
                            r_data_in       <= data_in;
                            tx_wr_proc_fsm  <= WR_RAM;
                        else
                            tx_wr_proc_fsm  <= IDLE;
                        end if;

                    when WR_RAM =>
                        ram_tx_addr_A       <= r_txfn(no_bits_natural(C_RAM_TX_WORDS - 1) - 1 downto 0);
                        ram_tx_wen_A        <= '1';
                        ram_tx_en_A         <= '1';
                        ram_tx_data_A       <= r_data_in;
                        fifo_tx_rd_en       <= '1';
                        tx_wr_proc_fsm      <= IDLE;
                    
                        --debug:
                        --debug_rx_ts_int <= r_rx_timing_data.rx_ts;
                        --debug_tx_ts_int <= tx_ts;
                        --debug_valid_int <= '1';

                    when others => 
                        tx_wr_proc_fsm      <= IDLE;
                end case;

                if preamble_synced = '0' then
                    preamble_syncs <= (others=>'0');
                elsif preamble_synced = '1' and rx_timing_data_valid = '1' then
                    if preamble_syncs = C_SYNC_THRESHOLD then
                        preamble_syncs <= C_SYNC_THRESHOLD;
                    else
                        preamble_syncs <= preamble_syncs + 1;
                    end if;
                end if;

            end if;
        end if; 
    end process;

	-- Manage the rx ts write process:
    ---- Reset RAM and Write new entries after ranging request is triggered. 
    ---- Stop writting after MGMT response is received.
	rx_wr_proc : process(clk) is
        begin
            if rising_edge(clk) then
                if rst = '1' then
                    rx_wr_proc_fsm <= IDLE;
                    ram_rx_addr_A   <= (others=>'0');
                    ram_rx_data_A   <= (others=>'0');
                    ram_rx_wen_A    <= '0';
                    ram_rx_en_A     <= '0';
                    rst_ram_rx      <= '0';
                    extend_rx_ram_wr_counter <= (others =>'0');
                    extend_rx_ram_wr         <= '0';
                    
                else
                    ram_rx_wen_A    <= '0';
                    ram_rx_en_A     <= '0';
                    rst_ram_rx      <= '0';
                                        
                    case rx_wr_proc_fsm is
                        when IDLE =>
                            if t1_tx_fn_valid = '1' then
                                rx_wr_proc_fsm <= RESET_RAM;
                            end if;

                        when RESET_RAM =>
                            rst_ram_rx <= '1'; 
                            rx_wr_proc_fsm <= WR_RAM;
                            extend_rx_ram_wr_counter <= (others =>'0');
                            extend_rx_ram_wr         <= '0';                            

                        when WR_RAM =>
                            if rx_timing_data_valid = '1' then
                                ram_rx_addr_A   <= rx_timing_data.tx_data.tx_fn(no_bits_natural(C_RAM_RX_WORDS - 1) - 1 downto 0);
                                ram_rx_wen_A    <= '1';
                                ram_rx_en_A     <= '1';
                                ram_rx_data_A   <= '1' & rx_timing_data.rx_ts;
                                if extend_rx_ram_wr = '1' then
                                    extend_rx_ram_wr_counter <= extend_rx_ram_wr_counter + 1;
                                end if;
                            end if;                        
                            if mgmt_response_valid = '1' then
                                
                                extend_rx_ram_wr         <= '1';
                                --rx_wr_proc_fsm <= IDLE;
                            end if; 
                            if extend_rx_ram_wr_counter = x"03" then
                                rx_wr_proc_fsm <= IDLE;
                            end if;

                        when others => 
                            rx_wr_proc_fsm      <= IDLE;
                    end case;
                end if;
            end if;
        end process;

	inst_fifo_ram_tx : entity share_let_1g.generic_simple_dp_ram
	generic map(
		DISTR_RAM  => true,
		WORDS      => C_RAM_TX_WORDS,
		BITWIDTH   => C_RAM_TX_DATA_BITWIDTH --(txfn 16b, tx_ts 40b, ts_applied 3b) 
	)
	port map(
		clk   => clk,
		rst   => rst,

		wen_A => ram_tx_wen_A,
		en_A  => ram_tx_en_A,

		a_A   => ram_tx_addr_A,
		d_A   => ram_tx_data_A,

		en_B  => ram_tx_en_B,
		a_B   => ram_tx_addr_B,
		q_B   => ram_tx_data_B
	);

    rst_ram_rx_mux <= rst_ram_rx and rst;

	inst_fifo_ram_rx : entity share_let_1g.generic_simple_dp_ram
	generic map(
		DISTR_RAM  => true,
		WORDS      => C_RAM_RX_WORDS,
		BITWIDTH   => C_RAM_RX_DATA_BITWIDTH -- tx_ts 40bit , valid 1 bit 
	)
	port map(
		clk   => clk,
		rst   => rst_ram_rx_mux,

		wen_A => ram_rx_wen_A,
		en_A  => ram_rx_en_A,

		a_A   => ram_rx_addr_A,
		d_A   => ram_rx_data_A,

		en_B  => ram_rx_en_B,
		a_B   => ram_rx_addr_B,
		q_B   => ram_rx_data_B
	);

    inst_tof_fifo_tx : tof_fifo_tx
    PORT MAP (
        rst         => rst,
        clk         => clk,
        din         => fifo_tx_din,
        wr_en       => tx_timing_data_valid,
        rd_en       => fifo_tx_rd_en,
        dout        => fifo_tx_dout,
        full        => open,
        empty       => open,
        valid       => fifo_tx_valid
    );

    inst_tof_interrogator : entity ranging_lib.tof_interrogator
        Port map( 
            clk            => clk, --: in  std_logic;
            rst            => rst, --: in  std_logic;
    
            -- 
            ----------------------
            preamble_synced          => preamble_synced, --: in std_logic;
    
            ram_tx_en_B      => ram_tx_en_B_int, --: out std_logic;
            ram_tx_addr_B    => ram_tx_addr_B_int, --: out std_logic_vector(no_bits_natural(C_RAM_TX_WORDS - 1) - 1 downto 0);
            ram_tx_data_B    => ram_tx_data_B, --: in std_logic_vector(C_RAM_TX_DATA_BITWIDTH - 1 downto 0);
            ram_tx_busy      => ram_tx_B_mux,

            ram_rx_en_B      => ram_rx_en_B, --: out std_logic;
            ram_rx_addr_B    => ram_rx_addr_B, --: out std_logic_vector(no_bits_natural(C_RAM_RX_WORDS - 1) - 1 downto 0);
            ram_rx_data_B    => ram_rx_data_B, --: in std_logic_vector(C_RAM_RX_DATA_BITWIDTH - 1 downto 0);
    
    
            -- Output data: Time of flight in 1/16 clk
            ----------------------
            tof_counter_valid   => tof_counter_valid, --: out std_logic;
            tof_counter         => tof_counter, --: out std_logic_vector(40-1 downto 0);
    
            -- SDA compliant ranging interface
            ----------------------
            trigger            => trigger, --: in std_logic;
            rx_timing_data_valid => rx_timing_data_valid,
            --Framer:
            t1_tx_fn           => t1_tx_fn, --: in std_logic_vector(15 downto 0);
            t1_tx_fn_valid     => t1_tx_fn_valid, --: in std_logic;
            send_ranging_req   => send_ranging_req, --: out std_logic;
    
            --Deframer:
            mgmt_response       => mgmt_response, --: in mgmt_response_t;
            mgmt_response_valid => mgmt_response_valid, --: in std_logic;
    
            -- Debug
            ----------------------
            debug_t1_ts         => debug_t1_ts, --: out std_logic_vector(40-1 downto 0);
            debug_t2_ts         => debug_t2_ts, --: out std_logic_vector(40-1 downto 0);
            debug_t3_ts         => debug_t3_ts, --: out std_logic_vector(40-1 downto 0);
            debug_t4_ts         => debug_t4_ts, --: out std_logic_vector(40-1 downto 0);
            debug_error         => debug_error, --: out std_logic;
            debug_valid         => debug_valid --: out std_logic
        );


    isnt_tof_responder : entity ranging_lib.tof_responder
        Port map ( 
            clk            => clk, --: in  std_logic;
            rst            => rst, --: in  std_logic;
    
            -- 
            ----------------------
            --preamble_synced          : in std_logic;
    
            ram_tx_en_B      => ram_tx_en_B_resp  , --: out std_logic;
            ram_tx_addr_B    => ram_tx_addr_B_resp, --: out std_logic_vector(no_bits_natural(C_RAM_RX_WORDS - 1) - 1 downto 0);
            ram_tx_data_B    => ram_tx_data_B, --: in std_logic_vector(C_RAM_RX_DATA_BITWIDTH - 1 downto 0);
            ram_tx_busy      => ram_tx_B_mux,
            -- SDA compliant ranging interface
            ----------------------
            --Framer:
            send_t3                  => send_t3                  , --: out std_logic;
            t3_tx_fn                 => t3_tx_fn                 , --: in std_logic_vector(15 downto 0);
            t3_tx_fn_valid           => t3_tx_fn_valid           , --: in std_logic;
            send_mgmt_response       => send_mgmt_response       , --: out mgmt_response_t;
            send_mgmt_response_valid => send_mgmt_response_valid , --: out std_logic;
            send_mgmt_response_ack   => send_mgmt_response_ack   , --: out std_logic;
            
            --Deframer:
            --t2_tx_fn_recv           : in std_logic_vector(15 downto 0);
            --t2_tx_fn_recv_valid     : in std_logic;
            --t3_tx_fn_sent           : in std_logic_vector(15 downto 0);
            --t3_tx_fn_sent_valid     : in std_logic;
            rx_timing_data          => rx_timing_data,          --: in rx_timing_data_t;
            rx_timing_data_valid    => rx_timing_data_valid     --: in std_logic
    
        );

end Behavioral;





















