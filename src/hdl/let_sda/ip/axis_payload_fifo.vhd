-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 14/10/2022 11:40:33 AM
-- Design Name: 
-- Module Name: axis_payload_fifo 
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;
library share_let_1g;
use share_let_1g.pkg_support.all;
use share_let_1g.pkg_components.all;

entity axis_payload_fifo is
    generic(
        DISTR_RAM       : boolean := true;
        FIFO_DEPTH      : natural := 128;
        FULL_THRESHOLD  : natural := 128;
        EMPTY_THRESHOLD : natural := 1
    );
    port(
        clk                 : in    std_logic;
        rst                 : in    std_logic;
        -- Input interfaces:
        axis_payload_if_in_s  : out t_axis_payload_if_s;
        axis_payload_if_in_m  : in t_axis_payload_if_m;
        -- Output interface:
        axis_payload_if_out_s : in t_axis_payload_if_s;
        axis_payload_if_out_m : out t_axis_payload_if_m;
        input_almost_full   : out   std_logic;
        input_almost_empty  : out   std_logic;
        fifo_max_level      : out   std_logic_vector(no_bits_natural(FIFO_DEPTH) - 1 downto 0)
    );
end entity axis_payload_fifo;

architecture RTL of axis_payload_fifo is
    -- Metadata buffer:
    constant c_metadata_length : natural := C_TX_FN_WIDTH + C_FRAME_TYPE_WIDTH + C_TX_NUM_WIDTH;
    signal metadata_in         : std_logic_vector(c_metadata_length - 1 downto 0);
    signal metadata_valid      : std_logic;
    signal metadata_buf        : std_logic_vector(c_metadata_length - 1 downto 0);
    signal metadata_buf_valid  : std_logic;
    signal metadata_buf_ready  : std_logic;
    signal tx_fn_buf           : std_logic_vector(C_TX_FN_WIDTH - 1 downto 0); -- Sequence number of this Tx Frame
    signal frame_type_buf      : std_logic_vector(C_FRAME_TYPE_WIDTH - 1 downto 0); -- Frame Type
    signal tx_num_buf          : std_logic_vector(C_TX_NUM_WIDTH - 1 downto 0); -- Transmission atempt

    signal int_axis_payload_if_in_ready : std_logic;

begin
    ASSERT FIFO_DEPTH >= C_FSO_PAYLOAD_SIZE REPORT "axis_payload_fifo: FIFO DEPTH must be larger that the size of a payload." SEVERITY ERROR;

    -- ASYNC:
    metadata_buf_ready <= axis_payload_if_out_m.axis_if_m.tlast and axis_payload_if_out_m.axis_if_m.tvalid and axis_payload_if_out_s.axis_if_s.tready;
    metadata_in        <= axis_payload_if_in_m.tx_num & axis_payload_if_in_m.frame_type & axis_payload_if_in_m.tx_fn;
    axis_payload_if_out_m.tx_fn          <= metadata_buf(C_TX_FN_WIDTH - 1 downto 0);
    axis_payload_if_out_m.frame_type     <= metadata_buf(C_FRAME_TYPE_WIDTH + C_TX_FN_WIDTH - 1 downto C_TX_FN_WIDTH);
    axis_payload_if_out_m.tx_num         <= metadata_buf(c_metadata_length - 1 downto C_FRAME_TYPE_WIDTH + C_TX_FN_WIDTH);
    metadata_valid     <= axis_payload_if_in_m.axis_if_m.tstart and axis_payload_if_in_m.axis_if_m.tstart and int_axis_payload_if_in_ready;

    axis_payload_if_in_s.axis_if_s.tready <= int_axis_payload_if_in_ready;

    Inst_axis_buffer : entity share_let_1g.axi4s_fifo_v2
        generic map(
            DISTR_RAM         => DISTR_RAM,
            FIFO_DEPTH        => FIFO_DEPTH,
            DATA_WIDTH        => 8,
            FULL_THRESHOLD    => FULL_THRESHOLD,
            EMPTY_THRESHOLD   => EMPTY_THRESHOLD,
            USE_OUTPUT_BUFFER => false
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => axis_payload_if_in_m.axis_if_m.tdata,
            input_valid        => axis_payload_if_in_m.axis_if_m.tvalid,
            input_last         => axis_payload_if_in_m.axis_if_m.tlast,
            input_ready        => int_axis_payload_if_in_ready, --axis_payload_if_in.axis_if.tready,
            input_user         => axis_payload_if_in_m.axis_if_m.tuser,
            input_start        => axis_payload_if_in_m.axis_if_m.tstart,
            input_almost_full  => input_almost_full,
            input_almost_empty => input_almost_empty,
            fifo_max_level     => fifo_max_level,
            -- Output data handling
            -----------------------
            output             => axis_payload_if_out_m.axis_if_m.tdata,
            output_valid       => axis_payload_if_out_m.axis_if_m.tvalid,
            output_last        => axis_payload_if_out_m.axis_if_m.tlast,
            output_ready       => axis_payload_if_out_s.axis_if_s.tready,
            output_user        => axis_payload_if_out_m.axis_if_m.tuser,
            output_start       => axis_payload_if_out_m.axis_if_m.tstart
        );

    -- FIFO buffer for incoming metadata 
    inst_metadata_buffer_input : entity share_let_1g.sync_fifo
        generic map(
            DISTR_RAM       => true,
            FIFO_DEPTH      => (FIFO_DEPTH / C_FSO_PAYLOAD_SIZE) + 8,
            DATA_WIDTH      => c_metadata_length, -- tx_fn+frame_type+tx_num
            FULL_THRESHOLD  => 32,
            EMPTY_THRESHOLD => 0
        )
        port map(
            clk                => clk,
            rst                => rst,
            -- Input data handling
            ----------------------
            input              => metadata_in,
            input_valid        => metadata_valid,
            input_ready        => open,
            input_almost_full  => open,
            input_almost_empty => open,
            fifo_max_level     => open,
            fifo_data_count    => open,
            -- Output data handling
            -----------------------
            output             => metadata_buf,
            output_valid       => metadata_buf_valid,
            output_ready       => metadata_buf_ready -- to sync with the payload
        );

end architecture RTL;
