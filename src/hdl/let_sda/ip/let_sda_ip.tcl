##################################################################
# CHECK VIVADO VERSION
##################################################################

set scripts_vivado_version 2022.1
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
  catch {common::send_msg_id "IPS_TCL-100" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_ip_tcl to create an updated script."}
  return 1
}

##################################################################
# START
##################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source let_sda_ip.tcl
# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./prj/let_sda_zcu208.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
  create_project let_sda_zcu208 prj -part xczu48dr-fsvg1517-2-e
  set_property BOARD_PART xilinx.com:zcu208:part0:2.0 [current_project]
  set_property target_language VHDL [current_project]
  set_property simulator_language Mixed [current_project]
}

##################################################################
# CHECK IPs
##################################################################

set bCheckIPs 1
set bCheckIPsPassed 1
if { $bCheckIPs == 1 } {
  set list_check_ips { xilinx.com:ip:fifo_generator:13.2 }
  set list_ips_missing ""
  common::send_msg_id "IPS_TCL-1001" "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

  foreach ip_vlnv $list_check_ips {
  set ip_obj [get_ipdefs -all $ip_vlnv]
  if { $ip_obj eq "" } {
    lappend list_ips_missing $ip_vlnv
    }
  }

  if { $list_ips_missing ne "" } {
    catch {common::send_msg_id "IPS_TCL-105" "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
    set bCheckIPsPassed 0
  }
}

if { $bCheckIPsPassed != 1 } {
  common::send_msg_id "IPS_TCL-102" "WARNING" "Will not continue with creation of design due to the error(s) above."
  return 1
}

##################################################################
# CREATE IP axi_fifo
##################################################################

set axi_fifo [create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.2 -module_name axi_fifo]

set_property -dict { 
  CONFIG.Fifo_Implementation {Common_Clock_Distributed_RAM}
  CONFIG.INTERFACE_TYPE {AXI_STREAM}
  CONFIG.Input_Data_Width {9}
  CONFIG.Input_Depth {2048}
  CONFIG.Output_Data_Width {9}
  CONFIG.Output_Depth {2048}
  CONFIG.Use_Embedded_Registers {false}
  CONFIG.Reset_Type {Asynchronous_Reset}
  CONFIG.Full_Flags_Reset_Value {1}
  CONFIG.Almost_Full_Flag {false}
  CONFIG.Valid_Flag {true}
  CONFIG.Data_Count_Width {11}
  CONFIG.Write_Data_Count_Width {11}
  CONFIG.Read_Data_Count_Width {11}
  CONFIG.Full_Threshold_Assert_Value {2046}
  CONFIG.Full_Threshold_Negate_Value {2045}
  CONFIG.TUSER_WIDTH {0}
  CONFIG.Enable_TLAST {true}
  CONFIG.FIFO_Implementation_wach {Common_Clock_Distributed_RAM}
  CONFIG.Full_Threshold_Assert_Value_wach {15}
  CONFIG.Empty_Threshold_Assert_Value_wach {14}
  CONFIG.FIFO_Implementation_wdch {Common_Clock_Builtin_FIFO}
  CONFIG.FIFO_Implementation_wrch {Common_Clock_Distributed_RAM}
  CONFIG.Full_Threshold_Assert_Value_wrch {15}
  CONFIG.Empty_Threshold_Assert_Value_wrch {14}
  CONFIG.FIFO_Implementation_rach {Common_Clock_Distributed_RAM}
  CONFIG.Full_Threshold_Assert_Value_rach {15}
  CONFIG.Empty_Threshold_Assert_Value_rach {14}
  CONFIG.FIFO_Implementation_rdch {Common_Clock_Builtin_FIFO}
  CONFIG.FIFO_Implementation_axis {Common_Clock_Distributed_RAM}
  CONFIG.FIFO_Application_Type_axis {Packet_FIFO}
  CONFIG.Input_Depth_axis {16384}
  CONFIG.Full_Threshold_Assert_Value_axis {16383}
  CONFIG.Empty_Threshold_Assert_Value_axis {16382}
} [get_ips axi_fifo]

set_property -dict { 
  GENERATE_SYNTH_CHECKPOINT {1}
} $axi_fifo

##################################################################

##################################################################
# CREATE IP dc_fifo_eth_side
##################################################################

set dc_fifo_eth_side [create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.2 -module_name dc_fifo_eth_side]

set_property -dict { 
  CONFIG.synchronization_stages_axi {3}
  CONFIG.INTERFACE_TYPE {AXI_STREAM}
  CONFIG.Reset_Type {Asynchronous_Reset}
  CONFIG.Clock_Type_AXI {Independent_Clock}
  CONFIG.TUSER_WIDTH {1}
  CONFIG.Enable_TLAST {true}
  CONFIG.FIFO_Implementation_wach {Independent_Clocks_Distributed_RAM}
  CONFIG.Full_Threshold_Assert_Value_wach {15}
  CONFIG.Empty_Threshold_Assert_Value_wach {13}
  CONFIG.FIFO_Implementation_wdch {Independent_Clocks_Builtin_FIFO}
  CONFIG.Empty_Threshold_Assert_Value_wdch {1018}
  CONFIG.FIFO_Implementation_wrch {Independent_Clocks_Distributed_RAM}
  CONFIG.Full_Threshold_Assert_Value_wrch {15}
  CONFIG.Empty_Threshold_Assert_Value_wrch {13}
  CONFIG.FIFO_Implementation_rach {Independent_Clocks_Distributed_RAM}
  CONFIG.Full_Threshold_Assert_Value_rach {15}
  CONFIG.Empty_Threshold_Assert_Value_rach {13}
  CONFIG.FIFO_Implementation_rdch {Independent_Clocks_Builtin_FIFO}
  CONFIG.Empty_Threshold_Assert_Value_rdch {1018}
  CONFIG.FIFO_Implementation_axis {Independent_Clocks_Builtin_FIFO}
  CONFIG.Input_Depth_axis {512}
  CONFIG.Full_Threshold_Assert_Value_axis {511}
  CONFIG.Empty_Threshold_Assert_Value_axis {506}
} [get_ips dc_fifo_eth_side]

set_property -dict { 
  GENERATE_SYNTH_CHECKPOINT {1}
} $dc_fifo_eth_side

##################################################################

##################################################################
# CREATE IP fcch_fifo
##################################################################

set fcch_fifo [create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.2 -module_name fcch_fifo]

set_property -dict { 
  CONFIG.Fifo_Implementation {Common_Clock_Block_RAM}
  CONFIG.Performance_Options {First_Word_Fall_Through}
  CONFIG.Input_Data_Width {20}
  CONFIG.Input_Depth {1024}
  CONFIG.Output_Data_Width {20}
  CONFIG.Output_Depth {1024}
  CONFIG.Use_Embedded_Registers {false}
  CONFIG.Valid_Flag {true}
  CONFIG.Use_Extra_Logic {true}
  CONFIG.Data_Count_Width {11}
  CONFIG.Write_Data_Count_Width {11}
  CONFIG.Read_Data_Count_Width {11}
  CONFIG.Full_Threshold_Assert_Value {1023}
  CONFIG.Full_Threshold_Negate_Value {1022}
  CONFIG.Empty_Threshold_Assert_Value {4}
  CONFIG.Empty_Threshold_Negate_Value {5}
} [get_ips fcch_fifo]

set_property -dict { 
  GENERATE_SYNTH_CHECKPOINT {1}
} $fcch_fifo

##################################################################

##################################################################
# CREATE IP fifo_frames
##################################################################

set fifo_frames [create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.2 -module_name fifo_frames]

set_property -dict { 
  CONFIG.INTERFACE_TYPE {AXI_STREAM}
  CONFIG.Reset_Type {Asynchronous_Reset}
  CONFIG.Clock_Type_AXI {Common_Clock}
  CONFIG.TUSER_WIDTH {1}
  CONFIG.Enable_TLAST {true}
  CONFIG.FIFO_Implementation_wach {Common_Clock_Distributed_RAM}
  CONFIG.Full_Threshold_Assert_Value_wach {15}
  CONFIG.Empty_Threshold_Assert_Value_wach {14}
  CONFIG.FIFO_Implementation_wdch {Common_Clock_Builtin_FIFO}
  CONFIG.Empty_Threshold_Assert_Value_wdch {1022}
  CONFIG.FIFO_Implementation_wrch {Common_Clock_Distributed_RAM}
  CONFIG.Full_Threshold_Assert_Value_wrch {15}
  CONFIG.Empty_Threshold_Assert_Value_wrch {14}
  CONFIG.FIFO_Implementation_rach {Common_Clock_Distributed_RAM}
  CONFIG.Full_Threshold_Assert_Value_rach {15}
  CONFIG.Empty_Threshold_Assert_Value_rach {14}
  CONFIG.FIFO_Implementation_rdch {Common_Clock_Builtin_FIFO}
  CONFIG.Empty_Threshold_Assert_Value_rdch {1022}
  CONFIG.FIFO_Implementation_axis {Common_Clock_Block_RAM}
  CONFIG.FIFO_Application_Type_axis {Packet_FIFO}
  CONFIG.Input_Depth_axis {16384}
  CONFIG.Enable_Data_Counts_axis {false}
  CONFIG.Programmable_Full_Type_axis {Single_Programmable_Full_Threshold_Constant}
  CONFIG.Full_Threshold_Assert_Value_axis {9000}
  CONFIG.Programmable_Empty_Type_axis {No_Programmable_Empty_Threshold}
  CONFIG.Empty_Threshold_Assert_Value_axis {16382}
} [get_ips fifo_frames]

set_property -dict { 
  GENERATE_SYNTH_CHECKPOINT {1}
} $fifo_frames

##################################################################

##################################################################
# CREATE IP fifo_prebuffer
##################################################################

set fifo_prebuffer [create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.2 -module_name fifo_prebuffer]

set_property -dict { 
  CONFIG.synchronization_stages_axi {3}
  CONFIG.INTERFACE_TYPE {AXI_STREAM}
  CONFIG.Reset_Type {Asynchronous_Reset}
  CONFIG.Clock_Type_AXI {Independent_Clock}
  CONFIG.TUSER_WIDTH {1}
  CONFIG.Enable_TLAST {true}
  CONFIG.FIFO_Implementation_wach {Independent_Clocks_Distributed_RAM}
  CONFIG.Full_Threshold_Assert_Value_wach {15}
  CONFIG.Empty_Threshold_Assert_Value_wach {13}
  CONFIG.FIFO_Implementation_wdch {Independent_Clocks_Builtin_FIFO}
  CONFIG.Empty_Threshold_Assert_Value_wdch {1018}
  CONFIG.FIFO_Implementation_wrch {Independent_Clocks_Distributed_RAM}
  CONFIG.Full_Threshold_Assert_Value_wrch {15}
  CONFIG.Empty_Threshold_Assert_Value_wrch {13}
  CONFIG.FIFO_Implementation_rach {Independent_Clocks_Distributed_RAM}
  CONFIG.Full_Threshold_Assert_Value_rach {15}
  CONFIG.Empty_Threshold_Assert_Value_rach {13}
  CONFIG.FIFO_Implementation_rdch {Independent_Clocks_Builtin_FIFO}
  CONFIG.Empty_Threshold_Assert_Value_rdch {1018}
  CONFIG.FIFO_Implementation_axis {Independent_Clocks_Block_RAM}
  CONFIG.FIFO_Application_Type_axis {Data_FIFO}
  CONFIG.Input_Depth_axis {32768}
  CONFIG.Enable_Data_Counts_axis {true}
  CONFIG.Programmable_Full_Type_axis {Single_Programmable_Full_Threshold_Input_Port}
  CONFIG.Full_Threshold_Assert_Value_axis {32767}
  CONFIG.Programmable_Empty_Type_axis {Single_Programmable_Empty_Threshold_Input_Port}
  CONFIG.Empty_Threshold_Assert_Value_axis {32766}
} [get_ips fifo_prebuffer]

set_property -dict { 
  GENERATE_SYNTH_CHECKPOINT {1}
} $fifo_prebuffer

##################################################################

##################################################################
# CREATE IP fifo_timestamp
##################################################################

set fifo_timestamp [create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.2 -module_name fifo_timestamp]

set_property -dict { 
  CONFIG.Fifo_Implementation {Independent_Clocks_Block_RAM}
  CONFIG.synchronization_stages {3}
  CONFIG.Performance_Options {First_Word_Fall_Through}
  CONFIG.Input_Data_Width {40}
  CONFIG.Input_Depth {16}
  CONFIG.Output_Data_Width {40}
  CONFIG.Output_Depth {16}
  CONFIG.Use_Embedded_Registers {false}
  CONFIG.Reset_Pin {true}
  CONFIG.Enable_Reset_Synchronization {true}
  CONFIG.Reset_Type {Asynchronous_Reset}
  CONFIG.Full_Flags_Reset_Value {1}
  CONFIG.Use_Dout_Reset {true}
  CONFIG.Valid_Flag {true}
  CONFIG.Data_Count_Width {4}
  CONFIG.Write_Data_Count_Width {4}
  CONFIG.Read_Data_Count_Width {4}
  CONFIG.Full_Threshold_Assert_Value {15}
  CONFIG.Full_Threshold_Negate_Value {14}
  CONFIG.Empty_Threshold_Assert_Value {4}
  CONFIG.Empty_Threshold_Negate_Value {5}
  CONFIG.Enable_Safety_Circuit {true}
} [get_ips fifo_timestamp]

set_property -dict { 
  GENERATE_SYNTH_CHECKPOINT {1}
} $fifo_timestamp

##################################################################

##################################################################
# CREATE IP packet_len_fifo
##################################################################

set packet_len_fifo [create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.2 -module_name packet_len_fifo]

set_property -dict { 
  CONFIG.Fifo_Implementation {Common_Clock_Distributed_RAM}
  CONFIG.INTERFACE_TYPE {Native}
  CONFIG.Performance_Options {First_Word_Fall_Through}
  CONFIG.Input_Data_Width {14}
  CONFIG.Output_Data_Width {14}
  CONFIG.Use_Embedded_Registers {false}
  CONFIG.Valid_Flag {true}
  CONFIG.Overflow_Flag {true}
  CONFIG.Use_Extra_Logic {true}
  CONFIG.Data_Count {true}
  CONFIG.Data_Count_Width {11}
  CONFIG.Write_Data_Count_Width {11}
  CONFIG.Read_Data_Count_Width {11}
  CONFIG.Full_Threshold_Assert_Value {1023}
  CONFIG.Full_Threshold_Negate_Value {1022}
  CONFIG.Empty_Threshold_Assert_Value {4}
  CONFIG.Empty_Threshold_Negate_Value {5}
} [get_ips packet_len_fifo]

set_property -dict { 
  GENERATE_SYNTH_CHECKPOINT {1}
} $packet_len_fifo

##################################################################

##################################################################
# CREATE IP tof_fifo_tx
##################################################################

set tof_fifo_tx [create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.2 -module_name tof_fifo_tx]

set_property -dict { 
  CONFIG.Fifo_Implementation {Common_Clock_Block_RAM}
  CONFIG.INTERFACE_TYPE {Native}
  CONFIG.Performance_Options {First_Word_Fall_Through}
  CONFIG.asymmetric_port_width {false}
  CONFIG.Input_Data_Width {59}
  CONFIG.Input_Depth {32}
  CONFIG.Output_Data_Width {59}
  CONFIG.Output_Depth {32}
  CONFIG.Reset_Type {Asynchronous_Reset}
  CONFIG.Full_Flags_Reset_Value {1}
  CONFIG.Valid_Flag {true}
  CONFIG.Use_Extra_Logic {true}
  CONFIG.Data_Count_Width {6}
  CONFIG.Write_Data_Count_Width {6}
  CONFIG.Read_Data_Count_Width {6}
  CONFIG.Full_Threshold_Assert_Value {31}
  CONFIG.Full_Threshold_Negate_Value {30}
  CONFIG.Empty_Threshold_Assert_Value {4}
  CONFIG.Empty_Threshold_Negate_Value {5}
  CONFIG.Enable_Safety_Circuit {true}
} [get_ips tof_fifo_tx]

set_property -dict { 
  GENERATE_SYNTH_CHECKPOINT {1}
} $tof_fifo_tx

##################################################################

