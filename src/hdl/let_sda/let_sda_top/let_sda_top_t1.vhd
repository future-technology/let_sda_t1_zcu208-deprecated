-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 05/07/2021 11:40:33 AM
-- Design Name: 
-- Module Name: let_sda_top_t1 - Behavioral
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2022.1
-- Description: Top wrapper for all LET SDA modules
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
--library xil_defaultlib;
--library let_sda_lib;
library share_let_1g;
use share_let_1g.pkg_components;
library ethernet_framer_lib;
library ethernet_deframer_lib;
library frame_header_lib;
library frame_filter_lib;
library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;
library monitoring_lib;
--library ip;


entity let_sda_top_t1 is
    generic(
        G_MINIMUN_FREE_BYTES_TO_SPLIT : NATURAL := 8;
        G_STARTUP_DELAY               : INTEGER := 30000
    );
    Port ( 
        clk               : in  std_logic; -- Main clock 330 MHz
        rst               : in  std_logic;

        -- Input interface ETH
        ----------------------
        client_in_axis_if_m       : in t_axis_if_m;
        client_in_axis_if_s       : out t_axis_if_s;

        cfg_pause_data          : in std_logic_vector(15 downto 0);
		tx_pause_valid          : out std_logic;
        tx_pause_data           : out std_logic_vector(15 downto 0);
		rx_fifo_skipped_frame   : out std_logic_vector(32 - 1 downto 0);

        -- Output interface ETH
        ----------------------
        client_out_axis_if_m      : out t_axis_if_m;
        client_out_axis_if_s      : in t_axis_if_s;

        -- Input interface Creonic
        ----------------------
        fec_in_axis_if_m          : in t_axis_if_m;
        fec_in_axis_if_s          : out t_axis_if_s;

        --preamble_synced         : in std_logic;
        crc16_valid             : in std_logic;
		crc16_correct           : in std_logic;
		crc32_valid             : in std_logic;
		crc32_correct           : in std_logic;

        -- Output interface Creonic
        ----------------------
        fec_out_axis_if_m         : out t_axis_if_m;
        fec_out_axis_if_s         : in t_axis_if_s;

        -- Framer Health Monitor
        ----------------------
        framer_warning_flag                : out  std_logic;
        framer_critical_error_flag         : out  std_logic;
        framer_health_monitor_statistics   : out health_monitor_statistics_t;

        -- Deframer Health Monitor
        ----------------------
        deframer_warning_flag                : out  std_logic;
        deframer_critical_error_flag         : out  std_logic;
        deframer_health_monitor_statistics   : out health_monitor_statistics_t;

        -- FCCH
        ----------------------
        -- From Register Map
        fh_tx_fcch_if_m   : in t_fcch_if_m;
        fh_tx_fcch_if_s   : out t_fcch_if_s;


        -- To Register Map
        fh_rx_fcch_if_m   : out t_fcch_if_m;
        fh_rx_fcch_if_s   : in t_fcch_if_s;

        -- TIMESTAMP TBD
        ----------------------
        --ts_record_en       : std_logic;
        --ts_data_if         : t_ts_insertion_if;
        --ts_insertion_ready : std_logic;
               
        
        -- Statistics
        ----------------------
		clear_stat                      : in  std_logic;
        
        -- Framer
		framer_total_packet_counter            : out std_logic_vector(31 downto 0);
		framer_total_packet_splitted_counter   : out std_logic_vector(31 downto 0);
		framer_total_data_frame_counter        : out std_logic_vector(31 downto 0);
		framer_actual_data_frame_counter       : out std_logic_vector(31 downto 0);
		framer_total_idle_frame_counter        : out std_logic_vector(31 downto 0);
		framer_total_packet_drop				: out std_logic_vector(31 downto 0);
		framer_total_prebuffer_packet_drop      : out std_logic_vector(31 downto 0);
		framer_frame_length_error				: out std_logic_vector(31 downto 0);
		framer_bitrate_in				        : out std_logic_vector(31 downto 0);
		framer_packet_cnt_in				    : out std_logic_vector(31 downto 0);

        -- Deframer
        deframer_total_packet_counter            : out std_logic_vector(31 downto 0);
        deframer_total_payload_counter           : out std_logic_vector(31 downto 0);
        deframer_total_packet_splitted_counter   : out std_logic_vector(31 downto 0);
        deframer_total_packet_merged_counter     : out std_logic_vector(31 downto 0);
        deframer_total_data_frame_counter        : out std_logic_vector(31 downto 0);
        deframer_total_idle_frame_counter        : out std_logic_vector(31 downto 0);
        deframer_total_payload_error_counter     : out std_logic_vector(31 downto 0);
        deframer_total_packet_error_counter      : out std_logic_vector(31 downto 0);
        deframer_watchdog_reset_counter          : out std_logic_vector(31 downto 0);
        deframer_packet_filter_cnt_in			: out std_logic_vector(31 downto 0);
		deframer_packet_filter_cnt_out			: out std_logic_vector(31 downto 0);
        deframer_frame_filter_cnt_in             : out std_logic_vector(31 downto 0);
        deframer_frame_filter_cnt_out            : out std_logic_vector(31 downto 0);
        deframer_bitrate_out                     : out std_logic_vector(31 downto 0);
        deframer_packet_cnt_out                  : out std_logic_vector(31 downto 0);

        sda_debug                       : out sda_debug_t;

        -- Version control
        ----------------------
        version_mayor                   : out unsigned(7 downto 0);
        version_minor                   : out unsigned(7 downto 0)

    );
end let_sda_top_t1;

architecture Behavioral of let_sda_top_t1 is

constant C_VERSION_MAYOR : unsigned(7 downto 0) := x"01";
constant C_VERSION_MINOR : unsigned(7 downto 0) := x"02";

--signal deframer_in_axis_tdata       : std_logic_vector(8 - 1 downto 0);
--signal deframer_in_axis_tvalid      : std_logic;
--signal deframer_in_axis_tlast       : std_logic;
signal deframer_in_axis_tready_int      : std_logic;
--signal rx_fifo_skipped_frame        : std_logic_vector(31 downto 0);

--signal deframer_out_axis_tdata       : std_logic_vector(8 - 1 downto 0);
--signal deframer_out_axis_tvalid      : std_logic;
--signal deframer_out_axis_tlast       : std_logic;
signal deframer_out_axis_tready_int    : std_logic;
--signal deframer_out_axis_tuser       : std_logic;

signal framer_out_axis_tdata_int  : std_logic_vector(8 - 1 downto 0);
signal framer_out_axis_tvalid_int : std_logic;
signal framer_out_axis_tlast_int  : std_logic;
        
signal filter_axis_tdata  : std_logic_vector(8 - 1 downto 0);
signal filter_axis_tvalid : std_logic;
signal filter_axis_tlast  : std_logic;
signal filter_axis_tready : std_logic;

signal fh_tx_axis_payload_if_m : t_axis_payload_if_m; -- Input axi-s
signal fh_tx_axis_payload_if_s : t_axis_payload_if_s; -- Input axi-s
signal fh_tx_axis_if_m         : t_axis_if_m; -- Output axi-s 
signal fh_tx_axis_if_s         : t_axis_if_s; -- Output axi-s 
signal ts_tx_axis_if_m         : t_axis_if_m; -- Output axi-s 
signal ts_tx_axis_if_s         : t_axis_if_s; -- Output axi-s 
signal fh_tx_ack_data_if_m     : t_ack_data_if_m; -- ACK data interface
signal fh_tx_ack_data_if_s     : t_ack_data_if_s; -- ACK data interface
signal fifo_tx_fcch_if_m         : t_fcch_if_m; -- FCCH interface
signal fifo_tx_fcch_if_s         : t_fcch_if_s; -- FCCH interface

signal fh_rx_axis_payload_if_m : t_axis_payload_if_m; -- Input axi-s
signal fh_rx_axis_payload_if_s : t_axis_payload_if_s; -- Input axi-s
signal fh_rx_axis_if_m         : t_axis_if_m; -- Output axi-s 
signal fh_rx_axis_if_s         : t_axis_if_s; -- Output axi-s 
signal fh_rx_ack_data_if_m     : t_ack_data_if_m; -- ACK data interface
signal fh_rx_ack_data_if_s     : t_ack_data_if_s; -- ACK data interface
signal fifo_rx_fcch_if_m         : t_fcch_if_m; -- FCCH interface     
signal fifo_rx_fcch_if_s         : t_fcch_if_s; -- FCCH interface     

signal framer_out_axis_payload_if_m : t_axis_payload_if_m;
signal framer_out_axis_payload_if_s : t_axis_payload_if_s;
signal deframer_in_axis_payload_if_m : t_axis_payload_if_m;
signal deframer_in_axis_payload_if_s : t_axis_payload_if_s;

signal ts_insertion_if : t_ts_insertion_if;
signal ts_valid        : std_logic;

-- Timestamp for Rx chain:
signal frame_ts_valid     : std_logic;
signal frame_ts_if        : t_frame_ts_if;
signal ts_association_if  : t_ts_association_if;
signal ts_association_valid : std_logic;

--signal framer_out_axis_tdata         : std_logic_vector(8 - 1 downto 0);
--signal framer_out_axis_tvalid        : std_logic;
--signal framer_out_axis_tlast         : std_logic;
--signal framer_out_axis_tready        : std_logic;
signal eth_rx_fifo_axis_tvalid       : STD_LOGIC;
signal eth_rx_fifo_axis_tready       : STD_LOGIC;
signal eth_rx_fifo_axis_tdata        : STD_LOGIC_VECTOR(7 DOWNTO 0);
signal eth_rx_fifo_axis_tlast        : STD_LOGIC;
signal eth_rx_fifo_axis_tuser        : STD_LOGIC;

--signal framer_diagnostics                       : framer_diagnostics_t;
--signal deframer_diagnostics                     : deframer_diagnostics_t;
--signal sda_debug                                : sda_debug_t;
--signal framer_health_monitor_statistics         : health_monitor_statistics_t;
--signal deframer_health_monitor_statistics       : health_monitor_statistics_t;
signal reserved                                 : std_logic_vector(6 downto 0);
signal clear_framer_statistics                  : std_logic;
signal clear_deframer_statistics                : std_logic;
--signal framer_monitor_warning_flag              : std_logic;
--signal framer_monitor_critical_error_flag       : std_logic;    
--signal deframer_monitor_warning_flag            : std_logic;
--signal deframer_monitor_critical_error_flag     : std_logic;

signal axis_mac_tx_tdata  : std_logic_vector ( 7 downto 0 );
signal axis_mac_tx_tkeep  : std_logic_vector ( 0 downto 0 );
signal axis_mac_tx_tlast  : std_logic;
signal axis_mac_tx_tready : std_logic;
signal axis_mac_tx_tuser  : std_logic;
signal axis_mac_tx_tvalid : std_logic;

-- From Framer
---------------------- 
signal tx_timing_data          :  tx_timing_data_t;
signal tx_timing_data_valid    :  std_logic;

-- From deframer
---------------------- 
signal rx_timing_data          :  rx_timing_data_t;
signal rx_timing_data_valid    :  std_logic;

-- TIMESTAMP
----------------------
signal ts_record_en       : std_logic;
signal ts_data_if         : t_ts_insertion_if;
signal ts_insertion_ready : std_logic;

-- Output data: Time of flight in picoseconds
----------------------
--signal tof_counter_valid   :  std_logic;
--signal tof_counter         :  std_logic_vector(40-1 downto 0);


--	signal timestamp_tx        : std_logic_vector(31 downto 0);
signal preamble_valid_tx  : std_logic;
--	signal timestamp_rx        : std_logic_vector(31 downto 0);
signal preamble_valid_rx  : std_logic;

signal timestamp_tx        : std_logic_vector(40-1 downto 0);
signal timestamp_valid_tx  : std_logic;
signal timestamp_rx        : std_logic_vector(40-1 downto 0);
signal timestamp_valid_rx  : std_logic;

signal let_timestamp_tx        : std_logic_vector(40-1 downto 0);
signal let_timestamp_valid_tx  : std_logic;
signal let_timestamp_rd_en_tx  : std_logic;
signal let_timestamp_rx        : std_logic_vector(40-1 downto 0);
signal let_timestamp_valid_rx  : std_logic;
signal let_timestamp_rd_en_rx  : std_logic;

-- SDA compliant ranging interface
----------------------
--signal trigger            :  std_logic;

--Framer:
signal t1_tx_fn           :  std_logic_vector(15 downto 0) := (others=>'0');
signal t1_tx_fn_valid     :  std_logic;
signal send_ranging_req   :  std_logic;
signal send_mgmt_response       :  mgmt_payload_t;
signal send_mgmt_response_valid :  std_logic;
signal send_mgmt_response_ack   :  std_logic;
signal send_t3                  :  std_logic;
signal t3_tx_fn                :  std_logic_vector(15 downto 0):= (others=>'0');
signal t3_tx_fn_valid          :  std_logic;

--Deframer:
signal mgmt_response       :  mgmt_payload_t := C_MGMT_RESPONSE_INIT;
signal mgmt_response_valid :  std_logic := '0';
-------------------------------------------

signal ranging_sync_delay   :  std_logic_vector(15 downto 0);
signal ranging_sync_period  :  std_logic_vector(15 downto 0);
signal preamble_sync        :  std_logic;
signal reset_timer          :  std_logic;

-- FCCH
signal tx_fcch_opcode       :  std_logic_vector(4-1 downto 0);
signal tx_fcch_pl           :  std_logic_vector(16-1 downto 0);
signal tx_fcch_valid        :  std_logic;
signal tx_fcch_rd_en        :  std_logic;

signal rx_fcch_data         :  std_logic_vector(20-1 downto 0);
signal rx_fcch_valid        :  std_logic;
signal rst_let_n            : std_logic;

begin

    version_mayor <= C_VERSION_MAYOR;
    version_minor <= C_VERSION_MINOR;
    fec_out_axis_if_m.tstart <= '0';

    Inst_frame_filter_top : entity frame_filter_lib.frame_filter_top
    PORT map(
      clk 						=> clk,
      rst 						=> rst,
      s_axis_tvalid 			=> fec_in_axis_if_m.tvalid, --deframer_in_axis_tvalid,
      s_axis_tready 			=> fec_in_axis_if_s.tready, --deframer_in_axis_tready_int,
      s_axis_tdata 				=> fec_in_axis_if_m.tdata, --deframer_in_axis_tdata,
      s_axis_tlast 				=> fec_in_axis_if_m.tlast, --deframer_in_axis_tlast,
      m_axis_tvalid 			=> fh_rx_axis_if_m.tvalid,
      m_axis_tready 			=> fh_rx_axis_if_s.tready,
      m_axis_tdata 				=> fh_rx_axis_if_m.tdata,
      m_axis_tlast 				=> fh_rx_axis_if_m.tlast,
      m_axis_tstart 			=> fh_rx_axis_if_m.tstart,
      crc16_valid 				=> crc16_valid,
      crc16_correct 			=> crc16_correct,
      crc32_valid 				=> crc32_valid,
      crc32_correct 			=> crc32_correct,
      packet_cnt_in	            => deframer_frame_filter_cnt_in,
      packet_cnt_out	        => deframer_frame_filter_cnt_out
    );

--    Inst_deframer_health_monitor : entity monitoring_lib.frame_health_monitor
--    Port map( 
--        clk            => clk,
--        rst            => rst,
--
--        -- Input data from encoder
--        ----------------------
--        s_axis_tdata     => deframer_in_axis_payload_if.axis_if.tdata, -- deframer_in_axis_tdata,
--        s_axis_tvalid    => deframer_in_axis_payload_if.axis_if.tvalid, -- deframer_in_axis_tvalid,
--        s_axis_tlast     => deframer_in_axis_payload_if.axis_if.tlast, -- deframer_in_axis_tlast,
--        s_axis_tready    => deframer_in_axis_payload_if.axis_if.tready, -- deframer_in_axis_tready_int,
--
--        -- ERROR FLAGS
--        warning_flag            => deframer_warning_flag,
--        critical_error_flag     => deframer_critical_error_flag,
--        health_monitor_statistics => deframer_health_monitor_statistics
--    );

--    Inst_framer_health_monitor : entity monitoring_lib.frame_health_monitor
--    Port map( 
--        clk            => clk,
--        rst            => rst,
--
--        -- Input data from encoder
--        ----------------------
--        s_axis_tdata     => framer_out_axis_payload_if.axis_if.tdata, -- framer_out_axis_tdata_int,
--        s_axis_tvalid    => framer_out_axis_payload_if.axis_if.tvalid, --framer_out_axis_tvalid_int,
--        s_axis_tlast     => framer_out_axis_payload_if.axis_if.tlast, --framer_out_axis_tlast_int,
--        s_axis_tready    => framer_out_axis_payload_if.axis_if.tready, --framer_out_axis_tready,
--
--        -- ERROR FLAGS
--        warning_flag            => framer_warning_flag,
--        critical_error_flag     => framer_critical_error_flag,
--        health_monitor_statistics => framer_health_monitor_statistics
--    );

    -- Framer Top Inst:
    ethernet_framer_top : entity ethernet_framer_lib.ethernet_framer_top 
    generic map (
        G_MINIMUN_FREE_BYTES_TO_SPLIT   => G_MINIMUN_FREE_BYTES_TO_SPLIT, --8,
        G_STARTUP_DELAY                 => G_STARTUP_DELAY --30000
    )
    port map(
        clk        => clk, -- : in  std_logic;
        rst        => rst, -- : in  std_logic;

        -- Input data 
        ----------------------
        axis_if_m          => client_in_axis_if_m, --: inout t_axis_if;
        axis_if_s          => client_in_axis_if_s, --: inout t_axis_if;

        -- Pause frame IF 
        ----------------------
        cfg_pause_data        => cfg_pause_data, -- TBD
        tx_pause_valid        => tx_pause_valid,
        tx_pause_data         => tx_pause_data,
        tx_pause_ready        => '1', -- not used
        rx_fifo_skipped_frame => rx_fifo_skipped_frame,
        
        -- Output data 
        ----------------------
        axis_payload_if_m  => framer_out_axis_payload_if_m, -- : inout t_axis_payload_if;
        axis_payload_if_s  => framer_out_axis_payload_if_s, -- : inout t_axis_payload_if;

        -- Statistics
        ----------------------            
        clear_stat                      => clear_stat,
        total_packet_counter            => framer_total_packet_counter            , --: out std_logic_vector(31 downto 0);
		total_packet_splitted_counter   => framer_total_packet_splitted_counter   , --: out std_logic_vector(31 downto 0);
		total_data_frame_counter        => framer_total_data_frame_counter        , --: out std_logic_vector(31 downto 0);
		actual_data_frame_counter       => framer_actual_data_frame_counter       , --: out std_logic_vector(31 downto 0);
		total_idle_frame_counter        => framer_total_idle_frame_counter        , --: out std_logic_vector(31 downto 0);
		total_packet_drop               => framer_total_packet_drop               , --: out std_logic_vector(31 downto 0);
		total_prebuffer_packet_drop     => framer_total_prebuffer_packet_drop     , --: out std_logic_vector(31 downto 0);
        frame_length_error              => framer_frame_length_error              , --: out std_logic_vector(31 downto 0)
		bitrate_in				        => framer_bitrate_in                      , --: out std_logic_vector(31 downto 0);
		packet_cnt_in				    => framer_packet_cnt_in                     --: out std_logic_vector(31 downto 0);
    );
    
    -- instantiate Deframer here 
    ethernet_deframer_top : entity ethernet_deframer_lib.ethernet_deframer_top 
    port map ( 
        clk          => clk,
        rst          => rst,

        -- Input data 
        ----------------------
        axis_payload_if_m               => fh_rx_axis_payload_if_m, --deframer_in_axis_payload_if, --: inout t_axis_payload_if;
        axis_payload_if_s               => fh_rx_axis_payload_if_s, --deframer_in_axis_payload_if, --: inout t_axis_payload_if;

        -- Output data to ethernet mac
        ----------------------
        axis_if_m                       => client_out_axis_if_m, --: inout t_axis_if;
        axis_if_s                       => client_out_axis_if_s, --: inout t_axis_if;

        -- Statistics
        ----------------------
        clear_stat                      => clear_stat,
        total_packet_counter            => deframer_total_packet_counter         , --: out std_logic_vector(31 downto 0);
        total_payload_counter           => deframer_total_payload_counter        , --: out std_logic_vector(31 downto 0);
        total_packet_splitted_counter   => deframer_total_packet_splitted_counter, --: out std_logic_vector(31 downto 0);
        total_packet_merged_counter     => deframer_total_packet_merged_counter  , --: out std_logic_vector(31 downto 0);
        total_data_frame_counter        => deframer_total_data_frame_counter     , --: out std_logic_vector(31 downto 0);
        total_idle_frame_counter        => deframer_total_idle_frame_counter     , --: out std_logic_vector(31 downto 0);
        total_payload_error_counter     => deframer_total_payload_error_counter  , --: out std_logic_vector(31 downto 0);
        total_packet_error_counter      => deframer_total_packet_error_counter   , --: out std_logic_vector(31 downto 0);
        watchdog_reset_counter          => deframer_watchdog_reset_counter       , --: out std_logic_vector(31 downto 0);
        packet_filter_cnt_in            => deframer_packet_filter_cnt_in         , --: out std_logic_vector(31 downto 0);
		packet_filter_cnt_out           => deframer_packet_filter_cnt_out        , --: out std_logic_vector(31 downto 0);
        bitrate_out                     => deframer_bitrate_out,
        packet_cnt_out                  => deframer_packet_cnt_out, --: out std_logic_vector(31 downto 0);
        sda_debug                       => sda_debug

    );

    inst_frame_header_tx : entity frame_header_lib.frame_header_tx
        port map(
            clk             => clk                       , --: in    std_logic;
            reset           => rst                       , --: in    std_logic;
            axis_payload_if_m => framer_out_axis_payload_if_m, --fh_tx_axis_payload_if , --: inout t_axis_payload_if; -- Input axi-s
            axis_payload_if_s => framer_out_axis_payload_if_s, --fh_tx_axis_payload_if , --: inout t_axis_payload_if; -- Input axi-s
            axis_if_m         => ts_tx_axis_if_m, --fec_out_axis_if_m           , --fh_tx_axis_if         , --: inout t_axis_if; -- Output axi-s
            axis_if_s         => ts_tx_axis_if_s, --fec_out_axis_if_s           , --fh_tx_axis_if         , --: inout t_axis_if; -- Output axi-s
            ack_data_if_m     => fh_tx_ack_data_if_m         , --: inout t_ack_data_if; -- ACK data interface
            ack_data_if_s     => fh_tx_ack_data_if_s         , --: inout t_ack_data_if; -- ACK data interface
            fcch_if_m         => fifo_tx_fcch_if_m             , --: inout t_fcch_if; -- FCCH interface
            fcch_if_s         => fifo_tx_fcch_if_s             , --: inout t_fcch_if; -- FCCH interface
            pl_rate         => (others=>'0') -- TODO: pl_rate          --: in    std_logic_vector(3 downto 0) -- Payload data rate
        );
        --TODO: connect this interface when ARQ integration:
        fh_tx_ack_data_if_m <= C_ACK_DATA_IF_MASTER_INIT;

    inst_frame_header_rx : entity frame_header_lib.frame_header_rx
        port map(
            clk             => clk                  , --: in    std_logic;
            reset           => rst                  , --: in    std_logic;
            axis_if_m         => fh_rx_axis_if_m        , --: inout t_axis_if; -- Output axi-s
            axis_if_s         => fh_rx_axis_if_s        , --: inout t_axis_if; -- Output axi-s
            axis_payload_if_m => fh_rx_axis_payload_if_m, --: inout t_axis_payload_if; -- Input axi-s
            axis_payload_if_s => fh_rx_axis_payload_if_s, --: inout t_axis_payload_if; -- Input axi-s
            ack_data_if_m     => fh_rx_ack_data_if_m    , --: inout t_ack_data_if; -- ACK data interface
            ack_data_if_s     => fh_rx_ack_data_if_s    , --: inout t_ack_data_if; -- ACK data interface
            fcch_if_m         => fifo_rx_fcch_if_m        , --: inout t_fcch_if; -- FCCH interface
            fcch_if_s         => fifo_rx_fcch_if_s        , --: inout t_fcch_if; -- FCCH interface
            ts_record_en        => ts_record_en      , --: in std_logic;
            ts_data_if          => ts_data_if        , --: out t_ts_insertion_if;
            ts_insertion_ready  => ts_insertion_ready, --: in std_logic;
            pl_rate         => open -- TODO: pl_rate         --: out   std_logic_vector(3 downto 0) -- Payload data rate
        );
        --TODO: connect this interface when ARQ integration:
        fh_rx_ack_data_if_s <= C_ACK_DATA_IF_SLAVE_INIT;
        ts_insertion_ready <= '1';
    -------------------------------------------
    --- Timestamp insertion for TX chain
    -------------------------------------------
    inst_timestamp_insertion : entity frame_header_lib.timestamp_insertion
        generic map(
            G_INSERTION_BYTE_OFFSET => 7, --: natural := 7;
            G_INSERTION_BIT_OFFSET  => 1  --: natural := 1
        )
        port map(
            clk             => clk, --: in    std_logic;
            reset           => rst, --: in    std_logic;
    
            ts_insertion_if => ts_insertion_if, --: in t_ts_insertion_if;
            ts_valid        => ts_valid, --: in std_logic;
    
            axis_if_in_m    => ts_tx_axis_if_m, --axis_if_in_m, --: in t_axis_if_m; -- Input axi-s
            axis_if_in_s    => ts_tx_axis_if_s, --axis_if_in_s, --: out t_axis_if_s; -- Input axi-s
            axis_if_out_m   => fec_out_axis_if_m, --: out t_axis_if_m; -- Output axi-s
            axis_if_out_s   => fec_out_axis_if_s --: in t_axis_if_s -- Output axi-s
        );
        ts_insertion_if.tx_ts       <= (others=>'0');
        ts_insertion_if.tod_seconds <= (others=>'0');
        ts_insertion_if.ts_applies <= (others=>'0');
        ts_valid <= '1';
        
    -------------------------------------------
    --- Timestamp association for RX chain
    -------------------------------------------
    inst_timestamp_association : entity frame_header_lib.timestamp_association
        port map(
            clk             => clk, --: in    std_logic;
            reset           => rst, --: in    std_logic;
    
            frame_ts_valid  => frame_ts_valid, --: in std_logic;
            frame_ts_if     => frame_ts_if, --: in t_frame_ts_if;
            axis_if_in_m    => fh_rx_axis_if_m, --: in t_axis_if_m; -- Input axi-s
            axis_if_in_s    => fh_rx_axis_if_s, --: in t_axis_if_s; -- Input axi-s
    
            ts_association_if    => ts_association_if, --: out t_ts_association_if;
            ts_association_valid => ts_association_valid  --: out std_logic;
        );

    -------------------------------------------
    --- FIFOS for FCCH tx
    -------------------------------------------
--    inst_fcch_tx_fifo_payload : entity share_let_1g.sync_fifo
--        generic map(
--            DISTR_RAM       => true,
--            FIFO_DEPTH      => 16,
--            DATA_WIDTH      => 16,
--            FULL_THRESHOLD  => 16,
--            EMPTY_THRESHOLD => 0
--        )
--        port map(
--            clk                => clk,
--            rst                => rst,
--            -- Input data handling
--            ----------------------
--            input              => fh_tx_fcch_if.payload,
--            input_valid        => fh_tx_fcch_if.valid,
--            input_ready        => fh_tx_fcch_if.rd_ena,
--            input_almost_full  => open,
--            input_almost_empty => open,
--            fifo_max_level     => open,
--            fifo_data_count    => open,
--            -- Output data handling
--            -----------------------
--            output             => fifo_tx_fcch_if.payload,
--            output_valid       => fifo_tx_fcch_if.valid  ,
--            output_ready       => fifo_tx_fcch_if.rd_ena  
--        );
--    inst_fcch_tx_fifo_opcode : entity share_let_1g.sync_fifo
--        generic map(
--            DISTR_RAM       => true,
--            FIFO_DEPTH      => 16,
--            DATA_WIDTH      => 4,
--            FULL_THRESHOLD  => 16,
--            EMPTY_THRESHOLD => 0
--        )
--        port map(
--            clk                => clk,
--            rst                => rst,
--            -- Input data handling
--            ----------------------
--            input              => fh_tx_fcch_if.opcode,
--            input_valid        => fh_tx_fcch_if.valid,
--            input_ready        => open, --fh_tx_fcch_if.rd_ena,
--            input_almost_full  => open,
--            input_almost_empty => open,
--            fifo_max_level     => open,
--            fifo_data_count    => open,
--            -- Output data handling
--            -----------------------
--            output             => fifo_tx_fcch_if.opcode,
--            output_valid       => open, --fifo_tx_fcch_if.valid ,
--            output_ready       => fifo_tx_fcch_if.rd_ena  
--        );
fifo_tx_fcch_if_m.valid <= '1';

    -------------------------------------------
    --- FIFOS for FCCH Rx
    -------------------------------------------
--    inst_fcch_rx_fifo_payload : entity share_let_1g.sync_fifo
--        generic map(
--            DISTR_RAM       => true,
--            FIFO_DEPTH      => 16,
--            DATA_WIDTH      => 16,
--            FULL_THRESHOLD  => 16,
--            EMPTY_THRESHOLD => 0
--        )
--        port map(
--            clk                => clk,
--            rst                => rst,
--            -- Input data handling
--            ----------------------
--            input              => fifo_rx_fcch_if.payload,
--            input_valid        => fifo_rx_fcch_if.valid,
--            input_ready        => fifo_rx_fcch_if.rd_ena,
--            input_almost_full  => open,
--            input_almost_empty => open,
--            fifo_max_level     => open,
--            fifo_data_count    => open,
--            -- Output data handling
--            -----------------------
--            output             => fh_rx_fcch_if.payload,
--            output_valid       => fh_rx_fcch_if.valid  ,
--            output_ready       => fh_rx_fcch_if.rd_ena  
--        );
--        
--    inst_fcch_rx_fifo_opcode : entity share_let_1g.sync_fifo
--        generic map(
--            DISTR_RAM       => true,
--            FIFO_DEPTH      => 16,
--            DATA_WIDTH      => 4,
--            FULL_THRESHOLD  => 16,
--            EMPTY_THRESHOLD => 0
--        )
--        port map(
--            clk                => clk,
--            rst                => rst,
--            -- Input data handling
--            ----------------------
--            input              => fifo_rx_fcch_if.opcode,
--            input_valid        => fifo_rx_fcch_if.valid,
--            input_ready        => open, --fifo_rx_fcch_if.rd_ena,
--            input_almost_full  => open,
--            input_almost_empty => open,
--            fifo_max_level     => open,
--            fifo_data_count    => open,
--            -- Output data handling
--            -----------------------
--            output             => fh_rx_fcch_if.opcode,
--            output_valid       => open, --fh_rx_fcch_if.valid ,
--            output_ready       => fh_rx_fcch_if.rd_ena  
--        );
fifo_rx_fcch_if_s.rd_ena <= '1';
-- TODO: This needs to be implemented in LET top
--  Inst_bit_sync : entity share_let_1g.bit_sync
--  port map(
--      clk_in   => clk_tx_eth,
--      data_in  => deframer_out_axis_tready,
--      data_out => tx_pause_ready
--  );

end Behavioral;