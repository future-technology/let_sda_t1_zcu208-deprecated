----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 08/17/2021 11:40:33 AM
-- Design Name: 
-- Module Name: let_sda_top_ranging_auto_tb - Behavioral
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2018.3
-- Description: Testbench for LET SDA Top wrapper
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;

library frame_encoding;
library frame_decoding;
--use frame_decoding.pkg_components.all;
use frame_encoding.pkg_support.all;
use frame_encoding.pkg_components.all;
library ethernet_framer_lib;
library ethernet_deframer_lib;
library sda_lib;

use sda_lib.pkg_sda.all;
--use ranging_lib.package_components.all;
library let_sda_lib;

entity let_sda_top_ranging_auto_tb is
	generic(

	-- clock period
	CLK_PERIOD     : time    := 5.000 ns;

	-- Number of frames to simulate
	NUM_FRAME   : natural := 30;

	-- Which level has an assertion when it fails?
	ASSERTION_SEVERITY  : severity_level := FAILURE

	);
end let_sda_top_ranging_auto_tb;


architecture sim of let_sda_top_ranging_auto_tb is

	constant preamble : std_logic_vector(71 downto 0) := X"77AD5B5843641E2E26";
	constant FRAME_SIZE  : natural := 239 * 8 + 14 - 4;
	constant C_TOF_DELAY  : TIME := 0.833*100 ns; -- TIME OF FLIGHT BETWEEN TERMINAL A AND B  (Real case-> 8000 km -> 27 ms)
	constant C_50KHZ_INC :time := 0.0035 ns;
	signal C_PHASE_A : time := 0 ns;
	signal C_PHASE_B : time := 0 ns;
	signal C_FREQ_A : time := 8.333 ns;
	signal C_FREQ_B : time := 8.333 ns;

	signal V_PHASE_A : time := 0 ns;
	signal V_PHASE_B : time := 0 ns;
	signal V_FREQ_A : time := 8.333 ns;
	signal V_FREQ_B : time := 8.333 ns;
	
	signal test_stage : integer range 0 to 100 := 0;

	signal clk                 : std_logic := '0';
	signal clk200              : std_logic := '0';
	signal mac_rx_clk_out      : std_logic := '0';
	signal rst                 : std_logic;
	signal reset_sync_200      : std_logic;
	signal mac_rx_reset        : std_logic;
	signal test_enable        	: std_logic;
	signal test_start        	: std_logic;
	signal test_finished       : std_logic;

	signal ta_disp_sum        : std_logic_vector(23 downto 0);
	signal tb_disp_sum        : std_logic_vector(23 downto 0);

	signal tx_ts_tx  		: std_logic_vector(40-1 downto 0);
	signal tx_ts_valid_tx  	: std_logic;

	-- configuration for the preamble sync algorithm
	signal cfg_aquisition_length  : std_logic_vector(32 - 1 downto 0);
	signal cfg_tracking_length    : std_logic_vector(32 - 1 downto 0);
	signal cfg_max_tracking_error : std_logic_vector(32 - 1 downto 0);
	
	signal ta_framer_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal ta_framer_axis_tvalid   : std_logic;
	signal ta_framer_axis_tlast    : std_logic;
	signal ta_framer_axis_tready   : std_logic;
	signal ta_deframer_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal ta_deframer_axis_tvalid   : std_logic;
	signal ta_deframer_axis_tlast    : std_logic;
	signal ta_deframer_axis_tready   : std_logic;	

	signal tb_framer_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal tb_framer_axis_tvalid   : std_logic;
	signal tb_framer_axis_tlast    : std_logic;
	signal tb_framer_axis_tready   : std_logic;
	signal tb_deframer_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal tb_deframer_axis_tvalid   : std_logic;
	signal tb_deframer_axis_tlast    : std_logic;
	signal tb_deframer_axis_tready   : std_logic;	
	
	signal ta_ch_tx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal ta_ch_tx_axis_tvalid   : std_logic;
	signal ta_ch_tx_axis_tlast    : std_logic;
	signal ta_ch_tx_axis_tready   : std_logic := '0';
	signal ta_ch_rx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal ta_ch_rx_axis_tdata_noise   : std_logic_vector(8 - 1 downto 0);
	signal ta_ch_rx_axis_tvalid   : std_logic;
	signal ta_ch_rx_axis_tready   : std_logic := '0';

	signal tb_ch_tx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal tb_ch_tx_axis_tvalid   : std_logic;
	signal tb_ch_tx_axis_tlast    : std_logic;
	signal tb_ch_tx_axis_tready   : std_logic := '0';
	signal tb_ch_rx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal tb_ch_rx_axis_tdata_noise   : std_logic_vector(8 - 1 downto 0);
	signal tb_ch_rx_axis_tvalid   : std_logic;
	signal tb_ch_rx_axis_tready   : std_logic := '0';

	signal loopback_ch_tx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal loopback_ch_tx_axis_tvalid   : std_logic;
	signal loopback_ch_tx_axis_tvalid_mux   : std_logic;
	signal loopback_ch_tx_axis_tlast    : std_logic;
	signal loopback_ch_tx_axis_tready   : std_logic := '0';
	signal loopback_ch_rx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal loopback_ch_rx_axis_tdata_noise   : std_logic_vector(8 - 1 downto 0);
	signal loopback_ch_rx_axis_tvalid   : std_logic;
	signal loopback_ch_rx_axis_tready   : std_logic := '0';
	signal loopback_ch_tx_axis_tready_mux   : std_logic := '0';
	signal ready_ctrl   : std_logic := '0';

	signal ta_tx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal ta_tx_axis_tvalid   : std_logic;
	signal ta_tx_axis_tlast    : std_logic;
	signal ta_tx_axis_tready   : std_logic := '0';
	signal tb_tx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal tb_tx_axis_tvalid   : std_logic;
	signal tb_tx_axis_tlast    : std_logic;
	signal tb_tx_axis_tready   : std_logic := '0';

	signal ta_rx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal ta_rx_axis_tvalid   : std_logic;
	signal ta_rx_axis_tlast    : std_logic;
	signal ta_rx_axis_tready   : std_logic;	
	signal tb_rx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal tb_rx_axis_tvalid   : std_logic;
	signal tb_rx_axis_tlast    : std_logic;
	signal tb_rx_axis_tready   : std_logic;

	signal ta_crc16_valid       : std_logic;
	signal ta_crc16_correct     : std_logic;
	signal ta_crc32_valid       : std_logic;
	signal ta_crc32_correct     : std_logic;
	signal ta_payload_cerr      : std_logic_vector(4 - 1 downto 0);
	signal ta_payload_ncerr     : std_logic;
	signal ta_payload_err_valid : std_logic;
	signal ta_header_cerr       : std_logic_vector(4 - 1 downto 0);
	signal ta_header_ncerr      : std_logic;
	signal ta_header_err_valid  : std_logic;	

	signal tb_crc16_valid       : std_logic;
	signal tb_crc16_correct     : std_logic;
	signal tb_crc32_valid       : std_logic;
	signal tb_crc32_correct     : std_logic;
	signal tb_payload_cerr      : std_logic_vector(4 - 1 downto 0);
	signal tb_payload_ncerr     : std_logic;
	signal tb_payload_err_valid : std_logic;
	signal tb_header_cerr       : std_logic_vector(4 - 1 downto 0);
	signal tb_header_ncerr      : std_logic;
	signal tb_header_err_valid  : std_logic;

	-- More diagnostic informations
	-------------------------------------------------------
	signal ta_clear_stat          : std_logic := '0';
	signal ta_total_frame_counter : std_logic_vector(31 downto 0);
	signal ta_crc_error_counter   : std_logic_vector(31 downto 0);
	signal ta_crc16_err_counter   : std_logic_vector(31 downto 0);
	signal ta_crc32_err_counter   : std_logic_vector(31 downto 0);
	signal ta_header_corrections  : std_logic_vector(31 downto 0);
	signal ta_payload_corrections : std_logic_vector(31 downto 0);
	signal ta_lpc_frame_counter   : std_logic_vector(31 downto 0);
	signal ta_lpc_corrections     : std_logic_vector(31 downto 0);	
	signal tb_clear_stat          : std_logic := '0';
	signal tb_total_frame_counter : std_logic_vector(31 downto 0);
	signal tb_crc_error_counter   : std_logic_vector(31 downto 0);
	signal tb_crc16_err_counter   : std_logic_vector(31 downto 0);
	signal tb_crc32_err_counter   : std_logic_vector(31 downto 0);
	signal tb_header_corrections  : std_logic_vector(31 downto 0);
	signal tb_payload_corrections : std_logic_vector(31 downto 0);
	signal tb_lpc_frame_counter   : std_logic_vector(31 downto 0);
	signal tb_lpc_corrections     : std_logic_vector(31 downto 0);

		-- Stats from the preamble sync
	signal ta_new_search_counter  : std_logic_vector(31 downto 0);
	signal ta_sync_loss_counter   : std_logic_vector(31 downto 0);	
	signal tb_new_search_counter  : std_logic_vector(31 downto 0);
	signal tb_sync_loss_counter   : std_logic_vector(31 downto 0);

	signal byte_cnt         : natural;
	signal got_frame_id     : std_logic;


	signal clk10     : std_logic := '0';
	signal rst_clk10 : std_logic;

	signal tx_reset  : std_logic := '0';
	signal rx_reset  : std_logic := '0';

	-- // Differential reference clock inputs
	signal ta_mgtrefclk0_x0y4_p : std_logic := '0';
	signal ta_mgtrefclk0_x0y4_n : std_logic := '0';
	signal tb_mgtrefclk0_x0y4_p : std_logic := '0';
	signal tb_mgtrefclk0_x0y4_n : std_logic := '0';

	-- // Serial data ports for transceiver channel 0
	signal ta_ch0_gthrxn_in  : std_logic;
	signal ta_ch0_gthrxp_in  : std_logic;
	signal ta_ch0_gthtxn_out : std_logic;
	signal ta_ch0_gthtxp_out : std_logic;

	signal tb_ch0_gthrxn_in  : std_logic;
	signal tb_ch0_gthrxp_in  : std_logic;
	signal tb_ch0_gthtxn_out : std_logic;
	signal tb_ch0_gthtxp_out : std_logic;
	
    signal ta_data_good_in   : std_logic;
    signal tb_data_good_in   : std_logic;
	signal data_good_in_d : std_logic;

	-- Output data: Time of flight in picoseconds
	----------------------
	signal trigger_ranging   		:  std_logic;
	signal ta_tof_counter_valid   :  std_logic;
	signal ta_tof_counter         :  std_logic_vector(40-1 downto 0);	
	signal tb_tof_counter_valid   :  std_logic;
	signal tb_tof_counter         :  std_logic_vector(40-1 downto 0);
	signal tof_counter_f  : natural;
	signal tof_ns : natural;
	signal transport_control : natural := 0;
	signal transport_vector_p : std_logic_vector(3 downto 0);
	signal transport_vector_n : std_logic_vector(3 downto 0);
	signal enable_link        : std_logic := '1';
	signal reset_timer        : std_logic := '0';

	-- Opening the file in write mode
	file file_out : TEXT open write_mode is "fileio.txt";
	signal pps_clk                     :  std_logic;
	signal LAPC_RPT_RSSI_FAST          :  std_logic_vector(14-1 downto 0);
	signal LAPC_RPT_RSSI_MEAN          :  std_logic_vector(8-1 downto 0);
	signal LAPC_RPT_RSSI_SDEV          :  std_logic_vector(8-1 downto 0);
	signal OISL_PMIN                   :  std_logic_vector(8-1 downto 0);
	signal OISL_PMAX                   :  std_logic_vector(8-1 downto 0);
	signal fifo_fcch_data_a                :  std_logic_vector(20-1 downto 0);
	signal fifo_fcch_valid_a               :  std_logic;
	signal fifo_fcch_rd_en_a               :  std_logic;	
	signal fifo_fcch_data_b                :  std_logic_vector(20-1 downto 0);
	signal fifo_fcch_valid_b               :  std_logic;
	signal fifo_fcch_rd_en_b               :  std_logic;

-- fso_phy_sim:
	component gtwizard_fso_1g_top is
		port (
	
			-- // Differential reference clock inputs
			mgtrefclk0_x0y4_p : in  std_logic;
			mgtrefclk0_x0y4_n : in  std_logic;
	
			-- // Serial data ports for transceiver channel 0
			ch0_gthrxn_in  : in  std_logic;
			ch0_gthrxp_in  : in  std_logic;
			ch0_gthtxn_out : out std_logic;
			ch0_gthtxp_out : out std_logic;
	
			-- // User-provided ports for reset helper block(s)
			hb_gtwiz_reset_clk_freerun_in : in std_logic;
			hb_gtwiz_reset_all_in         : in std_logic;
	
			-- // User data interface
			rx_reset_out          : out std_logic;
			rx_usrclk2_out        : out std_logic;
			gtwiz_userdata_rx_out : out std_logic_vector(15 downto 0);
			data_good_in          : in  std_logic;
	
			tx_reset_out         : out std_logic;
			tx_usrclk2_out       : out std_logic;
			gtwiz_userdata_tx_in : in  std_logic_vector(15 downto 0);
	
			-- // status and control signal - synced to the freerun clock
			link_down_latched_reset_in : in  std_logic;
			link_status_out            : out std_logic;
			link_down_latched_out      : out std_logic;
			init_done_out              : out std_logic;
			init_retry_ctr_out         : out std_logic_vector(3 downto 0);
			gtpowergood_out            : out std_logic;
			txprgdivresetdone_out      : out std_logic;
			rxprgdivresetdone_out      : out std_logic;
			txpmaresetdone_out         : out std_logic;
			rxpmaresetdone_out         : out std_logic;
			gtwiz_reset_tx_done_out    : out std_logic;
			gtwiz_reset_rx_done_out    : out std_logic;
	
			hb_gtwiz_reset_all_axi_in                  : in  std_logic;
			hb0_gtwiz_reset_tx_pll_and_datapath_axi_in : in  std_logic_vector(0 downto 0);
			hb0_gtwiz_reset_tx_datapath_axi_in         : in  std_logic_vector(0 downto 0);
			hb_gtwiz_reset_rx_pll_and_datapath_axi_in  : in  std_logic;
			hb_gtwiz_reset_rx_datapath_axi_in          : in  std_logic;
			link_down_latched_reset_axi_in             : in  std_logic
		);
	end component gtwizard_fso_1g_top;
	
	signal ta_txfifo_tdata  : std_logic_vector(15 downto 0);
	signal ta_txfifo_tvalid : std_logic;
	signal ta_txfifo_tready : std_logic;
	signal ta_rxfifo_tdata  : std_logic_vector(15 downto 0);
	signal ta_rxfifo_tvalid : std_logic;
	signal ta_rxfifo_tready : std_logic;
	
	signal tb_txfifo_tdata  : std_logic_vector(15 downto 0);
	signal tb_txfifo_tvalid : std_logic;
	signal tb_txfifo_tready : std_logic;
	signal tb_rxfifo_tdata  : std_logic_vector(15 downto 0);
	signal tb_rxfifo_tvalid : std_logic;
	signal tb_rxfifo_tready : std_logic;

	signal ta_hb_gtwiz_reset_clk_freerun_in : std_logic;
	signal tb_hb_gtwiz_reset_clk_freerun_in : std_logic;
	signal ta_hb_gtwiz_reset_all_in         : std_logic;
	signal tb_hb_gtwiz_reset_all_in         : std_logic;

	signal ta_rx_reset_int         : std_logic;
	signal ta_rx_reset_out         : std_logic;
	signal ta_rx_usrclk2_out       : std_logic;
	signal ta_gtwiz_userdata_rx_out :  std_logic_vector(15 downto 0);
	signal tb_rx_reset_int         : std_logic;
	signal tb_rx_reset_out         : std_logic;
	signal tb_rx_usrclk2_out       : std_logic;
	signal tb_gtwiz_userdata_rx_out :  std_logic_vector(15 downto 0);

	signal ta_tx_reset_int         : std_logic;
	signal ta_tx_reset_out         : std_logic;
	signal ta_tx_usrclk2_out       : std_logic;
	signal ta_gtwiz_userdata_tx_in : std_logic_vector(15 downto 0);
	signal tb_tx_reset_int         : std_logic;
	signal tb_tx_reset_out         : std_logic;
	signal tb_tx_usrclk2_out       : std_logic;
	signal tb_gtwiz_userdata_tx_in : std_logic_vector(15 downto 0);

	signal ta_link_down_latched_reset_in : std_logic := '0';
	signal ta_link_status_out            : std_logic;
	signal ta_link_down_latched_out      : std_logic;
	signal ta_init_done_out              : std_logic;
	signal ta_init_retry_ctr_out         : std_logic_vector(3 downto 0);
	signal ta_gtpowergood_out            : std_logic;
	signal ta_txprgdivresetdone_out      : std_logic;
	signal ta_rxprgdivresetdone_out      : std_logic;
	signal ta_txpmaresetdone_out         : std_logic;
	signal ta_rxpmaresetdone_out         : std_logic;
	signal ta_gtwiz_reset_tx_done_out    : std_logic;
	signal ta_gtwiz_reset_rx_done_out    : std_logic;
	
	signal tb_link_down_latched_reset_in : std_logic := '0';
	signal tb_link_status_out            : std_logic;
	signal tb_link_down_latched_out      : std_logic;
	signal tb_init_done_out              : std_logic;
	signal tb_init_retry_ctr_out         : std_logic_vector(3 downto 0);
	signal tb_gtpowergood_out            : std_logic;
	signal tb_txprgdivresetdone_out      : std_logic;
	signal tb_rxprgdivresetdone_out      : std_logic;
	signal tb_txpmaresetdone_out         : std_logic;
	signal tb_rxpmaresetdone_out         : std_logic;
	signal tb_gtwiz_reset_tx_done_out    : std_logic;
	signal tb_gtwiz_reset_rx_done_out    : std_logic;

	signal ta_hb_gtwiz_reset_all_axi_in                  : std_logic := '0';
	signal ta_hb0_gtwiz_reset_tx_pll_and_datapath_axi_in : std_logic_vector(0 downto 0) := "0";
	signal ta_hb0_gtwiz_reset_tx_datapath_axi_in         : std_logic_vector(0 downto 0) := "0";
	signal ta_hb_gtwiz_reset_rx_pll_and_datapath_axi_in  : std_logic := '0';
	signal ta_hb_gtwiz_reset_rx_datapath_axi_in          : std_logic := '0';
	signal ta_link_down_latched_reset_axi_in             : std_logic := '0';
	
	signal tb_hb_gtwiz_reset_all_axi_in                  : std_logic := '0';
	signal tb_hb0_gtwiz_reset_tx_pll_and_datapath_axi_in : std_logic_vector(0 downto 0) := "0";
	signal tb_hb0_gtwiz_reset_tx_datapath_axi_in         : std_logic_vector(0 downto 0) := "0";
	signal tb_hb_gtwiz_reset_rx_pll_and_datapath_axi_in  : std_logic := '0';
	signal tb_hb_gtwiz_reset_rx_datapath_axi_in          : std_logic := '0';
	signal tb_link_down_latched_reset_axi_in             : std_logic := '0';

	signal framer_in_axis_tdata   : std_logic_vector(8 - 1 downto 0);
	signal framer_in_axis_tvalid  : std_logic;
	signal framer_in_axis_tlast   : std_logic;
	signal framer_in_axis_tready  : std_logic;
	signal deframer_out_axis_tdata  : std_logic_vector(8 - 1 downto 0);
	signal deframer_out_axis_tvalid : std_logic;
	signal deframer_out_axis_tlast  : std_logic;
	signal deframer_out_axis_tready : std_logic;
	signal framer_in_axis_tuser : std_logic;
	signal deframer_out_axis_tuser : std_logic;
		
        -- Framer
	signal framer_total_packet_counter            : std_logic_vector(31 downto 0);
	signal framer_total_packet_splitted_counter   : std_logic_vector(31 downto 0);
	signal framer_total_data_frame_counter        : std_logic_vector(31 downto 0);
	signal framer_actual_data_frame_counter       : std_logic_vector(31 downto 0);
	signal framer_total_idle_frame_counter        : std_logic_vector(31 downto 0);
	signal framer_total_packet_drop				:  std_logic_vector(31 downto 0);
	signal framer_frame_length_error				:  std_logic_vector(31 downto 0);
	signal framer_bitrate_in				        :  std_logic_vector(31 downto 0);
	signal framer_packet_cnt_in				    :  std_logic_vector(31 downto 0);

        -- Deframer
    signal deframer_total_packet_counter            : std_logic_vector(31 downto 0);
    signal deframer_total_payload_counter           : std_logic_vector(31 downto 0);
    signal deframer_total_packet_splitted_counter   : std_logic_vector(31 downto 0);
    signal deframer_total_packet_merged_counter     : std_logic_vector(31 downto 0);
    signal deframer_total_data_frame_counter        : std_logic_vector(31 downto 0);
    signal deframer_total_idle_frame_counter        : std_logic_vector(31 downto 0);
    signal deframer_total_payload_error_counter     : std_logic_vector(31 downto 0);
    signal deframer_total_packet_error_counter      : std_logic_vector(31 downto 0);
    signal deframer_watchdog_reset_counter          : std_logic_vector(31 downto 0);
    signal deframer_packet_filter_cnt_in			: std_logic_vector(31 downto 0);
	signal deframer_packet_filter_cnt_out			: std_logic_vector(31 downto 0);
    signal deframer_frame_filter_cnt_in             : std_logic_vector(31 downto 0);
    signal deframer_frame_filter_cnt_out            : std_logic_vector(31 downto 0);
    signal deframer_bitrate_out            : std_logic_vector(31 downto 0);
    signal deframer_packet_cnt_out            : std_logic_vector(31 downto 0);

    signal framer_monitor_warning_flag              : std_logic;
    signal framer_monitor_critical_error_flag       : std_logic;    
    signal deframer_monitor_warning_flag            : std_logic;
    signal deframer_monitor_critical_error_flag     : std_logic;

begin

	-------------------------------------------
	-- component instantiation
	-------------------------------------------


LET_SDA_A : entity let_sda_lib.let_sda_top
    Port map ( 
        clk_let           => clk, --: in  std_logic; -- Main clock 200 MHz
        rst_let           => rst, --: in  std_logic;
        clk_tx_eth        => clk, --: in  std_logic; -- Ethernet TX Clock
        rst_tx_eth        => rst, --: in  std_logic;
        clk_rx_eth        => clk, --: in  std_logic; -- Ethernet RX Clock
        rst_rx_eth        => rst, --: in  std_logic; 
        clk_fso_tx        => '0', --: in  std_logic; -- FSO PHY TX Clock 
        clk_fso_rx        => '0', --: in  std_logic;
        rst_fso_tx        => ta_tx_reset_out, --: in  std_logic; -- FSO PHY RX Clock
        rst_fso_rx        => ta_rx_reset_out, --: in  std_logic;

        -- Input interface ETH
        ----------------------
        framer_in_axis_tdata     => framer_in_axis_tdata, --: in  std_logic_vector(8 - 1 downto 0);
        framer_in_axis_tvalid    => framer_in_axis_tvalid, --: in  std_logic;
        framer_in_axis_tlast     => framer_in_axis_tlast, --: in  std_logic;
        framer_in_axis_tready    => framer_in_axis_tready, --: out std_logic;
        framer_in_axis_tuser     => framer_in_axis_tuser, --: in  std_logic;

        cfg_pause_data          => (others=>'0'),--: in std_logic_vector(15 downto 0);
        tx_pause_valid          => open,--: out std_logic;
        tx_pause_data           => open,--: out std_logic_vector(15 downto 0);
        rx_fifo_skipped_frame   => open,--: out std_logic_vector(32 - 1 downto 0);

        -- Output interface ETH
        ----------------------
        deframer_out_axis_tdata  => deframer_out_axis_tdata, --: out std_logic_vector(8 - 1 downto 0);
        deframer_out_axis_tvalid => deframer_out_axis_tvalid, --: out std_logic;
        deframer_out_axis_tlast  => deframer_out_axis_tlast, --: out std_logic;
        deframer_out_axis_tready => '1', --: in  std_logic;
        deframer_out_axis_tuser  => deframer_out_axis_tuser, --: out std_logic;

        -- Input interface Creonic
        ----------------------tx_axis_tdata
        deframer_in_axis_tdata   => ta_framer_axis_tdata, --: in  std_logic_vector(8 - 1 downto 0);
        deframer_in_axis_tvalid  => ta_framer_axis_tvalid, --: in std_logic;
        deframer_in_axis_tlast   => ta_framer_axis_tlast, --: in std_logic;
        deframer_in_axis_tready  => ta_framer_axis_tready, --: out std_logic;
        preamble_synced          => '1', --: in std_logic;
        crc16_valid              => '1', --: in std_logic;
        crc16_correct            => '1', --: in std_logic;
        crc32_valid              => '1', --: in std_logic;
        crc32_correct            => '1', --: in std_logic;

        -- Output interface Creonic
        ----------------------
        framer_out_axis_tdata    => ta_framer_axis_tdata, --: out  std_logic_vector(8 - 1 downto 0);
        framer_out_axis_tvalid   => ta_framer_axis_tvalid, --: out std_logic;
        framer_out_axis_tlast    => ta_framer_axis_tlast, --: out std_logic;
        framer_out_axis_tready   => ta_framer_axis_tready, --: in std_logic;

        -- Input data from FSO PHY
        ----------------------
        fso_phy_data_tx          => (others=>'0'), --: in  std_logic_vector(16 - 1 downto 0);
        fso_phy_valid_tx         => '0', --: in  std_logic;
        fso_phy_data_rx          => (others=>'0'), --: in  std_logic_vector(16 - 1 downto 0);
        fso_phy_valid_rx         => '0', --: in  std_logic;

        -- Ranging data: Time of flight in 1/16 fso_clk
        ----------------------
		trigger					 => '0',
        tof_counter_valid        => ta_tof_counter_valid, --: out std_logic;
        tof_counter              => ta_tof_counter, --: out std_logic_vector(40-1 downto 0);

        -- Framer Health Monitor
        ----------------------
        framer_warning_flag                => open, --: out  std_logic;
        framer_critical_error_flag         => open, --: out  std_logic;
        framer_health_monitor_statistics   => open, --: out health_monitor_statistics_t;

        -- Deframer Health Monitor
        ----------------------
        deframer_warning_flag                => open, --: out  std_logic;
        deframer_critical_error_flag         => open, --: out  std_logic;
        deframer_health_monitor_statistics   => open, --: out health_monitor_statistics_t;
        
        pps_clk                     => pps_clk, --
        
        -- From Register Map
        LAPC_RPT_RSSI_FAST          => LAPC_RPT_RSSI_FAST, --: in std_logic_vector(14-1 downto 0);
        LAPC_RPT_RSSI_MEAN          => LAPC_RPT_RSSI_MEAN, --: in std_logic_vector(8-1 downto 0);
        LAPC_RPT_RSSI_SDEV          => LAPC_RPT_RSSI_SDEV, --: in std_logic_vector(8-1 downto 0);
        OISL_PMIN                   => OISL_PMIN, --: in std_logic_vector(8-1 downto 0);
        OISL_PMAX                   => OISL_PMAX, --: in std_logic_vector(8-1 downto 0);

        -- To Register Map
        fifo_fcch_data                => fifo_fcch_data_a, --: out std_logic_vector(20-1 downto 0);
        fifo_fcch_valid               => fifo_fcch_valid_a, --: out std_logic;
        fifo_fcch_rd_en               => '0', --: in std_logic;
        
        -- Statistics
        ----------------------
        clear_stat              => '0', --: in  std_logic;
        -- Framer
		framer_total_packet_counter            => framer_total_packet_counter         , -- : out std_logic_vector(31 downto 0);
		framer_total_packet_splitted_counter   => framer_total_packet_splitted_counter, -- : out std_logic_vector(31 downto 0);
		framer_total_data_frame_counter        => framer_total_data_frame_counter     , -- : out std_logic_vector(31 downto 0);
		framer_actual_data_frame_counter       => framer_actual_data_frame_counter    , -- : out std_logic_vector(31 downto 0);
		framer_total_idle_frame_counter        => framer_total_idle_frame_counter     , -- : out std_logic_vector(31 downto 0);
		framer_total_packet_drop               => framer_total_packet_drop            , -- : out std_logic_vector(31 downto 0);
		framer_frame_length_error              => framer_frame_length_error           , -- : out std_logic_vector(31 downto 0);
		framer_bitrate_in	                   => framer_bitrate_in	                  , --  : out std_logic_vector(31 downto 0);
		framer_packet_cnt_in                   => framer_packet_cnt_in                , --  : out std_logic_vector(31 downto 0);

        -- Deframer
        deframer_total_packet_counter            => deframer_total_packet_counter         , --: out std_logic_vector(31 downto 0);
        deframer_total_payload_counter           => deframer_total_payload_counter        , --: out std_logic_vector(31 downto 0);
        deframer_total_packet_splitted_counter   => deframer_total_packet_splitted_counter, --: out std_logic_vector(31 downto 0);
        deframer_total_packet_merged_counter     => deframer_total_packet_merged_counter  , --: out std_logic_vector(31 downto 0);
        deframer_total_data_frame_counter        => deframer_total_data_frame_counter     , --: out std_logic_vector(31 downto 0);
        deframer_total_idle_frame_counter        => deframer_total_idle_frame_counter     , --: out std_logic_vector(31 downto 0);
        deframer_total_payload_error_counter     => deframer_total_payload_error_counter  , --: out std_logic_vector(31 downto 0);
        deframer_total_packet_error_counter      => deframer_total_packet_error_counter   , --: out std_logic_vector(31 downto 0);
        deframer_watchdog_reset_counter          => deframer_watchdog_reset_counter       , --: out std_logic_vector(31 downto 0);
        deframer_packet_filter_cnt_in            => deframer_packet_filter_cnt_in         , --: out std_logic_vector(31 downto 0);
		deframer_packet_filter_cnt_out           => deframer_packet_filter_cnt_out        , --: out std_logic_vector(31 downto 0);
        deframer_frame_filter_cnt_in             => deframer_frame_filter_cnt_in          , --: out std_logic_vector(31 downto 0);
        deframer_frame_filter_cnt_out            => deframer_frame_filter_cnt_out         , --: out std_logic_vector(31 downto 0);
        deframer_bitrate_out                     => deframer_bitrate_out                  ,--: out std_logic_vector(31 downto 0);
        deframer_packet_cnt_out                  => deframer_packet_cnt_out               ,--: out std_logic_vector(31 downto 0);
        
       
        sda_debug               => open, --: out sda_debug_t;

        -- Version control
        ----------------------
        version_mayor           => open, --: out unsigned(7 downto 0);
        version_minor           => open  --: out unsigned(7 downto 0)

    );



	-------------------------------------------
	-- stimuli generation
	-------------------------------------------
	process
	begin
		framer_in_axis_tlast <= '0';
		framer_in_axis_tvalid <= '0';
		framer_in_axis_tuser <= '0';
		rst <= '1';
		wait for 200 ns;
		rst <= '0';
		test_finished 	<= '0';
		test_start 		<= '0';
        wait for 1 us;
		for k in 0 to 100 loop

			test_start <= '1';
			wait until rising_edge(clk);
			framer_in_axis_tvalid <= '1';
			framer_in_axis_tlast <= '0';
			framer_in_axis_tdata <= x"AB";
			test_start <= '0';
			test_stage <= test_stage + 1;
		end loop;
		
		wait until rising_edge(clk);
		framer_in_axis_tvalid <= '1';
		framer_in_axis_tlast <= '1';
		framer_in_axis_tdata <= x"AB";
		wait until rising_edge(clk);
		framer_in_axis_tvalid <= '0';
		framer_in_axis_tlast <= '0';
		framer_in_axis_tdata <= x"AB";
		
		wait for 30 us;
		wait;

		V_PHASE_A 	<= C_PHASE_A;
		V_PHASE_B 	<= C_PHASE_B;
		V_FREQ_A 	<= C_FREQ_A;
		V_FREQ_B 	<= C_FREQ_B;
		for k in 0 to 2 loop
			V_PHASE_A 	<= V_PHASE_A;
			V_PHASE_B 	<= V_PHASE_B;
			V_FREQ_A 	<= V_FREQ_A;
			V_FREQ_B 	<= V_FREQ_B + C_50KHZ_INC*k;
			test_start <= '1';
			wait until falling_edge(test_enable);
			test_start <= '0';
			test_stage <= test_stage + 1;
			wait for 300 ns;
		end loop;

		V_PHASE_A 	<= C_PHASE_A;
		V_PHASE_B 	<= C_PHASE_B;
		V_FREQ_A 	<= C_FREQ_A;
		V_FREQ_B 	<= C_FREQ_B;
		for k in 0 to 2 loop
			V_PHASE_A 	<= V_PHASE_A;
			V_PHASE_B 	<= V_PHASE_B;
			V_FREQ_A 	<= V_FREQ_A + C_50KHZ_INC*k;
			V_FREQ_B 	<= V_FREQ_B + C_50KHZ_INC*k;
			test_start <= '1';
			wait until falling_edge(test_enable);
			test_start <= '0';
			test_stage <= test_stage + 1;
			wait for 300 ns;
		end loop;

		V_PHASE_A 	<= C_PHASE_A;
		V_PHASE_B 	<= C_PHASE_B;
		V_FREQ_A 	<= C_FREQ_A;
		V_FREQ_B 	<= C_FREQ_B;
		for k in 0 to 2 loop
			V_PHASE_A 	<= V_PHASE_A + (real(k) * 3 ns);
			V_PHASE_B 	<= V_PHASE_B;
			V_FREQ_A 	<= V_FREQ_A;
			V_FREQ_B 	<= V_FREQ_B;
			test_start <= '1';
			wait until falling_edge(test_enable);
			test_start <= '0';
			test_stage <= test_stage + 1;
			wait for 300 ns;
		end loop;

		V_PHASE_A 	<= C_PHASE_A;
		V_PHASE_B 	<= C_PHASE_B;
		V_FREQ_A 	<= C_FREQ_A;
		V_FREQ_B 	<= C_FREQ_B;
		for k in 0 to 2 loop
			V_PHASE_A 	<= V_PHASE_A + (real(k) * 3 ns);
			V_PHASE_B 	<= V_PHASE_B;
			V_FREQ_A 	<= V_FREQ_A + C_50KHZ_INC*k;
			V_FREQ_B 	<= V_FREQ_B;
			test_start <= '1';
			wait until falling_edge(test_enable);
			test_start <= '0';
			test_stage <= test_stage + 1;
			wait for 300 ns;
		end loop;
		test_finished <= '1';
		wait;

	end process;


--	pr_stimuli: process
--	begin
--		loop
--			wait until test_start = '1';
--			test_enable <= '1';
--			-- reset the system
--			rst              <= '1';
--			rst_clk10        <= '1';
--			reset_sync_200   <= '1';
--			mac_rx_reset     <= '1';
--			trigger_ranging  <= '0';
--			wait for 200 * CLK_PERIOD;
--
--			-- release the reset
--			rst         	 <= '0';
--			rst_clk10   	 <= '0';
--			reset_sync_200   <= '0';
--			mac_rx_reset     <= '0';
--			wait for 5 * CLK_PERIOD;
--			wait for 400 us;
--			wait until rising_edge(clk); -- #1
--			trigger_ranging <= '1';
--			wait until rising_edge(clk);
--			trigger_ranging <= '0';
--
--			wait until ta_tof_counter_valid = '1';
--			------wait for 50 us;
--			wait for 200 ns;
--			test_enable <= '0';
--			wait for 200 ns;
--
--		end loop;
--		wait;
--	end process;

ta_mgtrefclk0_x0y4_n <= not ta_mgtrefclk0_x0y4_p;
tb_mgtrefclk0_x0y4_n <= not tb_mgtrefclk0_x0y4_p;


	-- Serial loopback
	--ch0_gthrxp_in <= transport ch0_gthtxp_out after 0.833*1 ns; --after 0.833*11 ns;
	--ch0_gthrxn_in <= transport ch0_gthtxn_out after 0.833*1 ns; --after 0.833*11 ns;
	ta_ch0_gthrxp_in <= transport tb_ch0_gthtxp_out after C_TOF_DELAY; --after 0.833*11 ns;--when enable_link = '1' else '0'; --after 0.833*11 ns;
	ta_ch0_gthrxn_in <= transport tb_ch0_gthtxn_out after C_TOF_DELAY; --after 0.833*11 ns;--when enable_link = '1' else '1'; --after 0.833*11 ns;
	tb_ch0_gthrxp_in <= transport ta_ch0_gthtxp_out after C_TOF_DELAY; --after 0.833*11 ns;--when enable_link = '1' else '0'; --after 0.833*11 ns;
	tb_ch0_gthrxn_in <= transport ta_ch0_gthtxn_out after C_TOF_DELAY; --after 0.833*11 ns;--when enable_link = '1' else '1'; --after 0.833*11 ns;
	
	
--	transport_vector_p(0) <= transport ch0_gthtxp_out after 0.833*1 ns; --after 0.833*4 ns;
--	transport_vector_n(0) <= transport ch0_gthtxn_out after 0.833*1 ns; --after 0.833*4 ns;
	
--	transport_vector_p(1) <= transport ch0_gthtxp_out after 0.833*2 ns; --after 0.833*4 ns;
--	transport_vector_n(1) <= transport ch0_gthtxn_out after 0.833*2 ns; --after 0.833*4 ns;
	
--	transport_vector_p(2) <= transport ch0_gthtxp_out after 0.833*3 ns; --after 0.833*4 ns;
--	transport_vector_n(2) <= transport ch0_gthtxn_out after 0.833*3 ns; --after 0.833*4 ns;
	
--	transport_vector_p(3) <= transport ch0_gthtxp_out after 0.833*4 ns; --after 0.833*4 ns;
--	transport_vector_n(3) <= transport ch0_gthtxn_out after 0.833*4 ns; --after 0.833*4 ns;
	
--ch0_gthrxp_in <= transport ch0_gthtxp_out after 13.4 ns; --after 0.833*4 ns;
--ch0_gthrxn_in <= transport ch0_gthtxn_out after 13.4 ns; --after 0.833*4 ns;
--    ch0_gthrxp_in <= ch0_gthtxp_out when transport_control = 0 else 
--                    transport_vector_p(0) when transport_control = 1 else
--                     transport_vector_p(1) when transport_control = 2 else
--                     transport_vector_p(2) when transport_control = 3 else
--                     transport_vector_p(3) when transport_control = 4 else   
--                     transport_vector_p(3);
--    ch0_gthrxn_in <= ch0_gthtxn_out when transport_control = 0 else
--                transport_vector_n(0) when transport_control = 1 else
--                 transport_vector_n(1) when transport_control = 2 else
--                 transport_vector_n(2) when transport_control = 3 else
--                 transport_vector_n(3) when transport_control = 4 else   
--                 transport_vector_n(3);
	
	process
	begin
       enable_link <= '1';
       
       wait for 700 us;
       enable_link <= '0';
       wait for 200 us;
       enable_link <= '1';
       wait for 523 us;
       enable_link <= '0';
       wait for 255 us;
       enable_link <= '1';
       wait for 501 us;
       enable_link <= '0';
       wait for 212 us;
       enable_link <= '1';
       wait for 510 us;
       enable_link <= '0';
       wait for 232 us;
       enable_link <= '1';
       wait;
--	   transport_control <= 0;
--	   wait for 300 us;
--	   transport_control <= 1;
--	   wait for 300 us;
--	   transport_control <= 2;
--	   wait for 300 us;
--	   transport_control <= 3;
--	   wait for 300 us;
--	   transport_control <= 4;
--	   wait for 300 us;	   
	   
	   wait;
	end process;
	
	-- insert some bit flips
--	ch_rx_axis_tdata_noise <= (others => '0') when (byte_cnt > 60000 and byte_cnt < 90000) else -- Signal loss
--	                          (ch_rx_axis_tdata xor "01000000") when (byte_cnt mod  5) = 0 else -- single bit error
--	                          (ch_rx_axis_tdata xor "11110000") when (byte_cnt mod 81) = 0 else -- multiple bit error
--	                           ch_rx_axis_tdata;


	-- Configuration for the preamble sync
	cfg_aquisition_length  <= std_logic_vector(to_unsigned(4, 32));
	cfg_tracking_length    <= std_logic_vector(to_unsigned(16, 32));
	cfg_max_tracking_error <= std_logic_vector(to_unsigned(4, 32));

	

	-------------------------------------------
	-- clock generation
	-------------------------------------------
	clk <= not clk after CLK_PERIOD / 2;
	clk10 <= not clk10 after 10 ns;
	clk200 <= not clk200 after CLK_PERIOD / 2;
	mac_rx_clk_out <= not mac_rx_clk_out after CLK_PERIOD / 2;
	-------------------------------------------
	-- fso_phy:
	-------------------------------------------



end architecture sim;
