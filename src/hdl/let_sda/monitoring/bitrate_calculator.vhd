----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 12/04/2021 11:40:33 AM
-- Design Name: 
-- Module Name: bitrate_calculator - Behavioral
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2018.3
-- Description: Calculates bitrate per second of an axi stream interface.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;



entity bitrate_calculator is
    Port ( 
        clk            : in  std_logic;
        rst            : in  std_logic;

        -- Input data from encoder
        ----------------------
        s_axis_tdata     : in  std_logic_vector(8 - 1 downto 0);
        s_axis_tvalid    : in  std_logic;
        s_axis_tlast     : in  std_logic;
        s_axis_tready    : in  std_logic;

        bitrate          : out std_logic_vector(31 downto 0);
        total_packet_cnt : out std_logic_vector(31 downto 0)

        );
end bitrate_calculator;

architecture Behavioral of bitrate_calculator is

    constant C_CYCLES_PER_SECOND : natural := 200000000;
    constant C_TIME_WINDOW_LIMIT : unsigned(31 downto 0) := to_unsigned(C_CYCLES_PER_SECOND, 32);

    signal byte_counter : unsigned(31 downto 0);
    signal total_packet_cnt_int : unsigned(31 downto 0);
    signal time_window_counter : unsigned(31 downto 0);
    signal bitrate_int : std_logic_vector(31 downto 0);

begin

bitrate <= bitrate_int(32-3-2-1 downto 0) & "000" & "00"; -- x 8bit x 4
total_packet_cnt <= std_logic_vector(total_packet_cnt_int);

process(clk,rst)
begin
    if rst = '1' then
        time_window_counter <= (others =>'0');
        byte_counter <= (others=>'0');
        total_packet_cnt_int <= (others=>'0');
        bitrate_int <= (others=>'0');
    else
        if rising_edge(clk) then
            
            if s_axis_tvalid = '1' and s_axis_tready = '1' then
                byte_counter <= byte_counter + 1;
            end if;

            if time_window_counter = (C_TIME_WINDOW_LIMIT/4)-1 then
                bitrate_int <= std_logic_vector(byte_counter);  
                time_window_counter <= (others =>'0');
                byte_counter <= (others=>'0');
            else
                time_window_counter <= time_window_counter + 1;
            end if;

            if s_axis_tvalid = '1' and s_axis_tlast = '1' and s_axis_tready = '1' then
                total_packet_cnt_int <= total_packet_cnt_int + 1;
            end if;

        end if;

    end if;

end process;

end Behavioral;



