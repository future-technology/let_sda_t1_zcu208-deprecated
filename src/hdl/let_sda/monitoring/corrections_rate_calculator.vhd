----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 04/13/2021 11:23:13 AM
-- Design Name: 
-- Module Name: corrections_rate_calculator - Behavioral
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: 
-- Description: Corrections rate per second calculator for creonic statistics
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
library share_let_1g;
use share_let_1g.pkg_components.axi4s_fifo;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity corrections_rate_calculator is
	port(
		clk               : in std_logic;
		rst               : in std_logic;

		crc16_valid       : in  std_logic;
		crc16_correct     : in  std_logic;
		crc32_valid       : in  std_logic;
		crc32_correct     : in  std_logic;

		payload_cerr      : in  std_logic_vector(4 - 1 downto 0);
		payload_ncerr     : in  std_logic;
		payload_err_valid : in  std_logic;

		header_cerr       : in  std_logic_vector(4 - 1 downto 0);
		header_ncerr      : in  std_logic;
		header_err_valid  : in  std_logic;

		lpc_correct_valid : in  std_logic;
		lpc_correction    : in  std_logic_vector(4 downto 0);

		header_corrections_per_second  	: out std_logic_vector(31 downto 0);
		payload_corrections_per_second  : out std_logic_vector(31 downto 0);
		lpc_corrections_per_second  	: out std_logic_vector(31 downto 0);
		corrections_rate        		: out std_logic_vector(31 downto 0); -- not used
		crc_errors_per_second   		: out std_logic_vector(31 downto 0)
	);
end corrections_rate_calculator;

architecture Behavioral of corrections_rate_calculator is

  --constant C_CYCLES_PER_SECOND : natural := 200000000;
  constant C_CYCLES_PER_SECOND : natural := 200000; -- test only
  constant C_TIME_WINDOW_LIMIT : unsigned(31 downto 0) := to_unsigned(C_CYCLES_PER_SECOND, 32);

	signal crc16_correct_vec : std_logic_vector(0 downto 0);
	signal crc16_correct_buf : std_logic_vector(0 downto 0);
	signal crc16_buf_valid   : std_logic;
	signal crc16_buf_ready   : std_logic;
	signal crc32_correct_vec : std_logic_vector(0 downto 0);
	signal crc32_correct_buf : std_logic_vector(0 downto 0);
	signal crc32_buf_valid   : std_logic;
	signal crc32_buf_ready   : std_logic;
	signal corrections_rate_reg   : std_logic;
	signal corrections_rate_reg2   : std_logic;
	signal corrections_rate_reg3   : std_logic;

	signal frame_cnt        : unsigned(31 downto 0);
	signal err_frame_cnt    : unsigned(31 downto 0);
	signal crc16_err_cnt    : unsigned(31 downto 0);
	signal crc32_err_cnt    : unsigned(31 downto 0);
	signal rs_header_ncorr  : unsigned(31 downto 0);
	signal rs_header_ncorr_aux  : unsigned(15 downto 0);
	signal rs_payload_ncorr : unsigned(31 downto 0);
	signal rs_payload_ncorr_aux : unsigned(15 downto 0);
	signal lpc_frame_cnt    : unsigned(31 downto 0);
	signal lpc_frame_cnt_aux: unsigned(15 downto 0);
	signal lpc_frame_cnt_aux_24 : unsigned(31 downto 0);
	signal lpc_ncorr        : unsigned(31 downto 0);
	signal lpc_ncorr_aux        : unsigned(15 downto 0);


  signal time_window_counter : unsigned(31 downto 0);
  signal corrections_per_second_int : unsigned(31 downto 0);
  signal corrections_rate_int : unsigned(15 downto 0);
  signal corrections_rate_int_100 : unsigned(31 downto 0);
  signal corrections_rate_out : unsigned(31 downto 0);
  signal crc_errors_per_second_int : unsigned(31 downto 0);
  signal header_corrections_per_second_int : unsigned(31 downto 0);
  signal payload_corrections_per_second_int : unsigned(31 downto 0);
  signal lpc_corrections_per_second_int : unsigned(31 downto 0);

begin

	crc16_correct_vec(0) <= crc16_correct;
	crc32_correct_vec(0) <= crc32_correct;

	--total_frame_counter <= std_logic_vector(frame_cnt);
	--crc_error_counter   <= std_logic_vector(err_frame_cnt);
	--crc16_err_counter   <= std_logic_vector(crc16_err_cnt);
	--crc32_err_counter   <= std_logic_vector(crc32_err_cnt);
	--header_corrections  <= std_logic_vector(rs_header_ncorr);
	header_corrections_per_second 	<= std_logic_vector(header_corrections_per_second_int);
	payload_corrections_per_second  <= std_logic_vector(payload_corrections_per_second_int);
	lpc_corrections_per_second     	<= std_logic_vector(lpc_corrections_per_second_int);

	corrections_rate      <= (others => '0'); -- Not used. std_logic_vector(corrections_per_second_int);
	crc_errors_per_second      <= std_logic_vector(crc_errors_per_second_int);
	corrections_rate_int_100 <= corrections_rate_int * 100;
	corrections_rate_out            <= "0000000000" & corrections_rate_int_100(31 downto 10); -- corrections_rate = (rs_header_ncorr + rs_payload_ncorr + lpc_ncorr) / (lpc_frame_cnt*24)
    lpc_frame_cnt_aux_24        <= lpc_frame_cnt_aux * 24;

	pr_stats_collect: process(clk) is
		variable v_bit_slip : unsigned(3 downto 0);
	begin
	if rising_edge(clk) then
		if rst = '1' then
			frame_cnt     <= (others => '0');
			err_frame_cnt <= (others => '0');
			crc16_err_cnt <= (others => '0');
			crc32_err_cnt <= (others => '0');
			rs_header_ncorr  <= (others => '0');
			rs_header_ncorr_aux  <= (others => '0');
			rs_payload_ncorr <= (others => '0');
			rs_payload_ncorr_aux <= (others => '0');
			crc16_buf_ready  <= '0';
			crc32_buf_ready  <= '0';
			lpc_frame_cnt    <= (others => '0');
			lpc_frame_cnt_aux<= (others => '0');
			lpc_ncorr        <= (others => '0');
			lpc_ncorr_aux        <= (others => '0');
            time_window_counter <= (others => '0');
            corrections_rate_reg <= '0';
		else
			corrections_rate_reg <= '0';
			corrections_rate_reg2 <= corrections_rate_reg;
			corrections_rate_reg3 <= corrections_rate_reg2;

			if crc16_buf_valid = '1' and crc32_buf_valid = '1' then
				crc16_buf_ready  <= '1';
				crc32_buf_ready  <= '1';
			end if;
			if crc32_buf_valid = '1' and crc32_buf_ready = '1' then
				crc16_buf_ready  <= '0';
				crc32_buf_ready  <= '0';
				frame_cnt <= frame_cnt + 1;
				--if crc16_correct_buf(0) = '0' then
				--	crc16_err_cnt <= crc16_err_cnt + 1;
				--end if;
				--if crc32_correct_buf(0) = '0' then
				--	crc32_err_cnt <= crc32_err_cnt + 1;
				--end if;
				if crc16_correct_buf(0) = '0' or crc32_correct_buf(0) = '0' then
					err_frame_cnt <= err_frame_cnt + 1;
				end if;
			end if;

			if header_err_valid = '1' then
				rs_header_ncorr <= rs_header_ncorr + 1;
				--rs_header_ncorr  <= rs_header_ncorr  + unsigned(header_cerr(3 downto 0));
				--rs_header_ncorr_aux  <= rs_header_ncorr_aux  + unsigned(header_cerr(3 downto 0));
			end if;

			if payload_err_valid = '1' then
				rs_payload_ncorr <= rs_payload_ncorr + 1;
				--rs_payload_ncorr <= rs_payload_ncorr + unsigned(payload_cerr(3 downto 0));
				--rs_payload_ncorr_aux <= rs_payload_ncorr_aux + unsigned(payload_cerr(3 downto 0));
			end if;

			if lpc_correct_valid = '1' then
				lpc_ncorr     <= lpc_ncorr + 1;
				--lpc_frame_cnt <= lpc_frame_cnt + 1;
				--lpc_frame_cnt_aux <= lpc_frame_cnt_aux + 1;
				--lpc_ncorr     <= lpc_ncorr + unsigned(lpc_correction);
				--lpc_ncorr_aux <= lpc_ncorr_aux + unsigned(lpc_correction);
			end if;

			if time_window_counter = (C_TIME_WINDOW_LIMIT)-1 then
        		time_window_counter <= (others => '0');
				frame_cnt           <= (others => '0');
				err_frame_cnt       <= (others => '0');
				crc16_err_cnt       <= (others => '0');
				crc32_err_cnt       <= (others => '0');
				rs_header_ncorr     <= (others => '0');
				rs_payload_ncorr    <= (others => '0');
				lpc_frame_cnt       <= (others => '0');
				lpc_ncorr           <= (others => '0');
        		--corrate_int         <= (rs_header_ncorr + rs_payload_ncorr + lpc_ncorr) / (lpc_frame_cnt*24);
        		--corrections_per_second_int <= (rs_header_ncorr + rs_payload_ncorr + lpc_ncorr);
				header_corrections_per_second_int	<= rs_header_ncorr;
				payload_corrections_per_second_int	<= rs_payload_ncorr;
				lpc_corrections_per_second_int		<= lpc_ncorr;
				crc_errors_per_second_int 			<= err_frame_cnt;
      else
        time_window_counter <= time_window_counter + 1;
      end if;

      --if lpc_frame_cnt_aux_24 >= 1024-1 then
      --  corrections_rate_int    <= (rs_header_ncorr_aux + rs_payload_ncorr_aux + lpc_ncorr_aux); 
      --  lpc_frame_cnt_aux       <= (others => '0');
      --  rs_header_ncorr_aux     <= (others => '0');
      --  rs_payload_ncorr_aux    <= (others => '0');
      --  lpc_ncorr_aux           <= (others => '0');
      --  corrections_rate_reg <= '1';
      --end if;
		--if corrections_rate_reg3 = '1' then
		--	corrections_rate <= std_logic_vector(corrections_rate_out);
		--end if;

		end if;
	end if;
	end process pr_stats_collect;

	-- FIFOs for different stats
	inst_crc16_buf : axi4s_fifo
	generic map(
		DISTR_RAM         => true,
		FIFO_DEPTH        => 32,
		DATA_WIDTH        => 1,
		FULL_THRESHOLD    => 32,
		EMPTY_THRESHOLD   => 32,
		USE_OUTPUT_BUFFER => false
	)
	port map(

		clk            => clk,
		rst            => rst,

		-- Input data handling
		----------------------
		input             => crc16_correct_vec,
		input_valid       => crc16_valid,
		input_last        => '0',
		input_ready        => open,
		input_almost_full  => open,
		input_almost_empty => open,
		fifo_max_level     => open,

		-- Output data handling
		-----------------------
		output          => crc16_correct_buf,
		output_valid    => crc16_buf_valid,
		output_last     => open,
		output_ready    => crc16_buf_ready
	);

	-- FIFOs for different stats
	inst_crc32_buf : axi4s_fifo
	generic map(
		DISTR_RAM         => true,
		FIFO_DEPTH        => 32,
		DATA_WIDTH        => 1,
		FULL_THRESHOLD    => 32,
		EMPTY_THRESHOLD   => 32,
		USE_OUTPUT_BUFFER => false
	)
	port map(

		clk            => clk,
		rst            => rst,

		-- Input data handling
		----------------------
		input             => crc32_correct_vec,
		input_valid       => crc32_valid,
		input_last        => '0',
		input_ready        => open,
		input_almost_full  => open,
		input_almost_empty => open,
		fifo_max_level     => open,

		-- Output data handling
		-----------------------
		output          => crc32_correct_buf,
		output_valid    => crc32_buf_valid,
		output_last     => open,
		output_ready    => crc32_buf_ready
	);



end Behavioral;
