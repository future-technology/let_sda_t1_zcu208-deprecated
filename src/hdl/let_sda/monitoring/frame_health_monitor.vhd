----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 12/04/2021 11:40:33 AM
-- Design Name: 
-- Module Name: frame_health_monitor - Behavioral
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2018.3
-- Description: Calculates bitrate per second of an axi stream interface.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
library sda_lib;
use sda_lib.pkg_sda.all;


entity frame_health_monitor is
    Port ( 
        clk            : in  std_logic;
        rst            : in  std_logic;

        -- Input data from encoder
        ----------------------
        s_axis_tdata     : in  std_logic_vector(8 - 1 downto 0);
        s_axis_tvalid    : in  std_logic;
        s_axis_tlast     : in  std_logic;
        s_axis_tready    : in  std_logic;

        -- ERROR FLAGS
        warning_flag            : out  std_logic;
        critical_error_flag     : out  std_logic;

        health_monitor_statistics : out health_monitor_statistics_t

        );
end frame_health_monitor;

architecture Behavioral of frame_health_monitor is
    constant C_MAX_READY_CLK_DISABLED    : natural := 10; 

    signal byte_counter : unsigned(31 downto 0);
    signal ready_counter : unsigned(31 downto 0);
    signal total_packet_cnt_int : unsigned(31 downto 0);
    signal time_window_counter : unsigned(31 downto 0);
    signal bitrate_int : std_logic_vector(31 downto 0);
    signal data_type : std_logic;
    signal payload_header : std_logic;

    signal frame_length_error_int	    : unsigned(31 downto 0);
    signal not_ready_clks_int		    : unsigned(31 downto 0);
    signal wrong_idle_condition_int		: unsigned(31 downto 0);

begin

    warning_flag            <= '1' when wrong_idle_condition_int > 0 else '0';
    critical_error_flag     <= '1' when frame_length_error_int > 0 or not_ready_clks_int > C_MAX_READY_CLK_DISABLED else '0';

    health_monitor_statistics.frame_length_error		<= std_logic_vector(frame_length_error_int);
    health_monitor_statistics.not_ready_clks			<= std_logic_vector(not_ready_clks_int);
    health_monitor_statistics.wrong_idle_condition		<= std_logic_vector(wrong_idle_condition_int);

process(clk,rst)
begin
    if rst = '1' then
        data_type           <= '0';
        byte_counter <= (others=>'0');
        ready_counter <= (others=>'0');
        wrong_idle_condition_int <= (others=>'0');
        frame_length_error_int <= (others=>'0');
        not_ready_clks_int <= (others=>'0');
    else
        if rising_edge(clk) then
            
            -- FRAME LENGTH ERROR DETECTION
            if s_axis_tvalid = '1' and s_axis_tready = '1' and s_axis_tlast = '0' then
                byte_counter <= byte_counter + 1;
            elsif s_axis_tvalid = '1' and s_axis_tready = '1' and s_axis_tlast = '1' then
                byte_counter <= (others => '0');
                if byte_counter /= C_FRAME_FULL_SIZE_BYTES-1 then
                    frame_length_error_int <= frame_length_error_int + 1;
                end if;
            end if;

            -- WRONG IDLE CONDITION ERROR DETECTION
            if byte_counter = 7 then -- FRAME TYPE BYTE
                data_type <= s_axis_tdata(0);
            elsif byte_counter = C_FRAME_HEADER_SIZE - 1 + 4 then
                --if s_axis_tdata = C_PAYLOAD_PREAMBLE and data_type = '1' then
                    
                --Detect wrong condition: Frame IDLE with payload magic number:
                if s_axis_tdata = C_PAYLOAD_PREAMBLE and data_type = '0' then
                    wrong_idle_condition_int <= wrong_idle_condition_int + 1;   
                end if;
            end if;

            -- READY LOW TIME DETECTION
            if s_axis_tvalid = '1' and s_axis_tready = '0' then
                ready_counter <= ready_counter + 1;
            elsif s_axis_tvalid = '1' and s_axis_tready = '1' and s_axis_tlast = '1' then
                ready_counter <= (others => '0');
                if not_ready_clks_int < ready_counter then
                    not_ready_clks_int <= ready_counter;
                end if;
            end if;

        end if;

    end if;

end process;

end Behavioral;



