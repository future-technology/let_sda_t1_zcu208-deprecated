----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/24/2022 04:09:13 PM
-- Design Name: 
-- Module Name: tb_corrections_rate_calculator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_corrections_rate_calculator is
--  Port ( );
end tb_corrections_rate_calculator;

architecture Behavioral of tb_corrections_rate_calculator is

component  corrections_rate_calculator
	port(
		clk               : in std_logic;
		rst               : in std_logic;

		crc16_valid       : in  std_logic;
		crc16_correct     : in  std_logic;
		crc32_valid       : in  std_logic;
		crc32_correct     : in  std_logic;

		payload_cerr      : in  std_logic_vector(4 - 1 downto 0);
		payload_ncerr     : in  std_logic;
		payload_err_valid : in  std_logic;

		header_cerr       : in  std_logic_vector(4 - 1 downto 0);
		header_ncerr      : in  std_logic;
		header_err_valid  : in  std_logic;

		lpc_correct_valid : in  std_logic;
		lpc_correction    : in  std_logic_vector(4 downto 0);

		header_corrections_per_second  	: out std_logic_vector(31 downto 0);
		payload_corrections_per_second  : out std_logic_vector(31 downto 0);
		lpc_corrections_per_second  	: out std_logic_vector(31 downto 0);
		corrections_rate        : out std_logic_vector(31 downto 0);
		crc_errors_per_second   : out std_logic_vector(31 downto 0)
	);
	end component;

signal clk               		: std_logic := '0';
signal rst               		: std_logic := '0';
signal crc16_valid       		:  std_logic;
signal crc16_correct     		:  std_logic;
signal crc32_valid       		:  std_logic;
signal crc32_correct     		:  std_logic;
signal payload_cerr      		:  std_logic_vector(4 - 1 downto 0);
signal payload_ncerr     		:  std_logic := '0';
signal payload_err_valid 		:  std_logic := '0';
signal header_cerr       		:  std_logic_vector(4 - 1 downto 0);
signal header_ncerr      		:  std_logic := '0';
signal header_err_valid  		:  std_logic := '0';
signal lpc_correct_valid 		:  std_logic := '0';
signal lpc_correction    		:  std_logic_vector(4 downto 0);
signal header_corrections_per_second  	:  std_logic_vector(31 downto 0);
signal payload_corrections_per_second  :  std_logic_vector(31 downto 0);
signal lpc_corrections_per_second  	:  std_logic_vector(31 downto 0);
signal corrections_rate        	: std_logic_vector(31 downto 0);
signal crc_errors_per_second   	: std_logic_vector(31 downto 0);

begin



clk <= not clk after 2.5 ns;

process
begin
	rst <= '1';
	wait for 20 ns;
	rst <= '0';
	wait for 1 us;

	for k in 0 to 3000 loop
		wait until rising_edge(clk);
		crc16_valid     <= '1';
		crc16_correct   <= '0';
		wait until rising_edge(clk);
		crc16_valid     <= '0';
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		crc32_valid     <= '1';
		crc32_correct   <= '0';
		wait until rising_edge(clk);
		crc32_valid     <= '0';
		wait for 300 ns;
		
		wait until rising_edge(clk);
		payload_err_valid <= '1';
		header_err_valid <= '1';
		lpc_correct_valid <= '1';
		wait until rising_edge(clk);
		payload_err_valid <= '0';
		header_err_valid <= '0';
		lpc_correct_valid <= '0';
		
		wait for 3 us;

	end loop;
	
	wait;


end process;


inst_corrections_rate_calculator : corrections_rate_calculator
	port map(
		clk               		=> clk, 					--: in std_logic;
		rst               		=> rst, 					--: in std_logic;
		crc16_valid       		=> crc16_valid, 			--: in  std_logic;
		crc16_correct     		=> crc16_correct, 			--: in  std_logic;
		crc32_valid       		=> crc32_valid, 			--: in  std_logic;
		crc32_correct     		=> crc32_correct, 			--: in  std_logic;
		payload_cerr      		=> payload_cerr, 			--: in  std_logic_vector(4 - 1 downto 0);
		payload_ncerr     		=> payload_ncerr, 			--: in  std_logic;
		payload_err_valid 		=> payload_err_valid, 		--: in  std_logic;
		header_cerr       		=> header_cerr, 			--: in  std_logic_vector(4 - 1 downto 0);
		header_ncerr      		=> header_ncerr, 			--: in  std_logic;
		header_err_valid  		=> header_err_valid, 		--: in  std_logic;
		lpc_correct_valid 		=> lpc_correct_valid, 		--: in  std_logic;
		lpc_correction    		=> lpc_correction, 			--: in  std_logic_vector(4 downto 0);
		header_corrections_per_second  	=> header_corrections_per_second, 	--: out std_logic_vector(31 downto 0);
		payload_corrections_per_second  => payload_corrections_per_second, 	--: out std_logic_vector(31 downto 0);
		lpc_corrections_per_second  	=> lpc_corrections_per_second, 	--: out std_logic_vector(31 downto 0);
		corrections_rate        		=> corrections_rate, 		--: out std_logic_vector(31 downto 0);
		crc_errors_per_second   		=> crc_errors_per_second 	--: out std_logic_vector(31 downto 0)
	);


end Behavioral;
