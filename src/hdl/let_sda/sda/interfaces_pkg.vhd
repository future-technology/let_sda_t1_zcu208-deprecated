-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: interfaces_t1_pkg
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library sda_lib;
use sda_lib.pkg_sda.all;
library share_let_1g;
use share_let_1g.pkg_support.all;

package interfaces_pkg is

    constant C_ARQ_REQ_CACHE_BUS_SIZE : natural := 20;

    constant C_TIMESTAMP_WIDTH : natural := C_US_COUNTER_WIDTH; --no_bits_natural(C_US_COUNTER_PERIOD_CLOCKS);
    constant C_TX_FN_WIDTH : natural := 16;  
    constant C_TX_NUM_WIDTH : natural := 3;  
    constant C_FRAME_TYPE_WIDTH : natural := 2;  
    constant C_ADDRESS_WIDTH : natural := no_bits_natural(C_ARQ_HOLDOFF_NFRAMES_MAX); 
    constant C_ENTRY_WIDTH : natural := 1 + C_FRAME_TYPE_WIDTH + (C_TIMESTAMP_WIDTH*2) +C_ADDRESS_WIDTH+C_TX_FN_WIDTH+C_TX_NUM_WIDTH;  -- 1 extra bit for "cached"
    

    -- Interface Ethernet Framer
    type t_axis_if_m is record
        tdata      : std_logic_vector(7 downto 0); -- Data.
        tvalid     : std_logic;         -- Data valid
        tlast      : std_logic;         -- Active high during last data word in packet.
        tuser      : std_logic;          -- Error
        tstart     : std_logic;         -- Indicates start of transmission. Non AXI-s standard
    end record;

    type t_axis_if_s is record
        tready     : std_logic;         -- Data ready signal
    end record;

    -- Interface Ethernet Framer
    type t_axis_ethernet_framer_if_m is record
        axis_if_m    : t_axis_if_m;         -- AXI-S inteface
        tx_fn      : std_logic_vector(C_TX_FN_WIDTH-1 downto 0); -- Sequence number of this Tx Frame
        frame_type : std_logic_vector(1 downto 0); -- Frame Type
    end record;

    type t_axis_ethernet_framer_if_s is record
        axis_if_s    : t_axis_if_s;         -- AXI-S inteface
    end record;

    -- Interface Frame header
    type t_axis_payload_if_m is record
        axis_if_m    : t_axis_if_m;         -- AXI-S inteface
        tx_fn      : std_logic_vector(C_TX_FN_WIDTH-1 downto 0); -- Sequence number of this Tx Frame
        frame_type : std_logic_vector(1 downto 0); -- Frame Type
        tx_num     : std_logic_vector(C_TX_NUM_WIDTH-1 downto 0); -- Transmission atempt
    end record;

    type t_axis_payload_if_s is record
        axis_if_s    : t_axis_if_s;         -- AXI-S inteface
    end record;

    type t_ack_data_if_m is record
        ack             : std_logic;
        ack_start_fn    : std_logic_vector(C_TX_FN_WIDTH-1 downto 0);
        ack_span        : std_logic_vector(2 downto 0);
        ack_valid       : std_logic;
    end record;

    type t_ack_data_if_s is record
        rd_ena          : std_logic;
    end record;

    type t_fcch_if_m is record
        payload      : std_logic_vector(C_FCCH_PAYLOAD_BIT_WIDTH-1 downto 0);
        opcode     : std_logic_vector(C_FCCH_OPCODE_BIT_WIDTH-1 downto 0);
        valid       : std_logic;
    end record;

    type t_fcch_if_s is record
        rd_ena      : std_logic;
    end record;

    type t_metadata_retrieve_if is record
        tx_fn               : std_logic_vector(C_TX_FN_WIDTH-1 downto 0);
        addr                : std_logic_vector(C_ADDRESS_WIDTH-1 downto 0); -- ?
        valid               : std_logic;
        --ready               : std_logic;
    end record;

    -- Req insertion interface for req control tx
    type t_request_insertion_if_m is record
        tx_fn      : std_logic_vector(C_TX_FN_WIDTH-1 downto 0);
        frame_type : std_logic_vector(C_FRAME_TYPE_WIDTH-1 downto 0);
        valid      : std_logic;
    end record;

    type t_request_insertion_if_s is record
        ready      : std_logic;
    end record;

    -- Req insertion interface for RAM controller.
    type t_request_insertion_ram_if_m is record
        tx_fn      : std_logic_vector(C_TX_FN_WIDTH-1 downto 0);
        --frame_type : std_logic_vector(1 downto 0); -- ?
        addr       : std_logic_vector(C_ADDRESS_WIDTH-1 downto 0); -- ?
        valid       : std_logic;
        
    end record;
    
    type t_request_insertion_ram_if_s is record
        busy       : std_logic;
        ready      : std_logic;
    end record;

    type t_request_resend_if_m is record
        tx_fn               : std_logic_vector(C_TX_FN_WIDTH-1 downto 0);
        addr                : std_logic_vector(C_ADDRESS_WIDTH-1 downto 0); -- ?
        frame_type          : std_logic_vector(C_FRAME_TYPE_WIDTH-1 downto 0); 
        tx_num              : std_logic_vector(C_TX_NUM_WIDTH-1 downto 0);
        valid               : std_logic;
        pending_request_n     : std_logic;
    end record;

    type t_request_resend_if_s is record
        ready               : std_logic;
    end record;

    type t_request_resend_buf_if is record
        data                : std_logic_vector(C_ADDRESS_WIDTH+C_TX_FN_WIDTH+C_TX_NUM_WIDTH+C_FRAME_TYPE_WIDTH-1 downto 0);
        valid               : std_logic;
        ready               : std_logic;
        pending_request_n     : std_logic;
    end record;

    --type t_request_resend_buf_if_s is record
    --    
    --end record;

    type t_request_cache_if_m is record
        tx_fn               : std_logic_vector(C_TX_FN_WIDTH-1 downto 0);
        addr                : std_logic_vector(C_ADDRESS_WIDTH-1 downto 0); -- ?
        valid               : std_logic;
    end record;

    type t_request_cache_if_s is record
        ready               : std_logic;
    end record;

    type t_request_cache_bus_if is array (0 to C_ARQ_REQ_CACHE_BUS_SIZE-1) of t_request_cache_if_m;

    type t_arq_config_if is record
        holdoff_nframes             : std_logic_vector(12-1 downto 0);
        max_retx                    : std_logic_vector(3-1 downto 0);
        holdoff_time                : std_logic_vector(C_TIMESTAMP_WIDTH-1 downto 0);
        cache_trigger_time          : std_logic_vector(C_TIMESTAMP_WIDTH-1 downto 0);
        core_counter_period         : std_logic_vector(12-1 downto 0);
        cache_enable                : std_logic;
    end record;

    type t_arq_hw_config is record
        rd_core_counter_period      : std_logic_vector(8-1 downto 0); 
        rd_memory_block_size        : std_logic_vector(12-1 downto 0); 
    end record;

    type t_arq_control_block_tx_diagnostics_if is record
        insertion_req_counter   : unsigned(16-1 downto 0);
        retx_counter            : unsigned(16-1 downto 0);
        cache_req_counter       : unsigned(16-1 downto 0);
        acked_counter           : unsigned(16-1 downto 0);
        max_retx_counter        : unsigned(16-1 downto 0);
        --clear_all               : std_logic;
    end record;

    type t_arq_control_tx_diagnostics_if is record
        insertion_req_counter   : unsigned(32-1 downto 0);
        retx_counter            : unsigned(32-1 downto 0);
        cache_req_counter       : unsigned(32-1 downto 0);
        acked_counter           : unsigned(32-1 downto 0);
        max_retx_counter        : unsigned(32-1 downto 0);
        --clear_all               : std_logic;
    end record;

    type t_arq_ram_controller_diag_if is record
        insertion_req_counter   : unsigned(32-1 downto 0);
        retx_counter            : unsigned(32-1 downto 0);
        cache_req_counter       : unsigned(32-1 downto 0);
        cache_retx_counter      : unsigned(32-1 downto 0);
        cache_timeout_counter   : unsigned(32-1 downto 0);
        cache_resend_miss       : unsigned(32-1 downto 0);
        address_error           : unsigned(32-1 downto 0);
        insertion_error         : unsigned(32-1 downto 0);
        insertion_abort         : unsigned(32-1 downto 0);
        retx_error            : unsigned(32-1 downto 0);
        cache_error             : unsigned(32-1 downto 0);
        ram_error               : std_logic;
        --ram_controller_error    : std_logic;
        --clear_all               : std_logic;
    end record;

    type t_arq_int_ram_diag_if is record
        insertion_req_counter   : unsigned(32-1 downto 0);
        retx_counter            : unsigned(32-1 downto 0);
        cache_req_counter       : unsigned(32-1 downto 0);
        cache_retx_counter      : unsigned(32-1 downto 0);
        address_error           : unsigned(32-1 downto 0);
        --clear_all               : std_logic;
    end record;

    -- Entry data structure for the ledger
    type t_entry_unit is record
        tx_fn               : std_logic_vector(C_TX_FN_WIDTH-1 downto 0);
        addr                : std_logic_vector(C_ADDRESS_WIDTH-1 downto 0);
        tx_num              : std_logic_vector(C_TX_NUM_WIDTH-1 downto 0);
        timestamp           : unsigned(C_TIMESTAMP_WIDTH-1 downto 0);
        cache_timestamp     : unsigned(C_TIMESTAMP_WIDTH-1 downto 0);
        frame_type          : std_logic_vector(C_FRAME_TYPE_WIDTH-1 downto 0);
        cached              : std_logic; -- cached flag to trigger cache only once
    end record;

    -- Entry data interface for the ledger
    type t_entry_if is record
        data                : std_logic_vector(C_ENTRY_WIDTH-1 downto 0);
        valid               : std_logic;
        ready               : std_logic;
    end record;

    -- RAM command interface
    type t_ram_cmd_if_m is record
        request : std_logic;
        cmd_valid : std_logic;
        addr    : std_logic_vector(C_ADDRESS_WIDTH-1 downto 0);
    end record;
   
    type t_ram_cmd_if_s is record
        granted : std_logic;
        busy    : std_logic;
    end record;

    type t_seek_frame_if_m is record
        tx_fn   : std_logic_vector(C_TX_FN_WIDTH-1 downto 0);
        seek    : std_logic;
    end record;

    type t_seek_frame_if_s is record
        found   : std_logic;
        busy    : std_logic;
    end record;

    type t_frame_ts_if is record
        ts : std_logic_vector(39 downto 0);
    end record;

    type t_ts_association_if is record
        ts : std_logic_vector(C_TX_TS_BIT_WIDTH-1 downto 0);
        fn : std_logic_vector(16-1 downto 0);
    end record;
---------------------------------------------------------
--                 INITIALIZATION
---------------------------------------------------------

    constant C_AXIS_IF_MASTER_INIT : t_axis_if_m := (
        tdata       => (others => '0'),
        tuser       => '0',
        tvalid      => '0',
        tstart      => '0',
        tlast       => '0'
    );
    constant C_AXIS_IF_SLAVE_INIT : t_axis_if_s := (
        tready      => '0'
    );

    constant C_AXIS_ETHERNET_FRAMER_IF_SLAVE_INIT : t_axis_ethernet_framer_if_s := (
        axis_if_s             => C_AXIS_IF_SLAVE_INIT
    );
    
    constant C_AXIS_ETHERNET_FRAMER_IF_MASTER_INIT : t_axis_ethernet_framer_if_m := (
        axis_if_m           => C_AXIS_IF_MASTER_INIT,
        tx_fn               => (others => '0'),
        frame_type          => (others => '0')
    );

    constant C_AXIS_PAYLOAD_IF_SLAVE_INIT : t_axis_payload_if_s := (
        axis_if_s             => C_AXIS_IF_SLAVE_INIT
    );
    
    constant C_AXIS_PAYLOAD_IF_MASTER_INIT : t_axis_payload_if_m := (
        axis_if_m           => C_AXIS_IF_MASTER_INIT,
        tx_fn               => (others => '0'),
        frame_type          => (others => '0'),
        tx_num              => (others => '0')
    );
    
    constant C_ACK_DATA_IF_SLAVE_INIT : t_ack_data_if_s := (
        rd_ena          => '0'
    );
    
    constant C_ACK_DATA_IF_MASTER_INIT : t_ack_data_if_m := (
        ack             => '0', 
        ack_start_fn    => (others => '0'),
        ack_span        => (others => '0'),
        ack_valid       => '0'
    );

    constant C_FCCH_IF_SLAVE_INIT : t_fcch_if_s := (
        rd_ena    => '0'
    );

    constant C_FCCH_IF_MASTER_INIT : t_fcch_if_m := (
        payload   => (others => '0'),
        opcode    => (others => '0'),
        valid     => '0'
    );

    constant C_REQ_INSERTION_IF_SLAVE_INIT : t_request_insertion_if_s := (
        ready    => '0'
    );

    constant C_REQ_INSERTION_IF_MASTER_INIT : t_request_insertion_if_m := (
        tx_fn   => (others => '0'),
        frame_type => (others => '0'),
        valid   => '0'
    );

    constant C_REQ_INSERTION_RAM_IF_SLAVE_INIT : t_request_insertion_ram_if_s := (
        busy     => '0',
        ready    => '0'
    );

    constant C_REQ_INSERTION_RAM_IF_MASTER_INIT : t_request_insertion_ram_if_m := (
        tx_fn   => (others => '0'),
        addr   => (others => '0'),
        valid   => '0'
    );


    constant C_REQ_RESEND_IF_SLAVE_INIT : t_request_resend_if_s := (
        ready    => '0'
    );

    constant C_REQ_RESEND_IF_MASTER_INIT : t_request_resend_if_m := (
        tx_fn   => (others => '0'),
        tx_num   => (others => '0'),
        frame_type   => (others => '0'),
        addr   => (others => '0'),
        valid   => '0',
        pending_request_n => '0'
    );

    constant C_REQ_CACHE_IF_SLAVE_INIT : t_request_cache_if_s := (
        ready    => '0'
    );

    constant C_REQ_CACHE_IF_MASTER_INIT : t_request_cache_if_m := (
        tx_fn   => (others => '0'),
        valid   => '0',
        addr    => (others=>'0')
    );

    constant C_RAM_CMD_IF_MASTER_INIT : t_ram_cmd_if_m := (
        request     => '0',-- : std_logic;
        cmd_valid   => '0',-- : std_logic;
        addr        => (others => '0') -- : std_logic_vector(C_ADDRESS_WIDTH-1 downto 0);
    );
    
    constant C_RAM_CMD_IF_SLAVE_INIT : t_ram_cmd_if_s := (
        granted     => '0',-- : std_logic;
        busy        => '0'-- : std_logic;
    );

    constant C_METADATA_RETRIEVE_INIT : t_metadata_retrieve_if := (
        tx_fn => (others=>'0'),
        addr  => (others=>'0'),
        valid => '0'
    );

    constant C_ARQ_CONFIG_IF_MASTER_INIT : t_arq_config_if := (
        holdoff_nframes       => (others=>'0'), --      : std_logic_vector(12-1 downto 0);
        max_retx              => (others=>'0'), --      : std_logic_vector(3-1 downto 0);
        holdoff_time          => (others=>'0'), --      : std_logic_vector(C_TIMESTAMP_WIDTH-1 downto 0);
        cache_trigger_time    => (others=>'0'), --      : std_logic_vector(C_TIMESTAMP_WIDTH-1 downto 0);
        core_counter_period   => (others=>'0'), --      : std_logic_vector(12-1 downto 0);
        cache_enable          => '0'                
    );

    constant C_SEEK_FRAME_IF_MASTER_INIT : t_seek_frame_if_m := (
        tx_fn   => (others=>'0'), -- : std_logic_vector(C_TX_FN_WIDTH-1 downto 0);
        seek    => '0' -- : std_logic;
    );

    constant C_SEEK_FRAME_IF_SLAVE_INIT : t_seek_frame_if_s := (
        found   => '0', -- : std_logic;
        busy    => '0' -- : std_logic;
    );


    function request_resend_if_data2slv(request_resend_if : t_request_resend_if_m) return std_logic_vector;
    function request_resend_if_slv2tx_fn(slv : std_logic_vector) return std_logic_vector;
    function request_resend_if_slv2tx_num(slv : std_logic_vector) return std_logic_vector;
    function request_resend_if_slv2addr(slv : std_logic_vector) return std_logic_vector;
    function request_resend_if_slv2frame_type(slv : std_logic_vector) return std_logic_vector;

    function ack_data_if_data2slv(ack_data_if : t_ack_data_if_m) return std_logic_vector;
    function ack_data_if_slv2ack(ack_data_slv : std_logic_vector) return std_logic;
    function ack_data_if_slv2ack_start_fn(ack_data_slv : std_logic_vector) return std_logic_vector; 
    function ack_data_if_slv2ack_span(ack_data_slv : std_logic_vector) return std_logic_vector;

    function entry_if_data2slv(entry_unit : t_entry_unit) return std_logic_vector;
    function entry_if_slv2record(entry_if_slv : std_logic_vector) return t_entry_unit; 
    function entry_if_slv2tx_fn(entry_if_slv : std_logic_vector) return std_logic_vector; 
    function entry_if_slv2tx_num(entry_if_slv : std_logic_vector) return std_logic_vector; 
    function entry_if_slv2addr(entry_if_slv : std_logic_vector) return std_logic_vector; 
    function entry_if_slv2timestamp(entry_if_slv : std_logic_vector) return unsigned; 
    function entry_if_slv2frame_type(entry_if_slv : std_logic_vector) return std_logic_vector; 
    function entry_if_slv2cache_timestamp(entry_if_slv : std_logic_vector) return unsigned;
    function entry_if_slv2cached(entry_if_slv : std_logic_vector) return std_logic;
        
end package interfaces_pkg;

package body interfaces_pkg is
    
    function request_resend_if_data2slv(request_resend_if : t_request_resend_if_m) return std_logic_vector is
        variable slv : std_logic_vector(C_FRAME_TYPE_WIDTH+C_ADDRESS_WIDTH+C_TX_FN_WIDTH+C_TX_NUM_WIDTH - 1 downto 0);
    begin
        slv := request_resend_if.frame_type & request_resend_if.addr & request_resend_if.tx_num & request_resend_if.tx_fn;
        return slv;
    end request_resend_if_data2slv;

    function request_resend_if_slv2tx_fn(slv : std_logic_vector) return std_logic_vector is
        --variable slv : std_logic_vector(19 - 1 downto 0);
    begin
        return slv(15 downto 0);
    end request_resend_if_slv2tx_fn;

    function request_resend_if_slv2tx_num(slv : std_logic_vector) return std_logic_vector is
        --variable slv : std_logic_vector(19 - 1 downto 0);
    begin
        return slv(18 downto 16);
    end request_resend_if_slv2tx_num;

    function request_resend_if_slv2addr(slv : std_logic_vector) return std_logic_vector is
        --variable slv : std_logic_vector(19 - 1 downto 0);
    begin
        return slv(C_ADDRESS_WIDTH+19-1 downto 19);
    end request_resend_if_slv2addr;
    
    function request_resend_if_slv2frame_type(slv : std_logic_vector) return std_logic_vector is
        --variable slv : std_logic_vector(19 - 1 downto 0);
    begin
        return slv(C_FRAME_TYPE_WIDTH+C_ADDRESS_WIDTH+19-1 downto 19+C_ADDRESS_WIDTH);
    end request_resend_if_slv2frame_type;

    function ack_data_if_data2slv(ack_data_if : t_ack_data_if_m) return std_logic_vector is
        variable slv : std_logic_vector(16+1 - 1 downto 0);
    begin
        slv := ack_data_if.ack & ack_data_if.ack_start_fn;
        return slv;
    end ack_data_if_data2slv;

    function ack_data_if_slv2ack(ack_data_slv : std_logic_vector) return std_logic is
        variable ack : std_logic;
    begin   
        return ack_data_slv(16);
    end ack_data_if_slv2ack;

    function ack_data_if_slv2ack_start_fn(ack_data_slv : std_logic_vector) return std_logic_vector is
        variable ack_data_if : t_ack_data_if_m;
    begin
        return ack_data_slv(15 downto 0);
    end ack_data_if_slv2ack_start_fn;

    function ack_data_if_slv2ack_span(ack_data_slv : std_logic_vector) return std_logic_vector is
        variable ack_data_if : t_ack_data_if_m;
    begin
        return ack_data_slv(19 downto 17); 
    end ack_data_if_slv2ack_span;

    function entry_if_data2slv(entry_unit : t_entry_unit) return std_logic_vector is 
    variable slv : std_logic_vector(C_ENTRY_WIDTH - 1 downto 0);
    begin
        slv := entry_unit.cached & std_logic_vector(entry_unit.cache_timestamp) & entry_unit.frame_type & std_logic_vector(entry_unit.timestamp) & entry_unit.tx_num & entry_unit.addr & entry_unit.tx_fn; --TODO
        return slv;
    end entry_if_data2slv;

    function entry_if_slv2tx_fn(entry_if_slv : std_logic_vector) return std_logic_vector is 
    begin
        return entry_if_slv(C_TX_FN_WIDTH-1 downto 0); 
    end entry_if_slv2tx_fn;

    function entry_if_slv2tx_num(entry_if_slv : std_logic_vector) return std_logic_vector is 
    begin
        return entry_if_slv(C_TX_NUM_WIDTH+C_TX_FN_WIDTH+C_ADDRESS_WIDTH-1 downto C_TX_FN_WIDTH+C_ADDRESS_WIDTH); 
    end entry_if_slv2tx_num;

    function entry_if_slv2addr(entry_if_slv : std_logic_vector) return std_logic_vector is 
    begin
        return entry_if_slv(C_ADDRESS_WIDTH+C_TX_FN_WIDTH-1 downto C_TX_FN_WIDTH); 
    end entry_if_slv2addr;

    function entry_if_slv2timestamp(entry_if_slv : std_logic_vector) return unsigned is 
    begin
        return unsigned(entry_if_slv(C_ENTRY_WIDTH-C_TIMESTAMP_WIDTH-C_FRAME_TYPE_WIDTH-1-1 downto C_TX_NUM_WIDTH+C_TX_FN_WIDTH+C_ADDRESS_WIDTH)); 
    end entry_if_slv2timestamp;

    function entry_if_slv2frame_type(entry_if_slv : std_logic_vector) return std_logic_vector is 
    begin
        return entry_if_slv(C_ENTRY_WIDTH-C_TIMESTAMP_WIDTH-1-1 downto C_TIMESTAMP_WIDTH+C_TX_NUM_WIDTH+C_TX_FN_WIDTH+C_ADDRESS_WIDTH); 
    end entry_if_slv2frame_type;

    function entry_if_slv2cache_timestamp(entry_if_slv : std_logic_vector) return unsigned is 
    begin
        return unsigned(entry_if_slv(C_ENTRY_WIDTH-1-1 downto C_TIMESTAMP_WIDTH+C_FRAME_TYPE_WIDTH+C_TX_NUM_WIDTH+C_TX_FN_WIDTH+C_ADDRESS_WIDTH)); 
    end entry_if_slv2cache_timestamp;

    function entry_if_slv2cached(entry_if_slv : std_logic_vector) return std_logic is 
    begin
        return entry_if_slv(C_ENTRY_WIDTH-1); 
    end entry_if_slv2cached;
    
    function entry_if_slv2record(entry_if_slv : std_logic_vector) return t_entry_unit is
        variable v_entry_unit : t_entry_unit;
    begin
        v_entry_unit.timestamp          := entry_if_slv2timestamp(entry_if_slv);
        v_entry_unit.cache_timestamp    := entry_if_slv2cache_timestamp(entry_if_slv);
        v_entry_unit.tx_num     := entry_if_slv2tx_num(entry_if_slv);
        v_entry_unit.addr       := entry_if_slv2addr(entry_if_slv);
        v_entry_unit.tx_fn      := entry_if_slv2tx_fn(entry_if_slv);
        v_entry_unit.frame_type := entry_if_slv2frame_type(entry_if_slv);
        v_entry_unit.cached     := entry_if_slv2cached(entry_if_slv);
        return v_entry_unit;
    end entry_if_slv2record;

end package body interfaces_pkg;
