--!
--! Copyright (C)  2021 Mynaric
--!
--! @file
--! @brief  Package for SDA definitions.
--! @author Gustavo Martin
--! @date   2021/04/03
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library share_let_1g;
use share_let_1g.pkg_support.all;

package pkg_sda is

    constant C_FRAME_HEADER_SIZE     : natural                           := 18; -- Update SDA T1  -- Old: Frame Header 14 bytes
    constant C_FRAME_HEADER_HC       : std_logic_vector((C_FRAME_HEADER_SIZE * 8) - 1 downto 0) := X"000000000000000000000000000000000100";
    constant C_PAYLOAD_PREAMBLE      : std_logic_vector(8 - 1 downto 0)  := x"AB";
    constant C_PACKET_PREAMBLE       : std_logic_vector(16 - 1 downto 0) := x"CDEF";
    constant C_FSO_FRAME_SIZE        : natural                           := 1052; -- Update SDA T1  -- Old: 239 * 8 - 4; -- Update SDA T1 -- FSO frame 1908 bytes
    constant C_FSO_PAYLOAD_SIZE      : natural                           := C_FSO_FRAME_SIZE; -- Update SDA T1  -- Old: 239 * 8 - 4; -- Update SDA T1 -- FSO frame 1908 bytes
    constant C_FRAME_FULL_SIZE_BYTES : natural                           := (C_FRAME_HEADER_SIZE + C_FSO_FRAME_SIZE);
    constant C_FRAME_FULL_SIZE       : natural                           := ((C_FRAME_HEADER_SIZE + C_FSO_FRAME_SIZE) * 8);
    constant C_HEADER_SIZE           : natural                           := 4;
    constant C_MGMT_PAYLOAD_SIZE     : natural                           := 4 * 5; -- 20 bytes
    constant C_ETHERNET_PACKET_MAX_LENGTH_BYTES     : natural            := 1518; -- maximum frame size of 1518 bytes
    constant C_FSO_PAYLOAD_HEADER_MAX_LENGTH        : natural            := C_FSO_FRAME_SIZE - C_HEADER_SIZE; -- 
    constant C_ARQ_HOLDOFF_NFRAMES_MAX              : natural            := 4096; -- MAX hold-off FRAMES
    constant C_ARQ_HOLDOFF_TIME_MAX                 : natural            := 2500; -- MAX hold-off time in uS
    constant C_ARQ_MAX_RETX                         : natural            := 5; -- Maximum number of retransmission attempts.
    constant C_US_COUNTER_MAX_HOLDOFF     : natural := C_ARQ_HOLDOFF_TIME_MAX*10; -- in tenths of uS (100 ns/lsb)--SDA T1 req: 2500 ms max time for holdoff 
    constant C_US_COUNTER_WIDTH         : natural := 16; -- factor of 10 is enough
    constant C_INTERNAL_RAM_BITWIDTH : natural := 64;    
    constant C_INTERNAL_RAM_BLOCK_SIZE : natural := (C_FSO_FRAME_SIZE+36);    
    constant C_INTERNAL_RAM_WORDS_MAX : natural := (C_ARQ_HOLDOFF_NFRAMES_MAX*(C_INTERNAL_RAM_BLOCK_SIZE))/C_INTERNAL_RAM_BITWIDTH/8;
    constant C_INTERNAL_RAM_ADDR_WIDTH_MAX : natural := no_bits_natural(C_INTERNAL_RAM_WORDS_MAX - 1);
    constant C_TX_TS_BIT_WIDTH : natural := 40;
    constant C_TODSEC_BIT_WIDTH : natural := 6;
    constant C_TS_APPLIES_BIT_WIDTH : natural := 3;
    constant C_FCCH_OPCODE_BIT_WIDTH : natural := 6;
    constant C_FCCH_PAYLOAD_BIT_WIDTH : natural := 16;

    constant C_FRAME_IDLE : std_logic_vector(1 downto 0) := "00";
    constant C_FRAME_DATA : std_logic_vector(1 downto 0) := "01";
    constant C_FRAME_MGMT : std_logic_vector(1 downto 0) := "10";
    
    --type T_FRAME_TYPE is (FT_FRAME_IDLE, FT_FRAME_DATA, FT_FRAME_MGMT);
    --attribute enum_encoding : string;
    --attribute enum_encoding of T_FRAME_TYPE : type is "00 01 10";

    constant C_MGMT_REQ  : std_logic := '0';
    constant C_MGMT_RESP : std_logic := '1';

    constant C_MGMT_RESPONSE_SIZE   : natural := 4 * 5; -- 20 bytes
    constant C_RAM_TX_DATA_BITWIDTH : natural := 40 + 3;
    constant C_RAM_RX_DATA_BITWIDTH : natural := 40 + 1; -- ts+valid
    constant C_RAM_TX_WORDS         : natural := 64; --32768;
    constant C_RAM_RX_WORDS         : natural := 64; --32768;

    type frame_header_ram_t is array (C_FRAME_HEADER_SIZE - 1 downto 0) of std_logic_vector(7 downto 0);
    type payload_ram_t is array (C_FSO_FRAME_SIZE - 1 downto 0) of std_logic_vector(7 downto 0);
    type frame_full_ram_t is array (C_FRAME_FULL_SIZE_BYTES - 1 downto 0) of std_logic_vector(7 downto 0);
    type mgmt_payload_ram_t is array (C_MGMT_PAYLOAD_SIZE - 1 downto 0) of std_logic_vector(7 downto 0);
    --type t2t3_response_ram_t is array(C_T2T3_RESPONSE_SIZE-1 downto 0) of std_logic_vector(7 downto 0);

    --------------------------
    --      FCCH CHANNEL    --
    --------------------------
    -- Not Present
    constant C_FCCH_NOT_PRESENT       : std_logic_vector(C_FCCH_OPCODE_BIT_WIDTH - 1 downto 0) := "111111";

    ------------------------------------------------------------------------
    type payload_header_t is record
        magic_number : std_logic_vector(7 downto 0);
        seq_num      : std_logic_vector(9 downto 0);
        length       : std_logic_vector(13 downto 0);
    end record;

    type packet_header_t is record
        magic_number : std_logic_vector(15 downto 0);
        reserved     : std_logic_vector(1 downto 0);
        length       : std_logic_vector(13 downto 0);
    end record;

    type frame_header_fields_t is record
        txfn         : std_logic_vector(16-1 downto 0);
        ack_start_fn : std_logic_vector(16-1 downto 0);
        ack_span     : std_logic_vector(3-1 downto 0);
        ack_valid    : std_logic;
        ack          : std_logic;
        tx_num       : std_logic_vector(3-1 downto 0);
        arq_nframes  : std_logic_vector(8-1 downto 0);
        arq_max_retx : std_logic_vector(3-1 downto 0);
        pl_rate      : std_logic_vector(4-1 downto 0);
        frame_type   : std_logic_vector(2-1 downto 0);
        tx_ts        : std_logic_vector(40-1 downto 0);
        tod_seconds  : std_logic_vector(6-1 downto 0);
        ts_applies   : std_logic_vector(3-1 downto 0);
        fcch_opcode  : std_logic_vector(6-1 downto 0);
        fcch_pl      : std_logic_vector(16-1 downto 0);
        zerotail     : std_logic_vector(16-1 downto 0);
    end record;

    type performance_statistics_t is record
        bitrate_in     : std_logic_vector(31 downto 0);
        bitrate_out    : std_logic_vector(31 downto 0);
        packet_cnt_in  : std_logic_vector(31 downto 0);
        packet_cnt_out : std_logic_vector(31 downto 0);
    end record;

    type framer_statistics_t is record
        total_packet_counter          : std_logic_vector(31 downto 0);
        total_packet_splitted_counter : std_logic_vector(31 downto 0);
        total_data_frame_counter      : std_logic_vector(31 downto 0);
        total_idle_frame_counter      : std_logic_vector(31 downto 0);
        total_mgmt_frame_counter      : std_logic_vector(31 downto 0);
        total_packet_drop             : std_logic_vector(31 downto 0);
        frame_length_error            : std_logic_vector(31 downto 0);
    end record;

    type deframer_statistics_t is record
        total_payload_counter         : std_logic_vector(31 downto 0);
        total_packet_counter          : std_logic_vector(31 downto 0);
        total_packet_splitted_counter : std_logic_vector(31 downto 0);
        total_packet_merged_counter   : std_logic_vector(31 downto 0);
        total_data_frame_counter      : std_logic_vector(31 downto 0);
        total_idle_frame_counter      : std_logic_vector(31 downto 0);
        total_payload_error_counter   : std_logic_vector(31 downto 0);
        total_packet_error_counter    : std_logic_vector(31 downto 0);
        watchdog_reset_counter        : std_logic_vector(31 downto 0);
        rx_fifo_skipped_frame         : std_logic_vector(31 downto 0);
        packet_filter_cnt_in          : std_logic_vector(31 downto 0);
        packet_filter_cnt_out         : std_logic_vector(31 downto 0);
        frame_filter_cnt_in           : std_logic_vector(31 downto 0);
        frame_filter_cnt_out          : std_logic_vector(31 downto 0);
    end record;

    type framer_diagnostics_t is record
        framer_statistics : framer_statistics_t;
        performance       : performance_statistics_t;
    end record;

    type deframer_diagnostics_t is record
        deframer_statistics : deframer_statistics_t;
        performance         : performance_statistics_t;
    end record;

    type sda_debug_t is record
        frame_header   : frame_header_fields_t;
        payload_header : std_logic_vector(32 - 1 downto 0);
        packet_header  : std_logic_vector(32 - 1 downto 0);
    end record;

    type health_monitor_statistics_t is record
        frame_length_error   : std_logic_vector(31 downto 0);
        not_ready_clks       : std_logic_vector(31 downto 0);
        wrong_idle_condition : std_logic_vector(31 downto 0);
    end record;

    type tx_timing_data_t is record
        tx_fn      : std_logic_vector(16 - 1 downto 0);
        tx_ts      : std_logic_vector(40 - 1 downto 0);
        ts_applies : std_logic_vector(3 - 1 downto 0);
    end record;

    type rx_timing_data_t is record
        tx_data    : tx_timing_data_t;
        rx_ts      : std_logic_vector(40 - 1 downto 0);
        mgmt_valid : std_logic;
        mgmt_type  : std_logic;
    end record;

    type complete_timing_data_t is record
        rx_data : rx_timing_data_t;
        tx_data : tx_timing_data_t;
    end record;

    type t_ts_insertion_if is record
        tx_ts           : std_logic_vector(C_TX_TS_BIT_WIDTH-1 downto 0);
        tod_seconds     : std_logic_vector(C_TODSEC_BIT_WIDTH-1 downto 0);
        ts_applies      : std_logic_vector(C_TS_APPLIES_BIT_WIDTH-1 downto 0);
    end record;
    
    type t_timestamp_data_array is array (0 to 7-1) of std_logic_vector(8-1 downto 0);
    signal ts_data_array : t_timestamp_data_array;

    constant C_FRAME_HEADER_INIT : frame_header_fields_t := (txfn         => (others => '0'),
                                                             ack_start_fn => (others => '0'),
                                                             ack_span     => (others => '0'),
                                                             ack_valid    => '0',
                                                             ack          => '0',
                                                             tx_num       => (others => '0'),
                                                             arq_nframes  => (others => '0'),
                                                             arq_max_retx => (others => '0'),
                                                             pl_rate      => (others => '0'),
                                                             frame_type   => (others => '0'),
                                                             tx_ts        => (others => '0'),
                                                             tod_seconds  => (others => '0'),
                                                             ts_applies   => (others => '0'),
                                                             fcch_opcode  => (others => '0'),
                                                             fcch_pl      => (others => '0'),
                                                             zerotail     => (others => '0')
                                                            );

    constant C_TX_TIMING_DATA_INIT : tx_timing_data_t := (tx_fn      => (others => '0'),
                                                          tx_ts      => (others => '0'),
                                                          ts_applies => (others => '0')
                                                         );

    constant C_RX_TIMING_DATA_INIT : rx_timing_data_t := (tx_data    => C_TX_TIMING_DATA_INIT,
                                                          rx_ts      => (others => '0'),
                                                          mgmt_valid => '0',
                                                          mgmt_type  => '0'
                                                         );

    function packet_header2slv(ph : packet_header_t) return std_logic_vector;
    function slv2packet_header(slv : std_logic_vector) return packet_header_t;
    function payload_header2slv(ph : payload_header_t) return std_logic_vector;
    function slv2payload_header(slv : std_logic_vector) return payload_header_t;
    function frame_header2slv(fh : frame_header_fields_t) return std_logic_vector;
    function slv2frame_header(slv : std_logic_vector) return frame_header_fields_t;
    function array2frame_header(ram : frame_header_ram_t) return frame_header_fields_t;
    function frame_header2ram(fhf : frame_header_fields_t) return frame_header_ram_t;

    ------------
    -- Ranging -
    ------------
    type mgmt_payload_t is record
        mgmt_seq               : std_logic_vector(16 - 1 downto 0);
        mgmt_type              : std_logic;
        response_valid         : std_logic;
        ranging_meas_valid     : std_logic;
        reserved1              : std_logic_vector(13 - 1 downto 0);
        egress_ts              : std_logic_vector(40 - 1 downto 0);
        ingress_ts             : std_logic_vector(40 - 1 downto 0);
        egress_tx_fn           : std_logic_vector(16 - 1 downto 0);
        ranging_measure_meters : std_logic_vector(32 - 1 downto 0);
    end record;

    constant C_MGMT_RESPONSE_INIT : mgmt_payload_t := (mgmt_seq               => (others => '0'),
                                                       mgmt_type              => '0',
                                                       response_valid         => '0',
                                                       ranging_meas_valid     => '0',
                                                       reserved1              => (others => '0'),
                                                       egress_ts              => (others => '0'),
                                                       ingress_ts             => (others => '0'),
                                                       egress_tx_fn           => (others => '0'),
                                                       ranging_measure_meters => (others => '0')
                                                      );

    function mgmt_response2slv(resp : mgmt_payload_t) return std_logic_vector;
    function slv2mgmt_response(slv : std_logic_vector) return mgmt_payload_t;
    function array2mgmt_response(ram : mgmt_payload_ram_t) return mgmt_payload_t;
    
    -- Timestamp:
    function ts_insertion_if2slv(ts_if : t_ts_insertion_if) return std_logic_vector;
    function slv2ts_insertion_if(slv : std_logic_vector) return t_ts_insertion_if;
    function ts_insertion_if2array(ts_if : t_ts_insertion_if) return t_timestamp_data_array;
    function array2ts_insertion_if(ram : t_timestamp_data_array) return t_ts_insertion_if;
    
end package pkg_sda;

package body pkg_sda is

    function slv2packet_header(slv : std_logic_vector) return packet_header_t is
        variable ph : packet_header_t;
    begin
        ph.magic_number := slv(31 downto 16);
        ph.reserved     := slv(15 downto 14);
        ph.length       := slv(13 downto 0);
        return ph;
    end function;
    
    function packet_header2slv(ph : packet_header_t) return std_logic_vector is
        variable slv : std_logic_vector((C_HEADER_SIZE * 8) - 1 downto 0);
    begin
        slv := C_PACKET_PREAMBLE & ph.reserved & ph.length;
        return slv;
    end function;

    function slv2payload_header(slv : std_logic_vector) return payload_header_t is
        variable ph : payload_header_t;
    begin
        ph.magic_number := slv(31 downto 24);
        ph.seq_num      := slv(23 downto 14);
        ph.length       := slv(13 downto 0);
        return ph;
    end function;
    
    function payload_header2slv(ph : payload_header_t) return std_logic_vector is
        variable slv : std_logic_vector((C_HEADER_SIZE * 8) - 1 downto 0);
    begin
        slv := C_PAYLOAD_PREAMBLE & ph.seq_num & ph.length;
        return slv;
    end function;

    function frame_header2slv(fh : frame_header_fields_t) return std_logic_vector is
        variable slv : std_logic_vector((C_FRAME_HEADER_SIZE * 8) - 1 downto 0);
    begin
        slv := fh.zerotail & fh.fcch_pl & fh.fcch_opcode & fh.ts_applies & fh.tod_seconds & fh.tx_ts & fh.frame_type & fh.pl_rate & fh.arq_max_retx & fh.arq_nframes & fh.tx_num & fh.ack & fh.ack_valid & fh.ack_span & fh.ack_start_fn & fh.txfn;
        return slv;
    end frame_header2slv;

    function slv2frame_header(slv : std_logic_vector) return frame_header_fields_t is
        variable fh : frame_header_fields_t;
    begin
        -------
        fh.txfn         := slv(16 - 1 downto 0);
        fh.ack_start_fn := slv(32 - 1 downto 16);
        fh.ack_span     := slv(35 - 1 downto 32);
        fh.ack_valid    := slv(35);
        fh.ack          := slv(36);
        fh.tx_num       := slv(40 - 1 downto 37);
        fh.arq_nframes  := slv(48 - 1 downto 40);
        fh.arq_max_retx := slv(51 - 1 downto 48);
        fh.pl_rate      := slv(55 - 1 downto 51);
        fh.frame_type   := slv(57 - 1 downto 55);
        fh.tx_ts        := slv(97 - 1 downto 57);
        fh.tod_seconds  := slv(103 - 1 downto 97);
        fh.ts_applies   := slv(106 - 1 downto 103);
        fh.fcch_opcode  := slv(112 - 1 downto 106);
        fh.fcch_pl      := slv(128 - 1 downto 112);
        fh.zerotail     := slv(144 - 1 downto 128);

        return fh;
    end slv2frame_header;

    function array2frame_header(ram : frame_header_ram_t) return frame_header_fields_t is
        variable fh             : frame_header_fields_t;
        variable v_frame_header : std_logic_vector((C_FRAME_HEADER_SIZE * 8) - 1 downto 0) := (others => '0');
    begin

        array2vector_loop : for i in 0 to C_FRAME_HEADER_SIZE - 1 loop
            v_frame_header(((i + 1) * 8) - 1 downto i * 8) := ram(i);
        end loop;                       -- identifier
        fh := slv2frame_header(v_frame_header);
        return fh;
    end array2frame_header;

    function frame_header2ram(fhf : frame_header_fields_t) return frame_header_ram_t is
        variable fh_ram             : frame_header_ram_t;
        variable v_frame_header : std_logic_vector((C_FRAME_HEADER_SIZE * 8) - 1 downto 0) := (others => '0');
    begin
        v_frame_header := frame_header2slv(fhf);
        array2vector_loop : for i in 0 to C_FRAME_HEADER_SIZE - 1 loop
            fh_ram(i) := v_frame_header(((i + 1) * 8) - 1 downto i * 8);
        end loop;                       -- identifier
        
        return fh_ram;
    end frame_header2ram;
    
    ---------------------------
    -- Timestamp             --
    ---------------------------
    function ts_insertion_if2slv(ts_if : t_ts_insertion_if) return std_logic_vector is
        variable slv : std_logic_vector(C_TX_TS_BIT_WIDTH+C_TODSEC_BIT_WIDTH+C_TS_APPLIES_BIT_WIDTH - 1 downto 0);
    begin
        slv := ts_if.ts_applies & ts_if.tod_seconds & ts_if.tx_ts;
        return slv;
    end ts_insertion_if2slv;

    function slv2ts_insertion_if(slv : std_logic_vector) return t_ts_insertion_if is
        variable resp : t_ts_insertion_if;
    begin
        resp.ts_applies         := slv(C_TODSEC_BIT_WIDTH+C_TX_TS_BIT_WIDTH+C_TS_APPLIES_BIT_WIDTH - 1 downto C_TODSEC_BIT_WIDTH+C_TX_TS_BIT_WIDTH);
        resp.tod_seconds        := slv(C_TODSEC_BIT_WIDTH+C_TX_TS_BIT_WIDTH - 1 downto C_TX_TS_BIT_WIDTH);
        resp.tx_ts              := slv(C_TX_TS_BIT_WIDTH - 1 downto 0);
        return resp;
    end slv2ts_insertion_if;

    function ts_insertion_if2array(ts_if : t_ts_insertion_if) return t_timestamp_data_array is
        variable slv : std_logic_vector(C_TX_TS_BIT_WIDTH+C_TODSEC_BIT_WIDTH+C_TS_APPLIES_BIT_WIDTH - 1 downto 0);
        variable slv_words : std_logic_vector((7*8) - 1 downto 0);
        variable ram : t_timestamp_data_array;
    begin
        slv := ts_insertion_if2slv(ts_if);
        slv_words := "000000" & slv & "0";

        array2vector_loop : for i in 0 to 7 - 1 loop -- 49bits/8 = 6Bytes + 1 bit
            ram(i) := slv_words(((i + 1) * 8) - 1 downto i * 8);
        end loop; 
        return ram;
    end ts_insertion_if2array;

    function array2ts_insertion_if(ram : t_timestamp_data_array) return t_ts_insertion_if is
        variable resp   : t_ts_insertion_if;
        variable slv : std_logic_vector(C_TX_TS_BIT_WIDTH+C_TODSEC_BIT_WIDTH+C_TS_APPLIES_BIT_WIDTH - 1 downto 0);
        variable slv_words : std_logic_vector((7*8) - 1 downto 0);    
    begin
        array2vector_loop : for i in 0 to 7 - 1 loop
            slv_words(((i + 1) * 8) - 1 downto i * 8) := ram(i);
        end loop;                       -- identifier
        slv := slv_words((7*8) - 1 - 6 downto 1);
        resp := slv2ts_insertion_if(slv);
        return resp;
    end array2ts_insertion_if;

    ---------------------------
    -- Ranging mgmt Response --
    ---------------------------
    function mgmt_response2slv(resp : mgmt_payload_t) return std_logic_vector is
        variable slv : std_logic_vector((C_MGMT_PAYLOAD_SIZE * 8) - 1 downto 0);
    begin
        slv := resp.ranging_measure_meters & resp.egress_tx_fn & resp.ingress_ts & resp.egress_ts & resp.reserved1 & resp.ranging_meas_valid & resp.response_valid & resp.mgmt_type & resp.mgmt_seq;
        return slv;
    end mgmt_response2slv;

    function slv2mgmt_response(slv : std_logic_vector) return mgmt_payload_t is
        variable resp : mgmt_payload_t;
    begin
        resp.ranging_measure_meters := slv(128 + 32 - 1 downto 128);
        resp.egress_tx_fn           := slv(112 + 16 - 1 downto 112);
        resp.ingress_ts             := slv(72 + 40 - 1 downto 72);
        resp.egress_ts              := slv(32 + 40 - 1 downto 32);
        resp.reserved1              := slv(32 - 1 downto 19);
        resp.ranging_meas_valid     := slv(18);
        resp.response_valid         := slv(17);
        resp.mgmt_type              := slv(16);
        resp.mgmt_seq               := slv(16 - 1 downto 0);
        return resp;
    end slv2mgmt_response;

    function array2mgmt_response(ram : mgmt_payload_ram_t) return mgmt_payload_t is
        variable resp   : mgmt_payload_t;
        variable v_resp : std_logic_vector((C_MGMT_PAYLOAD_SIZE * 8) - 1 downto 0) := (others => '0');
    begin

        array2vector_loop : for i in 0 to C_MGMT_PAYLOAD_SIZE - 1 loop
            v_resp(((i + 1) * 8) - 1 downto i * 8) := ram(i);
        end loop;                       -- identifier
        resp := slv2mgmt_response(v_resp);
        return resp;
    end array2mgmt_response;

end package body pkg_sda;

