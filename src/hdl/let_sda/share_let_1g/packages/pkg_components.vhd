--!
--! Copyright (C) 2012 Creonic GmbH
--!
--! @file
--! @brief  DVB-S2 Encoder IP Component declarations.
--! @author Matthias Alles
--! @date   2012/05/10
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library share_let_1g;
use share_let_1g.pkg_support.all;
use share_let_1g.pkg_param.all;

package pkg_components is

	component bit_sync is
		port(
			clk_in   : in std_logic;
			data_in  : in std_logic;
			data_out : out std_logic
		);
	end component bit_sync;

	component scrambler is
		port (
			clk : in std_logic;
			rst : in std_logic;
			data_in   : in std_logic_vector (119 downto 0);
			scram_en  : in std_logic;
			scram_rst : in std_logic;

			data_out : out std_logic_vector (119 downto 0)
		);
	end component scrambler;

	component generic_simple_dp_ram is
		generic(
			DISTR_RAM  : boolean := true;
			WORDS      : integer := 5;
			BITWIDTH   : integer := 1
		);
		port(
			clk : in std_logic;
			rst : in std_logic;

			wen_A : in  std_logic;
			en_A  : in  std_logic;

			a_A  : in  std_logic_vector(no_bits_natural(WORDS - 1) - 1 downto 0);
			d_A  : in  std_logic_vector(BITWIDTH - 1 downto 0 );

			en_B : in  std_logic;
			a_B  : in  std_logic_vector(no_bits_natural(WORDS - 1) - 1 downto 0);
			q_B  : out std_logic_vector(BITWIDTH - 1 downto 0)
		);
	end component generic_simple_dp_ram;

	component axi4s_fifo is
		generic (
			DISTR_RAM         : boolean := true;
			FIFO_DEPTH        : natural := 128;
			DATA_WIDTH        : natural := 1;
			FULL_THRESHOLD    : natural := 128;
			EMPTY_THRESHOLD   : natural := 1;
			USE_OUTPUT_BUFFER : boolean := false
		);
		port (

		clk            : in  std_logic;
		rst            : in  std_logic;

		-- Input data handling
		----------------------
		input             : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		input_valid       : in  std_logic;
		input_last        : in  std_logic;
		input_ready       : out std_logic;
		input_almost_full : out std_logic;
		input_almost_empty : out std_logic;
		fifo_max_level    : out std_logic_vector(no_bits_natural(FIFO_DEPTH) - 1 downto 0);

		-- Output data handling
		-----------------------
		output          : out std_logic_vector(DATA_WIDTH - 1 downto 0);
		output_valid    : out std_logic;
		output_last     : out std_logic;
		output_ready    : in  std_logic
	);
	end component axi4s_fifo;

	component axi4s_buffer is
	generic (
		DATA_WIDTH : natural
	);
	port (

		clk            : in  std_logic;
		rst            : in  std_logic;

		-- Input data handling
		----------------------
		input             : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		input_valid       : in  std_logic;
		input_last        : in  std_logic;
		input_ready       : out std_logic;


		-- Output data handling
		-----------------------
		output          : out std_logic_vector(DATA_WIDTH - 1 downto 0);
		output_valid    : out std_logic;
		output_last     : out std_logic;
		output_ready    : in  std_logic
	);
	end component axi4s_buffer;

	component inter_buffer is
		generic(
			BW_SYMB      : integer := 8;
			PARALLELISM  : integer := 15;
			MAX_ROW      : integer := 235;
			MAX_COL      : integer := 255;
			MAX_BLK_SEND : integer := 255
		);
		port(
			clk   : in std_logic;
			rst   : in std_logic;

			input        : in  std_logic_vector(PARALLELISM * BW_SYMB - 1 downto 0);
			input_valid  : in  std_logic;
			input_ready  : out std_logic;
			input_start  : in  std_logic;
			input_end    : in  std_logic;

			output       : out std_logic_vector(PARALLELISM * BW_SYMB - 1 downto 0);
			output_valid : out std_logic;
			output_ready : in  std_logic;
			output_start : out std_logic;
			output_end   : out std_logic
		);
	end component inter_buffer;

	component axi4s_fifo_v2 is
		generic (
			DISTR_RAM         : boolean := true;
			FIFO_DEPTH        : natural := 128;
			DATA_WIDTH        : natural := 1;
			FULL_THRESHOLD    : natural := 128;
			EMPTY_THRESHOLD   : natural := 1;
			USE_OUTPUT_BUFFER : boolean := false
		);
		port (

		clk            : in  std_logic;
		rst            : in  std_logic;

		-- Input data handling
		----------------------
		input             : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		input_valid       : in  std_logic;
		input_last        : in  std_logic;
		input_user        : in  std_logic;
		input_ready       : out std_logic;
		input_almost_full : out std_logic;
		input_almost_empty : out std_logic;
		fifo_max_level    : out std_logic_vector(no_bits_natural(FIFO_DEPTH) - 1 downto 0);

		-- Output data handling
		-----------------------
		output          : out std_logic_vector(DATA_WIDTH - 1 downto 0);
		output_valid    : out std_logic;
		output_last     : out std_logic;
		output_user     : out std_logic;
		output_ready    : in  std_logic
	);
	end component axi4s_fifo_v2;

	component axi4s_buffer_v2 is
		generic (
			DATA_WIDTH : natural
		);
		port (
	
			clk            : in  std_logic;
			rst            : in  std_logic;
	
			-- Input data handling
			----------------------
			input             : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
			input_valid       : in  std_logic;
			input_last        : in  std_logic;
			input_start        : in  std_logic;
			input_user        : in  std_logic;
			input_ready       : out std_logic;
	
	
			-- Output data handling
			-----------------------
			output          : out std_logic_vector(DATA_WIDTH - 1 downto 0);
			output_valid    : out std_logic;
			output_last     : out std_logic;
			output_start     : out std_logic;
			output_user     : out std_logic;
			output_ready    : in  std_logic
		);
		end component axi4s_buffer_v2;

		component sync_fifo is
			generic (
				DISTR_RAM         : boolean := true;
				FIFO_DEPTH        : natural := 128;
				DATA_WIDTH        : natural := 1;
				FULL_THRESHOLD    : natural := 128;
				EMPTY_THRESHOLD   : natural := 1
			);
			port (
	
			clk            : in  std_logic;
			rst            : in  std_logic;
	
			-- Input data handling
			----------------------
			input             : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
			input_valid       : in  std_logic;
			input_ready       : out std_logic;
			input_almost_full : out std_logic;
			input_almost_empty : out std_logic;
			fifo_max_level    : out std_logic_vector(no_bits_natural(FIFO_DEPTH) - 1 downto 0);
			fifo_data_count    : out std_logic_vector(no_bits_natural(FIFO_DEPTH) - 1 downto 0);

			-- Output data handling
			-----------------------
			output          : out std_logic_vector(DATA_WIDTH - 1 downto 0);
			output_valid    : out std_logic;
			output_ready    : in  std_logic
		);
		end component sync_fifo;

end package pkg_components;
