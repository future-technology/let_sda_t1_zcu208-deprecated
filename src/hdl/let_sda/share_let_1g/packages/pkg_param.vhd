--!
--! Copyright (C) 2014 Creonic GmbH
--!
--! @file
--! @brief  RS encoder parameter package.
--! @author Matthias Alles
--! @date   2014/07/08
--!

library ieee;
use ieee.std_logic_1164.all;

library share_let_1g;
use share_let_1g.pkg_support.all;

package pkg_param is

	---------------------------------------------------------------------------
	-- Throughput Parameters
	---------------------------------------------------------------------------

	--!
	--! Number of symbols the decoder can read, process and write per clock cycle.
	--!
	constant GLOBAL_PARALLELISM_INTERFACE : natural := 15;

	--! NUMBER OF USER DATA BYTES
	constant USER_DATA_BYTES : natural := 223;

	---------------------------------------------------------------------------
	-- Code Parameters
	---------------------------------------------------------------------------

	--! Maximum bitwidth of a single Galois-Field element.
	constant GLOBAL_BW_GF_ELEMENT : natural := 8;

	--! Primitive polynomial of the Galois Field.
	constant GLOBAL_PRIMITIVE_POLYNOMIAL : std_logic_vector(GLOBAL_BW_GF_ELEMENT downto 0) := "100011101"; -- 285

	--! Maximum number of symbols the decoder has to correct.
	-- constant GLOBAL_MAX_CORRECTION_SYMB_T : natural := 12;

	-- --! Maximum number of symbols the decoder has to correct. 131 RS
	-- constant GLOBAL_MAX_CORRECTION_SYMB_T_131 : natural := 2;

	-- --! Maximum number of symbols the decoder has to correct.
	-- constant GLOBAL_MAX_PARITY_BITS : natural := 2 * GLOBAL_MAX_CORRECTION_SYMB_T;

	-- constant GLOBAL_BW_MAX_PARITY_BITS : natural := 5;

	-- --! Codeword size in symbols
	-- constant GLOBAL_CW_SIZE : natural := GLOBAL_INFO_SIZE + 2 * GLOBAL_MAX_CORRECTION_SYMB_T;

	-- --! Codeword size in symbols for the 131 rs
	-- constant GLOBAL_CW_SIZE_131 : natural := GLOBAL_INFO_SIZE_131 + 2 * GLOBAL_MAX_CORRECTION_SYMB_T_131;

	-- --! Bitwidth for representing the bits we are able to correct (t).
	-- constant GLOBAL_BW_MAX_CORRECTION_SYMB_T : integer := no_bits_natural(GLOBAL_MAX_CORRECTION_SYMB_T);

	-- --! Bitwidth for representing the bits we are able to correct (t).
	-- constant GLOBAL_BW_MAX_CORRECTION_SYMB_T_131 : integer := no_bits_natural(GLOBAL_MAX_CORRECTION_SYMB_T_131);

	--! Number of interleaver pages, which are used.
	constant GLOBAL_NUM_PAGES : natural := 1;

	--! Number of fames, which are contained in one 3D block.
	constant GLOBAL_NUM_FRAMES_PER_BLOCK : natural := GLOBAL_NUM_PAGES * 255;

	--! The symbol which is added at the beginning of each FSO frame.
	constant GLOBAL_FSO_FRAME_START_SYMBOL : std_logic_vector(24 - 1 downto 0) := "101010101010101010101010";--(others => '1');
end pkg_param;
