--!
--! Copyright (C) 2010 - 2011 Creonic GmbH
--!
--! @file
--! @brief  Support package with useful functions
--! @author Matthias Alles
--! @date   2010/07/14
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package pkg_support is

	--!
	--! Calculate the number of bit '1' in the input
	--!
	function calc_zeros(v_input: std_logic_vector) return unsigned;
	
	--!
	--! Calculate the number of bit '1' in the input
	--!
	function calc_ones(v_input : std_logic_vector) return unsigned;

	--!
	--! Return the log_2 of an natural value, i.e. the number of bits required
	--! to represent this unsigned value.
	--!
	function no_bits_natural(value_in : natural) return natural;

	--!
	--! Return maximum of two input values
	--!
	function max(value_in_a, value_in_b : natural) return natural;

	--!
	--! Return minimum of two input values
	--!
	function min(value_in_a, value_in_b : natural) return natural;

	--! symmetric saturation of a signed value to BW bits
	function saturate_symmetric(value: in signed; BW : in natural) return signed;

	--! saturation of a signed value to BW bits
	function saturate(value: in signed; BW : in natural) return signed;


	--! ceil function for division of quit / divisor
	function ceil(quot : in natural; divisor : in natural) return natural;

	--!
	--! Procedure to simplify scrambling. Inputs are:
	--!  - sreg_polynom: shunts of the shift register, has to be one bit wider than sreg.
	--!  - sreg        : current register content, returns updated register content.
	--!  - swap_flag   : indicate, whether the current bit needs swapping.
	--!
	procedure scramble(sreg_polynom : in    std_logic_vector;
	                   v_sreg       : inout std_logic_vector;
	                   v_swap_flag  : out   std_logic);

	--! Convert std_logic ('0' or '1') to natural (0 or 1)
	function conv_to_nat(input : in std_logic) return natural;

	--! Round half even.
	function round_half_even(input : signed; remaining_bits : natural) return signed;

	--! Round to the nearest, saturate in case of overflow.
	function round_saturate(input : signed; remaining_bits: natural) return signed;

	--!
	--! Round to the nearest with modulo arithmetic,
	--! i.e., we wrap around from the maximum positive value to the minimum
	--! negative value in the case of an overflow.
	--!
	function round_modulo(input : signed; remaining_bits: natural) return signed;

	--! Cut least significant bits.
	function cut_lsb(input : signed; remaining_bits: natural) return signed;

	--! Cut most significant bits.
	function cut_msb(input : signed; remaining_bits: natural) return signed;
end pkg_support;


package body pkg_support is

	function calc_zeros(v_input: std_logic_vector) return unsigned is
		variable v_zeros_cnt  : unsigned(3 downto 0);
	begin
		--vhdl_cover_off
		case v_input(7 downto 0) is
		when "00000000" => v_zeros_cnt := to_unsigned(8 - 0, 4);
		when "00000001" => v_zeros_cnt := to_unsigned(8 - 1, 4);
		when "00000010" => v_zeros_cnt := to_unsigned(8 - 1, 4);
		when "00000011" => v_zeros_cnt := to_unsigned(8 - 2, 4);
		when "00000100" => v_zeros_cnt := to_unsigned(8 - 1, 4);
		when "00000101" => v_zeros_cnt := to_unsigned(8 - 2, 4);
		when "00000110" => v_zeros_cnt := to_unsigned(8 - 2, 4);
		when "00000111" => v_zeros_cnt := to_unsigned(8 - 3, 4);
		when "00001000" => v_zeros_cnt := to_unsigned(8 - 1, 4);
		when "00001001" => v_zeros_cnt := to_unsigned(8 - 2, 4);
		when "00001010" => v_zeros_cnt := to_unsigned(8 - 2, 4);
		when "00001011" => v_zeros_cnt := to_unsigned(8 - 3, 4);
		when "00001100" => v_zeros_cnt := to_unsigned(8 - 2, 4);
		when "00001101" => v_zeros_cnt := to_unsigned(8 - 3, 4);
		when "00001110" => v_zeros_cnt := to_unsigned(8 - 3, 4);
		when "00001111" => v_zeros_cnt := to_unsigned(8 - 4, 4);
		when "00010000" => v_zeros_cnt := to_unsigned(8 - 1, 4);
		when "00010001" => v_zeros_cnt := to_unsigned(8 - 2, 4);
		when "00010010" => v_zeros_cnt := to_unsigned(8 - 2, 4);
		when "00010011" => v_zeros_cnt := to_unsigned(8 - 3, 4);
		when "00010100" => v_zeros_cnt := to_unsigned(8 - 2, 4);
		when "00010101" => v_zeros_cnt := to_unsigned(8 - 3, 4);
		when "00010110" => v_zeros_cnt := to_unsigned(8 - 3, 4);
		when "00010111" => v_zeros_cnt := to_unsigned(8 - 4, 4);
		when "00011000" => v_zeros_cnt := to_unsigned(8 - 2, 4);
		when "00011001" => v_zeros_cnt := to_unsigned(8 - 3, 4);
		when "00011010" => v_zeros_cnt := to_unsigned(8 - 3, 4);
		when "00011011" => v_zeros_cnt := to_unsigned(8 - 4, 4);
		when "00011100" => v_zeros_cnt := to_unsigned(8 - 3, 4);
		when "00011101" => v_zeros_cnt := to_unsigned(8 - 4, 4);
		when "00011110" => v_zeros_cnt := to_unsigned(8 - 4, 4);
		when "00011111" => v_zeros_cnt := to_unsigned(8 - 5, 4);

		when "00100000" => v_zeros_cnt := to_unsigned(8-0-1, 4);
		when "00100001" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "00100010" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "00100011" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "00100100" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "00100101" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "00100110" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "00100111" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "00101000" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "00101001" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "00101010" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "00101011" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "00101100" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "00101101" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "00101110" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "00101111" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "00110000" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "00110001" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "00110010" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "00110011" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "00110100" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "00110101" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "00110110" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "00110111" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "00111000" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "00111001" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "00111010" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "00111011" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "00111100" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "00111101" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "00111110" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "00111111" => v_zeros_cnt := to_unsigned(8-5-1, 4);

		when "01000000" => v_zeros_cnt := to_unsigned(8-0-1, 4);
		when "01000001" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "01000010" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "01000011" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "01000100" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "01000101" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "01000110" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "01000111" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "01001000" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "01001001" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "01001010" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "01001011" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "01001100" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "01001101" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "01001110" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "01001111" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "01010000" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "01010001" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "01010010" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "01010011" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "01010100" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "01010101" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "01010110" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "01010111" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "01011000" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "01011001" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "01011010" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "01011011" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "01011100" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "01011101" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "01011110" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "01011111" => v_zeros_cnt := to_unsigned(8-5-1, 4);

		when "01100000" => v_zeros_cnt := to_unsigned(8-0-2, 4);
		when "01100001" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "01100010" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "01100011" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "01100100" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "01100101" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "01100110" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "01100111" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "01101000" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "01101001" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "01101010" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "01101011" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "01101100" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "01101101" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "01101110" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "01101111" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "01110000" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "01110001" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "01110010" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "01110011" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "01110100" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "01110101" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "01110110" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "01110111" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "01111000" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "01111001" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "01111010" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "01111011" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "01111100" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "01111101" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "01111110" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "01111111" => v_zeros_cnt := to_unsigned(8-5-2, 4);

		when "10000000" => v_zeros_cnt := to_unsigned(8-0-1, 4);
		when "10000001" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "10000010" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "10000011" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "10000100" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "10000101" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "10000110" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "10000111" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "10001000" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "10001001" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "10001010" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "10001011" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "10001100" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "10001101" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "10001110" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "10001111" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "10010000" => v_zeros_cnt := to_unsigned(8-1-1, 4);
		when "10010001" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "10010010" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "10010011" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "10010100" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "10010101" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "10010110" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "10010111" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "10011000" => v_zeros_cnt := to_unsigned(8-2-1, 4);
		when "10011001" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "10011010" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "10011011" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "10011100" => v_zeros_cnt := to_unsigned(8-3-1, 4);
		when "10011101" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "10011110" => v_zeros_cnt := to_unsigned(8-4-1, 4);
		when "10011111" => v_zeros_cnt := to_unsigned(8-5-1, 4);

		when "10100000" => v_zeros_cnt := to_unsigned(8-0-2, 4);
		when "10100001" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "10100010" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "10100011" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "10100100" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "10100101" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "10100110" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "10100111" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "10101000" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "10101001" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "10101010" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "10101011" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "10101100" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "10101101" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "10101110" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "10101111" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "10110000" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "10110001" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "10110010" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "10110011" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "10110100" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "10110101" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "10110110" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "10110111" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "10111000" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "10111001" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "10111010" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "10111011" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "10111100" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "10111101" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "10111110" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "10111111" => v_zeros_cnt := to_unsigned(8-5-2, 4);

		when "11000000" => v_zeros_cnt := to_unsigned(8-0-2, 4);
		when "11000001" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "11000010" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "11000011" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "11000100" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "11000101" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "11000110" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "11000111" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "11001000" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "11001001" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "11001010" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "11001011" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "11001100" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "11001101" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "11001110" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "11001111" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "11010000" => v_zeros_cnt := to_unsigned(8-1-2, 4);
		when "11010001" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "11010010" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "11010011" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "11010100" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "11010101" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "11010110" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "11010111" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "11011000" => v_zeros_cnt := to_unsigned(8-2-2, 4);
		when "11011001" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "11011010" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "11011011" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "11011100" => v_zeros_cnt := to_unsigned(8-3-2, 4);
		when "11011101" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "11011110" => v_zeros_cnt := to_unsigned(8-4-2, 4);
		when "11011111" => v_zeros_cnt := to_unsigned(8-5-2, 4);

		when "11100000" => v_zeros_cnt := to_unsigned(8-0-3, 4);
		when "11100001" => v_zeros_cnt := to_unsigned(8-1-3, 4);
		when "11100010" => v_zeros_cnt := to_unsigned(8-1-3, 4);
		when "11100011" => v_zeros_cnt := to_unsigned(8-2-3, 4);
		when "11100100" => v_zeros_cnt := to_unsigned(8-1-3, 4);
		when "11100101" => v_zeros_cnt := to_unsigned(8-2-3, 4);
		when "11100110" => v_zeros_cnt := to_unsigned(8-2-3, 4);
		when "11100111" => v_zeros_cnt := to_unsigned(8-3-3, 4);
		when "11101000" => v_zeros_cnt := to_unsigned(8-1-3, 4);
		when "11101001" => v_zeros_cnt := to_unsigned(8-2-3, 4);
		when "11101010" => v_zeros_cnt := to_unsigned(8-2-3, 4);
		when "11101011" => v_zeros_cnt := to_unsigned(8-3-3, 4);
		when "11101100" => v_zeros_cnt := to_unsigned(8-2-3, 4);
		when "11101101" => v_zeros_cnt := to_unsigned(8-3-3, 4);
		when "11101110" => v_zeros_cnt := to_unsigned(8-3-3, 4);
		when "11101111" => v_zeros_cnt := to_unsigned(8-4-3, 4);
		when "11110000" => v_zeros_cnt := to_unsigned(8-1-3, 4);
		when "11110001" => v_zeros_cnt := to_unsigned(8-2-3, 4);
		when "11110010" => v_zeros_cnt := to_unsigned(8-2-3, 4);
		when "11110011" => v_zeros_cnt := to_unsigned(8-3-3, 4);
		when "11110100" => v_zeros_cnt := to_unsigned(8-2-3, 4);
		when "11110101" => v_zeros_cnt := to_unsigned(8-3-3, 4);
		when "11110110" => v_zeros_cnt := to_unsigned(8-3-3, 4);
		when "11110111" => v_zeros_cnt := to_unsigned(8-4-3, 4);
		when "11111000" => v_zeros_cnt := to_unsigned(8-2-3, 4);
		when "11111001" => v_zeros_cnt := to_unsigned(8-3-3, 4);
		when "11111010" => v_zeros_cnt := to_unsigned(8-3-3, 4);
		when "11111011" => v_zeros_cnt := to_unsigned(8-4-3, 4);
		when "11111100" => v_zeros_cnt := to_unsigned(8-3-3, 4);
		when "11111101" => v_zeros_cnt := to_unsigned(8-4-3, 4);
		when "11111110" => v_zeros_cnt := to_unsigned(8-4-3, 4);
		when others     => v_zeros_cnt := to_unsigned(0, 4);
		end case;
		--vhdl_cover_on
		return v_zeros_cnt;
	end function calc_zeros;

	function calc_ones(v_input: std_logic_vector) return unsigned is
		variable v_ones_cnt  : unsigned(3 downto 0);
		-- variable v_input_int : std_logic_vector(7 downto 0);
	begin
		-- v_input_int := v_input(7 downto 0);
		case v_input(7 downto 0) is
		when "00000000" => v_ones_cnt := to_unsigned(0, 4);
		when "00000001" => v_ones_cnt := to_unsigned(1, 4);
		when "00000010" => v_ones_cnt := to_unsigned(1, 4);
		when "00000011" => v_ones_cnt := to_unsigned(2, 4);
		when "00000100" => v_ones_cnt := to_unsigned(1, 4);
		when "00000101" => v_ones_cnt := to_unsigned(2, 4);
		when "00000110" => v_ones_cnt := to_unsigned(2, 4);
		when "00000111" => v_ones_cnt := to_unsigned(3, 4);
		when "00001000" => v_ones_cnt := to_unsigned(1, 4);
		when "00001001" => v_ones_cnt := to_unsigned(2, 4);
		when "00001010" => v_ones_cnt := to_unsigned(2, 4);
		when "00001011" => v_ones_cnt := to_unsigned(3, 4);
		when "00001100" => v_ones_cnt := to_unsigned(2, 4);
		when "00001101" => v_ones_cnt := to_unsigned(3, 4);
		when "00001110" => v_ones_cnt := to_unsigned(3, 4);
		when "00001111" => v_ones_cnt := to_unsigned(4, 4);
		when "00010000" => v_ones_cnt := to_unsigned(1, 4);
		when "00010001" => v_ones_cnt := to_unsigned(2, 4);
		when "00010010" => v_ones_cnt := to_unsigned(2, 4);
		when "00010011" => v_ones_cnt := to_unsigned(3, 4);
		when "00010100" => v_ones_cnt := to_unsigned(2, 4);
		when "00010101" => v_ones_cnt := to_unsigned(3, 4);
		when "00010110" => v_ones_cnt := to_unsigned(3, 4);
		when "00010111" => v_ones_cnt := to_unsigned(4, 4);
		when "00011000" => v_ones_cnt := to_unsigned(2, 4);
		when "00011001" => v_ones_cnt := to_unsigned(3, 4);
		when "00011010" => v_ones_cnt := to_unsigned(3, 4);
		when "00011011" => v_ones_cnt := to_unsigned(4, 4);
		when "00011100" => v_ones_cnt := to_unsigned(3, 4);
		when "00011101" => v_ones_cnt := to_unsigned(4, 4);
		when "00011110" => v_ones_cnt := to_unsigned(4, 4);
		when "00011111" => v_ones_cnt := to_unsigned(5, 4);

		when "00100000" => v_ones_cnt := to_unsigned(0+1, 4);
		when "00100001" => v_ones_cnt := to_unsigned(1+1, 4);
		when "00100010" => v_ones_cnt := to_unsigned(1+1, 4);
		when "00100011" => v_ones_cnt := to_unsigned(2+1, 4);
		when "00100100" => v_ones_cnt := to_unsigned(1+1, 4);
		when "00100101" => v_ones_cnt := to_unsigned(2+1, 4);
		when "00100110" => v_ones_cnt := to_unsigned(2+1, 4);
		when "00100111" => v_ones_cnt := to_unsigned(3+1, 4);
		when "00101000" => v_ones_cnt := to_unsigned(1+1, 4);
		when "00101001" => v_ones_cnt := to_unsigned(2+1, 4);
		when "00101010" => v_ones_cnt := to_unsigned(2+1, 4);
		when "00101011" => v_ones_cnt := to_unsigned(3+1, 4);
		when "00101100" => v_ones_cnt := to_unsigned(2+1, 4);
		when "00101101" => v_ones_cnt := to_unsigned(3+1, 4);
		when "00101110" => v_ones_cnt := to_unsigned(3+1, 4);
		when "00101111" => v_ones_cnt := to_unsigned(4+1, 4);
		when "00110000" => v_ones_cnt := to_unsigned(1+1, 4);
		when "00110001" => v_ones_cnt := to_unsigned(2+1, 4);
		when "00110010" => v_ones_cnt := to_unsigned(2+1, 4);
		when "00110011" => v_ones_cnt := to_unsigned(3+1, 4);
		when "00110100" => v_ones_cnt := to_unsigned(2+1, 4);
		when "00110101" => v_ones_cnt := to_unsigned(3+1, 4);
		when "00110110" => v_ones_cnt := to_unsigned(3+1, 4);
		when "00110111" => v_ones_cnt := to_unsigned(4+1, 4);
		when "00111000" => v_ones_cnt := to_unsigned(2+1, 4);
		when "00111001" => v_ones_cnt := to_unsigned(3+1, 4);
		when "00111010" => v_ones_cnt := to_unsigned(3+1, 4);
		when "00111011" => v_ones_cnt := to_unsigned(4+1, 4);
		when "00111100" => v_ones_cnt := to_unsigned(3+1, 4);
		when "00111101" => v_ones_cnt := to_unsigned(4+1, 4);
		when "00111110" => v_ones_cnt := to_unsigned(4+1, 4);
		when "00111111" => v_ones_cnt := to_unsigned(5+1, 4);

		when "01000000" => v_ones_cnt := to_unsigned(0+1, 4);
		when "01000001" => v_ones_cnt := to_unsigned(1+1, 4);
		when "01000010" => v_ones_cnt := to_unsigned(1+1, 4);
		when "01000011" => v_ones_cnt := to_unsigned(2+1, 4);
		when "01000100" => v_ones_cnt := to_unsigned(1+1, 4);
		when "01000101" => v_ones_cnt := to_unsigned(2+1, 4);
		when "01000110" => v_ones_cnt := to_unsigned(2+1, 4);
		when "01000111" => v_ones_cnt := to_unsigned(3+1, 4);
		when "01001000" => v_ones_cnt := to_unsigned(1+1, 4);
		when "01001001" => v_ones_cnt := to_unsigned(2+1, 4);
		when "01001010" => v_ones_cnt := to_unsigned(2+1, 4);
		when "01001011" => v_ones_cnt := to_unsigned(3+1, 4);
		when "01001100" => v_ones_cnt := to_unsigned(2+1, 4);
		when "01001101" => v_ones_cnt := to_unsigned(3+1, 4);
		when "01001110" => v_ones_cnt := to_unsigned(3+1, 4);
		when "01001111" => v_ones_cnt := to_unsigned(4+1, 4);
		when "01010000" => v_ones_cnt := to_unsigned(1+1, 4);
		when "01010001" => v_ones_cnt := to_unsigned(2+1, 4);
		when "01010010" => v_ones_cnt := to_unsigned(2+1, 4);
		when "01010011" => v_ones_cnt := to_unsigned(3+1, 4);
		when "01010100" => v_ones_cnt := to_unsigned(2+1, 4);
		when "01010101" => v_ones_cnt := to_unsigned(3+1, 4);
		when "01010110" => v_ones_cnt := to_unsigned(3+1, 4);
		when "01010111" => v_ones_cnt := to_unsigned(4+1, 4);
		when "01011000" => v_ones_cnt := to_unsigned(2+1, 4);
		when "01011001" => v_ones_cnt := to_unsigned(3+1, 4);
		when "01011010" => v_ones_cnt := to_unsigned(3+1, 4);
		when "01011011" => v_ones_cnt := to_unsigned(4+1, 4);
		when "01011100" => v_ones_cnt := to_unsigned(3+1, 4);
		when "01011101" => v_ones_cnt := to_unsigned(4+1, 4);
		when "01011110" => v_ones_cnt := to_unsigned(4+1, 4);
		when "01011111" => v_ones_cnt := to_unsigned(5+1, 4);

		when "01100000" => v_ones_cnt := to_unsigned(0+2, 4);
		when "01100001" => v_ones_cnt := to_unsigned(1+2, 4);
		when "01100010" => v_ones_cnt := to_unsigned(1+2, 4);
		when "01100011" => v_ones_cnt := to_unsigned(2+2, 4);
		when "01100100" => v_ones_cnt := to_unsigned(1+2, 4);
		when "01100101" => v_ones_cnt := to_unsigned(2+2, 4);
		when "01100110" => v_ones_cnt := to_unsigned(2+2, 4);
		when "01100111" => v_ones_cnt := to_unsigned(3+2, 4);
		when "01101000" => v_ones_cnt := to_unsigned(1+2, 4);
		when "01101001" => v_ones_cnt := to_unsigned(2+2, 4);
		when "01101010" => v_ones_cnt := to_unsigned(2+2, 4);
		when "01101011" => v_ones_cnt := to_unsigned(3+2, 4);
		when "01101100" => v_ones_cnt := to_unsigned(2+2, 4);
		when "01101101" => v_ones_cnt := to_unsigned(3+2, 4);
		when "01101110" => v_ones_cnt := to_unsigned(3+2, 4);
		when "01101111" => v_ones_cnt := to_unsigned(4+2, 4);
		when "01110000" => v_ones_cnt := to_unsigned(1+2, 4);
		when "01110001" => v_ones_cnt := to_unsigned(2+2, 4);
		when "01110010" => v_ones_cnt := to_unsigned(2+2, 4);
		when "01110011" => v_ones_cnt := to_unsigned(3+2, 4);
		when "01110100" => v_ones_cnt := to_unsigned(2+2, 4);
		when "01110101" => v_ones_cnt := to_unsigned(3+2, 4);
		when "01110110" => v_ones_cnt := to_unsigned(3+2, 4);
		when "01110111" => v_ones_cnt := to_unsigned(4+2, 4);
		when "01111000" => v_ones_cnt := to_unsigned(2+2, 4);
		when "01111001" => v_ones_cnt := to_unsigned(3+2, 4);
		when "01111010" => v_ones_cnt := to_unsigned(3+2, 4);
		when "01111011" => v_ones_cnt := to_unsigned(4+2, 4);
		when "01111100" => v_ones_cnt := to_unsigned(3+2, 4);
		when "01111101" => v_ones_cnt := to_unsigned(4+2, 4);
		when "01111110" => v_ones_cnt := to_unsigned(4+2, 4);
		when "01111111" => v_ones_cnt := to_unsigned(5+2, 4);

		when "10000000" => v_ones_cnt := to_unsigned(0+1, 4);
		when "10000001" => v_ones_cnt := to_unsigned(1+1, 4);
		when "10000010" => v_ones_cnt := to_unsigned(1+1, 4);
		when "10000011" => v_ones_cnt := to_unsigned(2+1, 4);
		when "10000100" => v_ones_cnt := to_unsigned(1+1, 4);
		when "10000101" => v_ones_cnt := to_unsigned(2+1, 4);
		when "10000110" => v_ones_cnt := to_unsigned(2+1, 4);
		when "10000111" => v_ones_cnt := to_unsigned(3+1, 4);
		when "10001000" => v_ones_cnt := to_unsigned(1+1, 4);
		when "10001001" => v_ones_cnt := to_unsigned(2+1, 4);
		when "10001010" => v_ones_cnt := to_unsigned(2+1, 4);
		when "10001011" => v_ones_cnt := to_unsigned(3+1, 4);
		when "10001100" => v_ones_cnt := to_unsigned(2+1, 4);
		when "10001101" => v_ones_cnt := to_unsigned(3+1, 4);
		when "10001110" => v_ones_cnt := to_unsigned(3+1, 4);
		when "10001111" => v_ones_cnt := to_unsigned(4+1, 4);
		when "10010000" => v_ones_cnt := to_unsigned(1+1, 4);
		when "10010001" => v_ones_cnt := to_unsigned(2+1, 4);
		when "10010010" => v_ones_cnt := to_unsigned(2+1, 4);
		when "10010011" => v_ones_cnt := to_unsigned(3+1, 4);
		when "10010100" => v_ones_cnt := to_unsigned(2+1, 4);
		when "10010101" => v_ones_cnt := to_unsigned(3+1, 4);
		when "10010110" => v_ones_cnt := to_unsigned(3+1, 4);
		when "10010111" => v_ones_cnt := to_unsigned(4+1, 4);
		when "10011000" => v_ones_cnt := to_unsigned(2+1, 4);
		when "10011001" => v_ones_cnt := to_unsigned(3+1, 4);
		when "10011010" => v_ones_cnt := to_unsigned(3+1, 4);
		when "10011011" => v_ones_cnt := to_unsigned(4+1, 4);
		when "10011100" => v_ones_cnt := to_unsigned(3+1, 4);
		when "10011101" => v_ones_cnt := to_unsigned(4+1, 4);
		when "10011110" => v_ones_cnt := to_unsigned(4+1, 4);
		when "10011111" => v_ones_cnt := to_unsigned(5+1, 4);

		when "10100000" => v_ones_cnt := to_unsigned(0+2, 4);
		when "10100001" => v_ones_cnt := to_unsigned(1+2, 4);
		when "10100010" => v_ones_cnt := to_unsigned(1+2, 4);
		when "10100011" => v_ones_cnt := to_unsigned(2+2, 4);
		when "10100100" => v_ones_cnt := to_unsigned(1+2, 4);
		when "10100101" => v_ones_cnt := to_unsigned(2+2, 4);
		when "10100110" => v_ones_cnt := to_unsigned(2+2, 4);
		when "10100111" => v_ones_cnt := to_unsigned(3+2, 4);
		when "10101000" => v_ones_cnt := to_unsigned(1+2, 4);
		when "10101001" => v_ones_cnt := to_unsigned(2+2, 4);
		when "10101010" => v_ones_cnt := to_unsigned(2+2, 4);
		when "10101011" => v_ones_cnt := to_unsigned(3+2, 4);
		when "10101100" => v_ones_cnt := to_unsigned(2+2, 4);
		when "10101101" => v_ones_cnt := to_unsigned(3+2, 4);
		when "10101110" => v_ones_cnt := to_unsigned(3+2, 4);
		when "10101111" => v_ones_cnt := to_unsigned(4+2, 4);
		when "10110000" => v_ones_cnt := to_unsigned(1+2, 4);
		when "10110001" => v_ones_cnt := to_unsigned(2+2, 4);
		when "10110010" => v_ones_cnt := to_unsigned(2+2, 4);
		when "10110011" => v_ones_cnt := to_unsigned(3+2, 4);
		when "10110100" => v_ones_cnt := to_unsigned(2+2, 4);
		when "10110101" => v_ones_cnt := to_unsigned(3+2, 4);
		when "10110110" => v_ones_cnt := to_unsigned(3+2, 4);
		when "10110111" => v_ones_cnt := to_unsigned(4+2, 4);
		when "10111000" => v_ones_cnt := to_unsigned(2+2, 4);
		when "10111001" => v_ones_cnt := to_unsigned(3+2, 4);
		when "10111010" => v_ones_cnt := to_unsigned(3+2, 4);
		when "10111011" => v_ones_cnt := to_unsigned(4+2, 4);
		when "10111100" => v_ones_cnt := to_unsigned(3+2, 4);
		when "10111101" => v_ones_cnt := to_unsigned(4+2, 4);
		when "10111110" => v_ones_cnt := to_unsigned(4+2, 4);
		when "10111111" => v_ones_cnt := to_unsigned(5+2, 4);

		when "11000000" => v_ones_cnt := to_unsigned(0+2, 4);
		when "11000001" => v_ones_cnt := to_unsigned(1+2, 4);
		when "11000010" => v_ones_cnt := to_unsigned(1+2, 4);
		when "11000011" => v_ones_cnt := to_unsigned(2+2, 4);
		when "11000100" => v_ones_cnt := to_unsigned(1+2, 4);
		when "11000101" => v_ones_cnt := to_unsigned(2+2, 4);
		when "11000110" => v_ones_cnt := to_unsigned(2+2, 4);
		when "11000111" => v_ones_cnt := to_unsigned(3+2, 4);
		when "11001000" => v_ones_cnt := to_unsigned(1+2, 4);
		when "11001001" => v_ones_cnt := to_unsigned(2+2, 4);
		when "11001010" => v_ones_cnt := to_unsigned(2+2, 4);
		when "11001011" => v_ones_cnt := to_unsigned(3+2, 4);
		when "11001100" => v_ones_cnt := to_unsigned(2+2, 4);
		when "11001101" => v_ones_cnt := to_unsigned(3+2, 4);
		when "11001110" => v_ones_cnt := to_unsigned(3+2, 4);
		when "11001111" => v_ones_cnt := to_unsigned(4+2, 4);
		when "11010000" => v_ones_cnt := to_unsigned(1+2, 4);
		when "11010001" => v_ones_cnt := to_unsigned(2+2, 4);
		when "11010010" => v_ones_cnt := to_unsigned(2+2, 4);
		when "11010011" => v_ones_cnt := to_unsigned(3+2, 4);
		when "11010100" => v_ones_cnt := to_unsigned(2+2, 4);
		when "11010101" => v_ones_cnt := to_unsigned(3+2, 4);
		when "11010110" => v_ones_cnt := to_unsigned(3+2, 4);
		when "11010111" => v_ones_cnt := to_unsigned(4+2, 4);
		when "11011000" => v_ones_cnt := to_unsigned(2+2, 4);
		when "11011001" => v_ones_cnt := to_unsigned(3+2, 4);
		when "11011010" => v_ones_cnt := to_unsigned(3+2, 4);
		when "11011011" => v_ones_cnt := to_unsigned(4+2, 4);
		when "11011100" => v_ones_cnt := to_unsigned(3+2, 4);
		when "11011101" => v_ones_cnt := to_unsigned(4+2, 4);
		when "11011110" => v_ones_cnt := to_unsigned(4+2, 4);
		when "11011111" => v_ones_cnt := to_unsigned(5+2, 4);

		when "11100000" => v_ones_cnt := to_unsigned(0+3, 4);
		when "11100001" => v_ones_cnt := to_unsigned(1+3, 4);
		when "11100010" => v_ones_cnt := to_unsigned(1+3, 4);
		when "11100011" => v_ones_cnt := to_unsigned(2+3, 4);
		when "11100100" => v_ones_cnt := to_unsigned(1+3, 4);
		when "11100101" => v_ones_cnt := to_unsigned(2+3, 4);
		when "11100110" => v_ones_cnt := to_unsigned(2+3, 4);
		when "11100111" => v_ones_cnt := to_unsigned(3+3, 4);
		when "11101000" => v_ones_cnt := to_unsigned(1+3, 4);
		when "11101001" => v_ones_cnt := to_unsigned(2+3, 4);
		when "11101010" => v_ones_cnt := to_unsigned(2+3, 4);
		when "11101011" => v_ones_cnt := to_unsigned(3+3, 4);
		when "11101100" => v_ones_cnt := to_unsigned(2+3, 4);
		when "11101101" => v_ones_cnt := to_unsigned(3+3, 4);
		when "11101110" => v_ones_cnt := to_unsigned(3+3, 4);
		when "11101111" => v_ones_cnt := to_unsigned(4+3, 4);
		when "11110000" => v_ones_cnt := to_unsigned(1+3, 4);
		when "11110001" => v_ones_cnt := to_unsigned(2+3, 4);
		when "11110010" => v_ones_cnt := to_unsigned(2+3, 4);
		when "11110011" => v_ones_cnt := to_unsigned(3+3, 4);
		when "11110100" => v_ones_cnt := to_unsigned(2+3, 4);
		when "11110101" => v_ones_cnt := to_unsigned(3+3, 4);
		when "11110110" => v_ones_cnt := to_unsigned(3+3, 4);
		when "11110111" => v_ones_cnt := to_unsigned(4+3, 4);
		when "11111000" => v_ones_cnt := to_unsigned(2+3, 4);
		when "11111001" => v_ones_cnt := to_unsigned(3+3, 4);
		when "11111010" => v_ones_cnt := to_unsigned(3+3, 4);
		when "11111011" => v_ones_cnt := to_unsigned(4+3, 4);
		when "11111100" => v_ones_cnt := to_unsigned(3+3, 4);
		when "11111101" => v_ones_cnt := to_unsigned(4+3, 4);
		when "11111110" => v_ones_cnt := to_unsigned(4+3, 4);
		when "11111111" => v_ones_cnt := to_unsigned(5+3, 4);
		when others     => v_ones_cnt := to_unsigned(0, 4);
		end case;
		return v_ones_cnt;
	end function calc_ones;

	function no_bits_natural(value_in: natural) return natural is
		variable v_n_bit : unsigned(31 downto 0);
	begin
		if value_in = 0 then
			return 0;
		end if;
		v_n_bit := to_unsigned(value_in, 32);
		for i in 31 downto 0 loop
			if v_n_bit(i) = '1' then
				return i + 1;
			end if;
		end loop;
		return 1;
	end no_bits_natural;


	function max(value_in_a, value_in_b : natural) return natural is
	begin
		if value_in_a > value_in_b then
			return value_in_a;
		else
			return value_in_b;
		end if;
	end function;

	function min(value_in_a, value_in_b : natural) return natural is
	begin
		if value_in_a < value_in_b then
			return value_in_a;
		else
			return value_in_b;
		end if;
	end function;

	-- saturation of a signed value to BW bits, symmetric saturation
	function saturate_symmetric(value: in signed; BW : in natural) return signed is
	begin
		if value > 2 ** (BW - 1) - 1 then
			return to_signed(2 ** (BW - 1) - 1, BW);
		elsif value < -2 ** (BW - 1) + 1 then
			-- +1 because of signed magnitude
			return to_signed(-2 ** (BW - 1) + 1, BW);
		else
			return value(BW - 1 downto 0);
		end if;
	end function saturate_symmetric;

	-- saturation of a signed value to BW bits
	function saturate(value: in signed; BW : in natural) return signed is
	begin
		if value > 2 ** (BW - 1) - 1 then
			return to_signed(2 ** (BW - 1) - 1, BW);
		elsif value < -2 ** (BW - 1) then
			return to_signed(-2 ** (BW - 1), BW);
		else
			return value(BW - 1 downto 0);
		end if;
	end function saturate;


	function ceil(quot : in natural; divisor : in natural) return natural is
	begin
		if (quot + divisor - 1) / divisor > quot / divisor then
			return quot / divisor + 1;
		else
			return quot / divisor;
		end if;
	end function;


	procedure scramble(sreg_polynom : in    std_logic_vector;
	                   v_sreg       : inout std_logic_vector;
	                   v_swap_flag  : out   std_logic) is
		variable v_bit_swap : std_logic;
	begin
		v_bit_swap := '0';
		for i in 0 to v_sreg'length - 1 loop
			if v_sreg(i) = '1' and sreg_polynom(i + 1) = '1' then
				v_bit_swap := not v_bit_swap;
			end if;
		end loop;
		v_sreg      := v_sreg(v_sreg'length - 2 downto 0) & v_bit_swap;
		v_swap_flag := v_bit_swap;
	end procedure scramble;


	function conv_to_nat(input : in std_logic) return natural is
	begin
		if input = '0' then
			return 0;
		else
			return 1;
		end if;
	end function;


	function round_half_even(input : signed; remaining_bits: natural) return signed is
		constant BW_IN     : integer := input'length;
		constant BW_OUT    : integer := remaining_bits;
		constant FRACT_POS : integer := BW_IN - BW_OUT;

		variable v_result : signed(BW_OUT - 1 downto 0);
	begin
		v_result := input(BW_IN - 1 downto FRACT_POS);

		if input(FRACT_POS - 1) = '1' then
			if input(FRACT_POS - 2 downto 0) = 0 then
				if input(FRACT_POS) = '1' then
					v_result := v_result + 1;
				end if;
			else
				v_result := v_result + 1;
			end if;
		end if;

		return v_result;
	end function round_half_even;


	function round_saturate(input : signed; remaining_bits: natural) return signed is
		constant BW_IN     : integer := input'length;
		constant BW_OUT    : integer := remaining_bits;
		constant FRACT_POS : integer := BW_IN - BW_OUT;

		variable v_sum : signed(BW_IN downto 0);
		variable v_result : signed(BW_OUT - 1 downto 0);
		variable v_overflow : std_logic;
	begin

		-- First add "0.5" to the input value.
		v_sum := resize(input, BW_IN + 1) + 2**(FRACT_POS - 1);

		--
		-- Then cut and saturate.
		-- Note that the value can only flow over the maximum positive value,
		-- because we add 0.5.
		-- In the case of an overflow, the two MSB of v_sum are "01" and the
		-- result value is set to the maximum allowed positive number "0111..1".
		--
		v_overflow := (not v_sum(BW_IN)) and v_sum(BW_IN - 1);
		v_result(BW_OUT - 1) := v_sum(BW_IN - 1) xor v_overflow;
		if v_overflow = '1' then
			v_result(BW_OUT - 2 downto 0) := (others => '1');
		else
			v_result(BW_OUT - 2 downto 0) := v_sum(BW_IN - 2 downto FRACT_POS);
		end if;

		return v_result;
	end function round_saturate;


	function round_modulo(input : signed; remaining_bits: natural) return signed is
		constant BW_IN     : integer := input'length;
		constant BW_OUT    : integer := remaining_bits;
		constant FRACT_POS : integer := BW_IN - BW_OUT;

		variable v_sum : signed(BW_IN - 1 downto 0);
		variable v_result : signed(BW_OUT - 1 downto 0);
	begin

		-- First add "0.5" to the input value, modulo arithmetic (= no extension).
		v_sum := input + 2**(FRACT_POS - 1);

		-- Then cut LSBs.
		v_result(BW_OUT - 1 downto 0) := v_sum(BW_IN - 1 downto FRACT_POS);

		return v_result;
	end function round_modulo;


	function cut_lsb(input : signed; remaining_bits: natural) return signed is
		constant BW_IN       : integer := input'length;
		constant NEW_LSB_POS : integer := BW_IN - remaining_bits;
	begin
		return input(BW_IN - 1 downto NEW_LSB_POS);
	end function cut_lsb;


	function cut_msb(input : signed; remaining_bits: natural) return signed is
		constant BW_OUT    : integer := remaining_bits;
	begin
		return input(BW_OUT - 1 downto 0);
	end function cut_msb;

end pkg_support;
