--!
--! Copyright (C)  2020 Creonic GmbH
--!
--! @file
--! @brief  Package defining some usefull shared types.
--! @author Markus Fehrenz/Nhan Nguyen
--! @date   2020/03/02
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package pkg_types is

	constant LET_ETH_FRAME_LENGTH_BYTE : natural := 223;
	constant FEC_FRAME_LENGTH_BYTE : natural := 255;
	constant FSO_MAC_FRAME_LENGTH_BYTE : natural := 255 + 3 + 1;

	type t_byte_array is array (natural range <>) of std_logic_vector(7 downto 0);
	subtype t_let_frame is t_byte_array(LET_ETH_FRAME_LENGTH_BYTE - 1 downto 0);
	subtype t_fec_frame is t_byte_array(FEC_FRAME_LENGTH_BYTE - 1 downto 0);
	subtype t_fso_frame is t_byte_array(FSO_MAC_FRAME_LENGTH_BYTE - 1 downto 0);
	subtype t_fso_frame_ext is t_byte_array(268 - 1 downto 0);
	subtype t_eight_bytes is t_byte_array(7 downto 0);

	type t_cnt_array is array (natural range <>) of unsigned(3 downto 0);
	subtype t_eight_cnt is t_cnt_array(7 downto 0);

	-- This procedure shifts the byte array by a given number of values to the right.
	function shift_right_byte_array (byte_array : t_byte_array; shift_value : natural) return t_byte_array;
	function crc32(input: std_logic_vector(7 downto 0); crc:  std_logic_vector(31 downto 0)) return std_logic_vector;

	type trec_stat_let_deframing is record
		pkt_start_cnt  : unsigned(32 - 1 downto 0);
		pkt_end_cnt    : unsigned(32 - 1 downto 0);
		min_pkt_length : unsigned(8 - 1 downto 0);
		pkt_violation  : std_logic;
	end record;

	type trec_stat_let_collector is record
		total_frames          : unsigned(47 downto 0);
		total_error_frames    : unsigned(47 downto 0);
	end record;

	type trec_stat_fso_mac_tx_dc_fifo is record
		num_output_frames : unsigned(48 - 1 downto 0);
	end record;

	type trec_stat_fso_mac_tx_bw_changing is record
		num_input_frames  : unsigned(48 - 1 downto 0);
		num_output_bytes  : unsigned(48 - 1 downto 0);
		num_dummy_frames  : unsigned(48 - 1 downto 0);
	end record;
	constant RESET_STAT_FSO_MAC_TX_BW_CHANGING : trec_stat_fso_mac_tx_bw_changing := (others => (others => '0'));

	type trec_stat_fso_mac_tx is record
		dc_fifo      : trec_stat_fso_mac_tx_dc_fifo;
		bw_changing  : trec_stat_fso_mac_tx_bw_changing;
	end record;

	type trec_assert_fso_mac_tx_bw_changing is record
		input_violation : std_logic;
	end record;

	type trec_assert_fso_mac_tx is record
		bw_changing : trec_assert_fso_mac_tx_bw_changing;
	end record;

	-- FSO Mac RX

	type trec_stat_fso_mac_rx_byte_sync is record
		sync_locked      : std_logic;
		aquisition_state : std_logic;
		bit_slip_state   : std_logic;
		data_cnt        : unsigned(32 - 1 downto 0);
		slip_bit_cnt    : unsigned(32 - 1 downto 0);
		fail_cnt        : unsigned(32 - 1 downto 0);
		num_bits_sliped : unsigned(48 - 1 downto 0);
		num_sync_losses : unsigned(48 - 1 downto 0);
	end record;

	type trec_stat_fso_mac_rx_frame_collector is record
		num_input_bytes  : unsigned(48 - 1 downto 0);
		num_dummy_frames : unsigned(48 - 1 downto 0);
		num_frames       : unsigned(48 - 1 downto 0);
		num_bytes_sliped : unsigned(48 - 1 downto 0);
		num_invalid      : unsigned(32 - 1 downto 0);
	end record;
	constant RESET_STAT_FSO_MAC_RX_FRAME_COLLECTOR : trec_stat_fso_mac_rx_frame_collector := (others => (others => '0'));

	type trec_stat_fso_mac_rx_header_sync is record
		sync_locked : std_logic;
		num_bytes_sliped : unsigned(48 - 1 downto 0);
		num_sync_losses  : unsigned(48 - 1 downto 0);
		num_searches     : unsigned(48 - 1 downto 0);
		num_in_frames    : unsigned(48 - 1 downto 0);
		num_out_frames   : unsigned(48 - 1 downto 0);
	end record;
	constant RESET_STAT_FSO_MAC_RX_HEADER_SYNC : trec_stat_fso_mac_rx_header_sync := (sync_locked => '0', others => (others => '0'));

	type trec_stat_fso_mac_rx_block_sync is record
		sync_locked         : std_logic;
		num_aquisitions     : unsigned(48 - 1 downto 0);
		num_sync_losses     : unsigned(48 - 1 downto 0);
		num_sync_recoveries : unsigned(48 - 1 downto 0);
	end record;
	constant RESET_STAT_FSO_MAC_RX_BLOCK_SYNC : trec_stat_fso_mac_rx_block_sync := (sync_locked => '0', others => (others => '0'));

	type trec_stat_fso_mac_rx_top is record
		status                  : std_logic_vector(31 downto 0);
		dc_fifo_input_frames    : unsigned(48 - 1 downto 0);
		deframing_output_frames : unsigned(48 - 1 downto 0);
	end record;

	type trec_stat_fso_mac_rx is record
		byte_sync       : trec_stat_fso_mac_rx_byte_sync;
		frame_collector : trec_stat_fso_mac_rx_frame_collector;
		header_sync     : trec_stat_fso_mac_rx_header_sync;
		block_sync      : trec_stat_fso_mac_rx_block_sync;
		rx_top          : trec_stat_fso_mac_rx_top;
	end record;

	type trec_cfg_fso_mac_rx_block_sync is record
		aquisition_length  : unsigned(32 - 1 downto 0);
		recover_length     : unsigned(32 - 1 downto 0);
		tracking_length    : unsigned(32 - 1 downto 0);
		max_tracking_error : unsigned(32 - 1 downto 0);
	end record;

	constant RESET_CFG_FSO_MAC_RX_BLOCK_SYNC : trec_cfg_fso_mac_rx_block_sync := (aquisition_length  => to_unsigned(640, 32),
	                                                                             recover_length     => to_unsigned(640, 32),
	                                                                             tracking_length    => to_unsigned(1300500, 32), --2000,32),--
	                                                                             max_tracking_error => to_unsigned(57120, 32)); --550, 32));--

	type trec_cfg_fso_mac_rx_header_sync is record
		aquisition_length  : unsigned(32 - 1 downto 0);
		tracking_length    : unsigned(32 - 1 downto 0);
		max_tracking_error : unsigned(32 - 1 downto 0);
	end record;

	constant RESET_CFG_FSO_MAC_RX_HEADER_SYNC : trec_cfg_fso_mac_rx_header_sync := (aquisition_length  => to_unsigned(32, 32),
	                                                                               tracking_length    => to_unsigned(640, 32),
	                                                                               max_tracking_error => to_unsigned(84, 32));

	type trec_cfg_fso_mac_rx_byte_sync is record
		window_length      : unsigned(32 - 1 downto 0);
		max_tracking_error : unsigned(32 - 1 downto 0);
		wait_for_bit_slip  : unsigned(32 - 1 downto 0);
	end record;

	constant RESET_CFG_FSO_MAC_RX_BYTE_SYNC : trec_cfg_fso_mac_rx_byte_sync := (window_length      => to_unsigned( 64, 32),
	                                                                            max_tracking_error => to_unsigned( 16, 32),
	                                                                            wait_for_bit_slip  => to_unsigned(66, 32));

	type trec_cfg_fso_mac_rx is record
		byte_sync   : trec_cfg_fso_mac_rx_byte_sync;
		header_sync : trec_cfg_fso_mac_rx_header_sync;
		block_sync  : trec_cfg_fso_mac_rx_block_sync;
	end record;

	constant RESET_CFG_FSO_MAC_RX : trec_cfg_fso_mac_rx := (byte_sync => RESET_CFG_FSO_MAC_RX_BYTE_SYNC,
	                                                        header_sync => RESET_CFG_FSO_MAC_RX_HEADER_SYNC,
	                                                        block_sync => RESET_CFG_FSO_MAC_RX_BLOCK_SYNC);


	type trec_framer_diag_data is record
		total_frames : unsigned(47 downto 0);
		rx_fifo_skipped_frames : std_logic_vector(31 downto 0);
	end record;


	type t_frame_ram is array(0 to 15) of std_logic_vector(63 downto 0);

	type trec_deframer_diag_data is record
		let_collector     : trec_stat_let_collector;
		deframing         : trec_stat_let_deframing;
		crc_result        : std_logic_vector(32 - 1 downto 0);
		crc_error_cnt     : std_logic_vector(47 downto 0);
		non_crc_error_cnt : std_logic_vector(31 downto 0);
		pkt_fifo_cnt      : unsigned(32 - 1 downto 0);
		pkt_fifo_err_cnt  : unsigned(32 - 1 downto 0);
		frame_ram         : t_frame_ram;
	end record;

	type trec_enc_rs_diag_data is record
		enc_input_frames      : std_logic_vector(47 downto 0);
		enc_output_frames     : std_logic_vector(47 downto 0);
	end record;

	type trec_dec_rs_diag_data is record
		dec_input_frames       : std_logic_vector(47 downto 0);
		dec_output_frames      : std_logic_vector(47 downto 0);
		stage_0_total_frames   : std_logic_vector(47 downto 0);
		stage_1_total_frames   : std_logic_vector(47 downto 0);
		stage_2_total_frames   : std_logic_vector(47 downto 0);
		stage_3_total_frames   : std_logic_vector(47 downto 0);
		stage_0_error_frames   : std_logic_vector(47 downto 0);
		stage_1_error_frames   : std_logic_vector(47 downto 0);
		stage_2_error_frames   : std_logic_vector(47 downto 0);
		stage_3_error_frames   : std_logic_vector(47 downto 0);
		stage_0_corrections    : std_logic_vector(47 downto 0);
		stage_1_corrections    : std_logic_vector(47 downto 0);
		stage_2_corrections    : std_logic_vector(47 downto 0);
		stage_3_corrections    : std_logic_vector(47 downto 0);
	end record;

	type trec_ctrl_lpbk_cfg is record
		loopback_framer_en  : std_logic;
		loopback_fec_en     : std_logic;
		loopback_fso_mac_en : std_logic;
		statistic_clear     : std_logic;
		crc_enable          : std_logic;
	end record;

	type trec_topmdule_diag_data is record
		crc_result           : std_logic_vector(31 downto 0);
		fso_frame_counter    : std_logic_vector(23 downto 0);
		fso_rs_error         : std_logic;
		fso_block_counter    : std_logic_vector( 7 downto 0);
		enc_frame_counter    : std_logic_vector(23 downto 0);
		frame_counter_diff   : std_logic_vector(23 downto 0);
	end record;

end package pkg_types;

package body pkg_types is

	function shift_right_byte_array (byte_array : t_byte_array; shift_value : natural) return t_byte_array is
		constant LENGTH : natural := byte_array'length;
		variable v_result : t_byte_array(LENGTH - 1 downto 0);
	begin
		for i in 0 to 15 loop
			if shift_value = i then
				v_result(LENGTH - 1 - i downto 0)      := byte_array(LENGTH - 1 downto i);
				v_result(LENGTH - 1 downto LENGTH - i) := (others => (others => '0'));
			end if;
		end loop;
		return v_result;
	end function shift_right_byte_array;

	function crc32(input: std_logic_vector(7 downto 0); crc:  std_logic_vector(31 downto 0)) return std_logic_vector is
		variable v_sreg: std_logic_vector(31 downto 0);
		variable v_input : std_logic_vector(7 downto 0);
	begin
		for i in 0 to 7 loop
			v_input(i) := input(7 - i);
		end loop;

		v_sreg(0) := v_input(6) xor v_input(0) xor crc(24) xor crc(30);
		v_sreg(1) := v_input(7) xor v_input(6) xor v_input(1) xor v_input(0) xor crc(24) xor crc(25) xor crc(30) xor crc(31);
		v_sreg(2) := v_input(7) xor v_input(6) xor v_input(2) xor v_input(1) xor v_input(0) xor crc(24) xor crc(25) xor crc(26) xor crc(30) xor crc(31);
		v_sreg(3) := v_input(7) xor v_input(3) xor v_input(2) xor v_input(1) xor crc(25) xor crc(26) xor crc(27) xor crc(31);
		v_sreg(4) := v_input(6) xor v_input(4) xor v_input(3) xor v_input(2) xor v_input(0) xor crc(24) xor crc(26) xor crc(27) xor crc(28) xor crc(30);
		v_sreg(5) := v_input(7) xor v_input(6) xor v_input(5) xor v_input(4) xor v_input(3) xor v_input(1) xor v_input(0) xor crc(24) xor crc(25) xor crc(27) xor crc(28) xor crc(29) xor crc(30) xor crc(31);
		v_sreg(6) := v_input(7) xor v_input(6) xor v_input(5) xor v_input(4) xor v_input(2) xor v_input(1) xor crc(25) xor crc(26) xor crc(28) xor crc(29) xor crc(30) xor crc(31);
		v_sreg(7) := v_input(7) xor v_input(5) xor v_input(3) xor v_input(2) xor v_input(0) xor crc(24) xor crc(26) xor crc(27) xor crc(29) xor crc(31);
		v_sreg(8) := v_input(4) xor v_input(3) xor v_input(1) xor v_input(0) xor crc(0) xor crc(24) xor crc(25) xor crc(27) xor crc(28);
		v_sreg(9) := v_input(5) xor v_input(4) xor v_input(2) xor v_input(1) xor crc(1) xor crc(25) xor crc(26) xor crc(28) xor crc(29);
		v_sreg(10) := v_input(5) xor v_input(3) xor v_input(2) xor v_input(0) xor crc(2) xor crc(24) xor crc(26) xor crc(27) xor crc(29);
		v_sreg(11) := v_input(4) xor v_input(3) xor v_input(1) xor v_input(0) xor crc(3) xor crc(24) xor crc(25) xor crc(27) xor crc(28);
		v_sreg(12) := v_input(6) xor v_input(5) xor v_input(4) xor v_input(2) xor v_input(1) xor v_input(0) xor crc(4) xor crc(24) xor crc(25) xor crc(26) xor crc(28) xor crc(29) xor crc(30);
		v_sreg(13) := v_input(7) xor v_input(6) xor v_input(5) xor v_input(3) xor v_input(2) xor v_input(1) xor crc(5) xor crc(25) xor crc(26) xor crc(27) xor crc(29) xor crc(30) xor crc(31);
		v_sreg(14) := v_input(7) xor v_input(6) xor v_input(4) xor v_input(3) xor v_input(2) xor crc(6) xor crc(26) xor crc(27) xor crc(28) xor crc(30) xor crc(31);
		v_sreg(15) := v_input(7) xor v_input(5) xor v_input(4) xor v_input(3) xor crc(7) xor crc(27) xor crc(28) xor crc(29) xor crc(31);
		v_sreg(16) := v_input(5) xor v_input(4) xor v_input(0) xor crc(8) xor crc(24) xor crc(28) xor crc(29);
		v_sreg(17) := v_input(6) xor v_input(5) xor v_input(1) xor crc(9) xor crc(25) xor crc(29) xor crc(30);
		v_sreg(18) := v_input(7) xor v_input(6) xor v_input(2) xor crc(10) xor crc(26) xor crc(30) xor crc(31);
		v_sreg(19) := v_input(7) xor v_input(3) xor crc(11) xor crc(27) xor crc(31);
		v_sreg(20) := v_input(4) xor crc(12) xor crc(28);
		v_sreg(21) := v_input(5) xor crc(13) xor crc(29);
		v_sreg(22) := v_input(0) xor crc(14) xor crc(24);
		v_sreg(23) := v_input(6) xor v_input(1) xor v_input(0) xor crc(15) xor crc(24) xor crc(25) xor crc(30);
		v_sreg(24) := v_input(7) xor v_input(2) xor v_input(1) xor crc(16) xor crc(25) xor crc(26) xor crc(31);
		v_sreg(25) := v_input(3) xor v_input(2) xor crc(17) xor crc(26) xor crc(27);
		v_sreg(26) := v_input(6) xor v_input(4) xor v_input(3) xor v_input(0) xor crc(18) xor crc(24) xor crc(27) xor crc(28) xor crc(30);
		v_sreg(27) := v_input(7) xor v_input(5) xor v_input(4) xor v_input(1) xor crc(19) xor crc(25) xor crc(28) xor crc(29) xor crc(31);
		v_sreg(28) := v_input(6) xor v_input(5) xor v_input(2) xor crc(20) xor crc(26) xor crc(29) xor crc(30);
		v_sreg(29) := v_input(7) xor v_input(6) xor v_input(3) xor crc(21) xor crc(27) xor crc(30) xor crc(31);
		v_sreg(30) := v_input(7) xor v_input(4) xor crc(22) xor crc(28) xor crc(31);
		v_sreg(31) := v_input(5) xor crc(23) xor crc(29);
		return v_sreg;
	end crc32;

end package body pkg_types;
