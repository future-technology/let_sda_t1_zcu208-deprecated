--!
--! Copyright (C) 2020 Creonic GmbH
--!
--! @file
--! @brief  Bit synchronizer
--! @author Nhan Nguyen
--! @date   2020/03/02
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bit_sync is
	port(
		clk_in   : in std_logic;
		data_in  : in std_logic;
		data_out : out std_logic
	);
end entity bit_sync;

architecture rtl of bit_sync is

	attribute ASYNC_REG : string;

	signal in_meta   : std_logic := '0';
	signal in_sync_0 : std_logic := '0';
	signal in_sync_1 : std_logic := '0';
	signal in_sync_2 : std_logic := '0';
	signal in_sync_3 : std_logic := '0';
	signal in_out    : std_logic := '0';


	attribute ASYNC_REG of in_meta: signal is "TRUE";
	attribute ASYNC_REG of in_sync_0: signal is "TRUE";
	attribute ASYNC_REG of in_sync_1: signal is "TRUE";
	attribute ASYNC_REG of in_sync_2: signal is "TRUE";
	attribute ASYNC_REG of in_sync_3: signal is "TRUE";

begin

	-- sync registers
	pr_sync_reg : process(clk_in) is
	begin
		if rising_edge(clk_in) then
			in_meta   <= data_in;
			in_sync_0 <= in_meta;
			in_sync_1 <= in_sync_0;
			in_sync_2 <= in_sync_1;
			in_sync_3 <= in_sync_2;
			in_out    <= in_sync_3;
		end if;
	end process;

	data_out <= in_out;

end architecture rtl;
