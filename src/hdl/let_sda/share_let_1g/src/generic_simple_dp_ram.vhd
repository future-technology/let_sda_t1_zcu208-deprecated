--!
--! Copyright (C) 2010 - 2011 Creonic GmbH
--!
--! @file
--! @brief  Generic dual port RAM with one read and one write port
--! @author Matthias Alles
--! @date   2010/04/05
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library share_let_1g;
use share_let_1g.pkg_support.all;


entity generic_simple_dp_ram is
	generic(
		DISTR_RAM  : boolean := true;
		WORDS      : integer := 5;
		BITWIDTH   : integer := 1
	);
	port(
		clk : in std_logic;
		rst : in std_logic;

		wen_A : in  std_logic;
		en_A  : in  std_logic;

		a_A  : in  std_logic_vector(no_bits_natural(WORDS - 1) - 1 downto 0);
		d_A  : in  std_logic_vector(BITWIDTH - 1 downto 0 );

		en_B : in  std_logic;
		a_B  : in  std_logic_vector(no_bits_natural(WORDS - 1) - 1 downto 0);
		q_B  : out std_logic_vector(BITWIDTH - 1 downto 0)
	);
end generic_simple_dp_ram;


architecture rtl of generic_simple_dp_ram is

	type t_ram is array(WORDS - 1 downto 0) of
	                      std_logic_vector(BITWIDTH - 1 downto 0);
	signal simple_dp_ram : t_ram := (others => (others => '0'));
	signal ram_a_B : std_logic_vector(no_bits_natural(WORDS - 1) - 1 downto 0) :=
	                (others => '0');

	function get_ram_style_xilinx(dist_ram : in boolean) return string is
	begin
		if dist_ram then
			return "pipe_distributed";
		else
			return "block";
		end if;
	end function;

	function get_ram_style_altera(dist_ram : in boolean) return string is
	begin
		if dist_ram then
			return "MLAB, no_rw_check";
		else
			return "AUTO";
		end if;
	end function;

	attribute RAM_STYLE : string;
	attribute RAM_STYLE of simple_dp_ram : signal is get_ram_style_xilinx(DISTR_RAM);

	attribute ramstyle : string;
	attribute ramstyle of simple_dp_ram : signal is get_ram_style_altera(DISTR_RAM);

begin

	-- Storage is port A
	pr_port_a: process(clk)
	begin
	if rising_edge(clk) then
		if en_A = '1' then
			if wen_A =  '1' then
				simple_dp_ram(to_integer(unsigned(a_A))) <= d_A;
			end if;
		end if;
	end if;
	end process pr_port_a;

	q_B <= simple_dp_ram(to_integer(unsigned(ram_a_B)));

	-- Reading is port B
	pr_port_b: process(clk)
	begin
	if rising_edge(clk) then
		if en_B = '1' then
			ram_a_B <= a_B;
			-- q_B <= simple_dp_ram(to_integer(unsigned(a_B)));
		end if;
	end if;
	end process pr_port_b;

end rtl;
