--!
--! Copyright (C) 2020 Creonic GmbH
--!
--! @file
--! @brief  Buffer btw first and second encoder/decoder
--! @author Nhan Nguyen
--! @date   2015/04/27
--!
--! ENCODER  : MAX_ROW = 231, MAX_COL = 255, MAX_BLK_SEND = 255
--! DECODER  : MAX_ROW = 255, MAX_COL = 231, MAX_BLK_SEND = 231
--! BUFFER(*): MAX_ROW = 255, MAX_COL = 255, MAX_BLK_SEND = 255
--! If the decoder output the parity bits, set MAX_COL = 255,
--!  which means that all data from decoder will be written to the RAM.
--!  After that, if we dont want to read the parity bits from RAM, set
--!  the MAX_BLK_SEND to 235
--!  (*) BUFFER is used for converting rows <-> columns
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library share_let_1g;
use share_let_1g.pkg_support.all;
use share_let_1g.pkg_components.all;


entity inter_buffer is
	generic(
		BW_SYMB      : integer := 8;
		PARALLELISM  : integer := 15;
		MAX_ROW      : integer := 231;
		MAX_COL      : integer := 255;
		MAX_BLK_SEND : integer := 255
	);
	port(
		clk   : in std_logic;
		rst   : in std_logic;

		input        : in  std_logic_vector(PARALLELISM * BW_SYMB - 1 downto 0);
		input_valid  : in  std_logic;
		input_ready  : out std_logic;
		input_start  : in  std_logic;
		input_end    : in  std_logic;

		output       : out std_logic_vector(PARALLELISM * BW_SYMB - 1 downto 0);
		output_valid : out std_logic;
		output_ready : in  std_logic;
		output_start : out std_logic;
		output_end   : out std_logic
	);
end inter_buffer;


architecture rtl of inter_buffer is

	constant MAX_ADDR           : integer := ceil(MAX_ROW, PARALLELISM) * ceil(MAX_COL, PARALLELISM);
	constant BW_MAX_ADDR        : integer := no_bits_natural(MAX_ADDR - 1);
	constant MAX_OFFSET_ADDR    : integer := ceil(MAX_COL, PARALLELISM);
	constant MAX_RD_OFFSET_ADDR : integer := ceil(MAX_BLK_SEND, PARALLELISM);
	constant MAX_PKT_CNT        : integer := ceil(MAX_ROW, PARALLELISM);
	constant MAX_BASE_ADDR      : integer := MAX_ADDR - MAX_OFFSET_ADDR;
	constant LAST_WR_COL_INDEX  : integer := ((MAX_ROW - 1) mod PARALLELISM);
	constant LAST_RD_COL_INDEX  : integer := ((MAX_BLK_SEND - 1) mod PARALLELISM);
	constant BW_VALID_SREG      : integer := 4;
	constant BW_MAX_OFFSET_ADDR : integer := no_bits_natural(MAX_OFFSET_ADDR - 1);
	constant BW_PARALLELISM     : integer := no_bits_natural(PARALLELISM - 1);
	constant BW_RAM_COL         : integer := PARALLELISM * BW_SYMB;

	signal buf_wr_en       : std_logic;
	signal buf_wr_be       : std_logic_vector(PARALLELISM - 1 downto 0);
	signal buf_wr_addr     : std_logic_vector(BW_MAX_ADDR downto 0);
	signal buf_wr_addr_cnt : unsigned(BW_MAX_ADDR - 1 downto 0);
	signal buf_wr_d        : std_logic_vector(PARALLELISM * PARALLELISM * BW_SYMB - 1 downto 0);

	signal buf_rd_en       : std_logic;
	signal buf_rd_addr     : std_logic_vector(BW_MAX_ADDR downto 0);
	signal buf_rd_addr_cnt : unsigned(BW_MAX_ADDR - 1 downto 0);
	signal buf_rd_q        : std_logic_vector(PARALLELISM * PARALLELISM * BW_SYMB - 1 downto 0);
	signal buf_rd_q_reg    : std_logic_vector(PARALLELISM * PARALLELISM * BW_SYMB - 1 downto 0);
	signal buf_rd_q_buf    : std_logic_vector(PARALLELISM * PARALLELISM * BW_SYMB - 1 downto 0);

	signal input_ready_int : std_logic;
	signal input_reg : std_logic_vector(PARALLELISM * BW_SYMB - 1 downto 0);

	type t_load_fsm is (L_IDLE, L_LOAD, L_WAIT);
	type t_send_fsm is (S_IDLE, S_SEND, S_WAIT);
	signal load_fsm : t_load_fsm;
	signal send_fsm : t_send_fsm;

	signal buf_offset_wr_addr : unsigned(BW_MAX_OFFSET_ADDR - 1 downto 0);
	signal buf_base_wr_addr   : unsigned(BW_MAX_ADDR - 1 downto 0);
	signal buf_col_wr_sel     : std_logic_vector(PARALLELISM - 1 downto 0);
	signal buf_col_wr_sel_d   : std_logic_vector(PARALLELISM - 1 downto 0);
	signal high_wr_addr_sel   : std_logic;
	signal high_wr_addr_sel_d : std_logic;
	signal data_load_done     : std_logic;

	signal buf_loaded         : std_logic_vector(1 downto 0);
	signal data_loaded        : std_logic;
	signal buf_free          : std_logic;
	signal pipe_en            : std_logic;
	signal pipe_en_d          : std_logic;

	signal read_col_cnt       : unsigned(BW_PARALLELISM - 1 downto 0);
	signal read_col_cnt_d     : unsigned(BW_PARALLELISM - 1 downto 0);
	signal read_col_cnt_d1    : unsigned(BW_PARALLELISM - 1 downto 0);
	signal high_rd_addr_sel   : std_logic;
	signal high_rd_addr_sel_d : std_logic;
	signal output_valid_sreg  : std_logic_vector(BW_VALID_SREG - 1 downto 0);
	signal buf_base_rd_addr   : unsigned(BW_MAX_ADDR - 1 downto 0);
	signal buf_offset_rd_addr : unsigned(BW_MAX_OFFSET_ADDR - 1 downto 0);
	signal output_valid_int   : std_logic;
	signal read_col_sel       : unsigned(BW_PARALLELISM - 1 downto 0);
	signal output_mux : std_logic_vector(PARALLELISM * BW_SYMB - 1 downto 0);

	signal buf_send_done      : std_logic_vector(1 downto 0);
	signal output_cnt         : unsigned(BW_MAX_OFFSET_ADDR - 1 downto 0);

begin

	input_ready  <= input_ready_int;
	buf_rd_addr  <= high_rd_addr_sel_d & std_logic_vector(buf_rd_addr_cnt);
	buf_rd_en    <= output_valid_sreg(1) and pipe_en;
	buf_wr_addr  <= high_wr_addr_sel_d & std_logic_vector(buf_wr_addr_cnt);
	output_valid <= output_valid_int;

	-- connect the input register to all columns
	gen_input_wiring: for i in 0 to PARALLELISM - 1 generate
		buf_wr_d(PARALLELISM * BW_SYMB * (i + 1) - 1 downto PARALLELISM * BW_SYMB * i) <= input_reg;
	end generate gen_input_wiring;

	buf_wr_be <= buf_col_wr_sel_d;


	gen_inter_buf_ram: for i in 0 to PARALLELISM - 1 generate
	begin
		inst_generic_simple_dp_ram : generic_simple_dp_ram
		generic map(
			DISTR_RAM  => false,
			WORDS      => (2 ** (BW_MAX_ADDR + 1)),
			BITWIDTH   => BW_RAM_COL
		)
		port map(
			clk => clk,
			rst => rst,

			wen_A => buf_wr_be(i),
			en_A  => buf_wr_en,

			a_A  => buf_wr_addr,
			d_A  => buf_wr_d(BW_RAM_COL * (i + 1) - 1 downto BW_RAM_COL * i),

			en_B  => buf_rd_en,

			a_B   => buf_rd_addr,
			q_B   => buf_rd_q(BW_RAM_COL * (i + 1) - 1 downto BW_RAM_COL * i)
		);
	end generate gen_inter_buf_ram;

	buf_free <= not (buf_loaded(0) and buf_loaded(1));

	-- write the data to the buffer
	pr_write_data: process(clk) is
	begin
	if rising_edge(clk) then
		if rst = '1' then
			buf_wr_en          <= '0';
			buf_offset_wr_addr <= (others => '0');
			buf_base_wr_addr   <= (others => '0');
			buf_wr_addr_cnt    <= (others => '0');
			input_ready_int    <= '0';
			buf_col_wr_sel     <= (others => '0');
			buf_col_wr_sel(0)  <= '1';
			buf_col_wr_sel_d   <= (others => '0');
			high_wr_addr_sel   <= '0';
			high_wr_addr_sel_d <= '0';
			input_reg          <= (others => '0');
			data_load_done     <= '0';
			load_fsm           <= L_IDLE;
		else
			buf_wr_en          <= '0';
			data_load_done     <= '0';
			high_wr_addr_sel_d <= high_wr_addr_sel;
			buf_wr_addr_cnt    <= buf_base_wr_addr + buf_offset_wr_addr;
			buf_col_wr_sel_d   <= buf_col_wr_sel;

			case load_fsm is

			-- Wait for buffer to be available
			when L_IDLE =>
				input_ready_int <= '0';
				if buf_free = '1' then
					load_fsm        <= L_LOAD;
					input_ready_int <= '1';
				end if;

			--
			-- Load data to the RAM.
			-- Refer to the document for data organization in the buffer.
			--
			when L_LOAD =>
				if input_valid = '1' and input_ready_int = '1' then

					input_reg <= input;
					buf_wr_en <= '1';

					if buf_offset_wr_addr < MAX_OFFSET_ADDR - 1 then
						buf_offset_wr_addr <= buf_offset_wr_addr + 1;

					else
						buf_offset_wr_addr <= (others => '0');

						if buf_col_wr_sel(PARALLELISM - 1) = '0' then

							-- point to the next column
							buf_col_wr_sel   <= buf_col_wr_sel(PARALLELISM - 2 downto 0) & '0';
						else

							-- go back to the first column
							buf_col_wr_sel    <= (others => '0');
							buf_col_wr_sel(0) <= '1';
							buf_base_wr_addr  <= buf_base_wr_addr + MAX_OFFSET_ADDR;

						end if;

						if (buf_base_wr_addr = MAX_BASE_ADDR) and
						   buf_col_wr_sel(LAST_WR_COL_INDEX) = '1' then
							buf_base_wr_addr <= (others => '0');

							-- point to the next higher/lower part of the RAM
							high_wr_addr_sel <= not high_wr_addr_sel;

							-- Start from the first column again
							buf_col_wr_sel    <= (others => '0');
							buf_col_wr_sel(0) <= '1';

							input_ready_int  <= '0';
							data_load_done   <= '1';
							load_fsm         <= L_WAIT;
						end if;
					end if;
				end if;

			when L_WAIT =>
				load_fsm <= L_IDLE;

			end case;
		end if;
	end if;
	end process;


	--
	-- monitor the status of each buffer
	-- the buf_loaded will be asserted when data has been loaded to the
	-- corresponding buffer, and de-asserted when data has been already sent
	--
	pr_buf_ctrl: process(clk) is
	begin
	if rising_edge(clk) then
		if rst = '1' then
			buf_loaded  <= "00";
		else
			if buf_loaded(0) = '0' then
				if data_load_done = '1' and high_wr_addr_sel = '1' then
					buf_loaded(0) <= '1';
				end if;
			else
				if buf_send_done(0) = '1' then
					buf_loaded(0) <= '0';
				end if;
			end if;

			if buf_loaded(1) = '0' then
				if data_load_done = '1' and high_wr_addr_sel = '0' then
					buf_loaded(1) <= '1';
				end if;
			else
				if buf_send_done(1) = '1' then
					buf_loaded(1) <= '0';
				end if;
			end if;

		end if;
	end if;
	end process;

	pipe_en     <= (not output_valid_int) or output_ready;
	data_loaded <= buf_loaded(0) or buf_loaded(1);

	--
	-- when 235 rows of data have been written to the buffer, start reading the
	-- data out column-wise.
	--
	pr_read_data: process(clk) is
	begin
	if rising_edge(clk) then
		if rst = '1' then
			read_col_cnt       <= (others => '0');
			read_col_cnt_d     <= (others => '0');
			read_col_cnt_d1    <= (others => '0');
			high_rd_addr_sel   <= '0';
			output_valid_sreg  <= (others => '0');
			buf_base_rd_addr   <= (others => '0');
			buf_offset_rd_addr <= (others => '0');

			buf_rd_addr_cnt    <= (others => '0');
			read_col_sel       <= (others => '0');
			output_valid_int   <= '0';
			high_rd_addr_sel_d <= '0';
			output_start       <= '0';
			output_end         <= '0';
			buf_send_done      <= "00";
			output_cnt         <= (others => '0');
			send_fsm           <= S_IDLE;
		else

			if pipe_en = '1' then
				output_valid_sreg(BW_VALID_SREG - 1 downto 1) <= output_valid_sreg(BW_VALID_SREG - 2 downto 0);

				buf_rd_addr_cnt    <= buf_base_rd_addr + buf_offset_rd_addr;
				output_valid_int   <= output_valid_sreg(BW_VALID_SREG - 1);
				read_col_cnt_d     <= read_col_cnt;
				read_col_cnt_d1    <= read_col_cnt_d;
				read_col_sel       <= read_col_cnt_d1;
				high_rd_addr_sel_d <= high_rd_addr_sel;
			end if;

			-- Generate the _start and _end signal for the output interface
			if output_valid_int = '1' and output_ready = '1' then
				if output_cnt < MAX_PKT_CNT - 1 then
					output_cnt <= output_cnt + 1;
					if output_cnt = MAX_PKT_CNT - 2 then
						output_end <= '1';
					else
						output_end <= '0';
					end if;
					output_start <= '0';
				else
					output_cnt <= (others => '0');
					output_start <= '1';
					output_end   <= '0';
				end if;
			end if;
			if output_valid_sreg(BW_VALID_SREG - 1) = '1' and output_valid_int = '0' then
				output_start <= '1';
			end if;


			buf_send_done <= "00";

			case send_fsm is

			-- Wait for one of the buffer loaded
			when S_IDLE =>
				if data_loaded = '1' then
					send_fsm <= S_SEND;
					output_valid_sreg(0) <= '1';
				end if;

			--
			-- Read data from the buffer
			-- Refer to the documentation for the reading order
			--
			when S_SEND =>

				if pipe_en = '1' then
					if buf_base_rd_addr < MAX_BASE_ADDR then
						buf_base_rd_addr <= buf_base_rd_addr + MAX_OFFSET_ADDR;
					else
						buf_base_rd_addr <= (others => '0');

						if read_col_cnt < PARALLELISM - 1 then
							read_col_cnt  <= read_col_cnt + 1;
						else
							read_col_cnt       <= (others => '0');
							buf_offset_rd_addr <= buf_offset_rd_addr + 1;

						end if;

						if (read_col_cnt = LAST_RD_COL_INDEX and
						    buf_offset_rd_addr = MAX_RD_OFFSET_ADDR - 1) then
							read_col_cnt         <= (others => '0');
							buf_offset_rd_addr   <= (others => '0');
							high_rd_addr_sel     <= not high_rd_addr_sel;
							output_valid_sreg(0) <= '0';
							send_fsm             <= S_WAIT;
							if high_rd_addr_sel = '0' then
								buf_send_done(0) <= '1';
							else
								buf_send_done(1) <= '1';
							end if;
						end if;
					end if;

				end if;

			when S_WAIT =>

				-- if the last data has been registered to the pipeline
				if output_valid_sreg(1) = '0' and pipe_en = '1' then
					send_fsm      <= S_IDLE;
				end if;

			end case;

		end if;
	end if;
	end process pr_read_data;

	-- output mux
	pr_out_mux: process(buf_rd_q_reg, read_col_sel)
	begin
		-- Default values
		for j in 0 to PARALLELISM - 1 loop
			for k in 0 to BW_SYMB - 1 loop
				output_mux(j * BW_SYMB + k) <= buf_rd_q_reg(j * PARALLELISM * BW_SYMB + k);
			end loop;
		end loop;

		for i in 0 to PARALLELISM - 1 loop
			if read_col_sel = to_unsigned(i, BW_PARALLELISM) then
				for j in 0 to PARALLELISM - 1 loop
					for k in 0 to BW_SYMB - 1 loop
						output_mux(j * BW_SYMB + k) <= buf_rd_q_reg(i * BW_SYMB + j * PARALLELISM * BW_SYMB + k);
					end loop;
				end loop;
			end if;
		end loop;
	end process pr_out_mux;

	-- output pipeline
	pr_out_pipe: process(clk) is
	begin
	if rising_edge(clk) then
		if rst = '1' then
			buf_rd_q_reg <= (others => '0');
			buf_rd_q_buf <= (others => '0');
			output       <= (others => '0');
			pipe_en_d    <= '0';
		else
			pipe_en_d <= pipe_en;
			if pipe_en_d = '1' and pipe_en <= '0' and output_valid_sreg(2) = '1' then
				buf_rd_q_buf <= buf_rd_q;
			end if;
			if pipe_en = '1' then
				if pipe_en_d = '0' then
					buf_rd_q_reg <= buf_rd_q_buf;
				else
					buf_rd_q_reg <= buf_rd_q;
				end if;
				output       <= output_mux;
			end if;
		end if;
	end if;
	end process pr_out_pipe;


end rtl;
