--------------------------------------------------------------------------------
-- Entity: scrambler
-- Date:2015-09-17
-- Author: nguyen
--
-- Description ${cursor}
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
--use ieee.std_logic_unsigned.all;

entity scrambler is
	port (
		clk : in std_logic;
		rst : in std_logic;
		data_in   : in std_logic_vector (119 downto 0);
		scram_en  : in std_logic;
		scram_rst : in std_logic;

		data_out : out std_logic_vector (119 downto 0)
	);
end scrambler;

architecture arch of scrambler is
	signal data_c: std_logic_vector (119 downto 0);
	signal lfsr_q: std_logic_vector (14 downto 0);
	signal lfsr_c: std_logic_vector (14 downto 0);
begin
	lfsr_c(0) <= lfsr_q(1) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(13);
	lfsr_c(1) <= lfsr_q(0) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(13) xor lfsr_q(14);
	lfsr_c(2) <= lfsr_q(1) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(14);
	lfsr_c(3) <= lfsr_q(0) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(13);
	lfsr_c(4) <= lfsr_q(0) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(13) xor lfsr_q(14);
	lfsr_c(5) <= lfsr_q(1) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(14);
	lfsr_c(6) <= lfsr_q(0) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(13);
	lfsr_c(7) <= lfsr_q(0) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(14);
	lfsr_c(8) <= lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(13);
	lfsr_c(9) <= lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(13) xor lfsr_q(14);
	lfsr_c(10) <= lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(14);
	lfsr_c(11) <= lfsr_q(0) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(13);
	lfsr_c(12) <= lfsr_q(0) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(13) xor lfsr_q(14);
	lfsr_c(13) <= lfsr_q(1) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(14);
	lfsr_c(14) <= lfsr_q(0) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(12);

	data_c(0) <= data_in(0) xor lfsr_q(14);
	data_c(1) <= data_in(1) xor lfsr_q(13) xor lfsr_q(14);
	data_c(2) <= data_in(2) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(3) <= data_in(3) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(4) <= data_in(4) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(5) <= data_in(5) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(6) <= data_in(6) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(7) <= data_in(7) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(8) <= data_in(8) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(9) <= data_in(9) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(10) <= data_in(10) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(11) <= data_in(11) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(12) <= data_in(12) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(13) <= data_in(13) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(14) <= data_in(14) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(15) <= data_in(15) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13);
	data_c(16) <= data_in(16) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(14);
	data_c(17) <= data_in(17) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(13);
	data_c(18) <= data_in(18) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(14);
	data_c(19) <= data_in(19) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(13);
	data_c(20) <= data_in(20) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(14);
	data_c(21) <= data_in(21) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(13);
	data_c(22) <= data_in(22) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(14);
	data_c(23) <= data_in(23) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(13);
	data_c(24) <= data_in(24) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(14);
	data_c(25) <= data_in(25) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(13);
	data_c(26) <= data_in(26) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(14);
	data_c(27) <= data_in(27) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(13);
	data_c(28) <= data_in(28) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(14);
	data_c(29) <= data_in(29) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(13);
	data_c(30) <= data_in(30) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(12);
	data_c(31) <= data_in(31) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(14);
	data_c(32) <= data_in(32) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(13) xor lfsr_q(14);
	data_c(33) <= data_in(33) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(12) xor lfsr_q(13);
	data_c(34) <= data_in(34) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(11) xor lfsr_q(12);
	data_c(35) <= data_in(35) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(14);
	data_c(36) <= data_in(36) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(13) xor lfsr_q(14);
	data_c(37) <= data_in(37) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(12) xor lfsr_q(13);
	data_c(38) <= data_in(38) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(11) xor lfsr_q(12);
	data_c(39) <= data_in(39) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(14);
	data_c(40) <= data_in(40) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(13) xor lfsr_q(14);
	data_c(41) <= data_in(41) xor lfsr_q(1) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(12) xor lfsr_q(13);
	data_c(42) <= data_in(42) xor lfsr_q(0) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(11) xor lfsr_q(12);
	data_c(43) <= data_in(43) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(14);
	data_c(44) <= data_in(44) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(13) xor lfsr_q(14);
	data_c(45) <= data_in(45) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(46) <= data_in(46) xor lfsr_q(0) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13);
	data_c(47) <= data_in(47) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(14);
	data_c(48) <= data_in(48) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(13) xor lfsr_q(14);
	data_c(49) <= data_in(49) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(50) <= data_in(50) xor lfsr_q(0) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13);
	data_c(51) <= data_in(51) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(14);
	data_c(52) <= data_in(52) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(13) xor lfsr_q(14);
	data_c(53) <= data_in(53) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(54) <= data_in(54) xor lfsr_q(0) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13);
	data_c(55) <= data_in(55) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(14);
	data_c(56) <= data_in(56) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(13) xor lfsr_q(14);
	data_c(57) <= data_in(57) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(58) <= data_in(58) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13);
	data_c(59) <= data_in(59) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(14);
	data_c(60) <= data_in(60) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(13);
	data_c(61) <= data_in(61) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(12);
	data_c(62) <= data_in(62) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(14);
	data_c(63) <= data_in(63) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(13);
	data_c(64) <= data_in(64) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(12) xor lfsr_q(14);
	data_c(65) <= data_in(65) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(11) xor lfsr_q(13) xor lfsr_q(14);
	data_c(66) <= data_in(66) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(13);
	data_c(67) <= data_in(67) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(14);
	data_c(68) <= data_in(68) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(13);
	data_c(69) <= data_in(69) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(12);
	data_c(70) <= data_in(70) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(14);
	data_c(71) <= data_in(71) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(13);
	data_c(72) <= data_in(72) xor lfsr_q(1) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(12) xor lfsr_q(14);
	data_c(73) <= data_in(73) xor lfsr_q(0) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(11) xor lfsr_q(13) xor lfsr_q(14);
	data_c(74) <= data_in(74) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(13);
	data_c(75) <= data_in(75) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(12);
	data_c(76) <= data_in(76) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(11);
	data_c(77) <= data_in(77) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(14);
	data_c(78) <= data_in(78) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(13) xor lfsr_q(14);
	data_c(79) <= data_in(79) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(12) xor lfsr_q(13);
	data_c(80) <= data_in(80) xor lfsr_q(1) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(14);
	data_c(81) <= data_in(81) xor lfsr_q(0) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(13) xor lfsr_q(14);
	data_c(82) <= data_in(82) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(13);
	data_c(83) <= data_in(83) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(12);
	data_c(84) <= data_in(84) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(11);
	data_c(85) <= data_in(85) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(14);
	data_c(86) <= data_in(86) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(13) xor lfsr_q(14);
	data_c(87) <= data_in(87) xor lfsr_q(0) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(12) xor lfsr_q(13);
	data_c(88) <= data_in(88) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(14);
	data_c(89) <= data_in(89) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(13) xor lfsr_q(14);
	data_c(90) <= data_in(90) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(91) <= data_in(91) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(92) <= data_in(92) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13);
	data_c(93) <= data_in(93) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(14);
	data_c(94) <= data_in(94) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(13) xor lfsr_q(14);
	data_c(95) <= data_in(95) xor lfsr_q(0) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(13);
	data_c(96) <= data_in(96) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(14);
	data_c(97) <= data_in(97) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(13) xor lfsr_q(14);
	data_c(98) <= data_in(98) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(99) <= data_in(99) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13) xor lfsr_q(14);
	data_c(100) <= data_in(100) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(13);
	data_c(101) <= data_in(101) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(14);
	data_c(102) <= data_in(102) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(13) xor lfsr_q(14);
	data_c(103) <= data_in(103) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(13);
	data_c(104) <= data_in(104) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(14);
	data_c(105) <= data_in(105) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(13);
	data_c(106) <= data_in(106) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(14);
	data_c(107) <= data_in(107) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(13) xor lfsr_q(14);
	data_c(108) <= data_in(108) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(13);
	data_c(109) <= data_in(109) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(14);
	data_c(110) <= data_in(110) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(13) xor lfsr_q(14);
	data_c(111) <= data_in(111) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(13);
	data_c(112) <= data_in(112) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(14);
	data_c(113) <= data_in(113) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(13);
	data_c(114) <= data_in(114) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(14);
	data_c(115) <= data_in(115) xor lfsr_q(0) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(13) xor lfsr_q(14);
	data_c(116) <= data_in(116) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(13);
	data_c(117) <= data_in(117) xor lfsr_q(1) xor lfsr_q(3) xor lfsr_q(4) xor lfsr_q(6) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(11) xor lfsr_q(12) xor lfsr_q(14);
	data_c(118) <= data_in(118) xor lfsr_q(0) xor lfsr_q(2) xor lfsr_q(3) xor lfsr_q(5) xor lfsr_q(6) xor lfsr_q(8) xor lfsr_q(10) xor lfsr_q(11) xor lfsr_q(13) xor lfsr_q(14);
	data_c(119) <= data_in(119) xor lfsr_q(1) xor lfsr_q(2) xor lfsr_q(4) xor lfsr_q(5) xor lfsr_q(7) xor lfsr_q(9) xor lfsr_q(10) xor lfsr_q(12) xor lfsr_q(13);

	-- regs 000000010101001
	pr_regs : process (clk)
	begin
	if rising_edge(clk) then
		if (rst = '1') then
			lfsr_q   <= b"100101010000000";
			data_out <= (others => '0');
		else
			if (scram_rst = '1') then
		 		lfsr_q <= b"100101010000000";
			elsif (scram_en = '1') then
				lfsr_q <= lfsr_c;
			end if;

			if (scram_en = '1') then
				data_out <= data_c;
			end if;
		end if;
	end if;
	end process pr_regs;

end arch;

