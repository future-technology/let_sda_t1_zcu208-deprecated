--!
--! Copyright (C) 2012 Creonic GmbH
--!
--! @file
--! @brief  Async FIFO that allows to store data, based on Creonic Axi-Stream FIFO.
--! @author Gustavo Martin
--! @date   2022/09/14
--!
--! @details Simple Async FIFO .
--! @params DISTR_RAM Use distributed or Block RAM for FIFO.
--! @params FIFO_DEPTH Number of elements the FIFO can store.
--! @params DATA_WIDTH Bits within one AXI4-Stream data word.
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library share_let_1g;
use share_let_1g.pkg_support.all;
use share_let_1g.pkg_components.all;

entity sync_fifo is
	generic (
		DISTR_RAM         : boolean := true;
		FIFO_DEPTH        : natural := 128;
		DATA_WIDTH        : natural := 1;
		FULL_THRESHOLD    : natural := 128;
		EMPTY_THRESHOLD    : natural := 1
	);
	port (

	clk            : in  std_logic;
	rst            : in  std_logic;

	-- Input data handling
	----------------------
	input             : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
	input_valid       : in  std_logic;
	input_ready       : out std_logic;
	input_almost_full : out std_logic;
	input_almost_empty : out std_logic;
	fifo_max_level     : out std_logic_vector(no_bits_natural(FIFO_DEPTH) - 1 downto 0);
	fifo_data_count    : out std_logic_vector(no_bits_natural(FIFO_DEPTH) - 1 downto 0);

	-- Output data handling
	-----------------------
	output          : out std_logic_vector(DATA_WIDTH - 1 downto 0);
	output_valid    : out std_logic;
	output_ready    : in  std_logic
);
end entity sync_fifo;


architecture rtl of sync_fifo is

	constant BW_FIFO_DEPTH : natural := no_bits_natural(FIFO_DEPTH - 1);
	constant BW_FIFO_CNT : natural := no_bits_natural(FIFO_DEPTH);
	signal fifo_content_count : unsigned(BW_FIFO_CNT - 1 downto 0);
	signal max_fifo_content_count : unsigned(BW_FIFO_CNT - 1 downto 0);
	signal fifo_wr_ptr : unsigned(BW_FIFO_DEPTH - 1 downto 0);
	signal fifo_rd_ptr : unsigned(BW_FIFO_DEPTH - 1 downto 0);

	signal fifo_wen_A  : std_logic;
	signal fifo_en_A   : std_logic;

	signal fifo_addr_A : std_logic_vector(BW_FIFO_DEPTH - 1 downto 0);
	signal fifo_data_A : std_logic_vector(DATA_WIDTH - 1 downto 0);

	signal fifo_en_B   : std_logic;
	signal fifo_addr_B : std_logic_vector(BW_FIFO_DEPTH - 1 downto 0);
	signal fifo_data_B : std_logic_vector(DATA_WIDTH - 1 downto 0);


	signal input_read      : std_logic;
	signal input_ready_int : std_logic;

	signal fifo_output_read   : std_logic;
	signal fifo_output_read_d : std_logic;

	signal axi_buffer_ready : std_logic;

	signal buffer_full   : std_logic_vector(1 downto 0);
	signal buffer_data_0 : std_logic_vector(DATA_WIDTH - 1 downto 0);
	signal buffer_data_1 : std_logic_vector(DATA_WIDTH - 1 downto 0);

	signal write_buffer : std_logic_vector(1 downto 0);

	signal output_int       : std_logic_vector(DATA_WIDTH - 1 downto 0);
	signal output_valid_int : std_logic;
begin

	inst_fifo_ram : generic_simple_dp_ram
	generic map(
		DISTR_RAM  => DISTR_RAM,
		WORDS      => FIFO_DEPTH,
		BITWIDTH   => DATA_WIDTH -- last-flag is not stored.
	)
	port map(
		clk   => clk,
		rst   => rst,

		wen_A => fifo_wen_A,
		en_A  => fifo_en_A,

		a_A   => fifo_addr_A,
		d_A   => fifo_data_A,

		en_B  => fifo_en_B,
		a_B   => fifo_addr_B,
		q_B   => fifo_data_B
	);

	fifo_max_level <= std_logic_vector(max_fifo_content_count);
	fifo_data_count <= std_logic_vector(fifo_content_count);

	-- As long as the FIFO isn't full, we accept input data!
	input_ready_int <= '1' when fifo_content_count < FIFO_DEPTH else '0';
	input_ready <= input_ready_int;

	-- '1' is asserted as long as the buffer is nearly full.
	input_almost_full <= '0' when fifo_content_count < FULL_THRESHOLD else '1';

	input_almost_empty <= '0' when fifo_content_count > EMPTY_THRESHOLD else '1';

	-- We read the input when valid and accept are asserted at the same time.
	input_read <= input_valid and input_ready_int;


	--
	-- We read the FIFO RAM, when there is data available and we will have a free
	-- register to store the data in the next clock cycle.
	--
	fifo_output_read <= '1' when fifo_content_count > 0 and
	        ((buffer_full(0) = '0' and (buffer_full(1) = '0' or fifo_output_read_d = '0' or axi_buffer_ready = '1')) or
	         (fifo_output_read_d = '0' and axi_buffer_ready = '1'))
	        else '0';


	--
	-- We write into each buffer, if it is emtpy or currently read - and there is input data
	-- available. If both buffers are empty, we write only to buffer 1.
	--
	write_buffer(1) <= ((not buffer_full(1)) or axi_buffer_ready) and
	                   (buffer_full(0) or fifo_output_read_d);
	write_buffer(0) <= buffer_full(1) and (buffer_full(0) xnor axi_buffer_ready) and
	                   fifo_output_read_d;


	--
	-- FIFO Write Port (A)
	--
	fifo_wen_A  <= input_read;
	fifo_en_A   <= input_read;
	fifo_addr_A <= std_logic_vector(fifo_wr_ptr);
	fifo_data_A <= input;


	--
	-- FIFO Read Port (B)
	--
	fifo_en_B <= fifo_output_read;
	fifo_addr_B <= std_logic_vector(fifo_rd_ptr);

	--
	-- This process get the max fill level of the FIFO
	--
	pr_max_fill_level: process(clk) is
	begin
	if rising_edge(clk) then
		if rst = '1' then
			max_fifo_content_count <= (others => '0');
		else
			if max_fifo_content_count < fifo_content_count then
				max_fifo_content_count <= fifo_content_count;
			end if;
		end if;
	end if;
	end process pr_max_fill_level;


	--
	-- This process controls read / write pointer to the FIFO, as well as content counter.
	--
	pr_fifo_ctrl: process(clk) is
	begin
	if rising_edge(clk) then
		if rst = '1' then
			fifo_wr_ptr <= (others => '0');
			fifo_rd_ptr <= (others => '0');

			fifo_content_count <= (others => '0');
			fifo_output_read_d <= '0';
		else
			fifo_output_read_d <= fifo_output_read;

			if input_read = '1' then
				if fifo_wr_ptr < FIFO_DEPTH - 1 then
					fifo_wr_ptr <= fifo_wr_ptr + 1;
				else
					fifo_wr_ptr <= (others => '0');
				end if;
			end if;

			if fifo_output_read = '1' then
				if fifo_rd_ptr < FIFO_DEPTH - 1 then
					fifo_rd_ptr <= fifo_rd_ptr + 1;
				else
					fifo_rd_ptr <= (others => '0');
				end if;
			end if;

			-- Modify the content counter only when exclusively reading / writing.
			if input_read = '0' and fifo_output_read = '1' then
				fifo_content_count <= fifo_content_count - 1;
			elsif input_read = '1' and fifo_output_read = '0' then
				fifo_content_count <= fifo_content_count + 1;

				-- Check that we do never have an overflow!
-- pragma translate_off
				assert fifo_content_count < FIFO_DEPTH
				    report "Writing while FIFO Buffer is full!" severity failure;
				assert fifo_output_read_d /= '1' or write_buffer /= "00"
				    report "FIFO RAM output data cannot be written to register!" severity failure;
-- pragma translate_on
			end if;
		end if;
	end if;
	end process pr_fifo_ctrl;



	--
	-- This process reads the FIFO RAM output and stores it in the two
	-- output registers. Only with two registers we are able to provide a
	-- continous data stream on the output side. This is due to the one
	-- clock latency induced by reading from the memory.
	--
	pr_fifo_ram_output_reg: process(clk) is
	begin
	if rising_edge(clk) then
		if rst = '1' then
			buffer_full <= "00";
			buffer_data_0 <= (others => '0');
			buffer_data_1 <= (others => '0');
		else

			-- Write to the buffers and set the full flags.
			if write_buffer(0) = '1' then
				buffer_full(0) <= '1';
				buffer_data_0 <= fifo_data_B(DATA_WIDTH - 1 downto 0);
			elsif write_buffer(1) = '1' then
				buffer_full(0) <= '0';
			end if;
			if write_buffer(1) = '1' then
				buffer_full(1) <= '1';

				-- Get data from buffer 0, if full, otherwise from FIFO RAM
				if buffer_full(0) = '1' then
					buffer_data_1 <= buffer_data_0;
				else
					buffer_data_1 <= fifo_data_B(DATA_WIDTH - 1 downto 0);
				end if;
			elsif axi_buffer_ready = '1' then
				buffer_full(1) <= '0';
			end if;
		end if;
	end if;
	end process pr_fifo_ram_output_reg;


	--
	-- Use intermediate signals here, since they are either used for the output
	-- directly (USE_OUTPUT_BUFFER = false) or as input to the additional
	-- output buffer (USE_OUTPUT_BUFFER = true).
	--
	output_valid_int <= buffer_full(1);
	output_int       <= buffer_data_1;

	axi_buffer_ready <= output_ready;

	output_valid <= output_valid_int;
	output       <= output_int;


end architecture rtl;
