
----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Charles Garcia
-- 
-- Create Date: 11/11/2022 11:40:33 AM
-- Design Name: 
-- Module Name: preamble_framing_rx_tb - Behavioral
-- Project Name: let_sda_hard
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Testbench for LET SDA Top wrapper
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use std.textio.all;
use ieee.std_logic_textio.all;


entity frame_timing_detector_top_tb is
	generic(

	-- clock period
	CLK_PERIOD     : time    := 5.000 ns;

	-- Number of frames to simulate
	NUM_FRAME   : natural := 30;

	-- Which level has an assertion when it fails?
	ASSERTION_SEVERITY  : severity_level := FAILURE

	);
end frame_timing_detector_top_tb;


architecture sim of frame_timing_detector_top_tb is
       
       component frame_timing_detector_top is
    Port ( 
        clk_let           : in  std_logic;
        rst_let           : in  std_logic;
        clk_tx            : in  std_logic;
        clk_rx            : in  std_logic;
        rst_tx            : in  std_logic;
        rst_rx            : in  std_logic;

        -- Input data from FSO PHY
        ----------------------
        s_axis_data_tx     : in  std_logic_vector(32 - 1 downto 0);
        s_axis_valid_tx    : in  std_logic;
		s_axis_data_rx     : in  std_logic_vector(32 - 1 downto 0);
		s_axis_valid_rx    : in  std_logic;

        reset_timer         : in std_logic;

        -- Output data 
        ----------------------
        let_timestamp_tx        : out std_logic_vector(40-1 downto 0);
        let_timestamp_valid_tx  : out std_logic;
        let_timestamp_rd_en_tx  : in std_logic;
        let_timestamp_rx        : out std_logic_vector(40-1 downto 0);
        let_timestamp_valid_rx  : out std_logic;
        let_timestamp_rd_en_rx  : in std_logic
    );
    end component;
                                     
	constant preamble : std_logic_vector(64-1 downto 0) := X"53225b1d0d73df03";
	constant FRAME_BYTE_SIZE : natural :=  1176;-- (1176, 1368, 1512, 1704, 2232);
	constant TEST_DATA_BLOCK_SIZE : natural := (FRAME_BYTE_SIZE + 8) *100;


	signal s_axis_tdata_int : std_logic_vector(32-1 downto 0);
	

    -- Input data handling
    -- simulated diat from the 
    ----------------------                 
    signal s_axis_tdata    : std_logic_vector(7 downto 0);
	signal s_axis_ctrl_tready : std_logic;
	signal s_axis_ctrl_tvalid : std_logic;
	signal s_axis_ctrl_tdata  : std_logic_vector(2 downto 0);
	signal clear_stat         : std_logic;
	signal cfg_acquisition_length : unsigned(31 downto 0);
	signal cfg_tracking_length    : unsigned(31 downto 0);
	signal cfg_max_tracking_error : unsigned(31 downto 0);
	signal cfg_preamble_max_corr  : unsigned(6 downto 0); -- CAGS: preamle detection thresh control

	-- sim signals 
	
	signal tx_axis_tdata    : std_logic_vector(64 - 1 downto 0);
	
	
	signal C_PHASE_A : time := 0 ns;
	
	signal test_stage : integer range 0 to 100 := 0;

	signal clk                 : std_logic := '0';
	signal mac_rx_clk_out      : std_logic := '0';
	signal rst                 : std_logic;
	signal reset_sync_200      : std_logic;
	signal mac_rx_reset        : std_logic;
	signal test_enable        	: std_logic;
	signal test_start        	: std_logic;
	signal test_finished       : std_logic;

	signal ta_disp_sum        : std_logic_vector(23 downto 0);
	signal tb_disp_sum        : std_logic_vector(23 downto 0);

	signal tx_ts_tx  		: std_logic_vector(40-1 downto 0);
	signal tx_ts_valid_tx  	: std_logic;

	signal byte_cnt         : natural;
	signal got_frame_id     : std_logic;

    signal clk_let          : std_logic;
    signal rst_let          : std_logic;

	signal s_axis_data_tx   : std_logic_vector(32 - 1 downto 0);
	signal s_axis_valid_tx  : std_logic;
	signal s_axis_data_rx   : std_logic_vector(32 - 1 downto 0);
    signal s_axis_valid_rx  : std_logic;
	signal reset_timer 	    : std_logic;

	signal let_timestamp_tx : std_logic_vector(40-1 downto 0);
	signal let_timestamp_valid_tx  : std_logic;
	signal let_timestamp_rx        : std_logic_vector(40-1 downto 0);
	signal let_timestamp_valid_rx  : std_logic;
	signal let_timestamp_rd_en_tx  : std_logic;
	signal let_timestamp_rd_en_rx  : std_logic;
	-- Opening the file in write mode
	 type t_integer_array       is array(integer range <> )  of integer;
   

begin


    dtu : frame_timing_detector_top
   port map( 
        clk_let      => clk,
        rst_let      => rst,
        clk_tx       => clk,
        clk_rx       => clk,
        rst_tx       => rst,
        rst_rx       => rst,

        -- Input data from FSO PHY
        ----------------------
        s_axis_data_tx    => s_axis_data_tx,
        s_axis_valid_tx   => s_axis_valid_tx,
		s_axis_data_rx    => s_axis_data_rx,
		s_axis_valid_rx   => s_axis_valid_rx,

        reset_timer       => reset_timer,

        -- Output data 
        ----------------------
        let_timestamp_tx        => let_timestamp_tx,
        let_timestamp_valid_tx  => let_timestamp_valid_tx,
        let_timestamp_rd_en_tx  => let_timestamp_rd_en_tx,
        let_timestamp_rx        => let_timestamp_rx,
        let_timestamp_valid_rx  => let_timestamp_valid_rx,
        let_timestamp_rd_en_rx  => let_timestamp_rd_en_rx
    );
    

-------------------------------------------
	-- stimuli generation
	-------------------------------------------

	pr_stimuli: process
		variable v_block_cnt        : integer := 0;
		variable v_insert_invalid   : boolean := false;
		variable v_byte_cnt         : natural;
		variable v_frame_cnt        : natural;
		variable v_frame_cnt_word   : std_logic_vector(15 downto 0);
		
		 --file STIM_IN : text open READ_MODE is  "rx_data_sim_man.txt";
		 file STIM_IN : text open READ_MODE is  "rx_data_sim.txt";
		 variable v_LINE    : line; 
		 variable v_data_read        : t_integer_array(1 to 8);	 
		 variable  data_bit_shift : std_logic_vector(15 downto 0);
		 variable v_index_shift        : integer := 0;
       
	begin
	  
        readline(STIM_IN, v_LINE);
        readline(STIM_IN, v_LINE);
        readline(STIM_IN, v_LINE);
        readline(STIM_IN, v_LINE);
        readline(STIM_IN, v_LINE);
        readline(STIM_IN, v_LINE);
        readline(STIM_IN, v_LINE);
        readline(STIM_IN, v_LINE);
     
		-- reset the system
		rst              <= '1';
		reset_timer      <= '1';

		-- configuration
		s_axis_ctrl_tdata <= (others => '0');
		tx_axis_tdata <= (others => '0');
		s_axis_valid_rx   <= '0';
		s_axis_valid_tx   <= '0';
		let_timestamp_rd_en_tx <= '0';
		let_timestamp_rd_en_rx <= '0';
		
		wait for 5 * CLK_PERIOD;

		-- release the reset
		rst         <= '0';
		wait for 5 * CLK_PERIOD;

		-- send data of one block
		for byte_id in 0 to TEST_DATA_BLOCK_SIZE loop
		
			 s_axis_valid_tx   <= '1';
			 s_axis_valid_rx   <= '1';
			 let_timestamp_rd_en_tx <= '1';
			 let_timestamp_rd_en_rx <= '1';
			 reset_timer      <= '0';
	          
	          readline(STIM_IN, v_LINE);  
              tx_axis_tdata(64-1 downto 32) <= tx_axis_tdata(32-1 downto 0);
              
              for i in 1 to 8 loop
              read(v_LINE, v_data_read(i));
                  if v_data_read(i) = 1 then
                    tx_axis_tdata(32-i) <= '1';
                  else 
                    tx_axis_tdata(32-i) <= '0';  
                  end if;                             
              end loop;
              
              readline(STIM_IN, v_LINE);
              for j in 1 to 8 loop
              read(v_LINE, v_data_read(j));
                  if v_data_read(j)= 1 then
                    tx_axis_tdata(24-j) <= '1';
                  else 
                    tx_axis_tdata(24-j) <= '0';  
                  end if;                             
              end loop;
              
              readline(STIM_IN, v_LINE);
              for k in 1 to 8 loop
              read(v_LINE, v_data_read(k));
                  if v_data_read(k)= 1 then
                    tx_axis_tdata(16-k) <= '1';
                  else 
                    tx_axis_tdata(16-k) <= '0';  
                  end if;                             
              end loop;
              
              readline(STIM_IN, v_LINE);
              for h in 1 to 8 loop
              read(v_LINE, v_data_read(h));
                  if v_data_read(h)= 1 then
                    tx_axis_tdata(8-h) <= '1';
                  else 
                    tx_axis_tdata(8-h) <= '0';  
                  end if;                             
              end loop;
              
            if let_timestamp_valid_rx = '1' then
                v_index_shift := v_index_shift +1;
            end if;
               
            s_axis_data_tx  <= tx_axis_tdata(31+v_index_shift downto 0+v_index_shift);  
            s_axis_data_rx  <= tx_axis_tdata(31+v_index_shift downto 0+v_index_shift);  
               
				---wait until s_axis_tvalid = '1' and rising_edge(clk);
				
				wait until falling_edge(clk);		
		
		end loop;

		wait;
	end process pr_stimuli;



	-------------------------------------------
	-- clock generation
	-------------------------------------------
	clk <= not clk after CLK_PERIOD / 2;
	-------------------------------------------
	-- fso_phy:
	-------------------------------------------



end architecture sim;










