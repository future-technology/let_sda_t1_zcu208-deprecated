--! Copyright (C) 2020 Creonic GmbH
--!
--! @file
--! @brief  
--! @author Nhan Nguyen
--! @date   2020/01/17
--!


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;
use std.textio.all;
use IEEE.std_logic_textio;
-- Xilinx components
library unisim;
use unisim.vcomponents.all;

-- library share_let_10g;
-- use share_let_10g.pkg_support.all;
-- use share_let_10g.pkg_components.all;
-- use share_let_10g.pkg_param.all;
-- use share_let_10g.pkg_types.all;

library let_sda_lib;
use let_sda_lib.let_pckg.all; -- for CRC-32
library ethernet_framer_lib;
use ethernet_framer_lib.txt_util.all;

entity mac_packet_gen is
	port (
		clk     : in  std_logic;
		rst     : in  std_logic;

		mac_1g_rx_data    : in  std_logic_vector(8 - 1 downto 0);
		mac_1g_rx_valid   : in  std_logic;
		mac_1g_rx_last    : in  std_logic;
		mac_1g_rx_tuser   : in  std_logic;

		mac_1g_tx_data    : out std_logic_vector(8 - 1 downto 0);
		mac_1g_tx_valid   : out std_logic;
		mac_1g_tx_last    : out std_logic;
		mac_1g_tx_ready   : in  std_logic;
		mac_1g_tx_tuser   : out std_logic;

		fixed_packet_len  : in  std_logic;
		packet_len_in     : in  std_logic_vector(13 downto 0);
		num_packet        : in  std_logic_vector(31 downto 0);
		pkt_gen_en        : in  std_logic
	);
end entity mac_packet_gen;

architecture arch of mac_packet_gen is

	component xxv_ethernet_0_pktprbs_gen
	port (
		ip     : in  std_logic_vector(31 downto 0);
		op     : out std_logic_vector(31 downto 0);
		datout : out std_logic_vector(63 downto 0)
	);
	end component xxv_ethernet_0_pktprbs_gen;

	component xxv_ethernet_0_pkt_len_gen
	port (
		clk   : in  std_logic;
		reset : in  std_logic;
		enable : in  std_logic;
		pkt_len : out std_logic_vector(16 downto 0)
	);
	end component xxv_ethernet_0_pkt_len_gen;
	
	component bit_sync
	port(
		clk_in   : in std_logic;
		data_in  : in std_logic;
		data_out : out std_logic
	);
    end component bit_sync;
	
	
	COMPONENT eth_8_to_64_dw_conv
        PORT (
            aclk          : IN STD_LOGIC;
            aresetn       : IN STD_LOGIC;
            s_axis_tvalid : IN STD_LOGIC;
            s_axis_tready : OUT STD_LOGIC;
            s_axis_tdata  : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
            s_axis_tkeep  : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            s_axis_tlast  : IN STD_LOGIC;
            s_axis_tuser  : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            m_axis_tvalid : OUT STD_LOGIC;
            m_axis_tready : IN STD_LOGIC;
            m_axis_tdata  : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
            m_axis_tkeep  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            m_axis_tlast  : OUT STD_LOGIC;
            m_axis_tuser  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
        );
    END COMPONENT;

    COMPONENT eth_64_to_8_dw_conv
        PORT (
            aclk          : IN STD_LOGIC;
            aresetn       : IN STD_LOGIC;
            s_axis_tvalid : IN STD_LOGIC;
            s_axis_tready : OUT STD_LOGIC;
            s_axis_tdata  : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
            s_axis_tkeep  : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
            s_axis_tlast  : IN STD_LOGIC;
            s_axis_tuser  : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
            m_axis_tvalid : OUT STD_LOGIC;
            m_axis_tready : IN STD_LOGIC;
            m_axis_tdata  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            m_axis_tkeep  : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            m_axis_tlast  : OUT STD_LOGIC;
            m_axis_tuser  : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
        );
    END COMPONENT;

    COMPONENT eth_fifo_64_dc
        PORT (
            s_axis_aresetn : IN STD_LOGIC;
            s_axis_aclk    : IN STD_LOGIC;
            s_axis_tvalid  : IN STD_LOGIC;
            s_axis_tready  : OUT STD_LOGIC;
            s_axis_tdata   : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
            s_axis_tkeep   : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
            s_axis_tlast   : IN STD_LOGIC;
            s_axis_tuser   : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            m_axis_aclk    : IN STD_LOGIC;
            m_axis_tvalid  : OUT STD_LOGIC;
            m_axis_tready  : IN STD_LOGIC;
            m_axis_tdata   : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
            m_axis_tkeep   : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            m_axis_tlast   : OUT STD_LOGIC;
            m_axis_tuser   : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
        );
    END COMPONENT;

    constant GND                  : std_logic                        := '0';
    constant VCC                  : std_logic                        := '1';
    constant ONES_3B              : std_logic_vector(2 downto 0)     := (others => '1');

	-- CRC-32 parameters
	constant NUM_PARITY_BITS_32 : natural := 32;

	-- the checksum polynomial                                                   --  0   4   C   1   1   D   B   7
	constant POLYNOMIAL_32 : std_logic_vector(NUM_PARITY_BITS_32 - 1 downto 0) := "00000100110000010001110110110111";
	constant INIT_STATE_32 : std_logic_vector(NUM_PARITY_BITS_32 - 1 downto 0) := X"FFFFFFFF";

	-- The magic number - the residue                                               C   7   0   4   D   D   7   B
	constant RESIDUE       : std_logic_vector(NUM_PARITY_BITS_32 - 1 downto 0) := "11000111000001001101110101111011";

	constant SEED : std_logic_vector(31 downto 0) := "11010111011110111101100110001011";

	constant DST_ADDR : std_logic_vector(47 downto 0) := X"FFFFFFFFFFFF";
	constant SRC_ADDR  : std_logic_vector(47 downto 0) := X"14FEB5DD9A82";
	constant ETH_HEADER : std_logic_vector(15 downto 0) := X"0600";

	type t_reg_tap is array(7 downto 0) of std_logic_vector(NUM_PARITY_BITS_32 - 1 downto 0);
	signal shift_reg_32 : std_logic_vector(NUM_PARITY_BITS_32 - 1 downto 0) := INIT_STATE_32;
	signal crc_result_int   : std_logic_vector(NUM_PARITY_BITS_32 - 1 downto 0);
	signal crc_final        : std_logic_vector(NUM_PARITY_BITS_32 - 1 downto 0);

	signal mac_gen_data    : std_logic_vector(64 - 1 downto 0);
	signal mac_gen_valid   : std_logic;
	signal mac_gen_last    : std_logic;
	signal mac_gen_empty   : std_logic_vector(3 - 1 downto 0);

	signal mac_gen_data_d    : std_logic_vector(64 - 1 downto 0);
	signal mac_gen_valid_d   : std_logic;
	signal mac_gen_last_d    : std_logic;
	signal mac_gen_empty_d   : std_logic_vector(3 - 1 downto 0);

	signal mac_gen_data_d1   : std_logic_vector(64 - 1 downto 0);
	signal mac_gen_valid_d1  : std_logic;
	signal mac_gen_last_d1   : std_logic;
	signal mac_gen_empty_d1  : std_logic_vector(3 - 1 downto 0);

	signal mac_tx_data_gen    : std_logic_vector(64 - 1 downto 0);
	signal mac_tx_valid_gen   : std_logic;
	signal mac_tx_last_gen    : std_logic;
	signal mac_tx_empty_gen   : std_logic_vector(3 - 1 downto 0);
	signal mac_tx_keep_gen    : std_logic_vector(8 - 1 downto 0);

	signal mac_remain : unsigned(2 downto 0);
	type t_crc_fsm is (FORWARD_DATA, APPEND_LAST, WAIT_CYCLE);
	signal crc_state : t_crc_fsm;
	type t_gen_fsm is (IDLE, ADD_HEADER, ADD_DATA_0, ADD_DATA_1, WAIT_FCS, CAL_FCS, WAIT_CYCLE);
	signal gen_state : t_gen_fsm;

	signal gen_data_reg : std_logic_vector(63 downto 0);
	signal gen_data_out : std_logic_vector(63 downto 0);
	signal rand_in      : std_logic_vector(31 downto 0);
	signal rand_out     : std_logic_vector(31 downto 0);
	
	signal pkt_gen_en_sync_d : std_logic;
	signal pkt_gen_cnt : unsigned(31 downto 0);
	signal pkt_len_gen : std_logic_vector(16 downto 0);
	signal packet_len  : unsigned(13 downto 0);

	signal fixed_packet_len_sync  : std_logic;
	signal packet_len_in_sync     : std_logic_vector(13 downto 0);
	signal num_packet_sync        : std_logic_vector(31 downto 0);
	signal pkt_gen_en_sync        : std_logic;
	signal mux_sel                : std_logic;
	signal pkt_sending            : std_logic;



	signal mac_rx_data    : std_logic_vector(64 - 1 downto 0);
    signal mac_rx_valid   : std_logic;
    signal mac_rx_last    : std_logic;
    signal mac_rx_keep    : std_logic_vector(8 - 1 downto 0);
    signal mac_rx_error   : std_logic;
    signal mac_tx_data    : std_logic_vector(64 - 1 downto 0);
    signal mac_tx_valid   : std_logic;
    signal mac_tx_last    : std_logic;
    signal mac_tx_keep    : std_logic_vector(8 - 1 downto 0);
    signal mac_tx_error   : std_logic;
    signal clk_div8       : std_logic;

    signal tx_axis_tuser_wc     : std_logic_vector(7 downto 0);
    signal tx_fifo_axis_tready  : std_logic; -- ethernet tx axi str
    signal tx_fifo_axis_tvalid  : std_logic;
    signal tx_fifo_axis_tdata   : std_logic_vector(63 downto 0);
    signal tx_fifo_axis_tlast   : std_logic;
    signal tx_fifo_axis_tkeep   : std_logic_vector(7 downto 0);
    signal tx_fifo_axis_tuser   : std_logic;

    signal rx_axis_tuser_wc     : std_logic_vector(7 downto 0);
    signal rx_fifo_axis_tready  : std_logic; -- ethernet tx axi stream interface after CDC FIFO
    signal rx_fifo_axis_tvalid  : std_logic;
    signal rx_fifo_axis_tdata   : std_logic_vector(63 downto 0);
    signal rx_fifo_axis_tlast   : std_logic;
    signal rx_fifo_axis_tkeep   : std_logic_vector(7 downto 0);
    signal rx_fifo_axis_tuser   : std_logic;

    signal resetn               : std_logic;

    signal r_mac_1g_rx_data    : std_logic_vector(8 - 1 downto 0);
    signal r_mac_1g_rx_valid   : std_logic;
    signal r_mac_1g_rx_last    : std_logic;
    signal r_mac_1g_rx_tuser   : std_logic;

    signal r_mac_1g_tx_data    : std_logic_vector(8 - 1 downto 0);
    signal r_mac_1g_tx_valid   : std_logic;
    signal r_mac_1g_tx_last    : std_logic;
    signal r_mac_1g_tx_ready   : std_logic;
    signal r_mac_1g_tx_tuser   : std_logic;

	file in_file  : text; -- text is keyword
	file out_file : text; -- text is keyword

begin
	resetn <= not rst;

    clk_div8_bufg_inst : BUFGCE_DIV
    generic map (
        BUFGCE_DIVIDE   => 8,-- 1-8--ProgrammableInversionAttributes:Specifiesbuilt-inprogrammableinversiononspecificpins
        IS_CE_INVERTED  => '0',-- OptionalinversionforCE
        IS_CLR_INVERTED => '0',-- OptionalinversionforCLR
        IS_I_INVERTED   => '0'-- OptionalinversionforI
    )
    port map (
        O   => clk_div8, -- 1-bitoutput:Buffer
        CE  => VCC, -- 1-bitinput:Bufferenable
        CLR => GND, -- 1-bitinput:Asynchronousclear
        I   => clk    -- 1-bitinput:Buffer
    );
	-- Sync registers
	inst_fixed_packet_len_sync : bit_sync
	port map(
		clk_in   => clk_div8,
		data_in  => fixed_packet_len,
		data_out => fixed_packet_len_sync
	);
	inst_pkt_gen_en_sync : bit_sync
	port map(
		clk_in   => clk_div8,
		data_in  => pkt_gen_en,
		data_out => pkt_gen_en_sync
	);

	gen_packet_len_in_sync : for i in 0 to 13 generate
	begin
		inst_packet_len_in_sync : bit_sync
		port map(
			clk_in   => clk_div8,
			data_in  => packet_len_in(i),
			data_out => packet_len_in_sync(i)
		);
	end generate;
	gen_num_packet_sync : for i in 0 to 31 generate
	begin
		inst_num_packet_sync : bit_sync
		port map(
			clk_in   => clk_div8,
			data_in  => num_packet(i),
			data_out => num_packet_sync(i)
		);
	end generate;

	inst_xxv_ethernet_0_pktprbs_gen : xxv_ethernet_0_pktprbs_gen
	port map(
		ip     => rand_in,
		op     => rand_out,
		datout => gen_data_out
	);

	inst_xxv_ethernet_0_pkt_len_gen : xxv_ethernet_0_pkt_len_gen
	port map(
		clk    => clk_div8,
		reset  => rst,
		enable => '1',
		pkt_len => pkt_len_gen
	);

	mac_tx_keep_gen <= "00000001" when unsigned(mac_tx_empty_gen) = 7 else
	                   "00000011" when unsigned(mac_tx_empty_gen) = 6 else
	                   "00000111" when unsigned(mac_tx_empty_gen) = 5 else
	                   "00001111" when unsigned(mac_tx_empty_gen) = 4 else
	                   "00011111" when unsigned(mac_tx_empty_gen) = 3 else
	                   "00111111" when unsigned(mac_tx_empty_gen) = 2 else
	                   "01111111" when unsigned(mac_tx_empty_gen) = 1 else
	                   "11111111";

	-- MUX control
	pr_pkt_mux : process(clk_div8) is
	begin
	if rising_edge(clk_div8) then
		if rst = '1' then
			mac_tx_data  <= (others => '0');
			mac_tx_valid <= '0';
			mac_tx_last  <= '0';
			mac_tx_keep  <= (others => '0');
			mac_tx_error <= '0';
			mux_sel      <= '0';
			pkt_sending  <= '0';

		else
			if mux_sel = '0' then
				mac_tx_data  <= mac_rx_data;
				mac_tx_valid <= mac_rx_valid;
				mac_tx_last  <= mac_rx_last;
				mac_tx_keep  <= mac_rx_keep;
				mac_tx_error <= mac_rx_error;

				-- always send a complete packet
				if mac_rx_valid = '1' then
					pkt_sending <= '1';
					if mac_rx_last = '1' then
						pkt_sending <= '0';
					end if;
				end if;
				if pkt_sending = '0' and pkt_gen_en_sync = '1' then
					mux_sel <= '1';
				end if;
			else
				mac_tx_data  <= mac_tx_data_gen;
				mac_tx_valid <= mac_tx_valid_gen;
				mac_tx_last  <= mac_tx_last_gen;
				mac_tx_keep  <= mac_tx_keep_gen;
				mac_tx_error <= '0';

				-- always send a complete packet
				if mac_tx_valid_gen = '1' then
					pkt_sending <= '1';
					if mac_tx_last_gen = '1' then
						pkt_sending <= '0';
					end if;
				end if;
				if pkt_sending = '0' and pkt_gen_en_sync = '0' then
					mux_sel <= '0';
				end if;

			end if;
		end if;
	end if;
	end process pr_pkt_mux;

	-- Calcualte the CRC-32 for the baseband data field.
	pr_pkt_gen: process(clk_div8) is
		variable v_feedback_bit   : std_logic;
		variable v_sreg_input_bit : std_logic;
		variable v_shift_reg      : std_logic_vector(NUM_PARITY_BITS_32 - 1 downto 0);
		variable v_input_data     : std_logic_vector(64 - 1 downto 0);
		variable v_shift_reg_tap  : t_reg_tap;
		variable v_mac_gen_empty : unsigned(3 downto 0);
	begin
	if rising_edge(clk_div8) then
		if rst = '1' then
			shift_reg_32    <= INIT_STATE_32;
			crc_result_int  <= (others => '0');
			crc_final       <= (others => '0');
			rand_in         <= SEED;
			mac_gen_data    <= (others => '0');
			mac_gen_empty   <= (others => '0');
			mac_gen_valid   <= '0';
			mac_gen_last    <= '0';

			mac_gen_data_d   <= (others => '0');
			mac_gen_empty_d  <= (others => '0');
			mac_gen_valid_d  <= '0';
			mac_gen_last_d   <= '0';

			mac_gen_data_d1  <= (others => '0');
			mac_gen_empty_d1 <= (others => '0');
			mac_gen_valid_d1 <= '0';
			mac_gen_last_d1  <= '0';
			
			pkt_gen_en_sync_d    <= '0';
			pkt_gen_cnt     <= (others => '0');
			packet_len      <= (others => '0');
			gen_data_reg    <= (others => '0');
			gen_state       <= IDLE;

		else

			mac_gen_data_d  <= mac_gen_data;
			mac_gen_empty_d <= mac_gen_empty;
			mac_gen_valid_d <= mac_gen_valid;
			mac_gen_last_d  <= mac_gen_last;

			mac_gen_data_d1  <= mac_gen_data_d;
			mac_gen_empty_d1 <= mac_gen_empty_d;
			mac_gen_valid_d1 <= mac_gen_valid_d;
			mac_gen_last_d1  <= mac_gen_last_d;

			pkt_gen_en_sync_d <= pkt_gen_en;
			
			v_shift_reg      := shift_reg_32;
			v_feedback_bit   := '0';
			v_sreg_input_bit := '0';
			v_shift_reg_tap  := (others => (others => '0'));

			v_input_data     := mac_gen_data;
			if mac_gen_valid = '1' then
				for i in 0 to 7 loop
					v_shift_reg := crc32(v_input_data((i + 1) * 8 - 1 downto i * 8), v_shift_reg);
					v_shift_reg_tap(i) := v_shift_reg;
				end loop;

				shift_reg_32 <= v_shift_reg;

				if mac_gen_last = '1' then
					shift_reg_32 <= INIT_STATE_32;

					for i in 0 to 7 loop
						if unsigned(mac_gen_empty) = i then
							crc_result_int   <= v_shift_reg_tap(7 - i);
						end if;
					end loop;
				end if;
			end if;

			case gen_state is

			when IDLE => 
				mac_gen_valid <= '0';
				mac_gen_last  <= '0';
				mac_gen_empty <= (others => '0');
				if pkt_gen_en = '1' and pkt_gen_cnt < unsigned(num_packet_sync) then
				    if unsigned(pkt_len_gen(13 downto 0)) > 1500 then
				        packet_len <= to_unsigned(1500, 14);
				    elsif unsigned(pkt_len_gen(13 downto 0)) < 100 then
				        packet_len <= to_unsigned(100, 14);
				    else
				        packet_len <= unsigned(pkt_len_gen(13 downto 0));
				    end if;
					--packet_len <= unsigned(pkt_len_gen(13 downto 0));
					if fixed_packet_len_sync = '1' then
						packet_len <= unsigned(packet_len_in_sync);
					end if;
					gen_state <= ADD_HEADER;
				end if;
				if pkt_gen_en = '1' and pkt_gen_en_sync_d = '0' then
					pkt_gen_cnt <= (others => '0');
				end if;

			when ADD_HEADER =>
				rand_in       <= rand_out;
				gen_data_reg  <= gen_data_out;
				mac_gen_data  <= SRC_ADDR(15 downto 0) & DST_ADDR;
				mac_gen_valid <= '1';
				packet_len    <= packet_len - 8;
				gen_state     <= ADD_DATA_0;

			when ADD_DATA_0 =>
				rand_in       <= rand_out;
				gen_data_reg  <= gen_data_out;
				packet_len    <= packet_len - 8;
				mac_gen_valid <= '1';
				mac_gen_data  <= gen_data_reg(15 downto 0) & ETH_HEADER & SRC_ADDR(47 downto 16);
				gen_state     <= ADD_DATA_1;

			when ADD_DATA_1 =>
				rand_in       <= rand_out;
				gen_data_reg  <= gen_data_out;
				packet_len    <= packet_len - 8;
				mac_gen_valid <= '1';
				mac_gen_data  <= gen_data_reg;

				if packet_len <= 8 then
					mac_gen_last      <= '1';
					v_mac_gen_empty   := 8 - packet_len(3 downto 0);
					mac_gen_empty     <= std_logic_vector(v_mac_gen_empty(2 downto 0));
					gen_state         <= WAIT_FCS;
				end if;

			when WAIT_FCS =>
				mac_gen_valid <= '0';
				mac_gen_last  <= '0';
				mac_gen_empty <= (others => '0');
				gen_state     <= CAL_FCS;

			when CAL_FCS =>
				for i in 0 to 31 loop
					crc_final(i) <= crc_result_int(31 - i) xor '1';
				end loop;
				gen_state <= WAIT_CYCLE;

			when WAIT_CYCLE =>
				gen_state <= IDLE;
				pkt_gen_cnt <= pkt_gen_cnt + 1;

			end case;
		
		end if;
	end if;
	end process pr_pkt_gen;

	-- Calcualte the CRC-32 for the baseband data field.
	pr_add_crc32: process(clk_div8) is
	begin
	if rising_edge(clk_div8) then
		if rst = '1' then
			mac_remain            <= (others => '0');
			mac_tx_data_gen       <= (others => '0');
			mac_tx_valid_gen      <= '0';
			mac_tx_last_gen       <= '0';
			mac_tx_empty_gen      <= (others => '0');
			crc_state             <= FORWARD_DATA;
		else

			case crc_state is

			when FORWARD_DATA =>
				mac_tx_data_gen  <= mac_gen_data_d1;
				mac_tx_valid_gen <= mac_gen_valid_d1;
				mac_tx_last_gen  <= '0';
				mac_tx_empty_gen <= (others => '0');
				if mac_gen_valid_d1 = '1' and mac_gen_last_d1 = '1' then
					if unsigned(mac_gen_empty_d1) >= 4 then
						mac_tx_empty_gen <= std_logic_vector(unsigned(mac_gen_empty_d1) - 4);
						crc_state        <= WAIT_CYCLE;
						mac_tx_last_gen  <= '1';
						for i in 0 to 4 loop
							if unsigned(mac_gen_empty_d1) = 8 - i then
								mac_tx_data_gen((i + 4) * 8 - 1 downto i * 8) <= crc_final;
							end if;
						end loop;
					else
						crc_state        <= APPEND_LAST;
						mac_tx_last_gen  <= '0';
						mac_remain       <= 4 - unsigned(mac_gen_empty_d1);
						for i in 5 to 8 loop
							if unsigned(mac_gen_empty_d1) = (8 - i) then
								mac_tx_data_gen(64 - 1 downto i * 8) <= crc_final((8 - i) * 8 - 1 downto 0);
							end if;
						end loop;
					end if;
				end if;

			when APPEND_LAST =>
				for i in 1 to 4 loop
					if mac_remain = i then
						mac_tx_data_gen(i * 8 - 1 downto 0) <= crc_final(32 - 1 downto (4 - i) * 8);
						mac_tx_empty_gen <= std_logic_vector(to_unsigned(8 - i, 3));
					end if;
				end loop;
				mac_tx_valid_gen <= '1';
				mac_tx_last_gen  <= '1';
				crc_state        <= WAIT_CYCLE;

			when WAIT_CYCLE =>
				mac_tx_empty_gen <= (others => '0');
				mac_tx_valid_gen <= '0';
				mac_tx_last_gen  <= '0';
				crc_state        <= FORWARD_DATA;

			end case;
		end if;
	end if;
	end process pr_add_crc32;

   
  
   
   
  
    -- data-width conversion from 64 to 8 for mac_tx and 8 to 64 for mac_rx data
    eth_tx_cdc_fifo_64_inst : eth_fifo_64_dc
        port map  (
            s_axis_aresetn  => resetn, -- : IN STD_LOGIC;
            s_axis_aclk     => clk_div8, -- : IN STD_LOGIC;
            s_axis_tvalid   => mac_tx_valid, -- : IN STD_LOGIC;
            s_axis_tready   => open, -- : OUT STD_LOGIC;
            s_axis_tdata    => mac_tx_data, -- : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
            s_axis_tkeep    => mac_tx_keep, -- : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
            s_axis_tlast    => mac_tx_last, -- : IN STD_LOGIC;
            s_axis_tuser(0) => mac_tx_error, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            m_axis_aclk     => clk, -- : IN STD_LOGIC;
            m_axis_tvalid   => tx_fifo_axis_tvalid, -- : OUT STD_LOGIC;
            m_axis_tready   => tx_fifo_axis_tready, -- : IN STD_LOGIC;
            m_axis_tdata    => tx_fifo_axis_tdata, -- : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
            m_axis_tkeep    => tx_fifo_axis_tkeep, -- : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            m_axis_tlast    => tx_fifo_axis_tlast, -- : OUT STD_LOGIC;
            m_axis_tuser(0) => tx_fifo_axis_tuser  -- : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
        );

    tx_axis_tuser_wc <= (others => tx_fifo_axis_tuser); -- tuser width adjustment

    eth_tx_64_to_8_dw_conv_inst :  eth_64_to_8_dw_conv
        port map (
            aclk             => clk,             --: IN STD_LOGIC;
            aresetn          => resetn,                  --: IN STD_LOGIC;
            s_axis_tvalid    => tx_fifo_axis_tvalid,     --: IN STD_LOGIC;
            s_axis_tready    => tx_fifo_axis_tready,     --: OUT STD_LOGIC;
            s_axis_tdata     => tx_fifo_axis_tdata,      --: IN STD_LOGIC_VECTOR(63 DOWNTO 0);
            s_axis_tkeep     => tx_fifo_axis_tkeep,      --: IN STD_LOGIC_VECTOR(7 DOWNTO 0);
            s_axis_tlast     => tx_fifo_axis_tlast,      --: IN STD_LOGIC;
            s_axis_tuser     => tx_axis_tuser_wc,        --: IN STD_LOGIC_VECTOR(7 DOWNTO 0);
            m_axis_tvalid    => r_mac_1g_tx_valid,         --: OUT STD_LOGIC;
            m_axis_tready    => r_mac_1g_tx_ready,         --: IN STD_LOGIC;
            m_axis_tdata     => r_mac_1g_tx_data,          --: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            m_axis_tkeep     => open ,                   --: OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            m_axis_tlast     => r_mac_1g_tx_last,          --: OUT STD_LOGIC;
            m_axis_tuser(0)  => r_mac_1g_tx_tuser          --: OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
        );

    eth_rx_8_to_64_dw_conv_inst : eth_8_to_64_dw_conv
        port map (
            aclk             => clk, -- : IN STD_LOGIC;
            aresetn          => resetn, -- : IN STD_LOGIC;
            s_axis_tvalid    => r_mac_1g_rx_valid, -- : IN STD_LOGIC;
            s_axis_tready    => open, -- : OUT STD_LOGIC;
            s_axis_tdata     => r_mac_1g_rx_data, -- : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
            s_axis_tkeep(0)  => r_mac_1g_rx_valid, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            s_axis_tlast     => r_mac_1g_rx_last, -- : IN STD_LOGIC;
            s_axis_tuser(0)  => r_mac_1g_rx_tuser, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            m_axis_tvalid    => rx_fifo_axis_tvalid, -- : OUT STD_LOGIC;
            m_axis_tready    => rx_fifo_axis_tready, -- : IN STD_LOGIC;
            m_axis_tdata     => rx_fifo_axis_tdata, -- : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
            m_axis_tkeep     => rx_fifo_axis_tkeep, -- : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            m_axis_tlast     => rx_fifo_axis_tlast, -- : OUT STD_LOGIC;
            m_axis_tuser     => rx_axis_tuser_wc    -- : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
        );
    rx_fifo_axis_tuser <= or_reduce(rx_axis_tuser_wc); -- any bit of tuser going high should be sforwarded

    eth_rx_cdc_fifo_64_inst : eth_fifo_64_dc
        port map  (
            s_axis_aresetn    => resetn, -- : IN STD_LOGIC;
            s_axis_aclk       => clk, -- : IN STD_LOGIC;
            s_axis_tvalid     => rx_fifo_axis_tvalid, -- : IN STD_LOGIC;
            s_axis_tready     => rx_fifo_axis_tready, -- : OUT STD_LOGIC;
            s_axis_tdata      => rx_fifo_axis_tdata, -- : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
            s_axis_tkeep      => rx_fifo_axis_tkeep, -- : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
            s_axis_tlast      => rx_fifo_axis_tlast, -- : IN STD_LOGIC;
            s_axis_tuser(0)   => rx_fifo_axis_tuser, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            m_axis_aclk       => clk_div8, -- : IN STD_LOGIC;
            m_axis_tvalid     => mac_rx_valid, -- : OUT STD_LOGIC;
            m_axis_tready     => VCC, -- : IN STD_LOGIC;
            m_axis_tdata      => mac_rx_data, -- : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
            m_axis_tkeep      => mac_rx_keep, -- : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            m_axis_tlast      => mac_rx_last, -- : OUT STD_LOGIC;
            m_axis_tuser(0)   => mac_rx_error  -- : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
        );

	r_mac_1g_rx_data    <= mac_1g_rx_data;
	r_mac_1g_rx_valid   <= mac_1g_rx_valid;
	r_mac_1g_rx_last    <= mac_1g_rx_last;
	r_mac_1g_rx_tuser   <= mac_1g_rx_tuser;

	mac_1g_tx_data    <= r_mac_1g_tx_data;
	mac_1g_tx_valid   <= r_mac_1g_tx_valid;
	mac_1g_tx_last    <= r_mac_1g_tx_last;
	r_mac_1g_tx_ready <= mac_1g_tx_ready;
	mac_1g_tx_tuser   <= r_mac_1g_tx_tuser;

	-------------------------------------------
	-- record STIMULI input
	-------------------------------------------
	file_open(in_file, "mac_packet_gen_tx.txt", write_mode);
	pr_input : process(clk) is
		variable v_byte_cnt : natural := 0;
		variable v_frame_id : std_logic_vector(15 downto 0);
		variable v_frame_id_nat : natural;
		variable v_error_cnt : natural := 0;
		variable v_frame_cnt : natural := 0;
		variable v_line_out  : line;
		variable v_seq_num : natural := 0;
		variable v_payload_len : natural := 0;
		variable v_packet_len : natural := 0;
		variable v_row          : line;
        variable v_data_write : integer;
        constant header:    string := "------------PACKET START------------";
        variable packets_ok : natural := 0;

		
	begin
	

	if(rising_edge(clk)) then
	
		if rst = '1' then 
          v_byte_cnt := 0;
          packets_ok := 0;
--      		write(v_row,header);	
--        	writeline(l_file,v_row);  
            --m_axis_tready <= '1';    
		else
	       --m_axis_tready <= '1';
	      if r_mac_1g_tx_last = '1' and r_mac_1g_tx_valid = '1' and r_mac_1g_tx_ready = '1' then
--	            packets_ok := packets_ok + 1;
--		      	assert false report "Packet Received! " & str(v_byte_cnt+1) & " Bytes - Total Packets OK: " & str(packets_ok) & "." 
--				severity failure;
				write(v_row,hstr(r_mac_1g_tx_data));
				writeline(in_file,v_row);
				writeline(in_file,v_row);
				write(v_row,header);	
                writeline(in_file,v_row);   
				v_byte_cnt := 0;
				
		  elsif r_mac_1g_tx_valid = '1' and r_mac_1g_tx_ready = '1' then
		      write(v_row,hstr(r_mac_1g_tx_data));
		      --write(v_row,header);
		      --writeline(l_file,v_row); 
		      v_byte_cnt := v_byte_cnt + 1;
		      if (v_byte_cnt mod 4) = 0 then
		          writeline(in_file,v_row);       
--		      elsif v_byte_cnt = 15 then
--		          writeline(l_file,v_row);              
		      end if;
		        
		  end if;
		end if;
    end if;
	end process;	
	

	-------------------------------------------
	-- record OUTPUT of deframer 
	-------------------------------------------
	file_open(out_file, "mac_packet_gen_rx.txt", write_mode);
	pr_output_deframer : process(clk) is
		variable v_byte_cnt : natural := 0;
		variable v_frame_id : std_logic_vector(15 downto 0);
		variable v_frame_id_nat : natural;
		variable v_error_cnt : natural := 0;
		variable v_frame_cnt : natural := 0;
		variable v_line_out  : line;
		variable v_seq_num : natural := 0;
		variable v_payload_len : natural := 0;
		variable v_packet_len : natural := 0;
		variable v_row          : line;
        variable v_data_write : integer;
        constant header:    string := "------------PACKET START------------";
        variable packets_ok : natural := 0;

		
	begin
	

	if(rising_edge(clk)) then
	
		if rst = '1' then 
          v_byte_cnt := 0;
          packets_ok := 0;
--      		write(v_row,header);	
--        	writeline(l_file,v_row);  
            --m_axis_tready <= '1';    
		else
	       --m_axis_tready <= '1';
	      if r_mac_1g_rx_last = '1' and r_mac_1g_rx_valid = '1' then --and rx_axis_tready = '1' then
--	            packets_ok := packets_ok + 1;
--		      	assert false report "Packet Received! " & str(v_byte_cnt+1) & " Bytes - Total Packets OK: " & str(packets_ok) & "." 
--				severity failure;
				write(v_row,hstr(r_mac_1g_rx_data));
				writeline(out_file,v_row);
				writeline(out_file,v_row);
				write(v_row,header);	
                writeline(out_file,v_row);   
				v_byte_cnt := 0;
				
		  elsif r_mac_1g_rx_valid = '1' then --and rx_axis_tready = '1' then
		      write(v_row,hstr(r_mac_1g_rx_data));
		      --write(v_row,header);
		      --writeline(l_file,v_row); 
		      v_byte_cnt := v_byte_cnt + 1;
		      if (v_byte_cnt mod 4) = 0 then
		          writeline(out_file,v_row);       
--		      elsif v_byte_cnt = 15 then
--		          writeline(l_file,v_row);              
		      end if;
		        
		  end if;
		end if;
    end if;
	end process;
	



















end arch;

