----------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Charles
-- 
-- Create Date: 12/20/2022 03:30:27 PM
-- Design Name: 
-- Module Name: scrambler_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library frame_encoding;
use frame_encoding.pkg_support.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity scrambler_tb is
generic(

	-- clock period
	CLK_PERIOD     : time    := 5.000 ns;

	-- Number of frames to simulate
	NUM_FRAME   : natural := 30;

	-- Which level has an assertion when it fails?
	ASSERTION_SEVERITY  : severity_level := FAILURE

	);
end scrambler_tb;

architecture Behavioral of scrambler_tb is

    constant FRAME_BYTE_SIZE : natural :=  1176;-- (1176, 1368, 1512, 1704, 2232);
	constant FRAME_SIZE : natural := FRAME_BYTE_SIZE ;     -- 1176, 1368, 1512, 1704, 2232);
	constant TEST_DATA_BLOCK_SIZE : natural := (FRAME_BYTE_SIZE + 8) *100;
	
    signal clk                 : std_logic := '0';
    signal rst                 : std_logic;
    signal test_enable        	: std_logic;
    signal test_start        	: std_logic;
    signal test_finished       : std_logic;

    signal tx_scrambler_in_ready : std_logic;
    signal tx_scrambler_in_valid : std_logic;
    signal tx_scrambler_in_last  : std_logic;
    signal tx_scrambler_in_data  : std_logic_vector(8 - 1 downto 0);

    signal tx_scrambler_out_ready : std_logic;
    signal tx_scrambler_out_valid : std_logic;
    signal tx_scrambler_out_last  : std_logic;
    signal tx_scrambler_out_data  : std_logic_vector(8 - 1 downto 0);
    
    signal rx_scrambler_in_ready : std_logic;
    signal rx_scrambler_in_valid : std_logic;
    signal rx_scrambler_in_last  : std_logic;
    signal rx_scrambler_in_data  : std_logic_vector(8 - 1 downto 0);

    signal rx_scrambler_out_ready : std_logic;
    signal rx_scrambler_out_valid : std_logic;
    signal rx_scrambler_out_last  : std_logic;
    signal rx_scrambler_out_data  : std_logic_vector(8 - 1 downto 0);

begin

------------
	-- component instantiation
	-------------------------------------------

	-- Scrambler
	dtu_out : entity frame_encoding.scrambling
	port map(
		clk => clk,
		rst => rst,

		input_ready => tx_scrambler_in_ready,
		input_valid => tx_scrambler_in_valid,
		input_last  => tx_scrambler_in_last,
		input_data  => tx_scrambler_in_data,

		output_ready => rx_scrambler_in_ready,
		output_valid => tx_scrambler_out_valid,
		output_last  => tx_scrambler_out_last,
		output_data  => tx_scrambler_out_data
	);
    
    
    dtu_in : entity frame_encoding.scrambling
	port map(
		clk => clk,
		rst => rst,

		input_ready => rx_scrambler_in_ready,
		input_valid => tx_scrambler_out_valid,
		input_last  => tx_scrambler_out_last,
		input_data  => tx_scrambler_out_data,

		output_ready => rx_scrambler_out_ready,
		output_valid => rx_scrambler_out_valid,
		output_last  => rx_scrambler_out_last,
		output_data  => rx_scrambler_out_data
	);



-------------------------------------------
	-- stimuli generation
	-------------------------------------------

	pr_stimuli: process
		variable v_block_cnt        : integer := 0;
		variable v_insert_invalid   : boolean := false;
		variable v_byte_cnt         : natural;
		variable v_frame_cnt        : natural;
		variable v_frame_cnt_word   : std_logic_vector(15 downto 0);
		variable  data_bit_shift : std_logic_vector(15 downto 0);
       
	begin	  
		-- reset the system
		rst                   <= '1';

		-- configuration
		tx_scrambler_out_ready <= '0';
        rx_scrambler_out_ready <= '0';

        v_frame_cnt  := 0;
		
		wait for 5 * CLK_PERIOD;

		-- release the reset
		rst         <= '0';
		tx_scrambler_in_last  <= '1';
		tx_scrambler_in_valid <= '0';
		
		wait for 5 * CLK_PERIOD;

		-- send data of one block
		for byte_id in 0 to TEST_DATA_BLOCK_SIZE loop
		
		    if tx_scrambler_in_ready = '1' then
                v_frame_cnt  := v_frame_cnt + 1;
            end if;
            tx_scrambler_in_valid <= '1';  
            tx_scrambler_in_last <= '0'; 
             
            rx_scrambler_out_ready <= '1';
            
            tx_scrambler_in_data <= std_logic_vector(to_unsigned(v_frame_cnt,8));      
            if v_frame_cnt = (FRAME_BYTE_SIZE ) then
                 if rx_scrambler_out_ready = '1' then
                    rx_scrambler_in_last <= '1';
                    v_frame_cnt := 0;
                end if;               
            elsif (v_frame_cnt = 0) and (rx_scrambler_out_ready = '1') then          
                tx_scrambler_in_valid <= '0';        
            end if;
				---wait until s_axis_tvalid = '1' and rising_edge(clk);
				
		  wait until falling_edge(clk);
		
		end loop;

		wait;
	end process pr_stimuli;

	

	-------------------------------------------
	-- clock generation
	-------------------------------------------
	clk <= not clk after CLK_PERIOD / 2;
	-------------------------------------------
	-- fso_phy:
	-------------------------------------------



end Behavioral;
