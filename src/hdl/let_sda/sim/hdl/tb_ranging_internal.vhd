--!
--! Copyright (C) 2021 Mynaric GmbH
--!
--! @file
--! @author Gustavo Martin
--! @date   2021/06/14
--! @brief  Loopback testbench for ranging
--!
--!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;

library frame_encoding;
library frame_decoding;
--use frame_decoding.pkg_components.all;
use frame_encoding.pkg_support.all;
use frame_encoding.pkg_components.all;
library ethernet_framer_lib;
library ethernet_deframer_lib;
use frame_decoding.txt_util.all;
library ranging_lib;
library sda_lib;
use sda_lib.pkg_sda.all;
--use ranging_lib.package_components.all;

entity tb_ranging_internal is
	generic(

	-- clock period
	CLK_PERIOD     : time    := 5.000 ns;

	-- Number of frames to simulate
	NUM_FRAME   : natural := 30;

	-- Which level has an assertion when it fails?
	ASSERTION_SEVERITY  : severity_level := FAILURE

	);
end tb_ranging_internal;


architecture sim of tb_ranging_internal is

	constant preamble : std_logic_vector(71 downto 0) := X"77AD5B5843641E2E26";
	constant FRAME_SIZE  : natural := 239 * 8 + 14 - 4;
	signal clk                 : std_logic := '0';
	signal clk200              : std_logic := '0';
	signal mac_rx_clk_out      : std_logic := '0';
	signal rst                 : std_logic;
	signal reset_sync_200      : std_logic;
	signal mac_rx_reset        : std_logic;

	signal disp_sum        : std_logic_vector(23 downto 0);

	signal tx_ts_tx  		: std_logic_vector(40-1 downto 0);
	signal tx_ts_valid_tx  	: std_logic;

	-- configuration for the preamble sync algorithm
	signal cfg_aquisition_length  : std_logic_vector(32 - 1 downto 0);
	signal cfg_tracking_length    : std_logic_vector(32 - 1 downto 0);
	signal cfg_max_tracking_error : std_logic_vector(32 - 1 downto 0);
	
	signal framer_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal framer_axis_tvalid   : std_logic;
	signal framer_axis_tlast    : std_logic;
	signal framer_axis_tready   : std_logic;
	signal deframer_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal deframer_axis_tvalid   : std_logic;
	signal deframer_axis_tlast    : std_logic;
	signal deframer_axis_tready   : std_logic;	
	signal filter_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal filter_axis_tvalid   : std_logic;
	signal filter_axis_tlast    : std_logic;
	signal filter_axis_tready   : std_logic;
	
	signal ch_tx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal ch_tx_axis_tvalid   : std_logic;
	signal ch_tx_axis_tlast    : std_logic;
	signal ch_tx_axis_tready   : std_logic := '0';
	signal ch_rx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal ch_rx_axis_tdata_noise   : std_logic_vector(8 - 1 downto 0);
	signal ch_rx_axis_tvalid   : std_logic;
	signal ch_rx_axis_tready   : std_logic := '0';

	signal loopback_ch_tx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal loopback_ch_tx_axis_tvalid   : std_logic;
	signal loopback_ch_tx_axis_tvalid_mux   : std_logic;
	signal loopback_ch_tx_axis_tlast    : std_logic;
	signal loopback_ch_tx_axis_tready   : std_logic := '0';
	signal loopback_ch_rx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal loopback_ch_rx_axis_tdata_noise   : std_logic_vector(8 - 1 downto 0);
	signal loopback_ch_rx_axis_tvalid   : std_logic;
	signal loopback_ch_rx_axis_tready   : std_logic := '0';
	signal loopback_ch_tx_axis_tready_mux   : std_logic := '0';
	signal ready_ctrl   : std_logic := '0';

	signal tx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal tx_axis_tvalid   : std_logic;
	signal tx_axis_tlast    : std_logic;
	signal tx_axis_tready   : std_logic := '0';

	signal rx_axis_tdata    : std_logic_vector(8 - 1 downto 0);
	signal rx_axis_tvalid   : std_logic;
	signal rx_axis_tlast    : std_logic;
	signal rx_axis_tready   : std_logic;

	signal crc16_valid       : std_logic;
	signal crc16_correct     : std_logic;
	signal crc32_valid       : std_logic;
	signal crc32_correct     : std_logic;
	signal payload_cerr      : std_logic_vector(4 - 1 downto 0);
	signal payload_ncerr     : std_logic;
	signal payload_err_valid : std_logic;
	signal header_cerr       : std_logic_vector(4 - 1 downto 0);
	signal header_ncerr      : std_logic;
	signal header_err_valid  : std_logic;

	-- More diagnostic informations
	-------------------------------------------------------
	signal clear_stat          : std_logic := '0';
	signal total_frame_counter : std_logic_vector(31 downto 0);
	signal crc_error_counter   : std_logic_vector(31 downto 0);
	signal crc16_err_counter   : std_logic_vector(31 downto 0);
	signal crc32_err_counter   : std_logic_vector(31 downto 0);
	signal header_corrections  : std_logic_vector(31 downto 0);
	signal payload_corrections : std_logic_vector(31 downto 0);
	signal lpc_frame_counter   : std_logic_vector(31 downto 0);
	signal lpc_corrections     : std_logic_vector(31 downto 0);

		-- Stats from the preamble sync
	signal new_search_counter  : std_logic_vector(31 downto 0);
	signal sync_loss_counter   : std_logic_vector(31 downto 0);

	signal byte_cnt         : natural;
	signal got_frame_id     : std_logic;


	signal clk10     : std_logic := '0';
	signal rst_clk10 : std_logic;

	signal tx_reset  : std_logic;
	signal rx_reset  : std_logic;

	-- // Differential reference clock inputs
	signal mgtrefclk0_x0y4_p : std_logic := '0';
	signal mgtrefclk0_x0y4_n : std_logic := '0';

	-- // Serial data ports for transceiver channel 0
	signal ch0_gthrxn_in  : std_logic;
	signal ch0_gthrxp_in  : std_logic;
	signal ch0_gthtxn_out : std_logic;
	signal ch0_gthtxp_out : std_logic;

	signal data_good_in   : std_logic;
	signal data_good_in_d : std_logic;

	signal let_timestamp_tx        : std_logic_vector(40-1 downto 0);
	signal let_timestamp_valid_tx  : std_logic;
	signal let_timestamp_rd_en_tx  : std_logic;
	signal let_timestamp_rx        : std_logic_vector(40-1 downto 0);
	signal let_timestamp_valid_rx  : std_logic;
	signal let_timestamp_rd_en_rx  : std_logic;
 
	signal tx_ts         : std_logic_vector(40-1 downto 0);
	signal tx_ts_valid   : std_logic;
	signal tx_ts_rd_en   :  std_logic;

	        -- From Framer
        ---------------------- 
	signal tx_timing_data          :  tx_timing_data_t;
	signal tx_timing_data_valid    :  std_logic;

	-- From deframer
	---------------------- 
	signal rx_timing_data          :  rx_timing_data_t;
	signal rx_timing_data_valid    :  std_logic;

	-- Output data: Time of flight in picoseconds
	----------------------
	signal tof_counter_valid   :  std_logic;
	signal tof_counter         :  std_logic_vector(40-1 downto 0);
	signal tof_counter_f  : natural;
	signal tof_ns : natural;
	signal transport_control : natural := 0;
	signal transport_vector_p : std_logic_vector(3 downto 0);
	signal transport_vector_n : std_logic_vector(3 downto 0);
	signal enable_link        : std_logic := '1';

	signal debug_tx_ts        : std_logic_vector(40-1 downto 0);
	signal debug_rx_ts        : std_logic_vector(40-1 downto 0);
	signal debug_valid        : std_logic;
	signal reset_timer        : std_logic;

-- fso_phy_sim:
	component gtwizard_fso_1g_top is
		port (
	
			-- // Differential reference clock inputs
			mgtrefclk0_x0y4_p : in  std_logic;
			mgtrefclk0_x0y4_n : in  std_logic;
	
			-- // Serial data ports for transceiver channel 0
			ch0_gthrxn_in  : in  std_logic;
			ch0_gthrxp_in  : in  std_logic;
			ch0_gthtxn_out : out std_logic;
			ch0_gthtxp_out : out std_logic;
	
			-- // User-provided ports for reset helper block(s)
			hb_gtwiz_reset_clk_freerun_in : in std_logic;
			hb_gtwiz_reset_all_in         : in std_logic;
	
			-- // User data interface
			rx_reset_out          : out std_logic;
			rx_usrclk2_out        : out std_logic;
			gtwiz_userdata_rx_out : out std_logic_vector(15 downto 0);
			data_good_in          : in  std_logic;
	
			tx_reset_out         : out std_logic;
			tx_usrclk2_out       : out std_logic;
			gtwiz_userdata_tx_in : in  std_logic_vector(15 downto 0);
	
			-- // status and control signal - synced to the freerun clock
			link_down_latched_reset_in : in  std_logic;
			link_status_out            : out std_logic;
			link_down_latched_out      : out std_logic;
			init_done_out              : out std_logic;
			init_retry_ctr_out         : out std_logic_vector(3 downto 0);
			gtpowergood_out            : out std_logic;
			txprgdivresetdone_out      : out std_logic;
			rxprgdivresetdone_out      : out std_logic;
			txpmaresetdone_out         : out std_logic;
			rxpmaresetdone_out         : out std_logic;
			gtwiz_reset_tx_done_out    : out std_logic;
			gtwiz_reset_rx_done_out    : out std_logic;
	
			hb_gtwiz_reset_all_axi_in                  : in  std_logic;
			hb0_gtwiz_reset_tx_pll_and_datapath_axi_in : in  std_logic_vector(0 downto 0);
			hb0_gtwiz_reset_tx_datapath_axi_in         : in  std_logic_vector(0 downto 0);
			hb_gtwiz_reset_rx_pll_and_datapath_axi_in  : in  std_logic;
			hb_gtwiz_reset_rx_datapath_axi_in          : in  std_logic;
			link_down_latched_reset_axi_in             : in  std_logic
		);
	end component gtwizard_fso_1g_top;
	
	signal txfifo_tdata  : std_logic_vector(15 downto 0);
	signal txfifo_tvalid : std_logic;
	signal txfifo_tready : std_logic;

	signal rxfifo_tdata  : std_logic_vector(15 downto 0);
	signal rxfifo_tvalid : std_logic;
	signal rxfifo_tready : std_logic;

	signal hb_gtwiz_reset_clk_freerun_in : std_logic;
	signal hb_gtwiz_reset_all_in         : std_logic;

	signal rx_reset_int         : std_logic;
	signal rx_reset_out         : std_logic;
	signal rx_usrclk2_out       : std_logic;
	signal gtwiz_userdata_rx_out :  std_logic_vector(15 downto 0);

	signal tx_reset_int         : std_logic;
	signal tx_reset_out         : std_logic;
	signal tx_usrclk2_out       : std_logic;
	signal gtwiz_userdata_tx_in : std_logic_vector(15 downto 0);

	signal link_down_latched_reset_in : std_logic := '0';
	signal link_status_out            : std_logic;
	signal link_down_latched_out      : std_logic;
	signal init_done_out              : std_logic;
	signal init_retry_ctr_out         : std_logic_vector(3 downto 0);
	signal gtpowergood_out            : std_logic;
	signal txprgdivresetdone_out      : std_logic;
	signal rxprgdivresetdone_out      : std_logic;
	signal txpmaresetdone_out         : std_logic;
	signal rxpmaresetdone_out         : std_logic;
	signal gtwiz_reset_tx_done_out    : std_logic;
	signal gtwiz_reset_rx_done_out    : std_logic;

	signal hb_gtwiz_reset_all_axi_in                  : std_logic := '0';
	signal hb0_gtwiz_reset_tx_pll_and_datapath_axi_in : std_logic_vector(0 downto 0) := "0";
	signal hb0_gtwiz_reset_tx_datapath_axi_in         : std_logic_vector(0 downto 0) := "0";
	signal hb_gtwiz_reset_rx_pll_and_datapath_axi_in  : std_logic := '0';
	signal hb_gtwiz_reset_rx_datapath_axi_in          : std_logic := '0';
	signal link_down_latched_reset_axi_in             : std_logic := '0';

begin

	-------------------------------------------
	-- component instantiation
	-------------------------------------------

	UUT :  entity frame_decoding.frame_fec_top
	port map(
		clk            => clk,
		rst            => (rst or tx_reset),

		preamble        => preamble,

		cfg_aquisition_length  => cfg_aquisition_length,
		cfg_tracking_length    => cfg_tracking_length,
		cfg_max_tracking_error => cfg_max_tracking_error,

		-- TX data input - header and payload data
		----------------------
		tx_axis_tdata      => framer_axis_tdata,
		tx_axis_tvalid     => framer_axis_tvalid,
		tx_axis_tlast      => framer_axis_tlast,
		tx_axis_tready     => framer_axis_tready,

		disp_sum           => disp_sum,

		-- Output data handling
		-----------------------
		ch_tx_axis_tdata     => tx_axis_tdata,
		ch_tx_axis_tvalid    => tx_axis_tvalid,
		ch_tx_axis_tlast     => tx_axis_tlast,
		ch_tx_axis_tready    => tx_axis_tready,


		-- RX data input
		----------------------
		ch_rx_axis_tdata      => rx_axis_tdata,  --tx_axis_tdata, --rx_axis_tdata,
		ch_rx_axis_tvalid     => rx_axis_tvalid, --tx_axis_tvalid, --rx_axis_tvalid,

		-- Status
		-----------------------
		crc16_valid       => crc16_valid,
		crc16_correct     => crc16_correct,
		crc32_valid       => crc32_valid,
		crc32_correct     => crc32_correct,

		payload_cerr      => payload_cerr,
		payload_ncerr     => payload_ncerr,
		payload_err_valid => payload_err_valid,

		header_cerr       => header_cerr,
		header_ncerr      => header_ncerr,
		header_err_valid  => header_err_valid,

		preamble_synced   => data_good_in,

		-- More diagnostic informations
		-------------------------------------------------------
		clear_stat          => clear_stat,
		total_frame_counter => total_frame_counter,
		crc_error_counter   => crc_error_counter,
		crc16_err_counter   => crc16_err_counter,
		crc32_err_counter   => crc32_err_counter,
		header_corrections  => header_corrections,
		payload_corrections => payload_corrections,
		lpc_frame_counter   => lpc_frame_counter,
		lpc_corrections     => lpc_corrections,

		-- Stats from the preamble sync
		new_search_counter  => new_search_counter,
		sync_loss_counter   => sync_loss_counter,

		-- RX data output - header and payload data
		-----------------------
		rx_axis_tdata     => filter_axis_tdata,
		rx_axis_tvalid    => filter_axis_tvalid,
		rx_axis_tlast     => filter_axis_tlast,
		rx_axis_tready    => filter_axis_tready
	);

--	inst_axi4s_fifo : axi4s_fifo
--	-- inst_axi4s_fifo : entity frame_encoding.axi4s_fifo
--	generic map(
--		DISTR_RAM         => true,
--		FIFO_DEPTH        => 16384,
--		DATA_WIDTH        => 8,
--		FULL_THRESHOLD    => 16384,
--		EMPTY_THRESHOLD   => 4,
--		USE_OUTPUT_BUFFER => false
--	)
--	port map(
--
--		clk            => clk,
--		rst            => rst,
--
--		-- Input data handling
--		----------------------
--		input             => tx_axis_tdata,
--		input_valid       => tx_axis_tvalid,
--		input_last        => tx_axis_tlast,
--		input_ready       => tx_axis_tready,
--		input_almost_full  => open,
--		input_almost_empty => open,
--		fifo_max_level     => open,
--
--		-- Output data handling
--		-----------------------
--		output          => rx_axis_tdata,
--		output_valid    => rx_axis_tvalid,
--		output_last     => rx_axis_tlast,
--		output_ready    => rx_axis_tready
--	);

	-- FSO PHY(for simulaion only)
--	inst_phy:  entity frame_decoding.fso_phy_sim
--	port map(
--		clk   => clk,
--		rst   => rst,
--
--		clk10     => clk10,
--		rst_clk10 => rst_clk10,
--
--		tx_reset  => tx_reset,
--		rx_reset  => rx_reset,
--
--		-- // Differential reference clock inputs
--		mgtrefclk0_x0y4_p => mgtrefclk0_x0y4_p,
--		mgtrefclk0_x0y4_n => mgtrefclk0_x0y4_n,
--
--		-- // Serial data ports for transceiver channel 0
--		ch0_gthrxn_in  => ch0_gthrxn_in,
--		ch0_gthrxp_in  => ch0_gthrxp_in,
--		ch0_gthtxn_out => ch0_gthtxn_out,
--		ch0_gthtxp_out => ch0_gthtxp_out,
--
--		tx_axis_tdata  => ch_tx_axis_tdata,
--		tx_axis_tvalid => ch_tx_axis_tvalid,
--		tx_axis_tready => ch_tx_axis_tready,
--
--		rx_axis_tdata  => ch_rx_axis_tdata,
--		rx_axis_tvalid => ch_rx_axis_tvalid,
--
--		data_good_in   => data_good_in
--	);

    -- instantiate Framer here
	ethernet_framer : entity ethernet_framer_lib.ethernet_framer_top 
	generic map (
		MINIMUN_FREE_BYTES_TO_SPLIT => 64
	)
	port map(
		clk_let        => clk200, -- : in  std_logic;
		rst_let        => (reset_sync_200 or tx_reset or rx_reset), -- : in  std_logic;
		clk_eth        => mac_rx_clk_out, -- : in  std_logic;
		rst_eth        => mac_rx_reset, -- : in  std_logic;
		-- Input data 
		----------------------
		s_axis_tdata     => (others=>'0'),  -- : in  std_logic_vector(8 - 1 downto 0);
		s_axis_tvalid    => '0', -- : in  std_logic;
		s_axis_tlast     => '0',  -- : in  std_logic;
		s_axis_tready    => open, -- : out std_logic;
		s_axis_tuser     => '0', -- : out std_logic;

		cfg_pause_data        => x"0100", -- TBD
		tx_pause_valid        => open,
		tx_pause_data         => open,
		rx_fifo_skipped_frame => open,

		-- Control (subject to change in the future)
		---------------------- 
		tx_ts         => let_timestamp_tx,
		tx_ts_valid   => let_timestamp_valid_tx,
		tx_ts_rd_en   => let_timestamp_rd_en_tx,

        -- To frame tof calculation (subject to change in the future)
        ---------------------- 
        tx_timing_data          => tx_timing_data,
        tx_timing_data_valid    => tx_timing_data_valid,

		-- Output data 
		----------------------
		m_axis_tdata  => framer_axis_tdata  , -- : out  std_logic_vector(8 - 1 downto 0);
		m_axis_tvalid => framer_axis_tvalid , -- : out std_logic;
		m_axis_tlast  => framer_axis_tlast  , -- : out std_logic;
		m_axis_tready => framer_axis_tready , -- : in std_logic

		-- Statistics
		----------------------            
		clear_stat          => '0',
		framer_diagnostics  => open

	);

    Inst_frame_filter_top : entity ethernet_deframer_lib.frame_filter_top
    PORT map(
      clk 						=> clk200,
      rst 						=> reset_sync_200,
      s_axis_tvalid 			=> filter_axis_tvalid,
      s_axis_tready 			=> filter_axis_tready,
      s_axis_tdata 				=> filter_axis_tdata,
      s_axis_tlast 				=> filter_axis_tlast,
      m_axis_tvalid 			=> deframer_axis_tvalid,
      m_axis_tready 			=> deframer_axis_tready,
      m_axis_tdata 				=> deframer_axis_tdata,
      m_axis_tlast 				=> deframer_axis_tlast,
      crc16_valid 				=> crc16_valid,
      crc16_correct 			=> crc16_correct,
      crc32_valid 				=> crc32_valid,
      crc32_correct 			=> crc32_correct,
      packet_cnt_in	            => open,
      packet_cnt_out	        => open
    );

    ethernet_deframer: entity ethernet_deframer_lib.ethernet_deframer_top
    port map(
        clk_let              => clk200,
        rst_let              => reset_sync_200,
        clk_eth              => clk200,
        rst_eth              => reset_sync_200,
        s_axis_tdata         => deframer_axis_tdata,
        s_axis_tvalid        => deframer_axis_tvalid,
        s_axis_tlast         => deframer_axis_tlast, 
        s_axis_tready        => deframer_axis_tready, 
        m_axis_tdata         => open,
        m_axis_tvalid        => open,
        m_axis_tlast         => open,
        m_axis_tready        => '1',

		ranging_sync_delay   => x"3060",
		ranging_sync_period  => x"103C",
		        -- Control (subject to change in the future)
        ---------------------- 
        rx_ts         		 => let_timestamp_rx,
        rx_ts_valid   		 => let_timestamp_valid_rx,
        --rx_ts_rd_en   	     => let_timestamp_rd_en_rx,
        preamble_sync 		 => data_good_in,
		reset_timer 		 => reset_timer,
        -- To frame tof calculation (subject to change in the future)
        ---------------------- 
        rx_timing_data          => rx_timing_data,
        rx_timing_data_valid    => rx_timing_data_valid,
		-- Statistics
		----------------------
		clear_stat           => '0',
        deframer_diagnostics => open,
        sda_debug            => open
    );


	inst_frame_timing_detector_top : entity ranging_lib.frame_timing_detector_top
		Port map( 
			clk_let           => clk200,
			rst_let           => reset_sync_200,
			clk_tx            => tx_usrclk2_out,
			clk_rx            => tx_usrclk2_out,--rx_usrclk2_out,
			rst_tx            => tx_reset_out,--(tx_reset_out or rx_reset_out),
			rst_rx            => tx_reset_out,--(tx_reset_out or rx_reset_out),
	
			-- Input data from FSO PHY
			----------------------
			s_axis_data_tx     => gtwiz_userdata_tx_in,
			s_axis_valid_tx    => '1',
			s_axis_data_rx     => gtwiz_userdata_tx_in,--gtwiz_userdata_rx_out,
			s_axis_valid_rx    => '1',
			reset_timer 	   => reset_timer,
			-- Output data 
			----------------------
			let_timestamp_tx        => let_timestamp_tx, 
			let_timestamp_valid_tx  => let_timestamp_valid_tx, 
			let_timestamp_rd_en_tx  => let_timestamp_rd_en_tx,
			let_timestamp_rx        => let_timestamp_rx, 
			let_timestamp_valid_rx  => let_timestamp_valid_rx,
			let_timestamp_rd_en_rx  => '1' --let_timestamp_rd_en_rx
			);

	inst_frame_tof_calc : entity ranging_lib.frame_tof_calc
		Port map( 
			clk            			=> clk200,
			rst            			=> reset_sync_200,
	
	        preamble_synced         => data_good_in,
			-- From Framer
			---------------------- 
			tx_timing_data          => tx_timing_data,
			tx_timing_data_valid    => tx_timing_data_valid,
	
			-- From deframer
			---------------------- 
			rx_timing_data          => rx_timing_data,
			rx_timing_data_valid    => rx_timing_data_valid,
	
			-- Output data: Time of flight in picoseconds
			----------------------
			tof_counter_valid   			=> tof_counter_valid,
			tof_counter         			=> tof_counter,
			-- Debug
			----------------------
			debug_tx_ts             => debug_tx_ts,
			debug_rx_ts             => debug_rx_ts,
			debug_valid             => debug_valid
		);
    tof_counter_f <= natural(to_integer(unsigned(tof_counter)));
    tof_ns <= tof_counter_f/16;
	-------------------------------------------
	-- stimuli generation
	-------------------------------------------

	pr_stimuli: process
	begin

		-- reset the system
		rst              <= '1';
		rst_clk10        <= '1';
		reset_sync_200   <= '1';
		mac_rx_reset     <= '1';
		rx_axis_tready <= '0';
		wait for 200 * CLK_PERIOD;

		-- release the reset
		rst         	 <= '0';
		rst_clk10   	 <= '0';
		reset_sync_200   <= '0';
		mac_rx_reset     <= '0';
		wait for 5 * CLK_PERIOD;
		wait for 22 us;
		rx_axis_tready <= '1';
		wait;

	end process;

	mgtrefclk0_x0y4_p <= not mgtrefclk0_x0y4_p after 8.333 ns;
	mgtrefclk0_x0y4_n <= not mgtrefclk0_x0y4_p;

	-- Serial loopback
	--ch0_gthrxp_in <= transport ch0_gthtxp_out after 0.833*1 ns; --after 0.833*11 ns;
	--ch0_gthrxn_in <= transport ch0_gthtxn_out after 0.833*1 ns; --after 0.833*11 ns;
	ch0_gthrxp_in <=  ch0_gthtxp_out when enable_link = '1' else '0'; --after 0.833*11 ns;
	ch0_gthrxn_in <=  ch0_gthtxn_out when enable_link = '1' else '1'; --after 0.833*11 ns;
	
	
--	transport_vector_p(0) <= transport ch0_gthtxp_out after 0.833*1 ns; --after 0.833*4 ns;
--	transport_vector_n(0) <= transport ch0_gthtxn_out after 0.833*1 ns; --after 0.833*4 ns;
	
--	transport_vector_p(1) <= transport ch0_gthtxp_out after 0.833*2 ns; --after 0.833*4 ns;
--	transport_vector_n(1) <= transport ch0_gthtxn_out after 0.833*2 ns; --after 0.833*4 ns;
	
--	transport_vector_p(2) <= transport ch0_gthtxp_out after 0.833*3 ns; --after 0.833*4 ns;
--	transport_vector_n(2) <= transport ch0_gthtxn_out after 0.833*3 ns; --after 0.833*4 ns;
	
--	transport_vector_p(3) <= transport ch0_gthtxp_out after 0.833*4 ns; --after 0.833*4 ns;
--	transport_vector_n(3) <= transport ch0_gthtxn_out after 0.833*4 ns; --after 0.833*4 ns;
	
--	ch0_gthrxp_in <= transport ch0_gthtxp_out after 13.4 ns; --after 0.833*4 ns;
--	ch0_gthrxn_in <= transport ch0_gthtxn_out after 13.4 ns; --after 0.833*4 ns;
--    ch0_gthrxp_in <= ch0_gthtxp_out when transport_control = 0 else 
--                    transport_vector_p(0) when transport_control = 1 else
--                     transport_vector_p(1) when transport_control = 2 else
--                     transport_vector_p(2) when transport_control = 3 else
--                     transport_vector_p(3) when transport_control = 4 else   
--                     transport_vector_p(3);
--    ch0_gthrxn_in <= ch0_gthtxn_out when transport_control = 0 else
--                transport_vector_n(0) when transport_control = 1 else
--                 transport_vector_n(1) when transport_control = 2 else
--                 transport_vector_n(2) when transport_control = 3 else
--                 transport_vector_n(3) when transport_control = 4 else   
--                 transport_vector_n(3);
	
	process
	begin
       enable_link <= '1';
       wait for 500 us;
       enable_link <= '0';
       wait for 200 us;
       enable_link <= '1';
       wait for 523 us;
       enable_link <= '0';
       wait for 255 us;
       enable_link <= '1';
       wait for 501 us;
       enable_link <= '0';
       wait for 212 us;
       enable_link <= '1';
       wait for 510 us;
       enable_link <= '0';
       wait for 232 us;
       enable_link <= '1';
       wait;
--	   transport_control <= 0;
--	   wait for 300 us;
--	   transport_control <= 1;
--	   wait for 300 us;
--	   transport_control <= 2;
--	   wait for 300 us;
--	   transport_control <= 3;
--	   wait for 300 us;
--	   transport_control <= 4;
--	   wait for 300 us;	   
	   
	   wait;
	end process;
	
	-- insert some bit flips
	ch_rx_axis_tdata_noise <= (others => '0') when (byte_cnt > 60000 and byte_cnt < 90000) else -- Signal loss
	                          (ch_rx_axis_tdata xor "01000000") when (byte_cnt mod  5) = 0 else -- single bit error
	                          (ch_rx_axis_tdata xor "11110000") when (byte_cnt mod 81) = 0 else -- multiple bit error
	                           ch_rx_axis_tdata;


	-- Configuration for the preamble sync
	cfg_aquisition_length  <= std_logic_vector(to_unsigned(4, 32));
	cfg_tracking_length    <= std_logic_vector(to_unsigned(16, 32));
	cfg_max_tracking_error <= std_logic_vector(to_unsigned(4, 32));

	

	-------------------------------------------
	-- clock generation
	-------------------------------------------
	clk <= not clk after CLK_PERIOD / 2;
	clk10 <= not clk10 after 10 ns;
	clk200 <= not clk200 after CLK_PERIOD / 2;
	mac_rx_clk_out <= not mac_rx_clk_out after CLK_PERIOD / 2;
	-------------------------------------------
	-- fso_phy:
	-------------------------------------------
	hb_gtwiz_reset_all_in <= rst_clk10;
	hb_gtwiz_reset_clk_freerun_in <= clk10;

	tx_reset <= tx_reset_out;
	rx_reset <= rx_reset_out;

	rx_reset_int <= rst or rx_reset_out;
	tx_reset_int <= rst or tx_reset_out;

	inst_8_to_16 : entity frame_decoding.dwidth_8_to_16
	port map(
		aclk    => clk,
		aresetn => (not tx_reset_int),

		s_axis_tdata  => tx_axis_tdata,
		s_axis_tvalid => tx_axis_tvalid,
		s_axis_tready => tx_axis_tready,

		m_axis_tdata  => txfifo_tdata,
		m_axis_tvalid => txfifo_tvalid,
		m_axis_tready => txfifo_tready
	);

	inst_tx_fifo : entity frame_decoding.dc_fifo_phy
	port map(
		m_aclk        => tx_usrclk2_out,
		s_aclk        => clk,
		s_aresetn     => (not tx_reset_int),

		s_axis_tdata  => txfifo_tdata,
		s_axis_tvalid => txfifo_tvalid,
		s_axis_tready => txfifo_tready,

		m_axis_tdata  => gtwiz_userdata_tx_in,
		m_axis_tvalid => open,
		m_axis_tready => '1'
	);

	inst_rx_fifo : entity frame_decoding.dc_fifo_phy
	port map(
		m_aclk        => clk,
		s_aclk        => tx_usrclk2_out, --rx_usrclk2_out,
		s_aresetn     => (not tx_reset_int),--(not rx_reset_out),

		s_axis_tdata  => gtwiz_userdata_tx_in, --gtwiz_userdata_rx_out,
		s_axis_tvalid => '1',
		s_axis_tready => open,

		m_axis_tdata  => rxfifo_tdata,
		m_axis_tvalid => rxfifo_tvalid,
		m_axis_tready => rxfifo_tready
	);

	inst_16_to_8 : entity frame_decoding.dwidth_16_to_8
	port map(
		aclk    => clk,
		aresetn => (not rx_reset_int),

		s_axis_tdata  => rxfifo_tdata,
		s_axis_tvalid => rxfifo_tvalid,
		s_axis_tready => rxfifo_tready,

		m_axis_tdata  => rx_axis_tdata,
		m_axis_tvalid => rx_axis_tvalid,
		m_axis_tready => '1'
	);

	hb_gtwiz_reset_all_axi_in <= hb_gtwiz_reset_all_in;
	hb0_gtwiz_reset_tx_datapath_axi_in(0) <= hb_gtwiz_reset_all_in;
	hb_gtwiz_reset_rx_datapath_axi_in <= hb_gtwiz_reset_all_in;
	hb0_gtwiz_reset_tx_pll_and_datapath_axi_in(0) <= hb_gtwiz_reset_all_in;
	hb_gtwiz_reset_rx_pll_and_datapath_axi_in <= hb_gtwiz_reset_all_in;


	inst_gtwizard_fso_1g_top : gtwizard_fso_1g_top
	-- inst_gtwizard_fso_1g_top : entity frame_decoding.gtwizard_fso_1g_top
	port map(

		mgtrefclk0_x0y4_p => mgtrefclk0_x0y4_p,
		mgtrefclk0_x0y4_n => mgtrefclk0_x0y4_n,

		ch0_gthrxn_in  => ch0_gthrxn_in,
		ch0_gthrxp_in  => ch0_gthrxp_in,
		ch0_gthtxn_out => ch0_gthtxn_out,
		ch0_gthtxp_out => ch0_gthtxp_out,

		hb_gtwiz_reset_clk_freerun_in=> hb_gtwiz_reset_clk_freerun_in,
		hb_gtwiz_reset_all_in        => hb_gtwiz_reset_all_in,

		rx_reset_out          => rx_reset_out,
		rx_usrclk2_out        => rx_usrclk2_out,
		gtwiz_userdata_rx_out => gtwiz_userdata_rx_out,
		data_good_in          => data_good_in,

		tx_reset_out         => tx_reset_out,
		tx_usrclk2_out       => tx_usrclk2_out,
		gtwiz_userdata_tx_in => gtwiz_userdata_tx_in,

		link_down_latched_reset_in => link_down_latched_reset_in,
		link_status_out            => link_status_out,
		link_down_latched_out      => link_down_latched_out,
		init_done_out              => init_done_out,
		init_retry_ctr_out         => init_retry_ctr_out,
		gtpowergood_out            => gtpowergood_out,
		txprgdivresetdone_out      => txprgdivresetdone_out,
		rxprgdivresetdone_out      => rxprgdivresetdone_out,
		txpmaresetdone_out         => txpmaresetdone_out,
		rxpmaresetdone_out         => rxpmaresetdone_out,
		gtwiz_reset_tx_done_out    => gtwiz_reset_tx_done_out,
		gtwiz_reset_rx_done_out    => gtwiz_reset_rx_done_out,

		hb_gtwiz_reset_all_axi_in                  => hb_gtwiz_reset_all_axi_in,
		hb0_gtwiz_reset_tx_pll_and_datapath_axi_in => hb0_gtwiz_reset_tx_pll_and_datapath_axi_in,
		hb0_gtwiz_reset_tx_datapath_axi_in         => hb0_gtwiz_reset_tx_datapath_axi_in,
		hb_gtwiz_reset_rx_pll_and_datapath_axi_in  => hb_gtwiz_reset_rx_pll_and_datapath_axi_in,
		hb_gtwiz_reset_rx_datapath_axi_in          => hb_gtwiz_reset_rx_datapath_axi_in,
		link_down_latched_reset_axi_in             => link_down_latched_reset_axi_in
	);

-- second FSO for Looping with delay to test ranging

--	-- FSO PHY(for simulaion only)
--	inst_phy:  entity frame_decoding.fso_phy_sim
--	port map(
--		clk   => clk200,
--		rst   => reset_sync_200,

--		clk10     => clk10,
--		rst_clk10 => rst_clk10,

--		tx_reset  => tx_reset,
--		rx_reset  => rx_reset,

--		-- // Differential reference clock inputs
--		mgtrefclk0_x0y4_p => mgtrefclk0_x0y4_p,
--		mgtrefclk0_x0y4_n => mgtrefclk0_x0y4_n,

--		-- // Serial data ports for transceiver channel 0
--		ch0_gthrxn_in  => ch0_gthtxn_out,  --ch0_gthrxn_in,
--		ch0_gthrxp_in  => ch0_gthtxp_out,  --ch0_gthrxp_in,
--		ch0_gthtxn_out => ch0_gthrxn_in, --ch0_gthtxn_out,
--		ch0_gthtxp_out => ch0_gthrxp_in, --ch0_gthtxp_out,

--		tx_axis_tdata  => loopback_ch_tx_axis_tdata,
--		tx_axis_tvalid => loopback_ch_tx_axis_tvalid_mux,
--		tx_axis_tready => loopback_ch_tx_axis_tready,

--		rx_axis_tdata  => loopback_ch_rx_axis_tdata,
--		rx_axis_tvalid => loopback_ch_rx_axis_tvalid,

--		data_good_in   => '1'
--	);

--	inst_axi4s_fifo : axi4s_fifo
--	-- inst_axi4s_fifo : entity frame_encoding.axi4s_fifo
--	generic map(
--		DISTR_RAM         => true,
--		FIFO_DEPTH        => 131072,
--		DATA_WIDTH        => 8,
--		FULL_THRESHOLD    => 131072,
--		EMPTY_THRESHOLD   => 16,
--		USE_OUTPUT_BUFFER => false
--	)
--	port map(

--		clk            => clk200,
--		rst            => reset_sync_200,

--		-- Input data handling
--		----------------------
--		input             => loopback_ch_rx_axis_tdata,
--		input_valid       => loopback_ch_rx_axis_tvalid,
--		input_last        => '0',
--		input_ready       => open,
--		input_almost_full  => open,
--		input_almost_empty => open,
--		fifo_max_level     => open,

--		-- Output data handling
--		-----------------------
--		output          => loopback_ch_tx_axis_tdata,
--		output_valid    => loopback_ch_tx_axis_tvalid,
--		output_last     => open,
--		output_ready    => loopback_ch_tx_axis_tready_mux
--	);
	
	loopback_ch_tx_axis_tready_mux <= loopback_ch_tx_axis_tready when ready_ctrl = '1' else '0';
	loopback_ch_tx_axis_tvalid_mux <= loopback_ch_tx_axis_tvalid when ready_ctrl = '1' else '1'; 
	
	
	process
	begin
	   ready_ctrl <= '0';
	   wait for 270 us;
	   wait until rising_edge(clk200);
	   
	   ready_ctrl <= '0';
	   wait for 10 us;
	   wait for 500 us; --new test
	   wait until rising_edge(clk200);
	   
	   ready_ctrl <= '1';
	   ----------
	   
	   wait for 1500 us;
	   wait until rising_edge(clk200);
	   
	   ready_ctrl <= '0';
	   wait for 25 us;
	   wait until rising_edge(clk200);
	   
	   ready_ctrl <= '1';
	   
	   ----------
	   
	   wait for 1500 us;
	   wait until rising_edge(clk200);   
	   ready_ctrl <= '0';
	   wait for 50 us;
	   wait until rising_edge(clk200);
	   ready_ctrl <= '1';	   
	   
	   ----------
	   
	   wait for 1500 us;
	   wait until rising_edge(clk200);   
	   ready_ctrl <= '0';
	   wait for 100 us;
	   wait until rising_edge(clk200);
	   ready_ctrl <= '1';	
	   
	   ----------
	   
	   wait for 1500 us;
	   wait until rising_edge(clk200);   
	   ready_ctrl <= '0';
	   wait for 200 us;
	   wait until rising_edge(clk200);
	   ready_ctrl <= '1';	
	   
	   ----------
	   
	   wait for 1500 us;
	   wait until rising_edge(clk200);   
	   ready_ctrl <= '0';
	   wait for 700 us;
	   wait until rising_edge(clk200);
	   ready_ctrl <= '1';
	   
	   
	   wait;
	
	end process;

end architecture sim;
