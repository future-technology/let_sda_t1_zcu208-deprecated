-- standard packages
library ieee;
use ieee.std_logic_1164.all;  
use ieee.numeric_std.all;     
use ieee.std_logic_misc.all;
use ieee.math_real.all;
-- Xilinx components
library unisim;
use unisim.vcomponents.all;

library let_sda_lib;
use let_sda_lib.let_pckg.all;
use let_sda_lib.let_pckg_components.all;

-- LET top-level testbench

entity top_let_2_tb is 
end top_let_2_tb;

architecture tb of top_let_2_tb is  
  constant LET_CLK_PERIOD    : time := 6.4 ns;
  constant AXI_CLK_PERIOD    : time := 10.0 ns;
  constant RESET_TIME        : time := 20000.0 ns;

  signal clk200_p            : std_logic  := '0';
  signal clk200_n            : std_logic  := '1';
  signal reset               : std_logic  := '0'; 
  signal let_nom_clk_p       : std_logic  := '0'; 
  signal let_nom_clk_n       : std_logic  := '1';
  signal let_red_clk_p       : std_logic  := '0';
  signal let_red_clk_n       : std_logic  := '1';
  
  signal eth_gt0_txp         : std_logic;     
  signal eth_gt0_txn         : std_logic;     
  signal eth_gt0_rxp         : std_logic;     
  signal eth_gt0_rxn         : std_logic;     
  signal aur_gt0_txp         : std_logic;     
  signal aur_gt0_txn         : std_logic;     
  signal aur_gt0_rxp         : std_logic;     
  signal aur_gt0_rxn         : std_logic;     
  signal uart_rxd            : std_logic;     
  signal uart_txd            : std_logic;     
  signal sys_clk_nom_en      : std_logic;     
  signal sys_clk_red_en      : std_logic;     
  signal let_clk_nom_en      : std_logic;     
  signal let_clk_red_en      : std_logic;     
  signal en_fso_lvl_trs      : std_logic;     
  signal fso_lim_dis_1_1v8      : std_logic; 
  signal fso_lim_dis_2_1v8      : std_logic; 

  --mac packet generator
  signal mac_1g_rx_data    : std_logic_vector(8 - 1 downto 0);
  signal mac_1g_rx_valid   : std_logic;
  signal mac_1g_rx_last    : std_logic;
  signal mac_1g_rx_tuser   : std_logic;
  signal mac_1g_tx_data    : std_logic_vector(8 - 1 downto 0);
  signal mac_1g_tx_valid   : std_logic;
  signal mac_1g_tx_last    : std_logic;
  signal mac_1g_tx_ready   : std_logic;
  signal mac_1g_tx_tuser   : std_logic;
  signal fixed_packet_len  : std_logic;
  signal packet_len_in     : std_logic_vector(13 downto 0);
  signal num_packet        : std_logic_vector(31 downto 0);
  signal pkt_gen_en        : std_logic := '0';
  signal reset_pkt_gen     : std_logic;

  signal clk100          : std_logic := '0';
  signal reset_sync      : std_logic := '0';
  signal ref_clk_50      : std_logic := '0';
  signal let_refclk_nom          : std_logic := '0';

  signal mac_rx_clk_out : std_logic;
  signal mac_rx_reset   : std_logic;
  signal mac_tx_clk_out : std_logic;
  signal mac_tx_reset   : std_logic;

  signal mac_tx_resetn  : std_logic;
  signal mac_rx_resetn  : std_logic;


  signal mac_mux_data    : std_logic_vector(8 - 1 downto 0);
  signal mac_mux_valid   : std_logic;
  signal mac_mux_last    : std_logic;
  signal mac_mux_error   : std_logic;

  signal s_axi_awaddr     : std_logic_vector(32 - 1 downto 0);
  signal s_axi_awvalid    : std_logic;
  signal s_axi_awready    : std_logic;
  signal s_axi_wdata      : std_logic_vector(32 - 1 downto 0);
  signal s_axi_aresetn    : std_logic := '0';


begin
  -- clocks and resets etc. 
  let_refclk_nom <= not let_refclk_nom after LET_CLK_PERIOD/2; -- disable nominal clock for this test.
  let_nom_clk_p  <= not let_nom_clk_p after LET_CLK_PERIOD/2;
  let_nom_clk_n  <= not let_nom_clk_n after LET_CLK_PERIOD/2;
  let_red_clk_p  <= not let_red_clk_p after LET_CLK_PERIOD/2;
  let_red_clk_n  <= not let_red_clk_n after LET_CLK_PERIOD/2;
  reset          <= '1', '0'          after RESET_TIME; -- reset
  reset_sync     <= '1', '0'          after RESET_TIME; -- reset
  clk200_p       <= not clk200_p      after AXI_CLK_PERIOD/2; -- TCC/LET 100 MHz refclk
  clk200_n       <= not clk200_n      after AXI_CLK_PERIOD/2;
  clk100         <= not clk100        after AXI_CLK_PERIOD/2;
  ref_clk_50     <= not ref_clk_50        after AXI_CLK_PERIOD/4;


--  -- ethernet GT loopback or near-end PMA loopback in GT 
--  eth_gt0_rxp    <= eth_gt0_txp; --  after 500 ns; 
--  eth_gt0_rxn    <= eth_gt0_txn; --  after 500 ns; 
--  -- fso side loopback
aur_gt0_rxp    <= aur_gt0_txp; --  after 500 ns; 
aur_gt0_rxn    <= aur_gt0_txn; --  after 500 ns; 
uart_rxd       <= uart_txd;    -- UART Loopback


  -- instantiate DUT
  dut_let : entity let_sda_lib.top_let
  	port map(
  	  clk200_p         => clk200_p         , --   : in std_logic; -- 100 MHz clock from board, I know about the naming ambiguity
      clk200_n         => clk200_n         , --   : in std_logic;
      reset            => reset            , --   : in std_logic; -- system level reset for the LET system
      let_nom_clk_p    => let_nom_clk_p    , --   : in std_logic; -- GT reference clocks nominal and redundant
      let_nom_clk_n    => let_nom_clk_n    , --   : in std_logic;
      let_red_clk_p    => let_red_clk_p    , --   : in std_logic;
      let_red_clk_n    => let_red_clk_n    , --   : in std_logic;
      eth_gt0_txp      => eth_gt0_txp      , --   : out std_logic;  -- the GT serial link I/O pins for ethernet all 4 channels are connected to pins
      eth_gt0_txn      => eth_gt0_txn      , --   : out std_logic;
      eth_gt0_rxp      => eth_gt0_rxp      , --   : in std_logic;
      eth_gt0_rxn      => eth_gt0_rxn      , --   : in std_logic;
      aur_gt0_txp      => aur_gt0_txp      , --   : out std_logic; -- the GT serial link I/O pins for FSO PHY, wave B in use 
      aur_gt0_txn      => aur_gt0_txn      , --   : out std_logic; 
      aur_gt0_rxp      => aur_gt0_rxp      , --   : in std_logic;
      aur_gt0_rxn      => aur_gt0_rxn      , --   : in std_logic;
      uart_rxd         => uart_rxd         , --   : in std_logic;
      uart_txd         => uart_txd         , --   : out std_logic;
      sys_clk_nom_en   => sys_clk_nom_en   , --   : out std_logic;
      sys_clk_red_en   => sys_clk_red_en   , --   : out std_logic;
      let_clk_nom_en   => let_clk_nom_en   , --   : out std_logic;
      let_clk_red_en   => let_clk_red_en   , --   : out std_logic;
      en_fso_lvl_trs   => en_fso_lvl_trs   , --   : out std_logic;
      fso_lim_dis_1_1v8 => fso_lim_dis_1_1v8,
      fso_lim_dis_2_1v8 => fso_lim_dis_1_1v8
  	);
    
  -- LET GT refclk buffer 
  -- instantiate the diff to single-ended clock input buffers for the reference clocks
--  let_refclk_buf_inst : entity let_sda_lib.let_ref_clk_buf 
--      port map (
--          let_clk_nom_p         => let_nom_clk_p ,
--          let_clk_nom_n         => let_nom_clk_n,
--          let_clk_red_p         => let_red_clk_p,
--          let_clk_red_n         => let_red_clk_n,
--          let_gt_refclk_nom     => let_refclk_nom,
--          let_gt_refclk_red     => let_refclk_red,
--          let_gt_refclk_nom_out => let_gt_refclk_nom_out, -- BUFG'ed versions of nom and red refclk
--          let_gt_refclk_red_out => let_gt_refclk_red_out
--      );
--      );

  Inst_eth_1g_top_test : entity let_sda_lib.eth_1g_top
      port map (
          reset                    => reset_sync  ,
          ref_clk_50               => ref_clk_50  ,
          
          let_refclk_nom           => let_refclk_nom                 ,

          eth_gt0_txp              => eth_gt0_rxp                    ,
          eth_gt0_txn              => eth_gt0_rxn                    ,
          
          eth_gt0_rxp              => eth_gt0_txp                    ,
          eth_gt0_rxn              => eth_gt0_txn                    ,
                      
          s_axi_aclk                => clk100                         ,
          s_axi_aresetn             => s_axi_aresetn                           ,

          s_axi_awaddr              => s_axi_awaddr               ,
          s_axi_awvalid             => s_axi_awvalid                           ,
          s_axi_awready             => s_axi_awready                           ,
          s_axi_wdata               => s_axi_wdata               ,
          s_axi_wstrb               => (others => '1')               ,
          s_axi_wvalid              => s_axi_awvalid                           ,
          s_axi_wready              => open                           ,
          s_axi_bresp               => open                          ,
          s_axi_bvalid              => open                           ,
          s_axi_bready              => s_axi_awvalid                             ,
          s_axi_araddr              => (others => '0')               ,
          s_axi_arvalid             => '0'                           ,
          s_axi_arready             => open                           ,
          s_axi_rdata               => open               ,
          s_axi_rresp               => open                           ,
          s_axi_rvalid              => open                           ,
          s_axi_rready              => '0'                           ,
  
          rx_reset_in              => '0'                             ,
          rx_user_rst_out          => mac_rx_reset                      , --mac_rx_reset                   ,

          rx_clk_out               => mac_rx_clk_out                 ,

          rx_axis_tvalid           => mac_1g_rx_valid                  ,
          rx_axis_tdata            => mac_1g_rx_data                   ,
          rx_axis_tlast            => mac_1g_rx_last                   ,
          rx_axis_tuser            => mac_1g_rx_tuser                  ,

          tx_reset_in              => '0'                             ,
          tx_user_rst_out          => mac_tx_reset                   ,

          tx_clk_out               => mac_tx_clk_out                 ,

          tx_axis_tready           => mac_1g_tx_ready                 ,
          tx_axis_tvalid           => mac_1g_tx_valid                 ,
          tx_axis_tdata            => mac_1g_tx_data                  ,
          tx_axis_tlast            => mac_1g_tx_last                  ,
          tx_axis_tuser            => mac_1g_tx_tuser                 ,

          ctl_tx_pause_req_0       => (others => '0')                 ,   
          ctl_tx_resend_pause_0    => '0' 
      );

    pr_aux_eth_init : process is

    begin 
    s_axi_aresetn <= '0';
    s_axi_awvalid <= '0';
    wait for 30 ns;
    s_axi_aresetn <= '1';

    s_axi_awvalid <= '0';
    wait for 50 us;
    wait until rising_edge(clk100);
    s_axi_awaddr <= X"44a00500";
    s_axi_awvalid <= '1';
    s_axi_wdata <= X"00000068";
    wait until s_axi_awready = '1';
    wait until rising_edge(clk100);
    s_axi_awvalid <= '0';

    wait for 400 ns;
    wait until rising_edge(clk100);
    s_axi_awaddr <= X"44a00508";
    s_axi_awvalid <= '1';
    s_axi_wdata <= X"00004140";
    wait until s_axi_awready = '1';
    wait until rising_edge(clk100);
    s_axi_awvalid <= '0';

    wait for 400 ns;
    wait until rising_edge(clk100);
    s_axi_awaddr <= X"44a00504";
    s_axi_awvalid <= '1';
    s_axi_wdata <= X"07004800";
    wait until s_axi_awready = '1';
    wait until rising_edge(clk100);
    s_axi_awvalid <= '0'; --

    wait for 40 us;
    wait until rising_edge(clk100);
    s_axi_awaddr <= X"44a00508";
    s_axi_awvalid <= '1';
    s_axi_wdata <= X"00000140";
    wait until s_axi_awready = '1';
    wait until rising_edge(clk100);
    s_axi_awvalid <= '0';

    wait for 400 ns;
    wait until rising_edge(clk100);
    s_axi_awaddr <= X"44a00504";
    s_axi_awvalid <= '1';
    s_axi_wdata <= X"01004800";
    wait until s_axi_awready = '1';
    wait until rising_edge(clk100);
    s_axi_awvalid <= '0';

    wait for 400 ns;
    wait until rising_edge(clk100);
    s_axi_awaddr <= X"44a00404";
    s_axi_awvalid <= '1';
    s_axi_wdata <= X"92000000";
    wait until s_axi_awready = '1';
    wait until rising_edge(clk100);
    s_axi_awvalid <= '0';

    wait for 400 ns;
    wait until rising_edge(clk100);
    s_axi_awaddr <= X"44a00408";
    s_axi_awvalid <= '1';
    s_axi_wdata <= X"90000000";
    wait until s_axi_awready = '1';
    wait until rising_edge(clk100);
    s_axi_awvalid <= '0';

    wait for 400 ns;
    wait until rising_edge(clk100);
    s_axi_awaddr <= X"44a00708";
    s_axi_awvalid <= '1';
    s_axi_wdata <= X"80000000";
    wait until s_axi_awready = '1';
    wait until rising_edge(clk100); --
    s_axi_awvalid <= '0';

    wait for 400 ns;
    wait until rising_edge(clk100);
    s_axi_awaddr <= X"44a00404";
    s_axi_awvalid <= '1';
    s_axi_wdata <= X"92000000";
    wait until s_axi_awready = '1';
    wait until rising_edge(clk100);
    s_axi_awvalid <= '0';

    wait for 400 ns;
    wait until rising_edge(clk100);
    s_axi_awaddr <= X"44a00408";
    s_axi_awvalid <= '1';
    s_axi_wdata <= X"90000000";
    wait until s_axi_awready = '1';
    wait until rising_edge(clk100);
    s_axi_awvalid <= '0';

    wait for 400 ns;
    wait until rising_edge(clk100);
    s_axi_awaddr <= X"44a00708";
    s_axi_awvalid <= '1';
    s_axi_wdata <= X"80000000";
    wait until s_axi_awready = '1';
    wait until rising_edge(clk100);
    s_axi_awvalid <= '0';
    wait;


    end process;


    fixed_packet_len  <= '0';
    packet_len_in     <= std_logic_vector(to_unsigned(234, 14));
    num_packet        <= std_logic_vector(to_unsigned(100, 32));       
    --pkt_gen_en        <= '0', '1' after 350 us;  
    reset_pkt_gen <= mac_rx_reset or mac_tx_reset;     
    
    proc_ctrl_pkt_gen_en: process is
    begin
        wait until rising_edge(mac_tx_clk_out);
        pkt_gen_en <= '0';
        wait for 350 us;
        wait until rising_edge(mac_tx_clk_out);
        pkt_gen_en <= '1';
        wait for 100 us;
        wait;
        wait until rising_edge(mac_tx_clk_out);
        pkt_gen_en <= '0';
--        wait for 300 us;
--        wait until rising_edge(mac_tx_clk_out);
--        pkt_gen_en <= '1';
--        wait for 10 us;
--        wait until rising_edge(mac_tx_clk_out);
--        pkt_gen_en <= '0';
        wait;

    end process;

    -- sim related sources
    mac_packet_gen_inst : entity let_sda_lib.mac_packet_gen 
      port map (
        clk     => mac_tx_clk_out, -- : in  std_logic;
        rst     => reset_pkt_gen, -- : in  std_logic;    
        mac_1g_rx_data   => mac_1g_rx_data   , --  : in  std_logic_vector(8 - 1 downto 0);
        mac_1g_rx_valid  => mac_1g_rx_valid  , --  : in  std_logic;
        mac_1g_rx_last   => mac_1g_rx_last   , --  : in  std_logic;
        mac_1g_rx_tuser  => mac_1g_rx_tuser  , --  : in  std_logic;    
        mac_1g_tx_data   => mac_1g_tx_data   , --  : out std_logic_vector(8 - 1 downto 0);
        mac_1g_tx_valid  => mac_1g_tx_valid  , --  : out std_logic;
        mac_1g_tx_last   => mac_1g_tx_last   , --  : out std_logic;
        mac_1g_tx_ready  => mac_1g_tx_ready  , --  : in  std_logic;
        mac_1g_tx_tuser  => mac_1g_tx_tuser  , --  : out std_logic;    
        fixed_packet_len => fixed_packet_len , --  : in  std_logic;
        packet_len_in    => packet_len_in    , --  : in  std_logic_vector(13 downto 0);
        num_packet       => num_packet       , --  : in  std_logic_vector(31 downto 0);
        pkt_gen_en       => pkt_gen_en         --  : in  std_logic
      );


end tb;   
