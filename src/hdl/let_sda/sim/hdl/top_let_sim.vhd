-- LET SDA top-level design file

-- Author: Umar Farooq Zia, umar.zia@mynaric.com

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library let_sda_lib;
use let_sda_lib.let_pckg.all;
use let_sda_lib.let_pckg_components.all;

library frame_encoding;
--use frame_encoding.pkg_support.all;
--use frame_encoding.pkg_components.all;

library frame_decoding;
--use frame_decoding.pkg_support.all;
--use frame_decoding.pkg_types.all;
--use frame_decoding.pkg_components.all;

library ethernet_framer_lib;
library ethernet_deframer_lib;
--library flow_control_lib;




-- Xilinx components
library unisim;
use unisim.vcomponents.all;

----------------------------------------------------------------------------------------------------
-- data-flow at the top-level (AXI-stream)
-- free-space rx -> fso-phy -> fifo -> fec_decoder -> deframer -> eth tx -> client
-- free-space tx <- fso-phy <- fifo <- fec_encoder <- framer   <- eth rx <- client
--
----------------------------------------------------------------------------------------------------
entity top_let_sim is
  port(
    clk200_p            : in std_logic; -- 100 MHz clock from board, I know about the naming ambiguity
        clk200_n            : in std_logic;
        reset               : in std_logic; -- system level reset for the LET system
        let_nom_clk_p       : in std_logic; -- GT reference clocks nominal and redundant
        let_nom_clk_n       : in std_logic;
        let_red_clk_p       : in std_logic;
        let_red_clk_n       : in std_logic;

        eth_gt0_txp         : out std_logic;  -- the GT serial link I/O pins for ethernet all 4 channels are connected to pins
        eth_gt0_txn         : out std_logic;

        eth_gt0_rxp         : in std_logic;
        eth_gt0_rxn         : in std_logic;

        aur_gt0_txp         : out std_logic; -- the GT serial link I/O pins 
        aur_gt0_txn         : out std_logic; 
        aur_gt0_rxp         : in std_logic;
        aur_gt0_rxn         : in std_logic;

        aur_gt1_txp         : out std_logic; -- the GT serial link I/O pins 
        aur_gt1_txn         : out std_logic; 
        aur_gt1_rxp         : in std_logic;
        aur_gt1_rxn         : in std_logic;

        uart_rxd            : in std_logic;
        uart_txd            : out std_logic;
        sys_clk_nom_en      : out std_logic;
        sys_clk_red_en      : out std_logic;
        let_clk_nom_en      : out std_logic;
        let_clk_red_en      : out std_logic;
        en_fso_lvl_trs      : out std_logic;

        fso_lim_dis_1_1v8   : out std_logic;
        fso_lim_dis_2_1v8   : out std_logic
  );
end entity top_let_sim;

architecture rtl of top_let_sim is

    constant  GND : std_logic := '0';
    constant  VCC : std_logic := '1';

    signal AXI_ETH_REG_IF_m   : AXI_Lite_master;
  signal AXI_ETH_REG_IF_s   : AXI_Lite_slave;

  signal AXI_FSO_PHY_CTRL_m   : AXI_Lite_master;
  signal AXI_FSO_PHY_CTRL_s   : AXI_Lite_slave;

  signal AXI_FSO_PHY_DRP_m   : AXI_Lite_master;
  signal AXI_FSO_PHY_DRP_s   : AXI_Lite_slave;

  signal AXI_LET_m   : AXI_Lite_master;
  signal AXI_LET_s   : AXI_Lite_slave;

  signal axis_mac_rx_tdata  : std_logic_vector ( 7 downto 0 );
  signal axis_mac_rx_tkeep  : std_logic_vector ( 7 downto 0 );
  signal axis_mac_rx_tlast  : std_logic;
  signal axis_mac_rx_tuser  : std_logic;
  signal axis_mac_rx_tvalid : std_logic;

  signal axis_mac_tx_tdata  : std_logic_vector ( 7 downto 0 );
  signal axis_mac_tx_tkeep  : std_logic_vector ( 0 downto 0 );
  signal axis_mac_tx_tlast  : std_logic;
  signal axis_mac_tx_tready : std_logic;
  signal axis_mac_tx_tuser  : std_logic;
  signal axis_mac_tx_tvalid : std_logic;

  signal ctl_tx_pause_req    : std_logic_vector ( 8 downto 0 );
  signal ctl_tx_resend_pause : std_logic;
  signal global_tx_pause_req : std_logic;



  signal mac_rx_clk_out : std_logic;
  signal mac_rx_reset   : std_logic;
  signal mac_tx_clk_out : std_logic;
  signal mac_tx_reset   : std_logic;

  signal mac_tx_resetn  : std_logic;
  signal mac_rx_resetn  : std_logic;


  signal mac_mux_data    : std_logic_vector(8 - 1 downto 0);
  signal mac_mux_valid   : std_logic;
  signal mac_mux_last    : std_logic;
  signal mac_mux_error   : std_logic;



  signal rs_enc_input_valid : std_logic;

  signal enc_frame_counter       : std_logic_vector(23 downto 0);
  signal enc_frame_counter_valid : std_logic;

  signal loopback_framer      : std_logic;
  signal loopback_framer_rs   : std_logic;
  signal loopback_fec         : std_logic;
  signal loopback_fso_mac     : std_logic;
  signal crc_enable           : std_logic;

  signal link_down_latched_reset_in : std_logic := '0';
  signal link_status_out            : std_logic;
  signal link_down_latched_out      : std_logic;
  signal init_done_out              : std_logic;
  signal init_retry_ctr_out         : std_logic_vector(3 downto 0);
  signal gtpowergood_out            : std_logic;
  signal txprgdivresetdone_out      : std_logic;
  signal rxprgdivresetdone_out      : std_logic;
  signal txpmaresetdone_out         : std_logic;
  signal rxpmaresetdone_out         : std_logic;
  signal gtwiz_reset_tx_done_out    : std_logic;
  signal gtwiz_reset_rx_done_out    : std_logic;

  signal hb_gtwiz_reset_all_axi_in                  : std_logic;
    signal fso_gt_reset : std_logic;
  signal hb0_gtwiz_reset_tx_pll_and_datapath_axi_in : std_logic_vector(0 downto 0);
  signal hb0_gtwiz_reset_tx_datapath_axi_in         : std_logic_vector(0 downto 0);
  signal hb_gtwiz_reset_rx_pll_and_datapath_axi_in  : std_logic;
  signal hb_gtwiz_reset_rx_datapath_axi_in          : std_logic;
  signal link_down_latched_reset_axi_in             : std_logic;

    signal fso_gt_drpen           : std_logic;
    signal fso_gt_drpwe           : std_logic;
    signal fso_gt_drpdi           : std_logic_vector(15 downto 0);
    signal fso_gt_drpaddr         : std_logic_vector(8 downto 0);
    signal fso_gt_drprdy          : std_logic;
    signal fso_gt_drpdo           : std_logic_vector(15 downto 0);

    signal dclk                    : std_logic;
    signal init_clk                : std_logic;
    signal s_axi_aclk              : std_logic;
    signal let_refclk_sel          : std_logic;
    signal s_axi_aresetn           : std_logic;  
    signal eth_rx_reset_in         : std_logic;
    signal eth_rx_user_rst_out     : std_logic;
    signal eth_rx_clk_out          : std_logic;
    signal eth_tx_reset_in         : std_logic;
    signal eth_tx_user_rst_out     : std_logic;
    signal eth_tx_clk_out          : std_logic;
    signal eth_channel_sel         : std_logic_vector(1 downto 0);
    signal fso_channel_sel         : std_logic_vector(1 downto 0);


    signal let_refclk_nom          : std_logic;
    signal let_refclk_red          : std_logic;
    signal let_gt_refclk_nom_out   : std_logic;
    signal let_gt_refclk_red_out   : std_logic;
    signal eth_rx_clk_out_int      : std_logic;
    signal eth_tx_clk_out_int      : std_logic;
    signal aur_sys_clk_int         : std_logic;
    signal aur_sys_rst_int         : std_logic;
    signal eth_rx_user_rst_out_int : std_logic;
    signal eth_tx_user_rst_out_int : std_logic;  
    signal eth_rx_rst_n            : std_logic;
    signal eth_tx_rst_n            : std_logic; 
    signal aur_sys_rst_n           : std_logic;
    signal fso_gt_loopback         : std_logic_vector(2 downto 0);
    signal fso_gt_loopback_in      : std_logic_vector(2 downto 0);

    signal txdiffctrl   : std_logic_vector(3 downto 0);
    signal txpostcursor : std_logic_vector(4 downto 0);
    signal txprecursor  : std_logic_vector(4 downto 0);
    signal rxpolinv     : std_logic;
    signal rxcdrlock    : std_logic;

    signal ref_clk_50      : std_logic;


    signal fso_phy_rx_clk        : std_logic;
    signal fso_phy_tx_clk        : std_logic;
    signal rx_data_good          : std_logic;
    signal gtwiz_userdata_rx_out : std_logic_vector(15 downto 0);
    signal fso_phy_rx_rst        : std_logic;
    signal fso_phy_rx_rstn       : std_logic;
    signal fso_phy_tx_rst        : std_logic;
    signal fso_phy_tx_rstn       : std_logic;
    signal rxfifo_tdata          : std_logic_vector(15 downto 0);
    signal rxfifo_tvalid         : std_logic;       
    signal rxfifo_tready         : std_logic;
    signal fso_rx_axis_tdata     : std_logic_vector(7 downto 0);
    signal fso_rx_axis_tvalid    : std_logic;
    signal fso_rx_rst_int        : std_logic;
    signal fso_rx_rstn_int       : std_logic;
    signal fso_tx_rst_int        : std_logic;
    signal fso_tx_rstn_int       : std_logic;

    signal gtwiz_userdata_tx_in  : std_logic_vector(15 downto 0);
  
    signal clk100          : std_logic;
    signal clk25           : std_logic;
    signal clk156          : std_logic;
    signal reset_sync      : std_logic;
    signal reset_sync_156  : std_logic;
    signal reset_sync_25   : std_logic;

    signal clk200          : std_logic;
    signal reset_sync_200  : std_logic;

    signal cfg_aquisition_length        : std_logic_vector(32-1 downto 0); -- hard-code values for now
    signal cfg_tracking_length          : std_logic_vector(32-1 downto 0);
    signal cfg_max_tracking_error       : std_logic_vector(32-1 downto 0);
    signal disp_sum                     : std_logic_vector(23 downto 0);

    signal deframer_in_axis_tdata       : std_logic_vector(8 - 1 downto 0);
    signal deframer_in_axis_tvalid      : std_logic;
    signal deframer_in_axis_tlast       : std_logic;
    signal deframer_in_axis_tready      : std_logic;
    signal rx_fifo_skipped_frame        : std_logic_vector(31 downto 0);

    signal deframer_out_axis_tdata       : std_logic_vector(8 - 1 downto 0);
    signal deframer_out_axis_tvalid      : std_logic;
    signal deframer_out_axis_tlast       : std_logic;
    signal deframer_out_axis_tready      : std_logic;
    signal deframer_out_axis_tuser       : std_logic;

    signal framer_out_axis_tdata         : std_logic_vector(8 - 1 downto 0);
    signal framer_out_axis_tvalid        : std_logic;
    signal framer_out_axis_tlast         : std_logic;
    signal framer_out_axis_tready        : std_logic;
    signal eth_rx_fifo_axis_tvalid       : STD_LOGIC;
    signal eth_rx_fifo_axis_tready       : STD_LOGIC;
    signal eth_rx_fifo_axis_tdata        : STD_LOGIC_VECTOR(7 DOWNTO 0);
    signal eth_rx_fifo_axis_tlast        : STD_LOGIC;
    signal eth_rx_fifo_axis_tuser        : STD_LOGIC;

    signal fso_tx_axis_tdata              : std_logic_vector(8 - 1 downto 0);
    signal fso_tx_axis_tvalid             : std_logic;
    signal fso_tx_axis_tlast              : std_logic;
    signal fso_tx_axis_tready             : std_logic;

    signal fso_tx_fifo_tdata  : std_logic_vector (15 downto 0);
    signal fso_tx_fifo_tvalid : std_logic;
    signal fso_tx_fifo_tready : std_logic;
    
    signal loopback_in  : std_logic_vector(2 downto 0);
    signal channel_sel  : std_logic := '0';
    signal mac_1g_rx_data    : std_logic_vector(8 - 1 downto 0);
    signal mac_1g_rx_valid   : std_logic;
    signal mac_1g_rx_last    : std_logic;
    signal mac_1g_rx_tuser   : std_logic;
    signal mac_1g_tx_data    : std_logic_vector(8 - 1 downto 0);
    signal mac_1g_tx_valid   : std_logic;
    signal mac_1g_tx_last    : std_logic;
    signal mac_1g_tx_ready   : std_logic;
    signal mac_1g_tx_tuser   : std_logic;
    signal fixed_packet_len  : std_logic;
    signal packet_len_in     : std_logic_vector(13 downto 0);
    signal num_packet        : std_logic_vector(31 downto 0);
    signal pkt_gen_en        : std_logic := '0';
    signal reset_pkt_gen     : std_logic;
    signal ref_clk_25   : std_logic;

    signal clear_stat           : std_logic;
    signal total_frame_counter  : std_logic_vector(31 downto 0);
    signal crc_error_counter    : std_logic_vector(31 downto 0);
    signal crc16_err_counter    : std_logic_vector(31 downto 0);
    signal crc32_err_counter    : std_logic_vector(31 downto 0);
    signal header_corrections   : std_logic_vector(31 downto 0);
    signal payload_corrections  : std_logic_vector(31 downto 0);
    signal lpc_frame_counter    : std_logic_vector(31 downto 0);
    signal lpc_corrections      : std_logic_vector(31 downto 0);
    signal new_search_counter   : std_logic_vector(31 downto 0);
    signal sync_loss_counter    : std_logic_vector(31 downto 0);

    signal s_axis_pause_tdata : std_logic_vector(15 downto 0);
    signal rate_snoop                    : std_logic_vector(1 downto 0);


begin
  en_fso_lvl_trs      <= '1'; -- apparently needed to power-up FSO board.
    fso_phy_tx_rstn     <= not(fso_phy_tx_rst);
    fso_phy_rx_rstn     <= not(fso_phy_rx_rst);
    fso_rx_rst_int      <=  reset_sync_200 or fso_phy_rx_rst;
    fso_rx_rstn_int     <= not(fso_rx_rst_int);
    fso_tx_rst_int      <=  reset_sync_200 or fso_phy_tx_rst;
    fso_tx_rstn_int     <= not(fso_tx_rst_int);

    mac_tx_resetn  <= not(mac_tx_reset);
    mac_rx_resetn  <= not(mac_rx_reset);

    sys_clk_nom_en      <= '1';    -- enable clocks on hardware
    sys_clk_red_en      <= '0';    
    let_clk_nom_en      <= '1';    
    let_clk_red_en      <= '0';  
    eth_rx_reset_in     <= '0';
    eth_tx_reset_in     <= '0'; 
    -- eth_loopback        <= '0';
    -- fso_gt_loopback     <= '0' & aur_loopback & '0';
    -- fso_gt_loopback_in  <= fso_gt_loopback or loopback_in;
    let_refclk_sel      <= '0'; -- only the nominal clock is connected to the FSO PHY so hard-wire
    -- aur_channel_sel     <= "01"; -- wave B on TCC/LET hardware for Condor Mark 1

    fso_gt_reset <= reset_sync_25 or hb_gtwiz_reset_all_axi_in;

    -- Enable FSO Rx line drivers
    fso_lim_dis_1_1v8   <= '1';
    fso_lim_dis_2_1v8   <= '1';

    -- hard-coded configurations
    cfg_aquisition_length  <= std_logic_vector(to_unsigned(8, 32));
  cfg_tracking_length    <= std_logic_vector(to_unsigned(32, 32));
  cfg_max_tracking_error <= std_logic_vector(to_unsigned(10, 32));
    
    inst_microblaze_wrapper : microblaze_wrapper 
      port map (
        AXI_ETH_REG_IF_araddr        => AXI_ETH_REG_IF_m.araddr, -- out STD_LOGIC_VECTOR ( 31 downto 0 );
        AXI_ETH_REG_IF_arprot        => AXI_ETH_REG_IF_m.arprot, -- out STD_LOGIC_VECTOR ( 2 downto 0 );
        AXI_ETH_REG_IF_arready       => AXI_ETH_REG_IF_s.arready, -- in STD_LOGIC_VECTOR ( 0 to 0 );
        AXI_ETH_REG_IF_arvalid       => AXI_ETH_REG_IF_m.arvalid, -- out STD_LOGIC_VECTOR ( 0 to 0 );
        AXI_ETH_REG_IF_awaddr        => AXI_ETH_REG_IF_m.awaddr, -- out STD_LOGIC_VECTOR ( 31 downto 0 );
        AXI_ETH_REG_IF_awprot        => AXI_ETH_REG_IF_m.awprot, -- out STD_LOGIC_VECTOR ( 2 downto 0 );
        AXI_ETH_REG_IF_awready       => AXI_ETH_REG_IF_s.awready, -- in STD_LOGIC_VECTOR ( 0 to 0 );
        AXI_ETH_REG_IF_awvalid       => AXI_ETH_REG_IF_m.awvalid, -- out STD_LOGIC_VECTOR ( 0 to 0 );
        AXI_ETH_REG_IF_bready        => AXI_ETH_REG_IF_m.bready, -- out STD_LOGIC_VECTOR ( 0 to 0 );
        AXI_ETH_REG_IF_bresp         => AXI_ETH_REG_IF_s.bresp, -- in STD_LOGIC_VECTOR ( 1 downto 0 );
        AXI_ETH_REG_IF_bvalid        => AXI_ETH_REG_IF_s.bvalid, -- in STD_LOGIC_VECTOR ( 0 to 0 );
        AXI_ETH_REG_IF_rdata         => AXI_ETH_REG_IF_s.rdata, -- in STD_LOGIC_VECTOR ( 31 downto 0 );
        AXI_ETH_REG_IF_rready        => AXI_ETH_REG_IF_m.rready, -- out STD_LOGIC_VECTOR ( 0 to 0 );
        AXI_ETH_REG_IF_rresp         => AXI_ETH_REG_IF_s.rresp, -- in STD_LOGIC_VECTOR ( 1 downto 0 );
        AXI_ETH_REG_IF_rvalid        => AXI_ETH_REG_IF_s.rvalid,    -- in STD_LOGIC_VECTOR ( 0 to 0 );
        AXI_ETH_REG_IF_wdata         => AXI_ETH_REG_IF_m.wdata, -- out STD_LOGIC_VECTOR ( 31 downto 0 );
        AXI_ETH_REG_IF_wready        => AXI_ETH_REG_IF_s.wready, -- in STD_LOGIC_VECTOR ( 0 to 0 );
        AXI_ETH_REG_IF_wstrb         => AXI_ETH_REG_IF_m.wstrb, -- out STD_LOGIC_VECTOR ( 3 downto 0 );
        AXI_ETH_REG_IF_wvalid        => AXI_ETH_REG_IF_m.wvalid, -- out STD_LOGIC_VECTOR ( 0 to 0 );

        AXI_FSO_PHY_CTRL_araddr      => AXI_FSO_PHY_CTRL_m.araddr,  --   -- : out STD_LOGIC_VECTOR ( 31 downto 0 );
        AXI_FSO_PHY_CTRL_arprot      => AXI_FSO_PHY_CTRL_m.arprot,  --   -- : out STD_LOGIC_VECTOR ( 2 downto 0 );
        AXI_FSO_PHY_CTRL_arready     => AXI_FSO_PHY_CTRL_s.arready(0),   -- : in STD_LOGIC;
        AXI_FSO_PHY_CTRL_arvalid     => AXI_FSO_PHY_CTRL_m.arvalid(0),   -- : out STD_LOGIC;
        AXI_FSO_PHY_CTRL_awaddr      => AXI_FSO_PHY_CTRL_m.awaddr,       -- : out STD_LOGIC_VECTOR ( 31 downto 0 );
        AXI_FSO_PHY_CTRL_awprot      => AXI_FSO_PHY_CTRL_m.awprot,       -- : out STD_LOGIC_VECTOR ( 2 downto 0 );
        AXI_FSO_PHY_CTRL_awready     => AXI_FSO_PHY_CTRL_s.awready(0),   -- : in STD_LOGIC;
        AXI_FSO_PHY_CTRL_awvalid     => AXI_FSO_PHY_CTRL_m.awvalid(0),   -- : out STD_LOGIC;
        AXI_FSO_PHY_CTRL_bready      => AXI_FSO_PHY_CTRL_m.bready(0),    -- : out STD_LOGIC;
        AXI_FSO_PHY_CTRL_bresp       => AXI_FSO_PHY_CTRL_s.bresp,        -- : in STD_LOGIC_VECTOR ( 1 downto 0 );
        AXI_FSO_PHY_CTRL_bvalid      => AXI_FSO_PHY_CTRL_s.bvalid(0),    -- : in STD_LOGIC;
        AXI_FSO_PHY_CTRL_rdata       => AXI_FSO_PHY_CTRL_s.rdata,        -- : in STD_LOGIC_VECTOR ( 31 downto 0 );
        AXI_FSO_PHY_CTRL_rready      => AXI_FSO_PHY_CTRL_m.rready(0),    -- : out STD_LOGIC;
        AXI_FSO_PHY_CTRL_rresp       => AXI_FSO_PHY_CTRL_s.rresp,        -- : in STD_LOGIC_VECTOR ( 1 downto 0 );
        AXI_FSO_PHY_CTRL_rvalid      => AXI_FSO_PHY_CTRL_s.rvalid(0),    -- : in STD_LOGIC;
        AXI_FSO_PHY_CTRL_wdata       => AXI_FSO_PHY_CTRL_m.wdata,        -- : out STD_LOGIC_VECTOR ( 31 downto 0 );
        AXI_FSO_PHY_CTRL_wready      => AXI_FSO_PHY_CTRL_s.wready(0),    -- : in STD_LOGIC;
        AXI_FSO_PHY_CTRL_wstrb       => AXI_FSO_PHY_CTRL_m.wstrb,        -- : out STD_LOGIC_VECTOR ( 3 downto 0 );
        AXI_FSO_PHY_CTRL_wvalid      => AXI_FSO_PHY_CTRL_m.wvalid(0),    -- : out STD_LOGIC;

        AXI_FSO_PHY_DRP_araddr       => AXI_FSO_PHY_DRP_m.araddr,  -- out STD_LOGIC_VECTOR ( 31 downto 0 );
        AXI_FSO_PHY_DRP_arprot       => AXI_FSO_PHY_DRP_m.arprot,  -- out STD_LOGIC_VECTOR ( 2 downto 0 );
        AXI_FSO_PHY_DRP_arready      => AXI_FSO_PHY_DRP_s.arready(0), -- in STD_LOGIC;
        AXI_FSO_PHY_DRP_arvalid      => AXI_FSO_PHY_DRP_m.arvalid(0), -- out STD_LOGIC;
        AXI_FSO_PHY_DRP_awaddr       => AXI_FSO_PHY_DRP_m.awaddr,  -- out STD_LOGIC_VECTOR ( 31 downto 0 );
        AXI_FSO_PHY_DRP_awprot       => AXI_FSO_PHY_DRP_m.awprot,  -- out STD_LOGIC_VECTOR ( 2 downto 0 );
        AXI_FSO_PHY_DRP_awready      => AXI_FSO_PHY_DRP_s.awready(0), -- in STD_LOGIC;
        AXI_FSO_PHY_DRP_awvalid      => AXI_FSO_PHY_DRP_m.awvalid(0), -- out STD_LOGIC;
        AXI_FSO_PHY_DRP_bready       => AXI_FSO_PHY_DRP_m.bready(0),  -- out STD_LOGIC;
        AXI_FSO_PHY_DRP_bresp        => AXI_FSO_PHY_DRP_s.bresp,   -- in STD_LOGIC_VECTOR ( 1 downto 0 );
        AXI_FSO_PHY_DRP_bvalid       => AXI_FSO_PHY_DRP_s.bvalid(0),  -- in STD_LOGIC;
        AXI_FSO_PHY_DRP_rdata        => AXI_FSO_PHY_DRP_s.rdata,   -- in STD_LOGIC_VECTOR ( 31 downto 0 );
        AXI_FSO_PHY_DRP_rready       => AXI_FSO_PHY_DRP_m.rready(0),  -- out STD_LOGIC;
        AXI_FSO_PHY_DRP_rresp        => AXI_FSO_PHY_DRP_s.rresp,   -- in STD_LOGIC_VECTOR ( 1 downto 0 );
        AXI_FSO_PHY_DRP_rvalid       => AXI_FSO_PHY_DRP_s.rvalid(0),  -- in STD_LOGIC;
        AXI_FSO_PHY_DRP_wdata        => AXI_FSO_PHY_DRP_m.wdata,   -- out STD_LOGIC_VECTOR ( 31 downto 0 );
        AXI_FSO_PHY_DRP_wready       => AXI_FSO_PHY_DRP_s.wready(0),  -- in STD_LOGIC;
        AXI_FSO_PHY_DRP_wstrb        => AXI_FSO_PHY_DRP_m.wstrb,   -- out STD_LOGIC_VECTOR ( 3 downto 0 );
        AXI_FSO_PHY_DRP_wvalid       => AXI_FSO_PHY_DRP_m.wvalid(0),  -- out STD_LOGIC;

        AXI_LET_araddr               => AXI_LET_m.araddr(30 downto 0),   -- out STD_LOGIC_VECTOR ( 30 downto 0 );
        AXI_LET_arprot               => AXI_LET_m.arprot,   -- out STD_LOGIC_VECTOR ( 2 downto 0 );
        AXI_LET_arready              => AXI_LET_s.arready(0),  -- in STD_LOGIC;
        AXI_LET_arvalid              => AXI_LET_m.arvalid(0),  -- out STD_LOGIC;
        AXI_LET_awaddr               => AXI_LET_m.awaddr(30 downto 0),   -- out STD_LOGIC_VECTOR ( 30 downto 0 );
        AXI_LET_awprot               => AXI_LET_m.awprot,   -- out STD_LOGIC_VECTOR ( 2 downto 0 );
        AXI_LET_awready              => AXI_LET_s.awready(0),  -- in STD_LOGIC;
        AXI_LET_awvalid              => AXI_LET_m.awvalid(0),  -- out STD_LOGIC;
        AXI_LET_bready               => AXI_LET_m.bready(0),   -- out STD_LOGIC;
        AXI_LET_bresp                => AXI_LET_s.bresp,    -- in STD_LOGIC_VECTOR ( 1 downto 0 );
        AXI_LET_bvalid               => AXI_LET_s.bvalid(0),   -- in STD_LOGIC;
        AXI_LET_rdata                => AXI_LET_s.rdata,    -- in STD_LOGIC_VECTOR ( 31 downto 0 );
        AXI_LET_rready               => AXI_LET_m.rready(0),   -- out STD_LOGIC;
        AXI_LET_rresp                => AXI_LET_s.rresp,    -- in STD_LOGIC_VECTOR ( 1 downto 0 );
        AXI_LET_rvalid               => AXI_LET_s.rvalid(0),   -- in STD_LOGIC;
        AXI_LET_wdata                => AXI_LET_m.wdata,    -- out STD_LOGIC_VECTOR ( 31 downto 0 );
        AXI_LET_wready               => AXI_LET_s.wready(0),   -- in STD_LOGIC;
        AXI_LET_wstrb                => AXI_LET_m.wstrb,    -- out STD_LOGIC_VECTOR ( 3 downto 0 );
        AXI_LET_wvalid               => AXI_LET_m.wvalid(0),   -- out STD_LOGIC;

        CLK_IN1_D_0_clk_n            => clk200_n, -- in STD_LOGIC;
        CLK_IN1_D_0_clk_p            => clk200_p, -- in STD_LOGIC;
        clk_25M                      => clk25, -- out STD_LOGIC;
        freerun_100_clk              => clk100, -- out STD_LOGIC;
        freerun_clk_100_reset(0)     => reset_sync, -- out STD_LOGIC_VECTOR ( 0 to 0 );
        freerun_clk_100_resetn(0)    => s_axi_aresetn, -- out STD_LOGIC_VECTOR ( 0 to 0 );
        let_156_clk                  => clk200, -- out STD_LOGIC;
        reset_0                      => reset, -- in STD_LOGIC;
        reset_sync_156(0)            => reset_sync_200, --  out STD_LOGIC_VECTOR ( 0 to 0 );
        reset_sync_25(0)             => reset_sync_25, --  out STD_LOGIC_VECTOR ( 0 to 0 );
        rs232_uart_rxd               => uart_rxd, -- in STD_LOGIC;
        rs232_uart_txd               => uart_txd -- out STD_LOGIC
    );
 

    -- SW control blocks
    inst_axi4lite_ctrl_fso_phy : entity let_sda_lib.axi4lite_ctrl_fso_phy1g
  port map(
    clk => clk25,
    rst => reset_sync_25,

    -- Write address
    s_axi_awaddr  => AXI_FSO_PHY_CTRL_m.awaddr(7 downto 0),
    s_axi_awvalid => AXI_FSO_PHY_CTRL_m.awvalid(0),
    s_axi_awready => AXI_FSO_PHY_CTRL_s.awready(0),

    -- Write data
    s_axi_wdata  => AXI_FSO_PHY_CTRL_m.wdata, 
    s_axi_wvalid => AXI_FSO_PHY_CTRL_m.wvalid(0),
    s_axi_wready => AXI_FSO_PHY_CTRL_s.wready(0),

    -- Write response
    s_axi_bresp  => AXI_FSO_PHY_CTRL_s.bresp,
    s_axi_bvalid => AXI_FSO_PHY_CTRL_s.bvalid(0),
    s_axi_bready => AXI_FSO_PHY_CTRL_m.bready(0),

    -- Read address
    s_axi_araddr  => AXI_FSO_PHY_CTRL_m.araddr(7 downto 0),
    s_axi_arvalid => AXI_FSO_PHY_CTRL_m.arvalid(0),
    s_axi_arready => AXI_FSO_PHY_CTRL_s.arready(0),

    -- Read data
    s_axi_rdata  => AXI_FSO_PHY_CTRL_s.rdata,
    s_axi_rresp  => AXI_FSO_PHY_CTRL_s.rresp,
    s_axi_rvalid => AXI_FSO_PHY_CTRL_s.rvalid(0),
    s_axi_rready => AXI_FSO_PHY_CTRL_m.rready(0),

    gtpowergood_in            => gtpowergood_out,
    link_status_in            => link_status_out,
    link_down_latched_in      => link_down_latched_out,
    txprgdivresetdone_in      => txprgdivresetdone_out,
    rxprgdivresetdone_in      => rxprgdivresetdone_out,
    txpmaresetdone_in         => txpmaresetdone_out,
    rxpmaresetdone_in         => rxpmaresetdone_out,
    gtwiz_reset_tx_done_in    => gtwiz_reset_tx_done_out,
    gtwiz_reset_rx_done_in    => gtwiz_reset_rx_done_out,
    init_done_in              => init_done_out,
    init_retry_ctr_in         => init_retry_ctr_out,

    hb_gtwiz_reset_all_axi_out                  => hb_gtwiz_reset_all_axi_in,
    hb0_gtwiz_reset_tx_pll_and_datapath_axi_out => hb0_gtwiz_reset_tx_pll_and_datapath_axi_in,
    hb0_gtwiz_reset_tx_datapath_axi_out         => hb0_gtwiz_reset_tx_datapath_axi_in,
    hb_gtwiz_reset_rx_pll_and_datapath_axi_out  => hb_gtwiz_reset_rx_pll_and_datapath_axi_in,
    hb_gtwiz_reset_rx_datapath_axi_out          => hb_gtwiz_reset_rx_datapath_axi_in,
    link_down_latched_reset_axi_out             => link_down_latched_reset_axi_in,
    loopback_out                                => loopback_in,

    txdiffctrl_out    => txdiffctrl, 
        txpostcursor_out  => txpostcursor,
        txprecursor_out   => txprecursor,
        rxpolinv_out      => rxpolinv,
        rxcdrlock_in      => rxcdrlock
  ); 
    -- instantiate axi-lite to drp interface  for FSO PHY  GT
    fso_axi_drp_i : axi_to_drp
        port map (    
            ---------------------- Write Address Channel --------------------------
            S_AXI_AWADDR  => AXI_FSO_PHY_DRP_m.awaddr ,
            S_AXI_AWVALID => AXI_FSO_PHY_DRP_m.awvalid(0),
            S_AXI_AWREADY => AXI_FSO_PHY_DRP_s.awready(0), 
            ---------------------- Write Data Channel -----------------------------    
            S_AXI_WDATA   => AXI_FSO_PHY_DRP_m.wdata ,
            S_AXI_WSTRB   => AXI_FSO_PHY_DRP_m.wstrb ,
            S_AXI_WVALID  => AXI_FSO_PHY_DRP_m.wvalid(0) ,
            S_AXI_WREADY  => AXI_FSO_PHY_DRP_s.wready(0) ,
            S_AXI_BVALID  => AXI_FSO_PHY_DRP_s.bvalid(0),
            S_AXI_BRESP   => AXI_FSO_PHY_DRP_s.bresp ,
            S_AXI_BREADY  => AXI_FSO_PHY_DRP_m.bready(0) ,
            -------------------- Read Address Channel ---------------------------
            S_AXI_ARADDR  => AXI_FSO_PHY_DRP_m.araddr ,
            S_AXI_ARVALID => AXI_FSO_PHY_DRP_m.arvalid(0) ,
            S_AXI_ARREADY => AXI_FSO_PHY_DRP_s.arready(0) ,
            -------------------- Read Data Channel -----------------------------
            S_AXI_RDATA   => AXI_FSO_PHY_DRP_s.rdata ,
            S_AXI_RVALID  => AXI_FSO_PHY_DRP_s.rvalid(0) ,
            S_AXI_RRESP   => AXI_FSO_PHY_DRP_s.rresp ,
            S_AXI_RREADY  => AXI_FSO_PHY_DRP_m.rready(0) , 
            ---------------------- GT DRP Ports ----------------------
            DRPADDR_IN    => fso_gt_drpaddr   ,  
            DRPDI_IN      => fso_gt_drpdi    ,
            DRPDO_OUT     => fso_gt_drpdo    ,
            DRPRDY_OUT    => fso_gt_drprdy ,
            DRPEN_IN      => fso_gt_drpen   ,
            DRPWE_IN      => fso_gt_drpwe   ,
            DRP_CLK_IN    => clk25 ,
            RESET         => reset_sync_25
        );  

    -- LET GT refclk buffer 
    -- instantiate the diff to single-ended clock input buffers for the reference clocks
    let_refclk_buf_inst : entity let_sda_lib.let_ref_clk_buf 
        port map (
            let_clk_nom_p         => let_nom_clk_p ,
            let_clk_nom_n         => let_nom_clk_n,
            let_clk_red_p         => let_red_clk_p,
            let_clk_red_n         => let_red_clk_n,
            let_gt_refclk_nom     => let_refclk_nom,
            let_gt_refclk_red     => let_refclk_red,
            let_gt_refclk_nom_out => let_gt_refclk_nom_out, -- BUFG'ed versions of nom and red refclk
            let_gt_refclk_red_out => let_gt_refclk_red_out
        );

     
    -- instantiate FSO PHY single channel version for now
    inst_fso_phy : entity let_sda_lib.fso_phy_top  
        port map (
            gt_refclk                     => let_refclk_nom,               -- : in  std_logic;
            clk_fec                       => clk200,                       -- : in std_logic; -- clock at which the FEC is running
            reset_fec                     => reset_sync_200,               -- : in std_logic;
            clk25                         => clk25,                        -- : in std_logic;
            channel_sel                   => channel_sel,                  -- : in std_logic; -- 0 wave a, 1 wave b
            ch0_gthrxn_in                 => aur_gt0_rxn,                  -- : in  std_logic;
            ch0_gthrxp_in                 => aur_gt0_rxp,                  -- : in  std_logic;
            ch0_gthtxn_out                => aur_gt0_txn,                  -- : out std_logic;
            ch0_gthtxp_out                => aur_gt0_txp,                  -- : out std_logic;
            ch1_gthrxn_in                 => aur_gt1_rxn,                  -- : in  std_logic;
            ch1_gthrxp_in                 => aur_gt1_rxp,                  -- : in  std_logic;
            ch1_gthtxn_out                => aur_gt1_txn,                  -- : out std_logic;
            ch1_gthtxp_out                => aur_gt1_txp,                  -- : out std_logic;
            hb_gtwiz_reset_all_in         => fso_gt_reset, -- reset_sync_25,                -- : in std_logic;
            rx_reset_out                  => fso_phy_rx_rst,               -- : out std_logic;
            rx_usrclk2_out                => fso_phy_rx_clk,               -- : out std_logic;
            fso_rx_axis_tdata             => fso_rx_axis_tdata,            -- : out  std_logic_vector(8 - 1 downto 0);
            fso_rx_axis_tvalid            => fso_rx_axis_tvalid, --  : out  std_logic;
            data_good_in                  => rx_data_good,                 -- : in  std_logic;
            tx_reset_out                  => fso_phy_tx_rst,               -- : out std_logic;
            tx_usrclk2_out                => fso_phy_tx_clk,               -- : out std_logic;
            fso_tx_axis_tdata             => fso_tx_axis_tdata  , --: in  std_logic_vector(8 - 1 downto 0);
            fso_tx_axis_tvalid            => fso_tx_axis_tvalid , --: in  std_logic;
            fso_tx_axis_tlast             => fso_tx_axis_tlast  , --: in  std_logic;
            fso_tx_axis_tready            => fso_tx_axis_tready , --: out std_logic;
            link_down_latched_reset_in    => link_down_latched_reset_in,   -- : in  std_logic;
            link_status_out               => link_status_out ,   -- : out std_logic;
            link_down_latched_out         => link_down_latched_out ,   -- : out std_logic;
            init_done_out                 => init_done_out ,   -- : out std_logic;
            init_retry_ctr_out            => init_retry_ctr_out ,   -- : out std_logic_vector(3 downto 0);
            gtpowergood_out               => gtpowergood_out ,   -- : out std_logic;
            txpmaresetdone_out            => txpmaresetdone_out ,   -- : out std_logic;
            rxpmaresetdone_out            => rxpmaresetdone_out ,   -- : out std_logic;
            gtwiz_reset_tx_done_out       => gtwiz_reset_tx_done_out ,   -- : out std_logic;
            gtwiz_reset_rx_done_out       => gtwiz_reset_rx_done_out ,   -- : out std_logic;
            drpaddr_in                    => fso_gt_drpaddr,   -- : in std_logic_vector( 8 downto 0);
            drpclk_in(0)                  => clk25,   -- : in std_logic_vector (0 downto 0);
            drpdi_in                      => fso_gt_drpdi,   -- : in std_logic_vector(15 downto 0);
            drpen_in(0)                   => fso_gt_drpen,   -- : in std_logic_vector (0 downto 0);
            drpwe_in(0)                   => fso_gt_drpwe,   -- : in std_logic_vector (0 downto 0);
            drpdo_out                     => fso_gt_drpdo,   -- : out std_logic_vector(15 downto 0);
            drprdy_out(0)                 => fso_gt_drprdy ,   -- : out std_logic_vector (0 downto 0) 
            txdiffctrl_in                 => txdiffctrl, 
            txpostcursor_in               => txpostcursor,
            txprecursor_in                => txprecursor,
            rxpolinv_in                   => rxpolinv,
            rxcdrlock_out                 => rxcdrlock,
            loopback_in                   => loopback_in
        );	

    inst_axi4lite_ctrl_let : entity let_sda_lib.axi4lite_ctrl_let1g
        port map(
            clk => clk200,
            rst => reset_sync_200,
    
            -- Write address
            s_axi_awaddr  => AXI_LET_m.awaddr(7 downto 0),
            s_axi_awvalid => AXI_LET_m.awvalid(0),
            s_axi_awready => AXI_LET_s.awready(0),
    
            -- Write data
            s_axi_wdata  => AXI_LET_m.wdata,
            s_axi_wvalid => AXI_LET_m.wvalid(0),
            s_axi_wready => AXI_LET_s.wready(0),
    
            -- Write response
            s_axi_bresp  => AXI_LET_s.bresp,
            s_axi_bvalid => AXI_LET_s.bvalid(0),
            s_axi_bready => AXI_LET_m.bready(0),
    
            -- Read address
            s_axi_araddr  => AXI_LET_m.araddr(7 downto 0),
            s_axi_arvalid => AXI_LET_m.arvalid(0),
            s_axi_arready => AXI_LET_s.arready(0),
    
            -- Read data
            s_axi_rdata  => AXI_LET_s.rdata,
            s_axi_rresp  => AXI_LET_s.rresp,
            s_axi_rvalid => AXI_LET_s.rvalid(0),
            s_axi_rready => AXI_LET_m.rready(0),
    
            -- Modulator connection
            clear_stat          => clear_stat         , 
            total_frame_counter => total_frame_counter, 
            crc_error_counter   => crc_error_counter  , 
            crc16_err_counter   => crc16_err_counter  , 
            crc32_err_counter   => crc32_err_counter  , 
            header_corrections  => header_corrections , 
            payload_corrections => payload_corrections, 
            lpc_frame_counter   => lpc_frame_counter  , 
            lpc_corrections     => lpc_corrections    , 
            new_search_counter  => new_search_counter , 
            sync_loss_counter   => sync_loss_counter    
        );

    frame_fec_top : entity frame_decoding.frame_fec_top 
      port map (
        clk             => clk200, -- : in  std_logic;
        rst             => reset_sync_200, -- : in  std_logic;
    
        preamble        => HEADER_PREAMBLE, -- : in  std_logic_vector(71 downto 0);
    
        -- configuration for the preamble sync algorithm
        cfg_aquisition_length  => cfg_aquisition_length  , -- : in std_logic_vector(32 - 1 downto 0);
        cfg_tracking_length    => cfg_tracking_length    , -- : in std_logic_vector(32 - 1 downto 0);
        cfg_max_tracking_error => cfg_max_tracking_error , -- : in std_logic_vector(32 - 1 downto 0);
    
        -- TX data input - header and payload data
        ----------------------
        tx_axis_tdata      => framer_out_axis_tdata    , -- : in  std_logic_vector(8 - 1 downto 0);
        tx_axis_tvalid     => framer_out_axis_tvalid   , -- : in  std_logic;
        tx_axis_tlast      => framer_out_axis_tlast    , -- : in  std_logic;
        tx_axis_tready     => framer_out_axis_tready   , -- : out std_logic;
    
        disp_sum           => disp_sum, --: out std_logic_vector(23 downto 0);
    
        -- Output data handling
        -----------------------
        ch_tx_axis_tdata     => fso_tx_axis_tdata    , --: out std_logic_vector(8 - 1 downto 0);
        ch_tx_axis_tvalid    => fso_tx_axis_tvalid   , --: out std_logic;
        ch_tx_axis_tlast     => fso_tx_axis_tlast    , -- : out std_logic;
        ch_tx_axis_tready    => fso_tx_axis_tready   , --: in  std_logic;
    
    
        -- RX data input
        ----------------------
        ch_rx_axis_tdata      => fso_rx_axis_tdata,  --: in  std_logic_vector(8 - 1 downto 0);
        ch_rx_axis_tvalid     => fso_rx_axis_tvalid, --: in  std_logic;
    
        -- Status
        -----------------------
        crc16_valid       => open, -- : out std_logic;
        crc16_correct     => open, -- : out std_logic;
        crc32_valid       => open, -- : out std_logic;
        crc32_correct     => open, -- : out std_logic;
    
        payload_cerr      => open , -- : out std_logic_vector(4 - 1 downto 0);
        payload_ncerr     => open , -- : out std_logic;
        payload_err_valid => open , -- : out std_logic;
    
        header_cerr       => open, -- : out std_logic_vector(4 - 1 downto 0);
        header_ncerr      => open, -- : out std_logic;
        header_err_valid  => open, -- : out std_logic;
    
        preamble_synced   => rx_data_good, -- : out std_logic;
    
        -- More diagnostic informations
        -------------------------------------------------------
	    	clear_stat          => GND, -- : in  std_logic;
        total_frame_counter => open, -- : out std_logic_vector(31 downto 0);
        crc_error_counter   => open, -- : out std_logic_vector(31 downto 0);
        crc16_err_counter   => open, -- : out std_logic_vector(31 downto 0);
        crc32_err_counter   => open, -- : out std_logic_vector(31 downto 0);
        header_corrections  => open, -- : out std_logic_vector(31 downto 0);
        payload_corrections => open, -- : out std_logic_vector(31 downto 0);
        lpc_frame_counter   => open, -- : out std_logic_vector(31 downto 0);
        lpc_corrections     => open, -- : out std_logic_vector(31 downto 0);
    
        -- Stats from the preamble sync
        new_search_counter  => open, -- : out std_logic_vector(31 downto 0);
        sync_loss_counter   => open, -- : out std_logic_vector(31 downto 0);
    
        -- RX data output - header and payload data
        -----------------------
        rx_axis_tdata     => deframer_in_axis_tdata , -- : out std_logic_vector(8 - 1 downto 0);
        rx_axis_tvalid    => deframer_in_axis_tvalid, -- : out std_logic;
        rx_axis_tlast     => deframer_in_axis_tlast , -- : out std_logic;
        rx_axis_tready    => deframer_in_axis_tready  -- : in  std_logic
      );


    -- instantiate Framer here
     ethernet_framer : entity ethernet_framer_lib.ethernet_framer_top
        generic map (
            MINIMUN_FREE_BYTES_TO_SPLIT => 64
        )
      port map(
            clk_let        => clk200, -- : in  std_logic;
            rst_let        => reset_sync_200, -- : in  std_logic;
            clk_eth        => mac_rx_clk_out, -- : in  std_logic;
            rst_eth        => mac_rx_reset, -- : in  std_logic;
                    -- Input data 
            ----------------------
            s_axis_tdata     => mac_mux_data,  -- : in  std_logic_vector(8 - 1 downto 0);
            s_axis_tvalid    => mac_mux_valid, -- : in  std_logic;
            s_axis_tlast     => mac_mux_last,  -- : in  std_logic;
            s_axis_tready    => open, -- : out std_logic;

            tx_pause_req          => global_tx_pause_req,
            rx_fifo_skipped_frame => rx_fifo_skipped_frame,

            -- Output data 
            ----------------------
            m_axis_tdata  => framer_out_axis_tdata  , -- : out  std_logic_vector(8 - 1 downto 0);
            m_axis_tvalid => framer_out_axis_tvalid , -- : out std_logic;
            m_axis_tlast  => framer_out_axis_tlast  , -- : out std_logic;
            m_axis_tready => framer_out_axis_tready , -- : in std_logic

            -- Statistics
            ----------------------            
            clear_stat                      => '0',
            total_packet_counter            => open,
            total_packet_splitted_counter   => open,
            total_frame_counter             => open

        );

    -- instantiate Deframer here 
    ethernet_deframer : entity ethernet_deframer_lib.ethernet_deframer_top
        port map ( 
            clk            => clk200, -- : in  std_logic;
            rst            => reset_sync_200, -- : in  std_logic;
            -- Input data 
            ----------------------
            s_axis_tdata     => deframer_in_axis_tdata ,  -- : in  std_logic_vector(8 - 1 downto 0);
            s_axis_tvalid    => deframer_in_axis_tvalid,  -- : in  std_logic;
            s_axis_tlast     => deframer_in_axis_tlast ,  -- : in  std_logic;
            s_axis_tready    => deframer_in_axis_tready,  -- : out std_logic;
    
            -- Output data 
            ----------------------
            m_axis_tdata  => deframer_out_axis_tdata  , -- : out  std_logic_vector(8 - 1 downto 0);
            m_axis_tvalid => deframer_out_axis_tvalid , -- : out std_logic;
            m_axis_tlast  => deframer_out_axis_tlast  , -- : out std_logic;
            m_axis_tready => deframer_out_axis_tready , -- : in std_logic
            m_axis_tuser  => deframer_out_axis_tuser,

            -- Statistics
            ----------------------
            clear_stat                      => '0',
            total_packet_counter            => open,
            total_packet_splitted_counter   => open,
            total_packet_merged_counter     => open,
            total_frame_counter             => open,
            total_frame_error_counter       => open,
            total_packet_error_counter      => open

        );



    -- create 50 MHz from 100 MHz freerun clock
    -- BUFGCE_DIV: General Clock Buffer with Divide Function
    -- UltraScale
    BUFGCE_DIV_inst : BUFGCE_DIV
        generic map (
            BUFGCE_DIVIDE   => 2, -- 1-8
            IS_CE_INVERTED  => '0', -- Optional inversion for CE
            IS_CLR_INVERTED => '0', -- Optional inversion for CLR
            IS_I_INVERTED   => '0' -- Optional inversion for I
        )
        port map (
            O   => ref_clk_50, -- 1-bit output: Buffer
            CE  => VCC, -- 1-bit input: Buffer enable
            CLR => GND, -- 1-bit input: Asynchronous clear
            I   => clk100    -- 1-bit input: Buffer
        );
    
        BUFGCE_DIV_25_inst : BUFGCE_DIV
        generic map (
            BUFGCE_DIVIDE   => 4, -- 1-8
            IS_CE_INVERTED  => '0', -- Optional inversion for CE
            IS_CLR_INVERTED => '0', -- Optional inversion for CLR
            IS_I_INVERTED   => '0' -- Optional inversion for I
        )
        port map (
            O   => ref_clk_25, -- 1-bit output: Buffer
            CE  => VCC, -- 1-bit input: Buffer enable
            CLR => GND, -- 1-bit input: Asynchronous clear
            I   => clk100    -- 1-bit input: Buffer
        );
        


    -- snoop on the current rate change that is happening.
    pr_snoop_rc : process(clk25)
    begin 
        if rising_edge(clk25) then
            if fso_gt_drpwe = '1' and fso_gt_drpen = '1' and fso_gt_drpaddr = '0' & x"10" and fso_gt_drpdi = x"07B6" then 
                rate_snoop <= "01"; -- 625 Mbps
            elsif fso_gt_drpwe = '1' and fso_gt_drpen = '1' and fso_gt_drpaddr = '0' & x"10" and fso_gt_drpdi = x"07A6" then 
                rate_snoop <= "10"; -- 312.5 Mbps
            elsif fso_gt_drpwe = '1' and fso_gt_drpen = '1' and fso_gt_drpaddr = '0' & x"10" and fso_gt_drpdi = x"07C6" then 
                rate_snoop <= "00"; -- 1250 Mbps    
            else 
                rate_snoop <= (others => '0'); -- 1250 Mbps by default.
            end if;
        end if;
    end process; 

    -- TODO: pause generation logic needs to be looked into
    --s_axis_pause_tdata  <= x"0FF0"; -- fixed pause length in  512 bit time multiples
    s_axis_pause_tdata  <= x"009C" when rate_snoop = "10" else -- 312.5 
                           x"0048" when rate_snoop = "01" else x"0027";
    -- instantiate eth_1g_top

    eth_1g_top : entity let_sda_lib.eth_1g_top
        port map (
            reset                    => reset_sync  ,
            ref_clk_50               => ref_clk_50  ,
            
            let_refclk_nom           => let_refclk_nom                 ,

            eth_gt0_txp              => eth_gt0_txp                    ,
            eth_gt0_txn              => eth_gt0_txn                    ,
            
            eth_gt0_rxp              => eth_gt0_rxp                    ,
            eth_gt0_rxn              => eth_gt0_rxn                    ,
                       
            s_axi_aclk                => clk100  ,
            s_axi_aresetn             => s_axi_aresetn                 ,

            s_axi_awaddr              => AXI_ETH_REG_IF_m.awaddr       ,
            s_axi_awvalid             => AXI_ETH_REG_IF_m.awvalid(0)   ,
            s_axi_awready             => AXI_ETH_REG_IF_s.awready(0)   ,
            s_axi_wdata               => AXI_ETH_REG_IF_m.wdata        ,
            s_axi_wstrb               => AXI_ETH_REG_IF_m.wstrb        ,
            s_axi_wvalid              => AXI_ETH_REG_IF_m.wvalid(0)    ,
            s_axi_wready              => AXI_ETH_REG_IF_s.wready(0)    ,
            s_axi_bresp               => AXI_ETH_REG_IF_s.bresp        ,
            s_axi_bvalid              => AXI_ETH_REG_IF_s.bvalid(0)    ,
            s_axi_bready              => AXI_ETH_REG_IF_m.bready(0)    ,
            s_axi_araddr              => AXI_ETH_REG_IF_m.araddr       ,
            s_axi_arvalid             => AXI_ETH_REG_IF_m.arvalid(0)   ,
            s_axi_arready             => AXI_ETH_REG_IF_s.arready(0)   ,
            s_axi_rdata               => AXI_ETH_REG_IF_s.rdata        ,
            s_axi_rresp               => AXI_ETH_REG_IF_s.rresp        ,
            s_axi_rvalid              => AXI_ETH_REG_IF_s.rvalid(0)    ,
            s_axi_rready              => AXI_ETH_REG_IF_m.rready(0)    , 
   
            rx_reset_in              => eth_rx_reset_in                ,
            rx_user_rst_out          => mac_rx_reset                   ,

            rx_clk_out               => mac_rx_clk_out                 ,

            rx_axis_tvalid           => mac_mux_valid                  ,
            rx_axis_tdata            => mac_mux_data                   ,
            rx_axis_tlast            => mac_mux_last                   ,
            rx_axis_tuser            => mac_mux_error                  ,

            tx_reset_in              => eth_tx_reset_in                ,
            tx_user_rst_out          => mac_tx_reset                   ,

            tx_clk_out               => mac_tx_clk_out                 ,

            tx_axis_tready           => axis_mac_tx_tready             ,
            tx_axis_tvalid           => axis_mac_tx_tvalid             ,
            tx_axis_tdata            => axis_mac_tx_tdata              ,
            tx_axis_tlast            => axis_mac_tx_tlast              ,
            tx_axis_tuser            => axis_mac_tx_tuser              ,
            
            s_axis_pause_tdata       => s_axis_pause_tdata ,
            s_axis_pause_tvalid      => global_tx_pause_req
               
        );
 
    
 
 

    -- CDC fifo to convert rx ethernet data from 125 MHz to 200 clock rate.
--    eth_mac_rx_fifo :  dc_fifo_eth_side
--        port map (
--            m_aclk               =>  clk200, -- : IN STD_LOGIC;
--            s_aclk               =>  mac_rx_clk_out, -- : IN STD_LOGIC;
--            s_aresetn            =>  mac_rx_resetn, -- : IN STD_LOGIC;
--            s_axis_tvalid        =>  mac_mux_valid, -- : IN STD_LOGIC;
--            s_axis_tready        =>  open, -- : OUT STD_LOGIC;
--            s_axis_tdata         =>  mac_mux_data, -- : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
--            s_axis_tlast         =>  mac_mux_last, -- : IN STD_LOGIC;
--            s_axis_tuser(0)      =>  mac_mux_error, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--            m_axis_tvalid        =>  eth_rx_fifo_axis_tvalid, -- : OUT STD_LOGIC;
--            m_axis_tready        =>  eth_rx_fifo_axis_tready, -- : IN STD_LOGIC;
--            m_axis_tdata         =>  eth_rx_fifo_axis_tdata, -- : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
--            m_axis_tlast         =>  eth_rx_fifo_axis_tlast, -- : OUT STD_LOGIC;
--            m_axis_tuser(0)      =>  eth_rx_fifo_axis_tuser-- : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
--        );

    -- MAC input FIFO
--    inst_flow_control_fifo: entity flow_control_lib.rx_fifo
--    port map(
--      clk_mac => mac_rx_clk_out,
--      rst_mac => mac_rx_reset,
--
--      mac_data  => mac_mux_data,
--      mac_valid => mac_mux_valid,
--      mac_keep  => x"FF",
--      mac_error => mac_mux_error,
--      mac_ready => open,
--      mac_last  => mac_mux_last,
--
--      clk_let => clk200,
--      rst_let => reset_sync_200,
--
--      tx_pause_req          => global_tx_pause_req,
--      rx_fifo_skipped_frame => open,
--
--      mac_out_data   => eth_rx_fifo_axis_tdata,
--      mac_out_valid  => eth_rx_fifo_axis_tvalid,
--      mac_out_keep   => open,
--      mac_out_ready  => eth_rx_fifo_axis_tready,
--      mac_out_last   => eth_rx_fifo_axis_tlast,
--      mac_out_length => open
--    );

    -- CDC fifo to convert tx ethernet data (coming from deframer) from 200 MHz to 125 clock rate.
    eth_mac_tx_fifo :  dc_fifo_eth_side
        port map (
            m_aclk               => mac_tx_clk_out, -- : IN STD_LOGIC;
            s_aclk               => clk200, -- : IN STD_LOGIC;
            s_aresetn            => mac_tx_resetn, -- : IN STD_LOGIC;
            s_axis_tvalid        => deframer_out_axis_tvalid, -- : IN STD_LOGIC;
            s_axis_tready        => deframer_out_axis_tready, -- : OUT STD_LOGIC;
            s_axis_tdata         => deframer_out_axis_tdata, -- : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
            s_axis_tlast         => deframer_out_axis_tlast, -- : IN STD_LOGIC;
            s_axis_tuser(0)      => deframer_out_axis_tuser, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            m_axis_tvalid        => axis_mac_tx_tvalid , -- : OUT STD_LOGIC;
            m_axis_tready        => axis_mac_tx_tready , -- : IN STD_LOGIC;
            m_axis_tdata         => axis_mac_tx_tdata , -- : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            m_axis_tlast         => axis_mac_tx_tlast , -- : OUT STD_LOGIC;
            m_axis_tuser(0)      => axis_mac_tx_tuser  -- : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
        );
        --inst_fifo_loopback : fifo_loopback -- for testing 
        --  PORT MAP (
        --    m_aclk          => mac_rx_clk_out,
        --    s_aclk          => mac_tx_clk_out,
        --    s_aresetn       => eth_tx_reset_in,
        --    s_axis_tvalid   => mac_mux_valid,
        --    s_axis_tready   => open,
        --    s_axis_tdata    => mac_mux_data,
        --    s_axis_tlast    => mac_mux_last,
        --    s_axis_tuser(0) => mac_mux_error,
        --    m_axis_tvalid   => axis_mac_tx_tvalid,
        --    m_axis_tready   => axis_mac_tx_tready,
        --    m_axis_tdata    => axis_mac_tx_tdata,
        --    m_axis_tlast    => axis_mac_tx_tlast,
        --    m_axis_tuser(0) => axis_mac_tx_tuser
        --  );
  
  

    reset_pkt_gen <= mac_rx_reset or mac_tx_reset;

    mac_1g_rx_data     <= axis_mac_tx_tdata;  -- going to packet gen
    mac_1g_rx_valid    <= axis_mac_tx_tvalid; 
    mac_1g_rx_last     <= axis_mac_tx_tlast; 
    mac_1g_rx_tuser    <= axis_mac_tx_tuser;  
     
    axis_mac_tx_tready <= '1'; 
      
    fixed_packet_len  <= '1';
    packet_len_in     <= std_logic_vector(to_unsigned(1000, 14));
    num_packet        <= std_logic_vector(to_unsigned(10000, 32));       
    --pkt_gen_en        <= '0', '1' after 350 us;       
    
    proc_ctrl_pkt_gen_en: process is
    begin
        wait until rising_edge(mac_tx_clk_out);
        pkt_gen_en <= '0';
        wait for 350 us;
        wait until rising_edge(mac_tx_clk_out);
        pkt_gen_en <= '1';
        wait;
        wait for 1200 us;
        
        wait until rising_edge(mac_tx_clk_out);
        pkt_gen_en <= '0';
--        wait for 300 us;
--        wait until rising_edge(mac_tx_clk_out);
--        pkt_gen_en <= '1';
--        wait for 10 us;
--        wait until rising_edge(mac_tx_clk_out);
--        pkt_gen_en <= '0';
        wait;

    end process;
    
      
    -- sim related sources
    mac_packet_gen_inst : entity let_sda_lib.mac_packet_gen 
      port map (
        clk     => mac_tx_clk_out, -- : in  std_logic;
        rst     => reset_pkt_gen, -- : in  std_logic;    
        mac_1g_rx_data   => mac_1g_rx_data   , --  : in  std_logic_vector(8 - 1 downto 0);
        mac_1g_rx_valid  => mac_1g_rx_valid  , --  : in  std_logic;
        mac_1g_rx_last   => mac_1g_rx_last   , --  : in  std_logic;
        mac_1g_rx_tuser  => mac_1g_rx_tuser  , --  : in  std_logic;    
        mac_1g_tx_data   => mac_1g_tx_data   , --  : out std_logic_vector(8 - 1 downto 0);
        mac_1g_tx_valid  => mac_1g_tx_valid  , --  : out std_logic;
        mac_1g_tx_last   => mac_1g_tx_last   , --  : out std_logic;
        mac_1g_tx_ready  => mac_1g_tx_ready  , --  : in  std_logic;
        mac_1g_tx_tuser  => mac_1g_tx_tuser  , --  : out std_logic;    
        fixed_packet_len => fixed_packet_len , --  : in  std_logic;
        packet_len_in    => packet_len_in    , --  : in  std_logic_vector(13 downto 0);
        num_packet       => num_packet       , --  : in  std_logic_vector(31 downto 0);
        pkt_gen_en       => pkt_gen_en         --  : in  std_logic
      );

end architecture rtl;
