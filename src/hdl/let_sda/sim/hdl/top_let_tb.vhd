-- standard packages
library ieee;
use ieee.std_logic_1164.all;  
use ieee.numeric_std.all;     
use ieee.std_logic_misc.all;
use ieee.math_real.all;
-- Xilinx components
library unisim;
use unisim.vcomponents.all;

library let_sda_lib;
use let_sda_lib.let_pckg.all;
use let_sda_lib.let_pckg_components.all;

-- LET top-level testbench

entity top_let_tb is 
end top_let_tb;

architecture tb of top_let_tb is  
  constant LET_CLK_PERIOD    : time := 6.4 ns;
  constant AXI_CLK_PERIOD    : time := 10.0 ns;
  constant RESET_TIME        : time := 20000.0 ns;

  signal clk200_p            : std_logic  := '0';
  signal clk200_n            : std_logic  := '1';
  signal reset               : std_logic  := '0'; 
  signal let_nom_clk_p       : std_logic  := '0'; 
  signal let_nom_clk_n       : std_logic  := '1';
  signal let_red_clk_p       : std_logic  := '0';
  signal let_red_clk_n       : std_logic  := '1';
  
  signal eth_gt0_txp         : std_logic;     
  signal eth_gt0_txn         : std_logic;     
  signal eth_gt0_rxp         : std_logic;     
  signal eth_gt0_rxn         : std_logic;     
  signal aur_gt0_txp         : std_logic;     
  signal aur_gt0_txn         : std_logic;     
  signal aur_gt0_rxp         : std_logic;     
  signal aur_gt0_rxn         : std_logic;   
  signal aur_gt1_txp         : std_logic;     
  signal aur_gt1_txn         : std_logic;     
  signal aur_gt1_rxp         : std_logic;     
  signal aur_gt1_rxn         : std_logic;   
  signal uart_rxd            : std_logic;     
  signal uart_txd            : std_logic;     
  signal sys_clk_nom_en      : std_logic;     
  signal sys_clk_red_en      : std_logic;     
  signal let_clk_nom_en      : std_logic;     
  signal let_clk_red_en      : std_logic;     
  signal en_fso_lvl_trs      : std_logic;     


begin
  -- clocks and resets etc. 
  --let_refclk_nom <= not let_refclk_nom after LET_CLK_PERIOD/2; -- disable nominal clock for this test.
  let_nom_clk_p  <= not let_nom_clk_p after LET_CLK_PERIOD/2;
  let_nom_clk_n  <= not let_nom_clk_n after LET_CLK_PERIOD/2;
  let_red_clk_p  <= not let_red_clk_p after LET_CLK_PERIOD/2;
  let_red_clk_n  <= not let_red_clk_n after LET_CLK_PERIOD/2;
  reset          <= '1', '0'          after RESET_TIME; -- reset
  clk200_p       <= not clk200_p      after AXI_CLK_PERIOD/2; -- TCC/LET 100 MHz refclk
  clk200_n       <= not clk200_n      after AXI_CLK_PERIOD/2;
  

  -- ethernet GT loopback or near-end PMA loopback in GT 
  eth_gt0_rxp    <= eth_gt0_txp; --  after 500 ns; 
  eth_gt0_rxn    <= eth_gt0_txn; --  after 500 ns; 
  -- fso side loopback
  aur_gt0_rxp    <= aur_gt0_txp; --  after 500 ns; 
  aur_gt0_rxn    <= aur_gt0_txn; --  after 500 ns; 
  aur_gt1_rxp    <= aur_gt1_txp; --  after 500 ns; 
  aur_gt1_rxn    <= aur_gt1_txn; --  after 500 ns; 
  
  uart_rxd       <= uart_txd;    -- UART Loopback


  -- instantiate DUT
  dut_let : entity let_sda_lib.top_let_sim
  	port map(
  	  clk200_p         => clk200_p         , --   : in std_logic; -- 100 MHz clock from board, I know about the naming ambiguity
      clk200_n         => clk200_n         , --   : in std_logic;
      reset            => reset            , --   : in std_logic; -- system level reset for the LET system
      let_nom_clk_p    => let_nom_clk_p    , --   : in std_logic; -- GT reference clocks nominal and redundant
      let_nom_clk_n    => let_nom_clk_n    , --   : in std_logic;
      let_red_clk_p    => let_red_clk_p    , --   : in std_logic;
      let_red_clk_n    => let_red_clk_n    , --   : in std_logic;
      eth_gt0_txp      => eth_gt0_txp      , --   : out std_logic;  -- the GT serial link I/O pins for ethernet all 4 channels are connected to pins
      eth_gt0_txn      => eth_gt0_txn      , --   : out std_logic;
      eth_gt0_rxp      => eth_gt0_rxp      , --   : in std_logic;
      eth_gt0_rxn      => eth_gt0_rxn      , --   : in std_logic;
      aur_gt0_txp      => aur_gt0_txp      , --   : out std_logic; -- the GT serial link I/O pins for FSO PHY, wave B in use 
      aur_gt0_txn      => aur_gt0_txn      , --   : out std_logic; 
      aur_gt0_rxp      => aur_gt0_rxp      , --   : in std_logic;
      aur_gt0_rxn      => aur_gt0_rxn      , --   : in std_logic;
      aur_gt1_txp      => aur_gt1_txp,      
      aur_gt1_txn      => aur_gt1_txn,      
      aur_gt1_rxp      => aur_gt1_rxp,           
      aur_gt1_rxn      => aur_gt1_rxn,       
      uart_rxd         => uart_rxd         , --   : in std_logic;
      uart_txd         => uart_txd         , --   : out std_logic;
      sys_clk_nom_en   => sys_clk_nom_en   , --   : out std_logic;
      sys_clk_red_en   => sys_clk_red_en   , --   : out std_logic;
      let_clk_nom_en   => let_clk_nom_en   , --   : out std_logic;
      let_clk_red_en   => let_clk_red_en   , --   : out std_logic;
      en_fso_lvl_trs   => en_fso_lvl_trs    --   : out std_logic	
  	);
    
end tb;   
