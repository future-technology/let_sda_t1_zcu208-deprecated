##################################################
##  Add SD IPs
##################################################
source ../src/hdl/let_sda/ip/let_sda_ip.tcl

##################################################
##  Add Etherbet Framer/Deframer + SDA Common
##################################################

add_files -norecurse { \
  ../src/hdl/let_sda/ethernet_deframer/ethernet_deframer.vhd \
  ../src/hdl/let_sda/ethernet_deframer/ethernet_deframer_top.vhd \
  ../src/hdl/let_sda/ethernet_deframer/packet_filter_top.vhd \
  ../src/hdl/let_sda/ethernet_deframer/packet_filter.vhd \
  ../src/hdl/let_sda/ethernet_deframer/pkg_components.vhd \
  }
set_property library ethernet_deframer_lib [get_files { \
  ../src/hdl/let_sda/ethernet_deframer/ethernet_deframer.vhd \
  ../src/hdl/let_sda/ethernet_deframer/ethernet_deframer_top.vhd \
  ../src/hdl/let_sda/ethernet_deframer/packet_filter_top.vhd \
  ../src/hdl/let_sda/ethernet_deframer/packet_filter.vhd \
  ../src/hdl/let_sda/ethernet_deframer/pkg_components.vhd \
  }]

add_files -norecurse { \
  ../src/hdl/let_sda/frame_filter/frame_filter.vhd \
  ../src/hdl/let_sda/frame_filter/frame_filter_top.vhd \
  }
set_property library frame_filter_lib [get_files { \
  ../src/hdl/let_sda/frame_filter/frame_filter.vhd \
  ../src/hdl/let_sda/frame_filter/frame_filter_top.vhd \
  }]

add_files -norecurse { \
  ../src/hdl/let_sda/ethernet_framer/ethernet_framer.vhd \
  ../src/hdl/let_sda/ethernet_framer/ethernet_framer_top.vhd \
  ../src/hdl/let_sda/ethernet_framer/pkg_components.vhd \
  ../src/hdl/let_sda/ethernet_framer/prbs_gen.vhd \
  }
set_property library ethernet_framer_lib [get_files { \
  ../src/hdl/let_sda/ethernet_framer/ethernet_framer.vhd \
  ../src/hdl/let_sda/ethernet_framer/ethernet_framer_top.vhd \
  ../src/hdl/let_sda/ethernet_framer/pkg_components.vhd \
  ../src/hdl/let_sda/ethernet_framer/prbs_gen.vhd \
  }]

add_files -norecurse { \
  ../src/hdl/let_sda/arq/common/arq_axis_mux_1xn.vhd \
  ../src/hdl/let_sda/arq/common/arq_axis_mux_nx1.vhd \
  ../src/hdl/let_sda/arq/common/arq_internal_ram_controller.vhd \
  ../src/hdl/let_sda/arq/common/arq_ram_controller_top.vhd \
  ../src/hdl/let_sda/arq/common/arq_req_control_support_pkg.vhd \
  ../src/hdl/let_sda/arq/common/arq_rom_addr_translator.vhd \
  ../src/hdl/let_sda/arq/common/arq_support_pkg.vhd \
  ../src/hdl/let_sda/arq/common/arq_axis_pl_router_1x2.vhd \
  ../src/hdl/let_sda/arq/common/arq_axis_pl_router_2x1.vhd \
  ../src/hdl/let_sda/arq/transmitter/arq_timekeeper_tx.vhd \
  ../src/hdl/let_sda/arq/transmitter/arq_timekeeper_tx_wrapper.vhd \
  ../src/hdl/let_sda/arq/transmitter/arq_transmitter_top.vhd \
  ../src/hdl/let_sda/arq/transmitter/arq_router_tx.vhd \
  ../src/hdl/let_sda/arq/receiver/arq_timekeeper_rx.vhd \
  ../src/hdl/let_sda/arq/receiver/arq_timekeeper_rx_wrapper.vhd \
  ../src/hdl/let_sda/arq/receiver/arq_receiver_top.vhd \
  ../src/hdl/let_sda/arq/receiver/arq_router_rx.vhd \
  }
  
set_property library arq_lib [get_files { \
  ../src/hdl/let_sda/arq/common/arq_axis_mux_1xn.vhd \
  ../src/hdl/let_sda/arq/common/arq_axis_mux_nx1.vhd \
  ../src/hdl/let_sda/arq/common/arq_internal_ram_controller.vhd \
  ../src/hdl/let_sda/arq/common/arq_ram_controller_top.vhd \
  ../src/hdl/let_sda/arq/common/arq_req_control_support_pkg.vhd \
  ../src/hdl/let_sda/arq/common/arq_rom_addr_translator.vhd \
  ../src/hdl/let_sda/arq/common/arq_support_pkg.vhd \
  ../src/hdl/let_sda/arq/common/arq_axis_pl_router_1x2.vhd \
  ../src/hdl/let_sda/arq/common/arq_axis_pl_router_2x1.vhd \
  ../src/hdl/let_sda/arq/transmitter/arq_timekeeper_tx.vhd \
  ../src/hdl/let_sda/arq/transmitter/arq_timekeeper_tx_wrapper.vhd \
  ../src/hdl/let_sda/arq/transmitter/arq_transmitter_top.vhd \
  ../src/hdl/let_sda/arq/transmitter/arq_router_tx.vhd \
  ../src/hdl/let_sda/arq/receiver/arq_timekeeper_rx.vhd \
  ../src/hdl/let_sda/arq/receiver/arq_timekeeper_rx_wrapper.vhd \
  ../src/hdl/let_sda/arq/receiver/arq_receiver_top.vhd \
  ../src/hdl/let_sda/arq/receiver/arq_router_rx.vhd \
  }]

add_files -norecurse { \
  ../src/hdl/let_sda/sda/pkg_sda.vhd \
  ../src/hdl/let_sda/sda/interfaces_pkg.vhd \
  ../src/hdl/let_sda/ip/axis_payload_fifo.vhd \
  }
set_property library sda_lib [get_files { \
  ../src/hdl/let_sda/sda/pkg_sda.vhd \
  ../src/hdl/let_sda/sda/interfaces_pkg.vhd \
  ../src/hdl/let_sda/ip/axis_payload_fifo.vhd \
  }]

add_files -norecurse { \
  ../src/hdl/let_sda/frame_header/frame_header_tx.vhd \
  ../src/hdl/let_sda/frame_header/frame_header_rx.vhd \
  ../src/hdl/let_sda/frame_header/timestamp_association.vhd \
  ../src/hdl/let_sda/frame_header/timestamp_insertion.vhd \
  }
set_property library frame_header_lib [get_files { \
  ../src/hdl/let_sda/frame_header/frame_header_tx.vhd \
  ../src/hdl/let_sda/frame_header/frame_header_rx.vhd \
  ../src/hdl/let_sda/frame_header/timestamp_association.vhd \
  ../src/hdl/let_sda/frame_header/timestamp_insertion.vhd \
  }]

add_files -norecurse { \
  ../src/hdl/let_sda/frame_timing_detector/frame_timing_detector_top.vhd \
  ../src/hdl/let_sda/frame_timing_detector/frame_timing_detector.vhd \
  ../src/hdl/let_sda/frame_timing_detector/pkg_components.vhd \
  ../src/hdl/let_sda/frame_timing_detector/tof_interrogator.vhd \
  ../src/hdl/let_sda/frame_timing_detector/tof_responder.vhd \
  ../src/hdl/let_sda/frame_timing_detector/tof_transponder.vhd \
  }
set_property library ranging_lib [get_files { \
  ../src/hdl/let_sda/frame_timing_detector/frame_timing_detector_top.vhd \
  ../src/hdl/let_sda/frame_timing_detector/frame_timing_detector.vhd \
  ../src/hdl/let_sda/frame_timing_detector/pkg_components.vhd \
  ../src/hdl/let_sda/frame_timing_detector/tof_interrogator.vhd \
  ../src/hdl/let_sda/frame_timing_detector/tof_responder.vhd \
  ../src/hdl/let_sda/frame_timing_detector/tof_transponder.vhd \
  }]

add_files -norecurse { \
  ../src/hdl/let_sda/fcch_transponder/fcch_receiver.vhd \
  ../src/hdl/let_sda/fcch_transponder/fcch_transmitter.vhd \
  ../src/hdl/let_sda/fcch_transponder/fcch_transponder.vhd \
  ../src/hdl/let_sda/fcch_transponder/pkg_fcch.vhd \
  }
set_property library fcch_lib [get_files { \
  ../src/hdl/let_sda/fcch_transponder/fcch_receiver.vhd \
  ../src/hdl/let_sda/fcch_transponder/fcch_transmitter.vhd \
  ../src/hdl/let_sda/fcch_transponder/fcch_transponder.vhd \
  ../src/hdl/let_sda/fcch_transponder/pkg_fcch.vhd \
  }]

#################################
##  Add to Lib: SHARE_LET_1G
#################################

add_files -norecurse { \
  ../src/hdl/let_sda/share_let_1g/packages/pkg_components.vhd \
  ../src/hdl/let_sda/share_let_1g/packages/pkg_param.vhd \
  ../src/hdl/let_sda/share_let_1g/packages/pkg_support.vhd \
  ../src/hdl/let_sda/share_let_1g/src/axi4s_buffer.vhd \
  ../src/hdl/let_sda/share_let_1g/src/axi4s_buffer_v2.vhd \
  ../src/hdl/let_sda/share_let_1g/src/axi4s_fifo.vhd \
  ../src/hdl/let_sda/share_let_1g/src/axi4s_fifo_v2.vhd \
  ../src/hdl/let_sda/share_let_1g/src/bit_sync.vhd \
  ../src/hdl/let_sda/share_let_1g/src/sync_fifo.vhd \
  ../src/hdl/let_sda/share_let_1g/src/generic_simple_dp_ram.vhd \
  }
set_property library share_let_1g [get_files  { \
  ../src/hdl/let_sda/share_let_1g/packages/pkg_components.vhd \
  ../src/hdl/let_sda/share_let_1g/packages/pkg_param.vhd \
  ../src/hdl/let_sda/share_let_1g/packages/pkg_support.vhd \
  ../src/hdl/let_sda/share_let_1g/src/axi4s_buffer.vhd \
  ../src/hdl/let_sda/share_let_1g/src/axi4s_buffer_v2.vhd \
  ../src/hdl/let_sda/share_let_1g/src/axi4s_fifo.vhd \
  ../src/hdl/let_sda/share_let_1g/src/axi4s_fifo_v2.vhd \
  ../src/hdl/let_sda/share_let_1g/src/bit_sync.vhd \
  ../src/hdl/let_sda/share_let_1g/src/sync_fifo.vhd \
  ../src/hdl/let_sda/share_let_1g/src/generic_simple_dp_ram.vhd \
  }]
  
#######################################
##  Add Monitoring modules
##  to Lib: monitoring_lib
#######################################

add_files -norecurse { \
  ../src/hdl/let_sda/monitoring/bitrate_calculator.vhd \
  ../src/hdl/let_sda/monitoring/corrections_rate_calculator.vhd \
  ../src/hdl/let_sda/monitoring/frame_health_monitor.vhd \
  }
set_property library monitoring_lib [get_files  { \
  ../src/hdl/let_sda/monitoring/bitrate_calculator.vhd \
  ../src/hdl/let_sda/monitoring/corrections_rate_calculator.vhd \
  ../src/hdl/let_sda/monitoring/frame_health_monitor.vhd \
  }]

#######################################
##  Add others (incl. Calculators)
##  to Lib: LET_SDA_LIB
#######################################
add_files -norecurse { \
  ../src/hdl/let_sda/let_sda_top/let_sda_top_t1.vhd \
  }

set_property library let_sda_lib [get_files  { \
  ../src/hdl/let_sda/let_sda_top/let_sda_top_t1.vhd \
  }]
############
##  End
