-- Useful types and functions are defined here
-- compile to let_sda_lib


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package let_pckg is

    constant GND                  : std_logic                        := '0';
    constant VCC                  : std_logic                        := '1';
    constant ZERO_8B              : std_logic_vector(7 downto 0)     := (others => '0');
    constant ZERO_16B             : std_logic_vector(15 downto 0)    := (others => '0');
    constant ZERO_64B             : std_logic_vector(63 downto 0)    := (others => '0');

    constant HEADER_PREAMBLE      : std_logic_vector(71 downto 0) := x"77AD_5B58_4364_1E2E_26" ; -- 0x77AD5B5843641E2E26
    --constant PREAMBLE_IN        : std_logic_vector(71 downto 0) := X"77AD5B5843641E2E26";

    function crc32(input: std_logic_vector(7 downto 0); crc:  std_logic_vector(31 downto 0)) return std_logic_vector;

	-------------------------------------------------
	--  AXI4-Lite Bus Records
	-------------------------------------------------

	type AXI_Lite_master is record
		-- Wr.Address Channel -----------------------
		awaddr  : std_logic_vector(31 downto 0);
		awprot  : std_logic_vector(2 downto 0);
		awvalid : std_logic_vector(0 downto 0);
		-- Wr.Data Channel --------------------------
		wdata   : std_logic_vector(31 downto 0);
		wstrb   : std_logic_vector(3 downto 0);
		wvalid  : std_logic_vector(0 downto 0);
		-- Wr.Response Channel ----------------------
		bready  : std_logic_vector(0 downto 0);
		-- Rd.Address Channel -----------------------
		araddr  : std_logic_vector(31 downto 0);
		arprot  : std_logic_vector(2 downto 0);
		arvalid : std_logic_vector(0 downto 0);
		rready  : std_logic_vector(0 downto 0);
		-- Rd.Data Channel --------------------------
		--
	end record AXI_Lite_master;

	-----------------------
	--		.awaddr  ;
	--		.awprot  ;
	--		.awvalid ;
	--		.wdata   ;
	--		.wstrb   ;
	--		.wvalid  ;
	--		.bready  ;
	--		.araddr  ;
	--		.arprot  ;
	--		.arvalid ;
	--		.rready  ;
	-----------------------

	type AXI_Lite_slave is record
		-- Wr.Address Channel -----------------------
		awready : std_logic_vector(0 downto 0);
		-- Wr.Data Channel --------------------------
		wready  : std_logic_vector(0 downto 0);
		-- Wr.Response Channel ----------------------
		bresp   : std_logic_vector(1 downto 0);
		bvalid  : std_logic_vector(0 downto 0);
		-- Rd.Address Channel -----------------------
		arready : std_logic_vector(0 downto 0);
		-- Rd.Data Channel --------------------------
		rdata   : std_logic_vector(31 downto 0);
		rresp   : std_logic_vector(1 downto 0);
		rvalid  : std_logic_vector(0 downto 0);
	end record AXI_Lite_slave;

	-----------------------
	--		.awready ;
	--		.wready  ;
	--		.bresp   ;
	--		.bvalid  ;
	--		.arready ;
	--		.rdata   ;
	--		.rresp   ;
	--		.rvalid  ;
	-----------------------

	-------------------------------------------------
	--  AXI4 Bus Records
	-------------------------------------------------

	type AXI4_master is record
		-- Wr.Address Channel -----------------------
		awid     : std_logic_vector(0 to 0);
		awaddr   : std_logic_vector(31 downto 0);
		awlen    : std_logic_vector(7 downto 0);
		awsize   : std_logic_vector(2 downto 0);
		awburst  : std_logic_vector(1 downto 0);
		awlock   : std_logic_vector(0 to 0);
		awcache  : std_logic_vector(3 downto 0);
		awprot   : std_logic_vector(2 downto 0);
		awqos    : std_logic_vector(3 downto 0);
		awregion : std_logic_vector(3 downto 0);
		awvalid  : std_logic_vector(0 downto 0);
		-- Wr.Data Channel --------------------------
		wdata    : std_logic_vector(31 downto 0);
		wstrb    : std_logic_vector(3 downto 0);
		wlast    : std_logic;
		wvalid   : std_logic_vector(0 downto 0);
		-- Wr.Response Channel ----------------------
		bready   : std_logic_vector(0 downto 0);
		-- Rd.Address Channel -----------------------
		arid     : std_logic_vector(0 to 0);
		araddr   : std_logic_vector(31 downto 0);
		arlen    : std_logic_vector(7 downto 0);
		arsize   : std_logic_vector(2 downto 0);
		arburst  : std_logic_vector(1 downto 0);
		arlock   : std_logic_vector(0 to 0);
		arcache  : std_logic_vector(3 downto 0);
		arprot   : std_logic_vector(2 downto 0);
		arqos    : std_logic_vector(3 downto 0);
		arregion : std_logic_vector(3 downto 0);
		arvalid  : std_logic_vector(0 downto 0);
		rready   : std_logic_vector(0 downto 0);
		-- Rd.Data Channel --------------------------
		--
	end record AXI4_master;

	type AXI4_slave is record
		-- Wr.Address Channel -----------------------
		awready : std_logic_vector(0 downto 0);
		-- Wr.Data Channel --------------------------
		wready  : std_logic_vector(0 downto 0);
		-- Wr.Response Channel ----------------------
		bid     : std_logic_vector(0 to 0);
		bresp   : std_logic_vector(1 downto 0);
		bvalid  : std_logic_vector(0 downto 0);
		-- Rd.Address Channel -----------------------
		arready : std_logic_vector(0 downto 0);
		-- Rd.Data Channel --------------------------
		rid     : std_logic_vector(0 to 0);
		rdata   : std_logic_vector(31 downto 0);
		rresp   : std_logic_vector(1 downto 0);
		rlast   : std_logic;
		rvalid  : std_logic_vector(0 downto 0);
	end record AXI4_slave;

    type drp_if is record -- record for rx data from GT to Ethernet core
        drpen   : STD_LOGIC; -- _VECTOR(0 downto 0);
        drpwe   : STD_LOGIC; -- _VECTOR(0 downto 0);
        drpaddr : STD_LOGIC_VECTOR(8 DOWNTO 0);
        drpdi   : STD_LOGIC_VECTOR(15 DOWNTO 0);
        drprdy  : STD_LOGIC; -- _VECTOR(0 downto 0);
        drpdo   : STD_LOGIC_VECTOR(15 DOWNTO 0);
    end record;

   type drp_if_arr2   is array (0 to 1) of drp_if; -- array of record for DRP interface

   COMPONENT dc_fifo_eth_side
   PORT (
	 m_aclk : IN STD_LOGIC;
	 s_aclk : IN STD_LOGIC;
	 s_aresetn : IN STD_LOGIC;
	 s_axis_tvalid : IN STD_LOGIC;
	 s_axis_tready : OUT STD_LOGIC;
	 s_axis_tdata : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	 s_axis_tlast : IN STD_LOGIC;
	 s_axis_tuser : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
	 m_axis_tvalid : OUT STD_LOGIC;
	 m_axis_tready : IN STD_LOGIC;
	 m_axis_tdata : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	 m_axis_tlast : OUT STD_LOGIC;
	 m_axis_tuser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0) 
   );
 END COMPONENT;

end let_pckg;

package body let_pckg is
    function crc32(input: std_logic_vector(7 downto 0); crc:  std_logic_vector(31 downto 0)) return std_logic_vector is
		variable v_sreg: std_logic_vector(31 downto 0);
		variable v_input : std_logic_vector(7 downto 0);
	begin
		for i in 0 to 7 loop
			v_input(i) := input(7 - i);
		end loop;

		v_sreg(0) := v_input(6) xor v_input(0) xor crc(24) xor crc(30);
		v_sreg(1) := v_input(7) xor v_input(6) xor v_input(1) xor v_input(0) xor crc(24) xor crc(25) xor crc(30) xor crc(31);
		v_sreg(2) := v_input(7) xor v_input(6) xor v_input(2) xor v_input(1) xor v_input(0) xor crc(24) xor crc(25) xor crc(26) xor crc(30) xor crc(31);
		v_sreg(3) := v_input(7) xor v_input(3) xor v_input(2) xor v_input(1) xor crc(25) xor crc(26) xor crc(27) xor crc(31);
		v_sreg(4) := v_input(6) xor v_input(4) xor v_input(3) xor v_input(2) xor v_input(0) xor crc(24) xor crc(26) xor crc(27) xor crc(28) xor crc(30);
		v_sreg(5) := v_input(7) xor v_input(6) xor v_input(5) xor v_input(4) xor v_input(3) xor v_input(1) xor v_input(0) xor crc(24) xor crc(25) xor crc(27) xor crc(28) xor crc(29) xor crc(30) xor crc(31);
		v_sreg(6) := v_input(7) xor v_input(6) xor v_input(5) xor v_input(4) xor v_input(2) xor v_input(1) xor crc(25) xor crc(26) xor crc(28) xor crc(29) xor crc(30) xor crc(31);
		v_sreg(7) := v_input(7) xor v_input(5) xor v_input(3) xor v_input(2) xor v_input(0) xor crc(24) xor crc(26) xor crc(27) xor crc(29) xor crc(31);
		v_sreg(8) := v_input(4) xor v_input(3) xor v_input(1) xor v_input(0) xor crc(0) xor crc(24) xor crc(25) xor crc(27) xor crc(28);
		v_sreg(9) := v_input(5) xor v_input(4) xor v_input(2) xor v_input(1) xor crc(1) xor crc(25) xor crc(26) xor crc(28) xor crc(29);
		v_sreg(10) := v_input(5) xor v_input(3) xor v_input(2) xor v_input(0) xor crc(2) xor crc(24) xor crc(26) xor crc(27) xor crc(29);
		v_sreg(11) := v_input(4) xor v_input(3) xor v_input(1) xor v_input(0) xor crc(3) xor crc(24) xor crc(25) xor crc(27) xor crc(28);
		v_sreg(12) := v_input(6) xor v_input(5) xor v_input(4) xor v_input(2) xor v_input(1) xor v_input(0) xor crc(4) xor crc(24) xor crc(25) xor crc(26) xor crc(28) xor crc(29) xor crc(30);
		v_sreg(13) := v_input(7) xor v_input(6) xor v_input(5) xor v_input(3) xor v_input(2) xor v_input(1) xor crc(5) xor crc(25) xor crc(26) xor crc(27) xor crc(29) xor crc(30) xor crc(31);
		v_sreg(14) := v_input(7) xor v_input(6) xor v_input(4) xor v_input(3) xor v_input(2) xor crc(6) xor crc(26) xor crc(27) xor crc(28) xor crc(30) xor crc(31);
		v_sreg(15) := v_input(7) xor v_input(5) xor v_input(4) xor v_input(3) xor crc(7) xor crc(27) xor crc(28) xor crc(29) xor crc(31);
		v_sreg(16) := v_input(5) xor v_input(4) xor v_input(0) xor crc(8) xor crc(24) xor crc(28) xor crc(29);
		v_sreg(17) := v_input(6) xor v_input(5) xor v_input(1) xor crc(9) xor crc(25) xor crc(29) xor crc(30);
		v_sreg(18) := v_input(7) xor v_input(6) xor v_input(2) xor crc(10) xor crc(26) xor crc(30) xor crc(31);
		v_sreg(19) := v_input(7) xor v_input(3) xor crc(11) xor crc(27) xor crc(31);
		v_sreg(20) := v_input(4) xor crc(12) xor crc(28);
		v_sreg(21) := v_input(5) xor crc(13) xor crc(29);
		v_sreg(22) := v_input(0) xor crc(14) xor crc(24);
		v_sreg(23) := v_input(6) xor v_input(1) xor v_input(0) xor crc(15) xor crc(24) xor crc(25) xor crc(30);
		v_sreg(24) := v_input(7) xor v_input(2) xor v_input(1) xor crc(16) xor crc(25) xor crc(26) xor crc(31);
		v_sreg(25) := v_input(3) xor v_input(2) xor crc(17) xor crc(26) xor crc(27);
		v_sreg(26) := v_input(6) xor v_input(4) xor v_input(3) xor v_input(0) xor crc(18) xor crc(24) xor crc(27) xor crc(28) xor crc(30);
		v_sreg(27) := v_input(7) xor v_input(5) xor v_input(4) xor v_input(1) xor crc(19) xor crc(25) xor crc(28) xor crc(29) xor crc(31);
		v_sreg(28) := v_input(6) xor v_input(5) xor v_input(2) xor crc(20) xor crc(26) xor crc(29) xor crc(30);
		v_sreg(29) := v_input(7) xor v_input(6) xor v_input(3) xor crc(21) xor crc(27) xor crc(30) xor crc(31);
		v_sreg(30) := v_input(7) xor v_input(4) xor crc(22) xor crc(28) xor crc(31);
		v_sreg(31) := v_input(5) xor crc(23) xor crc(29);
		return v_sreg;
	end crc32;
end package body let_pckg;