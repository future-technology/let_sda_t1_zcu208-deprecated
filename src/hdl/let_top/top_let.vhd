-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
--  Project    : Condor Mk3 LET Tranche 1
--  Author     : Gustavo Martin
--  Description:                
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library let_sda_lib;
use work.let_pckg.all;

library frame_encoding;
library frame_decoding;
library ethernet_framer_lib;
library ethernet_deframer_lib;
library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;
library share_let_1g;
use share_let_1g.pkg_components;
-- Xilinx components
library unisim;
use unisim.vcomponents.all;

----------------------------------------------------------------------------------------------------
-- data-flow at the top-level (AXI-stream)
-- free-space rx -> fso-phy -> fifo -> fec_decoder -> deframer -> eth tx -> client
-- free-space tx <- fso-phy <- fifo <- fec_encoder <- framer   <- eth rx <- client
--
----------------------------------------------------------------------------------------------------
entity top_let is
    generic(
        g_DEBUG : natural := 0          -- 0 = no debug, 1 = print debug
    );
    port(
        clk_sys            : in  std_logic;
        s_axi_resetn       : in  std_logic;
        clk_let           : in  std_logic; -- 330 MHz
        clk_ldpc          : in  std_logic; -- 231 MHz
        clk_ref_fso_p        : in  std_logic; -- 156.25 MHz
        clk_ref_fso_n        : in  std_logic; -- 156.25 MHz
        clk_ref_eth        : in  std_logic;
        clk_gt_free       : in  std_logic; -- 25 MHz
        clk_eth_free      : in  std_logic; -- 50 MHz
        ext_rst_async         : in  std_logic;
        eth_txp       : out std_logic; -- the GT serial link I/O pins
        eth_txn       : out std_logic;
        eth_rxp       : in  std_logic;
        eth_rxn       : in  std_logic;
        fso_txp       : out std_logic; -- the  serial link I/O pins 
        fso_txn       : out std_logic;
        fso_rxp       : in  std_logic;
        fso_rxn       : in  std_logic;
        AXI_ETH_REG_IF_m  : in  AXI_Lite_master;
        AXI_ETH_REG_IF_s  : out AXI_Lite_slave;
        AXI_LET_m         : in  AXI_Lite_master;
        AXI_LET_s         : out AXI_Lite_slave;
        dcm_locked_0        : in  std_logic;
        dcm_locked_1        : in  std_logic;
        tx_ch1_modset_pwm : out std_logic;
        tx_ch2_modset_pwm : out std_logic;
        modset_pwm_enable : in  std_logic_vector(1 downto 0);
        modset_pwm_freq   : in  std_logic_vector(1 downto 0)
    );
end entity top_let;

architecture rtl of top_let is

    constant GND : std_logic := '0';
    constant VCC : std_logic := '1';

    constant PREAMBLE_IN : std_logic_vector(63 downto 0) := X"53225B1D0D73DF03";

    signal axis_mac_rx_tdata  : std_logic_vector(7 downto 0);
    signal axis_mac_rx_tkeep  : std_logic_vector(7 downto 0);
    signal axis_mac_rx_tlast  : std_logic;
    signal axis_mac_rx_tuser  : std_logic;
    signal axis_mac_rx_tvalid : std_logic;

    signal axis_mac_tx_tdata  : std_logic_vector(7 downto 0);
    signal axis_mac_tx_tkeep  : std_logic_vector(0 downto 0);
    signal axis_mac_tx_tlast  : std_logic;
    signal axis_mac_tx_tready : std_logic;
    signal axis_mac_tx_tuser  : std_logic;
    signal axis_mac_tx_tvalid : std_logic;

    signal ctl_tx_pause_req    : std_logic_vector(8 downto 0);
    signal ctl_tx_resend_pause : std_logic;
    signal global_tx_pause_req : std_logic;

    signal mac_rx_clk_out : std_logic;
    signal mac_rx_reset   : std_logic;
    signal mac_tx_clk_out : std_logic;
    signal mac_tx_reset   : std_logic;

    signal mac_tx_resetn : std_logic;
    signal mac_rx_resetn : std_logic;

    signal mac_mux_data  : std_logic_vector(8 - 1 downto 0);
    signal mac_mux_valid : std_logic;
    signal mac_mux_last  : std_logic;
    signal mac_mux_error : std_logic;

    signal rs_enc_input_valid : std_logic;

    signal enc_frame_counter       : std_logic_vector(23 downto 0);
    signal enc_frame_counter_valid : std_logic;

    signal loopback_framer    : std_logic;
    signal loopback_framer_rs : std_logic;
    signal loopback_fec       : std_logic;
    signal loopback_fso_mac   : std_logic;
    signal crc_enable         : std_logic;

    signal link_down_latched_reset_in : std_logic := '0';
    signal link_status_out            : std_logic;
    signal link_down_latched_out      : std_logic;
    signal init_done_out              : std_logic;
    signal init_retry_ctr_out         : std_logic_vector(3 downto 0);
    signal gtpowergood_out            : std_logic;
    signal txprgdivresetdone_out      : std_logic;
    signal rxprgdivresetdone_out      : std_logic;
    signal txpmaresetdone_out         : std_logic;
    signal rxpmaresetdone_out         : std_logic;
    signal gtwiz_reset_tx_done_out    : std_logic;
    signal gtwiz_reset_rx_done_out    : std_logic;

    signal hb_gtwiz_reset_all_axi_in                  : std_logic;
    signal fso_gt_reset                               : std_logic;
    signal hb0_gtwiz_reset_tx_pll_and_datapath_axi_in : std_logic_vector(0 downto 0);
    signal hb0_gtwiz_reset_tx_datapath_axi_in         : std_logic_vector(0 downto 0);
    signal hb_gtwiz_reset_rx_pll_and_datapath_axi_in  : std_logic;
    signal hb_gtwiz_reset_rx_datapath_axi_in          : std_logic;
    signal link_down_latched_reset_axi_in             : std_logic;

    signal dclk                : std_logic;
    signal init_clk            : std_logic;
    signal s_axi_aclk          : std_logic;
    signal let_refclk_sel      : std_logic;
    signal s_axi_aresetn       : std_logic;
    signal eth_rx_reset_in     : std_logic;
    signal eth_rx_user_rst_out : std_logic;
    signal eth_rx_clk_out      : std_logic;
    signal eth_tx_reset_in     : std_logic;
    signal eth_tx_user_rst_out : std_logic;
    signal eth_tx_clk_out      : std_logic;
    signal eth_channel_sel     : std_logic_vector(1 downto 0);
    signal fso_channel_sel     : std_logic_vector(1 downto 0);

    --signal let_refclk_nom          : std_logic;
    --signal let_refclk_red          : std_logic;
    signal let_gt_refclk_nom_out   : std_logic;
    signal let_gt_refclk_red_out   : std_logic;
    signal eth_rx_clk_out_int      : std_logic;
    signal eth_tx_clk_out_int      : std_logic;
    signal aur_sys_clk_int         : std_logic;
    signal aur_sys_rst_int         : std_logic;
    signal eth_rx_user_rst_out_int : std_logic;
    signal eth_tx_user_rst_out_int : std_logic;
    signal eth_rx_rst_n            : std_logic;
    signal eth_tx_rst_n            : std_logic;
    signal aur_sys_rst_n           : std_logic;
    signal fso_gt_loopback         : std_logic_vector(2 downto 0);
    signal fso_gt_loopback_in      : std_logic_vector(2 downto 0);

    signal txdiffctrl   : std_logic_vector(3 downto 0);
    signal txpostcursor : std_logic_vector(4 downto 0);
    signal txprecursor  : std_logic_vector(4 downto 0);
    signal rxpolinv     : std_logic;
    signal rxcdrlock    : std_logic;

    signal fso_phy_rx_clk        : std_logic;
    signal fso_phy_tx_clk        : std_logic;
    signal rx_data_good          : std_logic;
    signal gtwiz_userdata_rx_out : std_logic_vector(15 downto 0);
    signal fso_phy_rx_rst        : std_logic;
    signal fso_phy_rx_rstn       : std_logic;
    signal fso_phy_tx_rst        : std_logic;
    signal fso_phy_tx_rstn       : std_logic;
    signal rxfifo_tdata          : std_logic_vector(15 downto 0);
    signal rxfifo_tvalid         : std_logic;
    signal rxfifo_tready         : std_logic;
    signal fso_rx_axis_tdata     : std_logic_vector(7 downto 0);
    signal fso_rx_axis_tvalid    : std_logic;
    signal fso_rx_rst_int        : std_logic;
    signal fso_rx_rstn_int       : std_logic;
    signal fso_tx_rst_int        : std_logic;
    signal fso_tx_rstn_int       : std_logic;

    signal gtwiz_userdata_tx_in : std_logic_vector(15 downto 0);

    --signal clk100          : std_logic;
    signal clk25          : std_logic;
    signal clk156         : std_logic;
    signal reset_sync     : std_logic;
    signal reset_sync_156 : std_logic;
    signal reset_sync_25  : std_logic;

    --signal clk200          : std_logic;
    signal reset_sync_200   : std_logic;
    signal reset_sync_200_n : std_logic;

    signal cfg_acquisition_length : std_logic_vector(32 - 1 downto 0); -- hard-code values for now
    signal cfg_tracking_length    : std_logic_vector(32 - 1 downto 0);
    signal cfg_max_tracking_error : std_logic_vector(32 - 1 downto 0);
    signal cfg_preamble_max_corr  : std_logic_vector(6 downto 0);    -- CAGS Xcorr thresh
    signal disp_sum               : std_logic_vector(23 downto 0);

    signal deframer_in_axis_tdata  : std_logic_vector(8 - 1 downto 0);
    signal deframer_in_axis_tvalid : std_logic;
    signal deframer_in_axis_tlast  : std_logic;
    signal deframer_in_axis_tready : std_logic;
    signal rx_fifo_skipped_frame   : std_logic_vector(31 downto 0);

    signal deframer_out_axis_tdata  : std_logic_vector(8 - 1 downto 0);
    signal deframer_out_axis_tvalid : std_logic;
    signal deframer_out_axis_tlast  : std_logic;
    signal deframer_out_axis_tready : std_logic;
    signal deframer_out_axis_tuser  : std_logic;

    signal filter_axis_tdata  : std_logic_vector(8 - 1 downto 0);
    signal filter_axis_tvalid : std_logic;
    signal filter_axis_tlast  : std_logic;
    signal filter_axis_tready : std_logic;

    -- Framer
    signal framer_total_packet_counter          : std_logic_vector(31 downto 0);
    signal framer_total_packet_splitted_counter : std_logic_vector(31 downto 0);
    signal framer_total_data_frame_counter      : std_logic_vector(31 downto 0);
    signal framer_actual_data_frame_counter     : std_logic_vector(31 downto 0);
    signal framer_total_idle_frame_counter      : std_logic_vector(31 downto 0);
    signal framer_total_packet_drop             : std_logic_vector(31 downto 0);
    signal framer_total_prebuffer_packet_drop   : std_logic_vector(31 downto 0);
    signal framer_frame_length_error            : std_logic_vector(31 downto 0);
    signal framer_bitrate_in                    : std_logic_vector(31 downto 0);
    signal framer_packet_cnt_in                 : std_logic_vector(31 downto 0);

    -- Deframer
    signal deframer_total_packet_counter          : std_logic_vector(31 downto 0);
    signal deframer_total_payload_counter         : std_logic_vector(31 downto 0);
    signal deframer_total_packet_splitted_counter : std_logic_vector(31 downto 0);
    signal deframer_total_packet_merged_counter   : std_logic_vector(31 downto 0);
    signal deframer_total_data_frame_counter      : std_logic_vector(31 downto 0);
    signal deframer_total_idle_frame_counter      : std_logic_vector(31 downto 0);
    signal deframer_total_payload_error_counter   : std_logic_vector(31 downto 0);
    signal deframer_total_packet_error_counter    : std_logic_vector(31 downto 0);
    signal deframer_watchdog_reset_counter        : std_logic_vector(31 downto 0);
    signal deframer_packet_filter_cnt_in          : std_logic_vector(31 downto 0);
    signal deframer_packet_filter_cnt_out         : std_logic_vector(31 downto 0);
    signal deframer_frame_filter_cnt_in           : std_logic_vector(31 downto 0);
    signal deframer_frame_filter_cnt_out          : std_logic_vector(31 downto 0);
    signal deframer_bitrate_out                   : std_logic_vector(31 downto 0);
    signal deframer_packet_cnt_out                : std_logic_vector(31 downto 0);

    signal framer_monitor_warning_flag          : std_logic;
    signal framer_monitor_critical_error_flag   : std_logic;
    signal deframer_monitor_warning_flag        : std_logic;
    signal deframer_monitor_critical_error_flag : std_logic;

    signal framer_diagnostics                 : framer_diagnostics_t;
    signal deframer_diagnostics               : deframer_diagnostics_t;
    signal sda_debug                          : sda_debug_t;
    signal framer_health_monitor_statistics   : health_monitor_statistics_t;
    signal deframer_health_monitor_statistics : health_monitor_statistics_t;
    signal framer_warning_flag                : std_logic;
    signal framer_critical_error_flag         : std_logic;
    signal deframer_warning_flag              : std_logic;
    signal deframer_critical_error_flag       : std_logic;
    signal reserved                           : std_logic_vector(6 downto 0);
    signal clear_framer_statistics            : std_logic;
    signal clear_deframer_statistics          : std_logic;

    signal total_packet_counter          : std_logic_vector(31 downto 0);
    signal total_packet_splitted_counter : std_logic_vector(31 downto 0);
    signal total_packet_merged_counter   : std_logic_vector(31 downto 0);
    signal total_frame_error_counter     : std_logic_vector(31 downto 0);
    signal total_packet_error_counter    : std_logic_vector(31 downto 0);

    signal framer_out_axis_tdata  : std_logic_vector(8 - 1 downto 0);
    signal framer_out_axis_tvalid : std_logic;
    signal framer_out_axis_tlast  : std_logic;
    signal framer_out_axis_tready : std_logic;

    signal framer_in_axis_tdata  : std_logic_vector(8 - 1 downto 0);
    signal framer_in_axis_tvalid : std_logic;
    signal framer_in_axis_tlast  : std_logic;
    signal framer_in_axis_tuser  : std_logic;

    signal client_in_axis_if_m : t_axis_if_m;
    signal client_in_axis_if_s : t_axis_if_s;
    signal client_out_axis_if_m : t_axis_if_m;
    signal client_out_axis_if_s : t_axis_if_s;

    signal eth_rx_fifo_axis_tvalid : STD_LOGIC;
    signal eth_rx_fifo_axis_tready : STD_LOGIC;
    signal eth_rx_fifo_axis_tdata  : STD_LOGIC_VECTOR(7 DOWNTO 0);
    signal eth_rx_fifo_axis_tlast  : STD_LOGIC;
    signal eth_rx_fifo_axis_tuser  : STD_LOGIC;

    signal eth_tx_fifo_axis_tvalid : STD_LOGIC;
    signal eth_tx_fifo_axis_tready : STD_LOGIC;
    signal eth_tx_fifo_axis_tdata  : STD_LOGIC_VECTOR(7 DOWNTO 0);
    signal eth_tx_fifo_axis_tlast  : STD_LOGIC;
    signal eth_tx_fifo_axis_tuser  : STD_LOGIC;

    signal fso_tx_axis_tdata   : std_logic_vector(8 - 1 downto 0);
    signal fso_tx_axis_tdata_n : std_logic_vector(8 - 1 downto 0);
    signal fso_tx_axis_tvalid  : std_logic;
    signal fso_tx_axis_tlast   : std_logic;
    signal fso_tx_axis_tready  : std_logic;

    signal fso_tx_fifo_tdata  : std_logic_vector(15 downto 0);
    signal fso_tx_fifo_tvalid : std_logic;
    signal fso_tx_fifo_tready : std_logic;

    signal loopback_in : std_logic_vector(2 downto 0);

    signal ref_clk_25 : std_logic;

    signal clear_stat          : std_logic;
    signal clear_stat_sw       : std_logic;
    signal clear_stat_vio      : std_logic;
    signal total_frame_counter : std_logic_vector(31 downto 0);
    signal crc_error_counter   : std_logic_vector(31 downto 0);
    signal crc16_err_counter   : std_logic_vector(31 downto 0);
    signal crc32_err_counter   : std_logic_vector(31 downto 0);
    signal header_corrections  : std_logic_vector(31 downto 0);
    signal payload_corrections : std_logic_vector(31 downto 0);
    signal lpc_frame_counter   : std_logic_vector(31 downto 0);
    signal lpc_corrections     : std_logic_vector(31 downto 0);
    signal new_search_counter  : std_logic_vector(31 downto 0);
    signal sync_loss_counter   : std_logic_vector(31 downto 0);
    signal preamble_max_corr   : std_logic_vector(6 downto 0);  -- CAGS Xcorr thresh
    signal crc16_valid         : std_logic;
    signal crc16_correct       : std_logic;
    signal crc32_valid         : std_logic;
    signal crc32_correct       : std_logic;

    signal payload_cerr      : std_logic_vector(4 - 1 downto 0);
    signal payload_ncerr     : std_logic;
    signal payload_err_valid : std_logic;

    signal header_cerr      : std_logic_vector(4 - 1 downto 0);
    signal header_ncerr     : std_logic;
    signal header_err_valid : std_logic;

    signal lpc_correct_valid              : std_logic;
    signal lpc_correction                 : std_logic_vector(4 downto 0);
    signal corrections_per_second         : std_logic_vector(31 downto 0);
    signal header_corrections_per_second  : std_logic_vector(31 downto 0);
    signal payload_corrections_per_second : std_logic_vector(31 downto 0);
    signal lpc_corrections_per_second     : std_logic_vector(31 downto 0);
    signal corrections_rate               : std_logic_vector(31 downto 0);
    signal crc_errors_per_second          : std_logic_vector(31 downto 0);

    -- signals driven from VIO instance
    signal cfg_aquisition_length_vio  : std_logic_vector(32 - 1 downto 0);
    signal cfg_tracking_length_vio    : std_logic_vector(32 - 1 downto 0);
    signal cfg_max_tracking_error_vio : std_logic_vector(32 - 1 downto 0);
    signal cfg_preamble_max_corr_vio  : std_logic_vector(32 - 1 downto 0);  -- CAGS Xcorr thresh
    signal crc16_valid_vio            : std_logic;
    signal crc16_correct_vio          : std_logic;
    signal crc16_correct_reg_vio      : std_logic;
    signal crc32_valid_vio            : std_logic;
    signal crc32_correct_vio          : std_logic;
    signal crc32_correct_reg_vio      : std_logic;
    signal loopback_vio               : std_logic;
    signal rx_polinv_vio              : std_logic;
    signal reset_vio                  : std_logic;
    signal channel_sel_vio_enable     : std_logic;
    -- mux between normal and VIO signals
    signal reset_muxed                : std_logic;
    signal reset_aux_muxed            : std_logic;
    signal loopback_muxed             : std_logic_vector(2 downto 0);
    signal rx_polinv_mux              : std_logic;

    signal s_axis_pause_tdata : std_logic_vector(15 downto 0);
    signal cfg_pause_data     : std_logic_vector(15 downto 0);
    signal rate_snoop         : std_logic_vector(1 downto 0);

    signal lpc_frame_correct       : std_logic;
    signal lpc_frame_correct_valid : std_logic;
    signal lpc_frame_correct_reg   : std_logic_vector(32 - 1 downto 0);
    signal lpc_frame_correct_vio   : std_logic;

    signal payload_cerr_reg  : std_logic_vector(32 - 1 downto 0);
    signal payload_ncerr_vio : std_logic;

    signal header_cerr_reg  : std_logic_vector(32 - 1 downto 0);
    signal header_ncerr_vio : std_logic;

    signal resetn_sync_200    : std_logic;
    signal resetn_sync_100    : std_logic;
    signal resetn_sync_25     : std_logic;
    signal reset_sync_100     : std_logic;
    signal reset_eth_global_n : std_logic;

    signal channel_sel_rx     : std_logic;
    signal channel_sel_rx_vio : std_logic;
    signal channel_sel_tx     : std_logic;
    signal channel_sel_tx_vio : std_logic;
    signal data_inv_ena       : std_logic;

    signal bitrate_client_rx : std_logic_vector(31 downto 0);
    signal bitrate_client_tx : std_logic_vector(31 downto 0);

    signal pll_locked_vector : std_logic_vector(2 downto 0);

    signal S_AXIS_ETH_tdata_int  : STD_LOGIC_VECTOR(7 downto 0);
    signal S_AXIS_ETH_tlast_int  : STD_LOGIC;
    signal S_AXIS_ETH_tready_int : STD_LOGIC;
    signal S_AXIS_ETH_tuser_int  : STD_LOGIC_VECTOR(0 to 0);
    signal S_AXIS_ETH_tvalid_int : STD_LOGIC;

    signal fso_userdata_tx_out  : std_logic_vector(8 - 1 downto 0);
    signal fso_uservalid_tx_out : std_logic;
    signal fso_userclk_tx_out   : std_logic;
    signal fso_userrst_tx_out   : std_logic;
    signal fso_userdata_rx_out  : std_logic_vector(8 - 1 downto 0);
    signal fso_uservalid_rx_out : std_logic;
    signal fso_userclk_rx_out   : std_logic;
    signal fso_userrst_rx_out   : std_logic;


    signal fh_tx_fcch_if_m : t_fcch_if_m;
    signal fh_tx_fcch_if_s : t_fcch_if_s;
    signal fh_rx_fcch_if_m : t_fcch_if_m;
    signal fh_rx_fcch_if_s : t_fcch_if_s;

    signal fec_rx_axis_if_m : t_axis_if_m;
    signal fec_rx_axis_if_s : t_axis_if_s;
    signal fec_tx_axis_if_m : t_axis_if_m;
    signal fec_tx_axis_if_s : t_axis_if_s;
    
    signal reset_fso_rx  : std_logic;
    signal reset_fec_rx  : std_logic;
    signal reset_sda_rx  : std_logic;
    signal reset_let_sda : std_logic;
    signal rst_let_n : std_logic;

    signal preamble_synced : std_logic;
    signal fec_ctrl        : std_logic_vector(2 downto 0);
    signal fso_loopback_in : std_logic;
    signal tx_pause_valid  : std_logic;
    signal tx_pause_data   : std_logic_vector(15 downto 0);
    signal tx_pause_valid_int  : std_logic;
    signal tx_pause_data_int   : std_logic_vector(15 downto 0);

    signal r_vio_preamble_synced             : std_logic;                           
    signal r_vio_total_frame_counter         : std_logic_vector(31 downto 0);                   
    signal r_vio_crc_error_counter           : std_logic_vector(31 downto 0);               
    signal r_vio_crc16_err_counter           : std_logic_vector(31 downto 0);               
    signal r_vio_crc32_err_counter           : std_logic_vector(31 downto 0);               
    signal r_vio_new_search_counter          : std_logic_vector(31 downto 0);               
    signal r_vio_sync_loss_counter           : std_logic_vector(31 downto 0); 
    signal r_vio_preamble_max_corr           : std_logic_vector(6 downto 0);   -- CAGS Xcorr thresh
    signal r_vio_cfg_acquisition_length      : std_logic_vector(31 downto 0);                   
    signal r_vio_cfg_tracking_length         : std_logic_vector(31 downto 0);                   
    signal r_vio_cfg_max_tracking_error      : std_logic_vector(31 downto 0);                   
    signal r_vio_fso_loopback_in             : std_logic;               
    signal r_vio_fec_ctrl                    : std_logic_vector(2 downto 0);      
    --signal r_vio_deframer_total_payload_error_counter   : std_logic_vector(31 downto 0); 
    --signal r_vio_deframer_total_packet_error_counter    : std_logic_vector(31 downto 0); 
    --signal r_vio_deframer_total_packet_counter          : std_logic_vector(31 downto 0); 
    --signal r_vio_framer_total_packet_counter : std_logic_vector(31 downto 0); 
    signal r_vio_reset_vio : std_logic;

    signal r_vio_framer_total_packet_counter          : std_logic_vector(31 downto 0);
    signal r_vio_framer_total_packet_splitted_counter : std_logic_vector(31 downto 0);
    signal r_vio_framer_total_data_frame_counter      : std_logic_vector(31 downto 0);
    signal r_vio_framer_actual_data_frame_counter     : std_logic_vector(31 downto 0);
    signal r_vio_framer_total_idle_frame_counter      : std_logic_vector(31 downto 0);
    signal r_vio_framer_total_packet_drop             : std_logic_vector(31 downto 0);
    signal r_vio_framer_total_prebuffer_packet_drop   : std_logic_vector(31 downto 0);
    signal r_vio_framer_frame_length_error            : std_logic_vector(31 downto 0);
    signal r_vio_framer_bitrate_in                    : std_logic_vector(31 downto 0);
    signal r_vio_framer_packet_cnt_in                 : std_logic_vector(31 downto 0);

    -- Deframer
    signal r_vio_deframer_total_packet_counter          : std_logic_vector(31 downto 0);
    signal r_vio_deframer_total_payload_counter         : std_logic_vector(31 downto 0);
    signal r_vio_deframer_total_packet_splitted_counter : std_logic_vector(31 downto 0);
    signal r_vio_deframer_total_packet_merged_counter   : std_logic_vector(31 downto 0);
    signal r_vio_deframer_total_data_frame_counter      : std_logic_vector(31 downto 0);
    signal r_vio_deframer_total_idle_frame_counter      : std_logic_vector(31 downto 0);
    signal r_vio_deframer_total_payload_error_counter   : std_logic_vector(31 downto 0);
    signal r_vio_deframer_total_packet_error_counter    : std_logic_vector(31 downto 0);
    signal r_vio_deframer_watchdog_reset_counter        : std_logic_vector(31 downto 0);
    signal r_vio_deframer_packet_filter_cnt_in          : std_logic_vector(31 downto 0);
    signal r_vio_deframer_packet_filter_cnt_out         : std_logic_vector(31 downto 0);
    signal r_vio_deframer_frame_filter_cnt_in           : std_logic_vector(31 downto 0);
    signal r_vio_deframer_frame_filter_cnt_out          : std_logic_vector(31 downto 0);
    signal r_vio_deframer_bitrate_out                   : std_logic_vector(31 downto 0);
    signal r_vio_deframer_packet_cnt_out                : std_logic_vector(31 downto 0);


    attribute keep : string;
    attribute keep of rx_fifo_skipped_frame : signal is "true";
    attribute keep of global_tx_pause_req : signal is "true";
    signal reset_sync_let : std_logic;
    signal resetn_sync_let : std_logic;    
    signal reset_sync_ldpc : std_logic;
    signal resetn_sync_ldpc : std_logic;
    signal reset_sync_gt_free : std_logic;
    signal resetn_sync_gt_free : std_logic;
    signal reset_sync_eth_free : std_logic;
    signal resetn_sync_eth_free : std_logic;


COMPONENT vio_fec
  PORT (
    clk : IN STD_LOGIC;
    probe_in0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_in1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_in2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_in3 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_in4 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_in5 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_in6 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_in7 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_in8 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_in9 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_in10 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_out0 : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_out1 : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_out2 : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_out3 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_out4 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_out5 : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    probe_out6 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0)
  );
END COMPONENT;


attribute ASYNC_REG : string;

	attribute ASYNC_REG of preamble_synced             : signal is "TRUE"; --: std_logic;                           
	attribute ASYNC_REG of total_frame_counter         : signal is "TRUE"; --: std_logic_vector(31 downto 0);                   
	attribute ASYNC_REG of crc_error_counter           : signal is "TRUE"; --: std_logic_vector(31 downto 0);               
	attribute ASYNC_REG of crc16_err_counter           : signal is "TRUE"; --: std_logic_vector(31 downto 0);               
	attribute ASYNC_REG of crc32_err_counter           : signal is "TRUE"; --: std_logic_vector(31 downto 0);               
	attribute ASYNC_REG of new_search_counter          : signal is "TRUE"; --: std_logic_vector(31 downto 0);               
	attribute ASYNC_REG of sync_loss_counter           : signal is "TRUE"; --: std_logic_vector(31 downto 0);               
	attribute ASYNC_REG of cfg_acquisition_length      : signal is "TRUE"; --: std_logic_vector(31 downto 0);                   
	attribute ASYNC_REG of cfg_tracking_length         : signal is "TRUE"; --: std_logic_vector(31 downto 0);                   
	attribute ASYNC_REG of cfg_max_tracking_error      : signal is "TRUE"; --: std_logic_vector(31 downto 0);   
	attribute ASYNC_REG of cfg_preamble_max_corr       : signal is "TRUE"; --; std_logic_vector(6 downto 0);                
	attribute ASYNC_REG of fso_loopback_in             : signal is "TRUE"; --: std_logic;               
	attribute ASYNC_REG of fec_ctrl                    : signal is "TRUE"; --: std_logic_vector(2 downto 0);      
	attribute ASYNC_REG of deframer_total_payload_error_counter   : signal is "TRUE"; --: std_logic_vector(31 downto 0); 
	attribute ASYNC_REG of deframer_total_packet_error_counter    : signal is "TRUE"; --: std_logic_vector(31 downto 0); 
	attribute ASYNC_REG of deframer_total_packet_counter          : signal is "TRUE"; --: std_logic_vector(31 downto 0); 
	attribute ASYNC_REG of framer_total_packet_counter : signal is "TRUE"; --: std_logic_vector(31 downto 0); 
	attribute ASYNC_REG of reset_vio : signal is "TRUE"; --: std_logic;
begin

--    cfg_acquisition_length <= std_logic_vector(to_unsigned(4, 32));
--    cfg_tracking_length    <= std_logic_vector(to_unsigned(16, 32));
--    cfg_max_tracking_error <= std_logic_vector(to_unsigned(4, 32));

    --    clk25                  <= ref_clk_25;
    --    clk25_out              <= clk25;
    --en_fso_lvl_trs      <= '1'; -- apparently needed to power-up FSO board.
    fso_phy_tx_rstn        <= not (fso_phy_tx_rst);
    fso_phy_rx_rstn        <= not (fso_phy_rx_rst);
    fso_rx_rst_int         <= reset_sync_200 or fso_phy_rx_rst;
    fso_rx_rstn_int        <= not (fso_rx_rst_int);
    fso_tx_rst_int         <= reset_sync_200 or fso_phy_tx_rst;
    fso_tx_rstn_int        <= not (fso_tx_rst_int);
    reset_sync_200_n       <= not reset_sync_200;
    mac_tx_resetn          <= not (mac_tx_reset);
    mac_rx_resetn          <= not (mac_rx_reset);
    reset_eth_global_n     <= mac_tx_reset or mac_rx_reset;
    -- sys_clk_nom_en      <= '1';    -- enable clocks on hardware
    -- sys_clk_red_en      <= '0';    
    -- let_clk_nom_en      <= '1';    
    -- let_clk_red_en      <= '0';  
    eth_rx_reset_in        <= '0';
    eth_tx_reset_in        <= '0';
    let_refclk_sel         <= '0';      -- only the nominal clock is connected to the FSO PHY so hard-wire
    --fso_gt_reset           <= reset_sync_25 or hb_gtwiz_reset_all_axi_in;
    --fso_tx_axis_tdata_n    <= not fso_tx_axis_tdata;
    -- Enable FSO Rx line drivers
    --fso_lim_dis_1_1v8   <= '1';
    --fso_lim_dis_2_1v8   <= '1';
    --
    -- hard-coded configurations
--    cfg_acquisition_length  <= std_logic_vector(to_unsigned(8, 32));
--    cfg_tracking_length    <= std_logic_vector(to_unsigned(32, 32));
--    cfg_max_tracking_error <= std_logic_vector(to_unsigned(10, 32));

    bitrate_client_rx <= framer_bitrate_in; -- 
    bitrate_client_tx <= deframer_bitrate_out;
    
    cfg_pause_data <= x"1FFF";

    --reset_muxed     <= reset;
    reset_let_sda   <= '0';
    reset_aux_muxed <= reset_let_sda or reset_vio;
    --loopback_muxed  <= loopback_in(2) & (loopback_in(1) or loopback_vio) & loopback_in(0);
    --rx_polinv_mux   <= rxpolinv or rx_polinv_vio;
    --rst_let_n <= not (reset_sync_let);

    -- CDC fifo to convert tx ethernet data (coming from deframer) from 200 MHz to 125 clock rate.
    eth_mac_tx_fifo : dc_fifo_eth_side
    port map (
        m_aclk               => mac_tx_clk_out,            -- : IN STD_LOGIC;
        s_aclk               => clk_let,                    -- : IN STD_LOGIC;
        s_aresetn            => resetn_sync_let,             -- : IN STD_LOGIC;
        s_axis_tvalid        => client_out_axis_if_m.tvalid,  -- : IN STD_LOGIC;
        s_axis_tready        => client_out_axis_if_s.tready ,  -- : OUT STD_LOGIC;
        s_axis_tdata         => client_out_axis_if_m.tdata ,   -- : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axis_tlast         => client_out_axis_if_m.tlast ,   -- : IN STD_LOGIC;
        s_axis_tuser(0)      => client_out_axis_if_m.tuser,    -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axis_tvalid        => eth_tx_fifo_axis_tvalid ,       -- : OUT STD_LOGIC;
        m_axis_tready        => eth_tx_fifo_axis_tready ,       -- : IN STD_LOGIC;
        m_axis_tdata         => eth_tx_fifo_axis_tdata ,        -- : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axis_tlast         => eth_tx_fifo_axis_tlast ,        -- : OUT STD_LOGIC;
        m_axis_tuser(0)      => eth_tx_fifo_axis_tuser          -- : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
    );

    eth_mac_rx_fifo : dc_fifo_eth_side
    port map (
        m_aclk               => clk_let,            -- : IN STD_LOGIC;
        s_aclk               => mac_rx_clk_out,                    -- : IN STD_LOGIC;
        s_aresetn            => mac_rx_resetn,             -- : IN STD_LOGIC;
        s_axis_tvalid        => eth_rx_fifo_axis_tvalid,  -- : IN STD_LOGIC;
        s_axis_tready        => open, --eth_rx_fifo_axis_tready ,  -- : OUT STD_LOGIC;
        s_axis_tdata         => eth_rx_fifo_axis_tdata ,   -- : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axis_tlast         => eth_rx_fifo_axis_tlast ,   -- : IN STD_LOGIC;
        s_axis_tuser(0)      => eth_rx_fifo_axis_tuser,    -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axis_tvalid        => client_in_axis_if_m.tvalid ,       -- : OUT STD_LOGIC;
        m_axis_tready        => client_in_axis_if_s.tready ,       -- : IN STD_LOGIC;
        m_axis_tdata         => client_in_axis_if_m.tdata ,        -- : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axis_tlast         => client_in_axis_if_m.tlast ,        -- : OUT STD_LOGIC;
        m_axis_tuser(0)      => client_in_axis_if_m.tuser          -- : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
    );

    --- NEW ---
    inst_let_sda_top_t1 : entity let_sda_lib.let_sda_top_t1
        generic map(
            G_MINIMUN_FREE_BYTES_TO_SPLIT => 8,--: NATURAL := 8;
            G_STARTUP_DELAY               => 30 --: INTEGER := 30000
        )
        Port map(
            clk                                => clk_let, --: in  std_logic; -- Main clock 200 MHz
            rst                                => reset_sync_let, --: in  std_logic;

            -- Input interface ETH
            ----------------------
            client_in_axis_if_m       => client_in_axis_if_m, --: inout t_axis_if;
            client_in_axis_if_s       => client_in_axis_if_s, --: inout t_axis_if;

            cfg_pause_data                         => cfg_pause_data, --: in std_logic_vector(15 downto 0); -- TODO: Connection missing
            tx_pause_valid                         => tx_pause_valid_int, --: out std_logic;
            tx_pause_data                          => tx_pause_data_int, --: out std_logic_vector(15 downto 0);
            rx_fifo_skipped_frame                  => rx_fifo_skipped_frame, --: out std_logic_vector(32 - 1 downto 0);

            -- Output interface ETH
            ----------------------
            client_out_axis_if_m                     => client_out_axis_if_m, --deframer_out_axis_tdata, --: out std_logic_vector(8 - 1 downto 0);
            client_out_axis_if_s                     => client_out_axis_if_s, --deframer_out_axis_tdata, --: out std_logic_vector(8 - 1 downto 0);

            -- Input interface Creonic
            ----------------------
            fec_in_axis_if_m                 => fec_rx_axis_if_m, 
            fec_in_axis_if_s                 => fec_rx_axis_if_s, 
            
            --preamble_synced                        => preamble_synced, --: in std_logic;
            crc16_valid                            => '1', --crc16_valid, --: in std_logic;
            crc16_correct                          => '1', --crc16_correct, --: in std_logic;
            crc32_valid                            => '1', --crc32_valid, --: in std_logic;
            crc32_correct                          => '1', --crc32_correct, --: in std_logic;

            -- Output interface Creonic
            ----------------------
            fec_out_axis_if_m                        => fec_tx_axis_if_m, --: out  std_logic_vector(8 - 1 downto 0);
            fec_out_axis_if_s                        => fec_tx_axis_if_s, --: out  std_logic_vector(8 - 1 downto 0);

            -- Framer Health Monitor
            ----------------------
            framer_warning_flag                    => framer_warning_flag, --: out  std_logic;
            framer_critical_error_flag             => framer_critical_error_flag, --: out  std_logic;
            framer_health_monitor_statistics       => framer_health_monitor_statistics, --: out health_monitor_statistics_t;

            -- Deframer Health Monitor
            ----------------------
            deframer_warning_flag                  => deframer_warning_flag, --: out  std_logic;
            deframer_critical_error_flag           => deframer_critical_error_flag, --: out  std_logic;
            deframer_health_monitor_statistics     => deframer_health_monitor_statistics, --: out health_monitor_statistics_t;

            -- FCCH
            ----------------------
            -- From Register Map
            fh_tx_fcch_if_m   => fh_tx_fcch_if_m, --: inout t_fcch_if;
            fh_tx_fcch_if_s   => fh_tx_fcch_if_s, --: inout t_fcch_if;
    
    
            -- To Register Map
            fh_rx_fcch_if_m   => fh_rx_fcch_if_m, --: inout t_fcch_if;
            fh_rx_fcch_if_s   => fh_rx_fcch_if_s, --: inout t_fcch_if;
            
            -- Statistics
            ----------------------
            clear_stat                             => clear_stat, --clear_stat, --: in  std_logic;
            -- Framer
            framer_total_packet_counter            => framer_total_packet_counter, --: out std_logic_vector(31 downto 0);
            framer_total_packet_splitted_counter   => framer_total_packet_splitted_counter, --: out std_logic_vector(31 downto 0);
            framer_total_data_frame_counter        => framer_total_data_frame_counter, --: out std_logic_vector(31 downto 0);
            framer_actual_data_frame_counter       => framer_actual_data_frame_counter, --: out std_logic_vector(31 downto 0);
            framer_total_idle_frame_counter        => framer_total_idle_frame_counter, --: out std_logic_vector(31 downto 0);
            framer_total_packet_drop               => framer_total_packet_drop, --: out std_logic_vector(31 downto 0);
            framer_total_prebuffer_packet_drop     => framer_total_prebuffer_packet_drop, --: out std_logic_vector(31 downto 0);
            framer_frame_length_error              => framer_frame_length_error, --: out std_logic_vector(31 downto 0);
            framer_bitrate_in                      => framer_bitrate_in, --: out std_logic_vector(31 downto 0);
            framer_packet_cnt_in                   => framer_packet_cnt_in, --: out std_logic_vector(31 downto 0);

            -- Deframer
            deframer_total_packet_counter          => deframer_total_packet_counter, --: out std_logic_vector(31 downto 0);
            deframer_total_payload_counter         => deframer_total_payload_counter, --: out std_logic_vector(31 downto 0);
            deframer_total_packet_splitted_counter => deframer_total_packet_splitted_counter, --: out std_logic_vector(31 downto 0);
            deframer_total_packet_merged_counter   => deframer_total_packet_merged_counter, --: out std_logic_vector(31 downto 0);
            deframer_total_data_frame_counter      => deframer_total_data_frame_counter, --: out std_logic_vector(31 downto 0);
            deframer_total_idle_frame_counter      => deframer_total_idle_frame_counter, --: out std_logic_vector(31 downto 0);
            deframer_total_payload_error_counter   => deframer_total_payload_error_counter, --: out std_logic_vector(31 downto 0);
            deframer_total_packet_error_counter    => deframer_total_packet_error_counter, --: out std_logic_vector(31 downto 0);
            deframer_watchdog_reset_counter        => deframer_watchdog_reset_counter, --: out std_logic_vector(31 downto 0);
            deframer_packet_filter_cnt_in          => deframer_packet_filter_cnt_in, --: out std_logic_vector(31 downto 0);
            deframer_packet_filter_cnt_out         => deframer_packet_filter_cnt_out, --: out std_logic_vector(31 downto 0);
            deframer_frame_filter_cnt_in           => deframer_frame_filter_cnt_in, --: out std_logic_vector(31 downto 0);
            deframer_frame_filter_cnt_out          => deframer_frame_filter_cnt_out, --: out std_logic_vector(31 downto 0);
            deframer_bitrate_out                   => deframer_bitrate_out, --: out std_logic_vector(31 downto 0);
            deframer_packet_cnt_out                => deframer_packet_cnt_out, --: out std_logic_vector(31 downto 0);

            sda_debug                              => sda_debug, --: out sda_debug_t;

            -- Version control
            ----------------------
            version_mayor                          => open, --: out unsigned(7 downto 0);
            version_minor                          => open --: out unsigned(7 downto 0)
        );

        client_in_axis_if_m.tstart  <= '0';
        client_out_axis_if_m.tstart <= '0';
        --fec_tx_axis_if_m.tstart     <= '0';
        fec_rx_axis_if_m.tstart     <= '0'; 
        fh_rx_fcch_if_m.valid       <= '0'; 
        fh_tx_fcch_if_s.rd_ena      <= '0';

    --- NEW ---
    inst_frame_fec_top : entity frame_decoding.frame_fec_top
        port map(
            ldpc_clk               => clk_ldpc,
            ldpc_rst               => reset_sync_ldpc,
            clk                    => clk_let,
            rst                    => reset_sync_let,
            preamble               => PREAMBLE_IN,
            cfg_acquisition_length => cfg_acquisition_length,
            cfg_tracking_length    => cfg_tracking_length,
            cfg_max_tracking_error => cfg_max_tracking_error,
            cfg_preamble_max_corr  => cfg_preamble_max_corr,   -- CAGS Xcorr thresh
            tx_axis_ctrl_tready    => open,
            tx_axis_ctrl_tvalid    => '1', --tx_axis_ctrl_tvalid,
            tx_axis_ctrl_tdata     => fec_ctrl, -- TODO: fec_ctrl need to be configured with registers
            -- TX data input - header and payload data
            ----------------------
            tx_axis_tready         => fec_tx_axis_if_s.tready, --framer_out_axis_tready,
            tx_axis_tvalid         => fec_tx_axis_if_m.tvalid, --framer_out_axis_tvalid,
            tx_axis_tlast          => fec_tx_axis_if_m.tlast, --framer_out_axis_tlast,
            tx_axis_tdata          => fec_tx_axis_if_m.tdata, --framer_out_axis_tdata,
            -- Output data handling
            -----------------------
            ch_tx_axis_tready      => fso_tx_axis_tready,
            ch_tx_axis_tvalid      => fso_tx_axis_tvalid,
            ch_tx_axis_tlast       => fso_tx_axis_tlast,
            ch_tx_axis_tdata       => fso_tx_axis_tdata,
            ch_rx_axis_ctrl_tready => open, --ch_rx_axis_ctrl_tready,
            ch_rx_axis_ctrl_tvalid => '1', --ch_rx_axis_ctrl_tvalid,
            ch_rx_axis_ctrl_tdata  => fec_ctrl, --"000", --ch_rx_axis_ctrl_tdata,
            -- RX data input
            ----------------------
            ch_rx_axis_tvalid      => fso_rx_axis_tvalid, --ch_rx_axis_tvalid,
            ch_rx_axis_tdata       => fso_rx_axis_tdata, --ch_rx_axis_tdata,
            -- Status
            -----------------------
            crc16_valid            => crc16_valid,
            crc16_correct          => crc16_correct,
            crc32_valid            => crc32_valid,
            crc32_correct          => crc32_correct,
            preamble_synced        => preamble_synced,
            -- More diagnostic informations
            -------------------------------------------------------
            clear_stat             => clear_stat,
            total_frame_counter    => total_frame_counter,
            crc_error_counter      => crc_error_counter,
            crc16_err_counter      => crc16_err_counter,
            crc32_err_counter      => crc32_err_counter,
            -- Stats from the preamble sync
            new_search_counter     => new_search_counter,
            sync_loss_counter      => sync_loss_counter,
            -- RX data output - header and payload data
            -----------------------
            rx_axis_tready         => fec_rx_axis_if_s.tready, --deframer_in_axis_tready,
            rx_axis_tvalid         => fec_rx_axis_if_m.tvalid, --deframer_in_axis_tvalid,
            rx_axis_tlast          => fec_rx_axis_if_m.tlast, --deframer_in_axis_tlast,
            rx_axis_tdata          => fec_rx_axis_if_m.tdata --deframer_in_axis_tdata
        );

    inst_eth_2g5_top : entity work.eth_2g5_top
        port map(
            s_axi_resetn               => s_axi_resetn, --TODO: reset from BD
            glbl_rst            => reset_sync_eth_free,
            ref_clk_50          => clk_eth_free,
            clk_ref156          => clk_ref_eth, --clk_ref156,
            --clk_ref156_n          => clk_ref_eth_n, --clk_ref156,
            eth_txp         => eth_txp,
            eth_txn         => eth_txn,
            eth_rxp         => eth_rxp,
            eth_rxn         => eth_rxn,
            s_axi_aclk          => clk_sys,
            AXI_ETH_REG_IF_m    => AXI_ETH_REG_IF_m,
            AXI_ETH_REG_IF_s    => AXI_ETH_REG_IF_s,
            rx_user_rst_out     => mac_rx_reset,
            rx_clk_out          => mac_rx_clk_out,
            rx_axis_tvalid      => eth_rx_fifo_axis_tvalid,
            rx_axis_tdata       => eth_rx_fifo_axis_tdata,
            rx_axis_tlast       => eth_rx_fifo_axis_tlast,
            rx_axis_tuser       => eth_rx_fifo_axis_tuser,
            tx_user_rst_out     => mac_tx_reset,
            tx_clk_out          => mac_tx_clk_out,
            tx_axis_tready      => eth_tx_fifo_axis_tready,
            tx_axis_tvalid      => eth_tx_fifo_axis_tvalid,
            tx_axis_tdata       => eth_tx_fifo_axis_tdata,
            tx_axis_tlast       => eth_tx_fifo_axis_tlast,
            tx_axis_tuser       => eth_tx_fifo_axis_tuser,
            s_axis_pause_tdata  => tx_pause_data, 
            s_axis_pause_tvalid => '0' --tx_pause_valid
        );

    inst_fso_2g5_top : entity work.fso_2g5_top
        port map(
            clk_fec            => clk_let,
            reset_fec          => reset_sync_let,
            rst_async          => reset_sync_gt_free,
            refclk_p             => clk_ref_fso_p,
            refclk_n             => clk_ref_fso_n,
            freerun_clk        => clk_gt_free,
            fso_txp        => fso_txp,
            fso_txn        => fso_txn,
            fso_rxp        => fso_rxp,
            fso_rxn        => fso_rxn,
            fso_rx_axis_tdata  => fso_rx_axis_tdata,
            fso_rx_axis_tvalid => fso_rx_axis_tvalid,
            fso_tx_axis_tdata  => fso_tx_axis_tdata,
            fso_tx_axis_tvalid => fso_tx_axis_tvalid,
            fso_tx_axis_tready => fso_tx_axis_tready,
            data_good_in       => preamble_synced,
            loopback_in        => fso_loopback_in
        );

    -- TODO: Review this
    --Inst_bit_sync : entity share_let_1g.bit_sync
    --    port map(
    --        clk_in   => clk_tx_eth,
    --        data_in  => deframer_out_axis_tready,
    --        data_out => tx_pause_ready
    --    );

    inst_fifo_pause_frame : entity work.fifo_pause_frame
        PORT MAP (
          rst         => reset_sync_let,
          wr_clk      => clk_let,
          rd_clk      => mac_tx_clk_out,
          din         => tx_pause_data_int,
          wr_en       => tx_pause_valid_int,
          rd_en       => '1', --deframer_out_axis_tready, -- TODO: Needs review
          dout        => tx_pause_data,
          full        => open,
          empty       => open,
          valid       => tx_pause_valid
        );
    -------------------------------------------------------------------------------
    -------------------------------------------------------------------------------

    -- instantiate FSO PHY single channel version
    --    inst_fso_phy : entity let_sda_lib.fso_phy_lvds
    --        port map(
    --            clk_fec              => clk200, -- : in std_logic; -- clock at which the FEC is running
    --            reset_fec            => reset_sync_200, -- : in std_logic;
    --            sys_clk              => clk100, -- : in std_logic;
    --            reset                => reset_sync_100, --reset_muxed, -- : in std_logic;
    --            reset_sw             => reset_fso_rx,
    --            refclk               => let_refclk_nom, -- : in std_logic; -- ref clock for PLL, 125 MHz LET clock
    --            channel_sel_rx       => channel_sel_rx, -- : in std_logic; -- 0 wave a, 1 wave b
    --            channel_sel_tx       => channel_sel_tx, -- : in std_logic; -- 0 wave a, 1 wave b
    --            data_inv_ena         => data_inv_ena, -- : in std_logic; -- 0 wave a, 1 wave b
    --            fso_ch0_txp          => fso_ch0_txp, -- : out std_logic; -- the  serial link I/O pins
    --            fso_ch0_txn          => fso_ch0_txn, -- : out std_logic;
    --            fso_ch0_rxp          => fso_ch0_rxp, -- : in std_logic;
    --            fso_ch0_rxn          => fso_ch0_rxn, -- : in std_logic;
    --            fso_ch0_clkp         => fso_ch0_clkp, -- : in std_logic;
    --            fso_ch0_clkn         => fso_ch0_clkn, -- : in std_logic;
    --            fso_ch1_txp          => fso_ch1_txp, -- : out std_logic; -- the  serial link I/O pins
    --            fso_ch1_txn          => fso_ch1_txn, -- : out std_logic;
    --            fso_ch1_rxp          => fso_ch1_rxp, -- : in std_logic;
    --            fso_ch1_rxn          => fso_ch1_rxn, -- : in std_logic;
    --            fso_ch1_clkp         => fso_ch1_clkp, -- : in std_logic;
    --            fso_ch1_clkn         => fso_ch1_clkn, -- : in std_logic;
    --            fso_rx_axis_tdata    => fso_rx_axis_tdata, -- : out  std_logic_vector(8 - 1 downto 0);-- RX data input FSO -> FEC decoder stream
    --            fso_rx_axis_tvalid   => fso_rx_axis_tvalid, -- : out  std_logic;
    --            fso_tx_axis_tdata    => fso_tx_axis_tdata_n, -- : in  std_logic_vector(8 - 1 downto 0);    -- interface  FEC FEC -> FSO data stream
    --            fso_tx_axis_tvalid   => fso_tx_axis_tvalid, -- : in  std_logic;
    --            fso_tx_axis_tready   => fso_tx_axis_tready, -- : out std_logic;
    --            tx_pll_locked        => pll_locked_vector(0), -- : out std_logic;
    --            rx_pll_locked        => pll_locked_vector(1), -- : out std_logic;
    --            rx_pll_locked_red    => pll_locked_vector(2),
    --            AXI_FSO_PHY_TXPLL_m  => AXI_FSO_PHY_TXPLL_m, -- :  in AXI_Lite_master;
    --            AXI_FSO_PHY_TXPLL_s  => AXI_FSO_PHY_TXPLL_s, -- : out AXI_Lite_slave;
    --            --            AXI_FSO_PHY_RXPLL_m     => AXI_FSO_PHY_RXPLL_m, --: in  AXI_Lite_master;
    --            --            AXI_FSO_PHY_RXPLL_s     => AXI_FSO_PHY_RXPLL_s, --: out AXI_Lite_slave
    --            --            AXI_FSO_PHY_RXPLL_RED_m => AXI_FSO_PHY_RXPLL_RED_m, --: in  AXI_Lite_master;
    --            --            AXI_FSO_PHY_RXPLL_RED_s => AXI_FSO_PHY_RXPLL_RED_s, --: out AXI_Lite_slave
    --            tx_ch1_modset_pwm    => tx_ch1_modset_pwm, --: out    std_logic;
    --            tx_ch2_modset_pwm    => tx_ch2_modset_pwm, --: out    std_logic;
    --            modset_pwm_enable    => modset_pwm_enable, --: in    std_logic_vector(1 downto 0);
    --            modset_pwm_freq      => modset_pwm_freq, --: in    std_logic_vector(1 downto 0)
    --            -- Signals needed for ranging:
    --            fso_userdata_tx_out  => fso_userdata_tx_out, --: out std_logic_vector(8 - 1 downto 0);
    --            fso_uservalid_tx_out => fso_uservalid_tx_out, --: out std_logic;
    --            fso_userclk_tx_out   => fso_userclk_tx_out, --: out std_logic;
    --            fso_userrst_tx_out   => fso_userrst_tx_out, --: out std_logic;
    --            fso_userdata_rx_out  => fso_userdata_rx_out, --: out std_logic_vector(8 - 1 downto 0);
    --            fso_uservalid_rx_out => fso_uservalid_rx_out, --: out std_logic; 
    --            fso_userclk_rx_out   => fso_userclk_rx_out, --: out std_logic;
    --            fso_userrst_rx_out   => fso_userrst_rx_out --: out std_logic
    --        );

    --inst_fso_phy : entity let_sda_lib.fso_phy_top  
    --    port map (
    --        gt_refclk                     => let_refclk_nom,               -- : in  std_logic;
    --        clk_fec                       => clk200,                       -- : in std_logic; -- clock at which the FEC is running
    --        reset_fec                     => reset_sync_200,               -- : in std_logic;
    --        clk25                         => clk25,                        -- : in std_logic;
    --        channel_sel                   => channel_sel,                  -- : in std_logic; -- 0 wave a, 1 wave b
    --        ch0_gthrxn_in                 => aur_gt0_rxn,                  -- : in  std_logic;
    --        ch0_gthrxp_in                 => aur_gt0_rxp,                  -- : in  std_logic;
    --        ch0_gthtxn_out                => aur_gt0_txn,                  -- : out std_logic;
    --        ch0_gthtxp_out                => aur_gt0_txp,                  -- : out std_logic;
    --        ch1_gthrxn_in                 => aur_gt1_rxn,                  -- : in  std_logic;
    --        ch1_gthrxp_in                 => aur_gt1_rxp,                  -- : in  std_logic;
    --        ch1_gthtxn_out                => aur_gt1_txn,                  -- : out std_logic;
    --        ch1_gthtxp_out                => aur_gt1_txp,                  -- : out std_logic;
    --        hb_gtwiz_reset_all_in         => fso_gt_reset, -- reset_sync_25,                -- : in std_logic;
    --        rx_reset_out                  => fso_phy_rx_rst,               -- : out std_logic;
    --        rx_usrclk2_out                => fso_phy_rx_clk,               -- : out std_logic;
    --        fso_rx_axis_tdata             => fso_rx_axis_tdata,            -- : out  std_logic_vector(8 - 1 downto 0);
    --        fso_rx_axis_tvalid            => fso_rx_axis_tvalid, --  : out  std_logic;
    --        data_good_in                  => rx_data_good,                 -- : in  std_logic;
    --        tx_reset_out                  => fso_phy_tx_rst,               -- : out std_logic;
    --        tx_usrclk2_out                => fso_phy_tx_clk,               -- : out std_logic;
    --        fso_tx_axis_tdata             => fso_tx_axis_tdata  , --: in  std_logic_vector(8 - 1 downto 0);
    --        fso_tx_axis_tvalid            => fso_tx_axis_tvalid , --: in  std_logic;
    --        fso_tx_axis_tlast             => fso_tx_axis_tlast  , --: in  std_logic;
    --        fso_tx_axis_tready            => fso_tx_axis_tready , --: out std_logic;
    --        link_down_latched_reset_in    => link_down_latched_reset_in,   -- : in  std_logic;
    --        link_status_out               => link_status_out ,   -- : out std_logic;
    --        link_down_latched_out         => link_down_latched_out ,   -- : out std_logic;
    --        init_done_out                 => init_done_out ,   -- : out std_logic;
    --        init_retry_ctr_out            => init_retry_ctr_out ,   -- : out std_logic_vector(3 downto 0);
    --        gtpowergood_out               => gtpowergood_out ,   -- : out std_logic;
    --        txpmaresetdone_out            => txpmaresetdone_out ,   -- : out std_logic;
    --        rxpmaresetdone_out            => rxpmaresetdone_out ,   -- : out std_logic;
    --        gtwiz_reset_tx_done_out       => gtwiz_reset_tx_done_out ,   -- : out std_logic;
    --        gtwiz_reset_rx_done_out       => gtwiz_reset_rx_done_out ,   -- : out std_logic;
    --        drpaddr_in                    => fso_gt_drpaddr,   -- : in std_logic_vector( 8 downto 0);
    --        drpclk_in(0)                  => clk25,   -- : in std_logic_vector (0 downto 0);
    --        drpdi_in                      => fso_gt_drpdi,   -- : in std_logic_vector(15 downto 0);
    --        drpen_in(0)                   => fso_gt_drpen,   -- : in std_logic_vector (0 downto 0);
    --        drpwe_in(0)                   => fso_gt_drpwe,   -- : in std_logic_vector (0 downto 0);
    --        drpdo_out                     => fso_gt_drpdo,   -- : out std_logic_vector(15 downto 0);
    --        drprdy_out(0)                 => fso_gt_drprdy ,   -- : out std_logic_vector (0 downto 0) 
    --        txdiffctrl_in                 => txdiffctrl, 
    --        txpostcursor_in               => txpostcursor,
    --        txprecursor_in                => txprecursor,
    --        rxpolinv_in                   => rx_polinv_mux,
    --        rxcdrlock_out                 => rxcdrlock,
    --        loopback_in                   => loopback_muxed
    --    );	

    --
    -- REPLACE WITH NEW REG MAP!!!
    --

    --    inst_axi4lite_ctrl_let : entity let_sda_lib.axi4lite_ctrl_let1g
    --        port map(
    --            clk                   => clk200,
    --            rst                   => reset_sync_200,
    --            -- Write address
    --            s_axi_awaddr          => AXI_LET_m.awaddr(15 downto 0),
    --            s_axi_awvalid         => AXI_LET_m.awvalid(0),
    --            s_axi_awready         => AXI_LET_s.awready(0),
    --            -- Write data
    --            s_axi_wdata           => AXI_LET_m.wdata,
    --            s_axi_wvalid          => AXI_LET_m.wvalid(0),
    --            s_axi_wready          => AXI_LET_s.wready(0),
    --            -- Write response
    --            s_axi_bresp           => AXI_LET_s.bresp,
    --            s_axi_bvalid          => AXI_LET_s.bvalid(0),
    --            s_axi_bready          => AXI_LET_m.bready(0),
    --            -- Read address
    --            s_axi_araddr          => AXI_LET_m.araddr(15 downto 0),
    --            s_axi_arvalid         => AXI_LET_m.arvalid(0),
    --            s_axi_arready         => AXI_LET_s.arready(0),
    --            -- Read data
    --            s_axi_rdata           => AXI_LET_s.rdata,
    --            s_axi_rresp           => AXI_LET_s.rresp,
    --            s_axi_rvalid          => AXI_LET_s.rvalid(0),
    --            s_axi_rready          => AXI_LET_m.rready(0),
    --            clear_stat            => clear_stat_sw,
    --            total_frame_counter   => total_frame_counter,
    --            crc_error_counter     => crc_error_counter,
    --            crc16_err_counter     => crc16_err_counter,
    --            crc32_err_counter     => crc32_err_counter,
    --            header_corrections    => header_corrections,
    --            payload_corrections   => payload_corrections,
    --            lpc_frame_counter     => lpc_frame_counter,
    --            lpc_corrections       => lpc_corrections,
    --            new_search_counter    => new_search_counter,
    --            sync_loss_counter     => sync_loss_counter,
    --            rx_data_good          => rx_data_good,
    --            lpc_frame_correct_reg => lpc_frame_correct_reg,
    --            payload_cerr_reg      => payload_cerr_reg,
    --            header_cerr_reg       => header_cerr_reg,
    --            bitrate_client_rx     => bitrate_client_rx,
    --            bitrate_client_tx     => bitrate_client_tx,
    --            LAPC_RPT_RSSI_FAST    => LAPC_RPT_RSSI_FAST, --  : out std_logic_vector(14-1 downto 0);
    --            LAPC_RPT_RSSI_MEAN    => LAPC_RPT_RSSI_MEAN, --  : out std_logic_vector(8-1 downto 0);
    --            LAPC_RPT_RSSI_SDEV    => LAPC_RPT_RSSI_SDEV, --  : out std_logic_vector(8-1 downto 0);
    --            OISL_PMIN             => OISL_PMIN, --  : out std_logic_vector(8-1 downto 0);
    --            OISL_PMAX             => OISL_PMAX, --  : out std_logic_vector(8-1 downto 0);
    --            fifo_fcch_data        => fifo_fcch_data, --  : in std_logic_vector(20-1 downto 0);
    --            fifo_fcch_valid       => fifo_fcch_valid, --  : in std_logic;
    --            fifo_fcch_rd_en       => fifo_fcch_rd_en, --  : out std_logic;
    --            pll_locked_vector     => pll_locked_vector, --  : in std_logic_vector(2 downto 0)
    --            reset_fso             => reset_fso_rx, -- : out std_logic;
    --            reset_fec             => reset_fec_rx, --   : out std_logic;
    --            reset_sda             => reset_sda_rx, --   : out std_logic;
    --            reset_let_sda         => reset_let_sda,
    --            cfg_pause_data 	      => cfg_pause_data,
    --            -- Framer
    --		    framer_total_packet_counter            => framer_total_packet_counter           , --: in std_logic_vector(31 downto 0);
    --		    framer_total_packet_splitted_counter   => framer_total_packet_splitted_counter  , --: in std_logic_vector(31 downto 0);
    --		    framer_total_data_frame_counter        => framer_total_data_frame_counter       , --: in std_logic_vector(31 downto 0);
    --		    framer_actual_data_frame_counter       => framer_actual_data_frame_counter      , --: in std_logic_vector(31 downto 0);
    --		    framer_total_idle_frame_counter        => framer_total_idle_frame_counter       , --: in std_logic_vector(31 downto 0);
    --		    framer_total_packet_drop               => framer_total_packet_drop              , --: in std_logic_vector(31 downto 0);
    --		    framer_total_prebuffer_packet_drop     => framer_total_prebuffer_packet_drop    , --: in std_logic_vector(31 downto 0);
    --		    framer_frame_length_error              => framer_frame_length_error             , --: in std_logic_vector(31 downto 0);
    --		    framer_bitrate_in                      => framer_bitrate_in                     , --: in std_logic_vector(31 downto 0);
    --		    framer_packet_cnt_in                   => framer_packet_cnt_in                  , --: in std_logic_vector(31 downto 0);
    --    
    --		    -- Deframer
    --		    deframer_total_packet_counter            => deframer_total_packet_counter         , --: in std_logic_vector(31 downto 0);
    --		    deframer_total_payload_counter           => deframer_total_payload_counter        , --: in std_logic_vector(31 downto 0);
    --		    deframer_total_packet_splitted_counter   => deframer_total_packet_splitted_counter, --: in std_logic_vector(31 downto 0);
    --		    deframer_total_packet_merged_counter     => deframer_total_packet_merged_counter  , --: in std_logic_vector(31 downto 0);
    --		    deframer_total_data_frame_counter        => deframer_total_data_frame_counter     , --: in std_logic_vector(31 downto 0);
    --		    deframer_total_idle_frame_counter        => deframer_total_idle_frame_counter     , --: in std_logic_vector(31 downto 0);
    --		    deframer_total_payload_error_counter     => deframer_total_payload_error_counter  , --: in std_logic_vector(31 downto 0);
    --		    deframer_total_packet_error_counter      => deframer_total_packet_error_counter   , --: in std_logic_vector(31 downto 0);
    --		    deframer_watchdog_reset_counter          => deframer_watchdog_reset_counter       , --: in std_logic_vector(31 downto 0);
    --		    deframer_packet_filter_cnt_in            => deframer_packet_filter_cnt_in         , --: in std_logic_vector(31 downto 0);
    --		    deframer_packet_filter_cnt_out           => deframer_packet_filter_cnt_out        , --: in std_logic_vector(31 downto 0);
    --		    deframer_frame_filter_cnt_in             => deframer_frame_filter_cnt_in          , --: in std_logic_vector(31 downto 0);
    --		    deframer_frame_filter_cnt_out            => deframer_frame_filter_cnt_out         , --: in std_logic_vector(31 downto 0);
    --		    deframer_bitrate_out                     => deframer_bitrate_out                  , --: in std_logic_vector(31 downto 0);
    --		    deframer_packet_cnt_out                  => deframer_packet_cnt_out                --: in std_logic_vector(31 downto 0)
    --		
    --        );

    --    frame_fec_top : entity frame_decoding.frame_fec_top
    --        port map(
    --            clk                     => clk200, -- : in  std_logic;
    --            rst                     => reset_sync_200, -- : in  std_logic;
    --            rst_rx                  => reset_fec_rx,
    --            preamble                => HEADER_PREAMBLE, -- : in  std_logic_vector(71 downto 0);
    --
    --            -- configuration for the preamble sync algorithm
    --            cfg_aquisition_length   => cfg_aquisition_length_vio, -- : in std_logic_vector(32 - 1 downto 0);
    --            cfg_tracking_length     => cfg_tracking_length_vio, -- : in std_logic_vector(32 - 1 downto 0);
    --            cfg_max_tracking_error  => cfg_max_tracking_error_vio, -- : in std_logic_vector(32 - 1 downto 0);
    --
    --            -- TX data input - header and payload data
    --            ----------------------
    --            tx_axis_tdata           => framer_out_axis_tdata, -- : in  std_logic_vector(8 - 1 downto 0);
    --            tx_axis_tvalid          => framer_out_axis_tvalid, -- : in  std_logic;
    --            tx_axis_tlast           => framer_out_axis_tlast, -- : in  std_logic;
    --            tx_axis_tready          => framer_out_axis_tready, -- : out std_logic;
    --
    --            disp_sum                => disp_sum, --: out std_logic_vector(23 downto 0);
    --
    --            -- Output data handling
    --            -----------------------
    --            ch_tx_axis_tdata        => fso_tx_axis_tdata, --: out std_logic_vector(8 - 1 downto 0);
    --            ch_tx_axis_tvalid       => fso_tx_axis_tvalid, --: out std_logic;
    --            ch_tx_axis_tlast        => fso_tx_axis_tlast, -- : out std_logic;
    --            ch_tx_axis_tready       => fso_tx_axis_tready, --: in  std_logic;
    --
    --            -- RX data input
    --            ----------------------
    --            ch_rx_axis_tdata        => fso_rx_axis_tdata, --: in  std_logic_vector(8 - 1 downto 0);
    --            ch_rx_axis_tvalid       => fso_rx_axis_tvalid, --: in  std_logic;
    --
    --            -- Status
    --            -----------------------
    --            crc16_valid             => crc16_valid, -- open, -- : out std_logic;
    --            crc16_correct           => crc16_correct, -- open, -- : out std_logic;
    --            crc32_valid             => crc32_valid, -- open, -- : out std_logic;
    --            crc32_correct           => crc32_correct, -- open, -- : out std_logic;
    --
    --            lpc_frame_correct       => open, -- : out std_logic;
    --            lpc_frame_correction    => lpc_correction,
    --            lpc_frame_correct_valid => lpc_correct_valid, -- : out std_logic;
    --
    --            payload_cerr            => payload_cerr, -- : out std_logic_vector(4 - 1 downto 0);
    --            payload_ncerr           => payload_ncerr, -- : out std_logic;
    --            payload_err_valid       => payload_err_valid, -- : out std_logic;
    --
    --            header_cerr             => header_cerr, -- : out std_logic_vector(4 - 1 downto 0);
    --            header_ncerr            => header_ncerr, -- : out std_logic;
    --            header_err_valid        => header_err_valid, -- : out std_logic;
    --
    --            preamble_synced         => rx_data_good, -- : out std_logic;
    --
    --            -- More diagnostic informations
    --            -------------------------------------------------------
    --
    --            clear_stat              => clear_stat, -- : in  std_logic;
    --            total_frame_counter     => total_frame_counter, -- : out std_logic_vector(31 downto 0);
    --            crc_error_counter       => crc_error_counter, -- : out std_logic_vector(31 downto 0);
    --            crc16_err_counter       => crc16_err_counter, -- : out std_logic_vector(31 downto 0);
    --            crc32_err_counter       => crc32_err_counter, -- : out std_logic_vector(31 downto 0);
    --            header_corrections      => header_corrections, -- : out std_logic_vector(31 downto 0);
    --            payload_corrections     => payload_corrections, -- : out std_logic_vector(31 downto 0);
    --            lpc_frame_counter       => lpc_frame_counter, -- : out std_logic_vector(31 downto 0);
    --            lpc_corrections         => lpc_corrections, -- : out std_logic_vector(31 downto 0);
    --
    --            -- Stats from the preamble sync
    --            new_search_counter      => new_search_counter, -- : out std_logic_vector(31 downto 0);
    --            sync_loss_counter       => sync_loss_counter, -- : out std_logic_vector(31 downto 0);
    --
    --            -- RX data output - header and payload data
    --            -----------------------
    --            rx_axis_tdata           => deframer_in_axis_tdata, -- : out std_logic_vector(8 - 1 downto 0);
    --            rx_axis_tvalid          => deframer_in_axis_tvalid, -- : out std_logic;
    --            rx_axis_tlast           => deframer_in_axis_tlast, -- : out std_logic;
    --            rx_axis_tready          => deframer_in_axis_tready -- : in  std_logic
    --        );
    --
    --
    --inst_let_sda_top : entity let_sda_lib.let_sda_top
    --    Port map( 
    --        clk_let           => clk200, --: in  std_logic; -- Main clock 200 MHz
    --        rst_let           => reset_sync_200, --: in  std_logic;
    --        rst_let_rx        => reset_sda_rx,
    --        clk_tx_eth        => clk200, --: in  std_logic; -- Ethernet TX Clock
    --        rst_tx_eth        => reset_sync_200, --: in  std_logic;
    --        clk_rx_eth        => rx_mac_aclk_eth, --: in  std_logic; -- Ethernet RX Clock
    --        rst_rx_eth        => rx_reset_eth, --: in  std_logic; 
    --        clk_fso_tx        => fso_userclk_tx_out, --: in  std_logic; -- FSO PHY TX Clock 
    --        clk_fso_rx        => fso_userclk_rx_out, --: in  std_logic;
    --        rst_fso_tx        => fso_userrst_tx_out, --: in  std_logic; -- FSO PHY RX Clock
    --        rst_fso_rx        => fso_userrst_rx_out, --: in  std_logic;
    --
    --        -- Input interface ETH
    --        ----------------------
    --        framer_in_axis_tdata     => M_AXIS_ETH_tdata, --: in  std_logic_vector(8 - 1 downto 0);
    --        framer_in_axis_tvalid    => M_AXIS_ETH_tvalid, --: in  std_logic;
    --        framer_in_axis_tlast     => M_AXIS_ETH_tlast, --: in  std_logic;
    --        framer_in_axis_tready    => open, --: out std_logic;
    --        framer_in_axis_tuser     => M_AXIS_ETH_tuser, --: in  std_logic;
    --
    --        cfg_pause_data          => cfg_pause_data, --: in std_logic_vector(15 downto 0);
    --        tx_pause_valid          => tx_pause_valid, --: out std_logic;
    --        tx_pause_data           => tx_pause_data, --: out std_logic_vector(15 downto 0);
    --        rx_fifo_skipped_frame   => rx_fifo_skipped_frame, --: out std_logic_vector(32 - 1 downto 0);
    --
    --        -- Output interface ETH
    --        ----------------------
    --        deframer_out_axis_tdata  => S_AXIS_ETH_tdata_int, --: out std_logic_vector(8 - 1 downto 0);
    --        deframer_out_axis_tvalid => S_AXIS_ETH_tvalid_int, --: out std_logic;
    --        deframer_out_axis_tlast  => S_AXIS_ETH_tlast_int, --: out std_logic;
    --        deframer_out_axis_tready => S_AXIS_ETH_tready, --: in  std_logic;
    --        deframer_out_axis_tuser  => S_AXIS_ETH_tuser_int(0), --: out std_logic;
    --
    --        -- Input interface Creonic
    --        ----------------------
    --        deframer_in_axis_tdata  => deframer_in_axis_tdata, --: in  std_logic_vector(8 - 1 downto 0);
    --        deframer_in_axis_tvalid => deframer_in_axis_tvalid, --: in std_logic;
    --        deframer_in_axis_tlast  => deframer_in_axis_tlast, --: in std_logic;
    --        deframer_in_axis_tready => deframer_in_axis_tready, --: out std_logic;
    --        preamble_synced         => rx_data_good, --: in std_logic;
    --        crc16_valid             => crc16_valid, --: in std_logic;
    --        crc16_correct           => crc16_correct, --: in std_logic;
    --        crc32_valid             => crc32_valid, --: in std_logic;
    --        crc32_correct           => crc32_correct, --: in std_logic;
    --
    --        -- Output interface Creonic
    --        ----------------------
    --        framer_out_axis_tdata  => framer_out_axis_tdata, --: out  std_logic_vector(8 - 1 downto 0);
    --        framer_out_axis_tvalid => framer_out_axis_tvalid, --: out std_logic;
    --        framer_out_axis_tlast  => framer_out_axis_tlast, --: out std_logic;
    --        framer_out_axis_tready => framer_out_axis_tready, --: in std_logic;
    --
    --        -- Input data from FSO PHY
    --        ----------------------
    --        fso_phy_data_tx     => fso_userdata_tx_out, --: in  std_logic_vector(16 - 1 downto 0);
    --        fso_phy_valid_tx    => fso_uservalid_tx_out, --fso_tx_axis_tvalid, --: in  std_logic;
    --        fso_phy_data_rx     => fso_userdata_rx_out, --: in  std_logic_vector(16 - 1 downto 0);
    --        fso_phy_valid_rx    => fso_uservalid_rx_out, --fso_rx_axis_tvalid, --: in  std_logic;
    --
    --        -- Ranging data: Time of flight in 1/16 fso_clk
    --        ----------------------
    --        trigger             => '0', --: in  std_logic;
    --        tof_counter_valid   => open, --: out std_logic;
    --        tof_counter         => open, --: out std_logic_vector(40-1 downto 0);
    --
    --        -- Framer Health Monitor
    --        ----------------------
    --        framer_warning_flag                => framer_warning_flag, --: out  std_logic;
    --        framer_critical_error_flag         => framer_critical_error_flag, --: out  std_logic;
    --        framer_health_monitor_statistics   => framer_health_monitor_statistics, --: out health_monitor_statistics_t;
    --
    --        -- Deframer Health Monitor
    --        ----------------------
    --        deframer_warning_flag                => deframer_warning_flag, --: out  std_logic;
    --        deframer_critical_error_flag         => deframer_critical_error_flag, --: out  std_logic;
    --        deframer_health_monitor_statistics   => deframer_health_monitor_statistics, --: out health_monitor_statistics_t;
    --
    --        -- FCCH
    --        ----------------------
    --        pps_clk                     => '0', --: in std_logic;
    --        
    --        -- From Register Map
    --        LAPC_RPT_RSSI_FAST          => LAPC_RPT_RSSI_FAST, --: in std_logic_vector(14-1 downto 0);
    --        LAPC_RPT_RSSI_MEAN          => LAPC_RPT_RSSI_MEAN, --: in std_logic_vector(8-1 downto 0);
    --        LAPC_RPT_RSSI_SDEV          => LAPC_RPT_RSSI_SDEV, --: in std_logic_vector(8-1 downto 0);
    --        OISL_PMIN                   => OISL_PMIN, --: in std_logic_vector(8-1 downto 0);
    --        OISL_PMAX                   => OISL_PMAX, --: in std_logic_vector(8-1 downto 0);
    --        crc_errors_per_second       => crc_errors_per_second,  
    --                 
    --        -- To Register Map
    --        fifo_fcch_data                => fifo_fcch_data, --: out std_logic_vector(20-1 downto 0);
    --        fifo_fcch_valid               => fifo_fcch_valid, --: out std_logic;
    --        fifo_fcch_rd_en               => fifo_fcch_rd_en, --: in std_logic;
    --        --tx_axis_mac_tready            => tx_axis_mac_tready,
    --        
    --        -- Statistics
    --        ----------------------
    --        clear_stat                      => clear_stat, --: in  std_logic;
    --        -- Framer
    --		framer_total_packet_counter            => framer_total_packet_counter         , --: out std_logic_vector(31 downto 0);
    --		framer_total_packet_splitted_counter   => framer_total_packet_splitted_counter, --: out std_logic_vector(31 downto 0);
    --		framer_total_data_frame_counter        => framer_total_data_frame_counter     , --: out std_logic_vector(31 downto 0);
    --		framer_actual_data_frame_counter       => framer_actual_data_frame_counter    , --: out std_logic_vector(31 downto 0);
    --		framer_total_idle_frame_counter        => framer_total_idle_frame_counter     , --: out std_logic_vector(31 downto 0);
    --		framer_total_packet_drop               => framer_total_packet_drop            , --: out std_logic_vector(31 downto 0);
    --		framer_total_prebuffer_packet_drop     => framer_total_prebuffer_packet_drop  , --: out std_logic_vector(31 downto 0);
    --		framer_frame_length_error              => framer_frame_length_error           , --: out std_logic_vector(31 downto 0);
    --		framer_bitrate_in                      => framer_bitrate_in                   , --: out std_logic_vector(31 downto 0);
    --		framer_packet_cnt_in                   => framer_packet_cnt_in                , --: out std_logic_vector(31 downto 0);
    --
    --        -- Deframer
    --        deframer_total_packet_counter            => deframer_total_packet_counter         , --: out std_logic_vector(31 downto 0);
    --        deframer_total_payload_counter           => deframer_total_payload_counter        , --: out std_logic_vector(31 downto 0);
    --        deframer_total_packet_splitted_counter   => deframer_total_packet_splitted_counter, --: out std_logic_vector(31 downto 0);
    --        deframer_total_packet_merged_counter     => deframer_total_packet_merged_counter  , --: out std_logic_vector(31 downto 0);
    --        deframer_total_data_frame_counter        => deframer_total_data_frame_counter     , --: out std_logic_vector(31 downto 0);
    --        deframer_total_idle_frame_counter        => deframer_total_idle_frame_counter     , --: out std_logic_vector(31 downto 0);
    --        deframer_total_payload_error_counter     => deframer_total_payload_error_counter  , --: out std_logic_vector(31 downto 0);
    --        deframer_total_packet_error_counter      => deframer_total_packet_error_counter   , --: out std_logic_vector(31 downto 0);
    --        deframer_watchdog_reset_counter          => deframer_watchdog_reset_counter       , --: out std_logic_vector(31 downto 0);
    --        deframer_packet_filter_cnt_in            => deframer_packet_filter_cnt_in         , --: out std_logic_vector(31 downto 0);
    --		deframer_packet_filter_cnt_out           => deframer_packet_filter_cnt_out        , --: out std_logic_vector(31 downto 0);
    --        deframer_frame_filter_cnt_in             => deframer_frame_filter_cnt_in          , --: out std_logic_vector(31 downto 0);
    --        deframer_frame_filter_cnt_out            => deframer_frame_filter_cnt_out         , --: out std_logic_vector(31 downto 0);
    --        deframer_bitrate_out                     => deframer_bitrate_out                  , --: out std_logic_vector(31 downto 0);
    --        deframer_packet_cnt_out                  => deframer_packet_cnt_out               , --: out std_logic_vector(31 downto 0);
    --
    --        sda_debug                                => sda_debug, --: out sda_debug_t;
    --
    --        -- Version control
    --        ----------------------
    --        version_mayor                   => open, --: out unsigned(7 downto 0);
    --        version_minor                   => open --: out unsigned(7 downto 0)
    --    );

--    BUFGCE_DIV_25_inst : BUFGCE_DIV
--        generic map(
--            BUFGCE_DIVIDE   => 4,       -- 1-8
--            IS_CE_INVERTED  => '0',     -- Optional inversion for CE
--            IS_CLR_INVERTED => '0',     -- Optional inversion for CLR
--            IS_I_INVERTED   => '0'      -- Optional inversion for I
--        )
--        port map(
--            O   => ref_clk_25,          -- 1-bit output: Buffer
--            CE  => VCC,                 -- 1-bit input: Buffer enable
--            CLR => GND,                 -- 1-bit input: Asynchronous clear
--            I   => clk100               -- 1-bit input: Buffer
--        );

    --    eth_1g_top : entity let_sda_lib.eth_1g_top
    --        port map(
    --            reset               => sync100_rstn,
    --            gtx_clk             => let_refclk_nom, -- gtx_clk, 
    --            ref_clk             => ref_clk,
    --            --let_refclk_nom           =>                  ,

    --            --eth_gt0_txp              => eth_gt0_txp                    ,
    --            --eth_gt0_txn              => eth_gt0_txn                    ,

    --            --eth_gt0_rxp              => eth_gt0_rxp                    ,
    --            --eth_gt0_rxn              => eth_gt0_rxn                    ,

    --            s_axi_aclk          => clk100,
    --            s_axi_aresetn       => s_axi_aresetn,
    --            s_axi_awaddr        => AXI_ETH_REG_IF_m.awaddr,
    --            s_axi_awvalid       => AXI_ETH_REG_IF_m.awvalid(0),
    --            s_axi_awready       => AXI_ETH_REG_IF_s.awready(0),
    --            s_axi_wdata         => AXI_ETH_REG_IF_m.wdata,
    --            s_axi_wstrb         => AXI_ETH_REG_IF_m.wstrb,
    --            s_axi_wvalid        => AXI_ETH_REG_IF_m.wvalid(0),
    --            s_axi_wready        => AXI_ETH_REG_IF_s.wready(0),
    --            s_axi_bresp         => AXI_ETH_REG_IF_s.bresp,
    --            s_axi_bvalid        => AXI_ETH_REG_IF_s.bvalid(0),
    --            s_axi_bready        => AXI_ETH_REG_IF_m.bready(0),
    --            s_axi_araddr        => AXI_ETH_REG_IF_m.araddr,
    --            s_axi_arvalid       => AXI_ETH_REG_IF_m.arvalid(0),
    --            s_axi_arready       => AXI_ETH_REG_IF_s.arready(0),
    --            s_axi_rdata         => AXI_ETH_REG_IF_s.rdata,
    --            s_axi_rresp         => AXI_ETH_REG_IF_s.rresp,
    --            s_axi_rvalid        => AXI_ETH_REG_IF_s.rvalid(0),
    --            s_axi_rready        => AXI_ETH_REG_IF_m.rready(0),
    --            rx_reset_in         => eth_rx_reset_in,
    --            rx_user_rst_out     => mac_rx_reset,
    --            rx_clk_out          => mac_rx_clk_out,
    --            rx_axis_tvalid      => mac_mux_valid,
    --            rx_axis_tdata       => mac_mux_data,
    --            rx_axis_tlast       => mac_mux_last,
    --            rx_axis_tuser       => mac_mux_error,
    --            tx_reset_in         => eth_tx_reset_in,
    --            tx_user_rst_out     => mac_tx_reset,
    --            tx_clk_out          => mac_tx_clk_out,
    --            tx_axis_tready      => axis_mac_tx_tready,
    --            tx_axis_tvalid      => axis_mac_tx_tvalid,
    --            tx_axis_tdata       => axis_mac_tx_tdata,
    --            tx_axis_tlast       => axis_mac_tx_tlast,
    --            tx_axis_tuser       => axis_mac_tx_tuser,
    --            s_axis_pause_tdata  => s_axis_pause_tdata,
    --            s_axis_pause_tvalid => '0', --global_tx_pause_req, -- just for test
    --            rgmii_txd           => rgmii_txd,
    --            rgmii_tx_ctl        => rgmii_tx_ctl,
    --            rgmii_txc           => rgmii_txc,
    --            rgmii_rxd           => rgmii_rxd,
    --            rgmii_rx_ctl        => rgmii_rx_ctl,
    --            rgmii_rxc           => rgmii_rxc,
    --            mac_irq             => mac_irq,
    --            mdio                => mdio,
    --            mdc                 => mdc
    --        );

    -- CDC fifo to convert tx ethernet data (coming from deframer) from 200 MHz to 125 clock rate.
    --    eth_mac_tx_fifo : dc_fifo_eth_side
    --        port map(
    --            m_aclk          => tx_mac_aclk_eth, -- : IN STD_LOGIC;
    --            s_aclk          => clk200,  -- : IN STD_LOGIC;
    --            s_aresetn       => tx_reset_eth, -- : IN STD_LOGIC;
    --            s_axis_tvalid   => deframer_out_axis_tvalid, -- : IN STD_LOGIC;
    --            s_axis_tready   => deframer_out_axis_tready, -- : OUT STD_LOGIC;
    --            s_axis_tdata    => deframer_out_axis_tdata, -- : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    --            s_axis_tlast    => deframer_out_axis_tlast, -- : IN STD_LOGIC;
    --            s_axis_tuser(0) => deframer_out_axis_tuser, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --            m_axis_tvalid   => S_AXIS_ETH_tvalid, -- : OUT STD_LOGIC;
    --            m_axis_tready   => S_AXIS_ETH_tready, -- : IN STD_LOGIC;
    --            m_axis_tdata    => S_AXIS_ETH_tdata, -- : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    --            m_axis_tlast    => S_AXIS_ETH_tlast, -- : OUT STD_LOGIC;
    --            m_axis_tuser => S_AXIS_ETH_tuser -- : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
    --        );

    -- CDC fifo to convert tx ethernet data (coming from deframer) from 200 MHz to 125 clock rate.
    --    eth_mac_tx_fifo : dc_fifo_eth_side
    --        port map(
    --            m_aclk          => mac_tx_clk_out, -- : IN STD_LOGIC;
    --            s_aclk          => mac_rx_clk_out,  -- : IN STD_LOGIC;
    --            s_aresetn       => mac_rx_resetn, -- : IN STD_LOGIC;
    --            s_axis_tvalid   => mac_mux_valid, -- : IN STD_LOGIC;
    --            s_axis_tready   => open, -- : OUT STD_LOGIC;
    --            s_axis_tdata    => mac_mux_data, -- : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    --            s_axis_tlast    => mac_mux_last, -- : IN STD_LOGIC;
    --            s_axis_tuser(0) => mac_mux_error, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --            m_axis_tvalid   => axis_mac_tx_tvalid, -- : OUT STD_LOGIC;
    --            m_axis_tready   => axis_mac_tx_tready, -- : IN STD_LOGIC;
    --            m_axis_tdata    => axis_mac_tx_tdata, -- : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    --            m_axis_tlast    => axis_mac_tx_tlast, -- : OUT STD_LOGIC;
    --            m_axis_tuser(0) => axis_mac_tx_tuser -- : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
    --        );

    --    inst_corrections_rate_calculator : entity let_sda_lib.corrections_rate_calculator
    --        port map(
    --            clk                            => clk200,
    --            rst                            => reset_sync_200,
    --            crc16_valid                    => crc16_valid,
    --            crc16_correct                  => crc16_correct,
    --            crc32_valid                    => crc32_valid,
    --            crc32_correct                  => crc32_correct,
    --            payload_cerr                   => payload_cerr,
    --            payload_ncerr                  => payload_ncerr,
    --            payload_err_valid              => payload_err_valid,
    --            header_cerr                    => header_cerr,
    --            header_ncerr                   => header_ncerr,
    --            header_err_valid               => header_err_valid,
    --            lpc_correct_valid              => lpc_correct_valid,
    --            lpc_correction                 => lpc_correction,
    --            header_corrections_per_second  => header_corrections_per_second, --: out std_logic_vector(31 downto 0);
    --            payload_corrections_per_second => payload_corrections_per_second, --: out std_logic_vector(31 downto 0);
    --            lpc_corrections_per_second     => lpc_corrections_per_second, --: out std_logic_vector(31 downto 0);
    --            corrections_rate               => corrections_rate,
    --            crc_errors_per_second          => crc_errors_per_second
    --        );

    --resetn_200_out <= resetn_sync_200;
    --    tx_axi_rstn_eth <= resetn_sync_200;
    -- reset syncers
    sync_clk_let : entity work.proc_sys_reset_0
        port map(
            slowest_sync_clk      => clk_let,
            ext_reset_in          => ext_rst_async,
            aux_reset_in          => reset_aux_muxed, -- reset_let_sda or reset_vio;
            mb_debug_sys_rst      => GND,
            dcm_locked            => dcm_locked_1,
            mb_reset              => open,
            bus_struct_reset      => open,
            peripheral_reset(0)   => reset_sync_let,
            interconnect_aresetn  => open,
            peripheral_aresetn(0) => resetn_sync_let
        );
        
    sync_clk_ldpc : entity work.proc_sys_reset_0
        port map(
            slowest_sync_clk      => clk_ldpc,
            ext_reset_in          => ext_rst_async,
            aux_reset_in          => reset_aux_muxed, -- reset_let_sda or reset_vio;
            mb_debug_sys_rst      => GND,
            dcm_locked            => dcm_locked_1,
            mb_reset              => open,
            bus_struct_reset      => open,
            peripheral_reset(0)   => reset_sync_ldpc,
            interconnect_aresetn  => open,
            peripheral_aresetn(0) => resetn_sync_ldpc
        );

    sync_gt_free : entity work.proc_sys_reset_0
        port map(
            slowest_sync_clk      => clk_gt_free,
            ext_reset_in          => ext_rst_async,
            aux_reset_in          => reset_aux_muxed,
            mb_debug_sys_rst      => GND,
            dcm_locked            => dcm_locked_1,
            mb_reset              => open,
            bus_struct_reset      => open,
            peripheral_reset(0)   => reset_sync_gt_free,
            interconnect_aresetn  => open,
            peripheral_aresetn(0) => resetn_sync_gt_free
        );
        
    sync_eth_free : entity work.proc_sys_reset_0
        port map(
            slowest_sync_clk      => clk_eth_free,
            ext_reset_in          => ext_rst_async,
            aux_reset_in          => reset_aux_muxed,
            mb_debug_sys_rst      => GND,
            dcm_locked            => dcm_locked_1,
            mb_reset              => open,
            bus_struct_reset      => open,
            peripheral_reset(0)   => reset_sync_eth_free,
            interconnect_aresetn  => open,
            peripheral_aresetn(0) => resetn_sync_eth_free
        );

    sync_clk_sys : entity work.proc_sys_reset_0
        port map(
            slowest_sync_clk      => clk_sys,
            ext_reset_in          => ext_rst_async,
            aux_reset_in          => reset_aux_muxed, -- reset_let_sda or reset_vio;
            mb_debug_sys_rst      => GND,
            dcm_locked            => dcm_locked_1,
            mb_reset              => open,
            bus_struct_reset      => open,
            peripheral_reset       => open,
            interconnect_aresetn  => open,
            peripheral_aresetn(0) => s_axi_aresetn
        );


    --fso_loopback_in   <= '0';

--    S_AXIS_ETH_tdata      <= S_AXIS_ETH_tdata_int;
--    S_AXIS_ETH_tvalid     <= S_AXIS_ETH_tvalid_int;
--    S_AXIS_ETH_tlast      <= S_AXIS_ETH_tlast_int;
--    S_AXIS_ETH_tready_int <= S_AXIS_ETH_tready;
--    S_AXIS_ETH_tuser      <= S_AXIS_ETH_tuser_int;

    g_KEEP_DEBUG_ch_sel : if g_DEBUG = 0 generate
        --    channel_sel_tx <= channel_sel;
        --    channel_sel_rx <= channel_sel;
        reset_vio              <= '0';
        channel_sel_vio_enable <= '0';
        clear_stat             <= clear_stat_sw;

    end generate g_KEEP_DEBUG_ch_sel;

    g_KEEP_DEBUG : if g_DEBUG = 1 generate

        process(clk_sys)
        begin
            --if reset = '1' then
            --    
            --else
                if rising_edge(clk_sys) then
                    r_vio_preamble_synced          <= preamble_synced       ; --   : std_logic;                           
                    r_vio_total_frame_counter      <= total_frame_counter   ; --   : std_logic_vector(31 downto 0);                   
                    r_vio_crc_error_counter        <= crc_error_counter     ; --   : std_logic_vector(31 downto 0);               
                    r_vio_crc16_err_counter        <= crc16_err_counter     ; --   : std_logic_vector(31 downto 0);               
                    r_vio_crc32_err_counter        <= crc32_err_counter     ; --   : std_logic_vector(31 downto 0);               
                    r_vio_new_search_counter       <= new_search_counter    ; --   : std_logic_vector(31 downto 0);               
                    r_vio_sync_loss_counter        <= sync_loss_counter     ; --   : std_logic_vector(31 downto 0);    
                  
                    r_vio_framer_total_packet_counter          <= framer_total_packet_counter         ;
                    r_vio_framer_total_packet_splitted_counter <= framer_total_packet_splitted_counter;
                    r_vio_framer_total_data_frame_counter      <= framer_total_data_frame_counter     ;
                    r_vio_framer_actual_data_frame_counter     <= framer_actual_data_frame_counter    ;
                    r_vio_framer_total_idle_frame_counter      <= framer_total_idle_frame_counter     ;
                    r_vio_framer_total_packet_drop             <= framer_total_packet_drop            ;
                    r_vio_framer_total_prebuffer_packet_drop   <= framer_total_prebuffer_packet_drop  ;
                    r_vio_framer_frame_length_error            <= framer_frame_length_error           ;
                    r_vio_framer_bitrate_in                    <= framer_bitrate_in                   ;
                    r_vio_framer_packet_cnt_in                 <= framer_packet_cnt_in                ;

                    r_vio_deframer_total_packet_counter         <= deframer_total_packet_counter         ;      
                    r_vio_deframer_total_payload_counter        <= deframer_total_payload_counter        ;
                    r_vio_deframer_total_packet_splitted_counter<= deframer_total_packet_splitted_counter;
                    r_vio_deframer_total_packet_merged_counter  <= deframer_total_packet_merged_counter  ;
                    r_vio_deframer_total_data_frame_counter     <= deframer_total_data_frame_counter     ;
                    r_vio_deframer_total_idle_frame_counter     <= deframer_total_idle_frame_counter     ;
                    r_vio_deframer_total_payload_error_counter  <= deframer_total_payload_error_counter  ;
                    r_vio_deframer_total_packet_error_counter   <= deframer_total_packet_error_counter   ;
                    r_vio_deframer_watchdog_reset_counter       <= deframer_watchdog_reset_counter       ;
                    r_vio_deframer_packet_filter_cnt_in         <= deframer_packet_filter_cnt_in         ;
                    r_vio_deframer_packet_filter_cnt_out        <= deframer_packet_filter_cnt_out        ;
                    r_vio_deframer_frame_filter_cnt_in          <= deframer_frame_filter_cnt_in          ;
                    r_vio_deframer_frame_filter_cnt_out         <= deframer_frame_filter_cnt_out         ;
                    r_vio_deframer_bitrate_out                  <= deframer_bitrate_out                  ;
                    r_vio_deframer_packet_cnt_out               <= deframer_packet_cnt_out               ;
                end if;
            --end if;
        end process;

        process(clk_let)
        begin
            --if reset = '1' then
            --    
            --else
                if rising_edge(clk_let) then
                    fso_loopback_in <= r_vio_fso_loopback_in       ; --   : std_logic;               
                    fec_ctrl        <= r_vio_fec_ctrl              ; --
                    reset_vio      <= r_vio_reset_vio;                    
                    cfg_acquisition_length   <= r_vio_cfg_acquisition_length; --   : std_logic_vector(31 downto 0);                   
                    cfg_tracking_length      <= r_vio_cfg_tracking_length   ; --   : std_logic_vector(31 downto 0);                   
                    cfg_max_tracking_error   <= r_vio_cfg_max_tracking_error; --   : std_logic_vector(31 downto 0);
                    cfg_preamble_max_corr    <= r_vio_preamble_max_corr;      --   : std_logic_vector(6 downto 0); -- CAGS Xcorr thresh
                end if;
        end process;

        inst_vio_fec : vio_fec
            PORT MAP(
                clk          => clk_sys,
                probe_in0(0) => r_vio_preamble_synced,
                probe_in1    => r_vio_deframer_total_packet_counter,
                probe_in2    => r_vio_total_frame_counter,
                probe_in3    => r_vio_crc_error_counter,
                probe_in4    => r_vio_crc16_err_counter,
                probe_in5    => r_vio_crc32_err_counter,
                probe_in6    => r_vio_new_search_counter,
                probe_in7    => r_vio_sync_loss_counter,
                probe_in8    => r_vio_deframer_total_payload_error_counter,
                probe_in9    => r_vio_deframer_total_packet_error_counter,
                probe_in10   => r_vio_framer_total_packet_counter,
                probe_out0   => r_vio_cfg_acquisition_length,
                probe_out1   => r_vio_cfg_tracking_length,
                probe_out2   => r_vio_cfg_max_tracking_error,
                probe_out3(0)   => r_vio_fso_loopback_in,
                probe_out4(0)   => r_vio_reset_vio, --clear_stat_vio, --loopback_vio,
                probe_out5   => r_vio_fec_ctrl,
                probe_out6     =>  r_vio_preamble_max_corr   -- CAGS Xcorr thresh
                --            probe_out0 => cfg_acquisition_length,
                --            probe_out1 => cfg_tracking_length,
                --            probe_out2 => cfg_max_tracking_error
            );

        --vio for control/observation
        --vio should be clocked by a free-running clock 
--        vio_let_inst : entity work.vio_let
--            port map(
--                clk           => clk_let, -- : IN STD_LOGIC;
--                probe_in0(0)  => crc16_correct_reg_vio, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in1(0)  => crc32_correct_reg_vio, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in2(0)  => r_vio_preamble_synced, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in3(0)  => lpc_frame_correct_vio, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in4(0)  => payload_ncerr_vio, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in5(0)  => header_ncerr_vio, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in6     => (others => '0'), --header_corrections,  --!!!
--                probe_in7     => payload_corrections,
--                probe_in8     => lpc_corrections,
--                probe_in9     => sync_loss_counter,
--                probe_in10    => (others => '0'),
--                probe_in11    => framer_total_prebuffer_packet_drop, --framer_total_packet_counter,  
--                probe_in12    => framer_total_packet_drop, --framer_total_packet_splitted_counter,  
--                probe_in13    => (others => '0'), --deframer_total_packet_merged_counter,  
--                probe_in14    => (others => '0'), --deframer_total_idle_frame_counter,  
--                probe_in15    => (others => '0'), --deframer_total_payload_error_counter,  
--                probe_in16    => (others => '0'), --deframer_total_packet_error_counter,  
--                probe_in17    => framer_bitrate_in, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in18    => deframer_bitrate_out, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in19    => corrections_per_second, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in20    => corrections_rate, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in21    => crc32_err_counter, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in22    => (others => '0'), --crc16_err_counter, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in23    => (others => '0'), --crc_error_counter, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in24    => lpc_frame_counter, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in25    => deframer_watchdog_reset_counter, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in26    => crc_errors_per_second, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in27    => (others => '0'), -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in28    => (others => '0'), -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in29    => (others => '0'), -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in30    => (others => '0'), -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in31    => (others => '0'),
--                probe_in32    => (others => '0'),
--                probe_in33(0) => reset_sync_200, -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_in34(0) => '0', -- : IN STD_LOGIC_VECTOR(0 DOWNTO 0);  --!!!
--                probe_in35(0) => '0',
--                probe_in36(0) => '0',
--                probe_in37(0) => channel_sel_rx, --mac_tx_reset,
--                probe_in38(0) => channel_sel_tx, --mac_rx_reset, --            
--                probe_in39    => pll_locked_vector,
--                probe_out0(0) => channel_sel_rx_vio, -- reset_vio, -- : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_out1(0) => channel_sel_tx_vio, -- loopback_vio, -- : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_out2(0) => reset_vio, -- rx_polinv_vio, -- : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_out3(0) => channel_sel_vio_enable, -- : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--                probe_out4    => cfg_aquisition_length_vio,
--                probe_out5    => cfg_tracking_length_vio,
--                probe_out6    => cfg_max_tracking_error_vio,
--                probe_out7    => open,
--                probe_out8    => open,
--                probe_out9    => open
--            );

        vio_sda_inst : entity work.vio_sda
            PORT MAP(
                clk           => clk_let,
                probe_in0     => framer_total_packet_counter,
                probe_in1     => framer_total_packet_splitted_counter,
                probe_in2     => framer_total_data_frame_counter,
                probe_in3     => framer_total_idle_frame_counter,
                probe_in4     => deframer_total_payload_counter,
                probe_in5     => deframer_total_packet_counter,
                probe_in6     => deframer_total_packet_splitted_counter,
                probe_in7     => deframer_total_packet_merged_counter,
                probe_in8     => deframer_total_data_frame_counter,
                probe_in9     => deframer_total_idle_frame_counter,
                probe_in10    => deframer_total_payload_error_counter,
                probe_in11    => deframer_total_packet_error_counter,
                probe_in12    => (others=> '0'), --sda_debug.payload_header,
                probe_in13    => (others=> '0'), --sda_debug.packet_header,
                probe_in14    => (others=> '0'), --sda_debug.frame_header.txfn,
                probe_in15    => (others => '0'), --sda_debug.frame_header.rxfn, --!!!
                probe_in16(0) => '0', --sda_debug.frame_header.ack,
                probe_in17(0) => '0', --sda_debug.frame_header.ack_valid,
                probe_in18    => (others=> '0'), --sda_debug.frame_header.frame_type,
                probe_in19    => (others=> '0'), --sda_debug.frame_header.tx_ts,
                probe_in20    => (others=> '0'), --sda_debug.frame_header.tod_seconds,
                probe_in21    => (others=> '0'), --sda_debug.frame_header.ts_applies,
                probe_in22    => (others=> '0'), --sda_debug.frame_header.fcch_opcode,
                probe_in23    => (others => '0'), --sda_debug.frame_header.fcch_pl,
                probe_in24    => (others => '0'),
                probe_in25    => (others=> '0'), --framer_frame_length_error,
                probe_in26    => (others=> '0'), --framer_health_monitor_statistics.not_ready_clks,
                probe_in27    => (others=> '0'), --framer_health_monitor_statistics.wrong_idle_condition,
                probe_in28(0) => '0', --framer_warning_flag,
                probe_in29(0) => '0', --deframer_warning_flag,
                probe_in30    => (others=> '0'), --framer_actual_data_frame_counter,
                probe_in31    => (others=> '0'), --framer_frame_length_error,
                probe_in32(0) => '0', --framer_critical_error_flag,
                probe_in33(0) => '0', --deframer_critical_error_flag,
                probe_in34    => (others=> '0'), --deframer_health_monitor_statistics.frame_length_error,
                probe_in35    => (others=> '0'), --deframer_health_monitor_statistics.not_ready_clks,
                probe_in36    => (others=> '0'), --deframer_health_monitor_statistics.wrong_idle_condition,
                probe_out0(0) => clear_stat_vio,
                probe_out1    => open, --reset_vio,
                probe_out2    => open
            );
        --reserved <= sda_debug.frame_header.reserved4 & sda_debug.frame_header.reserved2 & sda_debug.frame_header.reserved1;
        clear_stat <= clear_stat_vio;
        -- debug 
        pause_frame_debug : entity work.ila_axis_8bit
            PORT MAP(
                clk       => mac_tx_clk_out,
                probe0    => tx_pause_data(7 downto 0),
                probe1(0) => tx_pause_valid,
                probe2(0) => '0',
                probe3(0) => '0',
                probe4(0) => '0'
            );

        rx_mac_if_debug : entity work.ila_axis_8bit
            PORT MAP(
                clk       => clk_let,
                probe0    => client_in_axis_if_m.tdata,
                probe1(0) => client_in_axis_if_m.tvalid,
                probe2(0) => '0',
                probe3(0) => client_in_axis_if_m.tlast,
                probe4(0)    => client_in_axis_if_m.tuser
            );
            
        tx_mac_if_debug : entity work.ila_axis_8bit
            PORT MAP(
                clk       => clk_let,
                probe0    => client_out_axis_if_m.tdata,
                probe1(0) => client_out_axis_if_m.tvalid,
                probe2(0) => client_out_axis_if_s.tready,
                probe3(0) => client_out_axis_if_m.tlast,
                probe4(0) => client_out_axis_if_m.tuser
            );

        --    rx_mac_if_debug : ila_axis_8bit
        --        PORT MAP (
        --        	clk       => mac_rx_clk_out,
        --        	probe0    => mac_mux_data, 
        --        	probe1(0) => mac_mux_valid, 
        --        	probe2(0) => mac_mux_last, 
        --        	probe3(0) => mac_mux_error,
        --        	probe4(0) => GND
        --        );

        -- debug FEC stream
        -- deframer input
        --deframer_in_debug : entity work.ila_axis_8bit
        --    PORT MAP(
        --        clk       => clk_let,
        --        probe0    => fec_rx_axis_if.tdata,
        --        probe1(0) => fec_rx_axis_if.tvalid,
        --        probe2(0) => fec_rx_axis_if.tready,
        --        probe3(0) => fec_rx_axis_if.tlast,
        --        probe4(0) => '0'
        --    );
        -- FEC output towards GT
        sda_tx_out_debug : entity work.ila_axis_8bit
            PORT MAP (
                clk       => clk_let,
                probe0    => fec_tx_axis_if_m.tdata, 
                probe1(0) => fec_tx_axis_if_m.tvalid, 
                probe2(0) => fec_tx_axis_if_m.tlast, 
                probe3(0) => fec_tx_axis_if_s.tready,
                probe4(0) => '0'
            );

        -- FEC output towards GT
        fec_rx_in_debug : entity work.ila_axis_8bit
            PORT MAP(
                clk       => clk_let,
                probe0    => fso_rx_axis_tdata,
                probe1(0) => fso_rx_axis_tvalid,
                probe2(0) => '0',
                probe3(0) => '0',
                probe4(0) => '0'
            );

        --framer_out_debug : entity work.ila_axis_8bit
        --    PORT MAP(
        --        clk       => clk_let,
        --        probe0    => fec_tx_axis_if.tdata(7 downto 0),
        --        probe1(0) => fec_tx_axis_if.tvalid,
        --        probe2(0) => fec_tx_axis_if.tlast,
        --        probe3(0) => fec_tx_axis_if.tready,
        --        probe4(0) => '0'
        --    );
    end generate g_KEEP_DEBUG;

end architecture rtl;
