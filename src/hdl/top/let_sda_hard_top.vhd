library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library let_sda_lib;
use work.let_pckg.all;
--use let_sda_lib.let_pckg.all;
--use let_sda_lib.let_pckg_components.all;
library xil_defaultlib;
library frame_encoding;
library frame_decoding;
library ethernet_framer_lib;
library ethernet_deframer_lib;
library sda_lib;
use sda_lib.pkg_sda.all;

-- Xilinx components
library unisim;
use unisim.vcomponents.all;

entity let_sda_soft_top is
    port(
--        MGTREFCLK0P_128        : in  std_logic; -- Bank 128 - MGTREFCLK0P_128   156.25MHz
--        MGTREFCLK0N_128        : in  std_logic; -- Bank 128 - MGTREFCLK0P_128
        --        CLK_100_N         : in  std_logic; -- Bank  84 VCCO - VCC1V8
        --        CLK_100_P         : in  std_logic; -- Bank  84 VCCO - VCC1V8 CLK_125_P
--        CLK_125_N              : in  std_logic; -- Bank  84 VCCO - VCC1V8
--        CLK_125_P              : in  std_logic; -- Bank  84 VCCO - VCC1V8  
        USER_SI570_C0_P        : in  std_logic; -- Bank  84 VCCO - VCC1V8
        USER_SI570_C0_N        : in  std_logic; -- Bank  84 VCCO - VCC1V8
        USER_SI570_C1_P        : in  std_logic; -- Bank  84 VCCO - VCC1V8
        USER_SI570_C1_N        : in  std_logic; -- Bank  84 VCCO - VCC1V8
        Q7_OUT_C_P             : in  std_logic;
        Q7_OUT_C_N             : in  std_logic;        
        Q11_OUT_C_P             : in  std_logic;
        Q11_OUT_C_N             : in  std_logic;
--        USER_SMA_MGT_CLOCK_C_N : out std_logic; -- Bank  84 VCCO - VCC1V8
--        USER_SMA_MGT_CLOCK_C_P : out std_logic; -- Bank  84 VCCO - name 
        RESET                  : in  std_logic;
        SFP0_RX_N              : in  std_logic; -- Bank 128 - MGTYRXP0_128    
        SFP0_RX_P              : in  std_logic; -- Bank 128 - MGTYRXP0_128
        SFP0_TX_N              : out std_logic; -- Bank 128 - MGTYRXP0_128    
        SFP0_TX_P              : out std_logic; -- Bank 128 - MGTYRXP0_128

        --        SFP1_RX_N         : in  std_logic; -- Bank 128 - MGTYRXP0_128    
        --        SFP1_RX_P         : in  std_logic; -- Bank 128 - MGTYRXP0_128
        --        SFP1_TX_N         : out std_logic; -- Bank 128 - MGTYRXP0_128    
        --        SFP1_TX_P         : out std_logic; -- Bank 128 - MGTYRXP0_128

        ETH0_RX_N              : in  std_logic; --    
        ETH0_RX_P              : in  std_logic; -- 
        ETH0_TX_N              : out std_logic; --     
        ETH0_TX_P              : out std_logic; -- 

        --        ETH1_RX_N         : in  std_logic; --    
        --        ETH1_RX_P         : in  std_logic; -- 
        --        ETH1_TX_N         : out std_logic; --    
        --        ETH1_TX_P         : out std_logic; --

        SFP0_TX_DISABLE_B      : out std_logic;
        SFP1_TX_DISABLE_B      : out std_logic;
        SFP2_TX_DISABLE_B      : out std_logic;
        SFP3_TX_DISABLE_B      : out std_logic
    );
end entity let_sda_soft_top;

architecture RTL of let_sda_soft_top is

    signal clk_200                                : std_logic;
    signal clk_100                                : std_logic;
    signal clk_100_i                              : std_logic;
    signal mgtrefclk                              : std_logic;
    signal mgtrefclk0_x0y1                        : std_logic;
    signal ldpc_clk                               : std_logic;
    signal clk_in1                                : std_logic;
    signal locked                                 : std_logic;
    signal rst_async                              : std_logic;
    signal rst_async_n                            : std_logic;
    signal s_axi_resetn                           : std_logic;
    ------------------------------------
    signal gtwiz_userclk_tx_reset_in              : STD_LOGIC;
    signal gtwiz_userclk_tx_srcclk_out            : STD_LOGIC;
    signal gtwiz_userclk_tx_usrclk_out            : STD_LOGIC;
    signal gtwiz_userclk_tx_usrclk2_out           : STD_LOGIC;
    signal gtwiz_userclk_tx_active_out            : STD_LOGIC;
    signal gtwiz_userclk_rx_reset_in              : STD_LOGIC;
    signal gtwiz_userclk_rx_srcclk_out            : STD_LOGIC;
    signal gtwiz_userclk_rx_usrclk_out            : STD_LOGIC;
    signal gtwiz_userclk_rx_usrclk2_out           : STD_LOGIC;
    signal gtwiz_userclk_rx_active_out            : STD_LOGIC;
    signal gtwiz_reset_clk_freerun_in             : STD_LOGIC;
    signal gtwiz_reset_all_in                     : STD_LOGIC;
    signal gtwiz_reset_tx_pll_and_datapath_in     : STD_LOGIC;
    signal gtwiz_reset_tx_datapath_in             : STD_LOGIC;
    signal gtwiz_reset_rx_pll_and_datapath_in     : STD_LOGIC;
    signal gtwiz_reset_rx_datapath_in             : STD_LOGIC;
    signal gtwiz_reset_rx_cdr_stable_out          : STD_LOGIC;
    signal gtwiz_reset_tx_done_out                : STD_LOGIC;
    signal gtwiz_reset_rx_done_out                : STD_LOGIC;
    signal gtwiz_userdata_tx_in                   : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal gtwiz_userdata_rx_out                  : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal drpclk_in                              : STD_LOGIC;
    signal gtrefclk0_in                           : STD_LOGIC;
    signal gtyrxn_in                              : STD_LOGIC;
    signal gtyrxp_in                              : STD_LOGIC;
    signal gtpowergood_out                        : STD_LOGIC;
    signal gtytxn_out                             : STD_LOGIC;
    signal gtytxp_out                             : STD_LOGIC;
    signal rxpmaresetdone_out                     : STD_LOGIC;
    signal txpmaresetdone_out                     : STD_LOGIC;
    signal link_down_latched_reset_in             : std_logic;
    signal link_status_out                        : std_logic;
    signal link_down_latched_out                  : std_logic;
    ----------------------
    signal eth_gtwiz_userclk_tx_reset_in          : STD_LOGIC;
    signal eth_gtwiz_userclk_tx_srcclk_out        : STD_LOGIC;
    signal eth_gtwiz_userclk_tx_usrclk_out        : STD_LOGIC;
    signal eth_gtwiz_userclk_tx_usrclk2_out       : STD_LOGIC;
    signal eth_gtwiz_userclk_tx_active_out        : STD_LOGIC;
    signal eth_gtwiz_userclk_rx_reset_in          : STD_LOGIC;
    signal eth_gtwiz_userclk_rx_srcclk_out        : STD_LOGIC;
    signal eth_gtwiz_userclk_rx_usrclk_out        : STD_LOGIC;
    signal eth_gtwiz_userclk_rx_usrclk2_out       : STD_LOGIC;
    signal eth_gtwiz_userclk_rx_active_out        : STD_LOGIC;
    signal eth_gtwiz_reset_clk_freerun_in         : STD_LOGIC;
    signal eth_gtwiz_reset_all_in                 : STD_LOGIC;
    signal eth_gtwiz_reset_tx_pll_and_datapath_in : STD_LOGIC;
    signal eth_gtwiz_reset_tx_datapath_in         : STD_LOGIC;
    signal eth_gtwiz_reset_rx_pll_and_datapath_in : STD_LOGIC;
    signal eth_gtwiz_reset_rx_datapath_in         : STD_LOGIC;
    signal eth_gtwiz_reset_rx_cdr_stable_out      : STD_LOGIC;
    signal eth_gtwiz_reset_tx_done_out            : STD_LOGIC;
    signal eth_gtwiz_reset_rx_done_out            : STD_LOGIC;
    signal eth_gtwiz_userdata_tx_in               : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal eth_gtwiz_userdata_rx_out              : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal eth_drpclk_in                          : STD_LOGIC;
    signal eth_gtrefclk0_in                       : STD_LOGIC;
    signal eth_gtyrxn_in                          : STD_LOGIC;
    signal eth_gtyrxp_in                          : STD_LOGIC;
    signal eth_gtpowergood_out                    : STD_LOGIC;
    signal eth_gtytxn_out                         : STD_LOGIC;
    signal eth_gtytxp_out                         : STD_LOGIC;
    signal eth_rxpmaresetdone_out                 : STD_LOGIC;
    signal eth_txpmaresetdone_out                 : STD_LOGIC;
    signal eth_link_down_latched_reset_in         : std_logic;
    signal eth_link_status_out                    : std_logic;
    signal eth_link_down_latched_out              : std_logic;

    ----------------------
    signal clk_tx_eth                             : std_logic; -- Ethernet TX Clock
    signal rst_tx_eth                             : std_logic;
    signal clk_rx_eth                             : std_logic; -- Ethernet RX Clock
    signal rst_rx_eth                             : std_logic;
    signal clk_fso_tx                             : std_logic; -- FSO PHY TX Clock 
    signal clk_fso_rx                             : std_logic;
    signal rst_fso_tx                             : std_logic; -- FSO PHY RX Clock
    signal rst_fso_rx                             : std_logic;
    signal framer_in_axis_tdata                   : std_logic_vector(8 - 1 downto 0);
    signal framer_in_axis_tvalid                  : std_logic;
    signal framer_in_axis_tlast                   : std_logic;
    signal framer_in_axis_tready                  : std_logic;
    signal framer_in_axis_tuser                   : std_logic;
    signal cfg_pause_data                         : std_logic_vector(15 downto 0);
    signal tx_pause_valid                         : std_logic;
    signal tx_pause_data                          : std_logic_vector(15 downto 0);
    signal rx_fifo_skipped_frame                  : std_logic_vector(32 - 1 downto 0);
    signal deframer_out_axis_tdata                : std_logic_vector(8 - 1 downto 0);
    signal deframer_out_axis_tvalid               : std_logic;
    signal deframer_out_axis_tlast                : std_logic;
    signal deframer_out_axis_tready               : std_logic;
    signal deframer_out_axis_tuser                : std_logic;
    signal deframer_in_axis_tdata                 : std_logic_vector(8 - 1 downto 0);
    signal deframer_in_axis_tvalid                : std_logic;
    signal deframer_in_axis_tlast                 : std_logic;
    signal deframer_in_axis_tready                : std_logic;
    signal preamble_synced                        : std_logic;
    signal crc16_valid                            : std_logic;
    signal crc16_correct                          : std_logic;
    signal crc32_valid                            : std_logic;
    signal crc32_correct                          : std_logic;
    signal framer_out_axis_tdata                  : std_logic_vector(8 - 1 downto 0);
    signal framer_out_axis_tvalid                 : std_logic;
    signal framer_out_axis_tlast                  : std_logic;
    signal framer_out_axis_tready                 : std_logic;
    signal fso_phy_data_tx                        : std_logic_vector(8 - 1 downto 0);
    signal fso_phy_valid_tx                       : std_logic;
    signal fso_phy_data_rx                        : std_logic_vector(8 - 1 downto 0);
    signal fso_phy_valid_rx                       : std_logic;
    signal trigger                                : std_logic;
    signal tof_counter_valid                      : std_logic;
    signal tof_counter                            : std_logic_vector(40 - 1 downto 0);
    signal framer_warning_flag                    : std_logic;
    signal framer_critical_error_flag             : std_logic;
    --    signal framer_health_monitor_statistics   : health_monitor_statistics_t;
    --    signal deframer_warning_flag                :  std_logic;
    --    signal deframer_critical_error_flag         :  std_logic;
    --    signal deframer_health_monitor_statistics   : health_monitor_statistics_t;
    signal pps_clk                                : std_logic;
    signal LAPC_RPT_RSSI_FAST                     : std_logic_vector(14 - 1 downto 0);
    signal LAPC_RPT_RSSI_MEAN                     : std_logic_vector(8 - 1 downto 0);
    signal LAPC_RPT_RSSI_SDEV                     : std_logic_vector(8 - 1 downto 0);
    signal OISL_PMIN                              : std_logic_vector(8 - 1 downto 0);
    signal OISL_PMAX                              : std_logic_vector(8 - 1 downto 0);
    signal crc_errors_per_second                  : std_logic_vector(32 - 1 downto 0);
    signal fifo_fcch_data                         : std_logic_vector(20 - 1 downto 0);
    signal fifo_fcch_valid                        : std_logic;
    signal fifo_fcch_rd_en                        : std_logic;
    signal clear_stat                             : std_logic;
    signal framer_total_packet_counter            : std_logic_vector(31 downto 0);
    signal framer_total_packet_splitted_counter   : std_logic_vector(31 downto 0);
    signal framer_total_data_frame_counter        : std_logic_vector(31 downto 0);
    signal framer_actual_data_frame_counter       : std_logic_vector(31 downto 0);
    signal framer_total_idle_frame_counter        : std_logic_vector(31 downto 0);
    signal framer_total_packet_drop               : std_logic_vector(31 downto 0);
    signal framer_total_prebuffer_packet_drop     : std_logic_vector(31 downto 0);
    signal framer_frame_length_error              : std_logic_vector(31 downto 0);
    signal framer_bitrate_in                      : std_logic_vector(31 downto 0);
    signal framer_packet_cnt_in                   : std_logic_vector(31 downto 0);
    signal deframer_total_packet_counter          : std_logic_vector(31 downto 0);
    signal deframer_total_payload_counter         : std_logic_vector(31 downto 0);
    signal deframer_total_packet_splitted_counter : std_logic_vector(31 downto 0);
    signal deframer_total_packet_merged_counter   : std_logic_vector(31 downto 0);
    signal deframer_total_data_frame_counter      : std_logic_vector(31 downto 0);
    signal deframer_total_idle_frame_counter      : std_logic_vector(31 downto 0);
    signal deframer_total_payload_error_counter   : std_logic_vector(31 downto 0);
    signal deframer_total_packet_error_counter    : std_logic_vector(31 downto 0);
    signal deframer_watchdog_reset_counter        : std_logic_vector(31 downto 0);
    signal deframer_packet_filter_cnt_in          : std_logic_vector(31 downto 0);
    signal deframer_packet_filter_cnt_out         : std_logic_vector(31 downto 0);
    signal deframer_frame_filter_cnt_in           : std_logic_vector(31 downto 0);
    signal deframer_frame_filter_cnt_out          : std_logic_vector(31 downto 0);
    signal deframer_bitrate_out                   : std_logic_vector(31 downto 0);
    signal deframer_packet_cnt_out                : std_logic_vector(31 downto 0);
    --    signal sda_debug                       : sda_debug_t;
    signal version_mayor                          : unsigned(7 downto 0);
    signal version_minor                          : unsigned(7 downto 0);

    --    signal framer_diagnostics        : framer_diagnostics_t;
    --    signal deframer_diagnostics      : deframer_diagnostics_t;
    --    signal sda_debug                 : sda_debug_t;
    --    signal framer_health_monitor_statistics : health_monitor_statistics_t;
    --    signal deframer_health_monitor_statistics : health_monitor_statistics_t;
    signal deframer_warning_flag        : std_logic;
    signal deframer_critical_error_flag : std_logic;
    signal reserved                     : std_logic_vector(6 downto 0);
    signal clear_framer_statistics      : std_logic;
    signal clear_deframer_statistics    : std_logic;

    -- configuration for the preamble sync algorithm
    signal cfg_acquisition_length : std_logic_vector(31 downto 0);
    signal cfg_tracking_length    : std_logic_vector(31 downto 0);
    signal cfg_max_tracking_error : std_logic_vector(31 downto 0);

    constant PREAMBLE_IN : std_logic_vector(63 downto 0) := X"53225B1D0D73DF03";

    signal rxfifo_tdata  : std_logic_vector(31 downto 0);
    signal rxfifo_tvalid : std_logic;
    signal rxfifo_tready : std_logic;

    signal fso_tx_fifo_tdata  : std_logic_vector(31 downto 0);
    signal fso_tx_fifo_tvalid : std_logic;
    signal fso_tx_fifo_tready : std_logic;

    signal ch_tx_axis_tdata  : std_logic_vector(7 downto 0);
    signal ch_tx_axis_tlast  : std_logic;
    signal ch_tx_axis_tvalid : std_logic;
    signal ch_tx_axis_tready : std_logic;
    signal ch_rx_axis_tvalid : std_logic;
    signal ch_rx_axis_tdata  : std_logic_vector(7 downto 0);

    signal ref_clk_25 : std_logic;

    signal total_frame_counter : std_logic_vector(31 downto 0);
    signal crc_error_counter   : std_logic_vector(31 downto 0);
    signal crc16_err_counter   : std_logic_vector(31 downto 0);
    signal crc32_err_counter   : std_logic_vector(31 downto 0);
    -- Stats from the preamble sync
    signal new_search_counter  : std_logic_vector(31 downto 0);
    signal sync_loss_counter   : std_logic_vector(31 downto 0);

    signal slowest_sync_clk     : STD_LOGIC;
    signal ext_reset_in         : STD_LOGIC;
    signal aux_reset_in         : STD_LOGIC;
    signal mb_debug_sys_rst     : STD_LOGIC;
    signal dcm_locked_0           : STD_LOGIC;
    signal dcm_locked_1           : STD_LOGIC;
    signal mb_reset             : STD_LOGIC;
    signal bus_struct_reset     : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal peripheral_reset     : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal interconnect_aresetn : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal peripheral_aresetn   : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal rx_reset_out         : std_logic;
    signal data_good_in         : std_logic;
    signal loopback_in          : std_logic_vector(2 downto 0);
    signal loopback_vio         : std_logic_vector(2 downto 0);
    signal fec_ctrl             : std_logic_vector(2 downto 0);
    signal tx_reset_out         : std_logic;
    signal sfp0_tx_disable_n    : std_logic;
    signal clk_sys              : std_logic;
    signal clk_let              : std_logic; -- Main clock 200 MHz
    signal clk_ldpc             : std_logic;
    signal clk_ref_fso           : std_logic;
    signal clk_ref_eth           : std_logic;
    signal clk_gt_free          : std_logic;
    signal clk_eth_free         : std_logic;
    signal AXI_ETH0_REG_IF_m    : AXI_Lite_master;
    signal AXI_ETH0_REG_IF_s    : AXI_Lite_slave;
    signal AXI_LET0_m           : AXI_Lite_master;
    signal AXI_LET0_s           : AXI_Lite_slave;
    signal AXI_ETH1_REG_IF_m    : AXI_Lite_master;
    signal AXI_ETH1_REG_IF_s    : AXI_Lite_slave;
    signal AXI_LET1_m           : AXI_Lite_master;
    signal AXI_LET1_s           : AXI_Lite_slave;
    signal sync100_rstn         : STD_LOGIC_VECTOR(0 to 0);
    signal eeprom_spi_cs_n      : STD_LOGIC_VECTOR(1 downto 0);
    signal eeprom_spi_miso      : STD_LOGIC;
    signal eeprom_spi_mosi      : STD_LOGIC;
    signal eeprom_spi_sck       : STD_LOGIC;
    signal clk_ref125           : std_logic;
    --------------------------------------------------------

begin

    --SFP0_TX_DISABLE_B   <= '1'; --   <= sfp0_tx_disable_n;      -- Tx enable
    SFP0_TX_DISABLE_B <= '1';
    SFP1_TX_DISABLE_B <= '1';
    SFP2_TX_DISABLE_B <= '1';
    SFP3_TX_DISABLE_B <= '1';
    -- Configuration for the preamble sync
    --    cfg_acquisition_length <= std_logic_vector(to_unsigned(4, 32));
    --    cfg_tracking_length    <= std_logic_vector(to_unsigned(16, 32));
    --    cfg_max_tracking_error <= std_logic_vector(to_unsigned(4, 32));
--    rst_async_n       <= peripheral_aresetn(0);
--    rst_async         <= peripheral_reset(0);
    loopback_in       <= loopback_vio;  --(others=>'0'); --"010"; --(others=>'0');

    inst_clock_gen_top : entity work.clock_gen_top
        port map(
            clk_in0_p               => USER_SI570_C0_P,
            clk_in0_n               => USER_SI570_C0_N,
            clk_in1_p               => USER_SI570_C1_P,
            clk_in1_n               => USER_SI570_C1_N,
            rst_ext                => RESET, --rst_ext, TODO: external reset disabled
--            mgtrefclk_p            => MGTREFCLK0P_128,
--            mgtrefclk_n            => MGTREFCLK0N_128,
--            CLK_125_P              => CLK_125_P,
--            CLK_125_N              => CLK_125_N,
--            USER_SMA_MGT_CLOCK_C_P => USER_SMA_MGT_CLOCK_C_P,
--            USER_SMA_MGT_CLOCK_C_N => USER_SMA_MGT_CLOCK_C_N,
            Q7_OUT_C_P             => Q7_OUT_C_P, --,
            Q7_OUT_C_N             => Q7_OUT_C_N, --,            
            Q11_OUT_C_P             => '0', --Q11_OUT_C_P, --,
            Q11_OUT_C_N             => '0', --Q11_OUT_C_N, --,
            clk_sys                => clk_sys,
            clk_let                => clk_let,
            clk_ldpc               => clk_ldpc,
            clk_ref_eth             => clk_ref_eth,
            clk_ref_fso             => open, --clk_ref_fso,
            clk_gt_free            => clk_gt_free,
            clk_eth_free           => clk_eth_free,
            rst_async              => rst_async,
            rst_async_n            => rst_async_n,
            dcm_locked_0           => dcm_locked_0,
            dcm_locked_1           => dcm_locked_1
        );

    inst_top_let_0 : entity work.top_let
        generic map(
            g_DEBUG => 1
        )
        port map(
            clk_sys           => clk_sys,
            s_axi_resetn      => s_axi_resetn,
            clk_let           => clk_let,
            clk_ldpc          => clk_ldpc,
            clk_ref_fso_p        => Q11_OUT_C_P,
            clk_ref_fso_n        => Q11_OUT_C_N,
            clk_ref_eth        => clk_ref_eth,
            clk_gt_free       => clk_gt_free,
            clk_eth_free      => clk_eth_free,
            ext_rst_async     => RESET, --TODO: external reset disabled
            eth_txp           => ETH0_TX_P,
            eth_txn           => ETH0_TX_N,
            eth_rxp           => ETH0_RX_P,
            eth_rxn           => ETH0_RX_N,
            fso_txp           => SFP0_TX_P,
            fso_txn           => SFP0_TX_N,
            fso_rxp           => SFP0_RX_P,
            fso_rxn           => SFP0_RX_N,
            AXI_ETH_REG_IF_m  => AXI_ETH0_REG_IF_m,
            AXI_ETH_REG_IF_s  => AXI_ETH0_REG_IF_s,
            AXI_LET_m         => AXI_LET0_m,
            AXI_LET_s         => AXI_LET0_s,
            dcm_locked_0        => dcm_locked_0,
            dcm_locked_1        => dcm_locked_1,
            tx_ch1_modset_pwm => open,  -- Not used in Dev Kit
            tx_ch2_modset_pwm => open,  -- Not used in Dev Kit
            modset_pwm_enable => (others => '0'), -- Not used in Dev Kit
            modset_pwm_freq   => (others => '0') -- Not used in Dev Kit
        );
        
    --    inst_top_let_1 : entity work.top_let
    --        generic map(
    --            g_DEBUG => 0
    --        )
    --        port map(
    --            clk_sys           => clk_sys,
    --            s_axi_resetn           => s_axi_resetn,
    --            clk_let           => clk_let,
    --            clk_ldpc          => clk_ldpc,
    --            clk_ref156        => clk_ref156,
    --            clk_gt_free       => clk_gt_free,
    --            clk_eth_free      => clk_eth_free,
    --            ext_rst_async     => '0',   --RESET, TODO: external reset disabled
    --            eth_txp           => ETH1_TX_P,
    --            eth_txn           => ETH1_TX_N,
    --            eth_rxp           => ETH1_RX_P,
    --            eth_rxn           => ETH1_RX_N,
    --            fso_txp           => SFP1_TX_P,
    --            fso_txn           => SFP1_TX_N,
    --            fso_rxp           => SFP1_RX_P,
    --            fso_rxn           => SFP1_RX_N,
    --            AXI_ETH_REG_IF_m  => AXI_ETH1_REG_IF_m,
    --            AXI_ETH_REG_IF_s  => AXI_ETH1_REG_IF_s,
    --            AXI_LET_m         => AXI_LET1_m,
    --            AXI_LET_s         => AXI_LET1_s,
    --            dcm_locked        => dcm_locked,
    --            tx_ch1_modset_pwm => open,
    --            tx_ch2_modset_pwm => open,
    --            modset_pwm_enable => (others => '0'),
    --            modset_pwm_freq   => (others => '0')
    --        );

    inst_system_LET_wrapper : entity work.system_LET_wrapper
        port map(
            AXI_LET0_AXI_ETH_REG_IF_araddr  => AXI_ETH0_REG_IF_m.araddr,
            AXI_LET0_AXI_ETH_REG_IF_arprot  => AXI_ETH0_REG_IF_m.arprot,
            AXI_LET0_AXI_ETH_REG_IF_arready => AXI_ETH0_REG_IF_s.arready,
            AXI_LET0_AXI_ETH_REG_IF_arvalid => AXI_ETH0_REG_IF_m.arvalid,
            AXI_LET0_AXI_ETH_REG_IF_awaddr  => AXI_ETH0_REG_IF_m.awaddr,
            AXI_LET0_AXI_ETH_REG_IF_awprot  => AXI_ETH0_REG_IF_m.awprot,
            AXI_LET0_AXI_ETH_REG_IF_awready => AXI_ETH0_REG_IF_s.awready,
            AXI_LET0_AXI_ETH_REG_IF_awvalid => AXI_ETH0_REG_IF_m.awvalid,
            AXI_LET0_AXI_ETH_REG_IF_bready  => AXI_ETH0_REG_IF_m.bready,
            AXI_LET0_AXI_ETH_REG_IF_bresp   => AXI_ETH0_REG_IF_s.bresp,
            AXI_LET0_AXI_ETH_REG_IF_bvalid  => AXI_ETH0_REG_IF_s.bvalid,
            AXI_LET0_AXI_ETH_REG_IF_rdata   => AXI_ETH0_REG_IF_s.rdata,
            AXI_LET0_AXI_ETH_REG_IF_rready  => AXI_ETH0_REG_IF_m.rready,
            AXI_LET0_AXI_ETH_REG_IF_rresp   => AXI_ETH0_REG_IF_s.rresp,
            AXI_LET0_AXI_ETH_REG_IF_rvalid  => AXI_ETH0_REG_IF_s.rvalid,
            AXI_LET0_AXI_ETH_REG_IF_wdata   => AXI_ETH0_REG_IF_m.wdata,
            AXI_LET0_AXI_ETH_REG_IF_wready  => AXI_ETH0_REG_IF_s.wready,
            AXI_LET0_AXI_ETH_REG_IF_wstrb   => AXI_ETH0_REG_IF_m.wstrb,
            AXI_LET0_AXI_ETH_REG_IF_wvalid  => AXI_ETH0_REG_IF_m.wvalid,
            AXI_LET0_AXI_LET_araddr         => AXI_LET0_m.araddr,
            AXI_LET0_AXI_LET_arprot         => AXI_LET0_m.arprot,
            AXI_LET0_AXI_LET_arready        => AXI_LET0_s.arready(0),
            AXI_LET0_AXI_LET_arvalid        => AXI_LET0_m.arvalid(0),
            AXI_LET0_AXI_LET_awaddr         => AXI_LET0_m.awaddr,
            AXI_LET0_AXI_LET_awprot         => AXI_LET0_m.awprot,
            AXI_LET0_AXI_LET_awready        => AXI_LET0_s.awready(0),
            AXI_LET0_AXI_LET_awvalid        => AXI_LET0_m.awvalid(0),
            AXI_LET0_AXI_LET_bready         => AXI_LET0_m.bready(0),
            AXI_LET0_AXI_LET_bresp          => AXI_LET0_s.bresp,
            AXI_LET0_AXI_LET_bvalid         => AXI_LET0_s.bvalid(0),
            AXI_LET0_AXI_LET_rdata          => AXI_LET0_s.rdata,
            AXI_LET0_AXI_LET_rready         => AXI_LET0_m.rready(0),
            AXI_LET0_AXI_LET_rresp          => AXI_LET0_s.rresp,
            AXI_LET0_AXI_LET_rvalid         => AXI_LET0_s.rvalid(0),
            AXI_LET0_AXI_LET_wdata          => AXI_LET0_m.wdata,
            AXI_LET0_AXI_LET_wready         => AXI_LET0_s.wready(0),
            AXI_LET0_AXI_LET_wstrb          => AXI_LET0_m.wstrb,
            AXI_LET0_AXI_LET_wvalid         => AXI_LET0_m.wvalid(0),
            AXI_LET1_AXI_ETH_REG_IF_araddr  => AXI_ETH1_REG_IF_m.araddr,
            AXI_LET1_AXI_ETH_REG_IF_arprot  => AXI_ETH1_REG_IF_m.arprot,
            AXI_LET1_AXI_ETH_REG_IF_arready => AXI_ETH1_REG_IF_s.arready,
            AXI_LET1_AXI_ETH_REG_IF_arvalid => AXI_ETH1_REG_IF_m.arvalid,
            AXI_LET1_AXI_ETH_REG_IF_awaddr  => AXI_ETH1_REG_IF_m.awaddr,
            AXI_LET1_AXI_ETH_REG_IF_awprot  => AXI_ETH1_REG_IF_m.awprot,
            AXI_LET1_AXI_ETH_REG_IF_awready => AXI_ETH1_REG_IF_s.awready,
            AXI_LET1_AXI_ETH_REG_IF_awvalid => AXI_ETH1_REG_IF_m.awvalid,
            AXI_LET1_AXI_ETH_REG_IF_bready  => AXI_ETH1_REG_IF_m.bready,
            AXI_LET1_AXI_ETH_REG_IF_bresp   => AXI_ETH1_REG_IF_s.bresp,
            AXI_LET1_AXI_ETH_REG_IF_bvalid  => AXI_ETH1_REG_IF_s.bvalid,
            AXI_LET1_AXI_ETH_REG_IF_rdata   => AXI_ETH1_REG_IF_s.rdata,
            AXI_LET1_AXI_ETH_REG_IF_rready  => AXI_ETH1_REG_IF_m.rready,
            AXI_LET1_AXI_ETH_REG_IF_rresp   => AXI_ETH1_REG_IF_s.rresp,
            AXI_LET1_AXI_ETH_REG_IF_rvalid  => AXI_ETH1_REG_IF_s.rvalid,
            AXI_LET1_AXI_ETH_REG_IF_wdata   => AXI_ETH1_REG_IF_m.wdata,
            AXI_LET1_AXI_ETH_REG_IF_wready  => AXI_ETH1_REG_IF_s.wready,
            AXI_LET1_AXI_ETH_REG_IF_wstrb   => AXI_ETH1_REG_IF_m.wstrb,
            AXI_LET1_AXI_ETH_REG_IF_wvalid  => AXI_ETH1_REG_IF_m.wvalid,
            AXI_LET1_AXI_LET_araddr         => AXI_LET1_m.araddr,
            AXI_LET1_AXI_LET_arprot         => AXI_LET1_m.arprot,
            AXI_LET1_AXI_LET_arready        => AXI_LET1_s.arready(0),
            AXI_LET1_AXI_LET_arvalid        => AXI_LET1_m.arvalid(0),
            AXI_LET1_AXI_LET_awaddr         => AXI_LET1_m.awaddr,
            AXI_LET1_AXI_LET_awprot         => AXI_LET1_m.awprot,
            AXI_LET1_AXI_LET_awready        => AXI_LET1_s.awready(0),
            AXI_LET1_AXI_LET_awvalid        => AXI_LET1_m.awvalid(0),
            AXI_LET1_AXI_LET_bready         => AXI_LET1_m.bready(0),
            AXI_LET1_AXI_LET_bresp          => AXI_LET1_s.bresp,
            AXI_LET1_AXI_LET_bvalid         => AXI_LET1_s.bvalid(0),
            AXI_LET1_AXI_LET_rdata          => AXI_LET1_s.rdata,
            AXI_LET1_AXI_LET_rready         => AXI_LET1_m.rready(0),
            AXI_LET1_AXI_LET_rresp          => AXI_LET1_s.rresp,
            AXI_LET1_AXI_LET_rvalid         => AXI_LET1_s.rvalid(0),
            AXI_LET1_AXI_LET_wdata          => AXI_LET1_m.wdata,
            AXI_LET1_AXI_LET_wready         => AXI_LET1_s.wready(0),
            AXI_LET1_AXI_LET_wstrb          => AXI_LET1_m.wstrb,
            AXI_LET1_AXI_LET_wvalid         => AXI_LET1_m.wvalid(0),
            --            EMC_INTF_addr                   => open, --EMC_INTF_addr,
            --            EMC_INTF_adv_ldn                => open, --EMC_INTF_adv_ldn,
            --            EMC_INTF_ben                    => open, --EMC_INTF_ben,
            --            EMC_INTF_ce                     => open, --EMC_INTF_ce,
            --            EMC_INTF_clken                  => open, --EMC_INTF_clken,
            --            EMC_INTF_cre                    => open, --EMC_INTF_cre,
            --            EMC_INTF_dq_io                  => open, --EMC_INTF_dq_io,
            --            EMC_INTF_lbon                   => open, --EMC_INTF_lbon,
            --            EMC_INTF_oen                    => open, --EMC_INTF_oen,
            --            EMC_INTF_qwen                   => open, --EMC_INTF_qwen,
            --            EMC_INTF_rnw                    => open, --EMC_INTF_rnw,
            --            EMC_INTF_rpn                    => open, --EMC_INTF_rpn,
            --            EMC_INTF_wait                   => "0",  --EMC_INTF_wait,
            --            EMC_INTF_wen                    => open, --EMC_INTF_wen,
            --            STARTUP_IO_cfgclk                  => open,
            --            STARTUP_IO_cfgmclk                 => open,
            --            STARTUP_IO_eos                     => open,
            --            STARTUP_IO_gsr                     => '0',
            --            STARTUP_IO_gts                     => '0',
            --            STARTUP_IO_keyclearb               => '1',
            --            STARTUP_IO_userdoneo               => '1',
            --            STARTUP_IO_usrclkts                => '0',
            --            STARTUP_IO_usrdonets               => '1',
            clk_let                         => clk_let,
            clnt1_edfa_nom_rx               => open, --clnt1_edfa_nom_rx,
            clnt1_edfa_nom_tx               => '0', --clnt1_edfa_nom_tx,
            clnt1_edfa_red_rx               => open, --clnt1_edfa_red_rx,
            clnt1_edfa_red_tx               => '0', --clnt1_edfa_red_tx,
            clnt2_edfa_nom_rx               => open, --clnt2_edfa_nom_rx,
            clnt2_edfa_nom_tx               => '0', --clnt2_edfa_nom_tx,
            clnt2_edfa_red_rx               => open, --clnt2_edfa_red_rx,
            clnt2_edfa_red_tx               => '0', --clnt2_edfa_red_tx,
            dcm_locked                      => dcm_locked_1,
            eeprom_spi_cs_n                 => eeprom_spi_cs_n,
            eeprom_spi_miso                 => eeprom_spi_miso,
            eeprom_spi_mosi                 => eeprom_spi_mosi,
            eeprom_spi_sck                  => eeprom_spi_sck,
            ext_reset                       => RESET,
            let1g_pps_rx                    => '0', --
            --            let1g_sys_mon_i2c_scl           => '0',  --let1g_sys_mon_i2c_scl,
            --            let1g_sys_mon_i2c_sda           => '0',  --let1g_sys_mon_i2c_sda,
            let1g_uart1_lvttl_rx            => '0', --let1g_uart1_lvttl_rx,
            let1g_uart1_lvttl_txd           => open, --let1g_uart1_lvttl_txd,
            let1g_uart2_lvttl_rx            => '0', --let1g_uart2_lvttl_rx,
            let1g_uart2_lvttl_txd           => open, --let1g_uart2_lvttl_txd,
            --            pow_sup_i2c_scl_io              => open,  --pow_sup_i2c_scl_io,
            --            pow_sup_i2c_sda_io              => open,  --pow_sup_i2c_sda_io,
            sem_flash_cs_b                  => open, --sem_flash_cs_b,
            sem_flash_miso                  => '0', --sem_flash_miso,
            sem_flash_mosi                  => open, --sem_flash_mosi,
            sem_flash_sck                   => open, --sem_flash_sck,
            serv_uart_rx1                   => '0', --serv_uart_rx1,
            serv_uart_rx2                   => '0', --serv_uart_rx2,
            serv_uart_tx1                   => open, --serv_uart_tx1,
            serv_uart_tx2                   => open, --serv_uart_tx2,
            sync100_rstn(0)                 => s_axi_resetn,
            clk_sys                         => clk_sys,
            tcu_sem_error                   => '0', --tcu_sem_error,
            tcu_sem_rx                      => open, --tcu_sem_rx,
            tcu_sem_tx                      => '0' --tcu_sem_tx
        );

        ------------------------------------------------------------------------------------------------------------------
        ---------
        ------------------------------------------------------------------------------------------------------------------

        --    inst_let_sda_top : entity let_sda_lib.let_sda_top
        --        Port map(
        --            clk_let                                => clk_200, --: in  std_logic; -- Main clock 200 MHz
        --            rst_let                                => rst_async, --: in  std_logic;
        --            rst_let_rx                             => rst_async,
        --            clk_tx_eth                             => clk_200, --: in  std_logic; -- Ethernet TX Clock
        --            rst_tx_eth                             => rst_async, --: in  std_logic;
        --            clk_rx_eth                             => clk_200, --: in  std_logic; -- Ethernet RX Clock
        --            rst_rx_eth                             => rst_async, --: in  std_logic; 
        --            clk_fso_tx                             => '0', --: in  std_logic; -- FSO PHY TX Clock 
        --            clk_fso_rx                             => '0', --: in  std_logic;
        --            rst_fso_tx                             => '0', --: in  std_logic; -- FSO PHY RX Clock
        --            rst_fso_rx                             => '0', --: in  std_logic;
        --
        --            -- Input interface ETH
        --            ----------------------
        --            framer_in_axis_tdata                   => framer_in_axis_tdata, --: in  std_logic_vector(8 - 1 downto 0);
        --            framer_in_axis_tvalid                  => '0', --framer_in_axis_tvalid, --: in  std_logic;
        --            framer_in_axis_tlast                   => framer_in_axis_tlast, --: in  std_logic;
        --            framer_in_axis_tready                  => open, --: out std_logic;
        --            framer_in_axis_tuser                   => framer_in_axis_tuser, --: in  std_logic;
        --
        --            cfg_pause_data                         => cfg_pause_data, --: in std_logic_vector(15 downto 0);
        --            tx_pause_valid                         => open, --tx_pause_valid, --: out std_logic;
        --            tx_pause_data                          => tx_pause_data, --: out std_logic_vector(15 downto 0);
        --            rx_fifo_skipped_frame                  => rx_fifo_skipped_frame, --: out std_logic_vector(32 - 1 downto 0);
        --
        --            -- Output interface ETH
        --            ----------------------
        --            deframer_out_axis_tdata                => deframer_out_axis_tdata, --: out std_logic_vector(8 - 1 downto 0);
        --            deframer_out_axis_tvalid               => deframer_out_axis_tvalid, --: out std_logic;
        --            deframer_out_axis_tlast                => deframer_out_axis_tlast, --: out std_logic;
        --            deframer_out_axis_tready               => deframer_out_axis_tready, --: in  std_logic;
        --            deframer_out_axis_tuser                => deframer_out_axis_tuser, --: out std_logic;
        --
        --            -- Input interface Creonic
        --            ----------------------
        --            deframer_in_axis_tdata                 => deframer_in_axis_tdata, --: in  std_logic_vector(8 - 1 downto 0);
        --            deframer_in_axis_tvalid                => deframer_in_axis_tvalid, --: in std_logic;
        --            deframer_in_axis_tlast                 => deframer_in_axis_tlast, --: in std_logic;
        --            deframer_in_axis_tready                => deframer_in_axis_tready, --: out std_logic;
        --            preamble_synced                        => preamble_synced, --: in std_logic;
        --            crc16_valid                            => crc16_valid, --: in std_logic;
        --            crc16_correct                          => crc16_correct, --: in std_logic;
        --            crc32_valid                            => crc32_valid, --: in std_logic;
        --            crc32_correct                          => crc32_correct, --: in std_logic;
        --
        --            -- Output interface Creonic
        --            ----------------------
        --            framer_out_axis_tdata                  => framer_out_axis_tdata, --: out  std_logic_vector(8 - 1 downto 0);
        --            framer_out_axis_tvalid                 => framer_out_axis_tvalid, --: out std_logic;
        --            framer_out_axis_tlast                  => framer_out_axis_tlast, --: out std_logic;
        --            framer_out_axis_tready                 => framer_out_axis_tready, --: in std_logic;
        --
        --            -- Input data from FSO PHY
        --            ----------------------
        --            fso_phy_data_tx                        => (others => '0'), --fso_userdata_tx_out, --: in  std_logic_vector(16 - 1 downto 0);
        --            fso_phy_valid_tx                       => '0', --fso_uservalid_tx_out, --fso_tx_axis_tvalid, --: in  std_logic;
        --            fso_phy_data_rx                        => (others => '0'), --fso_userdata_rx_out, --: in  std_logic_vector(16 - 1 downto 0);
        --            fso_phy_valid_rx                       => '0', --fso_uservalid_rx_out, --fso_rx_axis_tvalid, --: in  std_logic;
        --
        --            -- Ranging data: Time of flight in 1/16 fso_clk
        --            ----------------------
        --            trigger                                => '0', --: in  std_logic;
        --            tof_counter_valid                      => open, --: out std_logic;
        --            tof_counter                            => open, --: out std_logic_vector(40-1 downto 0);
        --
        --            -- Framer Health Monitor
        --            ----------------------
        --            framer_warning_flag                    => open, --framer_warning_flag, --: out  std_logic;
        --            framer_critical_error_flag             => open, --framer_critical_error_flag, --: out  std_logic;
        --            framer_health_monitor_statistics       => open, --framer_health_monitor_statistics, --: out health_monitor_statistics_t;
        --
        --            -- Deframer Health Monitor
        --            ----------------------
        --            deframer_warning_flag                  => open, --deframer_warning_flag, --: out  std_logic;
        --            deframer_critical_error_flag           => open, --deframer_critical_error_flag, --: out  std_logic;
        --            deframer_health_monitor_statistics     => open, --deframer_health_monitor_statistics, --: out health_monitor_statistics_t;
        --
        --            -- FCCH
        --            ----------------------
        --            pps_clk                                => '0', --: in std_logic;
        --
        --            -- From Register Map
        --            LAPC_RPT_RSSI_FAST                     => LAPC_RPT_RSSI_FAST, --: in std_logic_vector(14-1 downto 0);
        --            LAPC_RPT_RSSI_MEAN                     => LAPC_RPT_RSSI_MEAN, --: in std_logic_vector(8-1 downto 0);
        --            LAPC_RPT_RSSI_SDEV                     => LAPC_RPT_RSSI_SDEV, --: in std_logic_vector(8-1 downto 0);
        --            OISL_PMIN                              => OISL_PMIN, --: in std_logic_vector(8-1 downto 0);
        --            OISL_PMAX                              => OISL_PMAX, --: in std_logic_vector(8-1 downto 0);
        --            crc_errors_per_second                  => crc_errors_per_second,
        --            -- To Register Map
        --            fifo_fcch_data                         => fifo_fcch_data, --: out std_logic_vector(20-1 downto 0);
        --            fifo_fcch_valid                        => fifo_fcch_valid, --: out std_logic;
        --            fifo_fcch_rd_en                        => fifo_fcch_rd_en, --: in std_logic;
        --            --tx_axis_mac_tready            => tx_axis_mac_tready,
        --
        --            -- Statistics
        --            ----------------------
        --            clear_stat                             => clear_stat, --clear_stat, --: in  std_logic;
        --            -- Framer
        --            framer_total_packet_counter            => framer_total_packet_counter, --: out std_logic_vector(31 downto 0);
        --            framer_total_packet_splitted_counter   => framer_total_packet_splitted_counter, --: out std_logic_vector(31 downto 0);
        --            framer_total_data_frame_counter        => framer_total_data_frame_counter, --: out std_logic_vector(31 downto 0);
        --            framer_actual_data_frame_counter       => framer_actual_data_frame_counter, --: out std_logic_vector(31 downto 0);
        --            framer_total_idle_frame_counter        => framer_total_idle_frame_counter, --: out std_logic_vector(31 downto 0);
        --            framer_total_packet_drop               => framer_total_packet_drop, --: out std_logic_vector(31 downto 0);
        --            framer_total_prebuffer_packet_drop     => framer_total_prebuffer_packet_drop, --: out std_logic_vector(31 downto 0);
        --            framer_frame_length_error              => framer_frame_length_error, --: out std_logic_vector(31 downto 0);
        --            framer_bitrate_in                      => framer_bitrate_in, --: out std_logic_vector(31 downto 0);
        --            framer_packet_cnt_in                   => framer_packet_cnt_in, --: out std_logic_vector(31 downto 0);
        --
        --            -- Deframer
        --            deframer_total_packet_counter          => deframer_total_packet_counter, --: out std_logic_vector(31 downto 0);
        --            deframer_total_payload_counter         => deframer_total_payload_counter, --: out std_logic_vector(31 downto 0);
        --            deframer_total_packet_splitted_counter => deframer_total_packet_splitted_counter, --: out std_logic_vector(31 downto 0);
        --            deframer_total_packet_merged_counter   => deframer_total_packet_merged_counter, --: out std_logic_vector(31 downto 0);
        --            deframer_total_data_frame_counter      => deframer_total_data_frame_counter, --: out std_logic_vector(31 downto 0);
        --            deframer_total_idle_frame_counter      => deframer_total_idle_frame_counter, --: out std_logic_vector(31 downto 0);
        --            deframer_total_payload_error_counter   => deframer_total_payload_error_counter, --: out std_logic_vector(31 downto 0);
        --            deframer_total_packet_error_counter    => deframer_total_packet_error_counter, --: out std_logic_vector(31 downto 0);
        --            deframer_watchdog_reset_counter        => deframer_watchdog_reset_counter, --: out std_logic_vector(31 downto 0);
        --            deframer_packet_filter_cnt_in          => deframer_packet_filter_cnt_in, --: out std_logic_vector(31 downto 0);
        --            deframer_packet_filter_cnt_out         => deframer_packet_filter_cnt_out, --: out std_logic_vector(31 downto 0);
        --            deframer_frame_filter_cnt_in           => deframer_frame_filter_cnt_in, --: out std_logic_vector(31 downto 0);
        --            deframer_frame_filter_cnt_out          => deframer_frame_filter_cnt_out, --: out std_logic_vector(31 downto 0);
        --            deframer_bitrate_out                   => deframer_bitrate_out, --: out std_logic_vector(31 downto 0);
        --            deframer_packet_cnt_out                => deframer_packet_cnt_out, --: out std_logic_vector(31 downto 0);
        --
        --            sda_debug                              => open, --sda_debug, --: out sda_debug_t;
        --
        --            -- Version control
        --            ----------------------
        --            version_mayor                          => open, --: out unsigned(7 downto 0);
        --            version_minor                          => open --: out unsigned(7 downto 0)
        --        );
        --
        --    uut : entity frame_decoding.frame_fec_top
        --        port map(
        --            ldpc_clk               => ldpc_clk,
        --            clk                    => clk_200,
        --            rst                    => rst_async,
        --            preamble               => PREAMBLE_IN,
        --            cfg_acquisition_length => cfg_acquisition_length,
        --            cfg_tracking_length    => cfg_tracking_length,
        --            cfg_max_tracking_error => cfg_max_tracking_error,
        --            tx_axis_ctrl_tready    => open,
        --            tx_axis_ctrl_tvalid    => '1', --tx_axis_ctrl_tvalid,
        --            tx_axis_ctrl_tdata     => fec_ctrl, --"000", --tx_axis_ctrl_tdata,
        --            -- TX data input - header and payload data
        --            ----------------------
        --            tx_axis_tready         => framer_out_axis_tready,
        --            tx_axis_tvalid         => framer_out_axis_tvalid,
        --            tx_axis_tlast          => framer_out_axis_tlast,
        --            tx_axis_tdata          => framer_out_axis_tdata,
        --            -- Output data handling
        --            -----------------------
        --            ch_tx_axis_tready      => ch_tx_axis_tready,
        --            ch_tx_axis_tvalid      => ch_tx_axis_tvalid,
        --            ch_tx_axis_tlast       => ch_tx_axis_tlast,
        --            ch_tx_axis_tdata       => ch_tx_axis_tdata,
        --            ch_rx_axis_ctrl_tready => open, --ch_rx_axis_ctrl_tready,
        --            ch_rx_axis_ctrl_tvalid => '1', --ch_rx_axis_ctrl_tvalid,
        --            ch_rx_axis_ctrl_tdata  => fec_ctrl, --"000", --ch_rx_axis_ctrl_tdata,
        --            -- RX data input
        --            ----------------------
        --            ch_rx_axis_tvalid      => ch_rx_axis_tvalid, --ch_rx_axis_tvalid,
        --            ch_rx_axis_tdata       => ch_rx_axis_tdata, --ch_rx_axis_tdata,
        --            -- Status
        --            -----------------------
        --            crc16_valid            => crc16_valid,
        --            crc16_correct          => crc16_correct,
        --            crc32_valid            => crc32_valid,
        --            crc32_correct          => crc32_correct,
        --            preamble_synced        => preamble_synced,
        --            -- More diagnostic informations
        --            -------------------------------------------------------
        --            clear_stat             => clear_stat,
        --            total_frame_counter    => total_frame_counter,
        --            crc_error_counter      => crc_error_counter,
        --            crc16_err_counter      => crc16_err_counter,
        --            crc32_err_counter      => crc32_err_counter,
        --            -- Stats from the preamble sync
        --            new_search_counter     => new_search_counter,
        --            sync_loss_counter      => sync_loss_counter,
        --            -- RX data output - header and payload data
        --            -----------------------
        --            rx_axis_tready         => deframer_in_axis_tready,
        --            rx_axis_tvalid         => deframer_in_axis_tvalid,
        --            rx_axis_tlast          => deframer_in_axis_tlast,
        --            rx_axis_tdata          => deframer_in_axis_tdata
        --        );
        --
        --    inst_gtwizard_ultrascale_0_example_top : gtwizard_ultrascale_0_example_top
        --        PORT MAP(
        --            mgtrefclk0_x0y1               => mgtrefclk0_x0y1,
        --            ch0_gtyrxn_in                 => SFP0_RX_N,
        --            ch0_gtyrxp_in                 => SFP0_RX_P,
        --            ch0_gtytxn_out                => SFP0_TX_N,
        --            ch0_gtytxp_out                => SFP0_TX_P,
        --            hb_gtwiz_reset_clk_freerun_in => mgtrefclk, --gtwiz_reset_clk_freerun_in,
        --            hb_gtwiz_reset_all_in         => rst_async,
        --            link_down_latched_reset_in    => '0', --link_down_latched_reset_in,
        --            tx_usrclk2_out                => gtwiz_userclk_tx_usrclk2_out,
        --            rx_usrclk2_out                => gtwiz_userclk_rx_usrclk2_out,
        --            gtwiz_userdata_rx_out         => gtwiz_userdata_rx_out,
        --            gtwiz_userdata_tx_in          => gtwiz_userdata_tx_in,
        --            rx_reset_out                  => rx_reset_out,
        --            data_good_in                  => preamble_synced,
        --            tx_reset_out                  => tx_reset_out,
        --            loopback_in                   => loopback_in
        --        );

        --    inst_gtwizard_ultrascale_0 : gtwizard_ultrascale_0
        --        PORT MAP(
        --            gtwiz_userclk_tx_reset_in          => gtwiz_userclk_tx_reset_in,
        --            gtwiz_userclk_tx_srcclk_out        => gtwiz_userclk_tx_srcclk_out,
        --            gtwiz_userclk_tx_usrclk_out        => gtwiz_userclk_tx_usrclk_out,
        --            gtwiz_userclk_tx_usrclk2_out       => gtwiz_userclk_tx_usrclk2_out,
        --            gtwiz_userclk_tx_active_out        => gtwiz_userclk_tx_active_out,
        --            gtwiz_userclk_rx_reset_in          => gtwiz_userclk_rx_reset_in,
        --            gtwiz_userclk_rx_srcclk_out        => gtwiz_userclk_rx_srcclk_out,
        --            gtwiz_userclk_rx_usrclk_out        => gtwiz_userclk_rx_usrclk_out,
        --            gtwiz_userclk_rx_usrclk2_out       => gtwiz_userclk_rx_usrclk2_out,
        --            gtwiz_userclk_rx_active_out        => gtwiz_userclk_rx_active_out,
        --            gtwiz_reset_clk_freerun_in         => gtwiz_reset_clk_freerun_in,
        --            gtwiz_reset_all_in                 => gtwiz_reset_all_in,
        --            gtwiz_reset_tx_pll_and_datapath_in => gtwiz_reset_tx_pll_and_datapath_in,
        --            gtwiz_reset_tx_datapath_in         => gtwiz_reset_tx_datapath_in,
        --            gtwiz_reset_rx_pll_and_datapath_in => gtwiz_reset_rx_pll_and_datapath_in,
        --            gtwiz_reset_rx_datapath_in         => gtwiz_reset_rx_datapath_in,
        --            gtwiz_reset_rx_cdr_stable_out      => gtwiz_reset_rx_cdr_stable_out,
        --            gtwiz_reset_tx_done_out            => gtwiz_reset_tx_done_out,
        --            gtwiz_reset_rx_done_out            => gtwiz_reset_rx_done_out,
        --            gtwiz_userdata_tx_in               => gtwiz_userdata_tx_in,
        --            gtwiz_userdata_rx_out              => gtwiz_userdata_rx_out,
        --            drpclk_in                          => drpclk_in,
        --            gtrefclk0_in                       => gtrefclk0_in,
        --            gtyrxn_in(0)                       => SFP0_RX_N,
        --            gtyrxp_in(0)                       => SFP0_RX_P,
        --            gtpowergood_out                    => gtpowergood_out,
        --            gtytxn_out(0)                      => SFP0_TX_N,
        --            gtytxp_out(0)                      => SFP0_TX_P,
        --            rxpmaresetdone_out                 => rxpmaresetdone_out,
        --            txpmaresetdone_out                 => txpmaresetdone_out
        --        );

        --    inst_clk_wiz_200 : clk_wiz_200
        --        port map(
        --            -- Clock out ports  
        --            clk_200   => clk_200,
        --            ldpc_clk  => ldpc_clk,
        --            mgtrefclk => mgtrefclk,     --not used
        --            -- Status and control signals                
        --            reset     => '0',           --RESET,         --reset,
        --            -- Clock in ports
        --            clk_in1_p => USER_SI570_C0_P,
        --            clk_in1_n => USER_SI570_C0_N,
        --            locked    => locked
        --        );
        --
        --    IBUFDS_GTE4_inst : IBUFDS_GTE4
        --        generic map(
        --            REFCLK_EN_TX_PATH  => '0',  -- Refer to Transceiver User Guide
        --            REFCLK_HROW_CK_SEL => "00", -- Refer to Transceiver User Guide
        --            REFCLK_ICNTL_RX    => "00"  -- Refer to Transceiver User Guide
        --        )
        --        port map(
        --            O     => mgtrefclk0_x0y1,   -- 1-bit output: Refer to Transceiver User Guide
        --            ODIV2 => open,              -- 1-bit output: Refer to Transceiver User Guide
        --            CEB   => '0',               -- 1-bit input: Refer to Transceiver User Guide
        --            I     => MGTREFCLK0P_128,   -- 1-bit input: Refer to Transceiver User Guide
        --            IB    => MGTREFCLK0N_128    -- 1-bit input: Refer to Transceiver User Guide
        --        );

        --    inst_rx_fifo_wave_a : dc_fifo_phy
        --        port map(
        --            m_aclk        => clk_200,   -- 200 MHz
        --            s_aclk        => gtwiz_userclk_rx_usrclk2_out, -- 156.25/2 MHz
        --            s_aresetn     => rst_async_n, -- rx_reset_out, --rst_async_n,
        --            s_axis_tdata  => gtwiz_userdata_rx_out,
        --            s_axis_tvalid => '1',
        --            s_axis_tready => open,
        --            m_axis_tdata  => rxfifo_tdata,
        --            m_axis_tvalid => rxfifo_tvalid,
        --            m_axis_tready => rxfifo_tready
        --        );
        --
        --    inst_tx_fifo_wave_a : dc_fifo_phy
        --        port map(
        --            m_aclk        => gtwiz_userclk_tx_usrclk2_out,
        --            s_aclk        => clk_200,
        --            s_aresetn     => rst_async_n,
        --            s_axis_tdata  => fso_tx_fifo_tdata,
        --            s_axis_tvalid => fso_tx_fifo_tvalid,
        --            s_axis_tready => fso_tx_fifo_tready,
        --            m_axis_tdata  => gtwiz_userdata_tx_in,
        --            m_axis_tvalid => open,
        --            m_axis_tready => '1'
        --        );
        --
        --    inst_16_to_8_rx : dwidth_16_to_8
        --        port map(
        --            aclk          => clk_200,
        --            aresetn       => rst_async_n,
        --            s_axis_tdata  => rxfifo_tdata,
        --            s_axis_tvalid => rxfifo_tvalid,
        --            s_axis_tready => rxfifo_tready,
        --            m_axis_tdata  => ch_rx_axis_tdata, -- to FEC decoder
        --            m_axis_tvalid => ch_rx_axis_tvalid,
        --            m_axis_tready => '1'
        --        );
        --
        --    inst_8_to_16_tx : dwidth_8_to_16    -- for both waves a and b
        --        port map(
        --            aclk          => clk_200,
        --            aresetn       => rst_async_n,
        --            s_axis_tdata  => ch_tx_axis_tdata,
        --            s_axis_tvalid => ch_tx_axis_tvalid,
        --            s_axis_tready => ch_tx_axis_tready,
        --            m_axis_tdata  => fso_tx_fifo_tdata,
        --            m_axis_tvalid => fso_tx_fifo_tvalid,
        --            m_axis_tready => fso_tx_fifo_tready
        --        );

        -- FIFOS SDA <-> ETH

        -- IO BUFGs 

        --    BUFGCE_DIV_25_inst : BUFGCE_DIV
        --        generic map(
        --            BUFGCE_DIVIDE   => 8,       -- 1-8
        --            IS_CE_INVERTED  => '0',     -- Optional inversion for CE
        --            IS_CLR_INVERTED => '0',     -- Optional inversion for CLR
        --            IS_I_INVERTED   => '0'      -- Optional inversion for I
        --        )
        --        port map(
        --            O   => gtwiz_reset_clk_freerun_in, -- 1-bit output: Buffer
        --            CE  => '1',                 -- 1-bit input: Buffer enable
        --            CLR => '0',                 -- 1-bit input: Asynchronous clear
        --            I   => clk_200              -- 1-bit input: Buffer
        --        );

        --    BUFGCE_inst : BUFGCE
        --        generic map(
        --            IS_CE_INVERTED  => '0',     -- Optional inversion for CE
        --            IS_I_INVERTED   => '0'      -- Optional inversion for I
        --        )
        --        port map(
        --            O   => clk_100, -- 1-bit output: Buffer
        --            CE  => '1',                 -- 1-bit input: Buffer enable
        --            I   => clk_100_i --clk_100_i              -- 1-bit input: Buffer
        --        );

        --    IBUFDS_inst: IBUFDS
        --        generic map (
        --            DIFF_TERM       => TRUE, -- Differential Termination
        --            IBUF_LOW_PWR    => FALSE, -- Low power (TRUE) vs. performance (FALSE) setting for referenced I / O standards
        --            IOSTANDARD      => "DIFF_SSTL18_I")
        --        port map (
        --            O => clk_100_i, -- Buffer output
        --            I => CLK_100_P, -- Diff_p buffer input (connect directly to top-level port)
        --            IB => CLK_100_N -- Diff_n input buffer (connect directly to top-level port)
        --    );

        --    inst_proc_sys_reset_0 : proc_sys_reset_0
        --        PORT MAP(
        --            slowest_sync_clk     => mgtrefclk, --gtwiz_reset_clk_freerun_in, --clk_200, --clk_100,
        --            ext_reset_in         => RESET,
        --            aux_reset_in         => '0',
        --            mb_debug_sys_rst     => '0',
        --            dcm_locked           => locked,
        --            mb_reset             => open,
        --            bus_struct_reset     => bus_struct_reset,
        --            peripheral_reset     => peripheral_reset,
        --            interconnect_aresetn => interconnect_aresetn,
        --            peripheral_aresetn   => peripheral_aresetn
        --        );

        --    inst_vio_fec : vio_fec
        --        PORT MAP(
        --            clk          => clk_200,
        --            probe_in0(0) => preamble_synced,
        --            probe_in1(0) => link_status_out,
        --            probe_in2    => total_frame_counter,
        --            probe_in3    => crc_error_counter,
        --            probe_in4    => crc16_err_counter,
        --            probe_in5    => crc32_err_counter,
        --            probe_in6    => new_search_counter,
        --            probe_in7    => sync_loss_counter,
        --            probe_in8    => (others => '0'),
        --            probe_in9    => (others => '0'),
        --            probe_in10   => (others => '0'),
        --            probe_out0   => cfg_acquisition_length,
        --            probe_out1   => cfg_tracking_length,
        --            probe_out2   => cfg_max_tracking_error,
        --            probe_out3(0)   => sfp0_tx_disable_n,
        --            probe_out4   => loopback_vio,
        --            probe_out5   => fec_ctrl
        --            --            probe_out0 => cfg_acquisition_length,
        --            --            probe_out1 => cfg_tracking_length,
        --            --            probe_out2 => cfg_max_tracking_error
        --        );
        --
        --
        --        vio_sda_inst : vio_sda
        --        PORT MAP (
        --          clk            => clk_200,
        --          probe_in0      => framer_total_packet_counter,
        --          probe_in1      => framer_total_packet_splitted_counter,
        --          probe_in2      => framer_total_data_frame_counter,
        --          probe_in3      => framer_total_idle_frame_counter,
        --          probe_in4      => deframer_total_payload_counter,
        --          probe_in5      => deframer_total_packet_counter,
        --          probe_in6      => deframer_total_packet_splitted_counter,
        --          probe_in7      => deframer_total_packet_merged_counter,
        --          probe_in8      => deframer_total_data_frame_counter,
        --          probe_in9      => deframer_total_idle_frame_counter,
        --          probe_in10     => deframer_total_payload_error_counter,
        --          probe_in11     => deframer_total_packet_error_counter,
        --          probe_in12     => (others =>'0'), --sda_debug.payload_header,
        --          probe_in13     => (others =>'0'), --sda_debug.packet_header,
        --          probe_in14     => (others =>'0'), --sda_debug.frame_header.txfn,
        --          probe_in15     => (others =>'0'), --sda_debug.frame_header.rxfn,
        --          probe_in16     => (others =>'0'), --sda_debug.frame_header.ack,
        --          probe_in17     => (others =>'0'), --sda_debug.frame_header.ack_valid,
        --          probe_in18     => (others =>'0'), --sda_debug.frame_header.frame_type,
        --          probe_in19     => (others =>'0'), --sda_debug.frame_header.tx_ts,
        --          probe_in20     => (others =>'0'), --sda_debug.frame_header.tod_seconds,
        --          probe_in21     => (others =>'0'), --sda_debug.frame_header.ts_applies,
        --          probe_in22     => (others =>'0'), --sda_debug.frame_header.fcch_opcode,
        --          probe_in23     => (others =>'0'), --sda_debug.frame_header.fcch_pl,
        --          probe_in24     => (others =>'0'),
        --          probe_in25     => framer_frame_length_error,
        --          probe_in26     => (others =>'0'), --framer_health_monitor_statistics.not_ready_clks,
        --          probe_in27     => (others =>'0'), --framer_health_monitor_statistics.wrong_idle_condition,
        --          probe_in28(0)  => framer_warning_flag,
        --          probe_in29(0)  => deframer_warning_flag,
        --          probe_in30     => framer_actual_data_frame_counter,
        --          probe_in31     => framer_frame_length_error,
        --          probe_in32(0)  => framer_critical_error_flag,
        --          probe_in33(0)  => deframer_critical_error_flag,
        --          probe_in34     => (others =>'0'), --deframer_health_monitor_statistics.frame_length_error,
        --          probe_in35     => (others =>'0'), --deframer_health_monitor_statistics.not_ready_clks,
        --          probe_in36     => (others =>'0'), --deframer_health_monitor_statistics.wrong_idle_condition,
        --          probe_out0(0)  => clear_stat,
        --          probe_out1     => open,
        --          probe_out2     => open
        --        );

end architecture RTL;
