library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity let_sda_hard_top_tb is
end entity let_sda_hard_top_tb;

architecture RTL of let_sda_hard_top_tb is

    component let_sda_hard_top
        port(
        USER_SI570_C0_P        : in  std_logic; -- Bank  84 VCCO - VCC1V8
        USER_SI570_C0_N        : in  std_logic; -- Bank  84 VCCO - VCC1V8
        USER_SI570_C1_P        : in  std_logic; -- Bank  84 VCCO - VCC1V8
        USER_SI570_C1_N        : in  std_logic; -- Bank  84 VCCO - VCC1V8
        Q7_OUT_C_P             : in  std_logic;
        Q7_OUT_C_N             : in  std_logic;        
        Q11_OUT_C_P             : in  std_logic;
        Q11_OUT_C_N             : in  std_logic;
--        MGTREFCLK0P_128             : in  std_logic;
--        MGTREFCLK0N_128             : in  std_logic;
        RESET                  : in  std_logic;
        SFP0_RX_N              : in  std_logic; -- Bank 128 - MGTYRXP0_128    
        SFP0_RX_P              : in  std_logic; -- Bank 128 - MGTYRXP0_128
        SFP0_TX_N              : out std_logic; -- Bank 128 - MGTYRXP0_128    
        SFP0_TX_P              : out std_logic; -- Bank 128 - MGTYRXP0_128

        ETH0_RX_N              : in  std_logic; --    
        ETH0_RX_P              : in  std_logic; -- 
        ETH0_TX_N              : out std_logic; --     
        ETH0_TX_P              : out std_logic; -- 

        SFP0_TX_DISABLE_B      : out std_logic;
        SFP1_TX_DISABLE_B      : out std_logic;
        SFP2_TX_DISABLE_B      : out std_logic;
        SFP3_TX_DISABLE_B      : out std_logic
        );
    end component let_sda_hard_top;

    signal MGTREFCLK0P_128   : std_logic := '0'; -- Bank 128 - MGTREFCLK0P_128
    signal MGTREFCLK0N_128   : std_logic := '1'; -- Bank 128 - MGTREFCLK0P_128
    signal CLK_100_P   : std_logic := '0'; -- Bank 128 - MGTREFCLK0P_128
    signal CLK_100_N   : std_logic := '1'; -- Bank 128 - MGTREFCLK0P_128
    signal USER_SI570_C0_N         : std_logic := '1'; -- Bank  84 VCCO - VCC1V8
    signal USER_SI570_C0_P         : std_logic := '0'; -- Bank  84 VCCO - VCC1V8
    signal USER_SI570_C1_N         : std_logic := '1'; -- Bank  84 VCCO - VCC1V8
    signal USER_SI570_C1_P         : std_logic := '0'; -- Bank  84 VCCO - VCC1V8
    signal SFP0_RX_N         : std_logic; -- Bank 128 - MGTYRXP0_128    
    signal SFP0_RX_P         : std_logic; -- Bank 128 - MGTYRXP0_128
    signal SFP0_TX_N         : std_logic; -- Bank 128 - MGTYRXP0_128    
    signal SFP0_TX_P         : std_logic; -- Bank 128 - MGTYRXP0_128
    signal SFP0_TX_DISABLE_B : std_logic;
    signal RESET             : std_logic := '1';
    signal Q7_OUT_C_P : std_logic:= '0';
    signal Q7_OUT_C_N : std_logic:= '0';
    signal Q11_OUT_C_P : std_logic:= '0';
    signal Q11_OUT_C_N : std_logic:= '0';
    signal ETH0_RX_N : std_logic;
    signal ETH0_RX_P : std_logic;
    signal ETH0_TX_N : std_logic;
    signal ETH0_TX_P : std_logic;
    signal SFP1_TX_DISABLE_B : std_logic;
    signal SFP2_TX_DISABLE_B : std_logic;
    signal SFP3_TX_DISABLE_B : std_logic;

alias fec_cfg is <<signal .let_sda_hard_top_tb.uut.inst_top_let_0.fec_ctrl : std_logic_vector(2 downto 0) >>;

begin

    -------------------------------------------
    -- clock generation
    -------------------------------------------
    Q7_OUT_C_P <= not Q7_OUT_C_P after 3.200 ns;
    Q11_OUT_C_P <= not Q11_OUT_C_P after 3.200 ns;
    USER_SI570_C0_P <= not USER_SI570_C0_P after 1.499 ns;--333.5M  --3.076 ns; --1.66 ns;
    USER_SI570_C1_P <= not USER_SI570_C1_P after 1.666 ns;--300M    --3.076 ns; --1.66 ns;

    Q11_OUT_C_N <= not Q11_OUT_C_P;
    Q7_OUT_C_N <= not Q7_OUT_C_P;
    USER_SI570_C0_N <= not USER_SI570_C0_P;
    USER_SI570_C1_N <= not USER_SI570_C1_P;
    
    SFP0_RX_P <= SFP0_TX_P;
    SFP0_RX_N <= SFP0_TX_N;
    
    ETH0_RX_P <= ETH0_TX_P;
    ETH0_RX_N <= ETH0_TX_N;

    uut : let_sda_hard_top
        port map(
            USER_SI570_C0_P => USER_SI570_C0_P,
            USER_SI570_C0_N => USER_SI570_C0_N,            
            USER_SI570_C1_P => USER_SI570_C1_P,
            USER_SI570_C1_N => USER_SI570_C1_N,
            Q7_OUT_C_P => Q7_OUT_C_P,
            Q7_OUT_C_N => Q7_OUT_C_N,
            Q11_OUT_C_P => Q11_OUT_C_P,
            Q11_OUT_C_N => Q11_OUT_C_N,
--            MGTREFCLK0P_128 => Q11_OUT_C_P,
--            MGTREFCLK0N_128 => Q11_OUT_C_N,
            RESET => RESET,
            SFP0_RX_N => SFP0_RX_N,
            SFP0_RX_P => SFP0_RX_P,
            SFP0_TX_N => SFP0_TX_N,
            SFP0_TX_P => SFP0_TX_P,
            ETH0_RX_N => ETH0_RX_N,
            ETH0_RX_P => ETH0_RX_P,
            ETH0_TX_N => ETH0_TX_N,
            ETH0_TX_P => ETH0_TX_P,
            SFP0_TX_DISABLE_B => SFP0_TX_DISABLE_B,
            SFP1_TX_DISABLE_B => SFP1_TX_DISABLE_B,
            SFP2_TX_DISABLE_B => SFP2_TX_DISABLE_B,
            SFP3_TX_DISABLE_B => SFP3_TX_DISABLE_B
        );

    process
    begin
        --fec_cfg <= "011";
        RESET <= '1';
        wait for 700 ns;
        RESET <= '0';
        --fec_cfg <= "001";
        wait for 700 ns;
        --fec_cfg <= "000";
        wait for 700 us;
        --fec_cfg <= "001";
        wait for 200 us;
        --fec_cfg <= "010";
        wait;

    end process;

end architecture RTL;
