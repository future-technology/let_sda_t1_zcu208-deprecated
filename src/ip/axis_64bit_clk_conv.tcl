##################################################################
# CHECK VIVADO VERSION
##################################################################

set scripts_vivado_version 2022.1
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
  catch {common::send_msg_id "IPS_TCL-100" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_ip_tcl to create an updated script."}
  return 1
}

##################################################################
# START
##################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source axis_64bit_clk_conv.tcl
# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./prj/let_sda_zcu208.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
  create_project let_sda_zcu208 prj -part xczu48dr-fsvg1517-2-e
  set_property BOARD_PART xilinx.com:zcu208:part0:2.0 [current_project]
  set_property target_language VHDL [current_project]
  set_property simulator_language Mixed [current_project]
}

##################################################################
# CHECK IPs
##################################################################

set bCheckIPs 1
set bCheckIPsPassed 1
if { $bCheckIPs == 1 } {
  set list_check_ips { xilinx.com:ip:axis_clock_converter:1.1 }
  set list_ips_missing ""
  common::send_msg_id "IPS_TCL-1001" "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

  foreach ip_vlnv $list_check_ips {
  set ip_obj [get_ipdefs -all $ip_vlnv]
  if { $ip_obj eq "" } {
    lappend list_ips_missing $ip_vlnv
    }
  }

  if { $list_ips_missing ne "" } {
    catch {common::send_msg_id "IPS_TCL-105" "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
    set bCheckIPsPassed 0
  }
}

if { $bCheckIPsPassed != 1 } {
  common::send_msg_id "IPS_TCL-102" "WARNING" "Will not continue with creation of design due to the error(s) above."
  return 1
}

##################################################################
# CREATE IP axis_64bit_clk_conv
##################################################################

set axis_64bit_clk_conv [create_ip -name axis_clock_converter -vendor xilinx.com -library ip -version 1.1 -module_name axis_64bit_clk_conv]

set_property -dict { 
  CONFIG.TDATA_NUM_BYTES {8}
  CONFIG.SYNCHRONIZATION_STAGES {3}
  CONFIG.S_CLKIF.FREQ_HZ {100000000}
  CONFIG.M_CLKIF.FREQ_HZ {100000000}
} [get_ips axis_64bit_clk_conv]

set_property -dict { 
  GENERATE_SYNTH_CHECKPOINT {1}
} $axis_64bit_clk_conv

##################################################################

