##################################################################
# CHECK VIVADO VERSION
##################################################################

set scripts_vivado_version 2022.1
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
  catch {common::send_msg_id "IPS_TCL-100" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_ip_tcl to create an updated script."}
  return 1
}

##################################################################
# START
##################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source clk_wiz_0.tcl
# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./prj/let_sda_zcu208.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
  create_project let_sda_zcu208 prj -part xczu48dr-fsvg1517-2-e
  set_property BOARD_PART xilinx.com:zcu208:part0:2.0 [current_project]
  set_property target_language VHDL [current_project]
  set_property simulator_language Mixed [current_project]
}

##################################################################
# CHECK IPs
##################################################################

set bCheckIPs 1
set bCheckIPsPassed 1
if { $bCheckIPs == 1 } {
  set list_check_ips { xilinx.com:ip:clk_wiz:6.0 }
  set list_ips_missing ""
  common::send_msg_id "IPS_TCL-1001" "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

  foreach ip_vlnv $list_check_ips {
  set ip_obj [get_ipdefs -all $ip_vlnv]
  if { $ip_obj eq "" } {
    lappend list_ips_missing $ip_vlnv
    }
  }

  if { $list_ips_missing ne "" } {
    catch {common::send_msg_id "IPS_TCL-105" "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
    set bCheckIPsPassed 0
  }
}

if { $bCheckIPsPassed != 1 } {
  common::send_msg_id "IPS_TCL-102" "WARNING" "Will not continue with creation of design due to the error(s) above."
  return 1
}

##################################################################
# CREATE IP clk_wiz_0
##################################################################

set clk_wiz_0 [create_ip -name clk_wiz -vendor xilinx.com -library ip -version 6.0 -module_name clk_wiz_0]

set_property -dict { 
  CONFIG.OPTIMIZE_CLOCKING_STRUCTURE_EN {true}
  CONFIG.PRIMITIVE {Auto}
  CONFIG.PRIM_IN_FREQ {300}
  CONFIG.CLKIN1_JITTER_PS {33.330000000000005}
  CONFIG.CLKOUT2_USED {true}
  CONFIG.CLKOUT3_USED {true}
  CONFIG.CLKOUT4_USED {true}
  CONFIG.NUM_OUT_CLKS {4}
  CONFIG.CLK_OUT1_PORT {clk_sys}
  CONFIG.CLK_OUT2_PORT {clk_gt_free}
  CONFIG.CLK_OUT3_PORT {clk_eth_free}
  CONFIG.CLK_OUT4_PORT {clk_let}
  CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {25}
  CONFIG.CLKOUT3_REQUESTED_OUT_FREQ {50}
  CONFIG.CLKOUT4_REQUESTED_OUT_FREQ {325}
  CONFIG.PRIM_SOURCE {Differential_clock_capable_pin}
  CONFIG.CLKOUT1_DRIVES {Buffer}
  CONFIG.CLKOUT2_DRIVES {Buffer}
  CONFIG.CLKOUT3_DRIVES {Buffer}
  CONFIG.CLKOUT4_DRIVES {Buffer}
  CONFIG.CLKOUT5_DRIVES {Buffer}
  CONFIG.CLKOUT6_DRIVES {Buffer}
  CONFIG.CLKOUT7_DRIVES {Buffer}
  CONFIG.FEEDBACK_SOURCE {FDBK_AUTO}
  CONFIG.USE_LOCKED {true}
  CONFIG.USE_RESET {true}
  CONFIG.MMCM_DIVCLK_DIVIDE {3}
  CONFIG.MMCM_BANDWIDTH {OPTIMIZED}
  CONFIG.MMCM_CLKFBOUT_MULT_F {13}
  CONFIG.MMCM_CLKIN1_PERIOD {3.333}
  CONFIG.MMCM_CLKIN2_PERIOD {10.0}
  CONFIG.MMCM_COMPENSATION {AUTO}
  CONFIG.MMCM_CLKOUT0_DIVIDE_F {13}
  CONFIG.MMCM_CLKOUT1_DIVIDE {52}
  CONFIG.MMCM_CLKOUT2_DIVIDE {26}
  CONFIG.MMCM_CLKOUT3_DIVIDE {4}
  CONFIG.CLK_IN1_BOARD_INTERFACE {Custom}
  CONFIG.AUTO_PRIMITIVE {PLL}
  CONFIG.CLKOUT1_JITTER {100.297}
  CONFIG.CLKOUT1_PHASE_ERROR {80.292}
  CONFIG.CLKOUT2_JITTER {132.502}
  CONFIG.CLKOUT2_PHASE_ERROR {80.292}
  CONFIG.CLKOUT3_JITTER {115.201}
  CONFIG.CLKOUT3_PHASE_ERROR {80.292}
  CONFIG.CLKOUT4_JITTER {79.111}
  CONFIG.CLKOUT4_PHASE_ERROR {80.292}
} [get_ips clk_wiz_0]

set_property -dict { 
  GENERATE_SYNTH_CHECKPOINT {1}
} $clk_wiz_0

##################################################################

