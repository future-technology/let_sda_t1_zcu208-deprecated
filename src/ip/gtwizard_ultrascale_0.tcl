##################################################################
# CHECK VIVADO VERSION
##################################################################

set scripts_vivado_version 2022.1
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
  catch {common::send_msg_id "IPS_TCL-100" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_ip_tcl to create an updated script."}
  return 1
}

##################################################################
# START
##################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source gtwizard_ultrascale_0.tcl
# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./prj/let_sda_zcu208.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
  create_project let_sda_zcu208 prj -part xczu48dr-fsvg1517-2-e
  set_property BOARD_PART xilinx.com:zcu208:part0:2.0 [current_project]
  set_property target_language VHDL [current_project]
  set_property simulator_language Mixed [current_project]
}

##################################################################
# CHECK IPs
##################################################################

set bCheckIPs 1
set bCheckIPsPassed 1
if { $bCheckIPs == 1 } {
  set list_check_ips { xilinx.com:ip:gtwizard_ultrascale:1.7 }
  set list_ips_missing ""
  common::send_msg_id "IPS_TCL-1001" "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

  foreach ip_vlnv $list_check_ips {
  set ip_obj [get_ipdefs -all $ip_vlnv]
  if { $ip_obj eq "" } {
    lappend list_ips_missing $ip_vlnv
    }
  }

  if { $list_ips_missing ne "" } {
    catch {common::send_msg_id "IPS_TCL-105" "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
    set bCheckIPsPassed 0
  }
}

if { $bCheckIPsPassed != 1 } {
  common::send_msg_id "IPS_TCL-102" "WARNING" "Will not continue with creation of design due to the error(s) above."
  return 1
}

##################################################################
# CREATE IP gtwizard_ultrascale_0
##################################################################

set gtwizard_ultrascale_0 [create_ip -name gtwizard_ultrascale -vendor xilinx.com -library ip -version 1.7 -module_name gtwizard_ultrascale_0]

set_property -dict { 
  CONFIG.CHANNEL_ENABLE {X0Y4}
  CONFIG.TX_MASTER_CHANNEL {X0Y4}
  CONFIG.RX_MASTER_CHANNEL {X0Y4}
  CONFIG.TX_LINE_RATE {2.50}
  CONFIG.TX_PLL_TYPE {CPLL}
  CONFIG.TX_REFCLK_FREQUENCY {156.25}
  CONFIG.TX_DATA_ENCODING {RAW}
  CONFIG.TX_USER_DATA_WIDTH {32}
  CONFIG.TX_INT_DATA_WIDTH {32}
  CONFIG.TX_BUFFER_MODE {0}
  CONFIG.TX_OUTCLK_SOURCE {TXPLLREFCLK_DIV1}
  CONFIG.RX_LINE_RATE {2.50}
  CONFIG.RX_PLL_TYPE {CPLL}
  CONFIG.RX_REFCLK_FREQUENCY {156.25}
  CONFIG.RX_DATA_DECODING {RAW}
  CONFIG.RX_USER_DATA_WIDTH {32}
  CONFIG.RX_INT_DATA_WIDTH {32}
  CONFIG.RX_BUFFER_MODE {0}
  CONFIG.RX_JTOL_FC {1.4997001}
  CONFIG.RX_OUTCLK_SOURCE {RXOUTCLKPMA}
  CONFIG.RX_CB_MAX_LEVEL {1}
  CONFIG.ENABLE_OPTIONAL_PORTS {cpllreset_in drpclk_in loopback_in rxpolarity_in txdiffctrl_in txpostcursor_in txprecursor_in cplllock_out rxcdrlock_out}
  CONFIG.RX_REFCLK_SOURCE {}
  CONFIG.TX_REFCLK_SOURCE {}
  CONFIG.RX_RECCLK_OUTPUT {}
  CONFIG.LOCATE_TX_USER_CLOCKING {CORE}
  CONFIG.LOCATE_RX_USER_CLOCKING {CORE}
  CONFIG.TXPROGDIV_FREQ_SOURCE {CPLL}
  CONFIG.TXPROGDIV_FREQ_VAL {78.125}
  CONFIG.FREERUN_FREQUENCY {25}
} [get_ips gtwizard_ultrascale_0]

set_property -dict { 
  GENERATE_SYNTH_CHECKPOINT {1}
} $gtwizard_ultrascale_0

##################################################################

