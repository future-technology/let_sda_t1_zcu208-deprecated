##################################################################
# CHECK VIVADO VERSION
##################################################################

set scripts_vivado_version 2022.1
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
  catch {common::send_msg_id "IPS_TCL-100" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_ip_tcl to create an updated script."}
  return 1
}

##################################################################
# START
##################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source ldpc_dec.tcl
# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./prj/let_sda_hard.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
  create_project let_sda_hard prj -part xczu48dr-fsvg1517-2-e
  set_property BOARD_PART xilinx.com:zcu208:part0:2.0 [current_project]
  set_property target_language VHDL [current_project]
  set_property simulator_language Mixed [current_project]
}

##################################################################
# CHECK IPs
##################################################################

set bCheckIPs 1
set bCheckIPsPassed 1
if { $bCheckIPs == 1 } {
  set list_check_ips { xilinx.com:ip:sd_fec:1.1 }
  set list_ips_missing ""
  common::send_msg_id "IPS_TCL-1001" "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

  foreach ip_vlnv $list_check_ips {
  set ip_obj [get_ipdefs -all $ip_vlnv]
  if { $ip_obj eq "" } {
    lappend list_ips_missing $ip_vlnv
    }
  }

  if { $list_ips_missing ne "" } {
    catch {common::send_msg_id "IPS_TCL-105" "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
    set bCheckIPsPassed 0
  }
}

if { $bCheckIPsPassed != 1 } {
  common::send_msg_id "IPS_TCL-102" "WARNING" "Will not continue with creation of design due to the error(s) above."
  return 1
}

##################################################################
# CREATE IP ldpc_dec
##################################################################

set ldpc_dec [create_ip -name sd_fec -vendor xilinx.com -library ip -version 1.1 -module_name ldpc_dec]

set_property -dict { 
  CONFIG.Standard {5G}
  CONFIG.Turbo_Decode {false}
  CONFIG.LDPC_Decode {true}
  CONFIG.Enable_Wgt1 {true}
  CONFIG.Parameter_Interface {Initialized}
  CONFIG.Enable_IFs {true}
  CONFIG.Interrupts {true}
  CONFIG.DIN_Words {8}
  CONFIG.DOUT_Words {1}
  CONFIG.TD_PERCENT_LOAD {0}
  CONFIG.LD_PERCENT_LOAD {100}
  CONFIG.DRV_STANDARD {1}
  CONFIG.DRV_INITIALIZATION_PARAMS {{ 0x00000014,0x00000001,0x0000000C,0x00000000 }}
  CONFIG.DRV_TURBO_PARAMS {undefined}
  CONFIG.HDL_INITIALIZATION {{20 1 FEC_CODE} {12 0 AXIS_WIDTH} {32 63 {IER: Enable all interupts}} {16 63 {AXIS_ENABLE: Enable all channels }}}
} [get_ips ldpc_dec]

set_property -dict { 
  GENERATE_SYNTH_CHECKPOINT {1}
} $ldpc_dec

##################################################################

