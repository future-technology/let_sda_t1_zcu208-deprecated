-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: tb_arq_internal_ram_controller
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

library osvvm;
use osvvm.RandomPkg.all;
use osvvm.CoveragePkg.all;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;
library arq_lib;

library sda_test_lib;
use sda_test_lib.sda_test_pkg.all;
library sda_test_lib;
use sda_test_lib.eth_packet_test_pkg.all;
library sda_test_lib;
use sda_test_lib.packet_test_pkg.all;
library sda_test_lib;
use sda_test_lib.payload_test_pkg.all;

--library sda_test_lib;
--use sda_test_lib.ethernet_test_receiver;

entity tb_arq_internal_ram_controller is
    generic(runner_cfg : string := runner_cfg_default);
end entity;

architecture tb of tb_arq_internal_ram_controller is

    constant C_CLK_PERIOD      : time      := 10 ns;
    signal clk                 : std_logic := '0';
    signal reset               : std_logic := '1';
    signal error               : std_logic;
    signal send                : std_logic := '0';
    signal payload_length      : natural;
    signal seqnum              : natural;
    signal packet_len          : natural   := 0;
    signal axis_payload_if_in_m  : t_axis_payload_if_m;
    signal axis_payload_if_in_s  : t_axis_payload_if_s;
    signal axis_payload_if_out_m : t_axis_payload_if_m;
    signal axis_payload_if_out_s : t_axis_payload_if_s;
    signal payload             : payload_array_t;
    signal payload_valid       : std_logic;
    signal tx_fn               : std_logic_vector(16 - 1 downto 0);
    signal frame_type          : std_logic_vector(2 - 1 downto 0);
    signal tx_fn_out           : std_logic_vector(16 - 1 downto 0);
    signal frame_type_out      : std_logic_vector(2 - 1 downto 0);

    signal start_stimuli        : std_logic := '0';
    signal random_stimuli_wait  : std_logic := '0';
    signal stimuli_length       : natural;
    type t_stimuli_length_array is array (0 to 500 - 1) of natural;
    signal stimuli_length_array : t_stimuli_length_array;

    signal eth_packet_length : natural;
    signal eth_packet_id     : natural;
    --eth_packet        :    eth_packet_array_t;
    signal eth_packet_valid  : std_logic;
    signal eth_packet_error  : std_logic;
    signal axis_ethernet_if_m  : t_axis_if_m;
    signal axis_ethernet_if_s  : t_axis_if_s;

    signal eth_vector_id              : eth_packet_id_array_t;
    signal eth_vector_length          : eth_packet_length_array_t;
    signal tx_fn_error_injection      : std_logic;
    signal frame_type_error_injection : std_logic;
    signal seqnum_error_injection     : std_logic;
    signal is_idle                    : std_logic;
    signal pt_finished                : std_logic;
    signal back_pressure              : std_logic := '0';
    signal eth_packet_good_cnt        : natural   := 0;

    --DUT signals
    --Input interfaces
    signal ram_cmd_if_rd_m  : t_ram_cmd_if_m;
    signal ram_cmd_if_rd_s  : t_ram_cmd_if_s;
    signal ram_cmd_if_wr_m  : t_ram_cmd_if_m;
    signal ram_cmd_if_wr_s  : t_ram_cmd_if_s;
    signal axis_in_m        : t_axis_if_m;
    signal axis_in_s        : t_axis_if_s;
    --Output interfaces
    signal axis_out_m       : t_axis_if_m;
    signal axis_out_s       : t_axis_if_s;
    signal internal_error : std_logic;
begin

    --------------------------------------------------------------------------
    -- CLOCK AND RESET.
    --------------------------------------------------------------------------
    clk   <= NOT clk after C_CLK_PERIOD / 2;
    reset <= '0' after 5 * (C_CLK_PERIOD);

    test_runner : process
        variable v_tx_fn_ref              : natural := 0;
        variable v_tx_fn                  : natural := 0;
        variable v_packets_read           : natural := 0;
        variable v_error                  : std_logic;
        variable payload_id_last          : natural := 0;
        variable eth_vector_pointer       : natural;
        variable v_eth_vector_pointer_aux : natural := 0;
        variable pk_size                  : natural := 64;
        variable pk_offset                : natural := 0;
        variable v_packet                 : packet_array_t;
        variable split                    : std_logic;
        variable v_payload                : payload_array_t;
        variable data_flag                : std_logic;
        variable wait_for_idle            : std_logic;
        variable v_eth_packet_id          : eth_packet_id_array_t;
        variable v_eth_packet_length      : eth_packet_length_array_t;

    begin
        test_runner_setup(runner, runner_cfg);
        -- enable logging for passing check
        --show(get_logger(default_checker), display_handler, pass); 
        --show(get_logger(RED_CHECKER), display_handler, pass); 
        --packet_len <= 100;
        test_cases_loop : WHILE test_suite LOOP

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- check that packet header is working with all alignment in the payload
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            IF run("one_frame_push_pull") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: one_frame_push_pull");
                info("--------------------------------------------------------------------------------");
                start_stimuli              <= '0';
                stimuli_length             <= 2;
                seqnum_error_injection     <= '0';
                back_pressure              <= '1';
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';

                ram_cmd_if_rd_m.request   <= '0'; --: std_logic;
                --ram_cmd_if_rd_s.granted   <= 'Z'; --: std_logic;
                ram_cmd_if_rd_m.cmd_valid <= '0'; --: std_logic;
                ram_cmd_if_rd_m.addr      <= (others => '0'); --: std_logic_vector(C_ADDRESS_WIDTH-1 downto 0);
                --ram_cmd_if_rd_s.busy      <= 'Z'; --: std_logic;  

                ram_cmd_if_wr_m.request   <= '0'; --: std_logic;
                --ram_cmd_if_wr_s.granted   <= 'Z'; --: std_logic;
                ram_cmd_if_wr_m.cmd_valid <= '0'; --: std_logic;
                ram_cmd_if_wr_m.addr      <= (others => '0'); --: std_logic_vector(C_ADDRESS_WIDTH-1 downto 0);
                --ram_cmd_if_wr_s.busy      <= 'Z'; --: std_logic;  

                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);

                for ii in 0 to eth_packet_length_array_t'length - 1 loop
                    eth_vector_id(ii) <= ii + 1;
                end loop;
                eth_vector_length(0) <= 519;
                eth_vector_length(1) <= 520;

                wait for 90 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                -- Insert RAM command for writting a frame in RAM:
                -- Wait until frame is loaded in fifo:
                wait until axis_in_m.tlast = '1' and axis_in_m.tvalid = '1';
                wait until rising_edge(clk);
                ram_cmd_if_wr_m.request   <= '1';
                -- wait until access is granted:
                wait until ram_cmd_if_wr_s.granted = '1';
                wait until rising_edge(clk);
                -- send write command:
                ram_cmd_if_wr_m.cmd_valid <= '1';
                ram_cmd_if_wr_m.addr      <= std_logic_vector(to_unsigned(0, C_ADDRESS_WIDTH));
                wait until rising_edge(clk);
                ram_cmd_if_wr_m.cmd_valid <= '0';
                ram_cmd_if_wr_m.addr      <= (others => '0');
                --wait until ram_cmd_if_wr_s.busy = '1';
                wait until rising_edge(clk);
                wait until rising_edge(clk);
                ram_cmd_if_wr_m.request   <= '0';

                --Wait until WR process is finished:
                wait until ram_cmd_if_wr_s.busy = '0';
                wait until rising_edge(clk);
                wait until rising_edge(clk);
                wait for 300 ns;
                wait until rising_edge(clk);

                ram_cmd_if_rd_m.request   <= '1';
                --wait until access is granted:
                wait until ram_cmd_if_rd_s.granted = '1';
                wait until rising_edge(clk);
                -- send write command:
                back_pressure           <= '0';
                ram_cmd_if_rd_m.cmd_valid <= '1';
                ram_cmd_if_rd_m.addr      <= std_logic_vector(to_unsigned(0, C_ADDRESS_WIDTH));
                wait until rising_edge(clk);
                ram_cmd_if_rd_m.cmd_valid <= '0';
                ram_cmd_if_rd_m.addr      <= (others => '0');
                --wait until ram_cmd_if_wr_s.busy = '1';
                wait until rising_edge(clk);
                wait until rising_edge(clk);
                ram_cmd_if_rd_m.request   <= '0';
                --wait for 5 us; 
                wait for 500 ns;
                --back_pressure <= '1';
                --Wait until RD process is finished:
                --wait until ram_cmd_if_rd_s.busy = '0';
                wait until rising_edge(clk);
                wait until rising_edge(clk);
                wait for 300 ns;
                wait until rising_edge(clk);

                --           wait for 200 us;

                wait until payload_valid = '1';
                v_payload := payload;

                -- Check payload data received:
                payload_test_read_payload_data(
                    v_payload,
                    pk_offset,
                    v_packet,
                    pk_size,
                    v_eth_packet_id,
                    v_eth_packet_length,
                    eth_vector_pointer,
                    split,
                    payload_id_last,
                    v_error);
                check(v_error = '0', "Payload health check failed.");

                for ii in 0 to stimuli_length - 1 loop
                    check(v_eth_packet_length(ii) = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                    check(v_eth_packet_id(ii) = eth_vector_id(ii), "Wrong Ethernet packet ID");
                    check_false(v_eth_packet_length(ii) < 64, "Ethernet packet length can not be < 64.");
                end loop;
                wait for 50 us;

                info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- check that packet header is working with all alignment in the payload
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("timeout_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: timeout_check");
                info("--------------------------------------------------------------------------------");
                start_stimuli              <= '0';
                stimuli_length             <= 2;
                seqnum_error_injection     <= '0';
                back_pressure              <= '1';
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';

                ram_cmd_if_rd_m.request   <= '0'; --: std_logic;
                --ram_cmd_if_rd_s.granted   <= 'Z'; --: std_logic;
                ram_cmd_if_rd_m.cmd_valid <= '0'; --: std_logic;
                ram_cmd_if_rd_m.addr      <= (others => '0'); --: std_logic_vector(C_ADDRESS_WIDTH-1 downto 0);
                --ram_cmd_if_rd_s.busy      <= 'Z'; --: std_logic;  

                ram_cmd_if_wr_m.request   <= '0'; --: std_logic;
                --ram_cmd_if_wr_s.granted   <= 'Z'; --: std_logic;
                ram_cmd_if_wr_m.cmd_valid <= '0'; --: std_logic;
                ram_cmd_if_wr_m.addr      <= (others => '0'); --: std_logic_vector(C_ADDRESS_WIDTH-1 downto 0);
                --ram_cmd_if_wr_s.busy      <= 'Z'; --: std_logic;  

                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);

                for ii in 0 to eth_packet_length_array_t'length - 1 loop
                    eth_vector_id(ii) <= ii + 1;
                end loop;
                eth_vector_length(0) <= 519;
                eth_vector_length(1) <= 520;

                wait for 90 ns;
                ram_cmd_if_wr_m.request <= '1';
                -- wait until access is granted:
                wait until ram_cmd_if_wr_s.granted = '1';
                wait until rising_edge(clk);
                -- No command response to force timeout:
                wait until rising_edge(clk);
                wait until rising_edge(clk);
                wait until rising_edge(clk);
                wait until rising_edge(clk);
                wait until rising_edge(clk);
                wait until rising_edge(clk);
                wait until rising_edge(clk);
                wait until rising_edge(clk);
                wait until rising_edge(clk);

                -- Try again. 
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                -- Insert RAM command for writting a frame in RAM:
                -- Wait until frame is loaded in fifo:
                wait until axis_in_m.tlast = '1' and axis_in_m.tvalid = '1';
                wait until rising_edge(clk);
                ram_cmd_if_wr_m.request   <= '1';
                -- wait until access is granted:
                wait until ram_cmd_if_wr_s.granted = '1';
                wait until rising_edge(clk);
                -- send write command:
                ram_cmd_if_wr_m.cmd_valid <= '1';
                ram_cmd_if_wr_m.addr      <= std_logic_vector(to_unsigned(0, C_ADDRESS_WIDTH));
                wait until rising_edge(clk);
                ram_cmd_if_wr_m.cmd_valid <= '0';
                ram_cmd_if_wr_m.addr      <= (others => '0');
                --wait until ram_cmd_if_wr_s.busy = '1';
                wait until rising_edge(clk);
                wait until rising_edge(clk);
                ram_cmd_if_wr_m.request   <= '0';

                --Wait until WR process is finished:
                wait until ram_cmd_if_wr_s.busy = '0';
                wait until rising_edge(clk);
                wait until rising_edge(clk);
                wait for 300 ns;
                wait until rising_edge(clk);

                ram_cmd_if_rd_m.request   <= '1';
                --wait until access is granted:
                wait until ram_cmd_if_rd_s.granted = '1';
                wait until rising_edge(clk);
                -- send write command:
                back_pressure           <= '0';
                ram_cmd_if_rd_m.cmd_valid <= '1';
                ram_cmd_if_rd_m.addr      <= std_logic_vector(to_unsigned(0, C_ADDRESS_WIDTH));
                wait until rising_edge(clk);
                ram_cmd_if_rd_m.cmd_valid <= '0';
                ram_cmd_if_rd_m.addr      <= (others => '0');
                --wait until ram_cmd_if_wr_s.busy = '1';
                wait until rising_edge(clk);
                wait until rising_edge(clk);
                ram_cmd_if_rd_m.request   <= '0';
                --wait for 5 us; 
                wait for 500 ns;
                --back_pressure <= '1';
                --Wait until RD process is finished:
                --wait until ram_cmd_if_rd_s.busy = '0';
                wait until rising_edge(clk);
                wait until rising_edge(clk);
                wait for 300 ns;
                wait until rising_edge(clk);

                --           wait for 200 us;

                wait until payload_valid = '1';
                v_payload := payload;

                -- Check payload data received:
                payload_test_read_payload_data(
                    v_payload,
                    pk_offset,
                    v_packet,
                    pk_size,
                    v_eth_packet_id,
                    v_eth_packet_length,
                    eth_vector_pointer,
                    split,
                    payload_id_last,
                    v_error);
                check(v_error = '0', "Payload health check failed.");

                for ii in 0 to stimuli_length - 1 loop
                    check(v_eth_packet_length(ii) = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                    check(v_eth_packet_id(ii) = eth_vector_id(ii), "Wrong Ethernet packet ID");
                    check_false(v_eth_packet_length(ii) < 64, "Ethernet packet length can not be < 64.");
                end loop;
                wait for 50 us;

                info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- check that packet header is working with all alignment in the payload
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("frame_sequence_test") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: frame_sequence_test");
                info("--------------------------------------------------------------------------------");
                start_stimuli              <= '0';
                stimuli_length             <= 1;
                seqnum_error_injection     <= '0';
                back_pressure              <= '1';
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';

                ram_cmd_if_rd_m.request   <= '0'; --: std_logic;
                --ram_cmd_if_rd_s.granted   <= 'Z'; --: std_logic;
                ram_cmd_if_rd_m.cmd_valid <= '0'; --: std_logic;
                ram_cmd_if_rd_m.addr      <= (others => '0'); --: std_logic_vector(C_ADDRESS_WIDTH-1 downto 0);
                --ram_cmd_if_rd_s.busy      <= 'Z'; --: std_logic;  

                ram_cmd_if_wr_m.request   <= '0'; --: std_logic;
                --ram_cmd_if_wr_s.granted   <= 'Z'; --: std_logic;
                ram_cmd_if_wr_m.cmd_valid <= '0'; --: std_logic;
                ram_cmd_if_wr_m.addr      <= (others => '0'); --: std_logic_vector(C_ADDRESS_WIDTH-1 downto 0);
                --ram_cmd_if_wr_s.busy      <= 'Z'; --: std_logic;  

                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);

                for ii in 0 to eth_packet_length_array_t'length - 1 loop
                    eth_vector_id(ii)     <= ii + 1;
                    eth_vector_length(ii) <= 1044;
                end loop;

                for tt in 0 to 15 loop
                    wait for 90 ns;
                    WAIT UNTIL rising_edge(clk);
                    eth_vector_length(0) <= 1044;
                    eth_vector_id(0)     <= tt + 1;
                    start_stimuli        <= '1';
                    WAIT UNTIL rising_edge(clk);
                    start_stimuli        <= '0';
                    WAIT UNTIL rising_edge(clk);
                    WAIT UNTIL rising_edge(clk);

                    -- Insert RAM command for writting a frame in RAM:
                    -- Wait until frame is loaded in fifo:
                    wait until axis_in_m.tlast = '1' and axis_in_m.tvalid = '1';
                    wait until rising_edge(clk);
                    ram_cmd_if_wr_m.request   <= '1';
                    -- wait until access is granted:
                    wait until ram_cmd_if_wr_s.granted = '1';
                    wait until rising_edge(clk);
                    -- send write command:
                    ram_cmd_if_wr_m.cmd_valid <= '1';
                    ram_cmd_if_wr_m.addr      <= std_logic_vector(to_unsigned(tt, C_ADDRESS_WIDTH));
                    wait until rising_edge(clk);
                    ram_cmd_if_wr_m.cmd_valid <= '0';
                    ram_cmd_if_wr_m.addr      <= (others => '0');
                    --wait until ram_cmd_if_wr_s.busy = '1';
                    wait until rising_edge(clk);
                    wait until rising_edge(clk);
                    ram_cmd_if_wr_m.request   <= '0';

                    --Wait until WR process is finished:
                    wait until ram_cmd_if_wr_s.busy = '0';
                    wait until rising_edge(clk);
                    wait until rising_edge(clk);
                    wait for 300 ns;
                    wait until rising_edge(clk);

                    ram_cmd_if_rd_m.request   <= '1';
                    --wait until access is granted:
                    wait until ram_cmd_if_rd_s.granted = '1';
                    wait until rising_edge(clk);
                    -- send write command:
                    back_pressure           <= '0';
                    ram_cmd_if_rd_m.cmd_valid <= '1';
                    ram_cmd_if_rd_m.addr      <= std_logic_vector(to_unsigned(tt, C_ADDRESS_WIDTH));
                    wait until rising_edge(clk);
                    ram_cmd_if_rd_m.cmd_valid <= '0';
                    ram_cmd_if_rd_m.addr      <= (others => '0');
                    --wait until ram_cmd_if_wr_s.busy = '1';
                    wait until rising_edge(clk);
                    wait until rising_edge(clk);
                    ram_cmd_if_rd_m.request   <= '0';
                    --wait for 5 us; 
                    wait for 500 ns;
                    --back_pressure <= '1';
                    --Wait until RD process is finished:
                    --wait until ram_cmd_if_rd_s.busy = '0';
                    wait until rising_edge(clk);
                    wait until rising_edge(clk);
                    wait for 300 ns;
                    wait until rising_edge(clk);

                    --           wait for 200 us;

                    wait until payload_valid = '1';
                    v_payload := payload;

                    -- Check payload data received:
                    payload_test_read_payload_data(
                        v_payload,
                        pk_offset,
                        v_packet,
                        pk_size,
                        v_eth_packet_id,
                        v_eth_packet_length,
                        eth_vector_pointer,
                        split,
                        payload_id_last,
                        v_error);
                    check(v_error = '0', "Payload health check failed.");

                    check(v_eth_packet_length(0) = eth_vector_length(0), "Wrong Ethernet packet Length detected");
                    check(v_eth_packet_id(tt) = eth_vector_id(0), "Wrong Ethernet packet ID");
                    check_false(v_eth_packet_length(tt) < 64, "Ethernet packet length can not be < 64.");
                    wait for 5 us;
                end loop;
                wait for 50 us;

                info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- check that packet header is working with all alignment in the payload
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("frame_burst_sequence_test") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: frame_burst_sequence_test");
                info("--------------------------------------------------------------------------------");
                start_stimuli              <= '0';
                stimuli_length             <= 1;
                seqnum_error_injection     <= '0';
                back_pressure              <= '0';
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';

                ram_cmd_if_rd_m.request   <= '0'; --: std_logic;
                --ram_cmd_if_rd_s.granted   <= 'Z'; --: std_logic;
                ram_cmd_if_rd_m.cmd_valid <= '0'; --: std_logic;
                ram_cmd_if_rd_m.addr      <= (others => '0'); --: std_logic_vector(C_ADDRESS_WIDTH-1 downto 0);
                --ram_cmd_if_rd_s.busy      <= 'Z'; --: std_logic;  

                ram_cmd_if_wr_m.request   <= '0'; --: std_logic;
                --ram_cmd_if_wr_s.granted   <= 'Z'; --: std_logic;
                ram_cmd_if_wr_m.cmd_valid <= '0'; --: std_logic;
                ram_cmd_if_wr_m.addr      <= (others => '0'); --: std_logic_vector(C_ADDRESS_WIDTH-1 downto 0);
                --ram_cmd_if_wr_s.busy      <= 'Z'; --: std_logic;  

                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);

                for ii in 0 to eth_packet_length_array_t'length - 1 loop
                    eth_vector_id(ii)     <= ii + 1;
                    eth_vector_length(ii) <= 1044;
                end loop;

                for tt in 0 to 15 loop
                    wait for 90 ns;
                    WAIT UNTIL rising_edge(clk);
                    eth_vector_length(0) <= 1044;
                    eth_vector_id(0)     <= tt + 1;
                    start_stimuli        <= '1';
                    WAIT UNTIL rising_edge(clk);
                    start_stimuli        <= '0';
                    WAIT UNTIL rising_edge(clk);
                    WAIT UNTIL rising_edge(clk);

                    -- Insert RAM command for writting a frame in RAM:
                    -- Wait until frame is loaded in fifo:
                    wait until axis_in_m.tlast = '1' and axis_in_m.tvalid = '1';
                    wait until rising_edge(clk);
                    ram_cmd_if_wr_m.request   <= '1';
                    -- wait until access is granted:
                    wait until ram_cmd_if_wr_s.granted = '1';
                    wait until rising_edge(clk);
                    -- send write command:
                    ram_cmd_if_wr_m.cmd_valid <= '1';
                    ram_cmd_if_wr_m.addr      <= std_logic_vector(to_unsigned(tt, C_ADDRESS_WIDTH));
                    wait until rising_edge(clk);
                    ram_cmd_if_wr_m.cmd_valid <= '0';
                    ram_cmd_if_wr_m.addr      <= (others => '0');
                    --wait until ram_cmd_if_wr_s.busy = '1';
                    wait until rising_edge(clk);
                    wait until rising_edge(clk);
                    ram_cmd_if_wr_m.request   <= '0';

                    --Wait until WR process is finished:
                    wait until ram_cmd_if_wr_s.busy = '0';
                    wait until rising_edge(clk);
                    wait until rising_edge(clk);
                    wait for 5 us;
                    wait until rising_edge(clk);
                end loop;

                -- READING ALL FRAMES:     
                for tt in 0 to 15 loop
                    WAIT UNTIL rising_edge(clk);
                    eth_vector_length(0)    <= 1044;
                    eth_vector_id(0)        <= tt + 1;
                    wait for 1 us;
                    wait until rising_edge(clk);
                    ram_cmd_if_rd_m.request   <= '1';
                    wait until rising_edge(clk);
                    --wait until access is granted:
                    wait until ram_cmd_if_rd_s.granted = '1';
                    wait until rising_edge(clk);
                    -- send write command:
                    back_pressure           <= '0';
                    ram_cmd_if_rd_m.cmd_valid <= '1';
                    ram_cmd_if_rd_m.addr      <= std_logic_vector(to_unsigned(tt, C_ADDRESS_WIDTH));
                    wait until rising_edge(clk);
                    ram_cmd_if_rd_m.cmd_valid <= '0';
                    ram_cmd_if_rd_m.addr      <= (others => '0');
                    --wait until ram_cmd_if_wr_s.busy = '1';
                    wait until rising_edge(clk);
                    wait until rising_edge(clk);
                    ram_cmd_if_rd_m.request   <= '0';
                    --wait for 5 us; 
                    wait for 500 ns;
                    --back_pressure <= '1';
                    --Wait until RD process is finished:
                    --wait until ram_cmd_if_rd_s.busy = '0';
                    wait until rising_edge(clk);
                    wait until rising_edge(clk);
                    wait for 300 ns;
                    wait until rising_edge(clk);

                    --           wait for 200 us;

                    wait until payload_valid = '1';
                    v_payload := payload;

                    -- Check payload data received:
                    payload_test_read_payload_data(
                        v_payload,
                        pk_offset,
                        v_packet,
                        pk_size,
                        v_eth_packet_id,
                        v_eth_packet_length,
                        eth_vector_pointer,
                        split,
                        payload_id_last,
                        v_error);
                    check(v_error = '0', "Payload health check failed.");

                    check(v_eth_packet_length(0) = eth_vector_length(0), "Wrong Ethernet packet Length detected");
                    check(v_eth_packet_id(tt) = eth_vector_id(0), "Wrong Ethernet packet ID");
                    check_false(v_eth_packet_length(tt) < 64, "Ethernet packet length can not be < 64.");
                    wait for 5 us;
                end loop;
                wait for 50 us;

                info("Test finished");

                --        ----------------------------------------------------------------------
                --        -- TEST CASE DESCRIPTION:
                --        -- Random stress test
                --        -- Expected Result: 
                --        ----------------------------------------------------------------------
                --        ELSIF run("random_stress_test_1") THEN
                --            info("--------------------------------------------------------------------------------");
                --            info("TEST CASE: random_stress_test_1");
                --            info("--------------------------------------------------------------------------------");
                --            WAIT UNTIL reset = '0';
                --            start_stimuli <= '0';
                --            seqnum_error_injection <= '0';
                --            back_pressure <= '0';
                --            frame_type_error_injection <= '0';
                --            tx_fn_error_injection <= '0';
                --            is_idle <= '0';
                --            
                --            eth_vector_pointer := 0;
                --            WAIT UNTIL rising_edge(clk);
                --            v_tx_fn_ref := 0;
                --            stimuli_length <= 50; --eth_packet_length_array_t'length;
                --            --RV.InitSeed(now / 1 ns);
                --            RV.InitSeed(752);
                --            eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                --            eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                --
                --            for ii in 0 to 50 - 1 loop
                --                eth_vector_id(ii)       <= ii+1;
                --                eth_vector_length(ii) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                --                --info("Len=" & to_string(eth_vector_length(ii)));
                --            end loop;
                --
                --            wait for 90 ns;
                --            WAIT UNTIL rising_edge(clk);
                --            start_stimuli <= '1';
                --            WAIT UNTIL rising_edge(clk);
                --            start_stimuli <= '0';
                --            WAIT UNTIL rising_edge(clk);
                --            WAIT UNTIL rising_edge(clk);
                --
                --            --wait for 100 us;
                --            for ii in 0 to stimuli_length-1 loop
                --                wait until eth_packet_valid = '1';
                --                check(eth_packet_error = '0', "Ethernet packet error!");
                --                check(eth_packet_length = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                --                check(eth_packet_id = eth_vector_id(ii), "Wrong Ethernet packet ID");
                --                check_false(eth_packet_length < 64, "Ethernet packet length can not be < 64.");
                --                
                --            end loop;
                --            wait for 20 us;
                --            check(v_total_packet_counter = stimuli_length, "total_packet_counter error!"); 
                --            check(v_total_packet_splitted_counter > 0, "total_packet_splitted_counter error!"); 
                --            check(v_total_packet_merged_counter = v_total_packet_splitted_counter, "total_packet_merged_counter error!"); 
                --            check(v_total_payload_error_counter = 0, "total_payload_error_counter error!"); 
                --            check(v_total_packet_error_counter = 0, "total_packet_error_counter error!"); 
                --            check(v_watchdog_reset_counter = 0, "watchdog_reset_counter error!"); 
                --            check(v_packet_filter_cnt_in = stimuli_length, "packet_filter_cnt_in error!"); 
                --            check(v_packet_filter_cnt_out = stimuli_length, "packet_filter_cnt_out error!"); 
                --
                --            info("Test finished");

            END IF;                     -- for test_suite
        END LOOP test_cases_loop;

        WAIT FOR 20 ns;
        test_runner_cleanup(runner);
    end process;

    inst_payload_transmiter : entity sda_test_lib.payload_transmiter
        port map(
            -- Global Signal Interface
            clk                        => clk, --: in std_logic;

            -- Transaction Interfaces
            tx_fn_init                 => (others => '0'),
            axis_payload_if_m            => axis_payload_if_in_m, --: inout t_axis_payload_if;
            axis_payload_if_s            => axis_payload_if_in_s, --: inout t_axis_payload_if;
            eth_vector_id              => eth_vector_id, --: in eth_packet_id_array_t;
            eth_vector_length          => eth_vector_length, --stimuli_length_array, --: in eth_packet_length_array_t;
            stimuli_length             => stimuli_length, --: in natural;
            start_stimuli              => start_stimuli, --: in std_logic;
            is_idle                    => is_idle, --: in std_logic;
            finished                   => pt_finished, --: out std_logic;
            tx_fn_error_injection      => tx_fn_error_injection, --: in std_logic;
            frame_type_error_injection => frame_type_error_injection, --: in std_logic
            seqnum_error_injection     => seqnum_error_injection --: in std_logic
        );

    -- Little adjustment for different buses. We just want the Axi-s
    axis_in_m.tstart                    <= axis_payload_if_in_m.axis_if_m.tstart;
    axis_in_m.tlast                     <= axis_payload_if_in_m.axis_if_m.tlast;
    axis_in_m.tdata                     <= axis_payload_if_in_m.axis_if_m.tdata;
    axis_in_m.tuser                     <= axis_payload_if_in_m.axis_if_m.tuser;
    axis_in_m.tvalid                    <= axis_payload_if_in_m.axis_if_m.tvalid;
    axis_payload_if_in_s.axis_if_s.tready <= axis_in_s.tready;

    --axis_payload_if_in.tx_fn      <= (others => 'Z');
    --axis_payload_if_in.tx_num     <= (others => 'Z');
    --axis_payload_if_in.frame_type <= (others => 'Z');

    DUT : entity arq_lib.arq_internal_ram_controller
        generic map(
            G_INTERNAL_MEMORY_FSIZE => 16 -- Size in Frames
        )
        port map(
            clk            => clk,      --: in std_logic;
            rst            => reset,    --: in std_logic;

            --Input interfaces
            ram_cmd_if_rd_m  => ram_cmd_if_rd_m, --: inout t_ram_cmd_if;
            ram_cmd_if_rd_s  => ram_cmd_if_rd_s, --: inout t_ram_cmd_if;
            ram_cmd_if_wr_m  => ram_cmd_if_wr_m, --: inout t_ram_cmd_if;
            ram_cmd_if_wr_s  => ram_cmd_if_wr_s, --: inout t_ram_cmd_if;
            axis_in_m        => axis_in_m,  --: inout t_axis_if;
            axis_in_s        => axis_in_s,  --: inout t_axis_if;

            --Output interfaces
            axis_out_m       => axis_out_m, --: inout t_axis_if;
            axis_out_s       => axis_out_s, --: inout t_axis_if;
            internal_error => internal_error --: out std_logic
        );

    -- Little adjustment for different buses. We just want the Axi-s
    axis_payload_if_out_m.axis_if_m.tstart <= axis_out_m.tstart;
    axis_payload_if_out_m.axis_if_m.tvalid <= axis_out_m.tvalid;
    axis_payload_if_out_m.axis_if_m.tlast  <= axis_out_m.tlast;
    axis_payload_if_out_m.axis_if_m.tdata  <= axis_out_m.tdata;
    axis_payload_if_out_m.axis_if_m.tuser  <= axis_out_m.tuser;
    axis_out_s.tready                    <= axis_payload_if_out_s.axis_if_s.tready;

    axis_payload_if_out_m.tx_fn      <= (others => '0');
    axis_payload_if_out_m.tx_num     <= (others => '0');
    axis_payload_if_out_m.frame_type <= (others => '0');

    inst_payload_receiver : entity sda_test_lib.payload_receiver
        port map(
            -- Global Signal Interface
            clk             => clk,     --: in std_logic;

            -- Transaction Interfaces
            axis_payload_if_m => axis_payload_if_out_m, --: inout t_axis_payload_if;
            axis_payload_if_s => axis_payload_if_out_s, --: inout t_axis_payload_if;
            back_pressure   => back_pressure, --: in std_logic;
            tx_fn_out       => open,    --: in std_logic_vector(15 downto 0);
            frame_type_out  => open,    --: in std_logic_vector(1 downto 0);
            tx_num_out      => open,    --: in std_logic_vector(1 downto 0);
            payload         => payload, --: out payload_array_t;
            payload_valid   => payload_valid, --: out std_logic;
            error           => error,   --: out std_logic;
            payload_length  => payload_length, --: out natural;
            seqnum          => seqnum   --: out natural;
        );

    test_runner_watchdog(runner, 1500 us);
end architecture;
