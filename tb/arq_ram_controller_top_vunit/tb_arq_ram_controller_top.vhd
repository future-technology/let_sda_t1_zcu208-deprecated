-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: tb_arq_ram_controller_top
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

library osvvm;
use osvvm.RandomPkg.all;
use osvvm.CoveragePkg.all;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;
library arq_lib;

library sda_test_lib;
use sda_test_lib.sda_test_pkg.all;
library sda_test_lib;
use sda_test_lib.eth_packet_test_pkg.all;
library sda_test_lib;
use sda_test_lib.packet_test_pkg.all;
library sda_test_lib;
use sda_test_lib.payload_test_pkg.all;

--library sda_test_lib;
--use sda_test_lib.ethernet_test_receiver;

entity tb_arq_ram_controller_top is
    generic(runner_cfg : string := runner_cfg_default);
end entity;

architecture tb of tb_arq_ram_controller_top is

    constant C_CLK_PERIOD : time      := 10 ns;
    signal clk            : std_logic := '0';
    signal reset          : std_logic := '1';
    signal error          : std_logic;
    signal send           : std_logic := '0';
    signal payload_length : natural;
    signal seqnum         : natural;
    signal packet_len     : natural   := 0;
    signal payload        : payload_array_t;
    signal payload_valid  : std_logic;
    signal tx_fn          : std_logic_vector(16 - 1 downto 0);
    signal frame_type     : std_logic_vector(2 - 1 downto 0);
    signal tx_fn_out      : std_logic_vector(16 - 1 downto 0);
    signal tx_num_out     : std_logic_vector(3 - 1 downto 0);
    signal frame_type_out : std_logic_vector(2 - 1 downto 0);

    signal start_stimuli        : std_logic := '0';
    signal random_stimuli_wait  : std_logic := '0';
    signal stimuli_length       : natural;
    type t_stimuli_length_array is array (0 to 500 - 1) of natural;
    signal stimuli_length_array : t_stimuli_length_array;

    signal eth_packet_length : natural;
    signal eth_packet_id     : natural;
    --eth_packet        :    eth_packet_array_t;
    signal eth_packet_valid  : std_logic;
    signal eth_packet_error  : std_logic;
    --signal axis_ethernet_if  : t_axis_if;

    signal eth_vector_id              : eth_packet_id_array_t;
    signal eth_vector_length          : eth_packet_length_array_t;
    signal tx_fn_error_injection      : std_logic;
    signal frame_type_error_injection : std_logic;
    signal seqnum_error_injection     : std_logic;
    signal is_idle                    : std_logic;
    signal pt_finished                : std_logic;
    signal back_pressure              : std_logic := '0';
    signal eth_packet_good_cnt        : natural   := 0;

    --DUT signals
    --Input interfaces
    signal ram_cmd_if_rd_m       : t_ram_cmd_if_m;
    signal ram_cmd_if_rd_s       : t_ram_cmd_if_s;
    signal ram_cmd_if_wr_m       : t_ram_cmd_if_m;
    signal ram_cmd_if_wr_s       : t_ram_cmd_if_s;
    signal axis_in_m             : t_axis_if_m;
    signal axis_in_s             : t_axis_if_s;
    --Output interfaces
    signal axis_out_m            : t_axis_if_m;
    signal axis_out_s            : t_axis_if_s;
    signal internal_error      : std_logic;
    signal clear_diag      : std_logic;
    signal req_insertion_if_m    : t_request_insertion_ram_if_m;
    signal req_insertion_if_s    : t_request_insertion_ram_if_s;
    signal req_resend_if_m       : t_request_resend_if_m;
    signal req_resend_if_s       : t_request_resend_if_s;
    signal axis_payload_if_in_m  : t_axis_payload_if_m;
    signal axis_payload_if_in_s  : t_axis_payload_if_s;
    signal axis_payload_if_out_m : t_axis_payload_if_m;
    signal axis_payload_if_out_s : t_axis_payload_if_s;
    signal insert_fifo_empty   : std_logic;
    signal diag_if             : t_arq_ram_controller_diag_if;
    signal req_cache_if_m        : t_request_cache_if_m;
    signal req_cache_if_s        : t_request_cache_if_s;
    signal arq_config_if       : t_arq_config_if;
begin

    --------------------------------------------------------------------------
    -- CLOCK AND RESET.
    --------------------------------------------------------------------------
    clk   <= NOT clk after C_CLK_PERIOD / 2;
    reset <= '0' after 5 * (C_CLK_PERIOD);

    test_runner : process
        variable v_tx_fn_ref              : natural := 0;
        variable v_tx_fn                  : natural := 0;
        variable v_packets_read           : natural := 0;
        variable v_error                  : std_logic;
        variable payload_id_last          : natural := 0;
        variable eth_vector_pointer       : natural;
        variable v_eth_vector_pointer_aux : natural := 0;
        variable pk_size                  : natural := 64;
        variable pk_offset                : natural := 0;
        variable v_packet                 : packet_array_t;
        variable split                    : std_logic;
        variable v_payload                : payload_array_t;
        variable data_flag                : std_logic;
        variable wait_for_idle            : std_logic;
        variable v_eth_packet_id          : eth_packet_id_array_t;
        variable v_eth_packet_length      : eth_packet_length_array_t;

    begin
        test_runner_setup(runner, runner_cfg);
        -- enable logging for passing check
        --show(get_logger(default_checker), display_handler, pass); 
        --show(get_logger(RED_CHECKER), display_handler, pass); 
        --packet_len <= 100;
        test_cases_loop : WHILE test_suite LOOP

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- check that packet header is working with all alignment in the payload
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            --IF run("basic_rw_burst_test") THEN
            --    info("--------------------------------------------------------------------------------");
            --    info("TEST CASE: basic_rw_burst_test");
            --    info("--------------------------------------------------------------------------------");

            --    start_stimuli              <= '0';
            --    stimuli_length             <= 2;
            --    seqnum_error_injection     <= '0';
            --    back_pressure              <= '1';
            --    frame_type_error_injection <= '0';
            --    tx_fn_error_injection      <= '0';
            --    is_idle                    <= '0';
            --    clear_diag          <= '0';
            --    req_insertion_if           <= C_REQ_INSERTION_RAM_IF_MASTER_INIT;
            --    req_cache_if               <= C_REQ_CACHE_IF_MASTER_INIT;
            --    req_resend_if              <= C_REQ_RESEND_IF_MASTER_INIT;
            --    arq_config_if.holdoff_time        <= std_logic_vector(to_unsigned(200, C_TIMESTAMP_WIDTH));
            --    arq_config_if.core_counter_period <= std_logic_vector(to_unsigned(50 - 1, 12)); --20*5=100 tenths of us -> 1 us
            --    arq_config_if.cache_enable <= '1';

            --    WAIT UNTIL reset = '0';
            --    WAIT UNTIL rising_edge(clk);

            --    for ii in 0 to eth_packet_length_array_t'length - 1 loop
            --        eth_vector_id(ii) <= ii + 1;
            --    end loop;
            --    eth_vector_length(0) <= 519;
            --    eth_vector_length(1) <= 520;

            --    for ii in 0 to 15 loop
            --        eth_vector_id(0)     <= (ii * 2);
            --        eth_vector_id(1)     <= (ii * 2) + 1;
            --        eth_vector_length(0) <= 519;
            --        eth_vector_length(1) <= 520;

            --        wait for 90 ns;
            --        if insert_fifo_empty = '0' then
            --            wait until insert_fifo_empty = '1';
            --        end if;

            --        WAIT UNTIL rising_edge(clk);
            --        start_stimuli <= '1';
            --        WAIT UNTIL rising_edge(clk);
            --        start_stimuli <= '0';
            --        WAIT UNTIL rising_edge(clk);
            --        WAIT UNTIL rising_edge(clk);

            --        wait until rising_edge(clk);

            --        if req_insertion_if.ready = '0' then
            --            wait until req_insertion_if.ready = '1';
            --        end if;

            --        wait until rising_edge(clk);
            --        req_insertion_if.tx_fn <= std_logic_vector(to_unsigned(ii, 16));
            --        req_insertion_if.addr  <= std_logic_vector(to_unsigned(ii, C_ADDRESS_WIDTH));
            --        req_insertion_if.valid <= '1';
            --        -- wait until command is ack:
            --        wait until rising_edge(clk);
            --        req_insertion_if.valid <= '0';
            --        wait until rising_edge(clk);
            --        check(req_insertion_if.ready = '0', "req_insertion_if.ready should be low.");
            --        check(insert_fifo_empty = '0', "req_insertion_if.ready should be low.");

            --        wait for 20 us;
            --        check(req_insertion_if.ready = '1', "req_insertion_if.ready should be high.");
            --        check(insert_fifo_empty = '1', "req_insertion_if.ready should be high.");

            --    end loop;

            --    back_pressure <= '0';

            --    for jj in 0 to 15 loop
            --        wait for 1 us;
            --        eth_vector_id(0)     <= (jj * 2);
            --        eth_vector_id(1)     <= (jj * 2) + 1;
            --        eth_vector_length(0) <= 519;
            --        eth_vector_length(1) <= 520;

            --        -- if req_resend_if.ready = '0' then
            --        --     wait until req_resend_if.ready = '1';
            --        -- end if;

            --        -- request resend:
            --        wait until rising_edge(clk);
            --        req_resend_if.tx_fn      <= std_logic_vector(to_unsigned(jj, C_TX_FN_WIDTH));
            --        req_resend_if.tx_num     <= std_logic_vector(to_unsigned(2, C_TX_NUM_WIDTH));
            --        req_resend_if.frame_type <= std_logic_vector(to_unsigned(1, C_FRAME_TYPE_WIDTH));
            --        req_resend_if.addr       <= std_logic_vector(to_unsigned(jj, C_ADDRESS_WIDTH));
            --        req_resend_if.valid      <= '1';

            --        wait until req_resend_if.ready = '1';
            --        wait until rising_edge(clk);
            --        req_resend_if.valid <= '0';
            --        --wait for 50 us;

            --        -- start reading frames:

            --        wait until payload_valid = '1';
            --        v_payload := payload;

            --        -- Check payload data received:
            --        payload_test_read_payload_data(
            --            v_payload,
            --            pk_offset,
            --            v_packet,
            --            pk_size,
            --            v_eth_packet_id,
            --            v_eth_packet_length,
            --            eth_vector_pointer,
            --            split,
            --            payload_id_last,
            --            v_error);
            --        check(v_error = '0', "Payload health check failed.");

            --        check(frame_type_out = std_logic_vector(to_unsigned(1, C_FRAME_TYPE_WIDTH)), "ERROR: Wrong frame type");
            --        check(tx_fn_out = std_logic_vector(to_unsigned(jj, C_TX_FN_WIDTH)), "ERROR: Wrong TX_FN");
            --        check(tx_num_out = std_logic_vector(to_unsigned(2, C_TX_NUM_WIDTH)), "ERROR: Wrong TX_NUM");

            --        for ii in 0 to stimuli_length - 1 loop
            --            check(v_eth_packet_length(ii) = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
            --            check(v_eth_packet_id(ii) = eth_vector_id(ii), "Wrong Ethernet packet ID");
            --            check_false(v_eth_packet_length(ii) < 64, "Ethernet packet length can not be < 64.");
            --        end loop;
            --        eth_vector_pointer := 0;
            --    end loop;
            --    wait for 50 us;
            --    check(diag_if.insertion_req_counter = to_unsigned(16, 32), "insertion req counter");
            --    check(diag_if.retx_counter = to_unsigned(16, 32), "insertion req counter");
            --    check(diag_if.cache_req_counter = to_unsigned(0, 32), ": insertion req counter");
            --    check(diag_if.cache_retx_counter = to_unsigned(0, 32), "insertion req counter");
            --    check(diag_if.address_error = to_unsigned(0, 32), "insertion req counter");
            --    check(diag_if.insertion_error = to_unsigned(0, 32), "insertion req counter");
            --    check(diag_if.cache_error = to_unsigned(0, 32), "insertion req counter");
            --    check(diag_if.ram_error = '0', "insertion req counter");
            --    check(diag_if.retx_error = to_unsigned(0, 32), "insertion req counter");

            --    info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- check that packet header is working with all alignment in the payload
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            --ELSIF run("basic_rw_test") THEN
            --    info("--------------------------------------------------------------------------------");
            --    info("TEST CASE: basic_rw_test");
            --    info("--------------------------------------------------------------------------------");

            --    start_stimuli              <= '0';
            --    stimuli_length             <= 2;
            --    seqnum_error_injection     <= '0';
            --    back_pressure              <= '1';
            --    frame_type_error_injection <= '0';
            --    tx_fn_error_injection      <= '0';
            --    is_idle                    <= '0';
            --    arq_config_if.holdoff_time        <= std_logic_vector(to_unsigned(200, C_TIMESTAMP_WIDTH));
            --    arq_config_if.core_counter_period <= std_logic_vector(to_unsigned(50 - 1, 12)); --20*5=100 tenths of us -> 1 us
            --    arq_config_if.cache_enable <= '1';
            --    clear_diag <= '0';
            --    req_insertion_if  <= C_REQ_INSERTION_RAM_IF_MASTER_INIT;
            --    req_cache_if      <= C_REQ_CACHE_IF_MASTER_INIT;
            --    req_resend_if     <= C_REQ_RESEND_IF_MASTER_INIT;

            --    WAIT UNTIL reset = '0';
            --    WAIT UNTIL rising_edge(clk);

            --    for ii in 0 to eth_packet_length_array_t'length - 1 loop
            --        eth_vector_id(ii) <= ii + 1;
            --    end loop;
            --    eth_vector_length(0) <= 519;
            --    eth_vector_length(1) <= 520;

            --    back_pressure <= '0';

            --    for ii in 0 to 15 loop
            --        eth_vector_id(0)     <= (ii * 2);
            --        eth_vector_id(1)     <= (ii * 2) + 1;
            --        eth_vector_length(0) <= 519;
            --        eth_vector_length(1) <= 520;

            --        wait for 90 ns;
            --        if insert_fifo_empty = '0' then
            --            wait until insert_fifo_empty = '1';
            --        end if;

            --        WAIT UNTIL rising_edge(clk);
            --        start_stimuli <= '1';
            --        WAIT UNTIL rising_edge(clk);
            --        start_stimuli <= '0';
            --        WAIT UNTIL rising_edge(clk);
            --        WAIT UNTIL rising_edge(clk);

            --        wait until rising_edge(clk);

            --        if req_insertion_if.ready = '0' then
            --            wait until req_insertion_if.ready = '1';
            --        end if;

            --        wait until rising_edge(clk);
            --        req_insertion_if.tx_fn <= std_logic_vector(to_unsigned(ii, 16));
            --        req_insertion_if.addr  <= std_logic_vector(to_unsigned(ii, C_ADDRESS_WIDTH));
            --        req_insertion_if.valid <= '1';
            --        -- wait until command is ack:
            --        wait until rising_edge(clk);
            --        req_insertion_if.valid <= '0';
            --        wait until rising_edge(clk);
            --        check(req_insertion_if.ready = '0', "req_insertion_if.ready should be low.");
            --        check(insert_fifo_empty = '0', "req_insertion_if.ready should be low.");

            --        wait for 20 us;
            --        check(req_insertion_if.ready = '1', "req_insertion_if.ready should be high.");
            --        check(insert_fifo_empty = '1', "req_insertion_if.ready should be high.");

            --        wait for 1 us;

            --        -- if req_resend_if.ready = '0' then
            --        --     wait until req_resend_if.ready = '1';
            --        -- end if;

            --        -- request resend:
            --        wait until rising_edge(clk);
            --        req_resend_if.tx_fn      <= std_logic_vector(to_unsigned(ii, C_TX_FN_WIDTH));
            --        req_resend_if.tx_num     <= std_logic_vector(to_unsigned(2, C_TX_NUM_WIDTH));
            --        req_resend_if.frame_type <= std_logic_vector(to_unsigned(1, C_FRAME_TYPE_WIDTH));
            --        req_resend_if.addr       <= std_logic_vector(to_unsigned(ii, C_ADDRESS_WIDTH));
            --        req_resend_if.valid      <= '1';

            --        wait until req_resend_if.ready = '1';
            --        wait until rising_edge(clk);
            --        req_resend_if.valid <= '0';
            --        --wait for 50 us;

            --        -- start reading frames:

            --        wait until payload_valid = '1';
            --        v_payload := payload;

            --        -- Check payload data received:
            --        payload_test_read_payload_data(
            --            v_payload,
            --            pk_offset,
            --            v_packet,
            --            pk_size,
            --            v_eth_packet_id,
            --            v_eth_packet_length,
            --            eth_vector_pointer,
            --            split,
            --            payload_id_last,
            --            v_error);
            --        check(v_error = '0', "Payload health check failed.");

            --        check(frame_type_out = std_logic_vector(to_unsigned(1, C_FRAME_TYPE_WIDTH)), "ERROR: Wrong frame type");
            --        check(tx_fn_out = std_logic_vector(to_unsigned(ii, C_TX_FN_WIDTH)), "ERROR: Wrong TX_FN");
            --        check(tx_num_out = std_logic_vector(to_unsigned(2, C_TX_NUM_WIDTH)), "ERROR: Wrong TX_NUM");

            --        for kk in 0 to stimuli_length - 1 loop
            --            check(v_eth_packet_length(kk) = eth_vector_length(kk), "Wrong Ethernet packet Length detected");
            --            check(v_eth_packet_id(kk) = eth_vector_id(kk), "Wrong Ethernet packet ID");
            --            check_false(v_eth_packet_length(kk) < 64, "Ethernet packet length can not be < 64.");
            --        end loop;
            --        eth_vector_pointer := 0;
            --    end loop;
            --    wait for 50 us;
            --    check(diag_if.insertion_req_counter = to_unsigned(16, 32), "insertion req counter");
            --    check(diag_if.retx_counter = to_unsigned(16, 32), "insertion req counter");
            --    check(diag_if.cache_req_counter = to_unsigned(0, 32), ": insertion req counter");
            --    check(diag_if.cache_retx_counter = to_unsigned(0, 32), "insertion req counter");
            --    check(diag_if.address_error = to_unsigned(0, 32), "insertion req counter");
            --    check(diag_if.insertion_error = to_unsigned(0, 32), "insertion req counter");
            --    check(diag_if.cache_error = to_unsigned(0, 32), "insertion req counter");
            --    check(diag_if.ram_error = '0', "insertion req counter");
            --    check(diag_if.retx_error = to_unsigned(0, 32), "insertion req counter");

            --    info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- 10 insertions and 4 retx from cache to test ram and cache retx. Cache timeout
            --        -- avoided.
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            IF run("basic_cache_rw_test") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: basic_cache_rw_test");
                info("--------------------------------------------------------------------------------");

                start_stimuli                     <= '0';
                stimuli_length                    <= 2;
                seqnum_error_injection            <= '0';
                back_pressure                     <= '1';
                frame_type_error_injection        <= '0';
                tx_fn_error_injection             <= '0';
                is_idle                           <= '0';
                arq_config_if.holdoff_time        <= std_logic_vector(to_unsigned(200, C_TIMESTAMP_WIDTH));
                arq_config_if.core_counter_period <= std_logic_vector(to_unsigned(50 - 1, 12)); --20*5=100 tenths of us -> 1 us
                arq_config_if.cache_enable        <= '1';
                clear_diag                 <= '0';
                req_insertion_if_m                  <= C_REQ_INSERTION_RAM_IF_MASTER_INIT;
                req_cache_if_m                      <= C_REQ_CACHE_IF_MASTER_INIT;
                req_resend_if_m                     <= C_REQ_RESEND_IF_MASTER_INIT;

                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);

                for ii in 0 to eth_packet_length_array_t'length - 1 loop
                    eth_vector_id(ii) <= ii + 1;
                end loop;
                eth_vector_length(0) <= 519;
                eth_vector_length(1) <= 520;

                for ii in 0 to 19 loop
                    eth_vector_id(0)     <= (ii * 2);
                    eth_vector_id(1)     <= (ii * 2) + 1;
                    eth_vector_length(0) <= 519;
                    eth_vector_length(1) <= 520;

                    wait for 90 ns;
                    if insert_fifo_empty = '0' then
                        wait until insert_fifo_empty = '1';
                    end if;

                    WAIT UNTIL rising_edge(clk);
                    start_stimuli <= '1';
                    WAIT UNTIL rising_edge(clk);
                    start_stimuli <= '0';
                    WAIT UNTIL rising_edge(clk);
                    WAIT UNTIL rising_edge(clk);

                    wait until rising_edge(clk);

                    if req_insertion_if_s.ready = '0' then
                        wait until req_insertion_if_s.ready = '1';
                    end if;

                    wait until rising_edge(clk);
                    req_insertion_if_m.tx_fn <= std_logic_vector(to_unsigned(ii, 16));
                    req_insertion_if_m.addr  <= std_logic_vector(to_unsigned(ii, C_ADDRESS_WIDTH));
                    req_insertion_if_m.valid <= '1';
                    -- wait until command is ack:
                    wait until rising_edge(clk);
                    req_insertion_if_m.valid <= '0';
                    wait until rising_edge(clk);
                    check(req_insertion_if_s.ready = '0', "req_insertion_if.ready should be low.");
                    check(insert_fifo_empty = '0', "insert fifo empty: should be low.");

                    wait for 20 us;
                    check(req_insertion_if_s.ready = '1', "req_insertion_if.ready should be high.");
                    check(insert_fifo_empty = '1', "insert fifo empty: should be high.");

                end loop;

                back_pressure <= '0';

                for kk in 0 to 15 loop

                    -- if req_cache_if.ready = '0' then
                    --     wait until req_cache_if.ready = '1';
                    -- end if;

                    -- request resend:
                    wait until rising_edge(clk);
                    req_cache_if_m.tx_fn <= std_logic_vector(to_unsigned(kk, C_TX_FN_WIDTH));
                    req_cache_if_m.addr  <= std_logic_vector(to_unsigned(kk, C_ADDRESS_WIDTH));
                    req_cache_if_m.valid <= '1';
                    wait until req_cache_if_s.ready = '1';
                    wait until rising_edge(clk);
                    req_cache_if_m.valid <= '0';
                    wait for 300 ns;

                end loop;

                for jj in 0 to 15 loop
                    wait for 300 ns;
                    eth_vector_id(0)     <= (jj * 2);
                    eth_vector_id(1)     <= (jj * 2) + 1;
                    eth_vector_length(0) <= 519;
                    eth_vector_length(1) <= 520;

                    -- if req_resend_if.ready = '0' then
                    --     wait until req_resend_if.ready = '1';
                    -- end if;

                    -- request resend:
                    wait until rising_edge(clk);
                    req_resend_if_m.tx_fn      <= std_logic_vector(to_unsigned(jj, C_TX_FN_WIDTH));
                    req_resend_if_m.tx_num     <= std_logic_vector(to_unsigned(2, C_TX_NUM_WIDTH));
                    req_resend_if_m.frame_type <= std_logic_vector(to_unsigned(1, C_FRAME_TYPE_WIDTH));
                    req_resend_if_m.addr       <= std_logic_vector(to_unsigned(jj, C_ADDRESS_WIDTH));
                    req_resend_if_m.valid      <= '1';

                    wait until req_resend_if_s.ready = '1';
                    wait until rising_edge(clk);
                    req_resend_if_m.valid <= '0';
                    --wait for 50 us;

                    -- start reading frames:

                    wait until payload_valid = '1';
                    v_payload := payload;

                    -- Check payload data received:
                    payload_test_read_payload_data(
                        v_payload,
                        pk_offset,
                        v_packet,
                        pk_size,
                        v_eth_packet_id,
                        v_eth_packet_length,
                        eth_vector_pointer,
                        split,
                        payload_id_last,
                        v_error);
                    check(v_error = '0', "Payload health check failed.");

                    check(frame_type_out = std_logic_vector(to_unsigned(1, C_FRAME_TYPE_WIDTH)), "ERROR: Wrong frame type");
                    check(tx_fn_out = std_logic_vector(to_unsigned(jj, C_TX_FN_WIDTH)), "ERROR: Wrong TX_FN");
                    check(tx_num_out = std_logic_vector(to_unsigned(2, C_TX_NUM_WIDTH)), "ERROR: Wrong TX_NUM");

                    for ii in 0 to stimuli_length - 1 loop
                        check(v_eth_packet_length(ii) = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                        check(v_eth_packet_id(ii) = eth_vector_id(ii), "Wrong Ethernet packet ID");
                        check_false(v_eth_packet_length(ii) < 64, "Ethernet packet length can not be < 64.");
                    end loop;
                    eth_vector_pointer := 0;
                end loop;
                wait for 10 us;
                -- some stimuli to make it fail
                for kk in 5 to 10 loop
                    wait until rising_edge(clk);
                    req_resend_if_m.tx_fn      <= std_logic_vector(to_unsigned(kk + 100, C_TX_FN_WIDTH));
                    req_resend_if_m.tx_num     <= std_logic_vector(to_unsigned(2, C_TX_NUM_WIDTH));
                    req_resend_if_m.frame_type <= std_logic_vector(to_unsigned(1, C_FRAME_TYPE_WIDTH));
                    req_resend_if_m.addr       <= std_logic_vector(to_unsigned(kk, C_ADDRESS_WIDTH));
                    req_resend_if_m.valid      <= '1';
                    wait until req_resend_if_s.ready = '1';
                    wait until rising_edge(clk);
                    req_resend_if_m.valid      <= '0';
                    wait for 300 ns;
                end loop;

                wait for 200 us;

                check(diag_if.insertion_req_counter = to_unsigned(20, 32), "insertion req counter");
                check(diag_if.cache_req_counter = to_unsigned(16, 32), ": cache req counter");
                check(diag_if.cache_retx_counter = to_unsigned(16, 32), "cache retx counter");
                check(diag_if.cache_timeout_counter = to_unsigned(0, 32), "cache retx counter");
                check(diag_if.address_error = to_unsigned(0, 32), "addr error counter");
                check(diag_if.insertion_error = to_unsigned(4, 32), "insertion error counter");
                check(diag_if.insertion_abort = to_unsigned(4, 32), "insertion abort counter");
                check(diag_if.cache_error = to_unsigned(0, 32), "cache error counter");
                check(diag_if.ram_error = '0', "ram error flag");

                info("Test finished");

                --        ----------------------------------------------------------------------
                --        -- TEST CASE DESCRIPTION:
                --        -- Expected Result: 
                --        ----------------------------------------------------------------------
                -- ELSIF run("cache_timeout_test") THEN
                --     info("--------------------------------------------------------------------------------");
                --     info("TEST CASE: cache_timeout_test");
                --     info("--------------------------------------------------------------------------------");

                --     start_stimuli              <= '0';
                --     stimuli_length             <= 2;
                --     seqnum_error_injection     <= '0';
                --     back_pressure              <= '1';
                --     frame_type_error_injection <= '0';
                --     tx_fn_error_injection      <= '0';
                --     is_idle                    <= '0';
                --     arq_config_if.holdoff_time        <= std_logic_vector(to_unsigned(200, C_TIMESTAMP_WIDTH));
                --     arq_config_if.core_counter_period <= std_logic_vector(to_unsigned(50 - 1, 12)); --20*5=100 tenths of us -> 1 us
                --     arq_config_if.cache_enable <= '1';
                --     clear_diag <= '0';
                --     req_insertion_if  <= C_REQ_INSERTION_RAM_IF_MASTER_INIT;
                --     req_cache_if      <= C_REQ_CACHE_IF_MASTER_INIT;
                --     req_resend_if     <= C_REQ_RESEND_IF_MASTER_INIT;

                --     WAIT UNTIL reset = '0';
                --     WAIT UNTIL rising_edge(clk);

                --     for ii in 0 to eth_packet_length_array_t'length - 1 loop
                --         eth_vector_id(ii) <= ii + 1;
                --     end loop;
                --     eth_vector_length(0) <= 519;
                --     eth_vector_length(1) <= 520;

                --     for ii in 0 to 15 loop
                --         eth_vector_id(0)     <= (ii * 2);
                --         eth_vector_id(1)     <= (ii * 2) + 1;
                --         eth_vector_length(0) <= 519;
                --         eth_vector_length(1) <= 520;

                --         wait for 90 ns;
                --         if insert_fifo_empty = '0' then
                --             wait until insert_fifo_empty = '1';
                --         end if;

                --         WAIT UNTIL rising_edge(clk);
                --         start_stimuli <= '1';
                --         WAIT UNTIL rising_edge(clk);
                --         start_stimuli <= '0';
                --         WAIT UNTIL rising_edge(clk);
                --         WAIT UNTIL rising_edge(clk);

                --         wait until rising_edge(clk);

                --         if req_insertion_if.ready = '0' then
                --             wait until req_insertion_if.ready = '1';
                --         end if;

                --         wait until rising_edge(clk);
                --         req_insertion_if.tx_fn <= std_logic_vector(to_unsigned(ii, 16));
                --         req_insertion_if.addr  <= std_logic_vector(to_unsigned(ii, C_ADDRESS_WIDTH));
                --         req_insertion_if.valid <= '1';
                --         -- wait until command is ack:
                --         wait until rising_edge(clk);
                --         req_insertion_if.valid <= '0';
                --         wait until rising_edge(clk);
                --         check(req_insertion_if.ready = '0', "req_insertion_if.ready should be low.");
                --         check(insert_fifo_empty = '0', "req_insertion_if.ready should be low.");

                --         wait for 20 us;
                --         check(req_insertion_if.ready = '1', "req_insertion_if.ready should be high.");
                --         check(insert_fifo_empty = '1', "req_insertion_if.ready should be high.");

                --     end loop;

                --     back_pressure <= '0';

                --     for kk in 0 to 15 loop
                --         wait until rising_edge(clk);
                --         req_cache_if.tx_fn <= std_logic_vector(to_unsigned(kk, C_TX_FN_WIDTH));
                --         req_cache_if.addr  <= std_logic_vector(to_unsigned(kk, C_ADDRESS_WIDTH));
                --         req_cache_if.valid <= '1';
                --         wait until req_cache_if.ready = '1';
                --         wait until rising_edge(clk);
                --         req_cache_if.valid <= '0';
                --         wait for 1 us;

                --     end loop;

                --     check(diag_if.insertion_req_counter = to_unsigned(16, 32), "insertion req counter");
                --     check(diag_if.retx_counter = to_unsigned(0, 32), "retx req counter");
                --     check(diag_if.cache_req_counter = to_unsigned(16, 32), ": cache req counter");
                --     check(diag_if.cache_retx_counter = to_unsigned(0, 32), "cache retx counter");
                --     check(diag_if.cache_timeout_counter = to_unsigned(16, 32), "cache timeout counter");
                --     check(diag_if.address_error = to_unsigned(0, 32), "addr error counter");
                --     check(diag_if.insertion_error = to_unsigned(0, 32), "insertion error counter");
                --     check(diag_if.cache_error = to_unsigned(0, 32), "cache error counter");
                --     check(diag_if.ram_error = '0', "ram error counter");
                --     check(diag_if.insertion_error = to_unsigned(0, 32), "insertion error counter");

                --     info("Test finished");
                --------------------------------------------------------------------------------
                --        -- TEST CASE DESCRIPTION:
                --        -- check that packet header is working with all alignment in the payload
                --        -- Expected Result: 
                --        ----------------------------------------------------------------------
                --ELSIF run("timeout_check") THEN
                --    info("--------------------------------------------------------------------------------");
                --    info("TEST CASE: timeout_check");
                --    info("--------------------------------------------------------------------------------");
                --    start_stimuli <= '0';
                --    stimuli_length <= 2;
                --    seqnum_error_injection <= '0';
                --    back_pressure <= '1';
                --    frame_type_error_injection <= '0';
                --    tx_fn_error_injection <= '0';
                --    is_idle <= '0';     

                --    ram_cmd_if_rd.request   <= '0'; --: std_logic;
                --    ram_cmd_if_rd.granted   <= 'Z'; --: std_logic;
                --    ram_cmd_if_rd.cmd_valid <= '0'; --: std_logic;
                --    ram_cmd_if_rd.addr      <= (others=>'0'); --: std_logic_vector(C_ADDRESS_WIDTH-1 downto 0);
                --    ram_cmd_if_rd.busy      <= 'Z'; --: std_logic;  

                --    ram_cmd_if_wr.request   <= '0'; --: std_logic;
                --    ram_cmd_if_wr.granted   <= 'Z'; --: std_logic;
                --    ram_cmd_if_wr.cmd_valid <= '0'; --: std_logic;
                --    ram_cmd_if_wr.addr      <= (others=>'0'); --: std_logic_vector(C_ADDRESS_WIDTH-1 downto 0);
                --    ram_cmd_if_wr.busy      <= 'Z'; --: std_logic;  

                --    WAIT UNTIL reset = '0';
                --    WAIT UNTIL rising_edge(clk);

                --    for ii in 0 to eth_packet_length_array_t'length - 1 loop
                --        eth_vector_id(ii)       <= ii+1;
                --    end loop;
                --    eth_vector_length(0) <= 519;
                --    eth_vector_length(1) <= 520;

                --    wait for 90 ns;
                --    ram_cmd_if_wr.request <= '1';
                --    -- wait until access is granted:
                --    wait until ram_cmd_if_wr.granted = '1';
                --    wait until rising_edge(clk);
                --    -- No command response to force timeout:
                --    wait until rising_edge(clk);
                --    wait until rising_edge(clk);
                --    wait until rising_edge(clk);
                --    wait until rising_edge(clk);
                --    wait until rising_edge(clk);
                --    wait until rising_edge(clk);
                --    wait until rising_edge(clk);
                --    wait until rising_edge(clk);
                --    wait until rising_edge(clk);

                --    -- Try again. 
                --    WAIT UNTIL rising_edge(clk);
                --    start_stimuli <= '1';
                --    WAIT UNTIL rising_edge(clk);
                --    start_stimuli <= '0';
                --    WAIT UNTIL rising_edge(clk);
                --    WAIT UNTIL rising_edge(clk);

                --    -- Insert RAM command for writting a frame in RAM:
                --    -- Wait until frame is loaded in fifo:
                --    wait until axis_in.tlast = '1' and axis_in.tvalid = '1';
                --    wait until rising_edge(clk);
                --    ram_cmd_if_wr.request <= '1';
                --    -- wait until access is granted:
                --    wait until ram_cmd_if_wr.granted = '1';
                --    wait until rising_edge(clk);
                --    -- send write command:
                --    ram_cmd_if_wr.cmd_valid <= '1';
                --    ram_cmd_if_wr.addr      <= std_logic_vector(to_unsigned(0, C_ADDRESS_WIDTH));
                --    wait until rising_edge(clk);
                --    ram_cmd_if_wr.cmd_valid <= '0';
                --    ram_cmd_if_wr.addr      <= (others=>'0');
                --    --wait until ram_cmd_if_wr.busy = '1';
                --    wait until rising_edge(clk);
                --    wait until rising_edge(clk);
                --    ram_cmd_if_wr.request <= '0';

                --    --Wait until WR process is finished:
                --    wait until ram_cmd_if_wr.busy = '0';
                --    wait until rising_edge(clk);
                --    wait until rising_edge(clk);
                --    wait for 300 ns;
                --    wait until rising_edge(clk);

                --    ram_cmd_if_rd.request <= '1';
                --    --wait until access is granted:
                --    wait until ram_cmd_if_rd.granted = '1';
                --    wait until rising_edge(clk);
                --    -- send write command:
                --    back_pressure <= '0';
                --    ram_cmd_if_rd.cmd_valid <= '1';
                --    ram_cmd_if_rd.addr      <= std_logic_vector(to_unsigned(0, C_ADDRESS_WIDTH));
                --    wait until rising_edge(clk);
                --    ram_cmd_if_rd.cmd_valid <= '0';
                --    ram_cmd_if_rd.addr      <= (others=>'0');
                --    --wait until ram_cmd_if_wr.busy = '1';
                --    wait until rising_edge(clk);
                --    wait until rising_edge(clk);
                --    ram_cmd_if_rd.request <= '0';
                --    --wait for 5 us; 
                --    wait for 500 ns;
                --    --back_pressure <= '1';
                --     --Wait until RD process is finished:
                --    --wait until ram_cmd_if_rd.busy = '0';
                --    wait until rising_edge(clk);
                --    wait until rising_edge(clk);
                --    wait for 300 ns;
                --    wait until rising_edge(clk);

                ----           wait for 200 us;

                --    wait until payload_valid = '1';
                --    v_payload := payload;

                --    -- Check payload data received:
                --    payload_test_read_payload_data(
                --        v_payload,  
                --        pk_offset, 
                --        v_packet, 
                --        pk_size, 
                --        v_eth_packet_id, 
                --        v_eth_packet_length, 
                --        eth_vector_pointer,
                --        split,
                --        payload_id_last,
                --        v_error);
                --    check(v_error = '0', "Payload health check failed.");

                --    for ii in 0 to stimuli_length-1 loop
                --        check(v_eth_packet_length(ii) = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                --        check(v_eth_packet_id(ii) = eth_vector_id(ii), "Wrong Ethernet packet ID");
                --        check_false(v_eth_packet_length(ii) < 64, "Ethernet packet length can not be < 64.");
                --    end loop;
                --    wait for 50 us;  

                --    info("Test finished");

                --        ----------------------------------------------------------------------
                --        -- TEST CASE DESCRIPTION:
                --        -- check that packet header is working with all alignment in the payload
                --        -- Expected Result: 
                --        ----------------------------------------------------------------------
                --ELSIF run("frame_sequence_test") THEN
                --    info("--------------------------------------------------------------------------------");
                --    info("TEST CASE: frame_sequence_test");
                --    info("--------------------------------------------------------------------------------");
                --    start_stimuli <= '0';
                --    stimuli_length <= 1;
                --    seqnum_error_injection <= '0';
                --    back_pressure <= '1';
                --    frame_type_error_injection <= '0';
                --    tx_fn_error_injection <= '0';
                --    is_idle <= '0';     

                --    ram_cmd_if_rd.request   <= '0'; --: std_logic;
                --    ram_cmd_if_rd.granted   <= 'Z'; --: std_logic;
                --    ram_cmd_if_rd.cmd_valid <= '0'; --: std_logic;
                --    ram_cmd_if_rd.addr      <= (others=>'0'); --: std_logic_vector(C_ADDRESS_WIDTH-1 downto 0);
                --    ram_cmd_if_rd.busy      <= 'Z'; --: std_logic;  

                --    ram_cmd_if_wr.request   <= '0'; --: std_logic;
                --    ram_cmd_if_wr.granted   <= 'Z'; --: std_logic;
                --    ram_cmd_if_wr.cmd_valid <= '0'; --: std_logic;
                --    ram_cmd_if_wr.addr      <= (others=>'0'); --: std_logic_vector(C_ADDRESS_WIDTH-1 downto 0);
                --    ram_cmd_if_wr.busy      <= 'Z'; --: std_logic;  

                --    WAIT UNTIL reset = '0';
                --    WAIT UNTIL rising_edge(clk);

                --    for ii in 0 to eth_packet_length_array_t'length - 1 loop
                --        eth_vector_id(ii)       <= ii+1;
                --        eth_vector_length(ii) <= 1044;
                --    end loop;

                --    for tt in 0 to 15 loop
                --    wait for 90 ns;
                --    WAIT UNTIL rising_edge(clk);
                --    eth_vector_length(0) <= 1044;
                --    eth_vector_id(0)     <= tt+1;
                --    start_stimuli <= '1';
                --    WAIT UNTIL rising_edge(clk);
                --    start_stimuli <= '0';
                --    WAIT UNTIL rising_edge(clk);
                --    WAIT UNTIL rising_edge(clk);

                --    -- Insert RAM command for writting a frame in RAM:
                --    -- Wait until frame is loaded in fifo:
                --    wait until axis_in.tlast = '1' and axis_in.tvalid = '1';
                --    wait until rising_edge(clk);
                --    ram_cmd_if_wr.request <= '1';
                --    -- wait until access is granted:
                --    wait until ram_cmd_if_wr.granted = '1';
                --    wait until rising_edge(clk);
                --    -- send write command:
                --    ram_cmd_if_wr.cmd_valid <= '1';
                --    ram_cmd_if_wr.addr      <= std_logic_vector(to_unsigned(tt, C_ADDRESS_WIDTH));
                --    wait until rising_edge(clk);
                --    ram_cmd_if_wr.cmd_valid <= '0';
                --    ram_cmd_if_wr.addr      <= (others=>'0');
                --    --wait until ram_cmd_if_wr.busy = '1';
                --    wait until rising_edge(clk);
                --    wait until rising_edge(clk);
                --    ram_cmd_if_wr.request <= '0';

                --    --Wait until WR process is finished:
                --    wait until ram_cmd_if_wr.busy = '0';
                --    wait until rising_edge(clk);
                --    wait until rising_edge(clk);
                --    wait for 300 ns;
                --    wait until rising_edge(clk);

                --    ram_cmd_if_rd.request <= '1';
                --    --wait until access is granted:
                --    wait until ram_cmd_if_rd.granted = '1';
                --    wait until rising_edge(clk);
                --    -- send write command:
                --    back_pressure <= '0';
                --    ram_cmd_if_rd.cmd_valid <= '1';
                --    ram_cmd_if_rd.addr      <= std_logic_vector(to_unsigned(tt, C_ADDRESS_WIDTH));
                --    wait until rising_edge(clk);
                --    ram_cmd_if_rd.cmd_valid <= '0';
                --    ram_cmd_if_rd.addr      <= (others=>'0');
                --    --wait until ram_cmd_if_wr.busy = '1';
                --    wait until rising_edge(clk);
                --    wait until rising_edge(clk);
                --    ram_cmd_if_rd.request <= '0';
                --    --wait for 5 us; 
                --    wait for 500 ns;
                --    --back_pressure <= '1';
                --     --Wait until RD process is finished:
                --    --wait until ram_cmd_if_rd.busy = '0';
                --    wait until rising_edge(clk);
                --    wait until rising_edge(clk);
                --    wait for 300 ns;
                --    wait until rising_edge(clk);

                ----           wait for 200 us;

                --    wait until payload_valid = '1';
                --    v_payload := payload;

                --    -- Check payload data received:
                --    payload_test_read_payload_data(
                --        v_payload,  
                --        pk_offset, 
                --        v_packet, 
                --        pk_size, 
                --        v_eth_packet_id, 
                --        v_eth_packet_length, 
                --        eth_vector_pointer,
                --        split,
                --        payload_id_last,
                --        v_error);
                --    check(v_error = '0', "Payload health check failed.");

                --    check(v_eth_packet_length(0) = eth_vector_length(0), "Wrong Ethernet packet Length detected");
                --    check(v_eth_packet_id(tt) = eth_vector_id(0), "Wrong Ethernet packet ID");
                --    check_false(v_eth_packet_length(tt) < 64, "Ethernet packet length can not be < 64.");
                --    wait for 5 us;  
                --end loop;
                --    wait for 50 us;  

                --    info("Test finished");

                --        ----------------------------------------------------------------------
                --        -- TEST CASE DESCRIPTION:
                --        -- check that packet header is working with all alignment in the payload
                --        -- Expected Result: 
                --        ----------------------------------------------------------------------
                --ELSIF run("frame_burst_sequence_test") THEN
                --    info("--------------------------------------------------------------------------------");
                --    info("TEST CASE: frame_burst_sequence_test");
                --    info("--------------------------------------------------------------------------------");
                --    start_stimuli <= '0';
                --    stimuli_length <= 1;
                --    seqnum_error_injection <= '0';
                --    back_pressure <= '0';
                --    frame_type_error_injection <= '0';
                --    tx_fn_error_injection <= '0';
                --    is_idle <= '0';     

                --    ram_cmd_if_rd.request   <= '0'; --: std_logic;
                --    ram_cmd_if_rd.granted   <= 'Z'; --: std_logic;
                --    ram_cmd_if_rd.cmd_valid <= '0'; --: std_logic;
                --    ram_cmd_if_rd.addr      <= (others=>'0'); --: std_logic_vector(C_ADDRESS_WIDTH-1 downto 0);
                --    ram_cmd_if_rd.busy      <= 'Z'; --: std_logic;  

                --    ram_cmd_if_wr.request   <= '0'; --: std_logic;
                --    ram_cmd_if_wr.granted   <= 'Z'; --: std_logic;
                --    ram_cmd_if_wr.cmd_valid <= '0'; --: std_logic;
                --    ram_cmd_if_wr.addr      <= (others=>'0'); --: std_logic_vector(C_ADDRESS_WIDTH-1 downto 0);
                --    ram_cmd_if_wr.busy      <= 'Z'; --: std_logic;  

                --    WAIT UNTIL reset = '0';
                --    WAIT UNTIL rising_edge(clk);

                --    for ii in 0 to eth_packet_length_array_t'length - 1 loop
                --        eth_vector_id(ii)       <= ii+1;
                --        eth_vector_length(ii) <= 1044;
                --    end loop;

                --    for tt in 0 to 15 loop
                --    wait for 90 ns;
                --    WAIT UNTIL rising_edge(clk);
                --    eth_vector_length(0) <= 1044;
                --    eth_vector_id(0)     <= tt+1;
                --    start_stimuli <= '1';
                --    WAIT UNTIL rising_edge(clk);
                --    start_stimuli <= '0';
                --    WAIT UNTIL rising_edge(clk);
                --    WAIT UNTIL rising_edge(clk);

                --    -- Insert RAM command for writting a frame in RAM:
                --    -- Wait until frame is loaded in fifo:
                --    wait until axis_in.tlast = '1' and axis_in.tvalid = '1';
                --    wait until rising_edge(clk);
                --    ram_cmd_if_wr.request <= '1';
                --    -- wait until access is granted:
                --    wait until ram_cmd_if_wr.granted = '1';
                --    wait until rising_edge(clk);
                --    -- send write command:
                --    ram_cmd_if_wr.cmd_valid <= '1';
                --    ram_cmd_if_wr.addr      <= std_logic_vector(to_unsigned(tt, C_ADDRESS_WIDTH));
                --    wait until rising_edge(clk);
                --    ram_cmd_if_wr.cmd_valid <= '0';
                --    ram_cmd_if_wr.addr      <= (others=>'0');
                --    --wait until ram_cmd_if_wr.busy = '1';
                --    wait until rising_edge(clk);
                --    wait until rising_edge(clk);
                --    ram_cmd_if_wr.request <= '0';

                --    --Wait until WR process is finished:
                --    wait until ram_cmd_if_wr.busy = '0';
                --    wait until rising_edge(clk);
                --    wait until rising_edge(clk);
                --    wait for 5 us;
                --    wait until rising_edge(clk);
                --end loop;

                --    -- READING ALL FRAMES:     
                --for tt in 0 to 15 loop
                --    WAIT UNTIL rising_edge(clk);
                --    eth_vector_length(0) <= 1044;
                --    eth_vector_id(0)     <= tt+1;
                --    wait for 1 us;
                --    wait until rising_edge(clk);
                --    ram_cmd_if_rd.request <= '1';
                --    wait until rising_edge(clk);
                --    --wait until access is granted:
                --    wait until ram_cmd_if_rd.granted = '1';
                --    wait until rising_edge(clk);
                --    -- send write command:
                --    back_pressure <= '0';
                --    ram_cmd_if_rd.cmd_valid <= '1';
                --    ram_cmd_if_rd.addr      <= std_logic_vector(to_unsigned(tt, C_ADDRESS_WIDTH));
                --    wait until rising_edge(clk);
                --    ram_cmd_if_rd.cmd_valid <= '0';
                --    ram_cmd_if_rd.addr      <= (others=>'0');
                --    --wait until ram_cmd_if_wr.busy = '1';
                --    wait until rising_edge(clk);
                --    wait until rising_edge(clk);
                --    ram_cmd_if_rd.request <= '0';
                --    --wait for 5 us; 
                --    wait for 500 ns;
                --    --back_pressure <= '1';
                --     --Wait until RD process is finished:
                --    --wait until ram_cmd_if_rd.busy = '0';
                --    wait until rising_edge(clk);
                --    wait until rising_edge(clk);
                --    wait for 300 ns;
                --    wait until rising_edge(clk);

                ----           wait for 200 us;

                --    wait until payload_valid = '1';
                --    v_payload := payload;

                --    -- Check payload data received:
                --    payload_test_read_payload_data(
                --        v_payload,  
                --        pk_offset, 
                --        v_packet, 
                --        pk_size, 
                --        v_eth_packet_id, 
                --        v_eth_packet_length, 
                --        eth_vector_pointer,
                --        split,
                --        payload_id_last,
                --        v_error);
                --    check(v_error = '0', "Payload health check failed.");

                --    check(v_eth_packet_length(0) = eth_vector_length(0), "Wrong Ethernet packet Length detected");
                --    check(v_eth_packet_id(tt) = eth_vector_id(0), "Wrong Ethernet packet ID");
                --    check_false(v_eth_packet_length(tt) < 64, "Ethernet packet length can not be < 64.");
                --    wait for 5 us;  
                --end loop;
                --    wait for 50 us;  

                --    info("Test finished");

                --        ----------------------------------------------------------------------
                --        -- TEST CASE DESCRIPTION:
                --        -- Random stress test
                --        -- Expected Result: 
                --        ----------------------------------------------------------------------
                --        ELSIF run("random_stress_test_1") THEN
                --            info("--------------------------------------------------------------------------------");
                --            info("TEST CASE: random_stress_test_1");
                --            info("--------------------------------------------------------------------------------");
                --            WAIT UNTIL reset = '0';
                --            start_stimuli <= '0';
                --            seqnum_error_injection <= '0';
                --            back_pressure <= '0';
                --            frame_type_error_injection <= '0';
                --            tx_fn_error_injection <= '0';
                --            is_idle <= '0';
                --            
                --            eth_vector_pointer := 0;
                --            WAIT UNTIL rising_edge(clk);
                --            v_tx_fn_ref := 0;
                --            stimuli_length <= 50; --eth_packet_length_array_t'length;
                --            --RV.InitSeed(now / 1 ns);
                --            RV.InitSeed(752);
                --            eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                --            eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                --
                --            for ii in 0 to 50 - 1 loop
                --                eth_vector_id(ii)       <= ii+1;
                --                eth_vector_length(ii) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                --                --info("Len=" & to_string(eth_vector_length(ii)));
                --            end loop;
                --
                --            wait for 90 ns;
                --            WAIT UNTIL rising_edge(clk);
                --            start_stimuli <= '1';
                --            WAIT UNTIL rising_edge(clk);
                --            start_stimuli <= '0';
                --            WAIT UNTIL rising_edge(clk);
                --            WAIT UNTIL rising_edge(clk);
                --
                --            --wait for 100 us;
                --            for ii in 0 to stimuli_length-1 loop
                --                wait until eth_packet_valid = '1';
                --                check(eth_packet_error = '0', "Ethernet packet error!");
                --                check(eth_packet_length = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                --                check(eth_packet_id = eth_vector_id(ii), "Wrong Ethernet packet ID");
                --                check_false(eth_packet_length < 64, "Ethernet packet length can not be < 64.");
                --                
                --            end loop;
                --            wait for 20 us;
                --            check(v_total_packet_counter = stimuli_length, "total_packet_counter error!"); 
                --            check(v_total_packet_splitted_counter > 0, "total_packet_splitted_counter error!"); 
                --            check(v_total_packet_merged_counter = v_total_packet_splitted_counter, "total_packet_merged_counter error!"); 
                --            check(v_total_payload_error_counter = 0, "total_payload_error_counter error!"); 
                --            check(v_total_packet_error_counter = 0, "total_packet_error_counter error!"); 
                --            check(v_watchdog_reset_counter = 0, "watchdog_reset_counter error!"); 
                --            check(v_packet_filter_cnt_in = stimuli_length, "packet_filter_cnt_in error!"); 
                --            check(v_packet_filter_cnt_out = stimuli_length, "packet_filter_cnt_out error!"); 
                --
                --            info("Test finished");

            END IF;                     -- for test_suite
        END LOOP test_cases_loop;

        WAIT FOR 20 ns;
        test_runner_cleanup(runner);
    end process;

    inst_payload_transmiter : entity sda_test_lib.payload_transmiter
        port map(
            -- Global Signal Interface
            clk                        => clk, --: in std_logic;

            -- Transaction Interfaces
            tx_fn_init                 => (others => '0'),
            axis_payload_if_m            => axis_payload_if_in_m, --: inout t_axis_payload_if;
            axis_payload_if_s            => axis_payload_if_in_s, --: inout t_axis_payload_if;
            eth_vector_id              => eth_vector_id, --: in eth_packet_id_array_t;
            eth_vector_length          => eth_vector_length, --stimuli_length_array, --: in eth_packet_length_array_t;
            stimuli_length             => stimuli_length, --: in natural;
            start_stimuli              => start_stimuli, --: in std_logic;
            is_idle                    => is_idle, --: in std_logic;
            finished                   => pt_finished, --: out std_logic;
            tx_fn_error_injection      => tx_fn_error_injection, --: in std_logic;
            frame_type_error_injection => frame_type_error_injection, --: in std_logic
            seqnum_error_injection     => seqnum_error_injection --: in std_logic
        );

    -- Little adjustment for different buses. We just want the Axi-s
    -- axis_in.tstart      <= axis_payload_if_in.axis_if.tstart;
    -- axis_in.tlast       <= axis_payload_if_in.axis_if.tlast;
    -- -- axis_in.tdata       <= axis_payload_if_in.axis_if.tdata;
    -- axis_in.tuser       <= axis_payload_if_in.axis_if.tuser;
    -- axis_in.tvalid       <= axis_payload_if_in.axis_if.tvalid;
    -- axis_payload_if_in.axis_if.tready <= axis_in.tready;

    -- axis_payload_if_in.tx_fn       <= (others=>'Z');
    -- axis_payload_if_in.tx_num      <= (others=>'Z');
    -- axis_payload_if_in.frame_type  <= (others=>'Z');

    DUT : entity arq_lib.arq_ram_controller_top
        generic map(
            --G_INTERNAL_RAM : boolean := true;
            G_INTERNAL_MEMORY_SIZE => 16, --: natural := 16; -- Size in Frames
            G_CACHE_SIZE           => 16 --: natural := 16
        )
        port map(
            clk                 => clk, --: in std_logic;
            rst                 => reset, --: in std_logic;

            --Input interfaces
            req_resend_if_m       => req_resend_if_m, --: inout t_request_resend_if;
            req_resend_if_s       => req_resend_if_s, --: inout t_request_resend_if;
            req_insertion_if_m    => req_insertion_if_m, --: inout t_request_insertion_if;
            req_insertion_if_s    => req_insertion_if_s, --: inout t_request_insertion_if;
            req_cache_if_m        => req_cache_if_m, --: inout t_request_cache_if;
            req_cache_if_s        => req_cache_if_s, --: inout t_request_cache_if;
            axis_payload_if_in_m  => axis_payload_if_in_m, --  : inout t_axis_payload_if;
            axis_payload_if_in_s  => axis_payload_if_in_s, --  : inout t_axis_payload_if;
            insert_fifo_empty   => insert_fifo_empty, --: out std_logic; 
            arq_config_if       => arq_config_if,
            --Output interfaces
            axis_payload_if_out_m => axis_payload_if_out_m, --: inout t_axis_payload_if
            axis_payload_if_out_s => axis_payload_if_out_s, --: inout t_axis_payload_if
            --arq_ram_diagnostics_if : inout t_arq_control_tx_diagnostics_if
            clear_diag          => clear_diag,
            diag_if             => diag_if --t_arq_ram_controller_diag_if  --arq_ram_diagnostics_if : inout t_arq_control_tx_diagnostics_if
        );
    -- Little adjustment for different buses. We just want the Axi-s
    -- axis_payload_if_out.axis_if.tstart <= axis_out.tstart;
    -- axis_payload_if_out.axis_if.tvalid <= axis_out.tvalid;
    -- axis_payload_if_out.axis_if.tlast <= axis_out.tlast;
    -- axis_payload_if_out.axis_if.tdata <= axis_out.tdata;
    -- axis_payload_if_out.axis_if.tuser <= axis_out.tuser;
    -- axis_out.tready <= axis_payload_if_out.axis_if.tready;

    -- axis_payload_if_out.tx_fn       <= (others=>'0');
    -- axis_payload_if_out.tx_num      <= (others=>'0');
    -- axis_payload_if_out.frame_type  <= (others=>'0');

    inst_payload_receiver : entity sda_test_lib.payload_receiver
        port map(
            -- Global Signal Interface
            clk             => clk,     --: in std_logic;

            -- Transaction Interfaces
            axis_payload_if_m => axis_payload_if_out_m, --: inout t_axis_payload_if;
            axis_payload_if_s => axis_payload_if_out_s, --: inout t_axis_payload_if;
            back_pressure   => back_pressure, --: in std_logic;
            tx_fn_out       => tx_fn_out, --: in std_logic_vector(15 downto 0);
            frame_type_out  => frame_type_out, --: in std_logic_vector(1 downto 0);
            tx_num_out      => tx_num_out, --: in std_logic_vector(1 downto 0);
            payload         => payload, --: out payload_array_t;
            payload_valid   => payload_valid, --: out std_logic;
            error           => error,   --: out std_logic;
            payload_length  => payload_length, --: out natural;
            seqnum          => seqnum   --: out natural;
        );

    test_runner_watchdog(runner, 1500 us);
end architecture;
