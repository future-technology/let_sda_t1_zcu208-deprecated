-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: tb_arq_req_control_tx
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

library osvvm;
use osvvm.RandomPkg.all;
use osvvm.CoveragePkg.all;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

library arq_lib;

--library sda_test_lib;
--use sda_test_lib.frame_receiver;

entity tb_arq_timekeeper_tx_wrapper is
    generic(runner_cfg : string := runner_cfg_default);
end entity;

architecture tb of tb_arq_timekeeper_tx_wrapper is

    constant C_CLK_PERIOD : time      := 10 ns;
    signal clk            : std_logic := '0';
    signal reset          : std_logic := '1';
    signal error          : std_logic;
    signal send           : std_logic := '0';
    signal clear_diag     : std_logic := '0';

    --Input interfaces:
    signal arq_config_if        : t_arq_config_if;
    signal ack_data_if_m          : t_ack_data_if_m;
    signal ack_data_if_s          : t_ack_data_if_s;
    signal req_insertion_if_m     : t_request_insertion_if_m;
    signal req_insertion_if_s     : t_request_insertion_if_s;
    --Output interfaces;
    signal req_resend_if_m        : t_request_resend_if_m;
    signal req_resend_if_s        : t_request_resend_if_s;
    signal req_cache_if_m         : t_request_cache_if_m;
    signal req_cache_if_s         : t_request_cache_if_s;
    signal metadata_retrieve_if : t_metadata_retrieve_if;
    signal arq_diag_if          : t_arq_control_tx_diagnostics_if;
    signal arq_hw_config        : t_arq_hw_config;

    -- test signals:
    type t_request_resend_if_buffer is array (0 to C_ARQ_MAX_RETX - 1) of t_request_resend_if_m;
    type t_request_resend_if_array is array (0 to 500) of t_request_resend_if_buffer;
    signal request_resend_if_buffer : t_request_resend_if_buffer;
    signal request_resend_if_array  : t_request_resend_if_array;
    type t_req_resend_index_array is array (0 to 500) of natural;
    signal req_resend_index_array   : t_req_resend_index_array := (others => 0);
    signal test_end                 : std_logic;
    -- test signals:
    type t_request_cache_if_buffer is array (0 to C_ARQ_MAX_RETX - 1) of t_request_cache_if_m;
    type t_request_cache_if_array is array (0 to 500) of t_request_cache_if_buffer;
    signal request_cache_if_buffer  : t_request_cache_if_buffer;
    signal request_cache_if_array   : t_request_cache_if_array;
    type t_req_cache_index_array is array (0 to 500) of natural;
    signal req_cache_index_array    : t_req_cache_index_array  := (others => 0);

    signal end_stimuli : std_logic;
begin

    --------------------------------------------------------------------------
    -- CLOCK AND RESET.
    --------------------------------------------------------------------------
    clk   <= NOT clk after C_CLK_PERIOD / 2;
    reset <= '0' after 5 * (C_CLK_PERIOD);

    test_runner : process
        variable tx_fn              : std_logic_vector(15 downto 0); -- Sequence number of this Tx Frame
        variable frame_type         : std_logic_vector(1 downto 0); -- Frame Type
        variable tx_num             : std_logic_vector(2 downto 0); -- Transmission atempt
        variable stimuli_input_size : natural;
        variable ack_stimulus_size  : natural;
    begin
        test_runner_setup(runner, runner_cfg);
        -- enable logging for passing check
        --show(get_logger(default_checker), display_handler, pass); 
        --show(get_logger(RED_CHECKER), display_handler, pass); 
        --packet_len <= 100;
        test_cases_loop : WHILE test_suite LOOP
            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- single frame check.
            -- Expected Result: 
            ----------------------------------------------------------------------
            IF run("single_frame_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: single_frame_check");
                info("--------------------------------------------------------------------------------");

                end_stimuli                       <= '0';
                arq_config_if.holdoff_nframes     <= std_logic_vector(to_unsigned(256, 12));
                arq_config_if.max_retx            <= std_logic_vector(to_unsigned(5, 3));
                arq_config_if.holdoff_time        <= std_logic_vector(to_unsigned(50, C_TIMESTAMP_WIDTH));
                arq_config_if.core_counter_period <= std_logic_vector(to_unsigned(20 - 1, 12)); --20*5=100 tenths of us -> 1 us
                arq_config_if.cache_trigger_time  <= std_logic_vector(to_unsigned(30, C_TIMESTAMP_WIDTH)); -- not used
                ack_data_if_m.ack                   <= '1';
                ack_data_if_m.ack_start_fn          <= (others => '0');
                ack_data_if_m.ack_span              <= (others => '0');
                ack_data_if_m.ack_valid             <= '0';
                req_cache_if_s.ready                <= '1';

                req_insertion_if_m.tx_fn <= x"0007";
                req_insertion_if_m.valid <= '0';
                --req_insertion_if.ready <= 'Z';

                clear_diag <= '0';

                req_resend_if_s.ready <= '1';

                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                wait for 800 ns;
                wait until rising_edge(clk);

                wait for 5 us;
                wait until rising_edge(clk);
                --for ii in 0 to 0 loop
                req_insertion_if_m.valid <= '1';
                req_insertion_if_m.tx_fn <= std_logic_vector(to_unsigned(7, 16));
                --wait until req_insertion_if.ready = '1';
                wait until rising_edge(clk);
                req_insertion_if_m.valid <= '0';
                wait until metadata_retrieve_if.valid = '1';
                check(metadata_retrieve_if.tx_fn = req_insertion_if_m.tx_fn, "Retrieve metadata interface ERROR!");
                --wait for 5 us;
                wait until rising_edge(clk);
                --end loop;
                req_insertion_if_m.valid <= '0';

                wait for 100 us;
                --we are expecting max_retx number of resends:
                for jj in 1 to to_integer(unsigned(arq_config_if.max_retx)) loop
                    check(request_resend_if_array(7)(jj - 1).tx_fn = x"0007", "TX_FN error");
                    check(request_resend_if_array(7)(jj - 1).tx_num = std_logic_vector(to_unsigned(jj, C_TX_NUM_WIDTH)), "TX_NUM error");
                    check(request_resend_if_array(7)(jj - 1).addr = std_logic_vector(to_unsigned(0, C_ADDRESS_WIDTH)), "ADDRESS error");
                end loop;

                info("===== TEST CASE FINISHED =====");

            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- single frame check.
            -- Expected Result: 
            ----------------------------------------------------------------------
            ELSIF run("multiple_frame_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: multiple_frame_check");
                info("--------------------------------------------------------------------------------");

                stimuli_input_size                := 50;
                end_stimuli                       <= '0';
                arq_config_if.holdoff_nframes     <= std_logic_vector(to_unsigned(256, 12));
                arq_config_if.max_retx            <= std_logic_vector(to_unsigned(5, 3));
                arq_config_if.holdoff_time        <= std_logic_vector(to_unsigned(50, C_TIMESTAMP_WIDTH));
                arq_config_if.core_counter_period <= std_logic_vector(to_unsigned(20 - 1, 12)); --20*5=100 tenths of us -> 1 us
                arq_config_if.cache_trigger_time  <= std_logic_vector(to_unsigned(30, C_TIMESTAMP_WIDTH)); -- not used
                arq_config_if.cache_enable        <= '1'; -- not used
                ack_data_if_m.ack                   <= '1';
                ack_data_if_m.ack_start_fn          <= (others => '0');
                ack_data_if_m.ack_span              <= (others => '0');
                ack_data_if_m.ack_valid             <= '0';
                --ack_data_if.rd_ena                <= 'Z';
                req_cache_if_s.ready                <= '1';

                req_insertion_if_m.tx_fn <= x"0007";
                req_insertion_if_m.valid <= '0';
                --req_insertion_if.ready <= 'Z';

                clear_diag <= '0';

                req_resend_if_s.ready <= '1';

                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                wait for 800 ns;
                wait until rising_edge(clk);

                wait for 5 us;
                wait until rising_edge(clk);
                for ii in 0 to stimuli_input_size loop
                    loop
                        if req_insertion_if_s.ready = '1' then
                            exit;
                        end if;
                    end loop;
                    wait until rising_edge(clk);
                    req_insertion_if_m.valid      <= '1';
                    req_insertion_if_m.tx_fn      <= std_logic_vector(to_unsigned(ii, 16));
                    req_insertion_if_m.frame_type <= C_FRAME_DATA;
                    wait until rising_edge(clk);
                    req_insertion_if_m.valid      <= '0';
                    wait until metadata_retrieve_if.valid = '1';
                    check(metadata_retrieve_if.tx_fn = req_insertion_if_m.tx_fn, "Retrieve metadata interface ERROR!");
                    wait for 5 us;
                    wait until rising_edge(clk);
                end loop;
                req_insertion_if_m.valid <= '0';

                wait for 200 us;
                --we are expecting max_retx number of resends:
                for ii in 0 to stimuli_input_size - 1 loop
                    for jj in 1 to to_integer(unsigned(arq_config_if.max_retx)) loop
                        check(request_resend_if_array(ii)(jj - 1).tx_fn = std_logic_vector(to_unsigned(ii, C_TX_FN_WIDTH)), "TX_FN error");
                        check(request_resend_if_array(ii)(jj - 1).tx_num = std_logic_vector(to_unsigned(jj, C_TX_NUM_WIDTH)), "TX_NUM error");
                        check(request_resend_if_array(ii)(jj - 1).addr = std_logic_vector(to_unsigned(ii, C_ADDRESS_WIDTH)), "ADDRESS error");
                        check(request_cache_if_array(ii)(jj - 1).tx_fn = std_logic_vector(to_unsigned(ii, C_TX_FN_WIDTH)), "TX_FN error");
                        check(request_cache_if_array(ii)(jj - 1).addr = std_logic_vector(to_unsigned(ii, C_ADDRESS_WIDTH)), "ADDRESS error");
                    end loop;
                end loop;

                --wait for 50 us;
                info("===== TEST CASE FINISHED =====");

            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- single frame check.
            -- Expected Result: 
            ----------------------------------------------------------------------
            ELSIF run("overflow_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: overflow_check");
                info("--------------------------------------------------------------------------------");

                stimuli_input_size                := 180;
                end_stimuli                       <= '0';
                arq_config_if.holdoff_nframes     <= std_logic_vector(to_unsigned(8, 12));
                arq_config_if.max_retx            <= std_logic_vector(to_unsigned(5, 3));
                arq_config_if.holdoff_time        <= std_logic_vector(to_unsigned(900, C_TIMESTAMP_WIDTH));
                arq_config_if.core_counter_period <= std_logic_vector(to_unsigned(20 - 1, 12)); --20*5=100 tenths of us -> 1 us
                arq_config_if.cache_trigger_time  <= std_logic_vector(to_unsigned(800, C_TIMESTAMP_WIDTH)); -- not used
                ack_data_if_m.ack                   <= '1';
                ack_data_if_m.ack_start_fn          <= (others => '0');
                ack_data_if_m.ack_span              <= (others => '0');
                ack_data_if_m.ack_valid             <= '0';
                --ack_data_if_s.rd_ena                <= 'Z';
                req_cache_if_s.ready                <= '1';

                req_insertion_if_m.tx_fn <= x"0007";
                req_insertion_if_m.valid <= '0';
                --req_insertion_if.ready <= 'Z';

                clear_diag <= '0';

                req_resend_if_s.ready <= '1';

                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                wait for 800 ns;
                wait until rising_edge(clk);

                wait for 5 us;
                wait until rising_edge(clk);
                for ii in 0 to stimuli_input_size loop
                    loop
                        if req_insertion_if_s.ready = '1' then
                            exit;
                        end if;
                    end loop;
                    wait until rising_edge(clk);
                    req_insertion_if_m.valid <= '1';
                    req_insertion_if_m.tx_fn <= std_logic_vector(to_unsigned(ii, 16));
                    wait until rising_edge(clk);
                    req_insertion_if_m.valid <= '0';
                    wait for 4 us;
                    wait until rising_edge(clk);
                end loop;
                req_insertion_if_m.valid <= '0';

                wait for 1200 us;
                --we are expecting max_retx number of resends:
                for ii in 0 to stimuli_input_size - 1 loop
                    for jj in 1 to to_integer(unsigned(arq_config_if.max_retx)) loop
                        check(request_resend_if_array(ii)(jj - 1).tx_fn = std_logic_vector(to_unsigned(ii, C_TX_FN_WIDTH)), "TX_FN error");
                        check(request_resend_if_array(ii)(jj - 1).tx_num = std_logic_vector(to_unsigned(jj, C_TX_NUM_WIDTH)), "TX_NUM error");
                        --check(request_resend_if_array(ii)(jj-1).addr = std_logic_vector(to_unsigned(ii, C_ADDRESS_WIDTH)), "ADDRESS error");
                    end loop;
                end loop;

                --wait for 50 us;
                info("===== TEST CASE FINISHED =====");

            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- single frame check.
            -- Expected Result: 
            ----------------------------------------------------------------------
            ELSIF run("single_frame_ack_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: single_frame_ack_check");
                info("--------------------------------------------------------------------------------");

                end_stimuli                       <= '0';
                arq_config_if.holdoff_nframes     <= std_logic_vector(to_unsigned(256, 12));
                arq_config_if.max_retx            <= std_logic_vector(to_unsigned(5, 3));
                arq_config_if.holdoff_time        <= std_logic_vector(to_unsigned(50, C_TIMESTAMP_WIDTH));
                arq_config_if.core_counter_period <= std_logic_vector(to_unsigned(20 - 1, 12)); --20*5=100 tenths of us -> 1 us
                arq_config_if.cache_trigger_time  <= std_logic_vector(to_unsigned(30, C_TIMESTAMP_WIDTH)); -- not used
                ack_data_if_m.ack                   <= '1';
                ack_data_if_m.ack_start_fn          <= (others => '0');
                ack_data_if_m.ack_span              <= (others => '0');
                ack_data_if_m.ack_valid             <= '0';
                --ack_data_if.rd_ena                <= 'Z';
                req_cache_if_s.ready                <= '1';

                req_insertion_if_m.tx_fn <= x"0007";
                req_insertion_if_m.valid <= '0';
                --req_insertion_if.ready <= 'Z';

                clear_diag <= '0';

                req_resend_if_s.ready <= '1';

                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                wait for 800 ns;
                wait until rising_edge(clk);

                wait for 5 us;
                wait until rising_edge(clk);
                --for ii in 0 to 0 loop
                req_insertion_if_m.valid <= '1';
                req_insertion_if_m.tx_fn <= std_logic_vector(to_unsigned(7, 16));
                --wait until req_insertion_if.ready = '1';
                wait until rising_edge(clk);
                req_insertion_if_m.valid <= '0';
                --wait for 5 us;
                wait until rising_edge(clk);
                --end loop;
                req_insertion_if_m.valid <= '0';

                wait for 1 us;
                wait until rising_edge(clk);
                ack_data_if_m.ack_start_fn <= x"0007";
                ack_data_if_m.ack_valid    <= '1';
                ack_data_if_m.ack          <= '1';
                --wait until ack_data_if.rd_ena = '1';
                wait until rising_edge(clk);
                ack_data_if_m.ack_valid    <= '0';

                wait for 20 us;
                --we are expecting max_retx number of resends:
                for jj in 1 to to_integer(unsigned(arq_config_if.max_retx)) loop
                    check_false(request_resend_if_array(7)(jj - 1).tx_fn = x"0007", "TX_FN error");
                    check_false(request_resend_if_array(7)(jj - 1).tx_num = std_logic_vector(to_unsigned(jj, C_TX_NUM_WIDTH)), "TX_NUM error");
                    check_false(request_resend_if_array(7)(jj - 1).addr = std_logic_vector(to_unsigned(0, C_ADDRESS_WIDTH)), "ADDRESS error");
                end loop;

                info("===== TEST CASE FINISHED =====");

            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- single frame check.
            -- Expected Result: 
            ----------------------------------------------------------------------
            ELSIF run("single_frame_nack_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: single_frame_nack_check");
                info("--------------------------------------------------------------------------------");

                end_stimuli                       <= '0';
                arq_config_if.holdoff_nframes     <= std_logic_vector(to_unsigned(256, 12));
                arq_config_if.max_retx            <= std_logic_vector(to_unsigned(5, 3));
                arq_config_if.holdoff_time        <= std_logic_vector(to_unsigned(50, C_TIMESTAMP_WIDTH));
                arq_config_if.core_counter_period <= std_logic_vector(to_unsigned(20 - 1, 12)); --20*5=100 tenths of us -> 1 us
                arq_config_if.cache_trigger_time  <= std_logic_vector(to_unsigned(30, C_TIMESTAMP_WIDTH)); -- not used
                ack_data_if_m.ack                   <= '1';
                ack_data_if_m.ack_start_fn          <= (others => '0');
                ack_data_if_m.ack_span              <= (others => '0');
                ack_data_if_m.ack_valid             <= '0';
                --ack_data_if_s.rd_ena                <= 'Z';
                req_cache_if_s.ready                <= '1';

                req_insertion_if_m.tx_fn <= x"0007";
                req_insertion_if_m.valid <= '0';
                --req_insertion_if.ready <= 'Z';

                clear_diag <= '0';

                req_resend_if_s.ready <= '1';

                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                wait for 800 ns;
                wait until rising_edge(clk);

                wait for 5 us;
                wait until rising_edge(clk);
                --for ii in 0 to 0 loop
                req_insertion_if_m.valid <= '1';
                req_insertion_if_m.tx_fn <= std_logic_vector(to_unsigned(7, 16));
                --wait until req_insertion_if.ready = '1';
                wait until rising_edge(clk);
                req_insertion_if_m.valid <= '0';
                --wait for 5 us;
                wait until rising_edge(clk);
                --end loop;
                req_insertion_if_m.valid <= '0';

                wait for 1 us;
                wait until rising_edge(clk);
                ack_data_if_m.ack_start_fn <= x"0007";
                ack_data_if_m.ack_valid    <= '1';
                ack_data_if_m.ack          <= '0';
                --wait until ack_data_if.rd_ena = '1';
                wait until rising_edge(clk);
                ack_data_if_m.ack_valid    <= '0';

                wait for 100 us;
                --we are expecting max_retx number of resends:
                for jj in 1 to to_integer(unsigned(arq_config_if.max_retx)) loop
                    check(request_resend_if_array(7)(jj - 1).tx_fn = x"0007", "TX_FN error");
                    check(request_resend_if_array(7)(jj - 1).tx_num = std_logic_vector(to_unsigned(jj, C_TX_NUM_WIDTH)), "TX_NUM error");
                    check(request_resend_if_array(7)(jj - 1).addr = std_logic_vector(to_unsigned(0, C_ADDRESS_WIDTH)), "ADDRESS error");
                end loop;

                info("===== TEST CASE FINISHED =====");

            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- single frame check.
            -- Expected Result: 
            ----------------------------------------------------------------------
            ELSIF run("multiple_frame_ack_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: multiple_frame_ack_check");
                info("--------------------------------------------------------------------------------");

                stimuli_input_size                := 5;
                ack_stimulus_size                 := 2;
                end_stimuli                       <= '0';
                arq_config_if.holdoff_nframes     <= std_logic_vector(to_unsigned(256, 12));
                arq_config_if.max_retx            <= std_logic_vector(to_unsigned(5, 3));
                arq_config_if.holdoff_time        <= std_logic_vector(to_unsigned(400, C_TIMESTAMP_WIDTH));
                arq_config_if.core_counter_period <= std_logic_vector(to_unsigned(20 - 1, 12)); --20*5=100 tenths of us -> 1 us
                arq_config_if.cache_trigger_time  <= std_logic_vector(to_unsigned(300, C_TIMESTAMP_WIDTH)); -- not used
                ack_data_if_m.ack                   <= '1';
                ack_data_if_m.ack_start_fn          <= (others => '0');
                ack_data_if_m.ack_span              <= (others => '0');
                ack_data_if_m.ack_valid             <= '0';
                --ack_data_if_s.rd_ena                <= 'Z';
                req_cache_if_s.ready                <= '1';

                req_insertion_if_m.tx_fn <= x"0007";
                req_insertion_if_m.valid <= '0';
                --req_insertion_if.ready <= 'Z';

                clear_diag <= '0';

                req_resend_if_s.ready <= '1';

                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                wait for 800 ns;
                wait until rising_edge(clk);

                wait for 5 us;

                wait until rising_edge(clk);
                for ii in 0 to stimuli_input_size loop
                    loop
                        if req_insertion_if_s.ready = '1' then
                            exit;
                        end if;
                    end loop;
                    wait until rising_edge(clk);
                    req_insertion_if_m.valid <= '1';
                    req_insertion_if_m.tx_fn <= std_logic_vector(to_unsigned(ii, 16));
                    wait until rising_edge(clk);
                    req_insertion_if_m.valid <= '0';
                    wait for 5 us;
                    wait until rising_edge(clk);
                end loop;
                req_insertion_if_m.valid <= '0';
                wait for 100 ns;
                for kk in 0 to ack_stimulus_size - 1 loop
                    wait until rising_edge(clk);
                    ack_data_if_m.ack_start_fn <= std_logic_vector(to_unsigned(kk, 16));
                    ack_data_if_m.ack          <= '1';
                    ack_data_if_m.ack_valid    <= '1';
                    --wait until ack_data_if.rd_ena = '1';
                    wait until rising_edge(clk);
                    ack_data_if_m.ack_valid    <= '0';
                    wait for 5 us;
                end loop;

                wait for 600 us;
                --we are expecting max_retx number of resends:
                for ii in 0 to stimuli_input_size - 1 loop
                    for jj in 1 to to_integer(unsigned(arq_config_if.max_retx)) loop
                        if ii >= 0 and ii < ack_stimulus_size then
                            check_false(request_resend_if_array(ii)(jj - 1).tx_fn = std_logic_vector(to_unsigned(ii, C_TX_FN_WIDTH)), "TX_FN error");
                            check_false(request_resend_if_array(ii)(jj - 1).tx_num = std_logic_vector(to_unsigned(jj, C_TX_NUM_WIDTH)), "TX_NUM error");
                        --check_false(request_resend_if_array(ii)(jj-1).addr = std_logic_vector(to_unsigned(ii, C_ADDRESS_WIDTH)), "ADDRESS error");
                        else
                            check(request_resend_if_array(ii)(jj - 1).tx_fn = std_logic_vector(to_unsigned(ii, C_TX_FN_WIDTH)), "TX_FN error");
                            check(request_resend_if_array(ii)(jj - 1).tx_num = std_logic_vector(to_unsigned(jj, C_TX_NUM_WIDTH)), "TX_NUM error");
                            --check(request_resend_if_array(ii)(jj-1).addr = std_logic_vector(to_unsigned(ii, C_ADDRESS_WIDTH)), "ADDRESS error");
                        end if;
                    end loop;
                end loop;

                info("===== TEST CASE FINISHED =====");

            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- single frame check.
            -- Expected Result: 
            ----------------------------------------------------------------------
            ELSIF run("ack_timeout_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: ack_timeout_check");
                info("--------------------------------------------------------------------------------");

                stimuli_input_size                := 5;
                ack_stimulus_size                 := 2;
                end_stimuli                       <= '0';
                arq_config_if.holdoff_nframes     <= std_logic_vector(to_unsigned(256, 12));
                arq_config_if.max_retx            <= std_logic_vector(to_unsigned(5, 3));
                arq_config_if.holdoff_time        <= std_logic_vector(to_unsigned(400, C_TIMESTAMP_WIDTH));
                arq_config_if.core_counter_period <= std_logic_vector(to_unsigned(20 - 1, 12)); --20*5=100 tenths of us -> 1 us
                arq_config_if.cache_trigger_time  <= std_logic_vector(to_unsigned(200, C_TIMESTAMP_WIDTH)); -- not used
                ack_data_if_m.ack                   <= '1';
                ack_data_if_m.ack_start_fn          <= (others => '0');
                ack_data_if_m.ack_span              <= (others => '0');
                ack_data_if_m.ack_valid             <= '0';
                --ack_data_if_s.rd_ena                <= 'Z';
                req_cache_if_s.ready                <= '1';

                req_insertion_if_m.tx_fn <= x"0007";
                req_insertion_if_m.valid <= '0';
                --req_insertion_if.ready <= 'Z';

                clear_diag <= '0';

                req_resend_if_s.ready <= '1';

                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                wait for 800 ns;
                wait until rising_edge(clk);

                wait for 5 us;

                wait until rising_edge(clk);
                for ii in 0 to stimuli_input_size loop
                    loop
                        if req_insertion_if_s.ready = '1' then
                            exit;
                        end if;
                    end loop;
                    wait until rising_edge(clk);
                    req_insertion_if_m.valid <= '1';
                    req_insertion_if_m.tx_fn <= std_logic_vector(to_unsigned(ii, 16));
                    wait until rising_edge(clk);
                    req_insertion_if_m.valid <= '0';
                    wait for 5 us;
                    wait until rising_edge(clk);
                end loop;
                req_insertion_if_m.valid <= '0';
                wait for 100 ns;

                loop
                    if ack_data_if_s.rd_ena = '1' then
                        exit;
                    end if;
                end loop;
                --SENDING WRONG ACK TO TRIGGER TIMEOUT
                wait until rising_edge(clk);
                ack_data_if_m.ack_start_fn <= std_logic_vector(to_unsigned(777, 16)); -- wrong ACK
                ack_data_if_m.ack          <= '1';
                ack_data_if_m.ack_valid    <= '1';
                --wait until ack_data_if.rd_ena = '1';
                wait until rising_edge(clk);
                ack_data_if_m.ack_valid    <= '0';
                wait for 5 us;

                for kk in 0 to ack_stimulus_size - 1 loop
                    loop
                        if ack_data_if_s.rd_ena = '1' then
                            exit;
                        end if;
                    end loop;
                    wait until rising_edge(clk);
                    ack_data_if_m.ack_start_fn <= std_logic_vector(to_unsigned(kk, 16));
                    ack_data_if_m.ack          <= '1';
                    ack_data_if_m.ack_valid    <= '1';
                    --wait until ack_data_if.rd_ena = '1';
                    wait until rising_edge(clk);
                    ack_data_if_m.ack_valid    <= '0';
                    wait for 5 us;
                end loop;

                wait for 600 us;
                --we are expecting max_retx number of resends:
                for ii in 0 to stimuli_input_size - 1 loop
                    for jj in 1 to to_integer(unsigned(arq_config_if.max_retx)) loop
                        if ii >= 0 and ii < ack_stimulus_size then
                            check_false(request_resend_if_array(ii)(jj - 1).tx_fn = std_logic_vector(to_unsigned(ii, C_TX_FN_WIDTH)), "TX_FN error");
                            check_false(request_resend_if_array(ii)(jj - 1).tx_num = std_logic_vector(to_unsigned(jj, C_TX_NUM_WIDTH)), "TX_NUM error");
                        --check_false(request_resend_if_array(ii)(jj-1).addr = std_logic_vector(to_unsigned(ii, C_ADDRESS_WIDTH)), "ADDRESS error");
                        else
                            check(request_resend_if_array(ii)(jj - 1).tx_fn = std_logic_vector(to_unsigned(ii, C_TX_FN_WIDTH)), "TX_FN error");
                            check(request_resend_if_array(ii)(jj - 1).tx_num = std_logic_vector(to_unsigned(jj, C_TX_NUM_WIDTH)), "TX_NUM error");
                            --check(request_resend_if_array(ii)(jj-1).addr = std_logic_vector(to_unsigned(ii, C_ADDRESS_WIDTH)), "ADDRESS error");
                        end if;
                    end loop;
                end loop;

                info("===== TEST CASE FINISHED =====");

            END IF;                     -- for test_suite
        END LOOP test_cases_loop;

        WAIT FOR 20 ns;
        test_runner_cleanup(runner);
    end process;

    pcs_req_resend_if : process
        variable v_index     : natural := 0;
        variable v_rtx_index : natural := 0;

    begin
        v_index  := 0;
        test_end <= '0';
        loop
            wait until req_resend_if_m.valid = '1' and rising_edge(clk);
            v_index                                       := to_integer(unsigned(req_resend_if_m.tx_fn));
            v_rtx_index                                   := req_resend_index_array(v_index);
            request_resend_if_array(v_index)(v_rtx_index) <= req_resend_if_m;
            v_rtx_index                                   := v_rtx_index + 1;
            req_resend_index_array(v_index)               <= v_rtx_index;

            --v_index := v_index + 1;
            if end_stimuli = '1' then
                exit;
            end if;
        end loop;
        test_end <= '1';

        wait;
    end process;

    pcs_req_cache_if : process
        variable v_index     : natural := 0;
        variable v_rtx_index : natural := 0;

    begin
        v_index := 0;
        loop
            wait until req_cache_if_m.valid = '1' and rising_edge(clk);
            v_index                                      := to_integer(unsigned(req_cache_if_m.tx_fn));
            v_rtx_index                                  := req_cache_index_array(v_index);
            request_cache_if_array(v_index)(v_rtx_index) <= req_cache_if_m;
            v_rtx_index                                  := v_rtx_index + 1;
            req_cache_index_array(v_index)               <= v_rtx_index;

            --v_index := v_index + 1;
            if end_stimuli = '1' then
                exit;
            end if;
        end loop;

        wait;
    end process;

    UUT : entity arq_lib.arq_timekeeper_tx_wrapper
        generic map(
            G_MEMORY_BLOCK_SIZE          => 256, -- A low value increases performance at a huge resource cost.Must be power of 2.
            G_CORE_COUNTER_PERIOD_CLOCKS => 50
        )
        port map(
            clk                  => clk, --: in std_logic;
            rst                  => reset, --: in std_logic;

            --Input interfaces:
            arq_config_if        => arq_config_if, --: in t_arq_config_if;
            ack_data_if_m          => ack_data_if_m, --: inout  t_ack_data_if;
            ack_data_if_s          => ack_data_if_s, --: inout  t_ack_data_if;
            req_insertion_if_m     => req_insertion_if_m, --: inout t_request_insertion_if;
            req_insertion_if_s     => req_insertion_if_s, --: inout t_request_insertion_if;

            --Output interfaces;
            req_resend_if_m        => req_resend_if_m, --: inout t_request_resend_if;
            req_resend_if_s        => req_resend_if_s, --: inout t_request_resend_if;
            req_cache_if_m         => req_cache_if_m, --: out t_request_cache_bus_if
            req_cache_if_s         => req_cache_if_s, --: out t_request_cache_bus_if
            metadata_retrieve_if => metadata_retrieve_if,
            clear_diag           => clear_diag,
            diag_if              => arq_diag_if, --: inout t_arq_control_tx_diagnostics_if
            arq_hw_config        => arq_hw_config
        );

    test_runner_watchdog(runner, 10 ms);
end architecture;
