#!/usr/bin/env python3

##-----------------------------------------------------------------------------
##  Company    : Mynaric Lasercom GmbH, Gilching, Germany
##
##  Restricted � Highly Confidential
##
##  COPYRIGHT
##
##  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
##  This software program is the proprietary copyright material of
##  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
##  is intended to be effected by any use of this software program.
##  This applies also to the permitted use by an authorized user under license
##  notwithstanding the terms and conditions of any agreement entered into
##  by an end user with an intermediary other than Mynaric Lasercom GmbH.
##
##  No part of this software program may be reproduced, amended, varied,
##  re-written or stored in a retrieval system in any form or by any means
##  without the prior written permission of Mynaric Lasercom GmbH.
##
##-----------------------------------------------------------------------------
## Company: Mynaric
## Engineer: Gustavo Martin
## 
## Create Date: 25/06/2022 11:40:33 AM
## Design Name: 
## Module Name: run.py
## Project Name: LET SDA
## Target Devices: 
## Tool Versions: Vivado 2021.1
## Description: Library package to test SDA.
## 
## Dependencies: 
## 
## Revision: 
## Revision 0.01 - File Created
## Additional Comments:
## 
##--------------------------------------------------------------------------------

"""
Run
---

Demonstrates the VUnit run library.
"""

from pathlib import Path
from vunit import VUnit

def create_test_suite(VU):
    TB = Path(__file__).parent
    ROOT = Path(__file__).resolve().parents[2]
    TARGET = ROOT / "src/hdl/let_sda/arq/transmitter"
    COMMON = ROOT / "src/hdl/let_sda/arq/common"
    SDA = ROOT / "src/hdl/let_sda/sda"
    IP = ROOT / "src/hdl/let_sda/ip"
    SHARE_SRC = ROOT / "src/hdl/let_sda/share_let_1g/src"
    SHARE_PKG = ROOT / "src/hdl/let_sda/share_let_1g/packages"
    SDA_TEST_LIB_PATH = ROOT / "tb/sda_test_lib"

    # libraries
    VU.add_osvvm()
    #VU.add_verification_components()

    tb_lib = VU.add_library("tb_lib", allow_duplicate=True)
    sda_test_lib = VU.add_library("sda_test_lib", allow_duplicate=True)
    arq_lib = VU.add_library("arq_lib", allow_duplicate=True)
    share_let_1g = VU.add_library("share_let_1g", allow_duplicate=True)
    sda_lib = VU.add_library("sda_lib", allow_duplicate=True)

    arq_lib.add_source_files([TARGET / "*.vhd"])
    arq_lib.add_source_files([COMMON / "*.vhd"])
    share_let_1g.add_source_files([SHARE_PKG / "*.vhd"])
    share_let_1g.add_source_files([SHARE_SRC / "*.vhd"])
    #share_let_1g.add_source_files([SHARE / "*.vhd"])
    sda_lib.add_source_files([SDA / "*.vhd"])
    sda_lib.add_source_files([IP / "*.vhd"])
    tb_lib.add_source_files([TB / "tb_arq_transmitter_top.vhd"])

    sda_test_lib.add_source_files([SDA_TEST_LIB_PATH / "*_pkg.vhd"])
    sda_test_lib.add_source_files([SDA_TEST_LIB_PATH / "payload_transmiter.vhd"])
    sda_test_lib.add_source_files([SDA_TEST_LIB_PATH / "payload_receiver.vhd"])
    #sda_test_lib.set_compile_option("ghdl.a_flags", ["--ieee-asserts=disable-at-0"])

if __name__ == '__main__':
    VU = VUnit.from_argv()
    #libs.set_compile_option("ghdl.a_flags", ["--no-vital-checks"])
    #VU.set_sim_option("disable_ieee_warnings", True)
    create_test_suite(VU)
    VU.set_sim_option("disable_ieee_warnings", True)
    VU.main()
    #VU.set_sim_option("disable_ieee_warnings", True)
    #VU.set_compile_option('ghdl.a_flags', ['--ieee-asserts=disable', '-frelaxed-rules'])


