-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: tb_arq_transmitter_top
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

library osvvm;
use osvvm.RandomPkg.all;
use osvvm.CoveragePkg.all;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;
library arq_lib;

library sda_test_lib;
use sda_test_lib.sda_test_pkg.all;
library sda_test_lib;
use sda_test_lib.eth_packet_test_pkg.all;
library sda_test_lib;
use sda_test_lib.packet_test_pkg.all;
library sda_test_lib;
use sda_test_lib.payload_test_pkg.all;

--library sda_test_lib;
--use sda_test_lib.ethernet_test_receiver;

entity tb_arq_transmitter_top is
    generic(runner_cfg : string := runner_cfg_default);
end entity;

architecture tb of tb_arq_transmitter_top is

    constant C_CLK_PERIOD        : time      := 10 ns;
    signal clk                   : std_logic := '0';
    signal reset                 : std_logic := '1';
    signal error                 : std_logic;
    signal ram_rd_error          : std_logic;
    signal ram_wr_error          : std_logic;
    signal send                  : std_logic := '0';
    signal payload_length        : natural;
    signal ram_wr_payload_length : natural;
    signal seqnum                : natural;
    signal packet_len            : natural   := 0;
    signal payload               : payload_array_t;
    signal ram_wr_payload        : payload_array_t;
    signal payload_valid         : std_logic;
    signal ram_wr_payload_valid  : std_logic;
    signal tx_fn                 : std_logic_vector(16 - 1 downto 0);
    signal frame_type            : std_logic_vector(2 - 1 downto 0);
    signal tx_fn_out             : std_logic_vector(16 - 1 downto 0);
    signal tx_num_out             : std_logic_vector(C_TX_NUM_WIDTH-1 downto 0);
    signal ram_wr_tx_fn_out      : std_logic_vector(16 - 1 downto 0);
    signal frame_type_out        : std_logic_vector(2 - 1 downto 0);
    signal ram_wr_frame_type_out : std_logic_vector(2 - 1 downto 0);

    signal start_stimuli              : std_logic := '0';
    signal ram_rd_start_stimuli       : std_logic := '0';
    signal ram_wr_start_stimuli       : std_logic := '0';
    signal random_stimuli_wait        : std_logic := '0';
    signal ram_rd_random_stimuli_wait : std_logic := '0';
    signal stimuli_length             : natural;
    signal ram_rd_stimuli_length      : natural;
    type t_stimuli_length_array is array (0 to 500 - 1) of natural;
    signal stimuli_length_array       : t_stimuli_length_array;
    type t_frame_array is array (0 to 5) of std_logic_vector(16-1 downto 0);
    type t_frame_matrix is array (0 to 100-1) of t_frame_array;
    signal frame_matrix : t_frame_matrix;
    signal eth_packet_length : natural;
    signal eth_packet_id     : natural;
    --eth_packet        :    eth_packet_array_t;
    signal eth_packet_valid  : std_logic;
    signal eth_packet_error  : std_logic;
    --signal axis_ethernet_if  : t_axis_if;

    signal eth_vector_id              : eth_packet_id_array_t;
    signal eth_vector_length          : eth_packet_length_array_t;
    signal ram_rd_eth_vector_id       : eth_packet_id_array_t;
    signal ram_rd_eth_vector_length   : eth_packet_length_array_t;
    signal ram_wr_eth_vector_id       : eth_packet_id_array_t;
    signal ram_wr_eth_vector_length   : eth_packet_length_array_t;
    signal process_ram_wr_error       : std_logic;
    signal process_ram_rd_error       : std_logic;
    signal tx_fn_error_injection      : std_logic;
    signal frame_type_error_injection : std_logic;
    signal seqnum_error_injection     : std_logic;
    signal is_idle                    : std_logic;
    signal ram_rd_is_idle             : std_logic;
    signal pt_finished                : std_logic;
    signal back_pressure              : std_logic                              := '0';
    signal eth_packet_good_cnt        : natural                                := 0;
    signal virtual_addr               : unsigned(C_ADDRESS_WIDTH - 1 downto 0) := (others => '0');
    --DUT signals
    -- Config interface:
    signal arq_config_if              : t_arq_config_if;
    -- Input interfaces:
    signal axis_payload_if_in_m         : t_axis_payload_if_m;
    signal axis_payload_if_in_s         : t_axis_payload_if_s;
    -- Output interface:
    signal axis_payload_if_out_m        : t_axis_payload_if_m;
    signal axis_payload_if_out_s        : t_axis_payload_if_s;
    -- Ledger interface: 
    signal req_insertion_if_m           : t_request_insertion_if_m;
    signal req_insertion_if_s           : t_request_insertion_if_s;
    signal metadata_retrieve_if       : t_metadata_retrieve_if;
    signal req_insertion_ram_if_m       : t_request_insertion_ram_if_m;
    signal req_insertion_ram_if_s       : t_request_insertion_ram_if_s;
    signal ack_data_if_m                : t_ack_data_if_m;
    signal ack_data_if_s                : t_ack_data_if_s;
    signal arq_hw_config              : t_arq_hw_config;
    signal diag_control_if         : t_arq_control_tx_diagnostics_if;
    -- Ram interface: 
    signal axis_payload_if_ram_in_m     : t_axis_payload_if_m;
    signal axis_payload_if_ram_in_s     : t_axis_payload_if_s;
    signal axis_payload_if_ram_out_m    : t_axis_payload_if_m;
    signal axis_payload_if_ram_out_s    : t_axis_payload_if_s;
    signal axis_pl_if_ground_m          : t_axis_payload_if_m;
    signal axis_pl_if_ground_s          : t_axis_payload_if_s;
    signal metadata_error             : std_logic;
    signal diag_ram_if     : t_arq_ram_controller_diag_if ;
    signal global_eth_vector_pointer  : natural;

    signal sel_router_out  : std_logic;
    signal sel_router_back_pressure  : std_logic;
    signal axis_router_if_out_m          : t_axis_payload_if_m;
    signal axis_router_if_out_s          : t_axis_payload_if_s;
    signal axis_pl_if_bypass_buf_m          : t_axis_payload_if_m;
    signal axis_pl_if_bypass_buf_s          : t_axis_payload_if_s;
    signal axis_pl_if_bypass_buf_in_m : t_axis_payload_if_m;
    signal axis_pl_if_bypass_buf_in_s : t_axis_payload_if_s;


begin

    --------------------------------------------------------------------------
    -- CLOCK AND RESET.
    --------------------------------------------------------------------------
    clk   <= NOT clk after C_CLK_PERIOD / 2;
    reset <= '0' after 20 * (C_CLK_PERIOD);

    --req_insertion_if.tx_fn      <= (others => 'Z');
    --req_insertion_if.frame_type <= (others => 'Z');
    --req_insertion_if.valid      <= 'Z';
--
    --req_insertion_ram_if.tx_fn <= (others => 'Z');
    --req_insertion_ram_if.addr  <= (others => 'Z');

    test_runner : process
        variable v_tx_fn_ref              : natural := 0;
        variable v_tx_fn                  : natural := 0;
        variable v_tx_num                  : natural := 0;
        variable v_packets_read           : natural := 0;
        variable v_error                  : std_logic;
        variable payload_id_last          : natural := 0;
        variable eth_vector_pointer       : natural;
        variable v_eth_vector_pointer_aux : natural := 0;
        variable pk_size                  : natural := 64;
        variable pk_offset                : natural := 0;
        variable v_packet                 : packet_array_t;
        variable split                    : std_logic;
        variable v_payload                : payload_array_t;
        variable data_flag                : std_logic;
        variable wait_for_idle            : std_logic;
        variable v_eth_vector_id          : eth_packet_id_array_t;
        variable v_eth_vector_length      : eth_packet_length_array_t;

    begin
        test_runner_setup(runner, runner_cfg);
        -- enable logging for passing check
        --show(get_logger(default_checker), display_handler, pass); 
        --show(get_logger(RED_CHECKER), display_handler, pass); 
        --packet_len <= 100;
        test_cases_loop : WHILE test_suite LOOP

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- check that packet header is working with all alignment in the payload
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            IF run("arq_disabled") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: arq_disabled");
                info("--------------------------------------------------------------------------------");
                start_stimuli              <= '0';
                stimuli_length             <= 10;
                seqnum_error_injection     <= '0';
                back_pressure              <= '1';
                frame_type_error_injection <= '0';
                eth_vector_pointer         := 0;
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';
                sel_router_out             <= '1';
                sel_router_back_pressure   <= '0';
                arq_config_if          <= C_ARQ_CONFIG_IF_MASTER_INIT;
                --Disable ARQ:
                arq_config_if.max_retx <= (others => '0');

                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);

                for ii in 0 to eth_packet_length_array_t'length - 1 loop
                    eth_vector_id(ii)     <= ii;
                    eth_vector_length(ii) <= 1044;
                end loop;

                wait for 90 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                wait until rising_edge(clk);
                wait until rising_edge(clk);
                wait for 30 us;
                wait until rising_edge(clk);

                wait until rising_edge(clk);
                -- send write command:
                back_pressure <= '0';

                wait until rising_edge(clk);
                wait until rising_edge(clk);
                --wait for 5 us; 
                wait for 500 ns;
                --back_pressure <= '1';
                --Wait until RD process is finished:
                --wait until ram_cmd_if_rd.busy = '0';
                wait until rising_edge(clk);
                wait until rising_edge(clk);
                wait for 300 ns;
                wait until rising_edge(clk);

                --           wait for 200 us;
                wait_for_idle   := '1';
                payload_id_last := 0;
                v_tx_fn_ref     := 0;
                pk_offset       := 0;
                --while wait_for_idle = '1' loop
                for ii in 0 to 10 - 1 loop
                   wait until payload_valid = '1';
                    check(payload_length = C_FSO_PAYLOAD_SIZE, "ERROR: payload length is NOT C_FSO_PAYLOAD_SIZE");
                    v_tx_fn   := to_integer(unsigned(tx_fn_out));
                    v_payload := payload;
                    --info("payload_valid");
                    if frame_type_out = C_FRAME_IDLE then
                        v_error := payload_test_health_check(payload);
                        check(v_error = '1', "Data detected in IDLE payload.");
                        v_tx_fn := to_integer(unsigned(tx_fn_out));
                        if v_tx_fn > 2 then
                            wait_for_idle := '0';
                        end if;

                    elsif frame_type_out = C_FRAME_DATA then
                        payload_test_read_payload_data(
                            v_payload,
                            pk_offset,
                            v_packet,
                            pk_size,
                            v_eth_vector_id,
                            v_eth_vector_length,
                            eth_vector_pointer,
                            split,
                            payload_id_last,
                            v_error);
                        check(v_error = '0', "Payload health check failed.");

                        --info("Payload_read_data()");
                        --report to_string(v_eth_vector_length(eth_vector_pointer));
                    end if;

                    --check(frame_type = C_FRAME_IDLE, "Expect frame type to be IDLE");
                    v_tx_fn := to_integer(unsigned(tx_fn_out));
                    --check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");

                    wait until rising_edge(clk);
                    v_tx_fn_ref := v_tx_fn_ref + 1;
                end loop;

                for jj in 0 to stimuli_length - 1 loop
                    check(eth_vector_id(jj) = v_eth_vector_id(jj), "Wrong Ethernet packet ID");
                    check(eth_vector_length(jj) = v_eth_vector_length(jj), "Wrong Ethernet packet Length detected");
                    check_false(eth_vector_length(jj) < 64, "Ethernet packet length can not be < 64.");
                end loop;

                wait for 50 us;

                info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- 2 packet stimuli with 5 retx. 
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("basic_cache_retx_test") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: basic_cache_retx_test");
                info("--------------------------------------------------------------------------------");
                start_stimuli              <= '0';
                stimuli_length             <= 3;
                ram_rd_start_stimuli       <= '0';
                ram_wr_start_stimuli       <= '0';
                seqnum_error_injection     <= '0';
                back_pressure              <= '1';
                frame_type_error_injection <= '0';
                eth_vector_pointer         := 0;
                tx_fn_error_injection      <= '0';
                global_eth_vector_pointer  <= eth_vector_pointer;
                is_idle                    <= '0';
                sel_router_out             <= '0';
                sel_router_back_pressure   <= '0';

                ack_data_if_m <= C_ACK_DATA_IF_MASTER_INIT; 
                arq_config_if.holdoff_nframes     <= std_logic_vector(to_unsigned(256, 12));
                arq_config_if.max_retx            <= std_logic_vector(to_unsigned(5, 3));
                arq_config_if.holdoff_time        <= std_logic_vector(to_unsigned(400, C_TIMESTAMP_WIDTH));
                arq_config_if.core_counter_period <= std_logic_vector(to_unsigned(20 - 1, 12)); --20*5=100 tenths of us -> 1 us
                arq_config_if.cache_trigger_time  <= std_logic_vector(to_unsigned(200, C_TIMESTAMP_WIDTH)); -- not used
                ack_data_if_m.ack                   <= '1';
                ack_data_if_m.ack_start_fn          <= (others => '0');
                ack_data_if_m.ack_span              <= (others => '0');
                ack_data_if_m.ack_valid             <= '0';
                --ack_data_if.rd_ena                <= 'Z';

                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);

                for ii in 0 to eth_packet_length_array_t'length - 1 loop
                    eth_vector_id(ii)     <= ii;
                    eth_vector_length(ii) <= 1044;
                end loop;

                wait for 90 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli   <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli   <= '0';
                wait until rising_edge(clk);
                back_pressure   <= '0';
                wait for 500 us;

                -- open fifo mux:
                sel_router_out             <= '1';
                back_pressure              <= '0';
                -- test output:
                payload_id_last := 0;
                v_tx_fn_ref     := 0;
                pk_offset       := 0;
                --while wait_for_idle = '1' loop
                for ii in 0 to stimuli_length+(stimuli_length*5) - 1 loop

                    wait until payload_valid = '1';
                    check(payload_length = C_FSO_PAYLOAD_SIZE, "ERROR: payload length is NOT C_FSO_PAYLOAD_SIZE");
                    v_tx_fn   := to_integer(unsigned(tx_fn_out));
                    v_tx_num  := to_integer(unsigned(tx_num_out));
                    v_payload := payload;
                    --info("payload_valid");
                    if frame_type_out = C_FRAME_IDLE then
                        v_error := payload_test_health_check(payload);
                        check(v_error = '1', "Data detected in IDLE payload.");
                        v_tx_fn := to_integer(unsigned(tx_fn_out));
                        if v_tx_fn > 2 then
                            wait_for_idle := '0';
                        end if;

                    elsif frame_type_out = C_FRAME_DATA then
                        payload_test_read_payload_data(
                            v_payload,
                            pk_offset,
                            v_packet,
                            pk_size,
                            v_eth_vector_id,
                            v_eth_vector_length,
                            eth_vector_pointer,
                            split,
                            payload_id_last,
                            v_error);
                        check(v_error = '0', "Payload health check failed.");

                        frame_matrix(v_tx_fn)(v_tx_num) <= tx_fn_out;
                        global_eth_vector_pointer <= eth_vector_pointer;
                        --info("Payload_read_data()");
                        --report to_string(v_eth_vector_length(eth_vector_pointer));
                    end if;

                    check(frame_type_out = C_FRAME_DATA, "Expect frame type to be DATA");
                    v_tx_fn := to_integer(unsigned(tx_fn_out));
                    --check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");

                    wait until rising_edge(clk);
                    v_tx_fn_ref := v_tx_fn_ref + 1;
                end loop;
                
                wait for 200 us;
                for ii in 0 to 5 loop
                    for jj in 0 to stimuli_length - 1 loop
                        --check(eth_vector_id(jj) = v_eth_vector_id(ii+jj), "Wrong Ethernet packet ID");
                        report to_string(v_eth_vector_id(jj+(ii*2)));
                        check(eth_vector_length(jj) = v_eth_vector_length(ii+jj), "Wrong Ethernet packet Length detected");
                        check_false(eth_vector_length(jj) < 64, "Ethernet packet length can not be < 64.");
                        check(frame_matrix(jj)(ii) = std_logic_vector(to_unsigned(jj, 16)), "Frame check retx error");
                    end loop;
                end loop;

                check(diag_ram_if.insertion_req_counter = to_unsigned(stimuli_length, 32), "insertion req counter");
                -- check(diag_ram_if.cache_req_counter = to_unsigned(stimuli_length*5, 32), ": cache_req_counter ");
                check(diag_ram_if.cache_req_counter = to_unsigned(stimuli_length*5, 32), ": cache_req_counter ");
                -- check(diag_ram_if.cache_retx_counter = to_unsigned(stimuli_length*5, 32), "cache_retx_counter ");
                check(diag_ram_if.cache_retx_counter = to_unsigned(stimuli_length*5, 32), "cache_retx_counter ");
                -- check(diag_ram_if.cache_timeout_counter = to_unsigned(stimuli_length*5, 32), "cache_timeout_counter ");
                check(diag_ram_if.cache_timeout_counter = to_unsigned(0, 32), "cache_timeout_counter ");
                check(diag_ram_if.address_error = to_unsigned(0, 32), "address_error ");
                check(diag_ram_if.cache_error = to_unsigned(0, 32), "cache_error ");
                check(diag_ram_if.ram_error = '0', "ram_error ");
                check(diag_ram_if.insertion_error = to_unsigned(0, 32), "insertion_error ");
            
                info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- 2 packet stimuli with 5 retx. 
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("overload_cache_test") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: overload_cache_test");
                info("--------------------------------------------------------------------------------");
                start_stimuli              <= '0';
                stimuli_length             <= 15;
                ram_rd_start_stimuli       <= '0';
                ram_wr_start_stimuli       <= '0';
                seqnum_error_injection     <= '0';
                back_pressure              <= '1';
                frame_type_error_injection <= '0';
                eth_vector_pointer         := 0;
                tx_fn_error_injection      <= '0';
                global_eth_vector_pointer  <= eth_vector_pointer;
                is_idle                    <= '0';
                sel_router_out             <= '0';
                sel_router_back_pressure   <= '0';

                ack_data_if_m <= C_ACK_DATA_IF_MASTER_INIT; 
                arq_config_if.holdoff_nframes     <= std_logic_vector(to_unsigned(256, 12));
                arq_config_if.max_retx            <= std_logic_vector(to_unsigned(5, 3));
                arq_config_if.holdoff_time        <= std_logic_vector(to_unsigned(400, C_TIMESTAMP_WIDTH));
                arq_config_if.core_counter_period <= std_logic_vector(to_unsigned(20 - 1, 12)); --20*5=100 tenths of us -> 1 us
                arq_config_if.cache_trigger_time  <= std_logic_vector(to_unsigned(30, C_TIMESTAMP_WIDTH)); -- not used
                ack_data_if_m.ack                   <= '1';
                ack_data_if_m.ack_start_fn          <= (others => '0');
                ack_data_if_m.ack_span              <= (others => '0');
                ack_data_if_m.ack_valid             <= '0';
                --ack_data_if_s.rd_ena                <= 'Z';

                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);

                for ii in 0 to eth_packet_length_array_t'length - 1 loop
                    eth_vector_id(ii)     <= ii;
                    eth_vector_length(ii) <= 1044;
                end loop;

                wait for 90 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli   <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli   <= '0';
                wait until rising_edge(clk);
                back_pressure   <= '0';
                -- wait for 100 us;
                -- sel_router_back_pressure   <= '1';
                -- wait for 100 us;
                -- sel_router_back_pressure   <= '0';

                wait for 200 us;

                -- open fifo mux:
                sel_router_out             <= '1';
                back_pressure              <= '0';
                -- test output:
                payload_id_last := 0;
                v_tx_fn_ref     := 0;
                pk_offset       := 0;
                --while wait_for_idle = '1' loop
                for ii in 0 to stimuli_length+(stimuli_length*5) - 1 loop

                    wait until payload_valid = '1';
                    check(payload_length = C_FSO_PAYLOAD_SIZE, "ERROR: payload length is NOT C_FSO_PAYLOAD_SIZE");
                    v_tx_fn   := to_integer(unsigned(tx_fn_out));
                    v_tx_num  := to_integer(unsigned(tx_num_out));
                    v_payload := payload;
                    --info("payload_valid");
                    if frame_type_out = C_FRAME_IDLE then
                        v_error := payload_test_health_check(payload);
                        check(v_error = '1', "Data detected in IDLE payload.");
                        v_tx_fn := to_integer(unsigned(tx_fn_out));
                        if v_tx_fn > 2 then
                            wait_for_idle := '0';
                        end if;

                    elsif frame_type_out = C_FRAME_DATA then
                        payload_test_read_payload_data(
                            v_payload,
                            pk_offset,
                            v_packet,
                            pk_size,
                            v_eth_vector_id,
                            v_eth_vector_length,
                            eth_vector_pointer,
                            split,
                            payload_id_last,
                            v_error);
                        check(v_error = '0', "Payload health check failed.");

                        frame_matrix(v_tx_fn)(v_tx_num) <= tx_fn_out;
                        global_eth_vector_pointer <= eth_vector_pointer;
                        --info("Payload_read_data()");
                        --report to_string(v_eth_vector_length(eth_vector_pointer));
                    end if;

                    check(frame_type_out = C_FRAME_DATA, "Expect frame type to be DATA");
                    v_tx_fn := to_integer(unsigned(tx_fn_out));
                    --check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");
                     -- if v_tx_fn = 12 and v_tx_num = 5 then
                     --     exit;
                     -- end if;
                    wait until rising_edge(clk);
                    v_tx_fn_ref := v_tx_fn_ref + 1;
                end loop;
                
                 for ii in 0 to 5 loop
                    for jj in 0 to stimuli_length - 1 loop
                         if frame_matrix(jj)(ii) /= std_logic_vector(to_unsigned(jj, 16)) then
                            report "Failed-> " & " TX_FN:" & to_string(jj) & " TX_NUM:" & to_string(ii);
                        end if;
                    end loop;
                end loop;

                wait for 50 us;
                for ii in 0 to 5 loop
                    for jj in 0 to stimuli_length - 1 loop
                        --check(eth_vector_id(jj) = v_eth_vector_id(ii+jj), "Wrong Ethernet packet ID");
                        report to_string(v_eth_vector_id(jj+(ii*2)));
                        check(eth_vector_length(jj) = v_eth_vector_length(ii+jj), "Wrong Ethernet packet Length detected");
                        check_false(eth_vector_length(jj) < 64, "Ethernet packet length can not be < 64.");
                        check(frame_matrix(jj)(ii) = std_logic_vector(to_unsigned(jj, 16)), "Frame check retx error");
                    end loop;
                end loop;

                check(diag_ram_if.insertion_req_counter = to_unsigned(stimuli_length, 32), "insertion req counter");
                -- check(diag_ram_if.cache_req_counter = to_unsigned(stimuli_length*5, 32), ": cache_req_counter ");
                check(diag_ram_if.cache_req_counter = to_unsigned(stimuli_length*5, 32), ": cache_req_counter ");
                -- check(diag_ram_if.cache_retx_counter = to_unsigned(stimuli_length*5, 32), "cache_retx_counter ");
                check(diag_ram_if.cache_retx_counter = to_unsigned(stimuli_length*5, 32), "cache_retx_counter ");
                -- check(diag_ram_if.cache_timeout_counter = to_unsigned(stimuli_length*5, 32), "cache_timeout_counter ");
                check(diag_ram_if.cache_timeout_counter = to_unsigned(0, 32), "cache_timeout_counter ");
                check(diag_ram_if.address_error = to_unsigned(0, 32), "address_error ");
                check(diag_ram_if.cache_error = to_unsigned(0, 32), "cache_error ");
                check(diag_ram_if.ram_error = '0', "ram_error ");
                check(diag_ram_if.insertion_error = to_unsigned(0, 32), "insertion_error ");
            
                info("Test finished");
            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- check that packet header is working with all alignment in the payload
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("arq_data_insertion_and_bypass") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: arq_data_insertion_and_bypass");
                info("--------------------------------------------------------------------------------");
                start_stimuli              <= '0';
                stimuli_length             <= 2;
                ram_rd_start_stimuli       <= '0';
                ram_wr_start_stimuli       <= '0';
                seqnum_error_injection     <= '0';
                back_pressure              <= '1';
                frame_type_error_injection <= '0';
                eth_vector_pointer         := 0;
                tx_fn_error_injection      <= '0';
                global_eth_vector_pointer  <= eth_vector_pointer;
                is_idle                    <= '0';
                sel_router_out             <= '0';

                ack_data_if_m <= C_ACK_DATA_IF_MASTER_INIT; 
                arq_config_if.holdoff_nframes     <= std_logic_vector(to_unsigned(256, 12));
                arq_config_if.max_retx            <= std_logic_vector(to_unsigned(5, 3));
                arq_config_if.holdoff_time        <= std_logic_vector(to_unsigned(300, C_TIMESTAMP_WIDTH));
                arq_config_if.core_counter_period <= std_logic_vector(to_unsigned(20 - 1, 12)); --20*5=100 tenths of us -> 1 us
                arq_config_if.cache_trigger_time  <= std_logic_vector(to_unsigned(100, C_TIMESTAMP_WIDTH)); -- not used
                ack_data_if_m.ack                   <= '1';
                ack_data_if_m.ack_start_fn          <= (others => '0');
                ack_data_if_m.ack_span              <= (others => '0');
                ack_data_if_m.ack_valid             <= '0';
                --ack_data_if_s.rd_ena                <= 'Z';

                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);

                for ii in 0 to eth_packet_length_array_t'length - 1 loop
                    eth_vector_id(ii)     <= ii;
                    eth_vector_length(ii) <= 1044;
                end loop;

                wait for 90 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli   <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli   <= '0';
                wait until rising_edge(clk);
                back_pressure   <= '0';
                -- wait until pt_finished = '1';
                -- wait for 50 us;                
                -- wait until rising_edge(clk);
                -- ack_data_if.ack_start_fn <= x"0000"; 
                -- ack_data_if.ack          <= '1'; 
                -- ack_data_if.ack_valid    <= '1'; 
                -- wait until rising_edge(clk);
                -- ack_data_if.ack_valid    <= '0'; 

                -- wait until rising_edge(clk);
                -- ack_data_if.ack_start_fn <= x"0001"; 
                -- ack_data_if.ack          <= '1'; 
                -- ack_data_if.ack_valid    <= '1'; 
                -- wait until rising_edge(clk);
                -- ack_data_if.ack_valid    <= '0';               
                --wait for 100 us;

                -----------------------------
                ---------IDLE---------------
                --is_idle        <= '1';
                --stimuli_length <= 5;
                --wait until rising_edge(clk);
                --start_stimuli  <= '1';
                --wait until rising_edge(clk);
                --start_stimuli  <= '0';
                ----is_idle <= '0';
                --wait until pt_finished = '1';

                --wait for 100 us;
                -----------------------------------------------
                ------------- SECOND DATA BURST ---------------
                --is_idle                   <= '0';
                --WAIT UNTIL rising_edge(clk);
                --start_stimuli             <= '1';
                --WAIT UNTIL rising_edge(clk);
                --start_stimuli             <= '0';
                --wait until rising_edge(clk);               


                --wait_for_idle   := '1';
                --payload_id_last := 0;
                --v_tx_fn_ref     := 0;
                --pk_offset       := 0;
                ----while wait_for_idle = '1' loop
                --for ii in 0 to stimuli_length - 1 loop

                --    wait until payload_valid = '1';
                --    check(payload_length = C_FSO_PAYLOAD_SIZE, "ERROR: payload length is NOT C_FSO_PAYLOAD_SIZE");
                --    v_tx_fn   := to_integer(unsigned(tx_fn_out));
                --    v_payload := payload;
                --    --info("payload_valid");
                --    if frame_type_out = C_FRAME_IDLE then
                --        v_error := payload_test_health_check(payload);
                --        check(v_error = '1', "Data detected in IDLE payload.");
                --        v_tx_fn := to_integer(unsigned(tx_fn_out));
                --        if v_tx_fn > 2 then
                --            wait_for_idle := '0';
                --        end if;

                --    elsif frame_type_out = C_FRAME_DATA then
                --        payload_test_read_payload_data(
                --            v_payload,
                --            pk_offset,
                --            v_packet,
                --            pk_size,
                --            v_eth_vector_id,
                --            v_eth_vector_length,
                --            eth_vector_pointer,
                --            split,
                --            payload_id_last,
                --            v_error);
                --        check(v_error = '0', "Payload health check failed.");

                --        global_eth_vector_pointer <= eth_vector_pointer;
                --        --info("Payload_read_data()");
                --        --report to_string(v_eth_vector_length(eth_vector_pointer));
                --    end if;

                --    --check(frame_type = C_FRAME_IDLE, "Expect frame type to be IDLE");
                --    v_tx_fn := to_integer(unsigned(tx_fn_out));
                --    --check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");

                --    wait until rising_edge(clk);
                --    v_tx_fn_ref := v_tx_fn_ref + 1;
                --end loop;

                --for jj in 0 to stimuli_length - 1 loop
                --    check(eth_vector_id(jj) = v_eth_vector_id(jj), "Wrong Ethernet packet ID");
                --    --report to_string(v_eth_vector_length(jj));
                --    check(eth_vector_length(jj) = v_eth_vector_length(jj), "Wrong Ethernet packet Length detected");
                --    check_false(eth_vector_length(jj) < 64, "Ethernet packet length can not be < 64.");
                --end loop;

                -------------------------------
                -----------IDLE---------------
                --is_idle        <= '1';
                --stimuli_length <= 5;
                --wait until rising_edge(clk);
                --start_stimuli  <= '1';
                --wait until rising_edge(clk);
                --start_stimuli  <= '0';
                ----is_idle <= '0';
                --wait until pt_finished = '1';
                
                -------------------------------------------
                --------- SECOND DATA BURST ---------------
                -- is_idle                   <= '0';
                -- WAIT UNTIL rising_edge(clk);
                -- start_stimuli             <= '1';
                -- WAIT UNTIL rising_edge(clk);
                -- start_stimuli             <= '0';
                -- wait until rising_edge(clk);
                -- eth_vector_pointer        := 0;
                -- back_pressure             <= '0';
                -- global_eth_vector_pointer <= eth_vector_pointer;
                -- wait_for_idle             := '1';
                -- payload_id_last           := 0;
                -- v_tx_fn_ref               := 0;
                --pk_offset                 := 0;
                ----while wait_for_idle = '1' loop
                --for ii in 0 to stimuli_length - 1 loop

                --    wait until payload_valid = '1';
                --    check(payload_length = C_FSO_PAYLOAD_SIZE, "ERROR: payload length is NOT C_FSO_PAYLOAD_SIZE");
                --    v_tx_fn   := to_integer(unsigned(tx_fn_out));
                --    v_payload := payload;
                --    --info("payload_valid");
                --    if frame_type_out = C_FRAME_IDLE then
                --        v_error := payload_test_health_check(payload);
                --        check(v_error = '1', "Data detected in IDLE payload.");
                --        v_tx_fn := to_integer(unsigned(tx_fn_out));
                --        if v_tx_fn > 2 then
                --            wait_for_idle := '0';
                --        end if;

                --    elsif frame_type_out = C_FRAME_DATA then
                --        payload_test_read_payload_data(
                --            v_payload,
                --            pk_offset,
                --            v_packet,
                --            pk_size,
                --            v_eth_vector_id,
                --            v_eth_vector_length,
                --            eth_vector_pointer,
                --            split,
                --            payload_id_last,
                --            v_error);
                --        check(v_error = '0', "Payload health check failed.");
                --        global_eth_vector_pointer <= eth_vector_pointer;
                --        --info("Payload_read_data()");
                --        --report to_string(v_eth_vector_length(eth_vector_pointer));
                --    end if;

                --    --check(frame_type = C_FRAME_IDLE, "Expect frame type to be IDLE");
                --    v_tx_fn := to_integer(unsigned(tx_fn_out));
                --    --check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");

                --    wait until rising_edge(clk);
                --    v_tx_fn_ref := v_tx_fn_ref + 1;
                --end loop;

                --for jj in 0 to stimuli_length - 1 loop
                --    check(eth_vector_id(jj) = v_eth_vector_id(jj), "Wrong Ethernet packet ID");
                --    --report to_string(v_eth_vector_length(jj));
                --    check(eth_vector_length(jj) = v_eth_vector_length(jj), "Wrong Ethernet packet Length detected");
                --    check_false(eth_vector_length(jj) < 64, "Ethernet packet length can not be < 64.");
                --end loop;

                wait for 400 us;
                --check(metadata_error = '0', "Metadata interface error");
                --check(process_ram_wr_error = '0', "Process RAM WR error...");
                ----check(process_ram_rd_error = '0', "Process RAM RD error...");
                info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- check that packet header is working with all alignment in the payload
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("arq_back_pressure_test") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: arq_back_pressure_test");
                info("--------------------------------------------------------------------------------");
                start_stimuli              <= '0';
                stimuli_length             <= 13;
                ram_rd_start_stimuli       <= '0';
                ram_wr_start_stimuli       <= '0';
                seqnum_error_injection     <= '0';
                back_pressure              <= '1';
                frame_type_error_injection <= '0';
                eth_vector_pointer         := 0;
                tx_fn_error_injection      <= '0';
                global_eth_vector_pointer  <= eth_vector_pointer;
                is_idle                    <= '0';
                sel_router_out             <= '0';
                sel_router_back_pressure   <= '0';

                ack_data_if_m <= C_ACK_DATA_IF_MASTER_INIT; 
                arq_config_if.holdoff_nframes     <= std_logic_vector(to_unsigned(256, 12));
                arq_config_if.max_retx            <= std_logic_vector(to_unsigned(5, 3));
                arq_config_if.holdoff_time        <= std_logic_vector(to_unsigned(400, C_TIMESTAMP_WIDTH));
                arq_config_if.core_counter_period <= std_logic_vector(to_unsigned(20 - 1, 12)); --20*5=100 tenths of us -> 1 us
                arq_config_if.cache_trigger_time  <= std_logic_vector(to_unsigned(30, C_TIMESTAMP_WIDTH)); -- not used
                ack_data_if_m.ack                   <= '1';
                ack_data_if_m.ack_start_fn          <= (others => '0');
                ack_data_if_m.ack_span              <= (others => '0');
                ack_data_if_m.ack_valid             <= '0';
                --ack_data_if_s.rd_ena                <= 'Z';

                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);

                for ii in 0 to eth_packet_length_array_t'length - 1 loop
                    eth_vector_id(ii)     <= ii;
                    eth_vector_length(ii) <= 1044;
                end loop;

                wait for 90 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli   <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli   <= '0';
                wait until rising_edge(clk);
                back_pressure   <= '0';
                wait for 50 us;
                sel_router_back_pressure   <= '1';
                wait for 200 us;
                sel_router_back_pressure   <= '0';

                -- wait for 200 us;

                -- open fifo mux:
                sel_router_out             <= '1';
                back_pressure              <= '0';
                -- test output:
                payload_id_last := 0;
                v_tx_fn_ref     := 0;
                pk_offset       := 0;
                --while wait_for_idle = '1' loop
                for ii in 0 to stimuli_length+(stimuli_length*5) - 1 loop

                    wait until payload_valid = '1';
                    check(payload_length = C_FSO_PAYLOAD_SIZE, "ERROR: payload length is NOT C_FSO_PAYLOAD_SIZE");
                    v_tx_fn   := to_integer(unsigned(tx_fn_out));
                    v_tx_num  := to_integer(unsigned(tx_num_out));
                    v_payload := payload;
                    --info("payload_valid");
                    if frame_type_out = C_FRAME_IDLE then
                        v_error := payload_test_health_check(payload);
                        check(v_error = '1', "Data detected in IDLE payload.");
                        v_tx_fn := to_integer(unsigned(tx_fn_out));
                        if v_tx_fn > 2 then
                            wait_for_idle := '0';
                        end if;

                    elsif frame_type_out = C_FRAME_DATA then
                        payload_test_read_payload_data(
                            v_payload,
                            pk_offset,
                            v_packet,
                            pk_size,
                            v_eth_vector_id,
                            v_eth_vector_length,
                            eth_vector_pointer,
                            split,
                            payload_id_last,
                            v_error);
                        check(v_error = '0', "Payload health check failed.");

                        frame_matrix(v_tx_fn)(v_tx_num) <= tx_fn_out;
                        global_eth_vector_pointer <= eth_vector_pointer;
                        --info("Payload_read_data()");
                        --report to_string(v_eth_vector_length(eth_vector_pointer));
                    end if;

                    -- report to_string(ii);
                    check(frame_type_out = C_FRAME_DATA, "Expect frame type to be DATA");
                    v_tx_fn := to_integer(unsigned(tx_fn_out));
                    --check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");
                     -- if v_tx_fn = 12 and v_tx_num = 5 then
                     --     exit;
                     -- end if;
                    wait until rising_edge(clk);
                    v_tx_fn_ref := v_tx_fn_ref + 1;
                end loop;
                
                wait for 50 us;
                 
                for ii in 0 to 5 loop
                    for jj in 0 to stimuli_length - 1 loop
                         if frame_matrix(jj)(ii) /= std_logic_vector(to_unsigned(jj, 16)) then
                            report "Failed-> " & " TX_FN:" & to_string(jj) & " TX_NUM:" & to_string(ii);
                        end if;
                    end loop;
                end loop;

                for ii in 0 to 5 loop
                    for jj in 0 to stimuli_length - 1 loop
                        --check(eth_vector_id(jj) = v_eth_vector_id(ii+jj), "Wrong Ethernet packet ID");
                        report to_string(v_eth_vector_id(jj+(ii*2)));
                        check(eth_vector_length(jj) = v_eth_vector_length(ii+jj), "Wrong Ethernet packet Length detected");
                        check_false(eth_vector_length(jj) < 64, "Ethernet packet length can not be < 64.");
                        check(frame_matrix(jj)(ii) = std_logic_vector(to_unsigned(jj, 16)), "Frame check retx error");

                    end loop;
                end loop;

                check(diag_ram_if.insertion_req_counter = to_unsigned(stimuli_length, 32), "insertion req counter");
                -- check(diag_ram_if.cache_req_counter = to_unsigned(stimuli_length*5, 32), ": cache_req_counter ");
                check(diag_ram_if.cache_req_counter = to_unsigned(stimuli_length*5, 32), ": cache_req_counter ");
                -- check(diag_ram_if.cache_retx_counter = to_unsigned(stimuli_length*5, 32), "cache_retx_counter ");
                check(diag_ram_if.cache_retx_counter = to_unsigned(stimuli_length*5, 32), "cache_retx_counter ");
                -- check(diag_ram_if.cache_timeout_counter = to_unsigned(stimuli_length*5, 32), "cache_timeout_counter ");
                check(diag_ram_if.cache_timeout_counter = to_unsigned(0, 32), "cache_timeout_counter ");
                check(diag_ram_if.address_error = to_unsigned(0, 32), "address_error ");
                check(diag_ram_if.cache_error = to_unsigned(0, 32), "cache_error ");
                check(diag_ram_if.ram_error = '0', "ram_error ");
                check(diag_ram_if.insertion_error = to_unsigned(0, 32), "insertion_error ");
            
                info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- check that packet header is working with all alignment in the payload
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            --ELSIF run("arq_data_resend_and_bypass") THEN
            --    info("--------------------------------------------------------------------------------");
            --    info("TEST CASE: arq_data_resend_and_bypass");
            --    info("--------------------------------------------------------------------------------");
            --    start_stimuli              <= '0';
            --    stimuli_length             <= 5;
            --    ram_rd_start_stimuli       <= '0';
            --    ram_wr_start_stimuli       <= '0';
            --    seqnum_error_injection     <= '0';
            --    back_pressure              <= '1';
            --    frame_type_error_injection <= '0';
            --    eth_vector_pointer         := 0;
            --    tx_fn_error_injection      <= '0';
            --    global_eth_vector_pointer  <= eth_vector_pointer;
            --    is_idle                    <= '0';

            --    arq_config_if          <= C_ARQ_CONFIG_IF_MASTER_INIT;
            --    --Disable ARQ:
            --    arq_config_if.max_retx <= "001";
            --    -- Set default:

            --    WAIT UNTIL reset = '0';
            --    WAIT UNTIL rising_edge(clk);

            --    for ii in 0 to eth_packet_length_array_t'length - 1 loop
            --        eth_vector_id(ii)     <= ii;
            --        eth_vector_length(ii) <= 1044;
            --    end loop;

            --    wait for 90 ns;
            --    WAIT UNTIL rising_edge(clk);
            --    start_stimuli   <= '1';
            --    WAIT UNTIL rising_edge(clk);
            --    start_stimuli   <= '0';
            --    wait until rising_edge(clk);
            --    back_pressure   <= '0';
            --    wait_for_idle   := '1';
            --    payload_id_last := 0;
            --    v_tx_fn_ref     := 0;
            --    pk_offset       := 0;
            --    --while wait_for_idle = '1' loop
            --    for ii in 0 to stimuli_length - 1 loop

            --        wait until payload_valid = '1';
            --        check(payload_length = C_FSO_PAYLOAD_SIZE, "ERROR: payload length is NOT C_FSO_PAYLOAD_SIZE");
            --        v_tx_fn   := to_integer(unsigned(tx_fn_out));
            --        v_payload := payload;
            --        --info("payload_valid");
            --        if frame_type_out = C_FRAME_IDLE then
            --            v_error := payload_test_health_check(payload);
            --            check(v_error = '1', "Data detected in IDLE payload.");
            --            v_tx_fn := to_integer(unsigned(tx_fn_out));
            --            if v_tx_fn > 2 then
            --                wait_for_idle := '0';
            --            end if;

            --        elsif frame_type_out = C_FRAME_DATA then
            --            payload_test_read_payload_data(
            --                v_payload,
            --                pk_offset,
            --                v_packet,
            --                pk_size,
            --                v_eth_vector_id,
            --                v_eth_vector_length,
            --                eth_vector_pointer,
            --                split,
            --                payload_id_last,
            --                v_error);
            --            check(v_error = '0', "Payload health check failed.");

            --            global_eth_vector_pointer <= eth_vector_pointer;
            --            --info("Payload_read_data()");
            --            --report to_string(v_eth_vector_length(eth_vector_pointer));
            --        end if;

            --        --check(frame_type = C_FRAME_IDLE, "Expect frame type to be IDLE");
            --        v_tx_fn := to_integer(unsigned(tx_fn_out));
            --        --check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");

            --        wait until rising_edge(clk);
            --        v_tx_fn_ref := v_tx_fn_ref + 1;
            --    end loop;

            --    for jj in 0 to stimuli_length - 1 loop
            --        check(eth_vector_id(jj) = v_eth_vector_id(jj), "Wrong Ethernet packet ID");
            --        --report to_string(v_eth_vector_length(jj));
            --        check(eth_vector_length(jj) = v_eth_vector_length(jj), "Wrong Ethernet packet Length detected");
            --        check_false(eth_vector_length(jj) < 64, "Ethernet packet length can not be < 64.");
            --    end loop;

            --    -------------------------------
            --    -----------IDLE---------------
            --    is_idle        <= '1';
            --    stimuli_length <= 5;
            --    wait until rising_edge(clk);
            --    start_stimuli  <= '1';
            --    wait until rising_edge(clk);
            --    start_stimuli  <= '0';
            --    --is_idle <= '0';
            --    --wait until pt_finished = '1';          

            --    wait for 30 us;
            --    ---------------------------------------------
            --    ----------- Resend Pressure   ---------------
            --    ram_rd_eth_vector_id(0)     <= 27;
            --    ram_rd_eth_vector_id(1)     <= 28;
            --    ram_rd_eth_vector_id(2)     <= 29;
            --    ram_rd_eth_vector_id(3)     <= 30;
            --    ram_rd_eth_vector_id(4)     <= 31;
            --    ram_rd_eth_vector_length(0) <= 1044;
            --    ram_rd_eth_vector_length(1) <= 1044;
            --    ram_rd_eth_vector_length(2) <= 1044;
            --    ram_rd_eth_vector_length(3) <= 1044;
            --    ram_rd_eth_vector_length(4) <= 1044;

            --    ram_rd_is_idle            <= '0';
            --    ram_rd_stimuli_length     <= 5;
            --    wait until rising_edge(clk);
            --    ram_rd_start_stimuli      <= '1';
            --    wait until rising_edge(clk);
            --    ram_rd_start_stimuli      <= '0';
            --    eth_vector_pointer        := 0;
            --    global_eth_vector_pointer <= eth_vector_pointer;

            --    wait_for_idle   := '1';
            --    payload_id_last := 0;
            --    v_tx_fn_ref     := 0;
            --    pk_offset       := 0;
            --    --while wait_for_idle = '1' loop
            --    --wait until payload_valid = '1';
            --    --for ii in 0 to ram_rd_stimuli_length -1 loop
            --    loop
            --        wait until payload_valid = '1';
            --        check(payload_length = C_FSO_PAYLOAD_SIZE, "ERROR: payload length is NOT C_FSO_PAYLOAD_SIZE");
            --        v_tx_fn   := to_integer(unsigned(tx_fn_out));
            --        v_payload := payload;
            --        --info("payload_valid");
            --        if frame_type_out = C_FRAME_IDLE then
            --            v_error := payload_test_health_check(payload);
            --            check(v_error = '1', "Data detected in IDLE payload.");
            --            v_tx_fn := to_integer(unsigned(tx_fn_out));
            --            info("RESEND: detected frame idle in here");
            --        elsif frame_type_out = C_FRAME_DATA then
            --            payload_test_read_payload_data(
            --                v_payload,
            --                pk_offset,
            --                v_packet,
            --                pk_size,
            --                v_eth_vector_id,
            --                v_eth_vector_length,
            --                eth_vector_pointer,
            --                split,
            --                payload_id_last,
            --                v_error);
            --            check(v_error = '0', "Payload health check failed.");
            --            global_eth_vector_pointer <= eth_vector_pointer;
            --            info("RESEND: detected frame data in here");
            --            if ram_rd_eth_vector_id(4) = v_eth_vector_id(eth_vector_pointer - 1) then
            --                exit;
            --            end if;
            --            --info("Payload_read_data()");
            --            report to_string(v_eth_vector_id(eth_vector_pointer - 1));
            --        end if;
            --        if ram_rd_eth_vector_id(4) = v_eth_vector_id(4) then
            --            exit;
            --        end if;
            --        --check(frame_type = C_FRAME_IDLE, "Expect frame type to be IDLE");
            --        v_tx_fn := to_integer(unsigned(tx_fn_out));

            --        --check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");

            --        wait until rising_edge(clk);
            --        v_tx_fn_ref := v_tx_fn_ref + 1;
            --    end loop;

            --    for jj in 0 to ram_rd_stimuli_length - 1 loop
            --        check(ram_rd_eth_vector_id(jj) = v_eth_vector_id(jj), "RESEND: Wrong Ethernet packet ID");
            --        report to_string(jj);
            --        report to_string(v_eth_vector_id(jj));
            --        check(ram_rd_eth_vector_length(jj) = v_eth_vector_length(jj), "RESEND:  Wrong Ethernet packet Length detected");
            --        check_false(ram_rd_eth_vector_length(jj) < 64, "Ethernet packet length can not be < 64.");
            --    end loop;

            --    wait until pt_finished = '1';
            --    wait for 21 us;
            --    ---------------------------------------------
            --    ----------- SECOND DATA BURST ---------------
            --    is_idle                   <= '0';
            --    for ii in 0 to eth_packet_length_array_t'length - 1 loop
            --        eth_vector_id(ii)     <= ii + 80;
            --        eth_vector_length(ii) <= 1044;
            --    end loop;
            --    stimuli_length            <= 5;
            --    WAIT UNTIL rising_edge(clk);
            --    start_stimuli             <= '1';
            --    WAIT UNTIL rising_edge(clk);
            --    start_stimuli             <= '0';
            --    wait until rising_edge(clk);
            --    eth_vector_pointer        := 0;
            --    back_pressure             <= '0';
            --    global_eth_vector_pointer <= eth_vector_pointer;
            --    wait_for_idle             := '1';
            --    payload_id_last           := 0;
            --    v_tx_fn_ref               := 0;
            --    pk_offset                 := 0;
            --    --while wait_for_idle = '1' loop
            --    for ii in 0 to stimuli_length - 1 loop

            --        wait until payload_valid = '1';
            --        check(payload_length = C_FSO_PAYLOAD_SIZE, "ERROR: payload length is NOT C_FSO_PAYLOAD_SIZE");
            --        v_tx_fn   := to_integer(unsigned(tx_fn_out));
            --        v_payload := payload;
            --        --info("payload_valid");
            --        if frame_type_out = C_FRAME_IDLE then
            --            v_error := payload_test_health_check(payload);
            --            check(v_error = '1', "Data detected in IDLE payload.");
            --            v_tx_fn := to_integer(unsigned(tx_fn_out));
            --            if v_tx_fn > 2 then
            --                wait_for_idle := '0';
            --            end if;
            --            info("BURST 2: detected frame idle in here");

            --        elsif frame_type_out = C_FRAME_DATA then
            --            payload_test_read_payload_data(
            --                v_payload,
            --                pk_offset,
            --                v_packet,
            --                pk_size,
            --                v_eth_vector_id,
            --                v_eth_vector_length,
            --                eth_vector_pointer,
            --                split,
            --                payload_id_last,
            --                v_error);
            --            check(v_error = '0', "Payload health check failed.");
            --            global_eth_vector_pointer <= eth_vector_pointer;
            --            info("BURST 2: detected frame DATA in here");
            --            if eth_vector_id(4) = v_eth_vector_id(4) then
            --                exit;
            --            end if;         --report to_string(v_eth_vector_length(eth_vector_pointer));
            --        end if;

            --        --check(frame_type = C_FRAME_IDLE, "Expect frame type to be IDLE");
            --        v_tx_fn := to_integer(unsigned(tx_fn_out));
            --        --check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");

            --        wait until rising_edge(clk);
            --        v_tx_fn_ref := v_tx_fn_ref + 1;
            --    end loop;

            --    for jj in 0 to stimuli_length - 1 loop
            --        check(eth_vector_id(jj) = v_eth_vector_id(jj), "Wrong Ethernet packet ID");
            --        --report to_string(v_eth_vector_length(jj));
            --        check(eth_vector_length(jj) = v_eth_vector_length(jj), "Wrong Ethernet packet Length detected");
            --        check_false(eth_vector_length(jj) < 64, "Ethernet packet length can not be < 64.");
            --    end loop;

            --    wait for 10 us;
            --    check(metadata_error = '0', "Metadata interface error");
            --    check(process_ram_wr_error = '0', "Process RAM WR error...");
            --    --check(process_ram_rd_error = '0', "Process RAM RD error...");
            --    info("Test finished");

            END IF;                     -- for test_suite
        END LOOP test_cases_loop;

        WAIT FOR 20 ns;
        test_runner_cleanup(runner);
    end process;


    inst_payload_transmiter : entity sda_test_lib.payload_transmiter
        port map(
            -- Global Signal Interface
            clk                        => clk, --: in std_logic;

            -- Transaction Interfaces
            tx_fn_init                 => (others=>'0'),
            axis_payload_if_m            => axis_payload_if_in_m, --: inout t_axis_payload_if;
            axis_payload_if_s            => axis_payload_if_in_s, --: inout t_axis_payload_if;
            eth_vector_id              => eth_vector_id, --: in eth_packet_id_array_t;
            eth_vector_length          => eth_vector_length, --stimuli_length_array, --: in eth_packet_length_array_t;
            stimuli_length             => stimuli_length, --: in natural;
            start_stimuli              => start_stimuli, --: in std_logic;
            is_idle                    => is_idle, --: in std_logic;
            finished                   => pt_finished, --: out std_logic;
            tx_fn_error_injection      => tx_fn_error_injection, --: in std_logic;
            frame_type_error_injection => frame_type_error_injection, --: in std_logic
            seqnum_error_injection     => seqnum_error_injection --: in std_logic
        );

    DUT : entity arq_lib.arq_transmitter_top
        generic map(
            --G_INTERNAL_RAM : boolean := true;
            G_INTERNAL_MEMORY_SIZE       => 64, -- Size in Frames
            G_CACHE_SIZE                 => 16,
            G_MEMORY_BLOCK_SIZE          => 256, -- A low value increases performance at a huge resource cost. Must be power of 2.
            G_CORE_COUNTER_PERIOD_CLOCKS => 100
        )
        port map(
            clk                     => clk, --: in std_logic;
            rst                     => reset, --: in std_logic;
            -- Config interface:
            arq_config_if           => arq_config_if, -- : in    t_arq_config_if;
            -- Input interfaces:
            axis_pl_if_in_m       => axis_payload_if_in_m, -- : inout t_axis_payload_if;
            axis_pl_if_in_s       => axis_payload_if_in_s, -- : inout t_axis_payload_if;
            -- Output interface:
            axis_pl_if_out_m      => axis_payload_if_out_m, -- : inout t_axis_payload_if;
            axis_pl_if_out_s      => axis_payload_if_out_s, -- : inout t_axis_payload_if;

            ack_data_if_m         => ack_data_if_m,   --: inout t_ack_data_if;
            ack_data_if_s         => ack_data_if_s,   --: inout t_ack_data_if;
            arq_hw_config       => arq_hw_config, --: out   t_arq_hw_config;
            clear_diag          => '0', --clear_diag,
            diag_control_if    => diag_control_if,       --: inout t_arq_control_tx_diagnostics_if;
            diag_ram_if         => diag_ram_if  
        );

    inst_payload_receiver : entity sda_test_lib.payload_receiver
        port map(
            -- Global Signal Interface
            clk             => clk,     --: in std_logic;

            -- Transaction Interfaces
            axis_payload_if_m => axis_router_if_out_m , --: inout t_axis_payload_if;
            axis_payload_if_s => axis_router_if_out_s , --: inout t_axis_payload_if;
            back_pressure   => back_pressure, --: in std_logic;
            tx_fn_out       => tx_fn_out, --: in std_logic_vector(15 downto 0);
            frame_type_out  => frame_type_out, --: in std_logic_vector(1 downto 0);
            tx_num_out      => tx_num_out, --: in std_logic_vector(1 downto 0);
            payload         => payload, --: out payload_array_t;
            payload_valid   => payload_valid, --: out std_logic;
            error           => error,   --: out std_logic;
            payload_length  => payload_length, --: out natural;
            seqnum          => seqnum   --: out natural;
        );

    Inst_input_buffer_bypass : entity sda_lib.axis_payload_fifo
        generic map(
            DISTR_RAM       => false,
            FIFO_DEPTH      => C_FSO_PAYLOAD_SIZE*200,
            FULL_THRESHOLD  => C_FSO_PAYLOAD_SIZE*200,
            EMPTY_THRESHOLD => 0
        )
        port map(
            clk                 => clk,
            rst                 => reset,
            axis_payload_if_in_m  => axis_pl_if_bypass_buf_in_m, --axis_payload_if_out,
            axis_payload_if_in_s  => axis_pl_if_bypass_buf_in_s, --axis_payload_if_out,
            axis_payload_if_out_m => axis_pl_if_bypass_buf_m,
            axis_payload_if_out_s => axis_pl_if_bypass_buf_s,
            input_almost_full   => open,
            input_almost_empty  => open,
            fifo_max_level      => open
        );

    inst_arq_axis_pl_router_out : entity arq_lib.arq_axis_pl_router_2x1
        port map(
            --Input interfaces
            axis_payload_if_in_a_m => axis_pl_if_ground_m, --: inout t_axis_payload_if;
            axis_payload_if_in_a_s => axis_pl_if_ground_s, --: inout t_axis_payload_if;
            axis_payload_if_in_b_m => axis_pl_if_bypass_buf_m, --: inout t_axis_payload_if;
            axis_payload_if_in_b_s => axis_pl_if_bypass_buf_s, --: inout t_axis_payload_if;
            sel                  => sel_router_out, --: in    std_logic;
            --Output interfaces
            axis_payload_if_out_m  => axis_router_if_out_m, --: inout t_axis_payload_if;
            axis_payload_if_out_s  => axis_router_if_out_s --: inout t_axis_payload_if;
        );
    
    axis_pl_if_ground_m <= C_AXIS_PAYLOAD_IF_MASTER_INIT;          

    inst_arq_axis_pl_router_back_pressure : entity arq_lib.arq_axis_pl_router_2x1
        port map(
            --Input interfaces
            axis_payload_if_in_a_m => axis_pl_if_ground_m, --: inout t_axis_payload_if;
            axis_payload_if_in_a_s => axis_pl_if_ground_s, --: inout t_axis_payload_if;
            axis_payload_if_in_b_m => axis_payload_if_out_m, --: inout t_axis_payload_if;
            axis_payload_if_in_b_s => axis_payload_if_out_s, --: inout t_axis_payload_if;
            sel                  => (not sel_router_back_pressure), --: in    std_logic;
            --Output interfaces
            axis_payload_if_out_m  => axis_pl_if_bypass_buf_in_m, --: inout t_axis_payload_if;
            axis_payload_if_out_s  => axis_pl_if_bypass_buf_in_s --: inout t_axis_payload_if;
        );

    test_runner_watchdog(runner, 3000 us);
end architecture;
