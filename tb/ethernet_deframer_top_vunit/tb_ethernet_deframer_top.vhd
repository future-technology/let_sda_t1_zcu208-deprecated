-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: tb_ethernet_deframer_top
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

library osvvm;
use osvvm.RandomPkg.all;
use osvvm.CoveragePkg.all;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

library ethernet_deframer_lib;
--use ethernet_framer_lib.txt_util.all;

library sda_test_lib;
use sda_test_lib.sda_test_pkg.all;
library sda_test_lib;
use sda_test_lib.eth_packet_test_pkg.all;
library sda_test_lib;
use sda_test_lib.packet_test_pkg.all;
library sda_test_lib;
use sda_test_lib.payload_test_pkg.all;

library sda_test_lib;
use sda_test_lib.ethernet_test_receiver;

entity tb_ethernet_deframer_top is
    generic(runner_cfg : string := runner_cfg_default);
end entity;

architecture tb of tb_ethernet_deframer_top is

    constant C_CLK_PERIOD  : time      := 10 ns;
    signal clk             : std_logic := '0';
    signal reset           : std_logic := '1';
    signal error           : std_logic;
    signal send            : std_logic := '0';
    signal payload_length  : natural;
    signal seqnum          : natural;
    signal packet_len      : natural   := 0;
    signal axis_payload_if_m : t_axis_payload_if_m;
    signal axis_payload_if_s : t_axis_payload_if_s;
    signal payload         : payload_array_t;
    signal payload_valid   : std_logic;
    signal tx_fn           : std_logic_vector(16 - 1 downto 0);
    signal frame_type      : std_logic_vector(2 - 1 downto 0);
    signal tx_fn_out       : std_logic_vector(16 - 1 downto 0);
    signal frame_type_out  : std_logic_vector(2 - 1 downto 0);

    signal start_stimuli        : std_logic := '0';
    signal random_stimuli_wait  : std_logic := '0';
    signal stimuli_length       : natural;
    type t_stimuli_length_array is array (0 to 500 - 1) of natural;
    signal stimuli_length_array : t_stimuli_length_array;

    signal eth_packet_length : natural;
    signal eth_packet_id     : natural;
    --eth_packet        :    eth_packet_array_t;
    signal eth_packet_valid  : std_logic;
    signal eth_packet_error  : std_logic;
    signal axis_ethernet_if_m  : t_axis_if_m;
    signal axis_ethernet_if_s  : t_axis_if_s;

    signal eth_vector_id              : eth_packet_id_array_t;
    signal eth_vector_length          : eth_packet_length_array_t;
    signal tx_fn_error_injection      : std_logic;
    signal frame_type_error_injection : std_logic;
    signal seqnum_error_injection     : std_logic;
    signal is_idle                    : std_logic;
    signal pt_finished                : std_logic;
    signal back_pressure              : std_logic := '0';
    signal eth_packet_good_cnt        : natural   := 0;

    shared variable rv  : RandomPType;
    shared variable rvt : RandomPType;

    --statistics:
    signal total_packet_counter          : std_logic_vector(31 downto 0);
    signal total_payload_counter         : std_logic_vector(31 downto 0);
    signal total_packet_splitted_counter : std_logic_vector(31 downto 0);
    signal total_packet_merged_counter   : std_logic_vector(31 downto 0);
    signal total_data_frame_counter      : std_logic_vector(31 downto 0);
    signal total_idle_frame_counter      : std_logic_vector(31 downto 0);
    signal total_payload_error_counter   : std_logic_vector(31 downto 0);
    signal total_packet_error_counter    : std_logic_vector(31 downto 0);
    signal watchdog_reset_counter        : std_logic_vector(31 downto 0);
    signal packet_filter_cnt_in          : std_logic_vector(31 downto 0);
    signal packet_filter_cnt_out         : std_logic_vector(31 downto 0);

    signal v_total_packet_counter          : natural := 0;
    signal v_total_payload_counter         : natural := 0;
    signal v_total_packet_splitted_counter : natural := 0;
    signal v_total_packet_merged_counter   : natural := 0;
    signal v_total_data_frame_counter      : natural := 0;
    signal v_total_idle_frame_counter      : natural := 0;
    signal v_total_mgmt_frame_counter      : natural := 0;
    signal v_total_payload_error_counter   : natural := 0;
    signal v_total_packet_error_counter    : natural := 0;
    signal v_watchdog_reset_counter        : natural := 0;
    signal v_packet_filter_cnt_in          : natural := 0;
    signal v_packet_filter_cnt_out         : natural := 0;

begin

    --------------------------------------------------------------------------
    -- CLOCK AND RESET.
    --------------------------------------------------------------------------
    clk   <= NOT clk after C_CLK_PERIOD / 2;
    reset <= '0' after 5 * (C_CLK_PERIOD);

    v_total_packet_counter          <= to_integer(unsigned(total_packet_counter));
    v_total_payload_counter         <= to_integer(unsigned(total_payload_counter));
    v_total_packet_splitted_counter <= to_integer(unsigned(total_packet_splitted_counter));
    v_total_packet_merged_counter   <= to_integer(unsigned(total_packet_merged_counter));
    v_total_data_frame_counter      <= to_integer(unsigned(total_data_frame_counter));
    v_total_idle_frame_counter      <= to_integer(unsigned(total_idle_frame_counter));
    v_total_payload_error_counter   <= to_integer(unsigned(total_payload_error_counter));
    v_total_packet_error_counter    <= to_integer(unsigned(total_packet_error_counter));
    v_watchdog_reset_counter        <= to_integer(unsigned(watchdog_reset_counter));
    v_packet_filter_cnt_in          <= to_integer(unsigned(packet_filter_cnt_in));
    v_packet_filter_cnt_out         <= to_integer(unsigned(packet_filter_cnt_out));

    test_runner : process
        variable v_tx_fn_ref              : natural := 0;
        variable v_tx_fn                  : natural := 0;
        variable v_packets_read           : natural := 0;
        variable v_error                  : std_logic;
        variable payload_id_last          : natural := 0;
        variable eth_vector_pointer       : natural;
        variable v_eth_vector_pointer_aux : natural := 0;
        variable pk_size                  : natural := 64;
        variable pk_offset                : natural := 0;
        variable v_packet                 : packet_array_t;
        variable split                    : std_logic;
        variable v_payload                : payload_array_t;
        variable data_flag                : std_logic;
        variable wait_for_idle            : std_logic;

    begin
        test_runner_setup(runner, runner_cfg);
        -- enable logging for passing check
        --show(get_logger(default_checker), display_handler, pass); 
        --show(get_logger(RED_CHECKER), display_handler, pass); 
        --packet_len <= 100;
        test_cases_loop : WHILE test_suite LOOP
            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- check IDLE payload generation.
            -- Expected Result: 10 IDLE payloads and TX_FN 
            ----------------------------------------------------------------------
            IF run("idle_payload_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: IDLE payload check");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                --random_stimuli_wait <= '0';
                seqnum_error_injection     <= '0';
                back_pressure              <= '0';
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                for ii in 0 to eth_packet_length_array_t'length - 1 loop
                    eth_vector_id(ii)     <= ii + 1;
                    eth_vector_length(ii) <= 1044;
                end loop;
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                --check(17 = 17, "simple test failed");

                -------------------------------
                is_idle        <= '1';
                stimuli_length <= 10;
                wait until rising_edge(clk);
                start_stimuli  <= '1';
                wait until rising_edge(clk);
                start_stimuli  <= '0';
                --wait until pt_finished = '1';
                --            -------------------------------
                wait for stimuli_length * 11 us;
                check(total_idle_frame_counter = std_logic_vector(to_unsigned(stimuli_length, total_idle_frame_counter'length)), "IDLE payloads does not match.");
                check(total_data_frame_counter = std_logic_vector(to_unsigned(0, total_data_frame_counter'length)), "Data Payload not expected.");
                wait for 2 us;
                info("===== TEST CASE FINISHED =====");

            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- Packet split check.
            -- Expected Result: Packet merged and delivered without errors
            ----------------------------------------------------------------------
            ELSIF run("packet_split_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: Packet split check");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli              <= '0';
                seqnum_error_injection     <= '0';
                back_pressure              <= '0';
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';

                eth_vector_pointer        := 0;
                WAIT UNTIL rising_edge(clk);
                stimuli_length            <= 10;
                for ii in 0 to eth_packet_length_array_t'length - 1 loop
                    eth_vector_id(ii) <= ii + 1;
                end loop;
                eth_vector_length(0 to 9) <= (1011, 1500, 1507, 1499, 1500, 1503, 1511, 1504, 1513, 1511);
                --for kk in 0 to 99 loop
                --    stimuli_length_array(kk) <= 83;
                --end loop;

                wait for 90 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                for ii in 0 to stimuli_length - 1 loop
                    wait until eth_packet_valid = '1';

                    check(eth_packet_error = '0', "Ethernet packet error!");
                    check(eth_packet_length = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                    check(eth_packet_id = eth_vector_id(ii), "Wrong Ethernet packet ID");
                    check_false(eth_packet_length < 64, "Ethernet packet length can not be < 64.");

                end loop;
                wait for 20 us;
                check(v_total_packet_counter = stimuli_length, "total_packet_counter error!");
                check(v_total_packet_splitted_counter > 0, "total_packet_splitted_counter error!");
                check(v_total_packet_merged_counter = v_total_packet_splitted_counter, "total_packet_merged_counter error!");
                check(v_total_payload_error_counter = 0, "total_payload_error_counter error!");
                check(v_total_packet_error_counter = 0, "total_packet_error_counter error!");
                check(v_watchdog_reset_counter = 0, "watchdog_reset_counter error!");
                check(v_packet_filter_cnt_in = stimuli_length, "packet_filter_cnt_in error!");
                check(v_packet_filter_cnt_out = stimuli_length, "packet_filter_cnt_out error!");

                info("Test finished");

            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- check that packet header is working with all alignment in the payload
            -- Expected Result: 
            ----------------------------------------------------------------------
            ELSIF run("packet_all_alignment_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: packet_all_alignment_check");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli              <= '0';
                seqnum_error_injection     <= '0';
                back_pressure              <= '0';
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';

                WAIT UNTIL rising_edge(clk);
                stimuli_length <= 50;
                for ii in 0 to eth_packet_length_array_t'length - 1 loop
                    eth_vector_id(ii)     <= ii + 1;
                    eth_vector_length(ii) <= 64 + ii;
                end loop;
                --eth_vector_length(0 to 9) <= (65, 66, 67, 68, 69, 70, 71, 72, 73, 74);
                --eth_vector_length(0 to 9) <= (1044, 1044, 1044, 1044, 1044, 1044, 1044, 1044, 1044, 1044);

                wait for 90 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                --check(1 = 1, "Ethernet packet error!");
                for ii in 0 to stimuli_length - 1 loop
                    wait until eth_packet_valid = '1';

                    check(eth_packet_error = '0', "Ethernet packet error!");
                    check(eth_packet_length = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                    check(eth_packet_id = eth_vector_id(ii), "Wrong Ethernet packet ID");
                    check_false(eth_packet_length < 64, "Ethernet packet length can not be < 64.");

                end loop;
                wait for 20 us;
                check(v_total_packet_counter = stimuli_length, "total_packet_counter error!");
                --check(v_total_packet_splitted_counter > 0, "total_packet_splitted_counter error!"); 
                check(v_total_packet_merged_counter = v_total_packet_splitted_counter, "total_packet_merged_counter error!");
                check(v_total_payload_error_counter = 0, "total_payload_error_counter error!");
                check(v_total_packet_error_counter = 0, "total_packet_error_counter error!");
                check(v_watchdog_reset_counter = 0, "watchdog_reset_counter error!");
                check(v_packet_filter_cnt_in = stimuli_length, "packet_filter_cnt_in error!");
                check(v_packet_filter_cnt_out = stimuli_length, "packet_filter_cnt_out error!");

                info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- check that packet header is working with all alignment in the payload
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("packet_ending_in_last_byte") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: packet_ending_in_last_byte");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli              <= '0';
                seqnum_error_injection     <= '0';
                back_pressure              <= '0';
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';

                WAIT UNTIL rising_edge(clk);
                stimuli_length       <= 2;
                for ii in 0 to eth_packet_length_array_t'length - 1 loop
                    eth_vector_id(ii) <= ii + 1;
                end loop;
                eth_vector_length(0) <= 519;
                eth_vector_length(1) <= 520;

                wait for 90 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                --check(1 = 1, "Ethernet packet error!");
                for ii in 0 to stimuli_length - 1 loop
                    wait until eth_packet_valid = '1';

                    check(eth_packet_error = '0', "Ethernet packet error!");
                    check(eth_packet_length = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                    check(eth_packet_id = eth_vector_id(ii), "Wrong Ethernet packet ID");
                    check_false(eth_packet_length < 64, "Ethernet packet length can not be < 64.");

                end loop;
                wait for 20 us;
                check(v_total_packet_counter = stimuli_length, "total_packet_counter error!");
                check(v_total_packet_splitted_counter = 0, "total_packet_splitted_counter error!");
                check(v_total_packet_merged_counter = v_total_packet_splitted_counter, "total_packet_merged_counter error!");
                check(v_total_payload_error_counter = 0, "total_payload_error_counter error!");
                check(v_total_packet_error_counter = 0, "total_packet_error_counter error!");
                check(v_watchdog_reset_counter = 0, "watchdog_reset_counter error!");
                check(v_packet_filter_cnt_in = stimuli_length, "packet_filter_cnt_in error!");
                check(v_packet_filter_cnt_out = stimuli_length, "packet_filter_cnt_out error!");

                info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- check that packet header is working with all alignment in the payload
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("pading_fill_at_end_of_frame") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: pading_fill_at_end_of_frame_1");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli              <= '0';
                seqnum_error_injection     <= '0';
                back_pressure              <= '0';
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';

                WAIT UNTIL rising_edge(clk);
                stimuli_length <= 2;
                for ii in 0 to eth_packet_length_array_t'length - 1 loop
                    eth_vector_id(ii)     <= ii + 1;
                    eth_vector_length(ii) <= 518; --518
                end loop;
                --eth_vector_length(0) <= 1044;
                --eth_vector_length(1) <= 1044;
                --eth_vector_length(2) <= 1044;
                --eth_vector_length(0 to 9) <= (65, 66, 67, 68, 69, 70, 71, 72, 73, 74);
                --eth_vector_length(0 to 9) <= (1044, 1044, 1044, 1044, 1044, 1044, 1044, 1044, 1044, 1044);

                wait for 90 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                --check(1 = 1, "Ethernet packet error!");
                for ii in 0 to stimuli_length - 1 loop
                    wait until eth_packet_valid = '1';

                    check(eth_packet_error = '0', "Ethernet packet error!");
                    check(eth_packet_length = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                    check(eth_packet_id = eth_vector_id(ii), "Wrong Ethernet packet ID");
                    check_false(eth_packet_length < 64, "Ethernet packet length can not be < 64.");

                end loop;
                wait for 20 us;
                check(v_total_packet_counter = stimuli_length, "total_packet_counter error!");
                check(v_total_packet_splitted_counter = 0, "total_packet_splitted_counter error!");
                check(v_total_packet_merged_counter = v_total_packet_splitted_counter, "total_packet_merged_counter error!");
                check(v_total_payload_error_counter = 0, "total_payload_error_counter error!");
                check(v_total_packet_error_counter = 0, "total_packet_error_counter error!");
                check(v_watchdog_reset_counter = 0, "watchdog_reset_counter error!");
                check(v_packet_filter_cnt_in = stimuli_length, "packet_filter_cnt_in error!");
                check(v_packet_filter_cnt_out = stimuli_length, "packet_filter_cnt_out error!");

                info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- check that packet header is working with all alignment in the payload
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("pading_fill_at_end_of_frame_2") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: pading_fill_at_end_of_frame_2");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli              <= '0';
                seqnum_error_injection     <= '0';
                back_pressure              <= '0';
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';

                WAIT UNTIL rising_edge(clk);
                stimuli_length <= 2;
                for ii in 0 to eth_packet_length_array_t'length - 1 loop
                    eth_vector_id(ii)     <= ii + 1;
                    eth_vector_length(ii) <= 519; --518
                end loop;
                --eth_vector_length(0) <= 1044;
                --eth_vector_length(1) <= 1044;
                --eth_vector_length(2) <= 1044;
                --eth_vector_length(0 to 9) <= (65, 66, 67, 68, 69, 70, 71, 72, 73, 74);
                --eth_vector_length(0 to 9) <= (1044, 1044, 1044, 1044, 1044, 1044, 1044, 1044, 1044, 1044);

                wait for 90 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                --check(1 = 1, "Ethernet packet error!");
                for ii in 0 to stimuli_length - 1 loop
                    wait until eth_packet_valid = '1';

                    check(eth_packet_error = '0', "Ethernet packet error!");
                    check(eth_packet_length = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                    check(eth_packet_id = eth_vector_id(ii), "Wrong Ethernet packet ID");
                    check_false(eth_packet_length < 64, "Ethernet packet length can not be < 64.");

                end loop;
                wait for 20 us;
                check(v_total_packet_counter = stimuli_length, "total_packet_counter error!");
                check(v_total_packet_splitted_counter = 0, "total_packet_splitted_counter error!");
                check(v_total_packet_merged_counter = v_total_packet_splitted_counter, "total_packet_merged_counter error!");
                check(v_total_payload_error_counter = 0, "total_payload_error_counter error!");
                check(v_total_packet_error_counter = 0, "total_packet_error_counter error!");
                check(v_watchdog_reset_counter = 0, "watchdog_reset_counter error!");
                check(v_packet_filter_cnt_in = stimuli_length, "packet_filter_cnt_in error!");
                check(v_packet_filter_cnt_out = stimuli_length, "packet_filter_cnt_out error!");

                info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- check that packet header is working with all alignment in the payload
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("packet_insertion_avoided_within_last_8_bytes_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: packet_insertion_avoided_within_last_8_bytes_check");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli              <= '0';
                seqnum_error_injection     <= '0';
                back_pressure              <= '0';
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';

                WAIT UNTIL rising_edge(clk);
                stimuli_length <= 6;
                for ii in 0 to eth_packet_length_array_t'length - 1 loop
                    eth_vector_id(ii) <= ii + 1;
                    --eth_vector_length(ii) <= 519;  --518
                end loop;
                eth_vector_length(0) <= 519;
                eth_vector_length(1) <= 519;
                eth_vector_length(2) <= 1039;
                eth_vector_length(3) <= 1038;
                eth_vector_length(4) <= 1040;
                eth_vector_length(5) <= 1040;
                wait for 90 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli        <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli        <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                --wait for 40 us;
                --check(1 = 1, "Ethernet packet error!");
                for ii in 0 to stimuli_length - 1 loop
                    info("Waiting for packet");
                    wait until eth_packet_valid = '1';

                    check(eth_packet_error = '0', "Ethernet packet error!");
                    check(eth_packet_length = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                    check(eth_packet_id = eth_vector_id(ii), "Wrong Ethernet packet ID");
                    check_false(eth_packet_length < 64, "Ethernet packet length can not be < 64.");

                end loop;
                wait for 20 us;
                check(v_total_packet_counter = stimuli_length, "total_packet_counter error!");
                check(v_total_packet_splitted_counter = 0, "total_packet_splitted_counter error!");
                check(v_total_packet_merged_counter = v_total_packet_splitted_counter, "total_packet_merged_counter error!");
                check(v_total_payload_error_counter = 0, "total_payload_error_counter error!");
                check(v_total_packet_error_counter = 0, "total_packet_error_counter error!");
                check(v_watchdog_reset_counter = 0, "watchdog_reset_counter error!");
                check(v_packet_filter_cnt_in = stimuli_length, "packet_filter_cnt_in error!");
                check(v_packet_filter_cnt_out = stimuli_length, "packet_filter_cnt_out error!");

                info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- check that packet header is working with all alignment in the payload
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("packet_split_in_3_frames_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: packet_split_in_3_frames_check");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli              <= '0';
                seqnum_error_injection     <= '0';
                back_pressure              <= '0';
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';

                WAIT UNTIL rising_edge(clk);
                stimuli_length <= 6;
                for ii in 0 to eth_packet_length_array_t'length - 1 loop
                    eth_vector_id(ii) <= ii + 1;
                    --eth_vector_length(ii) <= 519;  --518
                end loop;
                eth_vector_length(0) <= 1020;
                eth_vector_length(1) <= 1518;
                eth_vector_length(2) <= 450;
                eth_vector_length(3) <= 1513;
                eth_vector_length(4) <= 1513;
                eth_vector_length(5) <= 1513;
                wait for 90 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli        <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli        <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                --wait for 40 us;
                --check(1 = 1, "Ethernet packet error!");
                for ii in 0 to stimuli_length - 1 loop
                    info("Waiting for packet");
                    wait until eth_packet_valid = '1';

                    check(eth_packet_error = '0', "Ethernet packet error!");
                    check(eth_packet_length = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                    check(eth_packet_id = eth_vector_id(ii), "Wrong Ethernet packet ID");
                    check_false(eth_packet_length < 64, "Ethernet packet length can not be < 64.");

                end loop;
                wait for 20 us;
                check(v_total_packet_counter = stimuli_length, "total_packet_counter error!");
                check(v_total_packet_splitted_counter > 1, "total_packet_splitted_counter error!");
                check(v_total_packet_merged_counter = v_total_packet_splitted_counter, "total_packet_merged_counter error!");
                check(v_total_payload_error_counter = 0, "total_payload_error_counter error!");
                check(v_total_packet_error_counter = 0, "total_packet_error_counter error!");
                check(v_watchdog_reset_counter = 0, "watchdog_reset_counter error!");
                check(v_packet_filter_cnt_in = stimuli_length, "packet_filter_cnt_in error!");
                check(v_packet_filter_cnt_out = stimuli_length, "packet_filter_cnt_out error!");

                info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- Random stress test
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("random_stress_test_1") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: random_stress_test_1");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli              <= '0';
                seqnum_error_injection     <= '0';
                back_pressure              <= '0';
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';

                eth_vector_pointer   := 0;
                WAIT UNTIL rising_edge(clk);
                v_tx_fn_ref          := 0;
                stimuli_length       <= 50; --eth_packet_length_array_t'length;
                --RV.InitSeed(now / 1 ns);
                RV.InitSeed(752);
                eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);

                for ii in 0 to 50 - 1 loop
                    eth_vector_id(ii)     <= ii + 1;
                    eth_vector_length(ii) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                    --info("Len=" & to_string(eth_vector_length(ii)));
                end loop;

                wait for 90 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                --wait for 100 us;
                for ii in 0 to stimuli_length - 1 loop
                    wait until eth_packet_valid = '1';
                    check(eth_packet_error = '0', "Ethernet packet error!");
                    check(eth_packet_length = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                    check(eth_packet_id = eth_vector_id(ii), "Wrong Ethernet packet ID");
                    check_false(eth_packet_length < 64, "Ethernet packet length can not be < 64.");

                end loop;
                wait for 20 us;
                check(v_total_packet_counter = stimuli_length, "total_packet_counter error!");
                check(v_total_packet_splitted_counter > 0, "total_packet_splitted_counter error!");
                check(v_total_packet_merged_counter = v_total_packet_splitted_counter, "total_packet_merged_counter error!");
                check(v_total_payload_error_counter = 0, "total_payload_error_counter error!");
                check(v_total_packet_error_counter = 0, "total_packet_error_counter error!");
                check(v_watchdog_reset_counter = 0, "watchdog_reset_counter error!");
                check(v_packet_filter_cnt_in = stimuli_length, "packet_filter_cnt_in error!");
                check(v_packet_filter_cnt_out = stimuli_length, "packet_filter_cnt_out error!");

                info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- Random stress test
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("idle_data_alternation_check_1") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: idle_data_alternation_check_1");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli              <= '0';
                seqnum_error_injection     <= '0';
                back_pressure              <= '0';
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';

                -------------------------------
                -----------IDLE---------------
                is_idle        <= '1';
                stimuli_length <= 5;
                wait until rising_edge(clk);
                start_stimuli  <= '1';
                wait until rising_edge(clk);
                start_stimuli  <= '0';
                wait until pt_finished = '1';

                --wait for stimuli_length*11 us;
                --check(total_idle_frame_counter = std_logic_vector(to_unsigned(stimuli_length, total_idle_frame_counter'length)), "IDLE payloads does not match.");
                --check(total_data_frame_counter = std_logic_vector(to_unsigned(0, total_data_frame_counter'length)), "Data Payload not expected.");
                wait for 2 us;
                -------------------------------
                -------------------------------
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';
                WAIT UNTIL rising_edge(clk);

                stimuli_length       <= 20; --eth_packet_length_array_t'length;
                --RV.InitSeed(now / 1 ns);
                RV.InitSeed(145);
                eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);

                for ii in 0 to 20 - 1 loop
                    eth_vector_id(ii)     <= ii + 1;
                    eth_vector_length(ii) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                    --info("Len=" & to_string(eth_vector_length(ii)));
                end loop;

                wait for 90 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                --wait until pt_finished = '1';
                --wait for 2 us;
                for ii in 0 to stimuli_length - 1 loop
                    wait until eth_packet_valid = '1';
                    check(eth_packet_error = '0', "Ethernet packet error!");
                    check(eth_packet_length = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                    check(eth_packet_id = eth_vector_id(ii), "Wrong Ethernet packet ID");
                    check_false(eth_packet_length < 64, "Ethernet packet length can not be < 64.");

                end loop;
                wait for 20 us;
                check(v_total_packet_counter = stimuli_length, "total_packet_counter error!");
                check(v_total_packet_splitted_counter > 0, "total_packet_splitted_counter error!");
                check(v_total_packet_merged_counter = v_total_packet_splitted_counter, "total_packet_merged_counter error!");
                check(v_total_payload_error_counter = 0, "total_payload_error_counter error!");
                check(v_total_packet_error_counter = 0, "total_packet_error_counter error!");
                check(v_watchdog_reset_counter = 0, "watchdog_reset_counter error!");
                check(v_packet_filter_cnt_in = stimuli_length, "packet_filter_cnt_in error!");
                check(v_packet_filter_cnt_out = stimuli_length, "packet_filter_cnt_out error!");

                -------------------------------
                -----------IDLE---------------
                is_idle        <= '1';
                stimuli_length <= 5;
                wait until rising_edge(clk);
                start_stimuli  <= '1';
                wait until rising_edge(clk);
                start_stimuli  <= '0';
                --is_idle <= '0';
                wait until pt_finished = '1';

                --wait for stimuli_length*11 us;
                wait for 5 us;
                -------------------------------
                -------------------------------
                -------------------------------
                -------------------------------
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';
                WAIT UNTIL rising_edge(clk);

                stimuli_length       <= 20; --eth_packet_length_array_t'length;
                --RV.InitSeed(now / 1 ns);
                RV.InitSeed(555);
                eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);

                for ii in 0 to 20 - 1 loop
                    eth_vector_id(ii)     <= ii + 1;
                    eth_vector_length(ii) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                    --info("Len=" & to_string(eth_vector_length(ii)));
                end loop;

                wait for 190 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                --wait until pt_finished = '1';
                --wait for 2 us;
                for ii in 0 to stimuli_length - 1 loop
                    wait until eth_packet_valid = '1';
                    check(eth_packet_error = '0', "Ethernet packet error!");
                    check(eth_packet_length = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                    check(eth_packet_id = eth_vector_id(ii), "Wrong Ethernet packet ID");
                    check_false(eth_packet_length < 64, "Ethernet packet length can not be < 64.");

                end loop;
                wait for 20 us;
                check(v_total_packet_counter = stimuli_length * 2, "total_packet_counter error!");
                check(v_total_packet_splitted_counter > 0, "total_packet_splitted_counter error!");
                check(v_total_packet_merged_counter = v_total_packet_splitted_counter, "total_packet_merged_counter error!");
                check(v_total_payload_error_counter = 0, "total_payload_error_counter error!");
                check(v_total_packet_error_counter = 0, "total_packet_error_counter error!");
                check(v_watchdog_reset_counter = 0, "watchdog_reset_counter error!");
                check(v_packet_filter_cnt_in = stimuli_length * 2, "packet_filter_cnt_in error!");
                check(v_packet_filter_cnt_out = stimuli_length * 2, "packet_filter_cnt_out error!");
                --
                --
                check(total_idle_frame_counter = std_logic_vector(to_unsigned(10, total_idle_frame_counter'length)), "IDLE payloads does not match.");

                info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- Random stress test
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("idle_data_alternation_check_2") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: idle_data_alternation_check_2");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli              <= '0';
                seqnum_error_injection     <= '0';
                back_pressure              <= '0';
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';

                -------------------------------
                -----------IDLE---------------
                is_idle        <= '1';
                stimuli_length <= 5;
                wait until rising_edge(clk);
                start_stimuli  <= '1';
                wait until rising_edge(clk);
                start_stimuli  <= '0';
                wait until pt_finished = '1';

                --wait for stimuli_length*11 us;
                --check(total_idle_frame_counter = std_logic_vector(to_unsigned(stimuli_length, total_idle_frame_counter'length)), "IDLE payloads does not match.");
                --check(total_data_frame_counter = std_logic_vector(to_unsigned(0, total_data_frame_counter'length)), "Data Payload not expected.");
                wait for 2 us;
                -------------------------------
                -------------------------------
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';
                WAIT UNTIL rising_edge(clk);

                stimuli_length       <= 20; --eth_packet_length_array_t'length;
                --RV.InitSeed(now / 1 ns);
                RV.InitSeed(57);
                eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);

                for ii in 0 to 20 - 1 loop
                    eth_vector_id(ii)     <= ii + 1;
                    eth_vector_length(ii) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                    --info("Len=" & to_string(eth_vector_length(ii)));
                end loop;

                wait for 90 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                --wait until pt_finished = '1';
                --wait for 2 us;
                for ii in 0 to stimuli_length - 1 loop
                    wait until eth_packet_valid = '1';
                    check(eth_packet_error = '0', "Ethernet packet error!");
                    check(eth_packet_length = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                    check(eth_packet_id = eth_vector_id(ii), "Wrong Ethernet packet ID");
                    check_false(eth_packet_length < 64, "Ethernet packet length can not be < 64.");

                end loop;
                wait for 2 us;
                check(v_total_packet_counter = stimuli_length, "total_packet_counter error!");
                check(v_total_packet_splitted_counter > 0, "total_packet_splitted_counter error!");
                check(v_total_packet_merged_counter = v_total_packet_splitted_counter, "total_packet_merged_counter error!");
                check(v_total_payload_error_counter = 0, "total_payload_error_counter error!");
                check(v_total_packet_error_counter = 0, "total_packet_error_counter error!");
                check(v_watchdog_reset_counter = 0, "watchdog_reset_counter error!");
                check(v_packet_filter_cnt_in = stimuli_length, "packet_filter_cnt_in error!");
                check(v_packet_filter_cnt_out = stimuli_length, "packet_filter_cnt_out error!");

                -------------------------------
                -----------IDLE---------------
                is_idle        <= '1';
                stimuli_length <= 1;
                wait until rising_edge(clk);
                start_stimuli  <= '1';
                wait until rising_edge(clk);
                start_stimuli  <= '0';
                --is_idle <= '0';
                wait until pt_finished = '1';

                --wait for stimuli_length*11 us;
                wait for 5 us;
                -------------------------------

                -------------------------------
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';
                WAIT UNTIL rising_edge(clk);

                stimuli_length       <= 20; --eth_packet_length_array_t'length;
                --RV.InitSeed(now / 1 ns);
                RV.InitSeed(66);
                eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);

                for ii in 0 to 20 - 1 loop
                    eth_vector_id(ii)     <= ii + 1;
                    eth_vector_length(ii) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                    --info("Len=" & to_string(eth_vector_length(ii)));
                end loop;

                wait for 190 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli  <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli  <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                --wait until pt_finished = '1';
                --wait for 2 us;
                for ii in 0 to stimuli_length - 1 loop
                    wait until eth_packet_valid = '1';
                    check(eth_packet_error = '0', "Ethernet packet error!");
                    check(eth_packet_length = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                    check(eth_packet_id = eth_vector_id(ii), "Wrong Ethernet packet ID");
                    check_false(eth_packet_length < 64, "Ethernet packet length can not be < 64.");

                end loop;
                wait for 2 us;
                check(v_total_packet_counter = stimuli_length * 2, "total_packet_counter error!");
                check(v_total_packet_splitted_counter > 0, "total_packet_splitted_counter error!");
                check(v_total_packet_merged_counter = v_total_packet_splitted_counter, "total_packet_merged_counter error!");
                check(v_total_payload_error_counter = 0, "total_payload_error_counter error!");
                check(v_total_packet_error_counter = 0, "total_packet_error_counter error!");
                check(v_watchdog_reset_counter = 0, "watchdog_reset_counter error!");
                check(v_packet_filter_cnt_in = stimuli_length * 2, "packet_filter_cnt_in error!");
                check(v_packet_filter_cnt_out = stimuli_length * 2, "packet_filter_cnt_out error!");
                --
                --
                -------------------------------
                -----------IDLE---------------
                is_idle        <= '1';
                stimuli_length <= 5;
                wait until rising_edge(clk);
                start_stimuli  <= '1';
                wait until rising_edge(clk);
                start_stimuli  <= '0';
                --is_idle <= '0';
                wait until pt_finished = '1';

                --wait for stimuli_length*11 us;
                wait for 5 us;
                -------------------------------

                check(total_idle_frame_counter = std_logic_vector(to_unsigned(11, total_idle_frame_counter'length)), "IDLE payloads does not match.");

                info("Test finished");

            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- check IDLE payload generation.
            -- Expected Result: 10 IDLE payloads and TX_FN 
            ----------------------------------------------------------------------
            ELSIF run("idle_payload_frame_type_error_injection") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: idle_payload_frame_type_error_injection");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                --random_stimuli_wait <= '0';
                seqnum_error_injection     <= '0';
                back_pressure              <= '0';
                frame_type_error_injection <= '1';
                tx_fn_error_injection      <= '0';
                for ii in 0 to eth_packet_length_array_t'length - 1 loop
                    eth_vector_id(ii)     <= ii + 1;
                    eth_vector_length(ii) <= 1044;
                end loop;
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                --check(17 = 17, "simple test failed");

                -------------------------------
                is_idle        <= '1';
                stimuli_length <= 10;
                wait until rising_edge(clk);
                start_stimuli  <= '1';
                wait until rising_edge(clk);
                start_stimuli  <= '0';
                --wait until pt_finished = '1';
                --            -------------------------------
                wait for stimuli_length * 11 us;
                check(total_idle_frame_counter = std_logic_vector(to_unsigned(0, total_idle_frame_counter'length)), "IDLE payloads does not match.");
                check(total_data_frame_counter = std_logic_vector(to_unsigned(stimuli_length, total_data_frame_counter'length)), "Data Payload not expected.");
                check(total_payload_error_counter = std_logic_vector(to_unsigned(stimuli_length, total_payload_error_counter'length)), "Total error counter not expected");

                wait for 2 us;
                info("===== TEST CASE FINISHED =====");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- Random stress test
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("data_payload_frame_type_error_injection") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: data_payload_frame_type_error_injection");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli              <= '0';
                seqnum_error_injection     <= '0';
                back_pressure              <= '0';
                frame_type_error_injection <= '1';
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';

                eth_vector_pointer   := 0;
                WAIT UNTIL rising_edge(clk);
                v_tx_fn_ref          := 0;
                stimuli_length       <= 10; --eth_packet_length_array_t'length;
                --RV.InitSeed(now / 1 ns);
                RV.InitSeed(23);
                eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);

                for ii in 0 to 10 - 1 loop
                    eth_vector_id(ii)     <= ii + 1;
                    eth_vector_length(ii) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                    --info("Len=" & to_string(eth_vector_length(ii)));
                end loop;

                wait for 90 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                wait for 200 us;
                --for ii in 0 to stimuli_length-1 loop
                --    --wait until eth_packet_valid = '1';
                --    --check(eth_packet_error = '1', "Ethernet packet error!");
                --    --check(eth_packet_length = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                --    --check(eth_packet_id = eth_vector_id(ii), "Wrong Ethernet packet ID");
                --    --check_false(eth_packet_length < 64, "Ethernet packet length can not be < 64.");
                --end loop;

                wait for 20 us;
                check(v_total_packet_counter = 0, "total_packet_counter error!");
                check(v_total_packet_splitted_counter = 0, "total_packet_splitted_counter error!");
                check(v_total_packet_merged_counter = v_total_packet_splitted_counter, "total_packet_merged_counter error!");
                check(v_total_payload_error_counter = 0, "total_payload_error_counter error!");
                check(v_total_packet_error_counter = 0, "total_packet_error_counter error!");
                check(v_total_idle_frame_counter > 0, "total_idle_frame_counter error!");
                check(v_total_data_frame_counter = 0, "total_data_frame_counter error!");
                check(v_watchdog_reset_counter = 0, "watchdog_reset_counter error!");
                check(v_packet_filter_cnt_in = 0, "packet_filter_cnt_in error!");
                check(v_packet_filter_cnt_out = 0, "packet_filter_cnt_out error!");

                info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- Random stress test
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("data_payload_tx_fn_error_injection") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: data_payload_tx_fn_error_injection");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli              <= '0';
                seqnum_error_injection     <= '0';
                back_pressure              <= '0';
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '1';
                is_idle                    <= '0';

                eth_vector_pointer   := 0;
                WAIT UNTIL rising_edge(clk);
                v_tx_fn_ref          := 0;
                stimuli_length       <= 20; --eth_packet_length_array_t'length;
                --RV.InitSeed(now / 1 ns);
                RV.InitSeed(44);
                eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);

                for ii in 0 to 20 - 1 loop
                    eth_vector_id(ii)     <= ii + 1;
                    eth_vector_length(ii) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                    --info("Len=" & to_string(eth_vector_length(ii)));
                end loop;

                wait for 90 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                --wait for 300 us;
                for ii in 0 to stimuli_length - 1 loop
                    wait until eth_packet_valid = '1';
                    check(eth_packet_error = '0', "Ethernet packet error!");
                    check(eth_packet_length = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                    check(eth_packet_id = eth_vector_id(ii), "Wrong Ethernet packet ID");
                    check_false(eth_packet_length < 64, "Ethernet packet length can not be < 64.");
                end loop;

                wait for 20 us;
                check(v_total_packet_counter = stimuli_length, "total_packet_counter error!");
                check(v_total_packet_splitted_counter > 0, "total_packet_splitted_counter error!");
                check(v_total_packet_merged_counter = v_total_packet_splitted_counter, "total_packet_merged_counter error!");
                check(v_total_payload_error_counter = 0, "total_payload_error_counter error!");
                check(v_total_packet_error_counter = 0, "total_packet_error_counter error!");
                check(v_total_idle_frame_counter = 0, "total_idle_frame_counter error!");
                check(v_total_data_frame_counter > 0, "total_data_frame_counter error!");
                check(v_watchdog_reset_counter = 0, "watchdog_reset_counter error!");
                check(v_packet_filter_cnt_in = stimuli_length, "packet_filter_cnt_in error!");
                check(v_packet_filter_cnt_out = stimuli_length, "packet_filter_cnt_out error!");

                info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- Random stress test
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("data_payload_seqnum_error_injection") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: data_payload_seqnum_error_injection");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli              <= '0';
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                seqnum_error_injection     <= '1';
                back_pressure              <= '0';
                is_idle                    <= '0';

                eth_vector_pointer   := 0;
                WAIT UNTIL rising_edge(clk);
                v_tx_fn_ref          := 0;
                stimuli_length       <= 30; --eth_packet_length_array_t'length;
                --RV.InitSeed(now / 1 ns);
                RV.InitSeed(44);
                eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);

                for ii in 0 to 30 - 1 loop
                    eth_vector_id(ii)     <= ii + 1;
                    eth_vector_length(ii) <= 1511;
                    --eth_vector_length(ii) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                    --info("Len=" & to_string(eth_vector_length(ii)));
                end loop;
                eth_vector_length(0)  <= 1500;
                eth_vector_length(1)  <= 1511;
                eth_vector_length(2)  <= 1411;
                eth_vector_length(3)  <= 64;
                eth_vector_length(4)  <= 64;
                eth_vector_length(5)  <= 64;
                eth_vector_length(6)  <= 64;
                eth_vector_length(22) <= 64;
                eth_vector_length(23) <= 64;
                eth_vector_length(24) <= 64;
                eth_vector_length(25) <= 64;

                wait for 90 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                wait for 400 us;
                --for ii in 0 to stimuli_length-1 loop
                --    wait until eth_packet_valid = '1';
                --    check(eth_packet_error = '1', "Ethernet packet error!");
                --    check(eth_packet_length = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                --    check(eth_packet_id = eth_vector_id(ii), "Wrong Ethernet packet ID");
                --    check_false(eth_packet_length < 64, "Ethernet packet length can not be < 64.");
                --end loop;

                wait for 20 us;
                check(v_total_packet_counter > 0, "total_packet_counter error!");
                check(v_total_packet_splitted_counter > 0, "total_packet_splitted_counter error!");
                check_false(v_total_packet_merged_counter = v_total_packet_splitted_counter, "total_packet_merged_counter error!");
                check(v_total_payload_error_counter = 0, "total_payload_error_counter error!");
                check(v_total_packet_error_counter > 0, "total_packet_error_counter error!");
                check(v_total_idle_frame_counter = 0, "total_idle_frame_counter error!");
                check(v_total_data_frame_counter > 0, "total_data_frame_counter error!");
                check(v_watchdog_reset_counter = 0, "watchdog_reset_counter error!");
                check(v_packet_filter_cnt_in > 0, "packet_filter_cnt_in error!");
                check(v_packet_filter_cnt_out = 8, "packet_filter_cnt_out error!");
                check(eth_packet_good_cnt = 8, "eth_packet_good error!");
                info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- Random stress test
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("back_pressure_test") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: back_pressure_test");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli              <= '0';
                back_pressure              <= '1';
                seqnum_error_injection     <= '0';
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                is_idle                    <= '0';

                eth_vector_pointer   := 0;
                WAIT UNTIL rising_edge(clk);
                v_tx_fn_ref          := 0;
                stimuli_length       <= 100; --eth_packet_length_array_t'length;
                --RV.InitSeed(now / 1 ns);
                RV.InitSeed(752);
                eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                eth_vector_length(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);

                for ii in 0 to 100 - 1 loop
                    eth_vector_id(ii)     <= ii + 1;
                    eth_vector_length(ii) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                    --info("Len=" & to_string(eth_vector_length(ii)));
                end loop;

                wait for 90 ns;
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                wait for 350 us;
                back_pressure <= '0';

                for ii in 0 to stimuli_length - 1 loop
                    wait until eth_packet_valid = '1';
                    check(eth_packet_error = '0', "Ethernet packet error!");
                    check(eth_packet_length = eth_vector_length(ii), "Wrong Ethernet packet Length detected");
                    check(eth_packet_id = eth_vector_id(ii), "Wrong Ethernet packet ID");
                    check_false(eth_packet_length < 64, "Ethernet packet length can not be < 64.");

                end loop;
                wait for 20 us;
                check(v_total_packet_counter = stimuli_length, "total_packet_counter error!");
                check(v_total_packet_splitted_counter > 0, "total_packet_splitted_counter error!");
                check(v_total_packet_merged_counter = v_total_packet_splitted_counter, "total_packet_merged_counter error!");
                check(v_total_payload_error_counter = 0, "total_payload_error_counter error!");
                check(v_total_packet_error_counter = 0, "total_packet_error_counter error!");
                check(v_watchdog_reset_counter = 0, "watchdog_reset_counter error!");
                check(v_packet_filter_cnt_in = stimuli_length, "packet_filter_cnt_in error!");
                check(v_packet_filter_cnt_out = stimuli_length, "packet_filter_cnt_out error!");

                info("Test finished");

                --        ----------------------------------------------------------------------
                --        -- TEST CASE DESCRIPTION:
                --        -- Random stress test
                --        -- Expected Result: 
                --        ----------------------------------------------------------------------
                --        ELSIF run("random_stress_test_2") THEN
                --            info("--------------------------------------------------------------------------------");
                --            info("TEST CASE: random_stress_test_2");
                --            info("--------------------------------------------------------------------------------");
                --            WAIT UNTIL reset = '0';
                --            start_stimuli <= '0';
                --            random_stimuli_wait <= '0';
                --            
                --            eth_vector_pointer := 0;
                --            WAIT UNTIL rising_edge(clk);
                --            v_tx_fn_ref := 0;
                --            stimuli_length <= 500;
                --            --RV.InitSeed(now / 1 ns);
                --            RV.InitSeed(127);
                --            stimuli_length_array(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                --            stimuli_length_array(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                --
                --            for kk in 0 to 500-1 loop
                --                stimuli_length_array(kk) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                --            end loop;
                --            
                --            WAIT UNTIL rising_edge(clk);
                --            WAIT UNTIL rising_edge(clk);
                --            WAIT UNTIL rising_edge(clk);
                --            start_stimuli <= '1';
                --            WAIT UNTIL rising_edge(clk);
                --            start_stimuli <= '0';
                --            WAIT UNTIL rising_edge(clk);
                --            WAIT UNTIL rising_edge(clk);
                --
                --            wait_for_idle := '1';
                --            payload_id_last := 0;
                --            v_tx_fn_ref := 0;
                --            pk_offset := 0;
                --            while wait_for_idle = '1' loop
                --            --for ii in 0 to 7-1 loop
                --                wait until payload_valid = '1';
                --                v_tx_fn := to_integer(unsigned(tx_fn_out));
                --                v_payload := payload;
                --
                --                if frame_type_out = C_FRAME_IDLE then
                --                    v_error := payload_test_health_check(payload);  
                --                    check(v_error = '1', "Data detected in IDLE payload.");
                --                    v_tx_fn := to_integer(unsigned(tx_fn_out));
                --                    if v_tx_fn > 2 then
                --                        wait_for_idle := '0';
                --                    end if;
                --                    
                --                elsif frame_type_out = C_FRAME_DATA then
                --                    payload_test_read_payload_data(
                --                        v_payload,  
                --                        pk_offset, 
                --                        v_packet, 
                --                        pk_size, 
                --                        eth_vector_id, 
                --                        eth_vector_length, 
                --                        eth_vector_pointer,
                --                        split,
                --                        payload_id_last,
                --                        v_error);
                --                    check(v_error = '0', "Payload health check failed.");
                --                   
                --                end if;
                --
                --                --check(frame_type = C_FRAME_IDLE, "Expect frame type to be IDLE");
                --                check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");
                --
                --                wait until rising_edge(clk);
                --                v_tx_fn_ref := v_tx_fn_ref + 1;
                --            end loop;
                --            
                --            check(to_integer(unsigned(total_packet_counter)) = stimuli_length, "total_packet_counter does NOT match stimuli length");
                --
                --            for jj in 0 to stimuli_length-1 loop
                --                check(eth_vector_id(jj) = jj, "Wrong Ethernet packet ID");
                --                check(eth_vector_length(jj) = stimuli_length_array(jj), "Wrong Ethernet packet Length detected");
                --                check_false(eth_vector_length(jj) < 64, "Ethernet packet length can not be < 64.");    
                --            end loop;
                --            info("Test finished");
                --
                --        ----------------------------------------------------------------------
                --        -- TEST CASE DESCRIPTION:
                --        -- Random stress test
                --        -- Expected Result: 
                --        ----------------------------------------------------------------------
                --        ELSIF run("packet_random_arrival_stress_test") THEN
                --            info("--------------------------------------------------------------------------------");
                --            info("TEST CASE: packet_random_arrival_stress_test");
                --            info("--------------------------------------------------------------------------------");
                --            WAIT UNTIL reset = '0';
                --            start_stimuli <= '0';
                --            random_stimuli_wait <= '1';
                --            
                --            eth_vector_pointer := 0;
                --            WAIT UNTIL rising_edge(clk);
                --            v_tx_fn_ref := 0;
                --            stimuli_length <= 500;
                --            --RV.InitSeed(now / 1 ns);
                --            RV.InitSeed(333);
                --            stimuli_length_array(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                --            stimuli_length_array(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                --
                --            for kk in 0 to 500-1 loop
                --                stimuli_length_array(kk) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                --            end loop;
                --            
                --            WAIT UNTIL rising_edge(clk);
                --            WAIT UNTIL rising_edge(clk);
                --            WAIT UNTIL rising_edge(clk);
                --            start_stimuli <= '1';
                --            WAIT UNTIL rising_edge(clk);
                --            start_stimuli <= '0';
                --            WAIT UNTIL rising_edge(clk);
                --            WAIT UNTIL rising_edge(clk);
                --
                --            wait_for_idle := '1';
                --            payload_id_last := 0;
                --            v_tx_fn_ref := 0;
                --            pk_offset := 0;
                --            while eth_vector_pointer < stimuli_length loop
                --            --for ii in 0 to 7-1 loop
                --                wait until payload_valid = '1';
                --                v_tx_fn := to_integer(unsigned(tx_fn_out));
                --                v_payload := payload;
                --
                --                if frame_type_out = C_FRAME_IDLE then
                --                    v_error := payload_test_health_check(payload);  
                --                    check(v_error = '1', "Data detected in IDLE payload.");
                --                    v_tx_fn := to_integer(unsigned(tx_fn_out));
                --                    if v_tx_fn > 2 then
                --                        wait_for_idle := '0';
                --                    end if;
                --                    
                --                elsif frame_type_out = C_FRAME_DATA then
                --                    payload_test_read_payload_data(
                --                        v_payload,  
                --                        pk_offset, 
                --                        v_packet, 
                --                        pk_size, 
                --                        eth_vector_id, 
                --                        eth_vector_length, 
                --                        eth_vector_pointer,
                --                        split,
                --                        payload_id_last,
                --                        v_error);
                --                    check(v_error = '0', "Payload health check failed.");
                --                   
                --                end if;
                --
                --                --check(frame_type = C_FRAME_IDLE, "Expect frame type to be IDLE");
                --                check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");
                --
                --                wait until rising_edge(clk);
                --                v_tx_fn_ref := v_tx_fn_ref + 1;
                --            end loop;
                --            
                --            check(to_integer(unsigned(total_packet_counter)) = stimuli_length, "total_packet_counter does NOT match stimuli length");
                --
                --            for jj in 0 to stimuli_length-1 loop
                --                check(eth_vector_id(jj) = jj, "Wrong Ethernet packet ID");
                --                check(eth_vector_length(jj) = stimuli_length_array(jj), "Wrong Ethernet packet Length detected");
                --                check_false(eth_vector_length(jj) < 64, "Ethernet packet length can not be < 64.");    
                --            end loop;
                --            
                --            random_stimuli_wait <= '0';
                --            info("Test finished");

            END IF;                     -- for test_suite
        END LOOP test_cases_loop;

        WAIT FOR 20 ns;
        test_runner_cleanup(runner);
    end process;

    --pr_stimuli : process    
    --    variable v_wait : time;
    --begin
    --    send <= '0';
    --    RV.InitSeed(now / 1 ns);
    --    v_wait := rvt.RandTime(1 us, 50 us);
    --    wait until start_stimuli = '1';
    --    for ii in 0 to stimuli_length-1 loop
    --        wait until rising_edge(clk);
    --        packet_len <= stimuli_length_array(ii);
    --        send <= '1';
    --        wait until rising_edge(clk);
    --        send <= '0';
    --        wait until axis_ethernet_if.tlast = '1' and axis_ethernet_if.tvalid = '1';
    --
    --        --random wait:
    --        if random_stimuli_wait = '1' then
    --            v_wait := rvt.RandTime(50 ns, 150000 ns); --150000
    --            wait for v_wait;
    --        end if;
    --
    --    end loop;
    --
    --end process;

    inst_ethernet_test_receiver : entity sda_test_lib.ethernet_test_receiver
        port map(
            -- Global Signal Interface
            clk                 => clk, --: in    std_logic;
            -- Transaction Interfaces
            axis_ethernet_if_m    => axis_ethernet_if_m, --: inout t_axis_ethernet_if;
            axis_ethernet_if_s    => axis_ethernet_if_s, --: inout t_axis_ethernet_if;
            back_pressure       => back_pressure,
            eth_packet_length   => eth_packet_length, --: out   natural;
            eth_packet_id       => eth_packet_id, --: out   natural;
            eth_packet_valid    => eth_packet_valid, --: out   std_logic;
            eth_packet_error    => eth_packet_error, --: out   std_logic
            eth_packet_good_cnt => eth_packet_good_cnt
        );
    --eth_packet_valid <= '1';
    --axis_ethernet_if.tready <= '1';

    inst_payload_transmiter : entity sda_test_lib.payload_transmiter
        port map(
            -- Global Signal Interface
            clk                        => clk, --: in std_logic;

            -- Transaction Interfaces
            tx_fn_init                 => (others=>'0'),
            axis_payload_if_m            => axis_payload_if_m, --: inout t_axis_payload_if;
            axis_payload_if_s            => axis_payload_if_s, --: inout t_axis_payload_if;
            eth_vector_id              => eth_vector_id, --: in eth_packet_id_array_t;
            eth_vector_length          => eth_vector_length, --stimuli_length_array, --: in eth_packet_length_array_t;
            stimuli_length             => stimuli_length, --: in natural;
            start_stimuli              => start_stimuli, --: in std_logic;
            is_idle                    => is_idle, --: in std_logic;
            finished                   => pt_finished, --: out std_logic;
            tx_fn_error_injection      => tx_fn_error_injection, --: in std_logic;
            frame_type_error_injection => frame_type_error_injection, --: in std_logic
            seqnum_error_injection     => seqnum_error_injection --: in std_logic
        );

    DUT : entity ethernet_deframer_lib.ethernet_deframer_top
        port map(
            clk                           => clk, -- : in  std_logic;
            rst                           => reset, -- : in  std_logic;

            -- Input data from encoder
            ----------------------
            axis_payload_if_m               => axis_payload_if_m, -- : in  std_logic_vector(8 - 1 downto 0);
            axis_payload_if_s               => axis_payload_if_s, -- : in  std_logic_vector(8 - 1 downto 0);

            -- Output data to ethernet mac
            ----------------------
            axis_if_m                       => axis_ethernet_if_m, -- : out  std_logic_vector(8 - 1 downto 0);
            axis_if_s                       => axis_ethernet_if_s, -- : out  std_logic_vector(8 - 1 downto 0);

            -- Statistics
            ----------------------
            clear_stat                    => '0', -- : in  std_logic;

            total_packet_counter          => total_packet_counter, -- : out std_logic_vector(31 downto 0);
            total_payload_counter         => total_payload_counter, -- : out std_logic_vector(31 downto 0);
            total_packet_splitted_counter => total_packet_splitted_counter, -- : out std_logic_vector(31 downto 0);
            total_packet_merged_counter   => total_packet_merged_counter, -- : out std_logic_vector(31 downto 0);
            total_data_frame_counter      => total_data_frame_counter, -- : out std_logic_vector(31 downto 0);
            total_idle_frame_counter      => total_idle_frame_counter, -- : out std_logic_vector(31 downto 0);
            total_payload_error_counter   => total_payload_error_counter, -- : out std_logic_vector(31 downto 0);
            total_packet_error_counter    => total_packet_error_counter, -- : out std_logic_vector(31 downto 0);
            watchdog_reset_counter        => watchdog_reset_counter, -- : out std_logic_vector(31 downto 0);
            packet_filter_cnt_in          => packet_filter_cnt_in, -- : out std_logic_vector(31 downto 0);
            packet_filter_cnt_out         => packet_filter_cnt_out, -- : out std_logic_vector(31 downto 0);
            sda_debug                     => open -- : out sda_debug_t

        );

    test_runner_watchdog(runner, 2000 ms);
end architecture;
