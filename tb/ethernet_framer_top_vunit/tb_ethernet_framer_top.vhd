-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: tb_ethernet_framer_top
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

library osvvm;
use osvvm.RandomPkg.all;
use osvvm.CoveragePkg.all;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

library ethernet_framer_lib;
--use ethernet_framer_lib.txt_util.all;

library sda_test_lib;
use sda_test_lib.sda_test_pkg.all;
library sda_test_lib;
use sda_test_lib.eth_packet_test_pkg.all;
library sda_test_lib;
use sda_test_lib.packet_test_pkg.all;
library sda_test_lib;
use sda_test_lib.payload_test_pkg.all;

library sda_test_lib;
use sda_test_lib.ethernet_test_transmiter;

entity tb_ethernet_framer_top is
    generic(runner_cfg : string := runner_cfg_default);
end entity;

architecture tb of tb_ethernet_framer_top is

    constant C_CLK_PERIOD   : time      := 10 ns;
    signal clk              : std_logic := '0';
    signal reset            : std_logic := '1';
    signal error            : std_logic;
    signal send             : std_logic := '0';
    signal back_pressure    : std_logic := '0';
    signal payload_length   : natural;
    signal seqnum           : natural;
    signal packet_len       : natural   := 0;
    signal axis_ethernet_if_m : t_axis_if_m;
    signal axis_ethernet_if_s : t_axis_if_s;
    signal axis_payload_if_m  : t_axis_payload_if_m;
    signal axis_payload_if_s  : t_axis_payload_if_s;
    signal payload          : payload_array_t;
    signal payload_valid    : std_logic;
    signal tx_fn            : std_logic_vector(16 - 1 downto 0);
    signal frame_type       : std_logic_vector(2 - 1 downto 0);
    signal tx_fn_out        : std_logic_vector(16 - 1 downto 0);
    signal frame_type_out   : std_logic_vector(2 - 1 downto 0);

    signal start_stimuli        : std_logic := '0';
    signal random_stimuli_wait  : std_logic := '0';
    signal stimuli_length       : natural;
    type t_stimuli_length_array is array (0 to 500 - 1) of natural;
    signal stimuli_length_array : t_stimuli_length_array;

    signal total_packet_counter          : std_logic_vector(31 downto 0);
    signal total_packet_splitted_counter : std_logic_vector(31 downto 0);
    signal total_data_frame_counter      : std_logic_vector(31 downto 0);
    signal actual_data_frame_counter     : std_logic_vector(31 downto 0);
    signal total_idle_frame_counter      : std_logic_vector(31 downto 0);
    signal total_packet_drop             : std_logic_vector(31 downto 0);
    signal frame_length_error            : std_logic_vector(31 downto 0);
    signal clear_stat                    : std_logic := '0';
    signal total_prebuffer_packet_drop   : std_logic_vector(31 downto 0);
    signal bitrate_in                    : std_logic_vector(31 downto 0);
    signal packet_cnt_in                 : std_logic_vector(31 downto 0);

    constant C_CFG_PAUSE_DATA    : std_logic_vector(15 downto 0) := X"FFFF";
    signal tx_pause_valid        : std_logic;
    signal tx_pause_data         : std_logic_vector(15 downto 0);
    signal tx_pause_ready        : std_logic                     := '1';
    signal rx_fifo_skipped_frame : std_logic_vector(32 - 1 downto 0);

    signal r_continue_cnt : natural;
    signal r_pause_cnt    : natural;

    shared variable rv  : RandomPType;
    shared variable rvt : RandomPType;

begin

    --------------------------------------------------------------------------
    -- CLOCK AND RESET.
    --------------------------------------------------------------------------
    clk   <= NOT clk after C_CLK_PERIOD / 2;
    reset <= '0' after 5 * (C_CLK_PERIOD);

    test_runner : process
        variable v_tx_fn_ref              : natural := 0;
        variable v_tx_fn                  : natural := 0;
        variable v_packets_read           : natural := 0;
        variable v_error                  : std_logic;
        variable payload_id_last          : natural := 0;
        variable eth_vector_id            : eth_packet_id_array_t;
        variable eth_vector_length        : eth_packet_length_array_t;
        variable eth_vector_pointer       : natural;
        variable v_eth_vector_pointer_aux : natural := 0;
        variable pk_size                  : natural := 64;
        variable pk_offset                : natural := 0;
        variable v_packet                 : packet_array_t;
        variable split                    : std_logic;
        variable v_payload                : payload_array_t;
        variable data_flag                : std_logic;
        variable wait_for_idle            : std_logic;

    begin
        test_runner_setup(runner, runner_cfg);
        -- enable logging for passing check
        --show(get_logger(default_checker), display_handler, pass); 
        --show(get_logger(RED_CHECKER), display_handler, pass); 
        --packet_len <= 100;
        test_cases_loop : WHILE test_suite LOOP
            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- check IDLE payload generation.
            -- Expected Result: 10 IDLE payloads and TX_FN 
            ----------------------------------------------------------------------
            IF run("idle_payload_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: IDLE payload check");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                v_tx_fn_ref         := 0;
                random_stimuli_wait <= '0';

                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                v_tx_fn_ref := 0;
                for ii in 0 to 10 - 1 loop
                    wait until payload_valid = '1';
                    v_tx_fn     := to_integer(unsigned(tx_fn_out));
                    check(frame_type_out = C_FRAME_IDLE, "Expect frame type to be IDLE");
                    check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");
                    check(payload_length = C_FSO_PAYLOAD_SIZE, "ERROR: payload length is NOT C_FSO_PAYLOAD_SIZE");
                    wait until rising_edge(clk);
                    v_tx_fn_ref := v_tx_fn_ref + 1;
                end loop;
                --v_tx_fn_ref := 0;
                --check(v_tx_fn_ref = 23, "Fail test");

                wait for 2 us;
                info("===== TEST CASE FINISHED =====");

            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- check DATA payload generation.
            -- Expected Result: 10 IDLE payloads and TX_FN 
            ----------------------------------------------------------------------
            ELSIF run("packet_split_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: Packet split check");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli       <= '0';
                random_stimuli_wait <= '0';

                eth_vector_pointer           := 0;
                WAIT UNTIL rising_edge(clk);
                v_tx_fn_ref                  := 0;
                stimuli_length_array(0 to 9) <= (1013, 1503, 601, 1501, 1503, 121, 64, 64, 64, 64);
                --for kk in 0 to 99 loop
                --    stimuli_length_array(kk) <= 83;
                --end loop;
                stimuli_length               <= 4;
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                start_stimuli                <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli                <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                wait_for_idle   := '1';
                payload_id_last := 0;
                v_tx_fn_ref     := 0;
                pk_offset       := 0;
                while wait_for_idle = '1' loop
                    --for ii in 0 to 7-1 loop
                    wait until payload_valid = '1';
                    check(payload_length = C_FSO_PAYLOAD_SIZE, "ERROR: payload length is NOT C_FSO_PAYLOAD_SIZE");
                    v_tx_fn   := to_integer(unsigned(tx_fn_out));
                    v_payload := payload;

                    if frame_type_out = C_FRAME_IDLE then
                        v_error := payload_test_health_check(payload);
                        check(v_error = '1', "Data detected in IDLE payload.");
                        v_tx_fn := to_integer(unsigned(tx_fn_out));
                        if v_tx_fn > 2 then
                            wait_for_idle := '0';
                        end if;

                    elsif frame_type_out = C_FRAME_DATA then
                        payload_test_read_payload_data(
                            v_payload,
                            pk_offset,
                            v_packet,
                            pk_size,
                            eth_vector_id,
                            eth_vector_length,
                            eth_vector_pointer,
                            split,
                            payload_id_last,
                            v_error);
                        check(v_error = '0', "Payload health check failed.");

                    end if;

                    --check(frame_type = C_FRAME_IDLE, "Expect frame type to be IDLE");
                    check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");

                    wait until rising_edge(clk);
                    v_tx_fn_ref := v_tx_fn_ref + 1;
                end loop;

                for jj in 0 to stimuli_length - 1 loop
                    check(eth_vector_id(jj) = jj, "Wrong Ethernet packet ID");
                    check(eth_vector_length(jj) = stimuli_length_array(jj), "Wrong Ethernet packet Length detected");
                    check_false(eth_vector_length(jj) < 64, "Ethernet packet length can not be < 64.");
                end loop;
                info("Test finished");

            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- check that packet header is working with all alignment in the payload
            -- Expected Result: 
            ----------------------------------------------------------------------
            ELSIF run("packet_all_alignment_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: packet_all_alignment_check");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli       <= '0';
                random_stimuli_wait <= '0';

                eth_vector_pointer           := 0;
                WAIT UNTIL rising_edge(clk);
                v_tx_fn_ref                  := 0;
                stimuli_length_array(0 to 9) <= (64, 65, 66, 67, 68, 69, 70, 71, 72, 73);
                --for kk in 0 to 99 loop
                --    stimuli_length_array(kk) <= 83;
                --end loop;
                stimuli_length               <= 10;
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                start_stimuli                <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli                <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                wait_for_idle   := '1';
                payload_id_last := 0;
                v_tx_fn_ref     := 0;
                pk_offset       := 0;
                while wait_for_idle = '1' loop
                    --for ii in 0 to 7-1 loop
                    wait until payload_valid = '1';
                    check(payload_length = C_FSO_PAYLOAD_SIZE, "ERROR: payload length is NOT C_FSO_PAYLOAD_SIZE");
                    v_tx_fn   := to_integer(unsigned(tx_fn_out));
                    v_payload := payload;

                    if frame_type_out = C_FRAME_IDLE then
                        v_error := payload_test_health_check(payload);
                        check(v_error = '1', "Data detected in IDLE payload.");
                        v_tx_fn := to_integer(unsigned(tx_fn_out));
                        if v_tx_fn > 2 then
                            wait_for_idle := '0';
                        end if;

                    elsif frame_type_out = C_FRAME_DATA then
                        payload_test_read_payload_data(
                            v_payload,
                            pk_offset,
                            v_packet,
                            pk_size,
                            eth_vector_id,
                            eth_vector_length,
                            eth_vector_pointer,
                            split,
                            payload_id_last,
                            v_error);
                        check(v_error = '0', "Payload health check failed.");

                    end if;

                    --check(frame_type = C_FRAME_IDLE, "Expect frame type to be IDLE");
                    check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");

                    wait until rising_edge(clk);
                    v_tx_fn_ref := v_tx_fn_ref + 1;
                end loop;

                check(to_integer(unsigned(total_packet_counter)) = stimuli_length, "total_packet_counter does NOT match stimuli length");
                check(to_integer(unsigned(packet_cnt_in)) = stimuli_length, "packet_cnt_in should not be zero");

                for jj in 0 to stimuli_length - 1 loop
                    check(eth_vector_id(jj) = jj, "Wrong Ethernet packet ID");
                    check(eth_vector_length(jj) = stimuli_length_array(jj), "Wrong Ethernet packet Length detected");
                    check_false(eth_vector_length(jj) < 64, "Ethernet packet length can not be < 64.");
                end loop;
                info("Test finished");

            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- check that packet header is working with all alignment in the payload
            -- Expected Result: 
            ----------------------------------------------------------------------
            ELSIF run("packet_ending_in_last_byte") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: packet_ending_in_last_byte");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli       <= '0';
                random_stimuli_wait <= '0';

                eth_vector_pointer           := 0;
                WAIT UNTIL rising_edge(clk);
                v_tx_fn_ref                  := 0;
                stimuli_length_array(0 to 9) <= (1044, 1044, 1044, 1044, 1044, 1044, 1044, 1044, 1044, 1044);
                --for kk in 0 to 99 loop
                --    stimuli_length_array(kk) <= 83;
                --end loop;
                stimuli_length               <= 10;
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                start_stimuli                <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli                <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                wait_for_idle   := '1';
                payload_id_last := 0;
                v_tx_fn_ref     := 0;
                pk_offset       := 0;
                while wait_for_idle = '1' loop
                    --for ii in 0 to 7-1 loop
                    wait until payload_valid = '1';
                    check(payload_length = C_FSO_PAYLOAD_SIZE, "ERROR: payload length is NOT C_FSO_PAYLOAD_SIZE");
                    v_tx_fn   := to_integer(unsigned(tx_fn_out));
                    v_payload := payload;

                    if frame_type_out = C_FRAME_IDLE then
                        v_error := payload_test_health_check(payload);
                        check(v_error = '1', "Data detected in IDLE payload.");
                        v_tx_fn := to_integer(unsigned(tx_fn_out));
                        if v_tx_fn > 2 then
                            wait_for_idle := '0';
                        end if;

                    elsif frame_type_out = C_FRAME_DATA then
                        payload_test_read_payload_data(
                            v_payload,
                            pk_offset,
                            v_packet,
                            pk_size,
                            eth_vector_id,
                            eth_vector_length,
                            eth_vector_pointer,
                            split,
                            payload_id_last,
                            v_error);
                        check(v_error = '0', "Payload health check failed.");

                    end if;

                    --check(frame_type = C_FRAME_IDLE, "Expect frame type to be IDLE");
                    check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");

                    wait until rising_edge(clk);
                    v_tx_fn_ref := v_tx_fn_ref + 1;
                end loop;

                check(to_integer(unsigned(total_packet_counter)) = stimuli_length, "total_packet_counter does NOT match stimuli length");
                check(to_integer(unsigned(packet_cnt_in)) = stimuli_length, "packet_cnt_in should not be zero");

                for jj in 0 to stimuli_length - 1 loop
                    check(eth_vector_id(jj) = jj, "Wrong Ethernet packet ID");
                    check(eth_vector_length(jj) = stimuli_length_array(jj), "Wrong Ethernet packet Length detected");
                    check_false(eth_vector_length(jj) < 64, "Ethernet packet length can not be < 64.");
                end loop;
                info("Test finished");

            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- check that packet header is working with all alignment in the payload
            -- Expected Result: 
            ----------------------------------------------------------------------
            ELSIF run("packet_insertion_avoided_within_last_8_bytes_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: packet_insertion_avoided_within_last_8_bytes_check");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli       <= '0';
                random_stimuli_wait <= '0';

                eth_vector_pointer           := 0;
                WAIT UNTIL rising_edge(clk);
                v_tx_fn_ref                  := 0;
                stimuli_length_array(0 to 9) <= (1040, 1039, 1038, 1043, 1041, 1040, 1042, 1040, 1040, 1040);
                --for kk in 0 to 99 loop
                --    stimuli_length_array(kk) <= 83;
                --end loop;
                stimuli_length               <= 10;
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                start_stimuli                <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli                <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                wait_for_idle            := '1';
                payload_id_last          := 0;
                v_eth_vector_pointer_aux := 0;
                v_tx_fn_ref              := 0;
                pk_offset                := 0;
                while wait_for_idle = '1' loop
                    --for ii in 0 to 7-1 loop
                    wait until payload_valid = '1';
                    check(payload_length = C_FSO_PAYLOAD_SIZE, "ERROR: payload length is NOT C_FSO_PAYLOAD_SIZE");
                    v_tx_fn   := to_integer(unsigned(tx_fn_out));
                    v_payload := payload;

                    if frame_type_out = C_FRAME_IDLE then
                        v_error := payload_test_health_check(payload);
                        check(v_error = '1', "Data detected in IDLE payload.");
                        v_tx_fn := to_integer(unsigned(tx_fn_out));
                        if v_tx_fn > 2 then
                            wait_for_idle := '0';
                        end if;

                    elsif frame_type_out = C_FRAME_DATA then
                        payload_test_read_payload_data(
                            v_payload,
                            pk_offset,
                            v_packet,
                            pk_size,
                            eth_vector_id,
                            eth_vector_length,
                            eth_vector_pointer,
                            split,
                            payload_id_last,
                            v_error);
                        check(v_error = '0', "Payload health check failed.");
                        -- check that only one packet is sent per frame:
                        check(eth_vector_pointer - 1 = v_eth_vector_pointer_aux, "Only one packet should be sent per frame");
                        v_eth_vector_pointer_aux := eth_vector_pointer;
                    end if;

                    --check(frame_type = C_FRAME_IDLE, "Expect frame type to be IDLE");
                    check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");

                    wait until rising_edge(clk);
                    v_tx_fn_ref := v_tx_fn_ref + 1;
                end loop;

                check(to_integer(unsigned(total_packet_counter)) = stimuli_length, "total_packet_counter does NOT match stimuli length");
                check(to_integer(unsigned(packet_cnt_in)) = stimuli_length, "packet_cnt_in should not be zero");

                for jj in 0 to stimuli_length - 1 loop
                    check(eth_vector_id(jj) = jj, "Wrong Ethernet packet ID");
                    check(eth_vector_length(jj) = stimuli_length_array(jj), "Wrong Ethernet packet Length detected");
                    check_false(eth_vector_length(jj) < 64, "Ethernet packet length can not be < 64.");
                end loop;
                info("Test finished");

            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- Random stress test
            -- Expected Result: 
            ----------------------------------------------------------------------
            ELSIF run("random_stress_test_1") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: random_stress_test_1");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli       <= '0';
                random_stimuli_wait <= '0';

                eth_vector_pointer      := 0;
                WAIT UNTIL rising_edge(clk);
                v_tx_fn_ref             := 0;
                stimuli_length          <= 50;
                --RV.InitSeed(now / 1 ns);
                RV.InitSeed(77);
                stimuli_length_array(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                stimuli_length_array(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);

                for kk in 0 to 50 - 1 loop
                    stimuli_length_array(kk) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                end loop;

                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                wait_for_idle   := '1';
                payload_id_last := 0;
                v_tx_fn_ref     := 0;
                pk_offset       := 0;
                while wait_for_idle = '1' loop
                    --for ii in 0 to 7-1 loop
                    wait until payload_valid = '1';
                    check(payload_length = C_FSO_PAYLOAD_SIZE, "ERROR: payload length is NOT C_FSO_PAYLOAD_SIZE");
                    v_tx_fn   := to_integer(unsigned(tx_fn_out));
                    v_payload := payload;

                    if frame_type_out = C_FRAME_IDLE then
                        v_error := payload_test_health_check(payload);
                        check(v_error = '1', "Data detected in IDLE payload.");
                        v_tx_fn := to_integer(unsigned(tx_fn_out));
                        if v_tx_fn > 2 then
                            wait_for_idle := '0';
                        end if;

                    elsif frame_type_out = C_FRAME_DATA then
                        payload_test_read_payload_data(
                            v_payload,
                            pk_offset,
                            v_packet,
                            pk_size,
                            eth_vector_id,
                            eth_vector_length,
                            eth_vector_pointer,
                            split,
                            payload_id_last,
                            v_error);
                        check(v_error = '0', "Payload health check failed.");

                    end if;

                    --check(frame_type = C_FRAME_IDLE, "Expect frame type to be IDLE");
                    check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");

                    wait until rising_edge(clk);
                    v_tx_fn_ref := v_tx_fn_ref + 1;
                end loop;

                check(to_integer(unsigned(total_packet_counter)) = stimuli_length, "total_packet_counter does NOT match stimuli length");
                check(to_integer(unsigned(packet_cnt_in)) = stimuli_length, "packet_cnt_in should not be zero");

                for jj in 0 to stimuli_length - 1 loop
                    check(eth_vector_id(jj) = jj, "Wrong Ethernet packet ID");
                    check(eth_vector_length(jj) = stimuli_length_array(jj), "Wrong Ethernet packet Length detected");
                    check_false(eth_vector_length(jj) < 64, "Ethernet packet length can not be < 64.");
                end loop;
                info("Test finished");

            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- Random stress test
            -- Expected Result: 
            ----------------------------------------------------------------------
            ELSIF run("random_stress_test_2") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: random_stress_test_2");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli       <= '0';
                random_stimuli_wait <= '0';

                eth_vector_pointer      := 0;
                WAIT UNTIL rising_edge(clk);
                v_tx_fn_ref             := 0;
                stimuli_length          <= 50;
                --RV.InitSeed(now / 1 ns);
                RV.InitSeed(89);
                stimuli_length_array(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                stimuli_length_array(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);

                for kk in 0 to 50 - 1 loop
                    stimuli_length_array(kk) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                end loop;

                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                wait_for_idle   := '1';
                payload_id_last := 0;
                v_tx_fn_ref     := 0;
                pk_offset       := 0;
                while wait_for_idle = '1' loop
                    --for ii in 0 to 7-1 loop
                    wait until payload_valid = '1';
                    check(payload_length = C_FSO_PAYLOAD_SIZE, "ERROR: payload length is NOT C_FSO_PAYLOAD_SIZE");
                    v_tx_fn   := to_integer(unsigned(tx_fn_out));
                    v_payload := payload;

                    if frame_type_out = C_FRAME_IDLE then
                        v_error := payload_test_health_check(payload);
                        check(v_error = '1', "Data detected in IDLE payload.");
                        v_tx_fn := to_integer(unsigned(tx_fn_out));
                        if v_tx_fn > 2 then
                            wait_for_idle := '0';
                        end if;

                    elsif frame_type_out = C_FRAME_DATA then
                        payload_test_read_payload_data(
                            v_payload,
                            pk_offset,
                            v_packet,
                            pk_size,
                            eth_vector_id,
                            eth_vector_length,
                            eth_vector_pointer,
                            split,
                            payload_id_last,
                            v_error);
                        check(v_error = '0', "Payload health check failed.");

                    end if;

                    --check(frame_type = C_FRAME_IDLE, "Expect frame type to be IDLE");
                    check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");

                    wait until rising_edge(clk);
                    v_tx_fn_ref := v_tx_fn_ref + 1;
                end loop;

                check(to_integer(unsigned(total_packet_counter)) = stimuli_length, "total_packet_counter does NOT match stimuli length");
                check(to_integer(unsigned(packet_cnt_in)) = stimuli_length, "packet_cnt_in should not be zero");

                for jj in 0 to stimuli_length - 1 loop
                    check(eth_vector_id(jj) = jj, "Wrong Ethernet packet ID");
                    check(eth_vector_length(jj) = stimuli_length_array(jj), "Wrong Ethernet packet Length detected");
                    check_false(eth_vector_length(jj) < 64, "Ethernet packet length can not be < 64.");
                end loop;
                info("Test finished");

            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- Random stress test
            -- Expected Result: 
            ----------------------------------------------------------------------
            ELSIF run("packet_random_arrival_stress_test") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: packet_random_arrival_stress_test");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli       <= '0';
                random_stimuli_wait <= '1';

                eth_vector_pointer      := 0;
                WAIT UNTIL rising_edge(clk);
                v_tx_fn_ref             := 0;
                stimuli_length          <= 50;
                --RV.InitSeed(now / 1 ns);
                RV.InitSeed(555);
                stimuli_length_array(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                stimuli_length_array(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);

                for kk in 0 to 50 - 1 loop
                    stimuli_length_array(kk) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                end loop;

                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                wait_for_idle   := '1';
                payload_id_last := 0;
                v_tx_fn_ref     := 0;
                pk_offset       := 0;
                while eth_vector_pointer < stimuli_length loop
                    --for ii in 0 to 7-1 loop
                    wait until payload_valid = '1';
                    check(payload_length = C_FSO_PAYLOAD_SIZE, "ERROR: payload length is NOT C_FSO_PAYLOAD_SIZE");
                    v_tx_fn   := to_integer(unsigned(tx_fn_out));
                    v_payload := payload;

                    if frame_type_out = C_FRAME_IDLE then
                        v_error := payload_test_health_check(payload);
                        check(v_error = '1', "Data detected in IDLE payload.");
                        v_tx_fn := to_integer(unsigned(tx_fn_out));
                        if v_tx_fn > 2 then
                            wait_for_idle := '0';
                        end if;

                    elsif frame_type_out = C_FRAME_DATA then
                        payload_test_read_payload_data(
                            v_payload,
                            pk_offset,
                            v_packet,
                            pk_size,
                            eth_vector_id,
                            eth_vector_length,
                            eth_vector_pointer,
                            split,
                            payload_id_last,
                            v_error);
                        check(v_error = '0', "Payload health check failed.");

                    end if;

                    --check(frame_type = C_FRAME_IDLE, "Expect frame type to be IDLE");
                    check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");

                    wait until rising_edge(clk);
                    v_tx_fn_ref := v_tx_fn_ref + 1;
                end loop;

                check(to_integer(unsigned(total_packet_counter)) = stimuli_length, "total_packet_counter does NOT match stimuli length");
                check(to_integer(unsigned(packet_cnt_in)) = stimuli_length, "packet_cnt_in should not be zero");

                for jj in 0 to stimuli_length - 1 loop
                    check(eth_vector_id(jj) = jj, "Wrong Ethernet packet ID");
                    check(eth_vector_length(jj) = stimuli_length_array(jj), "Wrong Ethernet packet Length detected");
                    check_false(eth_vector_length(jj) < 64, "Ethernet packet length can not be < 64.");
                end loop;

                random_stimuli_wait <= '0';
                info("Test finished");

            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- Random stress test
            -- Expected Result: 
            ----------------------------------------------------------------------
            ELSIF run("pause_frame_generation_test") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: pause_frame_generation_test");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli       <= '0';
                back_pressure       <= '1';
                tx_pause_ready      <= '1';
                random_stimuli_wait <= '0';

                eth_vector_pointer      := 0;
                WAIT UNTIL rising_edge(clk);
                v_tx_fn_ref             := 0;
                stimuli_length          <= 100;
                --RV.InitSeed(now / 1 ns);
                RV.InitSeed(77);
                stimuli_length_array(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                stimuli_length_array(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);

                for kk in 0 to 100 - 1 loop
                    stimuli_length_array(kk) <= 1044;
                end loop;

                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                wait_for_idle   := '1';
                payload_id_last := 0;
                v_tx_fn_ref     := 0;
                pk_offset       := 0;
                wait for 700 us;
                back_pressure   <= '0';
                wait for 900 us;
                check(r_continue_cnt = 1, "ERROR: r_continue_cnt is NOT 1");
                check(r_pause_cnt > 10, "ERROR: r_pause_cnt cnt is too low");

                check(to_integer(unsigned(rx_fifo_skipped_frame)) > 0, "rx_fifo_skipped_frame should not be zero");
                check(to_integer(unsigned(packet_cnt_in)) > 0, "packet_cnt_in should not be zero");

                info("Test finished");

            END IF;                     -- for test_suite
        END LOOP test_cases_loop;

        WAIT FOR 20 ns;
        test_runner_cleanup(runner);
    end process;

    pr_stimuli : process
        variable v_wait : time;
    begin
        send   <= '0';
        RV.InitSeed(now / 1 ns);
        v_wait := rvt.RandTime(1 us, 50 us);
        wait until start_stimuli = '1';
        for ii in 0 to stimuli_length - 1 loop
            wait until rising_edge(clk);
            packet_len <= stimuli_length_array(ii);
            send       <= '1';
            wait until rising_edge(clk);
            send       <= '0';
            wait until axis_ethernet_if_m.tlast = '1' and axis_ethernet_if_m.tvalid = '1';

            --random wait:
            if random_stimuli_wait = '1' then
                v_wait := rvt.RandTime(50 ns, 150000 ns); --150000
                wait for v_wait;
            end if;

        end loop;

    end process;

    pr_pause_frame_read : process
        variable v_wait : time;
    begin
        r_continue_cnt <= 0;
        r_pause_cnt    <= 0;
        loop
            wait until tx_pause_valid = '1';
            if tx_pause_data = x"0000" then
                r_continue_cnt <= r_continue_cnt + 1;
            elsif tx_pause_data = C_CFG_PAUSE_DATA then
                r_pause_cnt <= r_pause_cnt + 1;
            end if;
        end loop;
        wait;

    end process;

    inst_ethernet_test_transmiter : entity sda_test_lib.ethernet_test_transmiter
        port map(
            -- Global Signal Interface
            clk              => clk,    --: in std_logic;

            -- Transaction Interfaces
            axis_ethernet_if_m  => axis_ethernet_if_m, --: inout t_axis_ethernet_if ;
            axis_ethernet_if_s  => axis_ethernet_if_s, --: inout t_axis_ethernet_if ;
            packet_length       => packet_len, --: in natural;
            inject_error        => '0',    --inject_error, --: in std_logic;
            send                => send    --: in std_logic

        );

    inst_payload_receiver : entity sda_test_lib.payload_receiver
        port map(
            -- Global Signal Interface
            clk             => clk,     --: in std_logic;

            -- Transaction Interfaces
            axis_payload_if_m => axis_payload_if_m, --: inout t_axis_payload_if;
            axis_payload_if_s => axis_payload_if_s, --: inout t_axis_payload_if;
            back_pressure   => back_pressure, --: in std_logic;
            tx_fn_out       => tx_fn_out, --: in std_logic_vector(15 downto 0);
            frame_type_out  => frame_type_out, --: in std_logic_vector(1 downto 0);
            tx_num_out      => open,    --: in std_logic_vector(1 downto 0);
            payload         => payload, --: out payload_array_t;
            payload_valid   => payload_valid, --: out std_logic;
            error           => error,   --: out std_logic;
            payload_length  => payload_length, --: out natural;
            seqnum          => seqnum   --: out natural;
        );

    DUT : entity ethernet_framer_lib.ethernet_framer_top
        generic map(
            G_MINIMUN_FREE_BYTES_TO_SPLIT => 8,
            G_STARTUP_DELAY               => 10
        )
        port map(
            clk                           => clk,
            rst                           => reset,
            -- Input data 
            ----------------------
            axis_if_m                       => axis_ethernet_if_m, --
            axis_if_s                       => axis_ethernet_if_s, --

            cfg_pause_data                => C_CFG_PAUSE_DATA, --: in std_logic_vector(15 downto 0);
            tx_pause_valid                => tx_pause_valid, --: out std_logic;
            tx_pause_data                 => tx_pause_data, --: out std_logic_vector(15 downto 0);
            tx_pause_ready                => tx_pause_ready, --: in std_logic;
            rx_fifo_skipped_frame         => rx_fifo_skipped_frame, --: out std_logic_vector(32 - 1 downto 0);

            -- Output data 
            ----------------------
            axis_payload_if_m               => axis_payload_if_m,
            axis_payload_if_s               => axis_payload_if_s,

            -- Statistics
            ----------------------
            clear_stat                    => clear_stat, --: in  std_logic;
            total_packet_counter          => total_packet_counter, --: out std_logic_vector(31 downto 0);
            total_packet_splitted_counter => total_packet_splitted_counter, --: out std_logic_vector(31 downto 0);
            total_data_frame_counter      => total_data_frame_counter, --: out std_logic_vector(31 downto 0);
            actual_data_frame_counter     => actual_data_frame_counter, --: out std_logic_vector(31 downto 0);
            total_idle_frame_counter      => total_idle_frame_counter, --: out std_logic_vector(31 downto 0);
            total_packet_drop             => total_packet_drop, --: out std_logic_vector(31 downto 0);
            total_prebuffer_packet_drop   => total_prebuffer_packet_drop, --: out std_logic_vector(31 downto 0);
            frame_length_error            => frame_length_error, --: out std_logic_vector(31 downto 0);
            bitrate_in                    => bitrate_in, --: out std_logic_vector(31 downto 0);
            packet_cnt_in                 => packet_cnt_in --: out std_logic_vector(31 downto 0)
        );

    test_runner_watchdog(runner, 1000 ms);
end architecture;
