-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: tb_frame_header_fields
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

library osvvm;
use osvvm.RandomPkg.all;
use osvvm.CoveragePkg.all;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

--library frame_header_lib;

library sda_test_lib;
use sda_test_lib.frame_receiver;

entity tb_frame_header_fields is
    generic(runner_cfg : string := runner_cfg_default);
end entity;

architecture tb of tb_frame_header_fields is

    constant C_CLK_PERIOD : time      := 5 ns;
    signal clk            : std_logic := '0';
    signal reset          : std_logic := '1';
    signal error          : std_logic;
    signal send           : std_logic := '0';
    signal frame_valid    : std_logic;

    signal axis_if_m             : t_axis_if_m;
    signal axis_if_s             : t_axis_if_s;
    signal frame_length        : natural;
    signal header_length       : natural;
    signal valid               : std_logic := '0';
    
    signal frame_header_fields_ref : frame_header_fields_t;
    signal frame_header_fields_out : frame_header_fields_t;
    signal ts_insertion_ref : t_ts_insertion_if;
    signal ts_insertion_out : t_ts_insertion_if;
    signal timestamp_data_array : t_timestamp_data_array;

    signal frame_full_ram        : frame_full_ram_t;
    signal frame_header_ram      : frame_header_ram_t;
    signal frame_header_ram_out  : frame_header_ram_t;


begin

    --------------------------------------------------------------------------
    -- CLOCK AND RESET.
    --------------------------------------------------------------------------
    clk   <= NOT clk after C_CLK_PERIOD / 2;
    reset <= '0' after 5 * (C_CLK_PERIOD);

    test_runner : process
        variable tx_fn      : std_logic_vector(15 downto 0); -- Sequence number of this Tx Frame
        variable frame_type : std_logic_vector(1 downto 0); -- Frame Type
        variable tx_num     : std_logic_vector(2 downto 0); -- Transmission atempt

    begin
        test_runner_setup(runner, runner_cfg);
        -- enable logging for passing check
        --show(get_logger(default_checker), display_handler, pass); 
        --show(get_logger(RED_CHECKER), display_handler, pass); 
        --packet_len <= 100;
        test_cases_loop : WHILE test_suite LOOP
            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- check IDLE payload generation.
            -- Expected Result: 10 IDLE payloads and TX_FN 
            ----------------------------------------------------------------------
            IF run("basic_header_fields_test") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: basic_header_fields_test");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                

                frame_header_fields_ref.txfn         <= x"1234";
                frame_header_fields_ref.ack_start_fn <= x"ACBD";
                frame_header_fields_ref.ack_span     <= "010";
                frame_header_fields_ref.ack_valid    <= '1';
                frame_header_fields_ref.ack          <= '0';
                frame_header_fields_ref.tx_num       <= "101";
                frame_header_fields_ref.arq_nframes  <= x"78";
                frame_header_fields_ref.arq_max_retx <= "110";
                frame_header_fields_ref.pl_rate      <= "1001";
                frame_header_fields_ref.frame_type   <= "10";
                frame_header_fields_ref.tx_ts        <= x"9A67511984";
                frame_header_fields_ref.tod_seconds  <= "110101";
                frame_header_fields_ref.ts_applies   <= "111";
                frame_header_fields_ref.fcch_opcode  <= "001110";
                frame_header_fields_ref.fcch_pl      <= x"DEAC";
                frame_header_fields_ref.zerotail     <= x"7652";

                wait for 1000 ns;
                frame_header_ram <= frame_header2ram(frame_header_fields_ref);    
                wait for 1000 ns;     
                frame_header_fields_out <= array2frame_header(frame_header_ram);         
                wait for 1000 ns;

                check(frame_header_fields_out.ack_start_fn= frame_header_fields_ref.ack_start_fn, "Missmatch: ack_start_fn!!!");
                check(frame_header_fields_out.ack_span    = frame_header_fields_ref.ack_span    , "Missmatch: ack_span    !!!");
                check(frame_header_fields_out.ack_valid   = frame_header_fields_ref.ack_valid   , "Missmatch: ack_valid   !!!");
                check(frame_header_fields_out.ack         = frame_header_fields_ref.ack         , "Missmatch: ack         !!!");
                check(frame_header_fields_out.tx_num      = frame_header_fields_ref.tx_num      , "Missmatch: tx_num      !!!");
                check(frame_header_fields_out.arq_nframes = frame_header_fields_ref.arq_nframes , "Missmatch: arq_nframes !!!");
                check(frame_header_fields_out.arq_max_retx= frame_header_fields_ref.arq_max_retx, "Missmatch: arq_max_retx!!!");
                check(frame_header_fields_out.pl_rate     = frame_header_fields_ref.pl_rate     , "Missmatch: pl_rate     !!!");
                check(frame_header_fields_out.frame_type  = frame_header_fields_ref.frame_type  , "Missmatch: frame_type  !!!");
                check(frame_header_fields_out.tx_ts       = frame_header_fields_ref.tx_ts       , "Missmatch: tx_ts       !!!");
                check(frame_header_fields_out.tod_seconds = frame_header_fields_ref.tod_seconds , "Missmatch: tod_seconds !!!");
                check(frame_header_fields_out.ts_applies  = frame_header_fields_ref.ts_applies  , "Missmatch: ts_applies  !!!");
                check(frame_header_fields_out.fcch_opcode = frame_header_fields_ref.fcch_opcode , "Missmatch: fcch_opcode !!!");
                check(frame_header_fields_out.fcch_pl     = frame_header_fields_ref.fcch_pl     , "Missmatch: fcch_pl     !!!");
                check(frame_header_fields_out.zerotail    = frame_header_fields_ref.zerotail    , "Missmatch: zerotail    !!!");
                
                wait for 1 us;
                info("===== TEST CASE FINISHED =====");

            ELSIF run("ts_insertion_fields_test") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: ts_insertion_fields_test");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                
                for ii in 0 to 7 - 1 loop
                    timestamp_data_array(ii) <= (others=>'0');
                end loop;

                ts_insertion_ref.tx_ts       <= x"9A67511984";
                ts_insertion_ref.tod_seconds <= "110101";
                ts_insertion_ref.ts_applies  <= "101";


                wait for 1000 ns;
                
                timestamp_data_array <= ts_insertion_if2array(ts_insertion_ref);    
                wait for 1000 ns;     
                ts_insertion_out <= array2ts_insertion_if(timestamp_data_array);         
                wait for 1000 ns;

                check(ts_insertion_out.tx_ts       = ts_insertion_ref.tx_ts       , "Missmatch: tx_ts      !!!");
                check(ts_insertion_out.tod_seconds = ts_insertion_ref.tod_seconds , "Missmatch: tod_seconds!!!");
                check(ts_insertion_out.ts_applies  = ts_insertion_ref.ts_applies  , "Missmatch: ts_applies !!!");

                wait for 1 us;
                info("===== TEST CASE FINISHED =====");

            ELSIF run("header_fields_test_2") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: header_fields_test_2");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                
                for ii in 0 to 18 - 1 loop
                    frame_header_ram(ii) <= (others=>'0');
                end loop;

                frame_header_fields_ref.txfn         <= x"0000";
                frame_header_fields_ref.ack_start_fn <= x"0000";
                frame_header_fields_ref.ack_span     <= "000";
                frame_header_fields_ref.ack_valid    <= '0';
                frame_header_fields_ref.ack          <= '0';
                frame_header_fields_ref.tx_num       <= "000";
                frame_header_fields_ref.arq_nframes  <= x"00";
                frame_header_fields_ref.arq_max_retx <= "000";
                frame_header_fields_ref.pl_rate      <= "0000";
                frame_header_fields_ref.frame_type   <= "00";
                frame_header_fields_ref.tx_ts        <= x"FFFFFFFFFF";
                frame_header_fields_ref.tod_seconds  <= "111111";
                frame_header_fields_ref.ts_applies   <= "111";
                frame_header_fields_ref.fcch_opcode  <= "000000";
                frame_header_fields_ref.fcch_pl      <= x"0000";
                frame_header_fields_ref.zerotail     <= x"0000";

                frame_header_ram(7) <= x"FE";
                frame_header_ram(8) <= x"FF";
                frame_header_ram(9) <= x"FF";
                frame_header_ram(10) <= x"FF";
                frame_header_ram(11) <= x"FF";
                frame_header_ram(12) <= x"FF";
                frame_header_ram(13) <= x"03";
                frame_header_ram(14) <= x"00";


                wait for 1000 ns;
                frame_header_fields_out <= array2frame_header(frame_header_ram);         
                wait for 1000 ns;
                check(frame_header_fields_out.ack_start_fn= frame_header_fields_ref.ack_start_fn, "Missmatch: ack_start_fn!!!");
                check(frame_header_fields_out.ack_span    = frame_header_fields_ref.ack_span    , "Missmatch: ack_span    !!!");
                check(frame_header_fields_out.ack_valid   = frame_header_fields_ref.ack_valid   , "Missmatch: ack_valid   !!!");
                check(frame_header_fields_out.ack         = frame_header_fields_ref.ack         , "Missmatch: ack         !!!");
                check(frame_header_fields_out.tx_num      = frame_header_fields_ref.tx_num      , "Missmatch: tx_num      !!!");
                check(frame_header_fields_out.arq_nframes = frame_header_fields_ref.arq_nframes , "Missmatch: arq_nframes !!!");
                check(frame_header_fields_out.arq_max_retx= frame_header_fields_ref.arq_max_retx, "Missmatch: arq_max_retx!!!");
                check(frame_header_fields_out.pl_rate     = frame_header_fields_ref.pl_rate     , "Missmatch: pl_rate     !!!");
                check(frame_header_fields_out.frame_type  = frame_header_fields_ref.frame_type  , "Missmatch: frame_type  !!!");
                check(frame_header_fields_out.tx_ts       = frame_header_fields_ref.tx_ts       , "Missmatch: tx_ts       !!!");
                check(frame_header_fields_out.tod_seconds = frame_header_fields_ref.tod_seconds , "Missmatch: tod_seconds !!!");
                check(frame_header_fields_out.ts_applies  = frame_header_fields_ref.ts_applies  , "Missmatch: ts_applies  !!!");
                check(frame_header_fields_out.fcch_opcode = frame_header_fields_ref.fcch_opcode , "Missmatch: fcch_opcode !!!");
                check(frame_header_fields_out.fcch_pl     = frame_header_fields_ref.fcch_pl     , "Missmatch: fcch_pl     !!!");
                check(frame_header_fields_out.zerotail    = frame_header_fields_ref.zerotail    , "Missmatch: zerotail    !!!");
                
                wait for 1000 ns;
                frame_header_ram_out <= frame_header2ram(frame_header_fields_out);    
                wait for 1000 ns;
                for ii in 0 to 18 - 1 loop
                    check(frame_header_ram_out(ii) = frame_header_ram(ii), "Error in frameheader ram array");
                end loop;

                wait for 1 us;
                info("===== TEST CASE FINISHED =====");

            END IF;                     
        END LOOP test_cases_loop; -- for test_suite

        WAIT FOR 20 ns;
        test_runner_cleanup(runner);
    end process;

    test_runner_watchdog(runner, 50 ms);
end architecture;
