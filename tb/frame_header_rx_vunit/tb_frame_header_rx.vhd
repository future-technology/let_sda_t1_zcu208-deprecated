-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: tb_frame_header_rx
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

library osvvm;
use osvvm.RandomPkg.all;
use osvvm.CoveragePkg.all;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

library frame_header_lib;

library sda_test_lib;
use sda_test_lib.frame_transmiter;
use sda_test_lib.sda_test_pkg.all;
use sda_test_lib.eth_packet_test_pkg.all;
use sda_test_lib.packet_test_pkg.all;
use sda_test_lib.payload_test_pkg.all;
use sda_test_lib.frame_test_pkg.all;

entity tb_frame_header_rx is
    generic(runner_cfg : string := runner_cfg_default);
end entity;

architecture tb of tb_frame_header_rx is

    constant C_CLK_PERIOD : time      := 10 ns;
    signal clk            : std_logic := '0';
    signal reset          : std_logic := '1';
    signal error          : std_logic;
    signal send           : std_logic := '0';
    signal frame_valid    : std_logic;

    signal axis_if_m             : t_axis_if_m;
    signal axis_if_s             : t_axis_if_s;
    signal axis_payload_if_m     : t_axis_payload_if_m;
    signal axis_payload_if_s     : t_axis_payload_if_s;
    signal frame_header_fields : frame_header_fields_t;
    signal payload_ram         : payload_ram_t;
    signal payload_ram_ref     : payload_ram_t;
    signal frame_length        : natural;
    signal header_length       : natural;
    signal valid               : std_logic;

    signal ack_data_if_m   : t_ack_data_if_m; -- ACK data interface
    signal ack_data_if_s   : t_ack_data_if_s; -- ACK data interface
    signal ack_data_if_a_m : t_ack_data_if_m; -- ACK data interface
    signal ack_data_if_a_s : t_ack_data_if_s; -- ACK data interface
    signal ack_data_if_b_m : t_ack_data_if_m; -- ACK data interface   
    signal ack_data_if_b_s : t_ack_data_if_s; -- ACK data interface   
    signal fcch_if_m       : t_fcch_if_m;   -- FCCH interface
    signal fcch_if_s       : t_fcch_if_s;   -- FCCH interface
    signal fcch_if_a_m     : t_fcch_if_m;   -- FCCH interface
    signal fcch_if_a_s     : t_fcch_if_s;   -- FCCH interface
    signal fcch_if_b_m     : t_fcch_if_m;   -- FCCH interface
    signal fcch_if_b_s     : t_fcch_if_s;   -- FCCH interface
    signal pl_rate       : std_logic_vector(3 downto 0); -- Payload data rate

    signal start_stimuli       : std_logic := '0';
    signal random_stimuli_wait : std_logic := '0';

    signal start_fcch_stimuli : std_logic;
    signal start_ack_stimuli  : std_logic;

    type t_tx_fn_array is array (5 - 1 downto 0) of std_logic_vector(15 downto 0); -- Sequence number of this Tx Frame
    type t_frame_type_array is array (5 - 1 downto 0) of std_logic_vector(1 downto 0); -- Frame Type
    type t_tx_num_array is array (5 - 1 downto 0) of std_logic_vector(2 downto 0); -- Transmission atempt
    type t_pl_rate_array is array (5 - 1 downto 0) of std_logic_vector(4 - 1 downto 0); -- Transmission atempt

    signal tx_fn_array      : t_tx_fn_array;
    signal frame_type_array : t_frame_type_array;
    signal tx_num_array     : t_tx_num_array;
    signal pl_rate_array    : t_pl_rate_array;

    signal stimuli_id_array           : eth_packet_id_array_t;
    signal stimuli_length_array       : eth_packet_length_array_t;
    signal frame_header_fields_array  : frame_header_fields_array_t;
    signal stimuli_length             : natural;
    signal ack_stimuli_length         : natural;
    signal is_idle                    : std_logic;
    signal finished                   : std_logic;
    signal tx_fn_error_injection      : std_logic;
    signal frame_type_error_injection : std_logic;
    signal seqnum_error_injection     : std_logic;

    signal tx_fn          : std_logic_vector(15 downto 0);
    signal frame_type     : std_logic_vector(1 downto 0);
    signal tx_fn_out      : std_logic_vector(15 downto 0);
    signal frame_type_out : std_logic_vector(1 downto 0);
    signal tx_num_out     : std_logic_vector(2 downto 0);
    signal payload        : payload_array_t;
    signal payload_valid  : std_logic;
    signal payload_length : natural;
    signal seqnum         : natural;

    type t_ack_data_if_array is array (0 to 100 - 1) of t_ack_data_if_m;
    type t_fcch_if_array is array (0 to 100 - 1) of t_fcch_if_m;
    signal ack_data_if_array : t_ack_data_if_array;
    signal fcch_if_array     : t_fcch_if_array;

begin

    --------------------------------------------------------------------------
    -- CLOCK AND RESET.
    --------------------------------------------------------------------------
    clk   <= NOT clk after C_CLK_PERIOD / 2;
    reset <= '0' after 5 * (C_CLK_PERIOD);

    test_runner : process
        variable v_tx_fn_ref              : natural := 0;
        variable v_tx_fn                  : natural := 0;
        variable v_packets_read           : natural := 0;
        variable v_error                  : std_logic;
        variable payload_id_last          : natural := 0;
        variable eth_vector_id            : eth_packet_id_array_t;
        variable eth_vector_length        : eth_packet_length_array_t;
        variable eth_vector_pointer       : natural;
        variable v_eth_vector_pointer_aux : natural := 0;
        variable pk_size                  : natural := 64;
        variable pk_offset                : natural := 0;
        variable v_packet                 : packet_array_t;
        variable split                    : std_logic;
        variable v_payload                : payload_array_t;
        variable data_flag                : std_logic;
        variable wait_for_idle            : std_logic;
        variable v_frame_cnt              : natural;
    begin
        test_runner_setup(runner, runner_cfg);
        -- enable logging for passing check
        --show(get_logger(default_checker), display_handler, pass); 
        --show(get_logger(RED_CHECKER), display_handler, pass); 
        --packet_len <= 100;
        test_cases_loop : WHILE test_suite LOOP
            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- check IDLE payload generation.
            -- Expected Result: 10 IDLE payloads and TX_FN 
            ----------------------------------------------------------------------
            IF run("basic_functionality_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: basic_functionality_check");
                info("--------------------------------------------------------------------------------");
                tx_fn_error_injection      <= '0';
                frame_type_error_injection <= '0';
                seqnum_error_injection     <= '0';

                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                ack_stimuli_length <= 20;
                v_frame_cnt        := 0;
                -- set eth_vector array of stimuli
                for kk in 0 to 50 - 1 loop
                    stimuli_length_array(kk) <= 1044;
                    stimuli_id_array(kk)     <= kk + 1;
                end loop;

                -- set framer_header_fields array of stimuli
                for kk in 0 to 50 - 1 loop
                    frame_header_fields_array(kk).txfn         <= std_logic_vector(to_unsigned(kk, 16));
                    frame_header_fields_array(kk).ack_start_fn <= (others => '0');
                    frame_header_fields_array(kk).ack_span     <= (others => '0');
                    frame_header_fields_array(kk).ack_valid    <= '0';
                    frame_header_fields_array(kk).ack          <= '0';
                    frame_header_fields_array(kk).tx_num       <= "100";
                    frame_header_fields_array(kk).pl_rate      <= "1100";
                    frame_header_fields_array(kk).frame_type   <= C_FRAME_DATA;
                    frame_header_fields_array(kk).tx_ts        <= (others => '0');
                    frame_header_fields_array(kk).fcch_opcode  <= C_FCCH_NOT_PRESENT;
                    frame_header_fields_array(kk).fcch_pl      <= (others => '0');
                    frame_header_fields_array(kk).zerotail     <= (others => '0');
                end loop;

                for kk in 10 to 10 + 20 - 1 loop
                    frame_header_fields_array(kk).ack_start_fn <= std_logic_vector(to_unsigned(kk, 16));
                    frame_header_fields_array(kk).ack_span     <= "010";
                    frame_header_fields_array(kk).ack_valid    <= '1';
                    frame_header_fields_array(kk).ack          <= '1';
                    frame_header_fields_array(kk).fcch_opcode  <= "111110";
                    frame_header_fields_array(kk).fcch_pl      <= std_logic_vector(to_unsigned(kk, 16));
                end loop;
                -- set stimuli length
                stimuli_length <= 50;
                -- is idle? error injection?
                is_idle        <= '0';
                -- start_stimuli
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                start_stimuli  <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli  <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                -- read waiting for payload valid

                -- convert payload to eth packets
                wait_for_idle            := '1';
                payload_id_last          := 0;
                v_eth_vector_pointer_aux := 0;
                v_tx_fn_ref              := 0;
                pk_offset                := 0;
                --while wait_for_idle = '1' loop
                for ik in 0 to 50 - 1 loop
                    wait until payload_valid = '1';
                    --check(payload_length = C_FSO_PAYLOAD_SIZE, "ERROR: payload length is NOT C_FSO_PAYLOAD_SIZE");
                    v_tx_fn   := to_integer(unsigned(axis_payload_if_m.tx_fn));
                    v_payload := payload;

                    check(frame_header_fields_array(v_frame_cnt).tx_num = tx_num_out, "Wrong tx_num.");
                    check(frame_header_fields_array(v_frame_cnt).txfn = tx_fn_out, "Wrong tx_fn.");
                    check(frame_header_fields_array(v_frame_cnt).frame_type = frame_type_out, "Wrong frame_type.");

                    if axis_payload_if_m.frame_type = C_FRAME_IDLE then
                        v_error := payload_test_health_check(payload);
                        check(v_error = '1', "Data detected in IDLE payload.");
                        v_tx_fn := to_integer(unsigned(axis_payload_if_m.tx_fn));
                        if v_tx_fn > 2 then
                            wait_for_idle := '0';
                        end if;

                    elsif axis_payload_if_m.frame_type = C_FRAME_DATA then
                        payload_test_read_payload_data(
                            v_payload,
                            pk_offset,
                            v_packet,
                            pk_size,
                            eth_vector_id,
                            eth_vector_length,
                            eth_vector_pointer,
                            split,
                            payload_id_last,
                            v_error);
                        check(v_error = '0', "Payload health check failed.");
                        -- check that only one packet is sent per frame:
                        check(eth_vector_pointer - 1 = v_eth_vector_pointer_aux, "Only one packet should be sent per frame");
                        v_eth_vector_pointer_aux := eth_vector_pointer;
                    end if;

                    --check(frame_type = C_FRAME_IDLE, "Expect frame type to be IDLE");
                    check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");
                    v_frame_cnt := v_frame_cnt + 1;
                    wait until rising_edge(clk);
                    v_tx_fn_ref := v_tx_fn_ref + 1;
                end loop;
                -- check eth packets and metadata
                for jj in 0 to stimuli_length - 1 loop
                    check(eth_vector_id(jj) = stimuli_id_array(jj), "Wrong Ethernet packet ID");
                    check(eth_vector_length(jj) = stimuli_length_array(jj), "Wrong Ethernet packet Length detected");
                    check_false(eth_vector_length(jj) < 64, "Ethernet packet length can not be < 64.");
                end loop;
                -- check frame header fields 
                for ii in 0 to 20 - 1 loop
                    check(ack_data_if_array(ii).ack = frame_header_fields_array(ii + 10).ack, "ACK fields: Wrong ack");
                    check(ack_data_if_array(ii).ack_start_fn = frame_header_fields_array(ii + 10).ack_start_fn, "ACK fields: Wrong ack_start_fn");
                    check(ack_data_if_array(ii).ack_span = frame_header_fields_array(ii + 10).ack_span, "ACK fields: Wrong ack_span");
                end loop;
                for ii in 0 to 20 - 1 loop
                    check(fcch_if_array(ii).opcode = frame_header_fields_array(ii + 10).fcch_opcode, "FCCH fields: Wrong fcch_opcode");
                    check(fcch_if_array(ii).payload = frame_header_fields_array(ii + 10).fcch_pl(15 downto 0), "FCCH fields: Wrong fcch_pl");
                end loop;

                wait for 20 us;
                ---------------------------------------------------------------------------
                info("===== TEST CASE FINISHED =====");

            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- check IDLE payload generation.
            -- Expected Result: 10 IDLE payloads and TX_FN 
            ----------------------------------------------------------------------
            ELSIF run("idle_frame_basic_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: idle_frame_basic_check");
                info("--------------------------------------------------------------------------------");
                tx_fn_error_injection      <= '0';
                frame_type_error_injection <= '0';
                seqnum_error_injection     <= '0';

                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                ack_stimuli_length <= 20;
                v_frame_cnt        := 0;
                -- set eth_vector array of stimuli
                for kk in 0 to 50 - 1 loop
                    stimuli_length_array(kk) <= 1044;
                    stimuli_id_array(kk)     <= kk + 1;
                end loop;

                -- set framer_header_fields array of stimuli
                for kk in 0 to 50 - 1 loop
                    frame_header_fields_array(kk).txfn         <= std_logic_vector(to_unsigned(kk, 16));
                    frame_header_fields_array(kk).ack_start_fn <= (others => '0');
                    frame_header_fields_array(kk).ack_span     <= (others => '0');
                    frame_header_fields_array(kk).ack_valid    <= '0';
                    frame_header_fields_array(kk).ack          <= '0';
                    frame_header_fields_array(kk).tx_num       <= "100";
                    frame_header_fields_array(kk).pl_rate      <= "1100";
                    frame_header_fields_array(kk).frame_type   <= C_FRAME_DATA;
                    frame_header_fields_array(kk).tx_ts        <= (others => '0');
                    frame_header_fields_array(kk).fcch_opcode  <= C_FCCH_NOT_PRESENT;
                    frame_header_fields_array(kk).fcch_pl      <= (others => '0');
                    frame_header_fields_array(kk).zerotail     <= (others => '0');
                end loop;

                for kk in 10 to 10 + 20 - 1 loop
                    frame_header_fields_array(kk).ack_start_fn <= std_logic_vector(to_unsigned(kk, 16));
                    frame_header_fields_array(kk).ack_span     <= "010";
                    frame_header_fields_array(kk).ack_valid    <= '1';
                    frame_header_fields_array(kk).ack          <= '1';
                    frame_header_fields_array(kk).fcch_opcode  <= "111110";
                    frame_header_fields_array(kk).fcch_pl      <= std_logic_vector(to_unsigned(kk, 16));
                end loop;
                -- set stimuli length
                stimuli_length <= 50;
                -- is idle? error injection?
                is_idle        <= '1';
                -- start_stimuli
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                start_stimuli  <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli  <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                -- read waiting for payload valid

                -- convert payload to eth packets
                wait_for_idle            := '1';
                payload_id_last          := 0;
                v_eth_vector_pointer_aux := 0;
                v_tx_fn_ref              := 0;
                pk_offset                := 0;
                --while wait_for_idle = '1' loop
                for ik in 0 to 50 - 1 loop
                    wait until payload_valid = '1';
                    --check(payload_length = C_FSO_PAYLOAD_SIZE, "ERROR: payload length is NOT C_FSO_PAYLOAD_SIZE");
                    v_tx_fn   := to_integer(unsigned(axis_payload_if_m.tx_fn));
                    v_payload := payload;

                    check(frame_header_fields_array(v_frame_cnt).tx_num = tx_num_out, "Wrong tx_num.");
                    check(frame_header_fields_array(v_frame_cnt).txfn = tx_fn_out, "Wrong tx_fn.");
                    check(axis_payload_if_m.frame_type = C_FRAME_IDLE, "Wrong frame_type.");

                    if axis_payload_if_m.frame_type = C_FRAME_IDLE then
                        v_error := payload_test_health_check(payload);
                        check(v_error = '1', "Data detected in IDLE payload.");
                        v_tx_fn := to_integer(unsigned(axis_payload_if_m.tx_fn));
                        if v_tx_fn > 2 then
                            wait_for_idle := '0';
                        end if;

                    elsif axis_payload_if_m.frame_type = C_FRAME_DATA then
                        payload_test_read_payload_data(
                            v_payload,
                            pk_offset,
                            v_packet,
                            pk_size,
                            eth_vector_id,
                            eth_vector_length,
                            eth_vector_pointer,
                            split,
                            payload_id_last,
                            v_error);
                        check(v_error = '0', "Payload health check failed.");
                        -- check that only one packet is sent per frame:
                        check(eth_vector_pointer - 1 = v_eth_vector_pointer_aux, "Only one packet should be sent per frame");
                        v_eth_vector_pointer_aux := eth_vector_pointer;
                    end if;

                    --check(frame_type = C_FRAME_IDLE, "Expect frame type to be IDLE");
                    check(v_tx_fn = v_tx_fn_ref, "Wrong TX_FN.");
                    v_frame_cnt := v_frame_cnt + 1;
                    wait until rising_edge(clk);
                    v_tx_fn_ref := v_tx_fn_ref + 1;
                end loop;
                -- check eth packets and metadata
                --for jj in 0 to stimuli_length-1 loop
                --    check(eth_vector_id(jj) = stimuli_id_array(jj), "Wrong Ethernet packet ID");
                --    check(eth_vector_length(jj) = stimuli_length_array(jj), "Wrong Ethernet packet Length detected");
                --    check_false(eth_vector_length(jj) < 64, "Ethernet packet length can not be < 64.");    
                --end loop;
                -- check frame header fields 
                for ii in 0 to 20 - 1 loop
                    check(ack_data_if_array(ii).ack = frame_header_fields_array(ii + 10).ack, "ACK fields: Wrong ack");
                    check(ack_data_if_array(ii).ack_start_fn = frame_header_fields_array(ii + 10).ack_start_fn, "ACK fields: Wrong ack_start_fn");
                    check(ack_data_if_array(ii).ack_span = frame_header_fields_array(ii + 10).ack_span, "ACK fields: Wrong ack_span");
                end loop;
                for ii in 0 to 20 - 1 loop
                    check(fcch_if_array(ii).opcode = frame_header_fields_array(ii + 10).fcch_opcode, "FCCH fields: Wrong fcch_opcode");
                    check(fcch_if_array(ii).payload = frame_header_fields_array(ii + 10).fcch_pl(15 downto 0), "FCCH fields: Wrong fcch_pl");
                end loop;

                wait for 20 us;
                ---------------------------------------------------------------------------
                info("===== TEST CASE FINISHED =====");

            END IF;                     -- for test_suite
        END LOOP test_cases_loop;

        WAIT FOR 10 us;
        test_runner_cleanup(runner);
    end process;

    pr_ack_read : process
    begin
        wait until start_stimuli = '1';
        for ii in 0 to 20 - 1 loop
            wait until ack_data_if_m.ack_valid = '1' and rising_edge(clk);
            ack_data_if_array(ii).ack          <= ack_data_if_m.ack;
            ack_data_if_array(ii).ack_start_fn <= ack_data_if_m.ack_start_fn;
            ack_data_if_array(ii).ack_span     <= ack_data_if_m.ack_span;
        end loop;
        --wait;
    end process;

    pr_FCCH_read : process
    begin
        wait until start_stimuli = '1';
        for ii in 0 to 20 - 1 loop
            wait until fcch_if_m.valid = '1' and rising_edge(clk);
            fcch_if_array(ii).opcode  <= fcch_if_m.opcode;
            fcch_if_array(ii).payload <= fcch_if_m.payload;
        end loop;
        --wait;
    end process;

    inst_frame_transmiter : entity sda_test_lib.frame_transmiter
        port map(
            clk                        => clk, --: in std_logic;
            -- Transaction Interfaces
            axis_if_m                    => axis_if_m, --: inout t_axis_if;
            axis_if_s                    => axis_if_s, --: inout t_axis_if;
            eth_vector_id              => stimuli_id_array, --: in eth_packet_id_array_t;
            eth_vector_length          => stimuli_length_array, --: in eth_packet_length_array_t;
            frame_header_fields_array  => frame_header_fields_array, --: in frame_header_fields_array_t;
            stimuli_length             => stimuli_length, --: in natural;
            start_stimuli              => start_stimuli, --: in std_logic;
            is_idle                    => is_idle, --: in std_logic;
            finished                   => finished, --: out std_logic;
            tx_fn_error_injection      => tx_fn_error_injection, --: in std_logic;
            frame_type_error_injection => frame_type_error_injection, --: in std_logic;
            seqnum_error_injection     => seqnum_error_injection --: in std_logic
        );

    DUT : entity frame_header_lib.frame_header_rx
        port map(
            clk             => clk,     --: in    std_logic;
            reset           => reset,   --: in    std_logic;
            axis_if_m         => axis_if_m, --: inout t_axis_if; -- Output axi-s
            axis_if_s         => axis_if_s, --: inout t_axis_if; -- Output axi-s
            axis_payload_if_m => axis_payload_if_m, --: inout t_axis_payload_if; -- Input axi-s
            axis_payload_if_s => axis_payload_if_s, --: inout t_axis_payload_if; -- Input axi-s
            ack_data_if_m     => ack_data_if_m, --: inout t_ack_data_if; -- ACK data interface
            ack_data_if_s     => ack_data_if_s, --: inout t_ack_data_if; -- ACK data interface
            fcch_if_m         => fcch_if_m, --: inout t_fcch_if; -- FCCH interface
            fcch_if_s         => fcch_if_s, --: inout t_fcch_if; -- FCCH interface
            ts_record_en        => '0', --ts_record_en, --: in std_logic;
            ts_data_if          => open, --ts_data_if, --: out t_ts_insertion_if;
            ts_insertion_ready  => '1', --ts_insertion_ready, --: in std_logic;
            pl_rate         => pl_rate  --: out    std_logic_vector(3 downto 0) -- Payload data rate
        );

    inst_payload_receiver : entity sda_test_lib.payload_receiver
        port map(
            -- Global Signal Interface
            clk             => clk,     --: in std_logic;

            -- Transaction Interfaces
            axis_payload_if_m => axis_payload_if_m, --: inout t_axis_payload_if;
            axis_payload_if_s => axis_payload_if_s, --: inout t_axis_payload_if;
            back_pressure   => '0',     --: in std_logic;
            tx_fn_out       => tx_fn_out, --: in std_logic_vector(15 downto 0);
            frame_type_out  => frame_type_out, --: in std_logic_vector(1 downto 0);
            tx_num_out      => tx_num_out,
            payload         => payload, --: out payload_array_t;
            payload_valid   => payload_valid, --: out std_logic;
            error           => error,   --: out std_logic;
            payload_length  => payload_length, --: out natural;
            seqnum          => seqnum   --: out natural;
        );

    test_runner_watchdog(runner, 50 ms);
end architecture;
