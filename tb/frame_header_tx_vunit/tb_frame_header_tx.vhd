-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: tb_frame_header_tx
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

library osvvm;
use osvvm.RandomPkg.all;
use osvvm.CoveragePkg.all;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

library frame_header_lib;

library sda_test_lib;
use sda_test_lib.frame_receiver;

entity tb_frame_header_tx is
    generic(runner_cfg : string := runner_cfg_default);
end entity;

architecture tb of tb_frame_header_tx is

    constant C_CLK_PERIOD : time      := 10 ns;
    signal clk            : std_logic := '0';
    signal reset          : std_logic := '1';
    signal error          : std_logic;
    signal send           : std_logic := '0';
    signal frame_valid    : std_logic;

    signal axis_if_m             : t_axis_if_m;
    signal axis_if_s             : t_axis_if_s;
    signal axis_payload_if_m     : t_axis_payload_if_m;
    signal axis_payload_if_s     : t_axis_payload_if_s;
    signal frame_header_fields : frame_header_fields_t;
    signal payload_ram         : payload_ram_t;
    signal payload_ram_ref     : payload_ram_t;
    signal frame_length        : natural;
    signal header_length       : natural;
    signal valid               : std_logic;

    signal ack_data_if_m   : t_ack_data_if_m; -- ACK data interface
    signal ack_data_if_s   : t_ack_data_if_s; -- ACK data interface
    signal ack_data_if_a_m : t_ack_data_if_m; -- ACK data interface
    signal ack_data_if_a_s : t_ack_data_if_s; -- ACK data interface
    signal ack_data_if_b_m : t_ack_data_if_m; -- ACK data interface   
    signal ack_data_if_b_s : t_ack_data_if_s; -- ACK data interface   
    signal fcch_if_m       : t_fcch_if_m;   -- FCCH interface
    signal fcch_if_s       : t_fcch_if_s;   -- FCCH interface
    signal fcch_if_a_m     : t_fcch_if_m;   -- FCCH interface
    signal fcch_if_a_s     : t_fcch_if_s;   -- FCCH interface
    signal fcch_if_b_m     : t_fcch_if_m;   -- FCCH interface
    signal fcch_if_b_s     : t_fcch_if_s;   -- FCCH interface
    signal pl_rate       : std_logic_vector(3 downto 0); -- Payload data rate

    signal start_stimuli       : std_logic := '0';
    signal random_stimuli_wait : std_logic := '0';
    signal stimuli_length      : natural   := 5;

    signal start_fcch_stimuli : std_logic;
    signal start_ack_stimuli  : std_logic;

    type t_tx_fn_array is array (5 - 1 downto 0) of std_logic_vector(15 downto 0); -- Sequence number of this Tx Frame
    type t_frame_type_array is array (5 - 1 downto 0) of std_logic_vector(1 downto 0); -- Frame Type
    type t_tx_num_array is array (5 - 1 downto 0) of std_logic_vector(2 downto 0); -- Transmission atempt
    type t_pl_rate_array is array (5 - 1 downto 0) of std_logic_vector(4 - 1 downto 0); -- Transmission atempt

    signal tx_fn_array      : t_tx_fn_array;
    signal frame_type_array : t_frame_type_array;
    signal tx_num_array     : t_tx_num_array;
    signal pl_rate_array    : t_pl_rate_array;

begin

    --------------------------------------------------------------------------
    -- CLOCK AND RESET.
    --------------------------------------------------------------------------
    clk   <= NOT clk after C_CLK_PERIOD / 2;
    reset <= '0' after 5 * (C_CLK_PERIOD);

    test_runner : process
        variable tx_fn      : std_logic_vector(15 downto 0); -- Sequence number of this Tx Frame
        variable frame_type : std_logic_vector(1 downto 0); -- Frame Type
        variable tx_num     : std_logic_vector(2 downto 0); -- Transmission atempt

    begin
        test_runner_setup(runner, runner_cfg);
        -- enable logging for passing check
        --show(get_logger(default_checker), display_handler, pass); 
        --show(get_logger(RED_CHECKER), display_handler, pass); 
        --packet_len <= 100;
        test_cases_loop : WHILE test_suite LOOP
            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- check IDLE payload generation.
            -- Expected Result: 10 IDLE payloads and TX_FN 
            ----------------------------------------------------------------------
            IF run("all_functionality_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: all_functionality_check");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                tx_fn_array       <= (x"1234", x"ACDF", x"4325", x"1A2D", x"FC3A");
                frame_type_array  <= ("00", "01", "10", "11", "00");
                tx_num_array      <= ("000", "001", "011", "110", "111");
                pl_rate_array     <= ("1000", "0001", "0011", "1111", "0111");
                fcch_if_a_m.opcode  <= "000101";
                fcch_if_a_m.payload <= x"EDAF";
                fcch_if_b_m.opcode  <= "111110";
                fcch_if_b_m.payload <= x"7831";

                ack_data_if_a_m.ack          <= '1';
                ack_data_if_a_m.ack_start_fn <= x"12AF";
                ack_data_if_a_m.ack_span     <= "101";

                ack_data_if_b_m.ack          <= '0';
                ack_data_if_b_m.ack_start_fn <= x"DA76";
                ack_data_if_b_m.ack_span     <= "010";

                stimuli_length <= 5;
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                start_stimuli <= '1';

                for ii in 0 to 5 - 1 loop
                    wait until frame_valid = '1';
                    WAIT UNTIL rising_edge(clk);
                    WAIT UNTIL rising_edge(clk);
                    --frame length must match:
                    check(frame_length = C_FRAME_FULL_SIZE_BYTES, "ERROR: payload length is NOT C_FSO_PAYLOAD_SIZE");

                    -- check all header fields:
                    check(frame_header_fields.txfn = tx_fn_array(ii), "Wrong TX_FN.");
                    if ii = 3 then
                        check(frame_header_fields.ack_start_fn = ack_data_if_a_m.ack_start_fn, "Wrong ack_start_fn.");
                        check(frame_header_fields.ack_span = ack_data_if_a_m.ack_span, "Wrong ack_span.");
                        check(frame_header_fields.ack_valid = '1', "Wrong ack_valid.");
                        check(frame_header_fields.ack = ack_data_if_a_m.ack, "Wrong ack.");
                        check(frame_header_fields.fcch_opcode = fcch_if_a_m.opcode, "Wrong fcch_opcode.");
                        check(frame_header_fields.fcch_pl = fcch_if_a_m.payload, "Wrong fcch_pl.");
                    elsif ii = 4 then
                        check(frame_header_fields.ack_start_fn = ack_data_if_b_m.ack_start_fn, "Wrong ack_start_fn.");
                        check(frame_header_fields.ack_span = ack_data_if_b_m.ack_span, "Wrong ack_span.");
                        check(frame_header_fields.ack_valid = '1', "Wrong ack_valid.");
                        check(frame_header_fields.ack = ack_data_if_b_m.ack, "Wrong ack.");
                        check(frame_header_fields.fcch_opcode = fcch_if_b_m.opcode, "Wrong fcch_opcode.");
                        check(frame_header_fields.fcch_pl = fcch_if_b_m.payload, "Wrong fcch_pl.");
                    else
                        check(frame_header_fields.ack_valid = '0', "Wrong ack_valid.");
                        check(frame_header_fields.fcch_opcode = C_FCCH_NOT_PRESENT, "Wrong fcch_opcode.");
                    end if;

                    check(frame_header_fields.tx_num = tx_num_array(ii), "Wrong tx_num.");
                    check(frame_header_fields.pl_rate = pl_rate_array(ii), "Wrong pl_rate.");
                    check(frame_header_fields.frame_type = frame_type_array(ii), "Wrong frame_type.");
                    --check(frame_header_fields.tx_ts = (others=>'0'), "Wrong tx_ts.");

                    --check(frame_header_fields.zerotail = (others=>'0'), "Wrong zerotail.");

                    -- check payload integrity
                    for jj in 0 to C_FSO_PAYLOAD_SIZE - 1 loop
                        if payload_ram_ref(jj) /= payload_ram(jj) then
                            check(1 = 0, "Payload check failed. At least one byte missmatch detected.");
                        end if;
                    end loop;

                    wait until rising_edge(clk);

                end loop;

                wait for 5 us;
                info("===== TEST CASE FINISHED =====");

            END IF;                     -- for test_suite
        END LOOP test_cases_loop;

        WAIT FOR 20 ns;
        test_runner_cleanup(runner);
    end process;

    pr_stimuli : process
    begin
        axis_payload_if_m    <= C_AXIS_PAYLOAD_IF_MASTER_INIT;
        start_fcch_stimuli <= '0';
        start_ack_stimuli  <= '0';
        wait until start_stimuli = '1';
        for kk in 0 to stimuli_length - 1 loop
            if kk = 3 then
                start_fcch_stimuli <= '1';
                start_ack_stimuli  <= '1';
            end if;
            wait until rising_edge(clk);
            wait until rising_edge(clk);

            wait until axis_payload_if_s.axis_if_s.tready = '1' and rising_edge(clk);

            for ii in 0 to C_FSO_PAYLOAD_SIZE - 1 loop
                if ii = C_FSO_PAYLOAD_SIZE - 1 then
                    axis_payload_if_m.axis_if_m.tdata  <= x"FF";
                    axis_payload_if_m.axis_if_m.tvalid <= '1';
                    axis_payload_if_m.axis_if_m.tlast  <= '1';
                    payload_ram_ref(ii)            <= x"FF";
                else
                    axis_payload_if_m.axis_if_m.tdata  <= std_logic_vector(to_unsigned(ii + 1, 8));
                    payload_ram_ref(ii)            <= std_logic_vector(to_unsigned(ii + 1, 8));
                    axis_payload_if_m.axis_if_m.tvalid <= '1';
                    axis_payload_if_m.axis_if_m.tlast  <= '0';
                    if ii = 0 then
                        axis_payload_if_m.axis_if_m.tstart <= '1';
                    else
                        axis_payload_if_m.axis_if_m.tstart <= '0';
                    end if;
                    axis_payload_if_m.tx_fn          <= tx_fn_array(kk);
                    axis_payload_if_m.frame_type     <= frame_type_array(kk);
                    axis_payload_if_m.tx_num         <= tx_num_array(kk);
                    pl_rate                        <= pl_rate_array(kk);
                end if;
                wait until rising_edge(clk) and axis_payload_if_s.axis_if_s.tready = '1';
            end loop;
            axis_payload_if_m <= C_AXIS_PAYLOAD_IF_MASTER_INIT;

        end loop;
        wait;

    end process;

    FCCH_stimuli : process
    begin
        wait until start_fcch_stimuli = '1';
        fcch_if_m.valid   <= '1';
        fcch_if_m.opcode  <= fcch_if_a_m.opcode;
        fcch_if_m.payload <= fcch_if_a_m.payload;
        wait until rising_edge(clk) and fcch_if_s.rd_ena = '1';
        wait until rising_edge(clk);

        fcch_if_m.valid   <= '1';
        fcch_if_m.opcode  <= fcch_if_b_m.opcode;
        fcch_if_m.payload <= fcch_if_b_m.payload;
        wait until rising_edge(clk) and fcch_if_s.rd_ena = '1';
        wait until rising_edge(clk);

        wait;

    end process;

    ACK_stimuli : process
    begin
        wait until start_ack_stimuli = '1';
        ack_data_if_m.ack_valid    <= '1';
        ack_data_if_m.ack          <= ack_data_if_a_m.ack;
        ack_data_if_m.ack_start_fn <= ack_data_if_a_m.ack_start_fn;
        ack_data_if_m.ack_span     <= ack_data_if_a_m.ack_span;
        wait until rising_edge(clk) and ack_data_if_s.rd_ena = '1';
        wait until rising_edge(clk);

        ack_data_if_m.ack_valid    <= '1';
        ack_data_if_m.ack          <= ack_data_if_b_m.ack;
        ack_data_if_m.ack_start_fn <= ack_data_if_b_m.ack_start_fn;
        ack_data_if_m.ack_span     <= ack_data_if_b_m.ack_span;
        wait until rising_edge(clk) and ack_data_if_s.rd_ena = '1';
        wait until rising_edge(clk);
        wait;

    end process;

    inst_payload_receiver : entity sda_test_lib.frame_receiver
        port map(
            clk                 => clk, --: in std_logic;
            axis_if_m             => axis_if_m, --: inout t_axis_if; -- Input axi-s
            axis_if_s             => axis_if_s, --: inout t_axis_if; -- Input axi-s
            frame_header_fields => frame_header_fields, --: out frame_header_fields_t;
            payload_ram         => payload_ram, --: out payload_ram_t;
            frame_length        => frame_length, --: out natural;
            valid               => frame_valid --: out std_logic
        );

    DUT : entity frame_header_lib.frame_header_tx
        port map(
            clk             => clk,     --: in    std_logic;
            reset           => reset,   --: in    std_logic;
            axis_payload_if_m => axis_payload_if_m, --: inout t_axis_payload_if; -- Input axi-s
            axis_payload_if_s => axis_payload_if_s, --: inout t_axis_payload_if; -- Input axi-s
            axis_if_m         => axis_if_m, --: inout t_axis_if; -- Output axi-s
            axis_if_s         => axis_if_s, --: inout t_axis_if; -- Output axi-s
            ack_data_if_m     => ack_data_if_m, --: inout t_ack_data_if; -- ACK data interface
            ack_data_if_s     => ack_data_if_s, --: inout t_ack_data_if; -- ACK data interface
            fcch_if_m         => fcch_if_m, --: inout t_fcch_if; -- FCCH interface
            fcch_if_s         => fcch_if_s, --: inout t_fcch_if; -- FCCH interface
            pl_rate         => pl_rate  --: in    std_logic_vector(3 downto 0) -- Payload data rate
        );

    test_runner_watchdog(runner, 50 ms);
end architecture;
