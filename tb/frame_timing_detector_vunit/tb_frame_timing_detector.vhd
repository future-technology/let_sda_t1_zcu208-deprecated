-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: tb_frame_timing_detector
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

library osvvm;
use osvvm.RandomPkg.all;
use osvvm.CoveragePkg.all;

library timing_detector_lib;
library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;


entity tb_frame_timing_detector is
    generic(runner_cfg : string := runner_cfg_default);
end entity;

architecture tb of tb_frame_timing_detector is
	constant preamble : std_logic_vector(64-1 downto 0) := X"53225b1d0d73df03";
    constant C_CLK_PERIOD : time      := 5 ns;
    signal clk            : std_logic := '0';
    signal reset          : std_logic := '1';
    signal error          : std_logic;
    signal send           : std_logic := '0';
    signal frame_valid    : std_logic;
    signal preamble_valid   : std_logic;                     
    signal timestamp        : std_logic_vector(40-1 downto 0);
    signal timestamp_ref    : std_logic_vector(40-1 downto 0);
    signal timestamp_valid  : std_logic;                    
    signal s_axis_data : std_logic_vector(32-1 downto 0);
    signal s_axis_valid : std_logic;
    signal reset_timer     : std_logic;
    signal reset_timer_ref : std_logic;
    signal start_stimuli : std_logic := '0';

    type t_data_vector is array (0 to 4-1) of std_logic_vector(32-1 downto 0);
    signal data_vector : t_data_vector;
    signal preamble_cnt : natural;
    signal vector3words : std_logic_vector(32*4 - 1 downto 0);
    
begin

    --------------------------------------------------------------------------
    -- CLOCK AND RESET.
    --------------------------------------------------------------------------
    clk   <= NOT clk after C_CLK_PERIOD / 2;
    reset <= '0' after 5 * (C_CLK_PERIOD);

    test_runner : process
        variable tx_fn      : std_logic_vector(15 downto 0); -- Sequence number of this Tx Frame
        variable frame_type : std_logic_vector(1 downto 0); -- Frame Type
        variable tx_num     : std_logic_vector(2 downto 0); -- Transmission atempt

    begin
        test_runner_setup(runner, runner_cfg);
        -- enable logging for passing check
        --show(get_logger(default_checker), display_handler, pass); 
        --show(get_logger(RED_CHECKER), display_handler, pass); 
        --packet_len <= 100;
        test_cases_loop : WHILE test_suite LOOP
            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- check IDLE payload generation.
            -- Expected Result: 10 IDLE payloads and TX_FN 
            ----------------------------------------------------------------------
            IF run("preamble_bit_shift_test") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: preamble_bit_shift_test");
                info("--------------------------------------------------------------------------------");
                start_stimuli <= '0';

                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';

                wait for 100 us; 
                check(preamble_cnt = 33, "Not enough preamble detections");

                wait for 5 us;
                info("===== TEST CASE FINISHED =====");

            END IF;                     
        END LOOP test_cases_loop; -- for test_suite

        WAIT FOR 20 ns;
        test_runner_cleanup(runner);
    end process;


    pr_counter : process
        variable v_bit_cnt : natural := 0;
    begin
        preamble_cnt <= 0;
        loop
            wait until preamble_valid = '1';
            preamble_cnt <= preamble_cnt + 1;
        end loop;

    end process;


    pr_stimuli : process
        variable v_bit_cnt : natural := 0;
    begin
        s_axis_valid <= '0';
        s_axis_data  <= (others=>'0');
        vector3words  <= (others=>'0');
        wait until start_stimuli = '1';
        wait until rising_edge(clk);
        wait until rising_edge(clk);

        v_bit_cnt := 0;

        -- test covers all bit shifts
        for bb in 0 to 33 loop
            vector3words  <= (others=>'0');
            vector3words(128-1-bb downto 128-64-bb) <= preamble;
            data_vector(3) <= vector3words(32-1 downto 0);
            data_vector(2) <= vector3words(64-1 downto 32);
            data_vector(1) <= vector3words(96-1 downto 64);
            data_vector(0) <= vector3words(128-1 downto 96);
            

            wait until rising_edge(clk);
            -- sending 5 words of data
            for ii in 0 to 5 - 1 loop  
                s_axis_valid <= '1';
                s_axis_data  <= (others=>'0');
                wait until rising_edge(clk);
            end loop;  

            s_axis_valid <= '1';
            s_axis_data <= data_vector(0);
            wait until rising_edge(clk);            
            s_axis_data <= data_vector(1);
            wait until rising_edge(clk);
            s_axis_data <= data_vector(2);
            wait until rising_edge(clk);   
            s_axis_valid <= '1';
            wait until rising_edge(clk); 
        
            -- sending 10 words of zeroes
            for ii in 0 to 20 - 1 loop  
                s_axis_valid <= '1';
                s_axis_data  <= (others=>'0');
                wait until rising_edge(clk);
            end loop;
            s_axis_valid <= '0';
            wait for 800 ns;
            wait until rising_edge(clk);
            reset_timer <= '1';     
            reset_timer_ref <= '1'; 
            wait until rising_edge(clk); 
            reset_timer <= '0';     
            reset_timer_ref <= '0'; 
            wait for 800 ns;
            wait until rising_edge(clk);
        end loop;

        --wait;

    end process;

    inst_frame_timing_detector : entity timing_detector_lib.frame_timing_detector
    Port map( 
        clk            => clk,    
        rst            => reset,  
        clk_ref 	   => clk,   --: in  std_logic;
        rst_ref 	   => reset, --: in  std_logic;

        -- Input data from FSO PHY
        ----------------------
        s_axis_data     => s_axis_data, --fso_tx_fifo_tdata, ch_tx_sr_tdata
        s_axis_valid     => s_axis_valid,
        -- Output data 
        ----------------------
        reset_timer      => reset_timer, --: in std_logic;
        reset_timer_ref  => reset_timer_ref, --: in std_logic;

        -- Output data 
        ----------------------
		preamble_valid   => preamble_valid, --: out std_logic;                         
        timestamp        => open, --: out std_logic_vector(40-1 downto 0);
        timestamp_ref    => open, --: out std_logic_vector(40-1 downto 0);
        timestamp_valid  => open --: out std_logic                             
    );

    test_runner_watchdog(runner, 50 ms);
end architecture;
