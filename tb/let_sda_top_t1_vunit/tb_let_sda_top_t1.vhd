-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: tb_let_sda_top_t1
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

library osvvm;
use osvvm.RandomPkg.all;
use osvvm.CoveragePkg.all;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

library sda_oct_lib;
--use ethernet_framer_lib.txt_util.all;

library sda_test_lib;
use sda_test_lib.sda_test_pkg.all;
library sda_test_lib;
use sda_test_lib.eth_packet_test_pkg.all;
library sda_test_lib;
use sda_test_lib.packet_test_pkg.all;
library sda_test_lib;
use sda_test_lib.payload_test_pkg.all;

library sda_test_lib;
use sda_test_lib.ethernet_test_receiver;

entity tb_let_sda_top_t1 is
    generic(runner_cfg : string := runner_cfg_default);
end entity;

architecture tb of tb_let_sda_top_t1 is

    constant C_CLK_PERIOD  : time      := 10 ns;
    signal clk             : std_logic := '0';
    signal reset           : std_logic := '1';
    signal error           : std_logic;
    signal send            : std_logic := '0';
    signal payload_length  : natural;
    signal seqnum          : natural;
    signal packet_len      : natural   := 0;
    signal axis_payload_if_m : t_axis_payload_if_m;
    signal axis_payload_if_s : t_axis_payload_if_s;
    signal payload         : payload_array_t;
    signal payload_valid   : std_logic;
    signal tx_fn           : std_logic_vector(16 - 1 downto 0);
    signal frame_type      : std_logic_vector(2 - 1 downto 0);
    signal tx_fn_out       : std_logic_vector(16 - 1 downto 0);
    signal frame_type_out  : std_logic_vector(2 - 1 downto 0);

    signal start_stimuli        : std_logic := '0';
    signal random_stimuli_wait  : std_logic := '0';
    signal stimuli_length       : natural;
    type t_stimuli_length_array is array (0 to 500 - 1) of natural;
    signal stimuli_length_array : t_stimuli_length_array;

    signal eth_packet_length : natural;
    signal eth_packet_id     : natural;
    --eth_packet        :    eth_packet_array_t;
    signal eth_packet_valid  : std_logic;
    signal eth_packet_error  : std_logic;
    signal axis_ethernet_if_m  : t_axis_if_m;
    signal axis_ethernet_if_s  : t_axis_if_s;

    signal eth_vector_id              : eth_packet_id_array_t;
    signal eth_vector_length          : eth_packet_length_array_t;
    signal tx_fn_error_injection      : std_logic;
    signal frame_type_error_injection : std_logic;
    signal seqnum_error_injection     : std_logic;
    signal is_idle                    : std_logic;
    signal pt_finished                : std_logic;
    signal back_pressure              : std_logic := '0';
    signal eth_packet_good_cnt        : natural   := 0;

    shared variable rv  : RandomPType;
    shared variable rvt : RandomPType;

    signal client_out_axis_if_m : t_axis_if_m;
    signal client_out_axis_if_s : t_axis_if_s;
    signal client_in_axis_if_m : t_axis_if_m;
    signal client_in_axis_if_s : t_axis_if_s;
    signal fec_rx_axis_if_m : t_axis_if_m;
    signal fec_rx_axis_if_s : t_axis_if_s;
    signal fec_tx_axis_if_m : t_axis_if_m;
    signal fec_tx_axis_if_s : t_axis_if_s;

    signal cfg_pause_data : std_logic_vector(15 downto 0);
    signal tx_pause_valid : std_logic;
    signal tx_pause_data : std_logic_vector(15 downto 0);
    signal rx_fifo_skipped_frame : std_logic_vector(31 downto 0);

    signal fh_tx_fcch_if_m : t_fcch_if_m;
    signal fh_tx_fcch_if_s : t_fcch_if_s;
    signal fh_rx_fcch_if_m : t_fcch_if_m;
    signal fh_rx_fcch_if_s : t_fcch_if_s;


    --statistics:
    signal framer_total_packet_counter          : std_logic_vector(31 downto 0);
    signal framer_total_payload_counter         : std_logic_vector(31 downto 0);
    signal framer_total_packet_splitted_counter : std_logic_vector(31 downto 0);
    signal framer_total_packet_merged_counter   : std_logic_vector(31 downto 0);
    signal framer_total_data_frame_counter      : std_logic_vector(31 downto 0);
    signal framer_total_idle_frame_counter      : std_logic_vector(31 downto 0);
    signal framer_total_payload_error_counter   : std_logic_vector(31 downto 0);
    signal framer_total_packet_error_counter    : std_logic_vector(31 downto 0);
    signal framer_watchdog_reset_counter        : std_logic_vector(31 downto 0);
    signal framer_packet_filter_cnt_in          : std_logic_vector(31 downto 0);
    signal framer_packet_filter_cnt_out         : std_logic_vector(31 downto 0);

    signal deframer_total_packet_counter          : std_logic_vector(31 downto 0);
    signal deframer_total_payload_counter         : std_logic_vector(31 downto 0);
    signal deframer_total_packet_splitted_counter : std_logic_vector(31 downto 0);
    signal deframer_total_packet_merged_counter   : std_logic_vector(31 downto 0);
    signal deframer_total_data_frame_counter      : std_logic_vector(31 downto 0);
    signal deframer_total_idle_frame_counter      : std_logic_vector(31 downto 0);
    signal deframer_total_payload_error_counter   : std_logic_vector(31 downto 0);
    signal deframer_total_packet_error_counter    : std_logic_vector(31 downto 0);
    signal deframer_watchdog_reset_counter        : std_logic_vector(31 downto 0);
    signal deframer_packet_filter_cnt_in          : std_logic_vector(31 downto 0);
    signal deframer_packet_filter_cnt_out         : std_logic_vector(31 downto 0);
    signal deframer_frame_filter_cnt_in           : std_logic_vector(31 downto 0);
    signal deframer_frame_filter_cnt_out          : std_logic_vector(31 downto 0);
    signal deframer_bitrate_out                   : std_logic_vector(31 downto 0);
    signal deframer_packet_cnt_out                : std_logic_vector(31 downto 0);

    --statistics:
    signal n_framer_total_packet_counter          : natural := 0;
    signal n_framer_total_payload_counter         : natural := 0;
    signal n_framer_total_packet_splitted_counter : natural := 0;
    signal n_framer_total_packet_merged_counter   : natural := 0;
    signal n_framer_total_data_frame_counter      : natural := 0;
    signal n_framer_total_idle_frame_counter      : natural := 0;
    signal n_framer_total_payload_error_counter   : natural := 0;
    signal n_framer_total_packet_error_counter    : natural := 0;
    signal n_framer_watchdog_reset_counter        : natural := 0;
    signal n_framer_packet_filter_cnt_in          : natural := 0;
    signal n_framer_packet_filter_cnt_out         : natural := 0;

    signal n_deframer_total_packet_counter          : natural := 0;
    signal n_deframer_total_payload_counter         : natural := 0;
    signal n_deframer_total_packet_splitted_counter : natural := 0;
    signal n_deframer_total_packet_merged_counter   : natural := 0;
    signal n_deframer_total_data_frame_counter      : natural := 0;
    signal n_deframer_total_idle_frame_counter      : natural := 0;
    signal n_deframer_total_payload_error_counter   : natural := 0;
    signal n_deframer_total_packet_error_counter    : natural := 0;
    signal n_deframer_watchdog_reset_counter        : natural := 0;
    signal n_deframer_packet_filter_cnt_in          : natural := 0;
    signal n_deframer_packet_filter_cnt_out         : natural := 0;
    signal n_deframer_frame_filter_cnt_in           : natural := 0;
    signal n_deframer_frame_filter_cnt_out          : natural := 0;
    signal n_deframer_bitrate_out                   : natural := 0;
    signal n_deframer_packet_cnt_out                : natural := 0;


begin

    --------------------------------------------------------------------------
    -- CLOCK AND RESET.
    --------------------------------------------------------------------------
    clk   <= NOT clk after C_CLK_PERIOD / 2;
    reset <= '0' after 5 * (C_CLK_PERIOD);

    n_framer_total_packet_counter          <= to_integer(unsigned(framer_total_packet_counter));
    n_framer_total_payload_counter         <= to_integer(unsigned(framer_total_payload_counter));
    n_framer_total_packet_splitted_counter <= to_integer(unsigned(framer_total_packet_splitted_counter));
    n_framer_total_packet_merged_counter   <= to_integer(unsigned(framer_total_packet_merged_counter));
    n_framer_total_data_frame_counter      <= to_integer(unsigned(framer_total_data_frame_counter));
    n_framer_total_idle_frame_counter      <= to_integer(unsigned(framer_total_idle_frame_counter));
    n_framer_total_payload_error_counter   <= to_integer(unsigned(framer_total_payload_error_counter));
    n_framer_total_packet_error_counter    <= to_integer(unsigned(framer_total_packet_error_counter));
    n_framer_watchdog_reset_counter        <= to_integer(unsigned(framer_watchdog_reset_counter));
    n_framer_packet_filter_cnt_in          <= to_integer(unsigned(framer_packet_filter_cnt_in));
    n_framer_packet_filter_cnt_out         <= to_integer(unsigned(framer_packet_filter_cnt_out));

    n_deframer_total_packet_counter          <= to_integer(unsigned(deframer_total_packet_counter));
    n_deframer_total_payload_counter         <= to_integer(unsigned(deframer_total_payload_counter));
    n_deframer_total_packet_splitted_counter <= to_integer(unsigned(deframer_total_packet_splitted_counter));
    n_deframer_total_packet_merged_counter   <= to_integer(unsigned(deframer_total_packet_merged_counter));
    n_deframer_total_data_frame_counter      <= to_integer(unsigned(deframer_total_data_frame_counter));
    n_deframer_total_idle_frame_counter      <= to_integer(unsigned(deframer_total_idle_frame_counter));
    n_deframer_total_payload_error_counter   <= to_integer(unsigned(deframer_total_payload_error_counter));
    n_deframer_total_packet_error_counter    <= to_integer(unsigned(deframer_total_packet_error_counter));
    n_deframer_watchdog_reset_counter        <= to_integer(unsigned(deframer_watchdog_reset_counter));
    n_deframer_packet_filter_cnt_in          <= to_integer(unsigned(deframer_packet_filter_cnt_in));
    n_deframer_packet_filter_cnt_out         <= to_integer(unsigned(deframer_packet_filter_cnt_out));
    n_deframer_frame_filter_cnt_in           <= to_integer(unsigned(deframer_frame_filter_cnt_in));
    n_deframer_frame_filter_cnt_out          <= to_integer(unsigned(deframer_frame_filter_cnt_out));
    n_deframer_bitrate_out                   <= to_integer(unsigned(deframer_bitrate_out));
    n_deframer_packet_cnt_out                <= to_integer(unsigned(deframer_packet_cnt_out));

    test_runner : process
        variable v_tx_fn_ref              : natural := 0;
        variable v_tx_fn                  : natural := 0;
        variable v_packets_read           : natural := 0;
        variable v_error                  : std_logic;
        variable payload_id_last          : natural := 0;
        variable eth_vector_pointer       : natural;
        variable v_eth_vector_pointer_aux : natural := 0;
        variable pk_size                  : natural := 64;
        variable pk_offset                : natural := 0;
        variable v_packet                 : packet_array_t;
        variable split                    : std_logic;
        variable v_payload                : payload_array_t;
        variable data_flag                : std_logic;
        variable wait_for_idle            : std_logic;

    begin
        test_runner_setup(runner, runner_cfg);
        -- enable logging for passing check
        --show(get_logger(default_checker), display_handler, pass); 
        --show(get_logger(RED_CHECKER), display_handler, pass); 
        --packet_len <= 100;
        test_cases_loop : WHILE test_suite LOOP
            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- check IDLE payload generation.
            -- Expected Result: 10 IDLE payloads and TX_FN 
            ----------------------------------------------------------------------
            IF run("idle_payload_check") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: IDLE payload check");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                --random_stimuli_wait <= '0';
                seqnum_error_injection     <= '0';
                back_pressure              <= '0';
                frame_type_error_injection <= '0';
                tx_fn_error_injection      <= '0';
                for ii in 0 to eth_packet_length_array_t'length - 1 loop
                    eth_vector_id(ii)     <= ii + 1;
                    eth_vector_length(ii) <= 1044;
                end loop;
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                --check(17 = 17, "simple test failed");
                wait for 110 us;

                -- Checking IDLE count. Due to delay, deframer count will be -1
                check(unsigned(framer_total_idle_frame_counter) = unsigned(deframer_total_idle_frame_counter)+1, "IDLE payloads does not match.");
                check(framer_total_data_frame_counter = std_logic_vector(to_unsigned(0, framer_total_data_frame_counter'length)), "Data Payload not expected.");
                wait for 2 us;
                info("===== TEST CASE FINISHED =====");
--
            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- Random stress test
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("random_stress_test_1") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: random_stress_test_1");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli       <= '0';
                random_stimuli_wait <= '0';

                eth_vector_pointer      := 0;
                WAIT UNTIL rising_edge(clk);
                v_tx_fn_ref             := 0;
                stimuli_length          <= 50;
                --RV.InitSeed(now / 1 ns);
                RV.InitSeed(77);
                stimuli_length_array(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                stimuli_length_array(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);

                for kk in 0 to 50 - 1 loop
                    stimuli_length_array(kk) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                end loop;

                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                
                --wait for 300 us;
                for ii in 0 to stimuli_length - 1 loop
                    wait until eth_packet_valid = '1';
                    check(eth_packet_error = '0', "Ethernet packet error!");
                    check(eth_packet_length = stimuli_length_array(ii), "Wrong Ethernet packet Length detected");
                    check(eth_packet_id = ii, "Wrong Ethernet packet ID");
                    check_false(eth_packet_length < 64, "Ethernet packet length can not be < 64.");
--
                end loop;
                wait for 20 us;
                check(n_framer_total_packet_counter = stimuli_length, "total_packet_counter error!");
                check(n_framer_total_packet_splitted_counter > 0, "total_packet_splitted_counter error!");
                --check(n_deframer_total_packet_merged_counter = n_deframer_total_packet_splitted_counter, "total_packet_merged_counter error!");
                check(n_framer_total_payload_error_counter = 0, "total_payload_error_counter error!");
                check(n_framer_total_packet_error_counter = 0, "total_packet_error_counter error!");
                check(n_framer_watchdog_reset_counter = 0, "watchdog_reset_counter error!");
                --check(n_framer_packet_filter_cnt_in = stimuli_length, "packet_filter_cnt_in error!");
                --check(n_framer_packet_filter_cnt_out = stimuli_length, "packet_filter_cnt_out error!");
                check(n_deframer_total_packet_counter          = n_framer_total_packet_counter, "total_packet_counter error!");
                --check(n_deframer_total_payload_counter         = n_framer_total_payload_counter, "total_payload_counter error!");
                check(n_deframer_total_packet_splitted_counter = n_framer_total_packet_splitted_counter, "total_packet_splitted_counter error!");
                check(n_deframer_total_packet_merged_counter   = n_framer_total_packet_splitted_counter, "total_packet_merged_counter error!");
                check(n_deframer_total_data_frame_counter      = n_framer_total_data_frame_counter, "total_data_frame_counter error!");
                check(n_deframer_total_idle_frame_counter+1      = n_framer_total_idle_frame_counter, "total_idle_frame_counter error!");
                info("Test finished");

            --        ----------------------------------------------------------------------
            --        -- TEST CASE DESCRIPTION:
            --        -- Random stress test
            --        -- Expected Result: 
            --        ----------------------------------------------------------------------
            ELSIF run("random_arrival_stress_test") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: random_arrival_stress_test");
                info("--------------------------------------------------------------------------------");
                WAIT UNTIL reset = '0';
                start_stimuli       <= '0';
                random_stimuli_wait <= '1';

                eth_vector_pointer      := 0;
                WAIT UNTIL rising_edge(clk);
                v_tx_fn_ref             := 0;
                stimuli_length          <= 50;
                --RV.InitSeed(now / 1 ns);
                RV.InitSeed(77);
                stimuli_length_array(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                stimuli_length_array(0) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);

                for kk in 0 to 50 - 1 loop
                    stimuli_length_array(kk) <= rv.RandInt(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES);
                end loop;

                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);

                
                --wait for 300 us;
                for ii in 0 to stimuli_length - 1 loop
                    wait until eth_packet_valid = '1';
                    check(eth_packet_error = '0', "Ethernet packet error!");
                    check(eth_packet_length = stimuli_length_array(ii), "Wrong Ethernet packet Length detected");
                    check(eth_packet_id = ii, "Wrong Ethernet packet ID");
                    check_false(eth_packet_length < 64, "Ethernet packet length can not be < 64.");
--
                end loop;
                wait for 20 us;
                check(n_framer_total_packet_counter = stimuli_length, "total_packet_counter error!");
                check(n_framer_total_packet_splitted_counter > 0, "total_packet_splitted_counter error!");
                --check(n_deframer_total_packet_merged_counter = n_deframer_total_packet_splitted_counter, "total_packet_merged_counter error!");
                check(n_framer_total_payload_error_counter = 0, "total_payload_error_counter error!");
                check(n_framer_total_packet_error_counter = 0, "total_packet_error_counter error!");
                check(n_framer_watchdog_reset_counter = 0, "watchdog_reset_counter error!");
                --check(n_framer_packet_filter_cnt_in = stimuli_length, "packet_filter_cnt_in error!");
                --check(n_framer_packet_filter_cnt_out = stimuli_length, "packet_filter_cnt_out error!");
                check(n_deframer_total_packet_counter          = n_framer_total_packet_counter, "total_packet_counter error!");
                --check(n_deframer_total_payload_counter         = n_framer_total_payload_counter, "total_payload_counter error!");
                check(n_deframer_total_packet_splitted_counter = n_framer_total_packet_splitted_counter, "total_packet_splitted_counter error!");
                check(n_deframer_total_packet_merged_counter   = n_framer_total_packet_splitted_counter, "total_packet_merged_counter error!");
                check(n_deframer_total_data_frame_counter      = n_framer_total_data_frame_counter, "total_data_frame_counter error!");
                --check(n_deframer_total_idle_frame_counter+1      = n_framer_total_idle_frame_counter, "total_idle_frame_counter error!");
                info("Test finished");

            END IF;                     -- for test_suite
        END LOOP test_cases_loop;

        WAIT FOR 20 ns;
        test_runner_cleanup(runner);
    end process;

    pr_stimuli : process
        variable v_wait : time;
    begin
        send   <= '0';
        RV.InitSeed(now / 1 ns);
        v_wait := rvt.RandTime(1 us, 50 us);
        wait until start_stimuli = '1';
        for ii in 0 to stimuli_length - 1 loop
            wait until rising_edge(clk);
            packet_len <= stimuli_length_array(ii);
            send       <= '1';
            wait until rising_edge(clk);
            send       <= '0';
            wait until client_in_axis_if_m.tlast = '1' and client_in_axis_if_m.tvalid = '1'and client_in_axis_if_s.tready = '1';

            --random wait:
            if random_stimuli_wait = '1' then
                v_wait := rvt.RandTime(50 ns, 15000 ns); --150000
                wait for v_wait;
            end if;
        end loop;
    end process;

    inst_ethernet_test_transmiter : entity sda_test_lib.ethernet_test_transmiter
        port map(
            -- Global Signal Interface
            clk              => clk,    --: in std_logic;

            -- Transaction Interfaces
            axis_ethernet_if_m => client_in_axis_if_m, --: inout t_axis_ethernet_if ;
            axis_ethernet_if_s => client_in_axis_if_s, --: inout t_axis_ethernet_if ;
            packet_length    => packet_len, --: in natural;
            inject_error     => '0',    --inject_error, --: in std_logic;
            send             => send    --: in std_logic

        );

    inst_ethernet_test_receiver : entity sda_test_lib.ethernet_test_receiver
        port map(
            -- Global Signal Interface
            clk                 => clk, --: in    std_logic;
            -- Transaction Interfaces
            axis_ethernet_if_m    => client_out_axis_if_m, --: inout t_axis_ethernet_if;
            axis_ethernet_if_s    => client_out_axis_if_s, --: inout t_axis_ethernet_if;
            back_pressure       => back_pressure,
            eth_packet_length   => eth_packet_length, --: out   natural;
            eth_packet_id       => eth_packet_id, --: out   natural;
            eth_packet_valid    => eth_packet_valid, --: out   std_logic;
            eth_packet_error    => eth_packet_error, --: out   std_logic
            eth_packet_good_cnt => eth_packet_good_cnt
        );
    --eth_packet_valid <= '1';
    --axis_ethernet_if.tready <= '1';

        inst_let_sda_top_t1 : entity sda_oct_lib.let_sda_top_t1
        generic map(
            G_MINIMUN_FREE_BYTES_TO_SPLIT => 8, --: NATURAL := 8;
            G_STARTUP_DELAY               => 30 --: INTEGER := 30000
        )
        Port map(
            clk                                => clk, --: in  std_logic; -- Main clock 200 MHz
            rst                                => reset, --: in  std_logic;

            -- Input interface ETH
            ----------------------
            client_in_axis_if_m                     => client_in_axis_if_m, --: inout t_axis_if;
            client_in_axis_if_s                     => client_in_axis_if_s, --: inout t_axis_if;

            cfg_pause_data                         => cfg_pause_data, --: in std_logic_vector(15 downto 0); -- TODO: Connection missing
            tx_pause_valid                         => tx_pause_valid, --: out std_logic;
            tx_pause_data                          => tx_pause_data, --: out std_logic_vector(15 downto 0);
            rx_fifo_skipped_frame                  => rx_fifo_skipped_frame, --: out std_logic_vector(32 - 1 downto 0);

            -- Output interface ETH
            ----------------------
            client_out_axis_if_m                     => client_out_axis_if_m, --deframer_out_axis_tdata, --: out std_logic_vector(8 - 1 downto 0);
            client_out_axis_if_s                     => client_out_axis_if_s, --deframer_out_axis_tdata, --: out std_logic_vector(8 - 1 downto 0);

            -- Input interface Creonic
            ----------------------
            fec_in_axis_if_m                        => fec_rx_axis_if_m, 
            fec_in_axis_if_s                        => fec_rx_axis_if_s, 
            
            --preamble_synced                        => preamble_synced, --: in std_logic;
            crc16_valid                            => '1', --crc16_valid, --: in std_logic;
            crc16_correct                          => '1', --crc16_correct, --: in std_logic;
            crc32_valid                            => '1', --crc32_valid, --: in std_logic;
            crc32_correct                          => '1', --crc32_correct, --: in std_logic;

            -- Output interface Creonic
            ----------------------
            fec_out_axis_if_m                        => fec_tx_axis_if_m, --: out  std_logic_vector(8 - 1 downto 0);
            fec_out_axis_if_s                        => fec_tx_axis_if_s, --: out  std_logic_vector(8 - 1 downto 0);

            -- Framer Health Monitor
            ----------------------
            framer_warning_flag                    => open, --framer_warning_flag, --: out  std_logic;
            framer_critical_error_flag             => open, --framer_critical_error_flag, --: out  std_logic;
            framer_health_monitor_statistics       => open, --framer_health_monitor_statistics, --: out health_monitor_statistics_t;

            -- Deframer Health Monitor
            ----------------------
            deframer_warning_flag                  => open, --deframer_warning_flag, --: out  std_logic;
            deframer_critical_error_flag           => open, --deframer_critical_error_flag, --: out  std_logic;
            deframer_health_monitor_statistics     => open, --deframer_health_monitor_statistics, --: out health_monitor_statistics_t;

            -- FCCH
            ----------------------
            -- From Register Map
            fh_tx_fcch_if_m   => fh_tx_fcch_if_m, --: inout t_fcch_if;
            fh_tx_fcch_if_s   => fh_tx_fcch_if_s, --: inout t_fcch_if;
    
    
            -- To Register Map
            fh_rx_fcch_if_m   => fh_rx_fcch_if_m, --: inout t_fcch_if;
            fh_rx_fcch_if_s   => fh_rx_fcch_if_s, --: inout t_fcch_if;
            
            -- Statistics
            ----------------------
            clear_stat                             => '0', --clear_stat, --clear_stat, --: in  std_logic;
            -- Framer
            framer_total_packet_counter            => framer_total_packet_counter, --: out std_logic_vector(31 downto 0);
            framer_total_packet_splitted_counter   => framer_total_packet_splitted_counter, --: out std_logic_vector(31 downto 0);
            framer_total_data_frame_counter        => framer_total_data_frame_counter, --: out std_logic_vector(31 downto 0);
            framer_actual_data_frame_counter       => open , --framer_actual_data_frame_counter, --: out std_logic_vector(31 downto 0);
            framer_total_idle_frame_counter        => framer_total_idle_frame_counter, --: out std_logic_vector(31 downto 0);
            framer_total_packet_drop               => open, --framer_total_packet_drop, --: out std_logic_vector(31 downto 0);
            framer_total_prebuffer_packet_drop     => open, --framer_total_prebuffer_packet_drop, --: out std_logic_vector(31 downto 0);
            framer_frame_length_error              => open, --framer_frame_length_error, --: out std_logic_vector(31 downto 0);
            framer_bitrate_in                      => open, --framer_bitrate_in, --: out std_logic_vector(31 downto 0);
            framer_packet_cnt_in                   => open, --framer_packet_cnt_in, --: out std_logic_vector(31 downto 0);

            -- Deframer
            deframer_total_packet_counter          => deframer_total_packet_counter, --: out std_logic_vector(31 downto 0);
            deframer_total_payload_counter         => deframer_total_payload_counter, --: out std_logic_vector(31 downto 0);
            deframer_total_packet_splitted_counter => deframer_total_packet_splitted_counter, --: out std_logic_vector(31 downto 0);
            deframer_total_packet_merged_counter   => deframer_total_packet_merged_counter, --: out std_logic_vector(31 downto 0);
            deframer_total_data_frame_counter      => deframer_total_data_frame_counter, --: out std_logic_vector(31 downto 0);
            deframer_total_idle_frame_counter      => deframer_total_idle_frame_counter, --: out std_logic_vector(31 downto 0);
            deframer_total_payload_error_counter   => deframer_total_payload_error_counter, --: out std_logic_vector(31 downto 0);
            deframer_total_packet_error_counter    => deframer_total_packet_error_counter, --: out std_logic_vector(31 downto 0);
            deframer_watchdog_reset_counter        => deframer_watchdog_reset_counter, --: out std_logic_vector(31 downto 0);
            deframer_packet_filter_cnt_in          => deframer_packet_filter_cnt_in, --: out std_logic_vector(31 downto 0);
            deframer_packet_filter_cnt_out         => deframer_packet_filter_cnt_out, --: out std_logic_vector(31 downto 0);
            deframer_frame_filter_cnt_in           => deframer_frame_filter_cnt_in, --: out std_logic_vector(31 downto 0);
            deframer_frame_filter_cnt_out          => deframer_frame_filter_cnt_out, --: out std_logic_vector(31 downto 0);
            deframer_bitrate_out                   => deframer_bitrate_out, --: out std_logic_vector(31 downto 0);
            deframer_packet_cnt_out                => deframer_packet_cnt_out, --: out std_logic_vector(31 downto 0);

            sda_debug                              => open, -- sda_debug, --: out sda_debug_t;

            -- Version control
            ----------------------
            version_mayor                          => open, --: out unsigned(7 downto 0);
            version_minor                          => open --: out unsigned(7 downto 0)
        );

    -- Loopback:
    fec_tx_axis_if_s.tready <= fec_rx_axis_if_s.tready;
    fec_rx_axis_if_m.tvalid <= fec_tx_axis_if_m.tvalid;
    fec_rx_axis_if_m.tlast  <= fec_tx_axis_if_m.tlast;
    fec_rx_axis_if_m.tstart <= fec_tx_axis_if_m.tstart;
    fec_rx_axis_if_m.tdata  <= fec_tx_axis_if_m.tdata;

    fh_tx_fcch_if_m <= C_FCCH_IF_MASTER_INIT;
    fh_rx_fcch_if_s <= C_FCCH_IF_SLAVE_INIT;

    test_runner_watchdog(runner, 1 ms);
end architecture;
