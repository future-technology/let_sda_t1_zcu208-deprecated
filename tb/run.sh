#!/bin/bash

# Go to project root:
#cd ../
echo 'pwd'
mkdir -p ./temp/vunit_out
# Run docker:
#docker run --rm -v /$(pwd)://work -w //work ghdl/vunit:llvm-master sh -c ' VUNIT_SIMULATOR=ghdl; python3 ./tb/vunit_multi.py -o ./temp/vunit_out -v --gtkwave-fmt ghw '
#docker run --rm -v /$(pwd)://work -w //work ghdl/vunit:llvm-master sh -c ' VUNIT_SIMULATOR=ghdl; python3 ./tb/frame_header_tx_vunit/run.py -o ./temp/vunit_out -v --gtkwave-fmt ghw '
#docker run --rm -v /$(pwd)://work -w //work ghdl/vunit:llvm-master sh -c ' VUNIT_SIMULATOR=ghdl; python3 ./tb/frame_header_rx_vunit/run.py -o ./temp/vunit_out -v --gtkwave-fmt ghw '
#docker run --rm -v /$(pwd)://work -w //work ghdl/vunit:llvm-master sh -c ' VUNIT_SIMULATOR=ghdl; python3 ./tb/ethernet_deframer_vunit/run.py -o ./temp/vunit_out -v --gtkwave-fmt ghw '
#docker run --rm -v /$(pwd)://work -w //work ghdl/vunit:llvm-master sh -c ' VUNIT_SIMULATOR=ghdl; python3 ./tb/ethernet_framer_vunit/run.py -o ./temp/vunit_out -v --gtkwave-fmt ghw '
#docker run --rm -v /$(pwd)://work -w //work ghdl/vunit:llvm-master sh -c ' VUNIT_SIMULATOR=ghdl; python3 ./tb/ethernet_framer_top_vunit/run.py -o ./temp/vunit_out -v --gtkwave-fmt ghw '
#docker run --rm -v /$(pwd)://work -w //work ghdl/vunit:llvm-master sh -c ' VUNIT_SIMULATOR=ghdl; python3 ./tb/ethernet_deframer_top_vunit/run.py -o ./temp/vunit_out -v --gtkwave-fmt ghw '
#docker run --rm -v /$(pwd)://work -w //work ghdl/vunit:llvm-master sh -c ' VUNIT_SIMULATOR=ghdl; python3 ./tb/arq_timekeeper_tx_wrapper_vunit/run.py -o ./temp/vunit_out -v --gtkwave-fmt ghw '
#docker run --rm -v /$(pwd)://work -w //work ghdl/vunit:llvm-master sh -c ' VUNIT_SIMULATOR=ghdl; python3 ./tb/arq_timekeeper_tx_vunit/run.py -o ./temp/vunit_out -v --gtkwave-fmt ghw '
#docker run --rm -v /$(pwd)://work -w //work ghdl/vunit:llvm-master sh -c ' VUNIT_SIMULATOR=ghdl; python3 ./tb/arq_internal_ram_controller_vunit/run.py -o ./temp/vunit_out -v --gtkwave-fmt ghw '
#docker run --rm -v /$(pwd)://work -w //work ghdl/vunit:llvm-master sh -c ' VUNIT_SIMULATOR=ghdl; python3 ./tb/arq_ram_controller_top_vunit/run.py -o ./temp/vunit_out -v --gtkwave-fmt ghw '
#docker run --rm -v /$(pwd)://work -w //work ghdl/vunit:llvm-master sh -c ' VUNIT_SIMULATOR=ghdl; python3 ./tb/arq_router_tx_vunit/run.py -o ./temp/vunit_out -v --gtkwave-fmt ghw '
#docker run --rm -v /$(pwd)://work -w //work ghdl/vunit:llvm-master sh -c ' VUNIT_SIMULATOR=ghdl; python3 ./tb/arq_router_rx_vunit/run.py -o ./temp/vunit_out -v --gtkwave-fmt ghw '
#docker run --rm -v /$(pwd)://work -w //work ghdl/vunit:llvm-master sh -c ' VUNIT_SIMULATOR=ghdl; python3 ./tb/arq_transmitter_top_vunit/run.py -o ./temp/vunit_out -v --gtkwave-fmt ghw '
#docker run --rm -v /$(pwd)://work -w //work ghdl/vunit:llvm-master sh -c ' VUNIT_SIMULATOR=ghdl; python3 ./tb/let_sda_top_t1_vunit/run.py -o ./temp/vunit_out -v --gtkwave-fmt ghw '
#docker run --rm -v /$(pwd)://work -w //work ghdl/vunit:llvm-master sh -c ' VUNIT_SIMULATOR=ghdl; python3 ./tb/timestamp_insertion_vunit/run.py -o ./temp/vunit_out -v --gtkwave-fmt ghw '
#docker run --rm -v /$(pwd)://work -w //work ghdl/vunit:llvm-master sh -c ' VUNIT_SIMULATOR=ghdl; python3 ./tb/frame_header_fields_vunit/run.py -o ./temp/vunit_out -v --gtkwave-fmt ghw '
docker run --rm -v /$(pwd)://work -w //work ghdl/vunit:llvm-master sh -c ' VUNIT_SIMULATOR=ghdl; python3 ./tb/frame_timing_detector_vunit/run.py -o ./temp/vunit_out -v --gtkwave-fmt ghw '


