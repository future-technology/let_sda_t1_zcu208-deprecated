-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: eth_packet_test_pkg
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library sda_lib;
use sda_lib.pkg_sda.all;

package eth_packet_test_pkg is
    
    constant NUM_PARITY_BITS_16   : natural := 16;
    constant C_ETHERNET_MIN_BYTES : natural := 64;
    constant C_ETH_PACKET_PREAMBLE : std_logic_vector(16 - 1 downto 0) := x"FECA";
    
    type eth_packet_array_t is array (0 to C_ETHERNET_PACKET_MAX_LENGTH_BYTES - 1) of std_logic_vector(7 downto 0);
    
    type eth_packet_id_array_t is array (0 to 500-1) of natural;
    type eth_packet_length_array_t is array (0 to 500-1) of natural;
        
    constant C_SEED1 : integer := 1;
    constant C_SEED2 : integer := 1;
    
    type eth_packet_header_t is record
        magic        : std_logic_vector(15 downto 0);
        id           : std_logic_vector(15 downto 0);
        length       : std_logic_vector(15 downto 0);
    end record;
    
    function eth_packet_test_health_check(packet : eth_packet_array_t) return std_logic;
    function eth_packet_test_get_length(packet : eth_packet_array_t) return natural;
    function eth_packet_test_get_id(packet : eth_packet_array_t) return natural;
    function eth_packet_test_generate(id : natural; length : natural) return eth_packet_array_t;
    impure function eth_packet_test_generate_random(id : natural; seed1 : integer; seed2 : integer) return eth_packet_array_t;
    --function eth_test_get_eth_packet(packet : packet_array_t, offset : natural) return eth_packet_array_t;
    
    
    function crc32(input: std_logic_vector(7 downto 0); crc:  std_logic_vector(31 downto 0)) return std_logic_vector;
    impure function rand_int(min_val : integer; max_val : integer; seed1 : integer; seed2 : integer) return integer;
    impure function rand_slv(len : integer; seed1 : integer; seed2 : integer) return std_logic_vector;    
        
end package eth_packet_test_pkg;

package body eth_packet_test_pkg is
    
    function eth_packet_test_health_check(packet : eth_packet_array_t) return std_logic is
        variable v_eth_packet_header : eth_packet_header_t;
        variable v_shift_reg : std_logic_vector(31 downto 0);
        variable v_crc : std_logic_vector(31 downto 0);
        variable v_error : std_logic := '0';
    begin
        v_shift_reg := x"FFFFFFFF";
        v_error := '0';
        v_eth_packet_header.magic   := packet(1) & packet(0);
        v_eth_packet_header.id      := packet(3) & packet(2);
        v_eth_packet_header.length  := packet(5) & packet(4);
        v_crc                       := packet(to_integer(unsigned(v_eth_packet_header.length)) - 1 - 0) & 
                                        packet(to_integer(unsigned(v_eth_packet_header.length)) - 1 - 1) &
                                        packet(to_integer(unsigned(v_eth_packet_header.length)) - 1 - 2) & 
                                        packet(to_integer(unsigned(v_eth_packet_header.length)) - 1 - 3);
        
        --Preamble number check:
        if v_eth_packet_header.magic /= C_ETH_PACKET_PREAMBLE then
            v_error := '1';
            report "ERROR: Ethernet Packet preamble not detected !!!." severity failure; 
        end if;
        
        -- Length in range check:
        if (to_integer(unsigned(v_eth_packet_header.length)) < C_ETHERNET_MIN_BYTES or 
            to_integer(unsigned(v_eth_packet_header.length)) > C_ETHERNET_PACKET_MAX_LENGTH_BYTES) then
            v_error := '1'; 
            report "ERROR: Packet length not within ethernet range !!!." severity failure; 
        end if;
        
        --Compute CRC32
        for byten in 0 to (to_integer(unsigned(v_eth_packet_header.length)) - 1 - 4) loop
            v_shift_reg  := crc32(packet(byten), v_shift_reg);
        end loop;
        
        --Check CRC32
        if v_crc /= v_shift_reg then
            v_error := '1'; 
            report "ERROR: CRC32 check failed !!!." severity warning; 
        end if;
        
        return v_error;
    end function;
    
    function eth_packet_test_get_length(packet : eth_packet_array_t) return natural is
        variable v_eth_packet_header : eth_packet_header_t;
    begin
        v_eth_packet_header.length  := packet(5) & packet(4);
        return to_integer(unsigned(v_eth_packet_header.length));
    end function;
    
    function eth_packet_test_get_id(packet : eth_packet_array_t) return natural is
        variable v_eth_packet_header : eth_packet_header_t;
    begin
        v_eth_packet_header.id      := packet(3) & packet(2);
        return to_integer(unsigned(v_eth_packet_header.id));
    end function;
    
    function eth_packet_test_generate(id : natural; length : natural) return eth_packet_array_t is
        variable v_eth_packet   : eth_packet_array_t;
        variable v_id           : std_logic_vector(15 downto 0);
        variable v_length       : std_logic_vector(15 downto 0);
        variable v_crc          : std_logic_vector(31 downto 0);
        variable data           : unsigned(7 downto 0);
        variable data_slv       : std_logic_vector(7 downto 0);
    begin
        if length < 64 or length > C_ETHERNET_PACKET_MAX_LENGTH_BYTES then
            report "Length of eth_packet_test_generate out of bounds. id=" & to_string(id) & " len=" & to_string(length) severity failure;
        end if;

        v_crc           := x"FFFFFFFF";
        v_id            := std_logic_vector(to_unsigned(id, 16));
        v_length        := std_logic_vector(to_unsigned(length, 16));
        v_eth_packet(0) := C_ETH_PACKET_PREAMBLE(7 downto 0);
        v_eth_packet(1) := C_ETH_PACKET_PREAMBLE(15 downto 8);
        v_eth_packet(2) := v_id(7 downto 0);
        v_eth_packet(3) := v_id(15 downto 8);
        v_eth_packet(4) := v_length(7 downto 0);
        v_eth_packet(5) := v_length(15 downto 8);
        
        for byten in 0 to length-1 loop
            if byten > 5 then
                --v_eth_packet(byten) := rand_slv(8, to_integer(unsigned(v_crc)/2), to_integer(unsigned(v_crc)/2));
                data := unsigned(v_eth_packet(byten-1)) + to_unsigned(7, 8);
                --data_slv := (v_eth_packet(byten-1)(6 downto 0) & v_eth_packet(byten-1)(7));
                --data_slv := std_logic_vector(data(7 downto 0)) xor (not(v_id(7 downto 0))) xor v_length(7 downto 0);
--                if data_slv = 'U'  then
--                    report "ERROR: Eth packet data generation Undefined !!!." severity failure; 
--                end if;
                v_eth_packet(byten) := std_logic_vector(data);
                --report "Rand=" & to_hstring(v_eth_packet(byten)) & "h";
            end if;
            if byten < length - 1 - 3 then 
                v_crc  := crc32(v_eth_packet(byten), v_crc);
            end if;
        end loop;

        v_eth_packet(length - 1 - 0) := v_crc(31 downto 24);
        v_eth_packet(length - 1 - 1) := v_crc(23 downto 16);
        v_eth_packet(length - 1 - 2) := v_crc(15 downto 8);
        v_eth_packet(length - 1 - 3) := v_crc(7 downto 0);
        return v_eth_packet;
    end function;
    
    impure function eth_packet_test_generate_random(id : natural; seed1 : integer; seed2 : integer) return eth_packet_array_t is
        variable v_eth_packet   : eth_packet_array_t;
        variable v_crc          : std_logic_vector(31 downto 0);
        variable v_length       : integer;
        variable v_len_slv      : std_logic_vector(15 downto 0);
        variable v_id_slv      : std_logic_vector(15 downto 0);

    begin
        v_crc           := x"FFFFFFFF";
        v_length  := rand_int(64, C_ETHERNET_PACKET_MAX_LENGTH_BYTES, seed1, seed2);
        v_len_slv := std_logic_vector(to_unsigned(v_length, 16));
        v_id_slv  := std_logic_vector(to_unsigned(id, 16));
        
        v_eth_packet(0) := C_ETH_PACKET_PREAMBLE(7 downto 0);
        v_eth_packet(1) := C_ETH_PACKET_PREAMBLE(15 downto 8);
        v_eth_packet(2) := v_id_slv(7 downto 0);
        v_eth_packet(3) := v_id_slv(15 downto 8);
        v_eth_packet(4) := v_len_slv(7 downto 0);
        v_eth_packet(5) := v_len_slv(15 downto 8);
        
        for byten in 0 to v_length-1 loop
            if byten > 5 then
                v_eth_packet(byten) := rand_slv(8, to_integer(unsigned(v_crc)), to_integer(unsigned(v_crc)+2));
            end if;
            if byten < v_length - 1 - 3 then 
                v_crc  := crc32(v_eth_packet(byten), v_crc);
            end if;
        end loop;

        v_eth_packet(v_length - 1 - 1) := v_crc(31 downto 24);
        v_eth_packet(v_length - 1 - 2) := v_crc(23 downto 16);
        v_eth_packet(v_length - 1 - 3) := v_crc(15 downto 8);
        v_eth_packet(v_length - 1 - 4) := v_crc(7 downto 0);
        return v_eth_packet;
    end function;
    
--    function eth_test_get_eth_packet(packet : packet_array_t, offset : natural) return eth_packet_array_t is
--        variable v_eth_packet : eth_packet_array_t;
--    begin
--        for ii in 0 to v_eth_packet'length - 1 loop
--            v_eth_packet(ii) := packet(ii+C_HEADER_SIZE);
--        end loop;
--        for ii in 0 to 
--        v_eth_packet := packet(C_HEADER_SIZE + offset to C_FSO_FRAME_SIZE - 1);
--        return v_eth_packet;
--    end function;
    
    function crc32(input: std_logic_vector(7 downto 0); crc:  std_logic_vector(31 downto 0)) return std_logic_vector is
        variable v_sreg: std_logic_vector(31 downto 0);
        variable v_input : std_logic_vector(7 downto 0);
    begin
        v_input := input;

        v_sreg(0) := v_input(6) xor v_input(0) xor crc(24) xor crc(30);
        v_sreg(1) := v_input(7) xor v_input(6) xor v_input(1) xor v_input(0) xor crc(24) xor crc(25) xor crc(30) xor crc(31);
        v_sreg(2) := v_input(7) xor v_input(6) xor v_input(2) xor v_input(1) xor v_input(0) xor crc(24) xor crc(25) xor crc(26) xor crc(30) xor crc(31);
        v_sreg(3) := v_input(7) xor v_input(3) xor v_input(2) xor v_input(1) xor crc(25) xor crc(26) xor crc(27) xor crc(31);
        v_sreg(4) := v_input(6) xor v_input(4) xor v_input(3) xor v_input(2) xor v_input(0) xor crc(24) xor crc(26) xor crc(27) xor crc(28) xor crc(30);
        v_sreg(5) := v_input(7) xor v_input(6) xor v_input(5) xor v_input(4) xor v_input(3) xor v_input(1) xor v_input(0) xor crc(24) xor crc(25) xor crc(27) xor crc(28) xor crc(29) xor crc(30) xor crc(31);
        v_sreg(6) := v_input(7) xor v_input(6) xor v_input(5) xor v_input(4) xor v_input(2) xor v_input(1) xor crc(25) xor crc(26) xor crc(28) xor crc(29) xor crc(30) xor crc(31);
        v_sreg(7) := v_input(7) xor v_input(5) xor v_input(3) xor v_input(2) xor v_input(0) xor crc(24) xor crc(26) xor crc(27) xor crc(29) xor crc(31);
        v_sreg(8) := v_input(4) xor v_input(3) xor v_input(1) xor v_input(0) xor crc(0) xor crc(24) xor crc(25) xor crc(27) xor crc(28);
        v_sreg(9) := v_input(5) xor v_input(4) xor v_input(2) xor v_input(1) xor crc(1) xor crc(25) xor crc(26) xor crc(28) xor crc(29);
        v_sreg(10) := v_input(5) xor v_input(3) xor v_input(2) xor v_input(0) xor crc(2) xor crc(24) xor crc(26) xor crc(27) xor crc(29);
        v_sreg(11) := v_input(4) xor v_input(3) xor v_input(1) xor v_input(0) xor crc(3) xor crc(24) xor crc(25) xor crc(27) xor crc(28);
        v_sreg(12) := v_input(6) xor v_input(5) xor v_input(4) xor v_input(2) xor v_input(1) xor v_input(0) xor crc(4) xor crc(24) xor crc(25) xor crc(26) xor crc(28) xor crc(29) xor crc(30);
        v_sreg(13) := v_input(7) xor v_input(6) xor v_input(5) xor v_input(3) xor v_input(2) xor v_input(1) xor crc(5) xor crc(25) xor crc(26) xor crc(27) xor crc(29) xor crc(30) xor crc(31);
        v_sreg(14) := v_input(7) xor v_input(6) xor v_input(4) xor v_input(3) xor v_input(2) xor crc(6) xor crc(26) xor crc(27) xor crc(28) xor crc(30) xor crc(31);
        v_sreg(15) := v_input(7) xor v_input(5) xor v_input(4) xor v_input(3) xor crc(7) xor crc(27) xor crc(28) xor crc(29) xor crc(31);
        v_sreg(16) := v_input(5) xor v_input(4) xor v_input(0) xor crc(8) xor crc(24) xor crc(28) xor crc(29);
        v_sreg(17) := v_input(6) xor v_input(5) xor v_input(1) xor crc(9) xor crc(25) xor crc(29) xor crc(30);
        v_sreg(18) := v_input(7) xor v_input(6) xor v_input(2) xor crc(10) xor crc(26) xor crc(30) xor crc(31);
        v_sreg(19) := v_input(7) xor v_input(3) xor crc(11) xor crc(27) xor crc(31);
        v_sreg(20) := v_input(4) xor crc(12) xor crc(28);
        v_sreg(21) := v_input(5) xor crc(13) xor crc(29);
        v_sreg(22) := v_input(0) xor crc(14) xor crc(24);
        v_sreg(23) := v_input(6) xor v_input(1) xor v_input(0) xor crc(15) xor crc(24) xor crc(25) xor crc(30);
        v_sreg(24) := v_input(7) xor v_input(2) xor v_input(1) xor crc(16) xor crc(25) xor crc(26) xor crc(31);
        v_sreg(25) := v_input(3) xor v_input(2) xor crc(17) xor crc(26) xor crc(27);
        v_sreg(26) := v_input(6) xor v_input(4) xor v_input(3) xor v_input(0) xor crc(18) xor crc(24) xor crc(27) xor crc(28) xor crc(30);
        v_sreg(27) := v_input(7) xor v_input(5) xor v_input(4) xor v_input(1) xor crc(19) xor crc(25) xor crc(28) xor crc(29) xor crc(31);
        v_sreg(28) := v_input(6) xor v_input(5) xor v_input(2) xor crc(20) xor crc(26) xor crc(29) xor crc(30);
        v_sreg(29) := v_input(7) xor v_input(6) xor v_input(3) xor crc(21) xor crc(27) xor crc(30) xor crc(31);
        v_sreg(30) := v_input(7) xor v_input(4) xor crc(22) xor crc(28) xor crc(31);
        v_sreg(31) := v_input(5) xor crc(23) xor crc(29);
        return v_sreg;
    end crc32;
    
    impure function rand_slv(len : integer; seed1 : integer; seed2 : integer) return std_logic_vector is
      variable r : real;
      variable v_seed1 : integer := C_SEED1;
      variable v_seed2 : integer := C_SEED2;
      variable slv : std_logic_vector(len - 1 downto 0);
    begin
      v_seed1 := seed1;
      v_seed2 := seed2;
      if seed1 > 2147483562 then
          v_seed1 := 78;
      end if;
      if seed2 > 2147483562 then
          v_seed2 := 54;
      end if;
      uniform(v_seed1, v_seed2, r);
      uniform(v_seed1, v_seed2, r);
             
      for i in slv'range loop
        uniform(v_seed1, v_seed2, r); 
        --report "Rand=" & to_string(r);
        slv(i) := '1' when r > 0.5 else '0';  
      end loop;
      --report "Rand=" & to_hstring(slv) & "h";
      return slv;
    end function;
    
    impure function rand_int(min_val : integer; max_val : integer; seed1 : integer; seed2 : integer) return integer is
      variable r : real;
      variable v_seed1 : integer := C_SEED1;
      variable v_seed2 : integer := C_SEED2;
      variable output : integer;
    begin
      v_seed1 := seed1;
      v_seed2 := seed2;
      if seed1 > 2147483562 then
          v_seed1 := 78;
      end if;
      if seed2 > 2147483562 then
          v_seed2 := 54;
      end if;
      
      uniform(v_seed1, v_seed2, r);
      uniform(v_seed1, v_seed2, r);
      uniform(v_seed1, v_seed2, r);
      uniform(v_seed1, v_seed2, r);
      
      output := integer(
        round(r * real(max_val - min_val + 1) + real(min_val) - 0.5));
        
      return output;
    end function;
    
end package body eth_packet_test_pkg;
