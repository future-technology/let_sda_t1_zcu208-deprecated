-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: ethernet_test_receiver
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

library sda_test_lib;
use sda_test_lib.eth_packet_test_pkg.all;

entity ethernet_test_receiver is
    port(
        -- Global Signal Interface
        clk               : in    std_logic;
        -- Transaction Interfaces
        axis_ethernet_if_m  : in t_axis_if_m;
        axis_ethernet_if_s  : out t_axis_if_s;
        back_pressure     : in std_logic;
        eth_packet_length : out   natural;
        eth_packet_id     : out   natural;
        --eth_packet        : out   eth_packet_array_t;
        eth_packet_valid  : out   std_logic;
        eth_packet_error  : out   std_logic;
        eth_packet_good_cnt : out natural
    );
end entity;

architecture rtl of ethernet_test_receiver is

begin

    process
        variable v_eth_packet : eth_packet_array_t;
        variable v_index : natural := 0;
        variable v_seqnum : natural := 0;
        variable v_seqnum_ref : natural := 0;
        variable v_error : std_logic := '0';
    begin
        eth_packet_good_cnt     <= 0;
        loop
            
            --axis_ethernet_if_m.tvalid <= 'Z';
            --axis_ethernet_if_m.tlast  <= 'Z';
            --axis_ethernet_if_m.tuser  <= 'Z';
            --axis_ethernet_if_m.tdata  <= (others=>'Z');
            eth_packet_error       <= '0';
            eth_packet_valid       <= '0';
            eth_packet_id           <= 0;
            eth_packet_length       <= 0;
            if back_pressure = '1' then
                wait until rising_edge(clk);
                    axis_ethernet_if_s.tready <= '0';
                wait until back_pressure = '0';
            end if;
            axis_ethernet_if_s.tready <= '1';
            wait until rising_edge(clk);
            
            if axis_ethernet_if_m.tvalid = '1' and axis_ethernet_if_m.tlast = '0' and axis_ethernet_if_s.tready = '1' then
                if v_index < eth_packet_array_t'length then
                    v_eth_packet(v_index) := axis_ethernet_if_m.tdata;
                    v_index := v_index + 1;
                else
                    wait until rising_edge(clk);
                    eth_packet_error       <= '1';
                    wait until rising_edge(clk);
                    eth_packet_error       <= '0';
                    v_index := 0;
                end if;
            elsif axis_ethernet_if_m.tvalid = '1' and axis_ethernet_if_m.tlast = '1' and axis_ethernet_if_s.tready = '1' then
                if v_index < eth_packet_array_t'length then
                    v_eth_packet(v_index) := axis_ethernet_if_m.tdata;
                end if;
                v_index := 0;
                --eth_packet <= v_eth_packet;
                
                v_error := eth_packet_test_health_check(v_eth_packet);
                --report "eth_packet error=" & to_string(error); 
                eth_packet_error <= v_error;
                eth_packet_valid <= '1';
                if v_error = '0' then
                    eth_packet_id <= eth_packet_test_get_id(v_eth_packet);
                    eth_packet_length <= eth_packet_test_get_length(v_eth_packet);
                    eth_packet_valid <= '1';
                    eth_packet_good_cnt <= eth_packet_good_cnt + 1;
                    --report "Eth Packet Received!!! ID=" & to_string(eth_packet_id) & " (" & to_string(eth_packet_length) & " bytes)"; 
                end if;

                axis_ethernet_if_s.tready <= '0';
                wait until rising_edge(clk);

            end if;
        end loop;
    end process;   
end architecture;

    
    
