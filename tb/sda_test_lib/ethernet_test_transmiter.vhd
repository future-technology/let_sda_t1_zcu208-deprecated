-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: ethernet_test_transmiter
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

library sda_test_lib;
use sda_test_lib.eth_packet_test_pkg.all;

entity ethernet_test_transmiter is
  port (
      -- Global Signal Interface
      clk               : in std_logic;

      -- Transaction Interfaces
      axis_ethernet_if_m   : out t_axis_if_m;
      axis_ethernet_if_s   : in t_axis_if_s;
      packet_length      : in natural;
      inject_error       : in std_logic;
      send               : in std_logic
  ) ;
end entity;

architecture rtl of ethernet_test_transmiter is

    signal inject_error_int : std_logic; 
begin
    
    process
        variable v_eth_packet : eth_packet_array_t;
        variable packet_id : natural := 0;
        variable v_packet_len : natural;
        variable seed1,seed2 : positive := 1;
    begin
        axis_ethernet_if_m <= C_AXIS_IF_MASTER_INIT;
        seed1 := seed1 + 3;
        seed2 := seed2 + 7;
        
        wait until send = '1';
            
        if packet_length > 64-1 and packet_length < C_ETHERNET_PACKET_MAX_LENGTH_BYTES+1 then
            v_eth_packet := eth_packet_test_generate(packet_id, packet_length); -- id, len 
            v_packet_len := packet_length;     
        else
            v_eth_packet := eth_packet_test_generate_random(packet_id, seed1, seed2);
            v_packet_len := eth_packet_test_get_length(v_eth_packet);
        end if;
        inject_error_int <= inject_error;   
        --axis_ethernet_if
            
        for ii in 0 to v_packet_len - 1 loop    
            wait until rising_edge(clk);
            if axis_ethernet_if_s.tready = '1' then
                axis_ethernet_if_m.tdata  <= v_eth_packet(ii);
                axis_ethernet_if_m.tvalid <= '1';
                axis_ethernet_if_m.tlast  <= '0';
                if ii = v_packet_len - 1 then
                    axis_ethernet_if_m.tlast <= '1';
                    packet_id := packet_id + 1;
                end if;
            else
                axis_ethernet_if_m.tvalid <= '0';
                axis_ethernet_if_m.tlast  <= '0';
            end if;
            if inject_error_int = '1' then
                axis_ethernet_if_m.tuser <= '1';
            end if;
        end loop;
        wait until rising_edge(clk);

    end process;
    
end architecture;

