-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/07/2022 11:40:33 AM
-- Design Name: 
-- Module Name: frame_receiver
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

entity frame_receiver is
    port(
        clk : in std_logic;
        
        axis_if_m : in t_axis_if_m; -- Input axi-s
        axis_if_s : out t_axis_if_s; -- Input axi-s
        frame_header_fields : out frame_header_fields_t;
        payload_ram : out payload_ram_t;
        frame_length : out natural;
        valid : out std_logic
    );
end entity frame_receiver;

architecture RTL of frame_receiver is
    
begin
    --axis_if_m.tuser <= 'Z';
    
    process
        variable frame_full_ram : frame_full_ram_t;
        variable frame_header_ram : frame_header_ram_t;
        variable frame_byte_cnt : natural := 0;
        variable v_index : natural := 0;
    begin
        --axis_if_m.tvalid <= 'Z';
        --axis_if_m.tlast <= 'Z';
        axis_if_s.tready <= '0';
        --axis_if_m.tdata <= (others=>'Z');
        valid           <= '0';
        frame_full_ram := (others=>(others=>'0'));
        payload_ram <= (others=>(others=>'0'));
        frame_header_ram := (others=>(others=>'0'));
        
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        axis_if_s.tready <= '1';
        
        loop
            wait until rising_edge(clk);
            
            if axis_if_m.tvalid = '1' and axis_if_m.tlast = '0' then
                if v_index < frame_full_ram'length then
                    frame_full_ram(v_index) := axis_if_m.tdata;
                    v_index := v_index + 1;
                end if;
            elsif axis_if_m.tvalid = '1' and axis_if_m.tlast = '1' then
                if v_index < frame_full_ram'length then
                    frame_full_ram(v_index) := axis_if_m.tdata;
                end if;
                frame_length <= v_index + 1;
                v_index := 0;
                
                -- Take frame header:
                for ii in 0 to C_FRAME_HEADER_SIZE-1 loop
                    frame_header_ram(ii) := frame_full_ram(ii);
                end loop;
                frame_header_fields <= array2frame_header(frame_header_ram);
                
                -- Take payload header:
                for ii in 0 to C_FSO_PAYLOAD_SIZE-1 loop
                    payload_ram(ii) <= frame_full_ram(ii+C_FRAME_HEADER_SIZE);
                end loop;
                
                valid <= '1';
    
                axis_if_s.tready <= '0';
                wait until rising_edge(clk);
                valid <= '0';
                axis_if_s.tready <= '1';
    
            end if;
        end loop;
        
    end process;

end architecture RTL;
