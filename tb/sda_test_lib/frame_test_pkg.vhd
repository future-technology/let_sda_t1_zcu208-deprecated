-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/07/2022 11:40:33 AM
-- Design Name: 
-- Module Name: frame_test_pkg
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library sda_lib;
use sda_lib.pkg_sda.all;

package frame_test_pkg is
    
    type frame_header_fields_array_t is array (0 to 500-1) of frame_header_fields_t;
    
    --function payload_header2frame(fhf : frame_header_fields_t) return frame_full_ram_t;
    
    
end package frame_test_pkg;

package body frame_test_pkg is
    
--    function payload_header2frame(fhf : frame_header_fields_t, payload : payload_array_t) return frame_full_ram_t is
--        variable full_frame_ram             : frame_full_ram_t;
--        variable v_frame_header : std_logic_vector((C_FRAME_HEADER_SIZE * 8) - 1 downto 0) := (others => '0');
--    begin
--        v_frame_header := frame_header2slv(fhf);
--        array_loop : for i in 0 to C_FRAME_HEADER_SIZE - 1 loop
--            full_frame_ram(i) := v_frame_header(((i + 1) * 8) - 1 downto i * 8);
--        end loop;                       -- identifier
--        
--        return fh_ram;
--    end frame_full_ram_t;
    
end package body frame_test_pkg;
