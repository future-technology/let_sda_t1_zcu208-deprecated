-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: packet_test_pkg
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library sda_lib;
use sda_lib.pkg_sda.all;

library sda_test_lib;
use sda_test_lib.eth_packet_test_pkg.all;

package packet_test_pkg is

    constant C_ETHERNET_MIN_BYTES : natural := 64;
    type packet_array_t is array (0 to C_ETHERNET_PACKET_MAX_LENGTH_BYTES + C_HEADER_SIZE - 1) of std_logic_vector(7 downto 0);


    function packet_test_health_check(packet : packet_array_t) return std_logic;
    function packet_test_get_length(packet : packet_array_t) return natural;
    function packet_test_get_eth_packet(packet : packet_array_t) return eth_packet_array_t;
    --function packet_test_reassembly_packet(packet : packet_array_t; new_data : packet_array_t; offset : natural; length : natural) return packet_array_t;
    function packet_test_compose_packet(eth_packet : eth_packet_array_t; offset : natural; length : natural) return packet_array_t;

end package packet_test_pkg;

package body packet_test_pkg is

    function packet_test_health_check(packet : packet_array_t) return std_logic is
        variable v_packet_header : std_logic_vector(31 downto 0);
        variable v_pkh : packet_header_t;
        variable v_error : std_logic := '0';
    begin
        v_error := '0';
        v_packet_header(7 downto 0) := packet(0);
        v_packet_header(15 downto 8) := packet(1);
        v_packet_header(23 downto 16) := packet(2);
        v_packet_header(31 downto 24) := packet(3);
        
        v_pkh := slv2packet_header(v_packet_header);
        if v_pkh.magic_number /= C_PACKET_PREAMBLE then
            v_error := '1';
            report "ERROR: Packet preamble not detected !!!." severity failure; 
        end if;
        if to_integer(unsigned(v_pkh.length)) < C_ETHERNET_MIN_BYTES or to_integer(unsigned(v_pkh.length)) > C_ETHERNET_PACKET_MAX_LENGTH_BYTES then
            v_error := '1'; 
            report "ERROR: Packet length not within ethernet range !!!." severity failure; 
        end if;
        
        return v_error;
    end function;

    function packet_test_get_length(packet : packet_array_t) return natural is
        variable v_packet_header : std_logic_vector(31 downto 0);
        variable v_pkh : packet_header_t;
    begin
        v_packet_header(7 downto 0) := packet(0);
        v_packet_header(15 downto 8) := packet(1);
        v_packet_header(23 downto 16) := packet(2);
        v_packet_header(31 downto 24) := packet(3);
        
        v_pkh := slv2packet_header(v_packet_header);
        return to_integer(unsigned(v_pkh.length));
    end function;
    
    function packet_test_get_eth_packet(packet : packet_array_t) return eth_packet_array_t is
        variable v_eth_packet : eth_packet_array_t;
    begin
        for ii in 0 to v_eth_packet'length - 1 loop
            v_eth_packet(ii) := packet(ii+C_HEADER_SIZE);
        end loop;
        
        return v_eth_packet;
    end function;
    
--    procedure packet_test_reassembly_packet(
--        constant packet          : in packet_array_t; 
--        variable packet_offset   : inout natural; 
--        variable packet_size     : inout natural
--    ) is
--        variable v_packet : packet_array_t;
--    begin
--        if packet_offset+packet_size > packet_array_t'length then
--            report "ERROR: Compose packet data out of range !!!." severity failure; 
--        end if;
----        if length < 8 then
----            report "ERROR: Compose packet data too low !!!." severity failure; 
----        end if;
--
--        packet(packet_offset to packet_size+packet_size-1) := new_data(0 to length-1);
--        
--    end function;

    function packet_test_compose_packet(eth_packet : eth_packet_array_t; offset : natural; length : natural) return packet_array_t is
        variable v_packet   : packet_array_t;
        variable v_pkh      : packet_header_t;
        variable v_pkh_slv  : std_logic_vector(31 downto 0);
        variable v_virtual_offset : natural; 
    begin
        v_virtual_offset    := C_HEADER_SIZE + offset;
        v_pkh.magic_number  := C_PACKET_PREAMBLE;
        v_pkh.reserved      := (others=>'0');
        v_pkh.length        := std_logic_vector(to_unsigned(length, 14));
        v_pkh_slv           := packet_header2slv(v_pkh);
        
        if v_virtual_offset+length > packet_array_t'length then
            report "ERROR: Compose packet data out of range !!!." severity failure; 
        end if;
        if length < 8 then
            report "ERROR: Compose packet data too low !!!." severity failure; 
        end if;
        
        v_packet(0) := v_pkh_slv(7 downto 0);
        v_packet(1) := v_pkh_slv(15 downto 8);
        v_packet(2) := v_pkh_slv(23 downto 16);
        v_packet(3) := v_pkh_slv(31 downto 24);
        
        for ii in 0 to length-1 loop
            v_packet(ii+v_virtual_offset) := eth_packet(ii);
        end loop;
        --v_packet(v_virtual_offset to v_virtual_offset+length-1) := eth_packet(0 to length-1);
        
        return v_packet;
    end function;
    
end package body packet_test_pkg;

