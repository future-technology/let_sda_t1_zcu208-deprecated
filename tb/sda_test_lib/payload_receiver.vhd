-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: payload_receiver
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

library sda_test_lib;
use sda_test_lib.payload_test_pkg.all;


entity payload_receiver is
    port (
        -- Global Signal Interface
        clk               : in std_logic;
  
        -- Transaction Interfaces
        axis_payload_if_s    : out t_axis_payload_if_s;
        axis_payload_if_m    : in t_axis_payload_if_m;
        back_pressure      : in std_logic;
        tx_fn_out          : out std_logic_vector(15 downto 0);
        frame_type_out     : out std_logic_vector(1 downto 0);
        tx_num_out         : out std_logic_vector(2 downto 0);
        payload            : out payload_array_t;
        payload_valid      : out std_logic;
        error              : out std_logic;
        payload_length     : out natural;
        seqnum             : out natural
    );
end entity;

architecture rtl of payload_receiver is
    signal dbg_payload : payload_array_t;
    
begin
    
    -- tx_fn_out      <= axis_payload_if.tx_fn;
    -- frame_type_out <= axis_payload_if.frame_type;
    -- tx_num_out     <= axis_payload_if.tx_num;
    
    process
        variable v_payload : payload_array_t;
        variable v_index : natural := 0;
        variable v_seqnum : natural := 0;
        variable v_seqnum_ref : natural := 0;
    begin
        --axis_payload_if.axis_if.tready <= '0';
        --wait for 900 ns;
        if back_pressure = '1' then
            wait until rising_edge(clk);
            axis_payload_if_s.axis_if_s.tready <= '0';
--            report "waiting for back pressure down...";            
            wait until back_pressure = '0';
        end if;
--        report "after backpressure wait...";
        wait until rising_edge(clk);
        axis_payload_if_s.axis_if_s.tready <= '1';
        error           <= '0';
        payload_valid   <= '0';
        --wait;
        --v_index := 0;
        --wait until rising_edge(clk);
        
        if axis_payload_if_m.axis_if_m.tvalid = '1' and axis_payload_if_m.axis_if_m.tlast = '0' and axis_payload_if_s.axis_if_s.tready = '1' then
            if v_index < payload_array_t'length then
                v_payload(v_index) := axis_payload_if_m.axis_if_m.tdata;    
                dbg_payload(v_index) <= v_payload(v_index);
            end if;
            if axis_payload_if_m.axis_if_m.tstart = '1' then
                 tx_fn_out      <= axis_payload_if_m.tx_fn;
                frame_type_out <= axis_payload_if_m.frame_type;
                tx_num_out     <= axis_payload_if_m.tx_num;
                            
            end if;
            v_index := v_index + 1;
        elsif axis_payload_if_m.axis_if_m.tvalid = '1' and axis_payload_if_m.axis_if_m.tlast = '1' and axis_payload_if_s.axis_if_s.tready = '1' then
            if v_index < payload_array_t'length then
                v_payload(v_index) := axis_payload_if_m.axis_if_m.tdata;
                dbg_payload(v_index) <= v_payload(v_index);
            end if;
            payload_length <= v_index+1;
            v_index := 0;
            payload <= v_payload;
            payload_valid <= '1';
            if axis_payload_if_m.frame_type = C_FRAME_DATA then
                error <= payload_test_health_check(v_payload);            
                v_seqnum := payload_test_get_seqnum(v_payload);
                seqnum <= v_seqnum;
                --if seqnum_ref /= seqnum then
                --    report "ERROR: Payload seqnum failure: " & to_string(seqnum_ref) & " !!!." severity failure;
                --    error <= '1';
                --end if;
            end if;

            axis_payload_if_s.axis_if_s.tready <= '0';
            v_seqnum_ref := v_seqnum_ref + 1;
            wait until rising_edge(clk);

        end if;
--        else
--            report "ERROR: Payload Receiver out of bound: " & to_string(v_index) & " !!!." severity failure;
--            v_index := '0';
--            error <= '1';
--        end if;

    end process;
    
end architecture;
