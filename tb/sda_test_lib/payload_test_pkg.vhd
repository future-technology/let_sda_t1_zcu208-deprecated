-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: payload_test_pkg
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library sda_lib;
use sda_lib.pkg_sda.all;

library sda_test_lib;
use sda_test_lib.packet_test_pkg.all;
library sda_test_lib;
use sda_test_lib.eth_packet_test_pkg.all;

package payload_test_pkg is

    type tx_fn_array_t is array (0 to 500-1) of natural;
    type frame_type_array_t is array (0 to 500-1) of std_logic_vector(1 downto 0);
    type valid_array_t is array (0 to 500-1) of std_logic;

    type payload_array_t is array (0 to C_FSO_FRAME_SIZE - 1) of std_logic_vector(7 downto 0);
    
    function payload_test_health_check(payload : payload_array_t) return std_logic;
    function payload_test_get_length(payload : payload_array_t) return natural;
    function payload_test_get_seqnum(payload : payload_array_t) return natural;
    function payload_test_copy_packet(payload : payload_array_t; offset : natural; payload_size : natural) return packet_array_t;
    function payload_test_generate(length : natural; seq_num : natural) return payload_array_t;
    function payload_test_detect_packet(payload : payload_array_t; offset : natural) return std_logic;
    function payload_test_detect_packet_size(payload : payload_array_t; offset : natural) return natural;
        
    procedure payload_test_compose_payload(
        variable payload         : inout payload_array_t; 
        variable offset          : inout natural; 
        constant packet          : in packet_array_t; 
        variable packet_offset   : inout natural; 
        variable packet_size     : inout natural;
        variable skip            : out std_logic;
        variable split           : out std_logic;
        variable finished        : out std_logic
    );
    
    procedure payload_test_get_packet(
        variable payload       : in payload_array_t;
        variable offset        : inout natural;
        variable packet        : inout packet_array_t;
        variable packet_offset : inout natural;
        variable packet_size   : inout natural;
        variable split         : out std_logic;
        variable end_of_payload: out std_logic
    );
    
    procedure payload_test_read_payload_data(
        variable payload         : in payload_array_t; 
        --variable payload_offset  : inout natural;
        variable packet_offset   : inout natural;
        variable packet          : inout packet_array_t;  
        variable packet_size     : out natural;
        variable eth_vector_id   : inout eth_packet_id_array_t;
        variable eth_vector_len  : inout eth_packet_length_array_t;
        variable eth_vector_pointer : inout natural;
        variable split           : inout std_logic;
        variable payload_id_last : inout natural;
        variable error          : out std_logic
    );
    
    procedure payload_test_write_payload_data(
        variable payload                      : inout payload_array_t;
        variable payload_offset               : inout natural;
        variable payload_seq_num              : in natural;
        variable packet_offset                : inout natural;
        variable packet                       : inout packet_array_t;
        variable packet_size                  : inout natural;
        variable carry_data                   : inout natural;
        variable payload_valid                : out std_logic;
        variable split                        : out std_logic;
        variable skip                          : out std_logic
--        variable seq_num_err_injection        : in std_logic;
--        variable payload_header_err_injection : in std_logic;
--        variable payload_length_err_injection : in std_logic;
--        variable packet_header_err_injection  : in std_logic;
--        variable packet_align_err_injection   : in std_logic;
    );
    
end package payload_test_pkg;

package body payload_test_pkg is
    
    function payload_test_health_check(payload : payload_array_t) return std_logic is
        variable v_payload_header : std_logic_vector(31 downto 0);
        variable v_pkh : payload_header_t;
        variable v_error : std_logic := '0';
    begin
        v_error := '0';
        v_payload_header(7 downto 0) := payload(0);
        v_payload_header(15 downto 8) := payload(1);
        v_payload_header(23 downto 16) := payload(2);
        v_payload_header(31 downto 24) := payload(3);
        
        v_pkh := slv2payload_header(v_payload_header);
        if v_pkh.magic_number /= C_PAYLOAD_PREAMBLE then
            v_error := '1';
            --report "ERROR: payload preamble not detected !!!." severity warning; 
            --report to_string(payload(0));
            --report to_string(payload(1));
            --report to_string(payload(2));
            --report to_string(payload(3));
        else
            if to_integer(unsigned(v_pkh.length)) > C_ETHERNET_PACKET_MAX_LENGTH_BYTES then
                v_error := '1'; 
                report "ERROR: payload length not within ethernet range !!!." severity warning; 
            end if;
        end if;
        return v_error;
    end function;

    function payload_test_get_length(payload : payload_array_t) return natural is
        variable v_payload_header : std_logic_vector(31 downto 0);
        variable v_pkh : payload_header_t;
    begin
        v_payload_header(7 downto 0) := payload(0);
        v_payload_header(15 downto 8) := payload(1);
        v_payload_header(23 downto 16) := payload(2);
        v_payload_header(31 downto 24) := payload(3);
        
        v_pkh := slv2payload_header(v_payload_header);
        return to_integer(unsigned(v_pkh.length));
    end function;
    
    function payload_test_get_seqnum(payload : payload_array_t) return natural is
        variable v_payload_header : std_logic_vector(31 downto 0);
        variable v_pkh : payload_header_t;
    begin
        v_payload_header(7 downto 0) := payload(0);
        v_payload_header(15 downto 8) := payload(1);
        v_payload_header(23 downto 16) := payload(2);
        v_payload_header(31 downto 24) := payload(3);
        
        v_pkh := slv2payload_header(v_payload_header);
        return to_integer(unsigned(v_pkh.seq_num));
    end function;
    
    function payload_test_copy_packet(payload : payload_array_t; offset : natural; payload_size : natural) return packet_array_t is
        variable v_packet : packet_array_t;
    begin
        --payload_size  must be < payload_array_t'length
        if offset+payload_size > payload_array_t'length then
            report "ERROR: Compose packet data out of range !!!." severity failure; 
        end if;
        for ii in 0 to payload_size - 1 loop
            v_packet(ii) := payload(ii+C_HEADER_SIZE+offset);
        end loop;
        return v_packet;
    end function;
    
    function payload_test_detect_packet(payload : payload_array_t; offset : natural) return std_logic is
        variable v_packet_header : std_logic_vector(31 downto 0);
        variable v_pkh : packet_header_t;
        variable v_is_packet : std_logic := '0';
    begin
        v_is_packet := '0';
        
        if offset+C_HEADER_SIZE+3 < payload_array_t'length then
        
            v_packet_header(7 downto 0) := payload(0+offset+C_HEADER_SIZE);
            v_packet_header(15 downto 8) := payload(1+offset+C_HEADER_SIZE);
            v_packet_header(23 downto 16) := payload(2+offset+C_HEADER_SIZE);
            v_packet_header(31 downto 24) := payload(3+offset+C_HEADER_SIZE);
            
            v_pkh := slv2packet_header(v_packet_header);
            if v_pkh.magic_number = C_PACKET_PREAMBLE then
                v_is_packet := '1';
            end if;
        else
            v_is_packet := '0';
        end if;
        
        return v_is_packet;
    end function;
    
    function payload_test_detect_packet_size(payload : payload_array_t; offset : natural) return natural is
        variable v_packet_header : std_logic_vector(31 downto 0);
        variable v_pkh : packet_header_t;
        variable size : natural := 0;
    begin
        
        v_packet_header(7 downto 0) := payload(0+offset+C_HEADER_SIZE);
        v_packet_header(15 downto 8) := payload(1+offset+C_HEADER_SIZE);
        v_packet_header(23 downto 16) := payload(2+offset+C_HEADER_SIZE);
        v_packet_header(31 downto 24) := payload(3+offset+C_HEADER_SIZE);
        
        v_pkh := slv2packet_header(v_packet_header);
        size  := to_integer(unsigned(v_pkh.length)+C_HEADER_SIZE);
        
        return size;
    end function;
    
    
    function payload_test_generate(length : natural; seq_num : natural) return payload_array_t is
        variable v_payload   : payload_array_t;
        variable v_ph      : payload_header_t;
        variable v_ph_slv  : std_logic_vector(31 downto 0);
        variable v_virtual_offset : natural; 
    begin
        v_ph.magic_number  := C_PAYLOAD_PREAMBLE;
        v_ph.seq_num       := std_logic_vector(to_unsigned(seq_num, 10));
        v_ph.length        := std_logic_vector(to_unsigned(length, 14));
        v_ph_slv           := payload_header2slv(v_ph);
        
--        if v_virtual_offset+packet_size > packet_array_t'length then
--            report "ERROR: Compose packet data out of range !!!." severity failure; 
--        end if;
--        if length < 8 then
--            report "ERROR: Compose packet data too low !!!." severity failure; 
--        end if;
        
        v_payload(0) := v_ph_slv(7 downto 0);
        v_payload(1) := v_ph_slv(15 downto 8);
        v_payload(2) := v_ph_slv(23 downto 16);
        v_payload(3) := v_ph_slv(31 downto 24);
          
--        for ii in 0 to packet_size-1 loop
--            v_payload(ii+v_virtual_offset) := packet(ii);
--        end loop;
        --v_payload(v_virtual_offset to v_virtual_offset+length-1) := packet(0 to length-1);
        
        return v_payload;
    end function;
    
    procedure payload_test_compose_payload(
        variable payload         : inout payload_array_t; 
        variable offset          : inout natural; 
        constant packet          : in packet_array_t; 
        variable packet_offset   : inout natural; 
        variable packet_size     : inout natural;
        variable skip            : out std_logic;
        variable split           : out std_logic;
        variable finished        : out std_logic
    ) is
        variable v_payload   : payload_array_t;
        variable v_ph      : payload_header_t;
        variable v_ph_slv  : std_logic_vector(31 downto 0);
        variable v_virtual_offset : natural; 
        variable v_p_remaining : natural;
        variable v_byte    : natural;
        variable v_32balign    : natural;
    begin
        --v_payload          := payload;
        v_virtual_offset    := C_HEADER_SIZE + offset;
        v_p_remaining       := v_payload'length - v_virtual_offset; --remaining bytes available in this payload.
        skip                := '0';
        split               := '0';
        v_byte              := 0;
        
--        if packet_offset = 0 then
--            packet_offset := C_HEADER_SIZE;
--        end if;
--        if v_virtual_offset+packet_size > packet_array_t'length then
--            report "ERROR: Compose packet data out of range !!!." severity failure; 
--        end if;
        v_32balign := (v_virtual_offset) mod 4;
        while v_32balign /= 0 loop 
            --v_virtual_offset    := C_HEADER_SIZE + offset;
            if v_virtual_offset > payload_array_t'length - 1 then
                exit;
            end if;
            payload(v_virtual_offset) := (others => '0');
            offset := offset + 1;
            v_virtual_offset    := C_HEADER_SIZE + offset;
            v_32balign := (v_virtual_offset) mod 4;
        end loop;

        v_p_remaining := v_payload'length - v_virtual_offset;
        --v_virtual_offset    := C_HEADER_SIZE + offset;
        
        if v_p_remaining < 8+1 then
            report "INFO: Remaining available space not enough !!!." severity warning; 
            skip := '1';
            finished := '1';
            v_virtual_offset := C_HEADER_SIZE + offset;
            -- add padding:
            for ii in v_virtual_offset to payload_array_t'length - 1 loop
                v_byte := ii;
                payload(ii) := (others => '0');
            end loop;
            offset := offset + (v_byte+1);   
        else
            
            for ii in 0 to packet_size-1 loop
                
                
                -- if limit reached exit loop.
                if ii+v_virtual_offset = payload_array_t'length then
                    split := '1';
                    finished := '1';
                    exit;
                else
                    v_byte := ii;
                    payload(ii+v_virtual_offset) := packet(ii+packet_offset);
                end if;
              
            end loop;
            
            -- update packet offset remaining     
            packet_size   := packet_size - (v_byte+1);
            packet_offset := packet_offset + (v_byte+1); 
            offset        := offset + (v_byte+1);
            v_virtual_offset    := C_HEADER_SIZE + offset;
            if v_virtual_offset = payload_array_t'length then
                finished := '1';
            end if;
                
        end if;
        --payload := v_payload;       
    end procedure;
    
    procedure payload_test_get_packet(
        variable payload       : in payload_array_t;
        variable offset        : inout natural;
        variable packet        : inout packet_array_t;
        variable packet_offset : inout natural;
        variable packet_size   : inout natural;
        variable split         : out std_logic;
        variable end_of_payload: out std_logic
    ) is
        variable v_virtual_offset : natural;
        variable v_p_remaining    : natural;
        variable v_byte           : natural;
        variable v_32balign    : natural;
    begin
        split            := '0';
        end_of_payload   := '0';
        v_virtual_offset := C_HEADER_SIZE + offset;
        --payload_size  must be < payload_array_t'length

        v_32balign := (v_virtual_offset) mod 4;
        while v_32balign /= 0 loop
            offset := offset + 1;
            v_virtual_offset    := C_HEADER_SIZE + offset;
            if v_virtual_offset > payload_array_t'length-1 then
                end_of_payload := '1';
            end if;
            
            v_32balign := (v_virtual_offset) mod 4;
        end loop;

        if end_of_payload = '0' then
            for ii in 0 to packet_size - 1 loop
                
                -- if limit reached exit loop.
                if ii + v_virtual_offset > payload_array_t'length-1 then
                    --v_byte := ii;
                    --packet(ii + packet_offset) := payload(ii + v_virtual_offset);
                    report "REPORT limit reached: " & to_string(ii) & "  " & to_string(v_virtual_offset) & "  " & to_string(ii + v_virtual_offset);
                    --check packet is split
                    if ii > 0 then
                        --packet split
                        split := '1';
                    end if;
                    end_of_payload := '1';
                    exit;
                else
                    v_byte := ii;
                    packet(ii + packet_offset) := payload(ii + v_virtual_offset);
                end if;
    
            end loop;
            -- update packet offset and size remaining     
            packet_size   := packet_size - (v_byte + 1);
            packet_offset := packet_offset + (v_byte + 1);
            offset        := offset + (v_byte + 1);            
            
        end if;
        

        
        report "REPORT: " & to_string(offset) & "  " & to_string(packet_offset) & "  " & to_string(packet_size);

    end procedure;
    
    procedure payload_test_read_payload_data(
        variable payload         : in payload_array_t; 
        --variable payload_offset  : inout natural;
        variable packet_offset   : inout natural;
        variable packet          : inout packet_array_t;  
        variable packet_size     : out natural;
        variable eth_vector_id   : inout eth_packet_id_array_t;
        variable eth_vector_len  : inout eth_packet_length_array_t;
        variable eth_vector_pointer : inout natural;
        variable split           : inout std_logic;
        variable payload_id_last : inout natural;
        variable error          : out std_logic
    ) is
     variable v_eth_packet : eth_packet_array_t;
     variable pay_offset : natural := 0;
     variable payload_len : natural := 0;
     variable payload_id : natural := 0;
     variable is_packet  : std_logic := '0';
     variable end_of_payload : std_logic := '0';
     variable v_32balign : natural := 0;
     
    begin
        error := '0';
        pay_offset  := 0;
        is_packet   := '0';
        error := payload_test_health_check(payload);
        if error = '0' then
            payload_id  := payload_test_get_seqnum(payload);
                
            payload_len  := payload_test_get_length(payload);
            if payload_len > 0 and payload_id = payload_id_last+1 then
                if split = '0' then
                    report "Error: payload lost. Expected no split, but got split. len=" & to_string(payload_len) & ". id=" & to_string(payload_id); 
                end if;                    
                packet_size     := payload_len; 
                is_packet   := '1';
                --pk_offset   := pk_previous_offset; 

            else
                if split = '1' then
                    report "Error: payload lost. Expected split len=" & to_string(packet_size) & ". id=" & to_string(payload_id); 
                end if;
                is_packet   := payload_test_detect_packet(payload, pay_offset);
                packet_offset   := 0;
                packet_size     := 0;
                packet    := (others=>(others=>'0'));
            end if;
            
            while is_packet = '1' loop
                if payload_len = 0 or packet_offset = 0 then 
                    packet_size     := payload_test_detect_packet_size(payload, pay_offset);
                    packet_offset   := 0;
                    packet    := (others=>(others=>'0'));
                end if;
                payload_test_get_packet(payload, pay_offset, packet, packet_offset, packet_size, split, end_of_payload);
                --v_packet := packet_test_reassembly_packet(v_packet; v_packet_aux; offset : natural; length : natural)
                if split = '1' or end_of_payload = '1' then
                    exit;
                end if;
                
                -- if finished reading packet, process it.                    
                if packet_size = 0 then
                    error := packet_test_health_check(packet);
                    --report "packet error=" & to_string(error);
                    v_eth_packet := packet_test_get_eth_packet(packet);
                    error := eth_packet_test_health_check(v_eth_packet);
                    --report "eth_packet error=" & to_string(error); 
                    if error = '0' then
                        eth_vector_id(eth_vector_pointer) := eth_packet_test_get_id(v_eth_packet);
                        eth_vector_len(eth_vector_pointer) := eth_packet_test_get_length(v_eth_packet);
                        eth_vector_pointer := eth_vector_pointer + 1;
                        --report "Eth Packet Received!!! ID=" & to_string(eth_packet_id) & " (" & to_string(eth_packet_len) & " bytes)"; 
                    end if;
                    packet_offset := 0;
                    packet    := (others=>(others=>'0'));
                end if;
                -- TODO: ALIGN 32 BITS:
                v_32balign := (pay_offset) mod 4;
                while v_32balign /= 0 loop
                    pay_offset := pay_offset + 1;
                    if pay_offset > payload_array_t'length-1 then
                        exit;
                        end_of_payload := '1';
                        is_packet := '0';
                    end if;
                    v_32balign := (pay_offset) mod 4;
                end loop;
                if end_of_payload = '0' then
                    -- Detect packet
                    is_packet   := payload_test_detect_packet(payload, pay_offset);
                end if;
                
            end loop;
        else
            report "WARNING: Payload not valid." severity warning; 
        end if;
        payload_id_last := payload_id;

    end procedure;
    
    procedure payload_test_write_payload_data(
        variable payload                      : inout payload_array_t;
        variable payload_offset               : inout natural;
        variable payload_seq_num              : in natural;
        variable packet_offset                : inout natural;
        variable packet                       : inout packet_array_t;
        variable packet_size                  : inout natural;
        variable carry_data                   : inout natural;
        variable payload_valid                : out std_logic;
        variable split                          : out std_logic;
        variable skip                          : out std_logic
--        variable seq_num_err_injection        : in std_logic;
--        variable payload_header_err_injection : in std_logic;
--        variable payload_length_err_injection : in std_logic;
--        variable packet_header_err_injection  : in std_logic;
--        variable packet_align_err_injection   : in std_logic;
    ) is
        variable finished : std_logic := '0';
        --variable skip : std_logic := '0';
    
    begin

        payload_valid := '0';
        
        if payload_offset = 0 then
            --generate payload, init header
            payload := payload_test_generate(carry_data, payload_seq_num);
            --report "payload_test_generate=" & to_string(jj+1);
        end if;
        --insert packet into payload
        payload_test_compose_payload(payload, payload_offset, packet, packet_offset, packet_size, skip, split, finished);
        
        --check if packet was split
        if split = '1' then
            payload_offset := 0;
            carry_data := packet_size; 
            payload_valid := '1';
        elsif finished = '1' then
            payload_offset := 0;
            payload_valid := '1';
            if split = '0' then
                carry_data := 0;
            end if;
        end if;
        
--        --check if packet was split
--        if split = '1' then
--            pay_offset := 0;
--            payload_index := payload_index + 1;
--            carry_data := pk_size; 
--            v_payload_cache(payload_index) := payload_test_generate(carry_data, payload_index+1);
--            payload_test_compose_payload(v_payload_cache(payload_index), pay_offset, v_packet_cache(jj), pk_offset, pk_size, skip, split);
--            
--            --check if packet was split. T1 supports up to 2 splits for the same packet (3 payloads).
--            if split = '1' then
--                pay_offset := 0;
--                payload_index := payload_index + 1;
--                carry_data := pk_size; 
--                v_payload_cache(payload_index) := payload_test_generate(carry_data, payload_index+1);
--                payload_test_compose_payload(v_payload_cache(payload_index), pay_offset, v_packet_cache(jj), pk_offset, pk_size, skip, split);
--            end if;
--            if split = '1' then
--                pay_offset := 0;
--                payload_index := payload_index + 1;
--            end if;
--        end if;
    end procedure;
    
end package body payload_test_pkg;
