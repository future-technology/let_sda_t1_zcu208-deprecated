-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: payload_transmiter
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

library sda_test_lib;
use sda_test_lib.payload_test_pkg.all;
use sda_test_lib.packet_test_pkg.all;
use sda_test_lib.eth_packet_test_pkg.all;

entity payload_transmiter is
    port(
        -- Global Signal Interface
        clk                        : in    std_logic;
        -- Transaction Interfaces
        tx_fn_init                 : in std_logic_vector(16-1 downto 0);
        axis_payload_if_m            : out t_axis_payload_if_m;
        axis_payload_if_s            : in t_axis_payload_if_s;
        eth_vector_id               : in eth_packet_id_array_t;
        eth_vector_length           : in eth_packet_length_array_t;
        stimuli_length             : in natural;
        start_stimuli               : in std_logic;
        is_idle                     : in std_logic;
        finished                    : out std_logic;
        tx_fn_error_injection      : in std_logic;
        frame_type_error_injection : in std_logic;
        seqnum_error_injection : in std_logic
    );
end entity;

architecture rtl of payload_transmiter is
    
    signal start_axis : std_logic;
    signal payload  : payload_array_t;
    signal tx_fn_int    : std_logic_vector(15 downto 0) := (others =>'0');
    signal frame_type_int   :  std_logic_vector(1 downto 0) := (others =>'0');
    signal tx_finished : std_logic := '1';
    signal state_idle : std_logic := '0';

    begin
        axis_payload_if_m.tx_fn <= tx_fn_int;
        axis_payload_if_m.tx_num <= (others=>'0');

        --axis_payload_if_s.axis_if_s.tready <= 'Z';

        PRCS_STIMULI: process
            variable v_eth_packet_cache : eth_packet_array_t;
            variable v_packet_cache : packet_array_t;
            variable v_payload_cache : payload_array_t;
--            variable eth_vector_id      : eth_packet_id_array_t;
--            variable eth_vector_length  : eth_packet_length_array_t;
            variable eth_vector_pointer : natural;
            
            variable split                      : std_logic;
            variable skip                      : std_logic;
            variable v_payload                  : payload_array_t;
            variable payload_offset               : natural := 0;
            variable payload_seq_num              : natural := 0;
            variable packet_offset                :  natural := 0;
            variable packet                       :  packet_array_t;
            variable packet_size                  :  natural := 0;
            variable payload_valid                :  std_logic := '0';
            variable carry_data : natural := 0;
            variable v_init : std_logic := '0';
        begin

            state_idle <= '0';
            start_axis <= '0';
            finished <= '0';
            --tx_fn_int <= (others=>'0');
            for kk in 0 to payload'length-1 loop
                payload(kk) <= (others=>'0');
                v_payload(kk) := (others=>'0');
            end loop;

            if is_idle = '1' then
                axis_payload_if_m.frame_type <= C_FRAME_IDLE;
            else
                axis_payload_if_m.frame_type <= C_FRAME_DATA;
            end if;
            --axis_payload_if.frame_type <= C_FRAME_IDLE;
            wait until start_stimuli = '1';
            tx_fn_int <= tx_fn_init;
            
            if is_idle = '1' then
                state_idle <= '1';
               if frame_type_error_injection = '0' then
                    axis_payload_if_m.frame_type <= C_FRAME_IDLE;
               else
                    axis_payload_if_m.frame_type <= C_FRAME_DATA;
               end if;

                for jj in 0 to stimuli_length-1 loop
                    v_payload(0) := x"01";
                    for ii in 0 to payload_array_t'length - 2 loop
                        v_payload(ii+1) := std_logic_vector(unsigned(v_payload(ii))+1);
                    end loop;
                    
                   --transmit payload:
                   wait until rising_edge(clk);
                   start_axis <= '1';
                   payload <= v_payload;
                   wait until rising_edge(clk);
                   start_axis <= '0';
                   
                   --wait until payload is transmited.
                   wait until axis_payload_if_m.axis_if_m.tvalid = '1' and axis_payload_if_m.axis_if_m.tlast = '1' and axis_payload_if_s.axis_if_s.tready = '1';
                   -- update TX_FN
                   if tx_fn_error_injection = '0' then
                       tx_fn_int <= std_logic_vector(unsigned(tx_fn_int)+1);
                   else
                       tx_fn_int <= std_logic_vector(unsigned(tx_fn_int)+4);
                   end if;
               end loop;
               wait until rising_edge(clk);
               finished <= '1';
               wait until rising_edge(clk);
               finished <= '0';
               state_idle <= '0';
            else
                for jj in 0 to stimuli_length-1 loop
                    v_eth_packet_cache := eth_packet_test_generate(eth_vector_id(jj), eth_vector_length(jj)); -- id=1 len=64
                    v_packet_cache  := packet_test_compose_packet(v_eth_packet_cache, 0, eth_vector_length(jj));
                    packet_size := eth_vector_length(jj)+C_HEADER_SIZE;
                    if skip = '1' then
                        payload_offset := 0;
                    end if;
                    packet_offset := 0;
                    loop
                        if seqnum_error_injection = '1' then
                            payload_seq_num := payload_seq_num + 4;
                        end if;
                        payload_test_write_payload_data(
                            v_payload,
                            payload_offset,
                            payload_seq_num, 
                            packet_offset,
                            v_packet_cache,
                            packet_size,
                            carry_data,
                            payload_valid,
                            split,
                            skip);
                            
                        if payload_valid = '1' then
                           payload_seq_num := payload_seq_num + 1;
                           payload_offset := 0;
                           
                           --transmit payload:
                           --wait until tx_finished = '1';
                           wait until rising_edge(clk);
                           start_axis <= '1';
                           payload <= v_payload;
                           if frame_type_error_injection = '0' then
                                axis_payload_if_m.frame_type <= C_FRAME_DATA;
                           else
                                axis_payload_if_m.frame_type <= C_FRAME_IDLE;
                           end if;
                           wait until rising_edge(clk);
                           start_axis <= '0';
                           
                           --wait until payload is transmited.
                           wait until axis_payload_if_m.axis_if_m.tvalid = '1' and axis_payload_if_m.axis_if_m.tlast = '1' and axis_payload_if_s.axis_if_s.tready = '1' and falling_edge(clk);
                           
                           -- update TX_FN
                           if tx_fn_error_injection = '0' then
                               tx_fn_int <= std_logic_vector(unsigned(tx_fn_int)+1);
                           else
                               tx_fn_int <= std_logic_vector(unsigned(tx_fn_int)+4);
                           end if;
                           
                        end if;
                        if (split = '0' or (split = '0' and payload_valid = '1')) and skip = '0' then
                            exit;
                        end if;
                    end loop;
                end loop;
                
                -- end of the stimuli, check if current payload is finished. If not, add padding and send.
                if payload_valid = '0' then
                    for ii in payload_offset+C_HEADER_SIZE to payload_array_t'length - 1 loop
                        v_payload(ii) := (others=>'0');
                    end loop;
                   if frame_type_error_injection = '0' then
                        axis_payload_if_m.frame_type <= C_FRAME_DATA;
                   else
                        axis_payload_if_m.frame_type <= C_FRAME_IDLE;
                   end if; 
                   --transmit payload:
                   --wait until tx_finished = '1';
                   wait until rising_edge(clk);
                   start_axis <= '1';
                   payload <= v_payload;
                   wait until rising_edge(clk);
                   start_axis <= '0';
                   
                   --wait until payload is transmited.
                   wait until axis_payload_if_m.axis_if_m.tvalid = '1' and axis_payload_if_m.axis_if_m.tlast = '1' and axis_payload_if_s.axis_if_s.tready = '1' and falling_edge(clk);
                   wait until rising_edge(clk);
                   -- update TX_FN
                   if tx_fn_error_injection = '0' then
                       tx_fn_int <= std_logic_vector(unsigned(tx_fn_int)+1);
                   else
                       tx_fn_int <= std_logic_vector(unsigned(tx_fn_int)+4);
                   end if;
                   wait until rising_edge(clk);
                   finished <= '1';
                   wait until rising_edge(clk);
                   finished <= '0';
                   
               else
                   wait until rising_edge(clk);
                   finished <= '1';
                   wait until rising_edge(clk);
                   finished <= '0';
                end if;
                split := '0';
                skip := '0';
                carry_data := 0;
                payload_offset := 0;

            end if;
        end process;

        PRCS_AXIS_TRANSMIT: process

        begin
            tx_finished <= '1';
            axis_payload_if_m.axis_if_m.tdata  <= (others=>'0');
            axis_payload_if_m.axis_if_m.tlast  <= '0';
            axis_payload_if_m.axis_if_m.tvalid <= '0';
            axis_payload_if_m.axis_if_m.tstart <= '0';
            axis_payload_if_m.axis_if_m.tuser <= '0';
            --axis_payload_if.axis_if.tready <= 'Z';

            wait until start_axis = '1';
            tx_finished <= '0';
            wait until rising_edge(clk); -- and axis_payload_if.axis_if.tready = '1';
            for ii in 0 to payload_array_t'length - 1 loop   
                axis_payload_if_m.axis_if_m.tvalid <= '1'; 
                axis_payload_if_m.axis_if_m.tstart <= '0';
                axis_payload_if_m.axis_if_m.tdata  <= payload(ii);
                if ii = payload_array_t'length - 1 then
                    axis_payload_if_m.axis_if_m.tlast <= '1';
                    exit;
                elsif ii = 0 then
                    axis_payload_if_m.axis_if_m.tstart <= '1';
                end if;
                wait until rising_edge(clk) and axis_payload_if_s.axis_if_s.tready = '1';
                            
            end loop;
            wait until rising_edge(clk) and axis_payload_if_s.axis_if_s.tready = '1';
            tx_finished <= '1';
            axis_payload_if_m.axis_if_m.tvalid <= '0'; 
            axis_payload_if_m.axis_if_m.tlast  <= '0';
            axis_payload_if_m.axis_if_m.tstart <= '0';
            
        end process;


end architecture;
