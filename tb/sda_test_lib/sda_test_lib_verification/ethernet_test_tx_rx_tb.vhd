-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: ethernet_test_tx_rx_tb
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library sda_lib;
use sda_lib.pkg_sda.all;

library sda_test_lib;
use sda_test_lib.sda_test_pkg.all;
library sda_test_lib;
use sda_test_lib.eth_packet_test_pkg.all;
library sda_test_lib;
use sda_test_lib.packet_test_pkg.all;
library sda_test_lib;
use sda_test_lib.payload_test_pkg.all;

library sda_test_lib;
use sda_test_lib.ethernet_test_transmiter;

entity ethernet_test_tx_rx_tb is

end entity ethernet_test_tx_rx_tb;

architecture RTL of ethernet_test_tx_rx_tb is

    type eth_packet_cache_t is array (0 to 4) of eth_packet_array_t;
    type eth_packet_cache_size_t is array (0 to eth_packet_cache_t'high) of natural; 
    type packet_cache_t is array (0 to 4) of packet_array_t;
    type payload_cache_t is array (0 to (eth_packet_cache_t'length * 3) - 1) of payload_array_t;
    
    signal axis_ethernet_if_m : t_axis_ethernet_if_m;
    signal axis_ethernet_if_s : t_axis_ethernet_if_s;
    signal axis_ethernet_if_aux_m : t_axis_ethernet_if_m;
    signal axis_ethernet_if_aux_s : t_axis_ethernet_if_s;
    signal clk : std_logic := '0';
    signal packet_len : natural;
    signal inject_error : std_logic := '0';
    signal send : std_logic := '0';

    signal axis_payload_if_m            :  t_axis_payload_if_m;
    signal axis_payload_if_s            :  t_axis_payload_if_s;
    signal tx_fn                      :  std_logic_vector(15 downto 0);
    signal frame_type                 :  std_logic_vector(1 downto 0);
    signal eth_vector_id               :  eth_packet_id_array_t;
    signal eth_vector_length           :  eth_packet_length_array_t;
    signal stimuli_length             :  natural;
    signal start_stimuli               :  std_logic;
    signal tx_fn_error_injection      :  std_logic;
    signal frame_type_error_injection :  std_logic;
    signal is_idle                      : std_logic;
    signal pt_finished                  : std_logic;
    signal tvalid_aux                  : std_logic;
    
    signal eth_packet_length : natural;
    signal eth_packet_id     : natural;
        --eth_packet        :    eth_packet_array_t;
    signal eth_packet_valid  : std_logic;
    signal eth_packet_error  : std_logic;

begin
    
    clk <= not clk after 5 ns;
    
    
    DUT_1 : entity sda_test_lib.ethernet_test_receiver
    port map (
        -- Global Signal Interface
        clk               => clk, --: in    std_logic;
        -- Transaction Interfaces
        axis_ethernet_if_m  => axis_ethernet_if_aux_m, --: inout t_axis_ethernet_if;
        axis_ethernet_if_s  => axis_ethernet_if_aux_s, --: inout t_axis_ethernet_if;
        eth_packet_length => eth_packet_length, --: out   natural;
        eth_packet_id     => eth_packet_id, --: out   natural;
        eth_packet_valid  => eth_packet_valid, --: out   std_logic;
        eth_packet_error  => eth_packet_error --: out   std_logic
    );



    DUT_2 : entity sda_test_lib.ethernet_test_transmiter
    port map (
      -- Global Signal Interface
      clk                => clk, --: in std_logic;

      -- Transaction Interfaces
      axis_ethernet_if_m   => axis_ethernet_if_m, --: inout t_axis_ethernet_if ;
      axis_ethernet_if_s   => axis_ethernet_if_s, --: inout t_axis_ethernet_if ;
      packet_length      => packet_len, --: in natural;
      inject_error       => inject_error, --: in std_logic;
      send               => send --: in std_logic
     
    );
    
    axis_ethernet_if_aux_m.tdata    <= axis_ethernet_if_m.tdata;
    axis_ethernet_if_aux_m.tvalid   <= axis_ethernet_if_m.tvalid and tvalid_aux;
    axis_ethernet_if_s.tready       <= axis_ethernet_if_aux_s.tready;
    axis_ethernet_if_aux_m.tlast    <= axis_ethernet_if_m.tlast;
    axis_ethernet_if_aux_m.tuser    <= axis_ethernet_if_m.tuser;
    process
        
    begin
        packet_len <= 1518;
        send <= '0';
        tvalid_aux <= '1';
        wait for 30 ns;
        send <= '1';
        wait for 20 ns;
        send <= '0';
        wait until rising_edge(clk);
        wait for 500 ns;
        wait until rising_edge(clk);
        tvalid_aux <= '0';
        wait until rising_edge(clk);
        tvalid_aux <= '1';
        
        wait until eth_packet_valid = '1';
        report "Eth Packet Received!!! ID=" & to_string(eth_packet_id) & " (" & to_string(eth_packet_length) & " bytes)"; 
        if eth_packet_error = '1' then
            report "Error!!!";
        end if;
        
        --wait;
    end process;
  
        


end architecture RTL;
