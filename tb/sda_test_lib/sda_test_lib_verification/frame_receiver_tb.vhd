library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library sda_test_lib;
library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;


entity frame_receiver_tb is

end entity frame_receiver_tb;

architecture RTL of frame_receiver_tb is
    
signal clk :  std_logic := '0';
signal axis_if :  t_axis_if; -- Input axi-s
signal frame_header_fields :  frame_header_fields_t;
signal payload_ram :  payload_ram_t;
signal frame_length :  natural;
signal header_length :  natural;
signal valid :  std_logic;
signal error :  std_logic;
    
begin

    clk <= not clk after 5 ns;

    UUT: entity sda_test_lib.frame_receiver
        port map(
            clk                 => clk, --: in std_logic;
            axis_if_m             => axis_if_m, --: inout t_axis_if; -- Input axi-s
            axis_if_s             => axis_if_s, --: inout t_axis_if; -- Input axi-s
            frame_header_fields => frame_header_fields, --: out frame_header_fields_t;
            payload_ram         => payload_ram, --: out payload_ram_t;
            frame_length        => frame_length, --: out natural;
            valid               => valid --: out std_logic;
        );
        
        
    process
        variable v_frame_header_ram : frame_header_ram_t;
        variable v_frame_header_fields : frame_header_fields_t;
    begin
        axis_if <= C_AXIS_IF_MASTER_INIT;
        v_frame_header_fields              := C_FRAME_HEADER_INIT;
        v_frame_header_fields.txfn         := x"1234";
        v_frame_header_fields.ack_start_fn := x"ABCD";
        v_frame_header_fields.ack_span     := "101";
        v_frame_header_fields.ack_valid    := '1';
        v_frame_header_fields.ack          := '1';
        v_frame_header_fields.tx_num       := "100";
        v_frame_header_fields.pl_rate      := "0011";
        v_frame_header_fields.frame_type   := "10";
        v_frame_header_fields.tx_ts        := (others => '1');
        v_frame_header_fields.fcch_opcode  := "1010";
        v_frame_header_fields.fcch_pl      := (others => '0');  
           
        v_frame_header_ram := frame_header2ram(v_frame_header_fields);
        
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        
        wait until axis_if_s.tready = '1' and rising_edge(clk);
        
        for ii in 0 to C_FRAME_FULL_SIZE_BYTES-1 loop
            if ii = C_FRAME_FULL_SIZE_BYTES-1 then
                axis_if_m.tdata <= x"FF";
                axis_if_m.tvalid <= '1';
                axis_if_m.tlast <= '1';
            else
                if ii < C_FRAME_HEADER_SIZE then
                    axis_if_m.tdata <= v_frame_header_ram(ii);
                    axis_if_m.tvalid <= '1';
                    axis_if_m.tlast <= '0';
                else
                    axis_if_m.tdata <= std_logic_vector(to_unsigned(ii+1, 8));
                    axis_if_m.tvalid <= '1';
                    axis_if_m.tlast <= '0';
                end if;

            end if;
            wait until rising_edge(clk) and axis_if_s.tready = '1';
        end loop;
        axis_if_m <= C_AXIS_IF_MASTER_INIT;
        axis_if_s <= C_AXIS_IF_SLAVE_INIT;
        wait for 50 ns;
        
        v_frame_header_fields              := C_FRAME_HEADER_INIT;
        v_frame_header_fields.txfn         := x"1234";
        v_frame_header_fields.ack_start_fn := x"ABCD";
        v_frame_header_fields.ack_span     := "101";
        v_frame_header_fields.ack_valid    := '1';
        v_frame_header_fields.ack          := '1';
        v_frame_header_fields.tx_num       := "100";
        v_frame_header_fields.pl_rate      := "0011";
        v_frame_header_fields.frame_type   := "10";
        v_frame_header_fields.tx_ts        := (others => '1');
        v_frame_header_fields.fcch_opcode  := "1010";
        v_frame_header_fields.fcch_pl      := (others => '0');  
           
        v_frame_header_ram := frame_header2ram(v_frame_header_fields);
        
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        
        wait until axis_if_s.tready = '1' and rising_edge(clk);
        
        for ii in 0 to C_FRAME_FULL_SIZE_BYTES-1 loop
            if ii = C_FRAME_FULL_SIZE_BYTES-1 then
                axis_if_m.tdata <= x"FF";
                axis_if_m.tvalid <= '1';
                axis_if_m.tlast <= '1';
            else
                if ii < C_FRAME_HEADER_SIZE then
                    axis_if_m.tdata <= v_frame_header_ram(ii);
                    axis_if_m.tvalid <= '1';
                    axis_if_m.tlast <= '0';
                else
                    axis_if_m.tdata <= std_logic_vector(to_unsigned(ii+1, 8));
                    axis_if_m.tvalid <= '1';
                    axis_if_m.tlast <= '0';
                end if;

            end if;
            wait until rising_edge(clk) and axis_if_s.tready = '1';
        end loop;
        axis_if_m <= C_AXIS_IF_MASTER_INIT;
        axis_if_s <= C_AXIS_IF_SLAVE_INIT;
        wait;
        
    end process;
        
        

end architecture RTL;
