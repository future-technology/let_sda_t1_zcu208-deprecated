-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: payload_tx_rx_tb
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library sda_lib;
use sda_lib.pkg_sda.all;

library sda_test_lib;
use sda_test_lib.sda_test_pkg.all;
library sda_test_lib;
use sda_test_lib.eth_packet_test_pkg.all;
library sda_test_lib;
use sda_test_lib.packet_test_pkg.all;
library sda_test_lib;
use sda_test_lib.payload_test_pkg.all;

library sda_test_lib;
use sda_test_lib.ethernet_test_transmiter;

entity payload_tx_rx_tb is

end entity payload_tx_rx_tb;

architecture RTL of payload_tx_rx_tb is

    type eth_packet_cache_t is array (0 to 4) of eth_packet_array_t;
    type eth_packet_cache_size_t is array (0 to eth_packet_cache_t'high) of natural; 
    type packet_cache_t is array (0 to 4) of packet_array_t;
    type payload_cache_t is array (0 to (eth_packet_cache_t'length * 3) - 1) of payload_array_t;
    
    signal axis_ethernet_if : t_axis_ethernet_if;
    signal clk : std_logic := '0';
    signal packet_len : natural;
    signal inject_error : std_logic := '0';
    signal send : std_logic := '0';

    signal axis_payload_if            :  t_axis_payload_if;
    signal tx_fn                      :  std_logic_vector(15 downto 0);
    signal frame_type                 :  std_logic_vector(1 downto 0);
    signal eth_vector_id               :  eth_packet_id_array_t;
    signal eth_vector_length           :  eth_packet_length_array_t;
    signal stimuli_length             :  natural;
    signal start_stimuli               :  std_logic;
    signal tx_fn_error_injection      :  std_logic;
    signal frame_type_error_injection :  std_logic;
    signal is_idle                      : std_logic;
    signal pt_finished                  : std_logic;

begin
    
    clk <= not clk after 5 ns;
    
    
DUT : entity sda_test_lib.payload_transmiter
    port map(
        -- Global Signal Interface
        clk                        => clk, --: in    std_logic;
        -- Transaction Interfaces
        axis_payload_if_m            => axis_payload_if_m, --: inout t_axis_payload_if;
        axis_payload_if_s            => axis_payload_if_s, --: inout t_axis_payload_if;
        eth_vector_id              => eth_vector_id, -- : in eth_packet_id_array_t;
        eth_vector_length          => eth_vector_length, -- : in eth_packet_length_array_t;
        stimuli_length             => stimuli_length, --: in natural;
        is_idle                    => is_idle,
        finished                   => pt_finished,
        start_stimuli              => start_stimuli, -- : in std_logic;
        tx_fn_error_injection      => tx_fn_error_injection, --: in std_logic;
        frame_type_error_injection => frame_type_error_injection  --: in std_logic
    );

    process
    begin
        axis_payload_if_s.axis_if_s.tready <= '0';
        
        
        frame_type_error_injection <= '0';
        tx_fn_error_injection <= '0';
        -------------------------------
        is_idle <= '0';
        start_stimuli <= '0';
        stimuli_length <= 3;
        for ii in 0 to eth_packet_length_array_t'length - 1 loop
            eth_vector_id(ii)       <= ii+1;
            eth_vector_length(ii)   <= 519;
        end loop;
        --eth_vector_length(3)   <= 64;
        wait for 90 ns;
        wait until rising_edge(clk);
        start_stimuli <= '1';
        wait until rising_edge(clk);
        start_stimuli <= '0';
        
        wait for 1000 ns;
        axis_payload_if_s.axis_if_s.tready <= '1';
        wait until pt_finished = '1';
        wait;
        -------------------------------
        -------------------------------
        is_idle <= '1';
        stimuli_length <= 4;
        wait until rising_edge(clk);
        start_stimuli <= '1';
        wait until rising_edge(clk);
        start_stimuli <= '0';
        wait until pt_finished = '1';
        -------------------------------
        -------------------------------
        is_idle <= '0';
        start_stimuli <= '0';
        stimuli_length <= 10;
        for ii in 0 to eth_packet_length_array_t'length - 1 loop
            eth_vector_id(ii)       <= ii+1;
            eth_vector_length(ii)   <= 1510;
        end loop;
        wait for 90 ns;
        wait until rising_edge(clk);
        start_stimuli <= '1';
        wait until rising_edge(clk);
        start_stimuli <= '0';
        wait until pt_finished = '1';
        -------------------------------
        -------------------------------
        is_idle <= '1';
        stimuli_length <= 3;
        wait until rising_edge(clk);
        start_stimuli <= '1';
        tx_fn_error_injection <= '1'; -- Injecting error!
        wait until rising_edge(clk);
        start_stimuli <= '0';
        wait until pt_finished = '1';
        -------------------------------
        -------------------------------
        is_idle <= '0';
        start_stimuli <= '0';
        stimuli_length <= 3;
        for ii in 0 to eth_packet_length_array_t'length - 1 loop
            eth_vector_id(ii)       <= ii+1;
            eth_vector_length(ii)   <= 1510;
        end loop;
        wait for 90 ns;
        wait until rising_edge(clk);
        start_stimuli <= '1';
        wait until rising_edge(clk);
        start_stimuli <= '0';
        wait until pt_finished = '1';
        -------------------------------
        tx_fn_error_injection <= '0';
        wait;
    end process;

--    DUT : entity sda_test_lib.ethernet_test_transmiter
--    port map (
--      -- Global Signal Interface
--      clk                => clk, --: in std_logic;

--      -- Transaction Interfaces
--      axis_ethernet_if   => axis_ethernet_if, --: inout t_axis_ethernet_if ;
--      packet_length      => packet_len, --: in natural;
--      inject_error       => inject_error, --: in std_logic;
--      send               => send --: in std_logic
        
--    );
    

  
--    process
        
--    begin
--        packet_len <= 0;
--        send <= '0';
--        wait for 30 ns;
--        send <= '1';
--        wait for 20 ns;
--        send <= '0';
--        wait;
--        wait until axis_ethernet_if.tlast = '1';
--    end process;
  

    process
        variable v_eth_packet : eth_packet_array_t;
        variable v_packet : packet_array_t;
        variable v_payload : payload_array_t;
        
        variable v_eth_packet2 : eth_packet_array_t;
        variable v_packet2 : packet_array_t;
        variable v_payload2 : payload_array_t;
        
        variable payload_len : natural;
        variable error : std_logic;
        variable total_packet_num : natural := 0;
        variable pay_offset : natural := 0;
        variable pk_offset : natural := 0;
        variable payload_index : natural := 0;
        
        variable pk_size : natural := 64;
        variable carry_data : natural := 0;
        variable skip : std_logic;
        variable split : std_logic;
        variable is_packet : std_logic;
        variable end_of_payload : std_logic;
        variable eth_packet_id : natural;
        variable eth_packet_len : natural;
        variable payload_id : natural := 0;
        variable payload_id_last : natural := 0;
        variable eth_packet_valid : std_logic;
        
        variable v_eth_packet_a : eth_packet_array_t;
        variable v_packet_a : packet_array_t;
        variable v_eth_packet_b : eth_packet_array_t;
        variable v_packet_b : packet_array_t;
        variable v_eth_packet_cache : eth_packet_cache_t;
        variable v_packet_cache : packet_cache_t;
        variable v_payload_cache : payload_cache_t;
        variable v_eth_pk_sizes : eth_packet_cache_size_t;
        
        variable eth_vector_id      : eth_packet_id_array_t;
        variable eth_vector_length  : eth_packet_length_array_t;
        variable eth_vector_pointer : natural;
        
        variable payload                      : payload_array_t;
        variable payload_offset               : natural := 0;
        variable payload_seq_num              : natural := 0;
        variable packet_offset                :  natural := 0;
        variable packet                       :  packet_array_t;
        variable packet_size                  :  natural := 0;
        variable payload_valid                :  std_logic := '0';
        
    begin
        
        total_packet_num := 4;
        pay_offset := 0;
        payload_index := 0;
        pk_size := 64;
        v_eth_pk_sizes := (1010, 1510, 64, 64, 64);
        
--        for ii in 0 to total_packet_num-1 loop
--            v_eth_packet_cache(ii)  := eth_packet_test_generate(ii+1, v_eth_pk_sizes(ii)); -- id=1 len=64
--            v_packet_cache(ii)      := packet_test_compose_packet(v_eth_packet_cache(ii), 0, v_eth_pk_sizes(ii));
--        end loop;
--        
--        for jj in 0 to total_packet_num-1 loop
--            v_packet := v_packet_cache(jj);
--            packet_size := v_eth_pk_sizes(jj)+C_HEADER_SIZE;
--            split := '0';
--            packet_offset := 0;
--            loop
--                payload_test_write_payload_data(
--                    payload,
--                    payload_offset,
--                    payload_seq_num, 
--                    packet_offset,
--                    v_packet,
--                    packet_size,
--                    carry_data,
--                    payload_valid,
--                    split);
--                    
--                if payload_valid = '1' then
--                   payload_seq_num := payload_seq_num + 1;
--                   payload_offset := 0;
--                   --wait until payload is transmited.
--                end if;
--                if split = '0' or (split = '0' and payload_valid = '1') then
--                    exit;
--                end if;
--            end loop;
--        end loop;
        wait;
--        carry_data := 0;
--        for jj in 0 to total_packet_num-1 loop
--            --reset packet offset
--            pk_offset := 0;
--            pk_size := v_eth_pk_sizes(jj)+4; -- + CRC
--            
--            if pay_offset = 0 then
--                --generate payload, init header
--                v_payload_cache(payload_index) := payload_test_generate(carry_data, payload_index+1);
--                report "payload_test_generate=" & to_string(jj+1);
--            end if;
--            --insert packet into payload
--            payload_test_compose_payload(v_payload_cache(payload_index), pay_offset, v_packet_cache(jj), pk_offset, pk_size, skip, split);
--            
--            --check if packet was split
--            if split = '1' then
--                pay_offset := 0;
--                payload_index := payload_index + 1;
--                carry_data := pk_size; 
--                v_payload_cache(payload_index) := payload_test_generate(carry_data, payload_index+1);
--                payload_test_compose_payload(v_payload_cache(payload_index), pay_offset, v_packet_cache(jj), pk_offset, pk_size, skip, split);
--                
--                --check if packet was split. T1 supports up to 2 splits for the same packet (3 payloads).
--                if split = '1' then
--                    pay_offset := 0;
--                    payload_index := payload_index + 1;
--                    carry_data := pk_size; 
--                    v_payload_cache(payload_index) := payload_test_generate(carry_data, payload_index+1);
--                    payload_test_compose_payload(v_payload_cache(payload_index), pay_offset, v_packet_cache(jj), pk_offset, pk_size, skip, split);
--                end if;
--                if split = '1' then
--                    pay_offset := 0;
--                    payload_index := payload_index + 1;
--                end if;
--
--            end if;
--    
--        end loop;
        
        
--        v_eth_packet := eth_packet_test_generate(1, 64); -- id=1 len=64
--        v_packet     := packet_test_compose_packet(v_eth_packet, 0, 64);
--        
--        v_payload    := payload_test_generate(0, 1);
--        pk_size      := 64+4;
--        payload_test_compose_payload(v_payload, pay_offset, v_packet, pk_offset, pk_size, skip, split);
--        pk_size      := 64+4;
--        pk_offset    := 0;
--        payload_test_compose_payload(v_payload, pay_offset, v_packet, pk_offset, pk_size, skip, split);
--        
--        payload_len  := payload_test_get_length(v_payload);
--        
--        report "payload_len=" & to_string(payload_len);
--        
--        pay_offset := 0;
--        pk_offset  := 0;
--        error := payload_test_health_check(v_payload);
--        report "payload error=" & to_string(error);
--        
--        is_packet := payload_test_detect_packet(v_payload, pay_offset);
--        report "is_packet=" & to_string(is_packet);
        
        -- detect packet size
        
--        if is_packet = '1' then
--            -- payload_test_
--            pk_size := payload_test_detect_packet_size(v_payload, pay_offset);
--            payload_test_get_packet(v_payload, pay_offset, v_packet_a, pk_offset, pk_size, split);
--            error := packet_test_health_check(v_packet_a);
--            report "packet error=" & to_string(error);
--            
--            v_eth_packet_a := packet_test_get_eth_packet(v_packet_a);
--            error := eth_packet_test_health_check(v_eth_packet_a);
--            report "eth_packet error=" & to_string(error);          
--        end if;
--        
--
--        is_packet := payload_test_detect_packet(v_payload, pay_offset);
--        report "is_packet=" & to_string(is_packet);
--        pk_offset  := 0;
--        if is_packet = '1' then
--
--            pk_size := payload_test_detect_packet_size(v_payload, pay_offset);
--            
--            payload_test_get_packet(v_payload, pay_offset, v_packet_b, pk_offset, pk_size, split);
--            error := packet_test_health_check(v_packet_b);
--            report "packet error=" & to_string(error);
--            
--            v_eth_packet_b := packet_test_get_eth_packet(v_packet_b);
--            error := eth_packet_test_health_check(v_eth_packet_b);
--            report "eth_packet error=" & to_string(error);
--                       
--        end if;
        payload_id_last := 0;
        eth_vector_pointer := 0;

        for ii in 0 to 7 loop
            
            payload_test_read_payload_data(
                v_payload_cache(ii),  
                pk_offset, 
                v_packet, 
                pk_size, 
                eth_vector_id, 
                eth_vector_length, 
                eth_vector_pointer,
                split,
                payload_id_last,
                error);

        end loop;
        report "Eth Packet Received: ";
        for jj in 0 to eth_vector_pointer loop
            if eth_vector_length(jj) > 63 then 
                report "ID=" & to_string(eth_vector_id(jj)) & " (" & to_string(eth_vector_length(jj)) & " bytes)"; 
            end if;
        end loop;

--        for ii in 0 to 7 loop
--            pay_offset  := 0;
--            is_packet   := '0';
--            error := payload_test_health_check(v_payload_cache(ii));
--            if error = '0' then
--                payload_id  := payload_test_get_seqnum(v_payload_cache(ii));
--                    
--                payload_len  := payload_test_get_length(v_payload_cache(ii));
--                if payload_len > 0 and payload_id = payload_id_last+1 then
--                    if split = '0' then
--                        report "Error: payload lost. Expected no split, but got split. len=" & to_string(payload_len) & ". id=" & to_string(ii); 
--                    end if;                    
--                    pk_size     := payload_len; 
--                    is_packet   := '1';
--                    --pk_offset   := pk_previous_offset; 
--
--                else
--                    if split = '1' then
--                        report "Error: payload lost. Expected split len=" & to_string(pk_size) & ". id=" & to_string(ii); 
--                    end if;
--                    is_packet   := payload_test_detect_packet(v_payload_cache(ii), pay_offset);
--                    pk_offset   := 0;
--                    pk_size     := 0;
--                    v_packet    := (others=>(others=>'0'));
--                end if;
--                
--                while is_packet = '1' loop
--                    if payload_len = 0 or pk_offset = 0 then 
--                        pk_size     := payload_test_detect_packet_size(v_payload_cache(ii), pay_offset);
--                        pk_offset   := 0;
--                        v_packet    := (others=>(others=>'0'));
--                    end if;
--                    payload_test_get_packet(v_payload_cache(ii), pay_offset, v_packet, pk_offset, pk_size, split, end_of_payload);
--                    --v_packet := packet_test_reassembly_packet(v_packet; v_packet_aux; offset : natural; length : natural)
--                    if split = '1' or end_of_payload = '1' then
--                        exit;
--                    end if;
--                    
--                    -- if finished reading packet, process it.                    
--                    if pk_size = 0 then
--                        error := packet_test_health_check(v_packet);
--                        report "packet error=" & to_string(error);
--                        v_eth_packet := packet_test_get_eth_packet(v_packet);
--                        error := eth_packet_test_health_check(v_eth_packet);
--                        report "eth_packet error=" & to_string(error); 
--                        if error = '0' then
--                            eth_packet_id := eth_packet_test_get_id(v_eth_packet);
--                            eth_packet_len := eth_packet_test_get_length(v_eth_packet);
--                            report "Eth Packet Received!!! ID=" & to_string(eth_packet_id) & " (" & to_string(eth_packet_len) & " bytes)"; 
--                        end if;
--                        pk_offset := 0;
--                        v_packet    := (others=>(others=>'0'));
--                    end if;
--                    
--                    -- Detect packet
--                    is_packet   := payload_test_detect_packet(v_payload_cache(ii), pay_offset);
--                    
--                end loop;
--            else
--                report "Payload not valid=" & to_string(ii); 
--            end if;
--            payload_id_last := payload_id;
--        end loop;
        
        wait;
    end process;

end architecture RTL;
