-------------------------------------------------------------------------------
--  Company    : Mynaric Lasercom GmbH, Gilching, Germany
--
--  Restricted � Highly Confidential
--
--  COPYRIGHT
--
--  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
--  This software program is the proprietary copyright material of
--  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
--  is intended to be effected by any use of this software program.
--  This applies also to the permitted use by an authorized user under license
--  notwithstanding the terms and conditions of any agreement entered into
--  by an end user with an intermediary other than Mynaric Lasercom GmbH.
--
--  No part of this software program may be reproduced, amended, varied,
--  re-written or stored in a retrieval system in any form or by any means
--  without the prior written permission of Mynaric Lasercom GmbH.
--
-------------------------------------------------------------------------------
-- Company: Mynaric
-- Engineer: Gustavo Martin
-- 
-- Create Date: 25/06/2022 11:40:33 AM
-- Design Name: 
-- Module Name: tb_timestamp_insertion
-- Project Name: LET SDA
-- Target Devices: 
-- Tool Versions: Vivado 2021.1
-- Description: Library package to test SDA.
-- 
-- Dependencies: 
-- 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

library osvvm;
use osvvm.RandomPkg.all;
use osvvm.CoveragePkg.all;

library sda_lib;
use sda_lib.pkg_sda.all;
use sda_lib.interfaces_pkg.all;

library frame_header_lib;

library sda_test_lib;
use sda_test_lib.frame_receiver;

entity tb_timestamp_insertion is
    generic(runner_cfg : string := runner_cfg_default);
end entity;

architecture tb of tb_timestamp_insertion is

    constant C_CLK_PERIOD : time      := 5 ns;
    signal clk            : std_logic := '0';
    signal reset          : std_logic := '1';
    signal error          : std_logic;
    signal send           : std_logic := '0';
    signal frame_valid    : std_logic;

    signal axis_if_m             : t_axis_if_m;
    signal axis_if_s             : t_axis_if_s;
    signal frame_length        : natural;
    signal header_length       : natural;
    signal valid               : std_logic := '0';
    
    signal frame_header_fields : frame_header_fields_t;
    signal frame_full_ram      : frame_full_ram_t;
    signal frame_header_ram      : frame_header_ram_t;
    signal start_stimuli       : std_logic := '0';
    signal random_stimuli_wait : std_logic := '0';
    signal stimuli_length      : natural   := 5;
    signal ts_insertion_if     : t_ts_insertion_if;
    signal ts_valid               : std_logic := '0';
    signal data_to_send        : std_logic_vector(8-1 downto 0) := (others => '0');

    signal axis_if_in_m : t_axis_if_m; 
    signal axis_if_in_s : t_axis_if_s; 
    signal axis_if_out_m : t_axis_if_m;
    signal axis_if_out_s : t_axis_if_s;

    signal ts_association_valid : std_logic;
    signal ts_association_if : t_ts_association_if;

begin

    --------------------------------------------------------------------------
    -- CLOCK AND RESET.
    --------------------------------------------------------------------------
    clk   <= NOT clk after C_CLK_PERIOD / 2;
    reset <= '0' after 5 * (C_CLK_PERIOD);

    test_runner : process
        variable tx_fn      : std_logic_vector(15 downto 0); -- Sequence number of this Tx Frame
        variable frame_type : std_logic_vector(1 downto 0); -- Frame Type
        variable tx_num     : std_logic_vector(2 downto 0); -- Transmission atempt

    begin
        test_runner_setup(runner, runner_cfg);
        -- enable logging for passing check
        --show(get_logger(default_checker), display_handler, pass); 
        --show(get_logger(RED_CHECKER), display_handler, pass); 
        --packet_len <= 100;
        test_cases_loop : WHILE test_suite LOOP
            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- check IDLE payload generation.
            -- Expected Result: 10 IDLE payloads and TX_FN 
            ----------------------------------------------------------------------
            IF run("basic_injection_test") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: basic_injection_test");
                info("--------------------------------------------------------------------------------");
                axis_if_out_s.tready <= '1';
                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                

                ts_insertion_if.tx_ts       <= (others=>'1');
                ts_insertion_if.tod_seconds <= (others=>'1');
                ts_insertion_if.ts_applies  <= (others=>'1');
                data_to_send                <= (others=>'0');
                WAIT UNTIL rising_edge(clk);
                ts_valid <= '1';
                WAIT UNTIL rising_edge(clk);
                ts_valid <= '0';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                axis_if_out_s.tready <= '1';

                wait until axis_if_out_m.tvalid = '1' and rising_edge(clk);
                --wait until rising_edge(clk);
                for ii in 0 to C_FSO_PAYLOAD_SIZE - 1 loop
                    
                    if axis_if_out_m.tvalid = '1' then
                        frame_full_ram(ii) <= axis_if_out_m.tdata;
                        if ii < C_FRAME_HEADER_SIZE then
                            frame_header_ram(ii) <= axis_if_out_m.tdata;
                        end if;
                    end if;
                    wait until rising_edge(clk);
                end loop;
                wait for 400 ns;

                frame_header_fields <= array2frame_header(frame_header_ram);         
                wait for 1000 ns;
                check(frame_header_fields.tx_ts       = ts_insertion_if.tx_ts, "TX_TS does not match!!");
                check(frame_header_fields.tod_seconds = ts_insertion_if.tod_seconds, "tod_seconds does not match!!");
                check(frame_header_fields.ts_applies  = ts_insertion_if.ts_applies, "ts_applies does not match!!");
                check(frame_header_fields.frame_type  = "00", "frame_type does not match!!");
                check(frame_header_fields.fcch_pl     = x"0000", "fcch_pl does not match!!");
                check(frame_header_fields.pl_rate     = "0000", "pl_rate does not match!!");
                check(frame_full_ram(0) = x"AA", "First byte error");
                check(frame_full_ram(C_FSO_PAYLOAD_SIZE-1) = x"FF", "Last byte error. Frame corrupted.");

                wait for 5 us;
                info("===== TEST CASE FINISHED =====");

            ----------------------------------------------------------------------
            -- TEST CASE DESCRIPTION:
            -- check IDLE payload generation.
            -- Expected Result: 10 IDLE payloads and TX_FN 
            ----------------------------------------------------------------------
            ELSIF run("bit_mask_test") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: bit_mask_test");
                info("--------------------------------------------------------------------------------");
                axis_if_out_s.tready <= '1';
                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                

                ts_insertion_if.tx_ts       <= (others=>'0');
                ts_insertion_if.tod_seconds <= (others=>'0');
                ts_insertion_if.ts_applies  <= (others=>'0');
                data_to_send                <= (others=>'1');
                WAIT UNTIL rising_edge(clk);
                ts_valid <= '1';
                WAIT UNTIL rising_edge(clk);
                ts_valid <= '0';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                axis_if_out_s.tready <= '1';

                wait until axis_if_out_m.tvalid = '1' and rising_edge(clk);
                --wait until rising_edge(clk);
                for ii in 0 to C_FSO_PAYLOAD_SIZE - 1 loop
                    
                    if axis_if_out_m.tvalid = '1' then
                        frame_full_ram(ii) <= axis_if_out_m.tdata;
                        if ii < C_FRAME_HEADER_SIZE then
                            frame_header_ram(ii) <= axis_if_out_m.tdata;
                        end if;
                    end if;
                    wait until rising_edge(clk);
                end loop;
                wait for 400 ns;

                frame_header_fields <= array2frame_header(frame_header_ram);         
                wait for 1000 ns;
                check(frame_header_fields.tx_ts       = ts_insertion_if.tx_ts, "TX_TS does not match!!");
                check(frame_header_fields.tod_seconds = ts_insertion_if.tod_seconds, "tod_seconds does not match!!");
                check(frame_header_fields.ts_applies  = ts_insertion_if.ts_applies, "ts_applies does not match!!");
                check(frame_header_fields.frame_type  = "11", "frame_type does not match!!");
                check(frame_header_fields.fcch_pl     = x"FFFF", "fcch_pl does not match!!");
                check(frame_header_fields.pl_rate     = "1111", "pl_rate does not match!!");

                check(frame_full_ram(0) = x"AA", "First byte error");
                check(frame_full_ram(C_FSO_PAYLOAD_SIZE-1) = x"FF", "Last byte error. Frame corrupted.");

                wait for 5 us;
                info("===== TEST CASE FINISHED =====");

                ELSIF run("burst_injection_test") THEN
                info("--------------------------------------------------------------------------------");
                info("TEST CASE: burst_injection_test");
                info("--------------------------------------------------------------------------------");
                axis_if_out_s.tready <= '1';
                WAIT UNTIL reset = '0';
                WAIT UNTIL rising_edge(clk);
                WAIT UNTIL rising_edge(clk);
                

                ts_insertion_if.tx_ts       <= (others=>'1');
                ts_insertion_if.tod_seconds <= (others=>'0');
                ts_insertion_if.ts_applies  <= (others=>'1');
                data_to_send                <= (others=>'0');
                WAIT UNTIL rising_edge(clk);
                ts_valid <= '1';
                WAIT UNTIL rising_edge(clk);
                ts_valid <= '0';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                axis_if_out_s.tready <= '1';

                wait until axis_if_out_m.tvalid = '1' and rising_edge(clk);
                --wait until rising_edge(clk);
                for ii in 0 to C_FSO_PAYLOAD_SIZE - 1 loop
                    
                    if axis_if_out_m.tvalid = '1' then
                        frame_full_ram(ii) <= axis_if_out_m.tdata;
                        if ii < C_FRAME_HEADER_SIZE then
                            frame_header_ram(ii) <= axis_if_out_m.tdata;
                        end if;
                    end if;
                    wait until rising_edge(clk);
                end loop;
                wait for 400 ns;

                frame_header_fields <= array2frame_header(frame_header_ram);         
                wait for 1000 ns;
                check(frame_header_fields.tx_ts       = ts_insertion_if.tx_ts, "TX_TS does not match!!");
                check(frame_header_fields.tod_seconds = ts_insertion_if.tod_seconds, "tod_seconds does not match!!");
                check(frame_header_fields.ts_applies  = ts_insertion_if.ts_applies, "ts_applies does not match!!");
                check(frame_header_fields.frame_type  = "00", "frame_type does not match!!");
                check(frame_header_fields.fcch_pl     = x"0000", "fcch_pl does not match!!");
                check(frame_header_fields.pl_rate     = "0000", "pl_rate does not match!!");
                check(frame_full_ram(0) = x"AA", "First byte error");
                check(frame_full_ram(C_FSO_PAYLOAD_SIZE-1) = x"FF", "Last byte error. Frame corrupted.");

                wait for 30 ns;
                ts_insertion_if.tx_ts       <= (others=>'0');
                ts_insertion_if.tod_seconds <= (others=>'1');
                ts_insertion_if.ts_applies  <= (others=>'0');
                data_to_send                <= (others=>'1');
                WAIT UNTIL rising_edge(clk);
                ts_valid <= '1';
                WAIT UNTIL rising_edge(clk);
                ts_valid <= '0';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '1';
                WAIT UNTIL rising_edge(clk);
                start_stimuli <= '0';
                axis_if_out_s.tready <= '1';

                wait until axis_if_out_m.tvalid = '1' and rising_edge(clk);
                --wait until rising_edge(clk);
                for ii in 0 to C_FSO_PAYLOAD_SIZE - 1 loop
                    
                    if axis_if_out_m.tvalid = '1' then
                        frame_full_ram(ii) <= axis_if_out_m.tdata;
                        if ii < C_FRAME_HEADER_SIZE then
                            frame_header_ram(ii) <= axis_if_out_m.tdata;
                        end if;
                    end if;
                    wait until rising_edge(clk);
                end loop;
                wait for 400 ns;

                frame_header_fields <= array2frame_header(frame_header_ram);         
                wait for 1000 ns;
                check(frame_header_fields.tx_ts       = ts_insertion_if.tx_ts, "TX_TS does not match!!");
                check(frame_header_fields.tod_seconds = ts_insertion_if.tod_seconds, "tod_seconds does not match!!");
                check(frame_header_fields.ts_applies  = ts_insertion_if.ts_applies, "ts_applies does not match!!");
                check(frame_header_fields.frame_type  = "11", "frame_type does not match!!");
                check(frame_header_fields.fcch_pl     = x"FFFF", "fcch_pl does not match!!");
                check(frame_header_fields.pl_rate     = "1111", "pl_rate does not match!!");

                check(frame_full_ram(0) = x"AA", "First byte error");
                check(frame_full_ram(C_FSO_PAYLOAD_SIZE-1) = x"FF", "Last byte error. Frame corrupted.");

                wait for 5 us;
                info("===== TEST CASE FINISHED =====");

            END IF;                     
        END LOOP test_cases_loop; -- for test_suite

        WAIT FOR 20 ns;
        test_runner_cleanup(runner);
    end process;

    pr_stimuli : process
    begin
        axis_if_in_m <= C_AXIS_IF_MASTER_INIT;
        wait until start_stimuli = '1';
        wait until rising_edge(clk);
        wait until rising_edge(clk);

        wait until axis_if_in_s.tready = '1' and rising_edge(clk);

        for ii in 0 to C_FSO_PAYLOAD_SIZE - 1 loop
            if ii = C_FSO_PAYLOAD_SIZE - 1 then
                axis_if_in_m.tdata  <= x"FF";
                axis_if_in_m.tvalid <= '1';
                axis_if_in_m.tlast  <= '1';
            else
                axis_if_in_m.tdata  <= data_to_send;
                axis_if_in_m.tvalid <= '1';
                axis_if_in_m.tlast  <= '0';
                if ii = 0 then
                    axis_if_in_m.tstart <= '1';
                    axis_if_in_m.tdata  <= x"AA";
                else
                    axis_if_in_m.tstart <= '0';
                end if;

            end if;
            wait until rising_edge(clk) and axis_if_in_s.tready = '1';
        end loop;
        axis_if_in_m <= C_AXIS_IF_MASTER_INIT;

        --wait;

    end process;

--    inst_payload_receiver : entity sda_test_lib.frame_receiver
--        port map(
--            clk                 => clk, --: in std_logic;
--            axis_if_m             => axis_if_m, --: inout t_axis_if; -- Input axi-s
--            axis_if_s             => axis_if_s, --: inout t_axis_if; -- Input axi-s
--            frame_header_fields => frame_header_fields, --: out frame_header_fields_t;
--            payload_ram         => payload_ram, --: out payload_ram_t;
--            frame_length        => frame_length, --: out natural;
--            valid               => frame_valid --: out std_logic
--        );

    DUT : entity frame_header_lib.timestamp_insertion
        generic map(
            G_INSERTION_BYTE_OFFSET => 7, --: natural := 7;
            G_INSERTION_BIT_OFFSET  => 1  --: natural := 1
        )
        port map(
            clk             => clk, --: in    std_logic;
            reset           => reset, --: in    std_logic;
    
            ts_insertion_if => ts_insertion_if, --: in t_ts_insertion_if;
            ts_valid        => ts_valid, --: in std_logic;
    
            axis_if_in_m    => axis_if_in_m, --: in t_axis_if_m; -- Input axi-s
            axis_if_in_s    => axis_if_in_s, --: out t_axis_if_s; -- Input axi-s
            axis_if_out_m   => axis_if_out_m, --: out t_axis_if_m; -- Output axi-s
            axis_if_out_s   => axis_if_out_s, --: in t_axis_if_s -- Output axi-s
            ts_association_if    => ts_association_if, --: out t_ts_association_if;
            ts_association_valid => ts_association_valid  --: out std_logic
        );

    test_runner_watchdog(runner, 50 ms);
end architecture;
