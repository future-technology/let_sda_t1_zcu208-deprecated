#!/usr/bin/env python3

##-----------------------------------------------------------------------------
##  Company    : Mynaric Lasercom GmbH, Gilching, Germany
##
##  Restricted � Highly Confidential
##
##  COPYRIGHT
##
##  Ownership/Copyright (c) 2022 Mynaric Lasercom GmbH, Gilching, Germany
##  This software program is the proprietary copyright material of
##  Mynaric Lasercom GmbH. No Transfer or assignment of such copyright
##  is intended to be effected by any use of this software program.
##  This applies also to the permitted use by an authorized user under license
##  notwithstanding the terms and conditions of any agreement entered into
##  by an end user with an intermediary other than Mynaric Lasercom GmbH.
##
##  No part of this software program may be reproduced, amended, varied,
##  re-written or stored in a retrieval system in any form or by any means
##  without the prior written permission of Mynaric Lasercom GmbH.
##
##-----------------------------------------------------------------------------
## Company: Mynaric
## Engineer: Gustavo Martin
## 
## Create Date: 25/06/2022 11:40:33 AM
## Design Name: 
## Module Name: vunit_multi.py
## Project Name: LET SDA
## Target Devices: 
## Tool Versions: Vivado 2021.1
## Description: Library package to test SDA.
## 
## Dependencies: 
## 
## Revision: 
## Revision 0.01 - File Created
## Additional Comments:
## 
##--------------------------------------------------------------------------------
from pathlib import Path
from os.path import join, dirname
from vunit import VUnit
from glob import glob
import imp

def create_test_suites(prj):
    root = Path(__file__).parent
    run_scripts = glob(join(root, "*", "run.py"))

    for run_script in run_scripts:
        file_handle, path_name, description = imp.find_module("run", [dirname(run_script)])
        run = imp.load_module("run", file_handle, path_name, description)
        run.create_test_suite(prj)
        file_handle.close()

prj = VUnit.from_argv()
create_test_suites(prj)
prj.main()


