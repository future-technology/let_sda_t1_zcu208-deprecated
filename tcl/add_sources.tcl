# CUSTOMIZABLE
set_property BOARD_PART xilinx.com:zcu208:part0:2.0 [current_project]

# Add creonic_sda_oct_v3 files
source ../tcl/creonic_sda_oct_v3.tcl

# Add SDA files
source ../src/hdl/let_sda/tcl/add_let_sda.tcl

# Add IPs:
source ../src/ip/gtwizard_ultrascale_0.tcl
source ../src/ip/axi_ethernet.tcl
source ../src/ip/clk_wiz_main.tcl
source ../src/ip/clk_wiz_0.tcl
source ../src/ip/dwidth_8_to_32.tcl
source ../src/ip/dwidth_32_to_8.tcl
source ../src/ip/dc_fifo_phy.tcl
source ../src/ip/vio_fec.tcl
source ../src/ip/vio_sda.tcl
source ../src/ip/ila_axis_8bit.tcl
source ../src/ip/proc_sys_reset_0.tcl
source ../src/ip/fifo_pause_frame.tcl
#source ../src/ip/ldpc_dec.tcl
#source ../src/ip/ldpc_enc.tcl

# FSO phy files. GT transceiver example files used
add_files -fileset sources_1 -norecurse ../src/hdl/fso_phy/fso_2g5_top.vhd
add_files -fileset sources_1 -norecurse ../src/hdl/fso_phy/gtwizard_ultrascale_0_example_bit_sync.v
add_files -fileset sources_1 -norecurse ../src/hdl/fso_phy/gtwizard_ultrascale_0_example_checking_raw.v
add_files -fileset sources_1 -norecurse ../src/hdl/fso_phy/gtwizard_ultrascale_0_example_init.v
add_files -fileset sources_1 -norecurse ../src/hdl/fso_phy/gtwizard_ultrascale_0_example_reset_sync.v
add_files -fileset sources_1 -norecurse ../src/hdl/fso_phy/gtwizard_ultrascale_0_example_stimulus_raw.v
add_files -fileset sources_1 -norecurse ../src/hdl/fso_phy/gtwizard_ultrascale_0_example_top.v
add_files -fileset sources_1 -norecurse ../src/hdl/fso_phy/gtwizard_ultrascale_0_example_top_sim.v
add_files -fileset sources_1 -norecurse ../src/hdl/fso_phy/gtwizard_ultrascale_0_example_wrapper.v
#add_files -fileset sources_1 -norecurse ../src/hdl/fso_phy/gtwizard_ultrascale_0_example_wrapper_functions.v
add_files -fileset sources_1 -norecurse ../src/hdl/fso_phy/gtwizard_ultrascale_0_prbs_any.v

# ETH phy files.
add_files -fileset sources_1 -norecurse ../src/hdl/ethernet_phy/eth_2g5_top.vhd
add_files -fileset sources_1 -norecurse ../src/hdl/ethernet_phy/axi_ethernet_support.v
add_files -fileset sources_1 -norecurse ../src/hdl/ethernet_phy/axi_ethernet_reset_sync.v
add_files -fileset sources_1 -norecurse ../src/hdl/ethernet_phy/axi_ethernet_support_clocks.v
add_files -fileset sources_1 -norecurse ../src/hdl/ethernet_phy/axi_ethernet_support_resets.v

# Generic
add_files -fileset constrs_1 -norecurse ../src/constr/ZCU208_master.xdc
add_files -fileset constrs_1 -norecurse ../src/constr/gtwizard_ultrascale_0_example_top.xdc
add_files -fileset constrs_1 -norecurse ../src/constr/axi_ethernet_example_design.xdc
add_files -fileset constrs_1 -norecurse ../src/constr/vio_constraints.xdc
add_files -fileset sources_1 -norecurse ../src/hdl/top/let_sda_soft_top.vhd
add_files -fileset sources_1 -norecurse ../src/hdl/let_top/top_let.vhd
add_files -fileset sources_1 -norecurse ../src/hdl/let_top/let_pckg.vhd
add_files -fileset sources_1 -norecurse ../src/hdl/clock/clock_gen_top.vhd
add_files -fileset sim_1 -norecurse ../src/hdl/top/sim/let_sda_hard_top_tb.vhd
add_files -fileset sim_1 -norecurse ../src/hdl/top/sim/let_sda_soft_top_tb.vhd

set_property top let_sda_soft_top_tb [get_filesets sim_1]
update_compile_order -fileset sim_1




