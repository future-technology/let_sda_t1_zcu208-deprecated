################################################################################
##
##  File Name      : build.tcl
##  Initial Author : Gustavo Martin <gustavo.martin@mynaric.com>
##
################################################################################
##
##  File Summary   : Vivado build TCL script executed from build.sh
##
################################################################################


################################################################################
## Args
set project_dir  "[pwd]"
#foreach i $argv {puts $i}

if { [llength $argv] == 9 } {

	set PRJ_NAME [lindex $argv 0]
	set REQ_VIVADO_VERSION [lindex $argv 1]
	set BOARD [lindex $argv 2]
	set PART [lindex $argv 3]
	set FPGA_TOP [lindex $argv 4]
	set TARGET_LANGUAGE [lindex $argv 5]
	set SIMULATOR_LANGUAGE [lindex $argv 6]
	set DEFAULT_LIB [lindex $argv 7]
	set TARGET_SIMULATOR [lindex $argv 8]
	puts "BOARD: $BOARD "
} else {
	puts "No board."
	set PRJ_NAME [lindex $argv 0]
	set REQ_VIVADO_VERSION [lindex $argv 1]
	set PART [lindex $argv 2]
	set FPGA_TOP [lindex $argv 3]
	set TARGET_LANGUAGE [lindex $argv 4]
	set SIMULATOR_LANGUAGE [lindex $argv 5]
	set DEFAULT_LIB [lindex $argv 6]
	set TARGET_SIMULATOR [lindex $argv 7]
    set BOARD ""
}

puts "PRJ_NAME: $PRJ_NAME"
puts "REQ_VIVADO_VERSION: $REQ_VIVADO_VERSION"
puts "PART: $PART "
puts "FPGA_TOP: $FPGA_TOP "
puts "TARGET_LANGUAGE: $TARGET_LANGUAGE "
puts "SIMULATOR_LANGUAGE: $SIMULATOR_LANGUAGE "
puts "DEFAULT_LIB: $DEFAULT_LIB "
puts "TARGET_SIMULATOR: $TARGET_SIMULATOR "

################################################################################

# Vivado version verification
set vivado_version [version -short]
if {[lsearch -exact $REQ_VIVADO_VERSION $vivado_version] < 0} {
    puts ""
	puts -nonewline "ERROR: Please use Vivado '[lindex $REQ_VIVADO_VERSION 0]'"
	puts " and not '$vivado_version' to build the system!"
	exit 2
    }
if {![string equal "[lindex $REQ_VIVADO_VERSION 0]" "$vivado_version"]} {
    puts -nonewline "WARNING: You are using Vivado '$vivado_version' and not "
    puts "the recommended '[lindex $REQ_VIVADO_VERSION 0]'!"
}
set vivado_year [lindex [split "${vivado_version}" "."] 0]



################################################################################
## Set up project
set fileset_constrs_name "constrs_1"
set fileset_sources_name "sources_1"
set fileset_sim_name     "sim_1"


puts "Creating project \"$PRJ_NAME\" in $project_dir with $PART"

puts "Create filesets constraints, source, simulation"
create_project $PRJ_NAME $project_dir -part $PART -force
set cur_prj [get_projects $PRJ_NAME]

#if { $BOARD eq "" } {
#    puts "Board part not selected."
#} else {
#    set_property BOARD_PART $BOARD $cur_prj
#}

set_property BOARD_PART $BOARD $cur_prj
set_property target_language  $TARGET_LANGUAGE  $cur_prj
set_property default_lib      $DEFAULT_LIB      $cur_prj
set_property target_simulator $TARGET_SIMULATOR $cur_prj

if {[info exists simulator_language]} {
	set_property simulator_language $target_simulator $cur_prj
}
	
	
# Set project properties
set obj [current_project]
set_property -name "part" -value $PART -objects $obj
set_property -name "default_lib" -value $DEFAULT_LIB -objects $obj
set_property -name "simulator_language" -value "Mixed" -objects $obj
set_property -name "target_language" -value $TARGET_LANGUAGE -objects $obj
set_property -name "corecontainer.enable" -value "1" -objects $obj
set_property -name "enable_core_container" -value "1" -objects $obj
set_property -name "enable_vhdl_2008" -value "1" -objects $obj
set_property -name "ip_cache_permissions" -value "read write" -objects $obj
set_property -name "ip_output_repo" -value "$project_dir/${PRJ_NAME}.cache/ip" -objects $obj
set_property -name "mem.enable_memory_map_generation" -value "1" -objects $obj
set_property -name "sim.central_dir" -value "$project_dir/${PRJ_NAME}.ip_user_files" -objects $obj
set_property -name "sim.ip.auto_export_scripts" -value "1" -objects $obj
set_property -name "webtalk.activehdl_export_sim" -value "15" -objects $obj
set_property -name "webtalk.ies_export_sim" -value "15" -objects $obj
set_property -name "webtalk.modelsim_export_sim" -value "15" -objects $obj
set_property -name "webtalk.questa_export_sim" -value "15" -objects $obj
set_property -name "webtalk.riviera_export_sim" -value "15" -objects $obj
set_property -name "webtalk.vcs_export_sim" -value "15" -objects $obj
set_property -name "webtalk.xsim_export_sim" -value "15" -objects $obj
set_property -name "xpm_libraries" -value "XPM_CDC XPM_FIFO XPM_MEMORY" -objects $obj
	
	
# Create 'synth_1' run (if not found)
if {[string equal [get_runs -quiet synth_1] ""]} {
    create_run -name synth_1 -part $PART -strategy "Flow_PerfOptimized_high" -report_strategy {No Reports} -constrset constrs_1
} else {
  set_property strategy "Flow_PerfOptimized_high" [get_runs synth_1]
  #set_property flow "Vivado Synthesis 2020" [get_runs synth_1]
}
set obj [get_runs synth_1]
set_property set_report_strategy_name 1 $obj
set_property report_strategy {Vivado Synthesis Default Reports} $obj
set_property set_report_strategy_name 0 $obj
# Create 'synth_1_synth_report_utilization_0' report (if not found)
if { [ string equal [get_report_configs -of_objects [get_runs synth_1] synth_1_synth_report_utilization_0] "" ] } {
  create_report_config -report_name synth_1_synth_report_utilization_0 -report_type report_utilization:1.0 -steps synth_design -runs synth_1
}
set obj [get_report_configs -of_objects [get_runs synth_1] synth_1_synth_report_utilization_0]
if { $obj != "" } {

}
set obj [get_runs synth_1]
set_property -name "part" -value $PART -objects $obj
set_property -name "strategy" -value "Flow_PerfOptimized_high" -objects $obj

# set the current synth run
current_run -synthesis [get_runs synth_1]

#report_ip_status -name ip_status
	
source ../tcl/strategy.tcl

# add_files -scan_for_includes ../src/


# findFiles can find files in subdirs and add it into a list
proc findFiles { basedir pattern } {

    # Fix the directory name, this ensures the directory name is in the
    # native format for the platform and contains a final directory seperator
    set basedir [string trimright [file join [file normalize $basedir] { }]]
    set fileList {}
    array set myArray {}
    
    # Look in the current directory for matching files, -type {f r}
    # means ony readable normal files are looked at, -nocomplain stops
    # an error being thrown if the returned list is empty

    foreach fileName [glob -nocomplain -type {f r} -path $basedir $pattern] {
        lappend fileList $fileName
    }
    
    # Now look for any sub direcories in the current directory
    foreach dirName [glob -nocomplain -type {d  r} -path $basedir *] {
        # Recusively call the routine on the sub directory and append any
        # new files to the results
        # put $dirName
        set subDirList [findFiles $dirName $pattern]
        if { [llength $subDirList] > 0 } {
            foreach subDirFile $subDirList {
                lappend fileList $subDirFile
            }
        }
    }
    return $fileList
}
 puts "Searching for source files..."
# set SrcVHD [findFiles ../src/ "*.vhd"]
# set SrcVer [findFiles ../src/ "*.v"]
# set SrcNGC [findFiles ../src/ "*.ngc"]
# set SrcXCIX [findFiles ../src/ "*.xcix"]
# set SrcXCI [findFiles ../src/ "*.xci"]
# set SrcXDC [findFiles ../src/ "*.xdc"]
# set SrctbVHD [findFiles ../src/ "tb_*.vhd"]

# #Add NGC source files
# if { [llength $SrcNGC] > 0 } {
#     add_files -norecurse $SrcNGC
#     puts "$SrcNGC"
# }
# #Add XCIX source files
# if { [llength $SrcXCIX] > 0 } {
#     add_files -norecurse $SrcXCIX
#     export_ip_user_files -of_objects [get_files $SrcXCIX] -force -quiet
#     puts "$SrcXCIX"
# }
# #Add XCI source files
# if { [llength $SrcXCI] > 0 } {
#     add_files -norecurse $SrcXCI
#     export_ip_user_files -of_objects [get_files $SrcXCI] -force -quiet
#     puts "$SrcXCI"
# }
# #Add VHDL source files
# if { [llength $SrcVHD] > 0 } {
#     add_files $SrcVHD
#     puts "$SrcVHD"
# }
# #Add XDC source files
# if { [llength $SrcXDC] > 0 } {
#     add_files -fileset constrs_1 -norecurse $SrcXDC
#     puts "$SrcXDC"
# }
# #Add Verilog source files
# if { [llength $SrcVer] > 0 } {
#     add_files $SrcVer
#     puts "$SrcVer"
# }
# #Exclude testbenches from synth
# if { [llength $SrctbVHD] > 0 } {
#     set_property used_in_synthesis false [get_files $SrctbVHD]
#	 puts "$SrctbVHD"
# }
 
 
 
# Add sources or generate IP from tcl:
source ../tcl/add_sources.tcl
#source ../tcl/add_ip.tcl

#source ../src/add_scripts/add_hdl.tcl
set_property file_type {VHDL 2008} [get_files *.vhd]
#source ../src/add_scripts/add_ip.tcl
 puts "Finished adding files..."


foreach bd_script [glob -nocomplain -dir ../bd/ *.tcl] {
    source $bd_script
    set basename [file rootname [file tail $bd_script]]
    make_wrapper -files [get_files ./${PRJ_NAME}.srcs/sources_1/bd/${basename}/${basename}.bd] -top  
    set wrapper_dir "./${PRJ_NAME}.srcs/sources_1/bd/${basename}/hdl/${basename}_wrapper.vhd"
    add_files -norecurse $wrapper_dir
    update_compile_order -fileset sources_1
	validate_bd_design
}

update_compile_order -fileset sources_1
set_property top $FPGA_TOP [current_fileset]
update_compile_order -fileset sources_1

#Strategy by default:
#set_property strategy Performance_ExtraTimingOpt [get_runs impl_1]

exit 0		
	
