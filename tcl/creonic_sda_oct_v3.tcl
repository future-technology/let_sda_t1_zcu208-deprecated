set USE_XILINX_FEC false
set HW_BUILD_PATH ./build
set XILINX_CORES_PATH ../src/hdl/creonic_sda_oct_v3/xilinx_ip
set CREONIC_ENC  ../src/hdl/creonic_sda_oct_v3/creonic_ldpc_enc
set CREONIC_DEC  ../src/hdl/creonic_sda_oct_v3/creonic_ldpc_dec
set CREONIC_SIM  ../src/hdl/creonic_sda_oct_v3/sim

set sub_libs_list {dec_viterbi \
				   frame_encoding \
				   frame_decoding}

foreach sub_lib $sub_libs_list {
	set sub_lib_dir ../src/hdl/creonic_sda_oct_v3/$sub_lib
	set sub_lib_files [glob $sub_lib_dir/*.vhd]
	foreach vhdl_file $sub_lib_files {
		add_files -fileset sources_1 -norecurse $vhdl_file
		set_property library $sub_lib [get_files $vhdl_file]
	}
}

set XILINX_FEC {ldpc_dec ldpc_enc}

set XILINX_CORES {
	axis_128bit_clk_conv \
	axis_16bit_clk_conv \
	axis_64bit_clk_conv \
	dc_fifo_phy \
	dwidth_16_to_8 \
	dwidth_8_to_16 \
	dwidth_8_to_16_llr}

if {$USE_XILINX_FEC} {
	set XILINX_CORES [concat $XILINX_FEC $XILINX_CORES]
}

#foreach ip_cores $XILINX_CORES {
#	puts "Adding $ip_cores to the project"
#	add_files -fileset sources_1 -norecurse $XILINX_CORES_PATH/$ip_cores/$ip_cores.xci
#	set_property library rcs2_receiver [get_files $XILINX_CORES_PATH/$ip_cores/$ip_cores.xci]
#	upgrade_ip [get_ips  $ip_cores] -log ${ip_cores}_ip_upgrade.log
#
#	reset_target all [get_files  $XILINX_CORES_PATH/$ip_cores/$ip_cores.xci]
#
#	export_ip_user_files -of_objects [get_ips $ip_cores] -no_script -sync -force -quiet
#	generate_target all [get_files  $XILINX_CORES_PATH/$ip_cores/$ip_cores.xci]
#	catch { config_ip_cache -export [get_ips -all ${ip_cores}] }
#	export_ip_user_files -of_objects [get_files $XILINX_CORES_PATH/$ip_cores/$ip_cores.xci] -no_script -sync -force -quiet
#	create_ip_run [get_files -of_objects [get_fileset sources_1] $XILINX_CORES_PATH/$ip_cores/$ip_cores.xci]
#}

# Creonic IPs:
##
source ../src/ip/axis_16bit_clk_conv.tcl
source ../src/ip/axis_64bit_clk_conv.tcl
source ../src/ip/axis_128bit_clk_conv.tcl
#source ../src/ip/dc_fifo_phy.tcl # Not used
source ../src/ip/dwidth_8_to_16.tcl
source ../src/ip/dwidth_8_to_16_llr.tcl
source ../src/ip/dwidth_16_to_8.tcl
################################################################################


#set ooc_runs [get_runs -filter {IS_SYNTHESIS && name != "synth_1"} ]
#foreach run $ooc_runs { reset_run $run }

#if { [ llength $ooc_runs ] } {
#  launch_runs -jobs 8 $ooc_runs
#}
#foreach run $ooc_runs { wait_on_run $run }


add_files -fileset sources_1 -norecurse $XILINX_CORES_PATH/pkg_components.vhd
set_property library xilinx_ip [get_files $XILINX_CORES_PATH/pkg_components.vhd]

add_files -fileset sources_1 -norecurse $CREONIC_ENC/src/enc_ldpc_struct_top.edn
set_property library enc_ldpc_struct [get_files $CREONIC_ENC/src/enc_ldpc_struct_top.edn]

add_files -fileset sources_1 -norecurse $CREONIC_DEC/src/dec_ldpc_top.edn
set_property library dec_ldpc [get_files $CREONIC_DEC/src/dec_ldpc_top.edn]

#add_files -fileset sources_1 -norecurse -quiet  
#add_files -fileset constrs_1 -norecurse -quiet ./frame_fec_top.xdc
#set_property top frame_fec_top [current_fileset]
#set_property -name {STEPS.SYNTH_DESIGN.ARGS.MORE OPTIONS} -value { } -objects [get_runs synth_1]
#update_compile_order -fileset sources_1
#update_compile_order -fileset sources_1
#launch_runs synth_1
#wait_on_run synth_1
#open_run synth_1 -name netlist_1
#report_utilization      -file $PROJECT_PATH/$PROJECT_NAME.runs/synth_1/${PROJECT_NAME}_utilization_synth.rpt -hierarchical -append
#report_timing_summary   -file $PROJECT_PATH/$PROJECT_NAME.runs/synth_1/${PROJECT_NAME}_timing_synth.rpt -max_paths 350
#report_high_fanout_nets -file $PROJECT_PATH/$PROJECT_NAME.runs/synth_1/${PROJECT_NAME}_fanout_synth.rpt -max_nets 10
#write_edif -security_mode all -force $PROJECT_PATH/frame_fec_top.edn

add_files -fileset sim_1 -norecurse $XILINX_CORES_PATH/ldpc_enc/ldpc_enc_sim_netlist.vhdl
add_files -fileset sim_1 -norecurse $XILINX_CORES_PATH/ldpc_dec/ldpc_dec_sim_netlist.vhdl

add_files -fileset sim_1 -norecurse $CREONIC_SIM/tb_header_encoding.vhdl
add_files -fileset sim_1 -norecurse $CREONIC_SIM/tb_payload_encoding.vhdl
add_files -fileset sim_1 -norecurse $CREONIC_SIM/test_vectors_pkg.vhdl
set_property library frame_decoding [get_files $CREONIC_SIM/test_vectors_pkg.vhdl]

