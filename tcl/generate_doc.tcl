# CUSTOMIZABLE
################################################################################
## Args
set project_dir  "[pwd]"
#foreach i $argv {puts $i}
set PRJ_NAME [lindex $argv 0]
set BD_NAME [lindex $argv 1]
open_project ${PRJ_NAME}.xpr

# Generate doc:
open_bd_design ./${PRJ_NAME}.srcs/sources_1/bd/${BD_NAME}/${BD_NAME}.bd
write_bd_layout -force -scope all -format pdf -orientation portrait ../doc/${BD_NAME}_top.pdf
write_bd_layout -force -scope all -hierarchy ETHERNET_CLIENT -format pdf -orientation portrait  ../doc/ETHERNET_CLIENT.pdf
write_bd_layout -force -scope all -hierarchy FSO_TX_RX_Control -format pdf -orientation portrait  ../doc/FSO_TX_RX_Control.pdf
write_bd_layout -force -scope all -hierarchy Backplane -format pdf -orientation portrait  ../doc/Backplane.pdf
write_bd_layout -force -scope all -hierarchy Service_Interface -format pdf -orientation portrait  ../doc/Service_Interface.pdf
write_bd_layout -force -scope all -hierarchy LET1G_MB_Core -format pdf -orientation portrait  ../doc/LET1G_MB_Core.pdf
write_bd_layout -force -scope all -hierarchy Client_side_GPIOs -format pdf -orientation portrait  ../doc/Client_side_GPIOs.pdf
close_bd_design [get_bd_designs *]
close_project
exit

