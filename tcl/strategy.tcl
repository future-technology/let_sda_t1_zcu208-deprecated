# CUSTOMIZABLE
# Set properties for Synthesis and Implementation (Custom field)
puts "Setting custom strategy properties..."
#set_property strategy Flow_PerfOptimized_high [get_runs synth_1]
#set_property STEPS.SYNTH_DESIGN.ARGS.BUFG 0 [get_runs synth_1]
#set_property STEPS.SYNTH_DESIGN.ARGS.FANOUT_LIMIT 1000 [get_runs synth_1]
