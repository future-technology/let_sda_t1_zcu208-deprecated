
## Args
set project_dir  "[pwd]"
#foreach i $argv {puts $i}
set PRJ_NAME [lindex $argv 0]
set BD_NAME [lindex $argv 1]

set_msg_config -id {[IP_Flow 19-3664]} -new_severity INFO
open_project ${PRJ_NAME}.xpr
puts "[pwd]"

################################################################################
# CUSTOMIZABLE
################################################################################

#Call scripts to save IP into tcl
source ../src/hdl/let_sda/tcl/save_let_sda_ip.tcl

#Generic:
# write_ip_tcl -force [get_ips *] ../ip/ip.tcl
write_ip_tcl -force [get_ips gtwizard_ultrascale_0] ../src/ip/gtwizard_ultrascale_0.tcl
write_ip_tcl -force [get_ips axi_ethernet] ../src/ip/axi_ethernet.tcl
write_ip_tcl -force [get_ips clk_wiz_main] ../src/ip/clk_wiz_main.tcl
write_ip_tcl -force [get_ips clk_wiz_0] ../src/ip/clk_wiz_0.tcl
write_ip_tcl -force [get_ips dwidth_8_to_32] ../src/ip/dwidth_8_to_32.tcl
write_ip_tcl -force [get_ips dwidth_32_to_8] ../src/ip/dwidth_32_to_8.tcl
write_ip_tcl -force [get_ips dc_fifo_phy] ../src/ip/dc_fifo_phy.tcl
write_ip_tcl -force [get_ips vio_fec] ../src/ip/vio_fec.tcl
write_ip_tcl -force [get_ips proc_sys_reset_0] ../src/ip/proc_sys_reset_0.tcl
write_ip_tcl -force [get_ips vio_sda] ../src/ip/vio_sda.tcl
write_ip_tcl -force [get_ips ila_axis_8bit] ../src/ip/ila_axis_8bit.tcl
write_ip_tcl -force [get_ips fifo_pause_frame] ../src/ip/fifo_pause_frame.tcl
#write_ip_tcl -force [get_ips ldpc_dec] ../src/ip/ldpc_dec.tcl
#write_ip_tcl -force [get_ips ldpc_enc] ../src/ip/ldpc_enc.tcl
##
# Creonic IPs:
##
write_ip_tcl -force [get_ips axis_16bit_clk_conv] ../src/ip/axis_16bit_clk_conv.tcl
write_ip_tcl -force [get_ips axis_64bit_clk_conv] ../src/ip/axis_64bit_clk_conv.tcl
write_ip_tcl -force [get_ips axis_128bit_clk_conv] ../src/ip/axis_128bit_clk_conv.tcl
write_ip_tcl -force [get_ips dc_fifo_phy] ../src/ip/dc_fifo_phy.tcl
write_ip_tcl -force [get_ips dwidth_8_to_16] ../src/ip/dwidth_8_to_16.tcl
write_ip_tcl -force [get_ips dwidth_8_to_16_llr] ../src/ip/dwidth_8_to_16_llr.tcl
write_ip_tcl -force [get_ips dwidth_16_to_8] ../src/ip/dwidth_16_to_8.tcl
################################################################################

close_project